package com.rongcloud;


import io.rong.imkit.RongIM;
import io.rong.imkit.RongIM.ConversationBehaviorListener;
import io.rong.imkit.RongIM.OnReceiveMessageListener;
import io.rong.imkit.RongIM.OnSendMessageListener;
import io.rong.imlib.RongIMClient.ConversationType;
import io.rong.imlib.RongIMClient.Message;
import io.rong.imlib.RongIMClient.UserInfo;
import io.rong.message.ImageMessage;
import io.rong.message.RichContentMessage;
import io.rong.message.TextMessage;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.ddoctor.user.activity.ask.DoctorDetailActivity;
import com.ddoctor.user.config.AppBuildConfig;
import com.ddoctor.utils.MyUtils;

/**
 * 融云SDK事件监听处理。 把事件统一处理，开发者可直接复制到自己的项目中去使用。
 * <p/>
 * 该类包含的监听事件有： 1、消息接收器：OnReceiveMessageListener。
 * 2、发出消息接收器：OnSendMessageListener。 3、用户信息提供者：GetUserInfoProvider。
 * 4、好友信息提供者：GetFriendsProvider。 5、群组信息提供者：GetGroupInfoProvider。
 * 6、会话界面操作的监听器：ConversationBehaviorListener。
 * 7、连接状态监听器，以获取连接相关状态：ConnectionStatusListener。 8、地理位置提供者：LocationProvider。
 */
public final class RongCloudEvent implements RongIM.GetUserInfoProvider,
		RongIM.GetFriendsProvider, RongIM.ConnectionStatusListener,
		OnReceiveMessageListener, OnSendMessageListener,
		ConversationBehaviorListener {

	private static final String TAG = RongCloudEvent.class.getSimpleName();

	private static RongCloudEvent mRongCloudInstance;
	private String userId;
	private Context mContext;

	/**
	 * 获取RongCloud 实例。
	 * 
	 * @return RongCloud。
	 */
	public static RongCloudEvent getInstance() {
		return mRongCloudInstance;
	}

	/**
	 * 初始化 RongCloud.
	 * 
	 * @param context
	 *            上下文。
	 */
	public static void init(Context context) {

		if (mRongCloudInstance == null) {

			synchronized (RongCloudEvent.class) {

				if (mRongCloudInstance == null) {
					mRongCloudInstance = new RongCloudEvent(context);
				}
			}
		}
	}

	/**
	 * 构造方法。
	 * 
	 * @param context
	 *            上下文。
	 */
	private RongCloudEvent(Context context) {
		mContext = context;
		initDefaultListener();
	}

	/**
	 * RongIM.init(this) 后直接可注册的Listener。
	 */
	private void initDefaultListener() {
		RongIM.setGetUserInfoProvider(this, true);// 设置用户信息提供者。
		RongIM.setGetFriendsProvider(this); // 设置好友关系提供者。
		RongIM.setConversationBehaviorListener(this);
	}

	/*
	 * 连接成功注册。 <p/> 在RongIM-connect-onSuccess后调用。
	 */
	public void setOtherListener() {
		RongIM.getInstance().setReceiveMessageListener(this);
		RongIM.getInstance().setSendMessageListener(this);// 设置发出消息接收监听器.
		RongIM.getInstance().setConnectionStatusListener(this);// 设置连接状态监听器。
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Override
	public UserInfo getUserInfo(String userId) {
		return RCProvider.getInstance().getUserInfoById(userId);
	}

	@Override
	public List<UserInfo> getFriends() {
		return RCProvider.getInstance().getmDocInfos();
	}

	@Override
	public void onChanged(ConnectionStatus connectionStatus) {
		if (connectionStatus == ConnectionStatus.DISCONNECTED) {
			
		}
	}

	@Override
	public void onReceived(Message msg, int left) {
		
	}

	@Override
	public Message onSent(Message msg) {
		return null;
	}

	/**
	 * ConversationBehaviorListener
	 * 
	 * @param context
	 * @param msg
	 * @return
	 */
	@Override
	public boolean onClickMessage(Context context, Message msg) {
		if (msg.getContent() instanceof ImageMessage) {
//			ImageMessage imageMessage = (ImageMessage) msg.getContent();
//			String[] urlArray = new String[] { (imageMessage.getLocalUri() == null ? imageMessage
//					.getRemoteUri() : imageMessage.getLocalUri()).toString() };
//			Intent intent = new Intent(context, PhotoViewAct.class);
//			Bundle data = new Bundle();
//			data.putStringArray("url", urlArray);
//			intent.putExtra(Const.BUNDLEKEY, data);
//			context.startActivity(intent);

		} else if (msg.getContent() instanceof TextMessage) {

		} else if (msg.getContent() instanceof RichContentMessage) {
			
		}

		return false;
	}

	/**
	 * ConversationBehaviorListener
	 * 
	 * @param context
	 * @param conversationType
	 * @param userInfo
	 * @return
	 */
	@Override
	public boolean onClickUserPortrait(Context context,
			ConversationType conversationType, UserInfo userInfo) {
		String userId2 = userInfo.getUserId();
		MyUtils.showLog("点击头像   "+userId2+" "+userInfo.toString());
		if (userId2.startsWith(RongCloudIdType.RC_TYPE_DOC.getType())) {
			Intent intent = new Intent();
			Bundle data = new Bundle();
			data.putInt("doctorId", Integer.valueOf(userId2.substring(1)));
//			data.putInt("hasRelation", 3);
			intent.putExtra(AppBuildConfig.BUNDLEKEY, data);
			intent.setClass(context, DoctorDetailActivity.class);
			context.startActivity(intent);
		} else if (userId2.startsWith(RongCloudIdType.RC_TYPE_PATIENT.getType())) {
			if (null != userId && userId2.contains(userId)) {
				MyUtils.showLog("", "点击本人头像");
			} else {
				MyUtils.showLog("", "点击其他病友头像");
			}
		}

		return false;
	}

}
