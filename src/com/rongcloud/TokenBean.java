package com.rongcloud;

public class TokenBean {

	private int code;
	private String userId;
	private String token;

	public TokenBean() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TokenBean(int code, String userId, String token) {
		super();
		this.code = code;
		this.userId = userId;
		this.token = token;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

}
