package com.rongcloud;

/**
 * 融云用户ID 类型
 * 
 * @author 萧
 * @Date 2015-5-3下午7:04:15
 * @TODO TODO
 */
public enum RongCloudIdType {

	RC_TYPE_DOC("d"), RC_TYPE_PATIENT("p"), RC_TYPE_SELF("");
	private String type;

	RongCloudIdType(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}

}
