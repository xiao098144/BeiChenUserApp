package com.rongcloud;

import io.rong.imlib.RongIMClient.UserInfo;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;

import com.ddoctor.enums.RetError;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.service.Connect2RongCloud;
import com.ddoctor.user.task.GetRongYunTokenTask;
import com.ddoctor.user.task.TaskPostCallBack;
import com.ddoctor.user.wapi.bean.DoctorBean;
import com.ddoctor.user.wapi.bean.PatientBean;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.MyUtils;


/**
 * 融云用户体系Provider
 * 
 * @author 萧
 * @Date 2015-5-2下午9:02:08
 * @TODO TODO
 */
public class RCProvider {

	private static RCProvider provider;
	private String mUserId;
	private Context mContext;

	public static RCProvider getInstance() {
		if (null == provider) {
			provider = new RCProvider();
		}
		return provider;
	}

	private RCProvider() {

	}

	public static void init(Context context) {
		provider = new RCProvider(context);
	}

	private RCProvider(Context context) {
		mContext = context;
	}

	public void setUserId(String userId) {
		this.mUserId = userId;
	}

	public String getUserId() {
		return mUserId;
	}

	private List<UserInfo> mUserInfos;
	private List<UserInfo> mDocInfos;

	public List<UserInfo> getmUserInfos() {
		return mUserInfos;
	}

	public void setmUserInfos(List<UserInfo> mUserInfos) {
		this.mUserInfos = mUserInfos;
	}

	public List<UserInfo> getmDocInfos() {
		return mDocInfos;
	}

	public void setmDocInfos(List<DoctorBean> docInfos) {
		mDocInfos = new ArrayList<UserInfo>();
		for (int i = 0; i < docInfos.size(); i++) {
			DoctorBean doctorBean = docInfos.get(i);
			UserInfo userInfo = new UserInfo(RongCloudIdType.RC_TYPE_DOC.getType()+doctorBean.getId(),
					doctorBean.getName(), doctorBean.getImage());
			mDocInfos.add(userInfo);
		}
	}

	/**
	 * 获取用户信息
	 * 
	 * @param userId
	 * @return
	 */
	public UserInfo getUserInfoById(String userId) {

		UserInfo userInfoReturn = null;
		if (!TextUtils.isEmpty(userId)) {  
			Log.e("RCProvider", "userId = " + userId);
			if (userId.startsWith(RongCloudIdType.RC_TYPE_DOC.getType())) {
				userInfoReturn = getUserFromList(mDocInfos, userId);
			} else if (userId.startsWith(RongCloudIdType.RC_TYPE_PATIENT
					.getType())) {
				if (null != mUserId && userId.contains(mUserId)) {
					try {
						PatientBean patientBean = DataModule.getInstance().getLoginedUserInfo();
						userInfoReturn = new UserInfo(userId,
								patientBean.getName(),
								patientBean.getImage());
					} catch (Exception e) {
						Log.e("", "个人信息为空");
					}
				} else {
					userInfoReturn = getUserFromList(mUserInfos, userId);
				}
			}
		}

		return userInfoReturn;
	}

	private UserInfo getUserFromList(List<UserInfo> list, String userId) {
		UserInfo userInfoReturn = null;
		if (list != null) {
			for (UserInfo userInfo : list) {
				if (userId.equals(userInfo.getUserId())) {
					userInfoReturn = userInfo;
					break;
				}
			}
		}
		return userInfoReturn;
	}

	/**
	 * 通过userid 获得username
	 * 
	 * @param userId
	 * @return
	 */
	public String getUserNameByUserId(String userId) {
		UserInfo userInfoReturn = null;
		if (!TextUtils.isEmpty(userId) && mUserInfos != null) {
			for (UserInfo userInfo : mUserInfos) {
				if (userId.equals(userInfo.getUserId())) {
					userInfoReturn = userInfo;
					break;
				}
			}
		}
		return userInfoReturn.getName();
	}

	/**
	 * 获取用户信息列表
	 * 
	 * @param userIds
	 * @return
	 */
	public List<UserInfo> getUserInfoByIds(String[] userIds) {

		List<UserInfo> userInfoList = new ArrayList<UserInfo>();

		if (userIds != null && userIds.length > 0) {
			for (String userId : userIds) {
				for (UserInfo userInfo : mUserInfos) {
					if (userId.equals(userInfo.getUserId())) {
						userInfoList.add(userInfo);
					}
				}
			}
		}
		return userInfoList;
	}

	public void getRCToken() {
		GetRongYunTokenTask task  = new GetRongYunTokenTask();
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {
			
			@Override
			public void taskFinish(DDResult result) {
				if (result.getError() == RetError.NONE) {
					MyUtils.showLog("获取融云token成功");
					MyUtils.showLog("融云Id  "+DataModule.getInstance().getRYUserId()+"  token "+DataModule.getInstance().getRYToken());
					Intent intent = new Intent();
					intent.setClass(mContext, Connect2RongCloud.class);
					
					mContext.startService(intent);
				} else {
//					ToastUtil.showToast(result.getErrorMessage());
				}
			}
		});
		task.executeParallel("");

	}

	/** 获取我的医生列表 */
	public void getAllMyDocList() {
		

	}
	
}
