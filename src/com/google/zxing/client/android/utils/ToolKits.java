package com.google.zxing.client.android.utils;

import android.content.Context;

public class ToolKits
{

	/**
	 *将dip转换为像素.
	 * 
	 * @author Jack Jiang, 2014-05-21
	 */
	public static int dip2px(Context context, float dipValue)
	{
		final float scale = context.getResources().getDisplayMetrics().density;
		return (int) (dipValue * scale + 0.5f);
	}

}
