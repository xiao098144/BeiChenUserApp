/*
 * Copyright (C) 2008 ZXing authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.zxing.client.android;

import java.io.IOException;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.os.Handler;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.Window;
import android.view.WindowManager;

import com.beichen.user.R;
import com.google.zxing.Result;
import com.google.zxing.client.android.camera.CameraManager;

/**
 * The barcode reader activity itself. This is loosely based on the CameraPreview
 * example included in the Android SDK.
 *
 * @author dswitkin@google.com (Daniel Switkin)
 * @author Sean Owen
 */
public  class CaptureActivity extends Activity implements SurfaceHolder.Callback {

//  private static final String TAG = CaptureActivity.class.getSimpleName();

//	public  static final String INTENT_DATA_ID="__intent_data_id";
//	private Object intent_data;

//  private static final Set<ResultMetadataType> DISPLAYABLE_METADATA_TYPES;
//  static {
//    DISPLAYABLE_METADATA_TYPES = new HashSet<ResultMetadataType>(5);
//    DISPLAYABLE_METADATA_TYPES.add(ResultMetadataType.ISSUE_NUMBER);
//    DISPLAYABLE_METADATA_TYPES.add(ResultMetadataType.SUGGESTED_PRICE);
//    DISPLAYABLE_METADATA_TYPES.add(ResultMetadataType.ERROR_CORRECTION_LEVEL);
//    DISPLAYABLE_METADATA_TYPES.add(ResultMetadataType.POSSIBLE_COUNTRY);
//  }

//  private enum Source {
//    NATIVE_APP_INTENT,
//    PRODUCT_SEARCH_LINK,
//    ZXING_LINK,
//    NONE
//  }

  private CaptureActivityHandler handler;
  private ViewfinderView viewfinderView;
//  private TextView statusView;
//  private View resultView;
//  private Result lastResult;
  private boolean hasSurface;
//  private Source source;
//  private String sourceUrl;
//  private String returnUrlTemplate;
//  private Vector<BarcodeFormat> decodeFormats;
//  private String characterSet;
//  private String versionName;
  private InactivityTimer inactivityTimer;
  private BeepManager beepManager;


//  private final DialogInterface.OnClickListener aboutListener =
//      new DialogInterface.OnClickListener() {
//    public void onClick(DialogInterface dialogInterface, int i) {
//      Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.zxing_url)));
//      intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
//      startActivity(intent);
//    }
//  };

  ViewfinderView getViewfinderView() {
    return viewfinderView;
  }

  public Handler getHandler() {
    return handler;
  }

  @Override
  public void onCreate(Bundle icicle) {
    super.onCreate(icicle);

    Window window = getWindow();
    window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    setContentView(R.layout.capture);
//    intent_data=getIntent().getSerializableExtra(INTENT_DATA_ID);
    CameraManager.init(getApplication());
    viewfinderView = (ViewfinderView) findViewById(R.id.viewfinder_view);
//    resultView = findViewById(R.id.result_view);
//    statusView = (TextView) findViewById(R.id.status_view);
    handler = null;
//    lastResult = null;
    hasSurface = false;
    inactivityTimer = new InactivityTimer(this);
    beepManager = new BeepManager(this);
  }

  @Override
  protected void onResume() {
    super.onResume();
//    resetStatusView();
    SurfaceView surfaceView = (SurfaceView) findViewById(R.id.preview_view);
    SurfaceHolder surfaceHolder = surfaceView.getHolder();
    if (hasSurface) {
      // The activity was paused but not stopped, so the surface still exists. Therefore
      // surfaceCreated() won't be called, so init the camera here.
      initCamera(surfaceHolder);
    } else {
      // Install the callback and wait for surfaceCreated() to init the camera.
      surfaceHolder.addCallback(this);
      surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

//    Intent intent = getIntent();
//    String action = intent == null ? null : intent.getAction();
//    String dataString = intent == null ? null : intent.getDataString();
    
    /////////////////////////////////////////////////////////////
//    if (intent != null && action != null) {
//      if (action.equals(Intents.Scan.ACTION)) {
//        // Scan the formats the intent requested, and return the result to the calling activity.
//        source = Source.NATIVE_APP_INTENT;
//        decodeFormats = DecodeFormatManager.parseDecodeFormats(intent);
//        if (intent.hasExtra(Intents.Scan.WIDTH) && intent.hasExtra(Intents.Scan.HEIGHT)) {
//          int width = intent.getIntExtra(Intents.Scan.WIDTH, 0);
//          int height = intent.getIntExtra(Intents.Scan.HEIGHT, 0);
//          if (width > 0 && height > 0) {
//            CameraManager.get().setManualFramingRect(width, height);
//          }
//        }       
//      } else {
        // Scan all formats and handle the results ourselves (launched from Home).
//        source = Source.NONE;
//        decodeFormats = null;
//      }
//      characterSet = intent.getStringExtra(Intents.Scan.CHARACTER_SET);
//    } else {
//      source = Source.NONE;
//      decodeFormats = null;
//      characterSet = null;
//    }

    //������������������
    beepManager.updatePrefs();
    inactivityTimer.onResume();
  }

  @Override
  protected void onPause() {
    super.onPause();
    if (handler != null) {
      handler.quitSynchronously();
      handler = null;
    }
    inactivityTimer.onPause();
    CameraManager.get().closeDriver();
  }

  @Override
  protected void onDestroy() {
    inactivityTimer.shutdown();
    super.onDestroy();
  }

//  @Override
//  public boolean onKeyDown(int keyCode, KeyEvent event) {
//    if (keyCode == KeyEvent.KEYCODE_BACK) {
//      if (source == Source.NATIVE_APP_INTENT) {
//        setResult(RESULT_CANCELED);
//        finish();
//        return true;
//      } else if ((source == Source.NONE || source == Source.ZXING_LINK) && lastResult != null) {
//        resetStatusView();
//        if (handler != null) {
//          handler.sendEmptyMessage(R.id.restart_preview);
//        }
//        return true;
//      }
//    } else if (keyCode == KeyEvent.KEYCODE_FOCUS || keyCode == KeyEvent.KEYCODE_CAMERA) {
//      // Handle these events so they don't launch the Camera app
//      return true;
//    }
//    return super.onKeyDown(keyCode, event);
//  }

//  @Override
//  public boolean onCreateOptionsMenu(Menu menu) {
//    super.onCreateOptionsMenu(menu);
//    menu.add(0, SHARE_ID, 0, R.string.menu_share)
//        .setIcon(android.R.drawable.ic_menu_share);
//    menu.add(0, HISTORY_ID, 0, R.string.menu_history)
//        .setIcon(android.R.drawable.ic_menu_recent_history);
//    menu.add(0, SETTINGS_ID, 0, R.string.menu_settings)
//        .setIcon(android.R.drawable.ic_menu_preferences);
//    menu.add(0, HELP_ID, 0, R.string.menu_help)
//        .setIcon(android.R.drawable.ic_menu_help);
//    menu.add(0, ABOUT_ID, 0, R.string.menu_about)
//        .setIcon(android.R.drawable.ic_menu_info_details);
//    return true;
//  }
//
//  // Don't display the share menu item if the result overlay is showing.
//  @Override
//  public boolean onPrepareOptionsMenu(Menu menu) {
//    super.onPrepareOptionsMenu(menu);
//    menu.findItem(SHARE_ID).setVisible(lastResult == null);
//    return true;
//  }

//  @Override
//  public boolean onOptionsItemSelected(MenuItem item) {
//    switch (item.getItemId()) {
//      case SHARE_ID: {
//        Intent intent = new Intent(Intent.ACTION_VIEW);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
//        intent.setClassName(this, ShareActivity.class.getName());
//        startActivity(intent);
//        break;
//      }
//      case HISTORY_ID: {
//        AlertDialog historyAlert = historyManager.buildAlert();
//        historyAlert.show();
//        break;
//      }
//      case SETTINGS_ID: {
//        Intent intent = new Intent(Intent.ACTION_VIEW);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
//        intent.setClassName(this, PreferencesActivity.class.getName());
//        startActivity(intent);
//        break;
//      }
//      case HELP_ID: {
//        Intent intent = new Intent(Intent.ACTION_VIEW);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
//        intent.setClassName(this, HelpActivity.class.getName());
//        startActivity(intent);
//        break;
//      }
//      case ABOUT_ID:
//        AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        builder.setTitle(getString(R.string.title_about) + versionName);
//        builder.setMessage(getString(R.string.msg_about) + "\n\n" + getString(R.string.zxing_url));
//        builder.setIcon(R.drawable.launcher_icon);
//        builder.setPositiveButton(R.string.button_open_browser, aboutListener);
//        builder.setNegativeButton(R.string.button_cancel, null);
//        builder.show();
//        break;
//    }
//    return super.onOptionsItemSelected(item);
//  }
  @Override
  public void surfaceCreated(SurfaceHolder holder) {
    if (!hasSurface) {
      hasSurface = true;
      initCamera(holder);
    }
  }
  @Override
  public void surfaceDestroyed(SurfaceHolder holder) {
    hasSurface = false;
  }
  @Override
  public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

  }

  /**
   * A valid barcode has been found, so give an indication of success and show the results.
   * 
   * @param rawResult The contents of the barcode.
   * @param barcode   A greyscale bitmap of the camera data which was decoded.
   */
  public void handleDecode(Result rawResult) {
	  inactivityTimer.onActivity();
	  beepManager.playBeepSoundAndVibrate();
	  doAfterDecode(rawResult.getText());
  }

  /**
   * ����������ṩ�� ֮��ʵ�ֵ�
   * �ҵ��������Ҫ�Ĳ���
   * @param code
   */
  public void doAfterDecode(String code)
  {
  }

  /**
   * Superimpose a line for 1D or dots for 2D to highlight the key features of the barcode.
   *
   * @param barcode   A bitmap of the captured image.
   * @param rawResult The decoded results which contains the points to draw.
   */
//  private void drawResultPoints(Bitmap barcode, Result rawResult) {
//    ResultPoint[] points = rawResult.getResultPoints();
//    if (points != null && points.length > 0) {
//      Canvas canvas = new Canvas(barcode);
//      Paint paint = new Paint();
//      paint.setColor(getResources().getColor(R.color.result_image_border));
//      paint.setStrokeWidth(3.0f);
//      paint.setStyle(Paint.Style.STROKE);
//      Rect border = new Rect(2, 2, barcode.getWidth() - 2, barcode.getHeight() - 2);
//      canvas.drawRect(border, paint);
//
//      paint.setColor(getResources().getColor(R.color.result_points));
//      if (points.length == 2) {
//        paint.setStrokeWidth(4.0f);
//        drawLine(canvas, paint, points[0], points[1]);
//      } else if (points.length == 4 &&
//                 (rawResult.getBarcodeFormat().equals(BarcodeFormat.UPC_A) ||
//                  rawResult.getBarcodeFormat().equals(BarcodeFormat.EAN_13))) {
//        // Hacky special case -- draw two lines, for the barcode and metadata
//        drawLine(canvas, paint, points[0], points[1]);
//        drawLine(canvas, paint, points[2], points[3]);
//      } else {
//        paint.setStrokeWidth(10.0f);
//        for (ResultPoint point : points) {
//          canvas.drawPoint(point.getX(), point.getY(), paint);
//        }
//      }
//    }
//  }

//  private static void drawLine(Canvas canvas, Paint paint, ResultPoint a, ResultPoint b) {
//    canvas.drawLine(a.getX(), a.getY(), b.getX(), b.getY(), paint);
//  }

//  // Put up our own UI for how to handle the decoded contents.
//  private void handleDecodeInternally(Result rawResult, ResultHandler resultHandler, Bitmap barcode) {
//    statusView.setVisibility(View.GONE);
//    viewfinderView.setVisibility(View.GONE);
//    resultView.setVisibility(View.VISIBLE);
//
//    ImageView barcodeImageView = (ImageView) findViewById(R.id.barcode_image_view);
//    if (barcode == null) {
//      barcodeImageView.setImageBitmap(BitmapFactory.decodeResource(getResources(),
//          R.drawable.launcher_icon));
//    } else {
//      barcodeImageView.setImageBitmap(barcode);
//    }
//
//    TextView formatTextView = (TextView) findViewById(R.id.format_text_view);
//    formatTextView.setText(rawResult.getBarcodeFormat().toString());
//
//    TextView typeTextView = (TextView) findViewById(R.id.type_text_view);
//    typeTextView.setText(resultHandler.getType().toString());
//
//    DateFormat formatter = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT);
//    String formattedTime = formatter.format(new Date(rawResult.getTimestamp()));
//    TextView timeTextView = (TextView) findViewById(R.id.time_text_view);
//    timeTextView.setText(formattedTime);
//
//
//    TextView metaTextView = (TextView) findViewById(R.id.meta_text_view);
//    View metaTextViewLabel = findViewById(R.id.meta_text_view_label);
//    metaTextView.setVisibility(View.GONE);
//    metaTextViewLabel.setVisibility(View.GONE);
//    Map<ResultMetadataType,Object> metadata =
//        (Map<ResultMetadataType,Object>) rawResult.getResultMetadata();
//    if (metadata != null) {
//      StringBuilder metadataText = new StringBuilder(20);
//      for (Map.Entry<ResultMetadataType,Object> entry : metadata.entrySet()) {
//        if (DISPLAYABLE_METADATA_TYPES.contains(entry.getKey())) {
//          metadataText.append(entry.getValue()).append('\n');
//        }
//      }
//      if (metadataText.length() > 0) {
//        metadataText.setLength(metadataText.length() - 1);
//        metaTextView.setText(metadataText);
//        metaTextView.setVisibility(View.VISIBLE);
//        metaTextViewLabel.setVisibility(View.VISIBLE);
//      }
//    }
//
//    TextView contentsTextView = (TextView) findViewById(R.id.contents_text_view);
//    CharSequence displayContents = resultHandler.getDisplayContents();
//    contentsTextView.setText(displayContents);
//    // Crudely scale betweeen 22 and 32 -- bigger font for shorter text
//    int scaledSize = Math.max(22, 32 - displayContents.length() / 4);
//    contentsTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, scaledSize);
//
//    TextView supplementTextView = (TextView) findViewById(R.id.contents_supplement_text_view);
//    supplementTextView.setText("");
//    supplementTextView.setOnClickListener(null);
////    if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(
////        PreferencesActivity.KEY_SUPPLEMENTAL, true)) {
////      SupplementalInfoRetriever.maybeInvokeRetrieval(supplementTextView, resultHandler.getResult(),
////          handler, this);
////    }
//
//    int buttonCount = resultHandler.getButtonCount();
//    ViewGroup buttonView = (ViewGroup) findViewById(R.id.result_button_view);
//    buttonView.requestFocus();
////    for (int x = 0; x < ResultHandler.MAX_BUTTON_COUNT; x++) {
////      TextView button = (TextView) buttonView.getChildAt(x);
////      if (x < buttonCount) {
////        button.setVisibility(View.VISIBLE);
////        button.setText(resultHandler.getButtonText(x));
////        button.setOnClickListener(new ResultButtonListener(resultHandler, x));
////      } else {
////        button.setVisibility(View.GONE);
////      }
////    }
//
////    if (copyToClipboard && !resultHandler.areContentsSecure()) {
////      ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
////      clipboard.setText(displayContents);
////    }
//  }

  // Briefly show the contents of the barcode, then handle the result outside Barcode Scanner.
//  private void handleDecodeExternally(Result rawResult, ResultHandler resultHandler, Bitmap barcode) {
//    viewfinderView.drawResultBitmap(barcode);
//
//    // Since this message will only be shown for a second, just tell the user what kind of
//    // barcode was found (e.g. contact info) rather than the full contents, which they won't
//    // have time to read.
//    statusView.setText(getString(resultHandler.getDisplayTitle()));
//
////    if (copyToClipboard && !resultHandler.areContentsSecure()) {
////      ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
////      clipboard.setText(resultHandler.getDisplayContents());
////    }
//
//    if (source == Source.NATIVE_APP_INTENT) {
//      // Hand back whatever action they requested - this can be changed to Intents.Scan.ACTION when
//      // the deprecated intent is retired.
//      Intent intent = new Intent(getIntent().getAction());
//      intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
//      intent.putExtra(Intents.Scan.RESULT, rawResult.toString());
//      intent.putExtra(Intents.Scan.RESULT_FORMAT, rawResult.getBarcodeFormat().toString());
//      byte[] rawBytes = rawResult.getRawBytes();
//      if (rawBytes != null && rawBytes.length > 0) {
//        intent.putExtra(Intents.Scan.RESULT_BYTES, rawBytes);
//      }
//      Message message = Message.obtain(handler, R.id.return_scan_result);
//      message.obj = intent;
//      handler.sendMessageDelayed(message, INTENT_RESULT_DURATION);
//    } else if (source == Source.PRODUCT_SEARCH_LINK) {
//      // Reformulate the URL which triggered us into a query, so that the request goes to the same
//      // TLD as the scan URL.
//      Message message = Message.obtain(handler, R.id.launch_product_query);
//      int end = sourceUrl.lastIndexOf("/scan");
//      message.obj = sourceUrl.substring(0, end) + "?q=" +
//          resultHandler.getDisplayContents().toString() + "&source=zxing";
//      handler.sendMessageDelayed(message, INTENT_RESULT_DURATION);
//    } else if (source == Source.ZXING_LINK) {
//      // Replace each occurrence of RETURN_CODE_PLACEHOLDER in the returnUrlTemplate
//      // with the scanned code. This allows both queries and REST-style URLs to work.
//      Message message = Message.obtain(handler, R.id.launch_product_query);
//      message.obj = returnUrlTemplate.replace(RETURN_CODE_PLACEHOLDER,
//          resultHandler.getDisplayContents().toString());
//      handler.sendMessageDelayed(message, INTENT_RESULT_DURATION);
//    }
//  }

  /**
   * We want the help screen to be shown automatically the first time a new version of the app is
   * run. The easiest way to do this is to check android:versionCode from the manifest, and compare
   * it to a value stored as a preference.
   */
  

  private void initCamera(SurfaceHolder surfaceHolder) {
    try {
      CameraManager.get().openDriver(surfaceHolder);
      // Creating the handler starts the preview, which can also throw a RuntimeException.
      if (handler == null) {
        handler = new CaptureActivityHandler(this);//, decodeFormats, characterSet);
      }
    } catch (IOException ioe) {
//      Log.w(TAG, ioe);
      displayFrameworkBugMessageAndExit();
    } catch (RuntimeException e) {
      // Barcode Scanner has seen crashes in the wild of this variety:
      // java.?lang.?RuntimeException: Fail to connect to camera service
//      Log.w(TAG, "Unexpected error initializating camera", e);
      displayFrameworkBugMessageAndExit();
    }
  }

  private void displayFrameworkBugMessageAndExit() {
    AlertDialog.Builder builder = new AlertDialog.Builder(this);
    builder.setTitle(getString(R.string.app_name));
    builder.setMessage(getString(R.string.msg_camera_framework_bug));
    builder.setPositiveButton(R.string.button_ok, new FinishListener(this));
    builder.setOnCancelListener(new FinishListener(this));
    builder.show();
  }

//  private void resetStatusView() {
//    resultView.setVisibility(View.GONE);
//    statusView.setText(R.string.msg_default_status);
//    statusView.setVisibility(View.VISIBLE);
//    viewfinderView.setVisibility(View.VISIBLE);
//    lastResult = null;
//  }

//  public void drawViewfinder() {
//    viewfinderView.drawViewfinder();
//  }
}
