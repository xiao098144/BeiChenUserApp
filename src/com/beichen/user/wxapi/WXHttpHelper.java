package com.beichen.user.wxapi;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;

import com.ddoctor.user.config.AppBuildConfig;

/**
 * @filename WXHttpHelper.java
 * @TODO
 * @date 2014-9-15上午10:32:11
 * @Administrator 萧
 * 
 */
public class WXHttpHelper {

	private static WXHttpHelper wxHttpHelper = null;

	private boolean isValid = false;

	public boolean isValid() {
		return isValid;
	}

	public void setValid(boolean isValid) {
		this.isValid = isValid;
	}

	private WXHttpHelper() {

	}

	public static WXHttpHelper getInstance() {
		if (null == wxHttpHelper) {
			wxHttpHelper = new WXHttpHelper();
		}
		return wxHttpHelper;
	}

	public String refreshToken = null;

	public String getAccessToken = null;

	private Handler handler;

	private boolean falg = true;

	public boolean isFalg() {
		return falg;
	}

	public void setFalg(boolean falg) {
		this.falg = falg;
	}

	public void setHandler(Handler handler) {
		this.handler = handler;
	}

	private String setAccessTokenUrl(String Code) {
		return "https://api.weixin.qq.com/sns/oauth2/access_token?appid="
				+ AppBuildConfig.WXAPP_ID + "&secret=" + AppBuildConfig.WXAppSecret + "&code=" + Code
				+ "&grant_type=authorization_code";
	}

	public void getAccessToken(final String Code) {
		setFalg(true);
		new Thread(new Runnable() {

			@Override
			public void run() {
				while (falg) {
					getAccessToken = setAccessTokenUrl(Code);
					String response = getData(getAccessToken);
					JSONObject jsonObject = null;
					Bundle bundle = new Bundle();
					bundle.putString("accessData", response);
					Message msg = handler.obtainMessage();
					try {
						jsonObject = new JSONObject(response);
						if (requestError(jsonObject.optString("errcode"))) {
							refreshToken(jsonObject.optString("refresh_token"));
						} else {
							msg.what = -1;
							msg.obj = "验证失败，请重试 ";
							msg.setData(bundle);
							handler.sendMessage(msg);
						}
					} catch (JSONException e) {
						msg.what = -2;
						msg.obj = "连接出错，请重试 ";
						msg.setData(bundle);
						handler.sendMessage(msg);
					}

				}

			}
		}).start();

	}

	public void refreshToken(final String RefreshToken) {
		setFalg(true);
		new Thread(new Runnable() {

			@Override
			public void run() {
				while (falg) {
					refreshToken = setRefreshToken(RefreshToken);
					String response = getData(refreshToken);
					JSONObject jsonObject = null;
					Bundle bundle = new Bundle();
					bundle.putString("refreshData", response);
					Message msg = handler.obtainMessage();
					try {
						jsonObject = new JSONObject(response);
						if (requestError(jsonObject.optString("errcode"))) {
							msg.what = 0;
							bundle.putString("openid",
									jsonObject.optString("openid"));
							bundle.putString("access_token",
									jsonObject.optString("access_token"));
							msg.setData(bundle);
							handler.sendMessage(msg);
						} else {
							msg.what = -1;
							msg.obj = " 验证失败，请重试 ";
							msg.setData(bundle);
							handler.sendMessage(msg);
						}
					} catch (JSONException e) {
						msg.what = -2;
						msg.obj = " 连接出错，请重试 ";
						msg.setData(bundle);
						handler.sendMessage(msg);
					}
				}

			}
		}).start();

	}

	private String setRefreshToken(String RefreshToken) {
		return "https://api.weixin.qq.com/sns/oauth2/refresh_token?appid="
				+ AppBuildConfig.WXAPP_ID + "&grant_type=refresh_token&refresh_token="
				+ RefreshToken;
	}

	/**
	 * 验证AccessToken 与 Openid 是否过期
	 * 
	 * @return
	 */
	public boolean validAccessToken(final String access_token,
			final String openid) {
		setFalg(true);
		new Thread(new Runnable() {

			@Override
			public void run() {
				final String validAccessTokenUrl = setValidAccessToken(
						access_token, openid);
				String response = getData(validAccessTokenUrl);
				JSONObject jsonObject = null;
				try {
					jsonObject = new JSONObject(response);
					if (0 == jsonObject.optInt("errcode")) {
						isValid = true;
					}
				} catch (JSONException e) {
					isValid = false;
				}
			}
		}).start();

		return true;
	}

	private String setValidAccessToken(String access_token, String openid) {
		return "https://api.weixin.qq.com/sns/auth?access_token="
				+ access_token + "&openid=" + openid;
	}

	private boolean requestError(String responseCode) {
		if (TextUtils.isEmpty(responseCode)) {
			return true;
		} else {
			if ("40001".equals(responseCode)) {
				return false;
			}
			if ("40002".equals(responseCode)) {
				return false;
			}
			if ("40029".equals(responseCode)) {
				return false;
			}
			if ("40030".equals(responseCode)) {
				return false;
			}
		}
		return false;
	}

	private String getData(String url) {
		StringBuffer sBuffer = new StringBuffer();
		try {
			URL u = new URL(url);
			InputStream in = null;
			HttpURLConnection conn = (HttpURLConnection) u.openConnection();
			conn.setDoInput(true);
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Content-Type",
					"application/json; charset=utf-8");
			if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
				byte[] buf = new byte[1024];
				in = conn.getInputStream();
				for (int n; (n = in.read(buf)) != -1;) {
					sBuffer.append(new String(buf, 0, n, "UTF-8"));
				}
			}
			in.close();
			conn.disconnect();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sBuffer.toString();
	}

}
