package com.beichen.user.wxapi;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;

import com.beichen.user.R;
import com.ddoctor.application.MyApplication;
import com.ddoctor.enums.RetError;
import com.ddoctor.user.activity.BaseActivity;
import com.ddoctor.user.activity.MainTabActivity;
import com.ddoctor.user.activity.regist.BindPhoneActivity;
import com.ddoctor.user.config.AppBuildConfig;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.task.QuickLoginTask;
import com.ddoctor.user.task.TaskPostCallBack;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.DialogUtil;
import com.ddoctor.utils.ToastUtil;
import com.tencent.mm.sdk.modelbase.BaseReq;
import com.tencent.mm.sdk.modelbase.BaseResp;
import com.tencent.mm.sdk.modelmsg.SendAuth;
import com.tencent.mm.sdk.modelmsg.SendAuth.Resp;
import com.tencent.mm.sdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.sdk.openapi.WXAPIFactory;

/**
 * @filename WXEntryActivity.java
 * @TODO
 * @date 2014-9-13下午8:29:33
 * @Administrator 萧
 * 
 */
public class WXEntryActivity extends BaseActivity implements IWXAPIEventHandler {

	public final static int WX_QUICKLOGIN = 1;
	public final static int WX_SHARE = 2;
	public static int currentAction = WX_SHARE;

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	private Handler handler = new Handler(new Handler.Callback() {

		@Override
		public boolean handleMessage(Message msg) {
			switch (msg.what) {
			case 0: {
				Bundle data = msg.getData();
				quicklogin(data.getString("openid"), 1);
				WXHttpHelper.getInstance().setFalg(false);
			}
				break;
			case -1: {
//				ToastUtil.showToast((String) msg.obj);
				WXHttpHelper.getInstance().setFalg(false);
			}
				break;
			case -2: {
				ToastUtil.showToast((String) msg.obj);
				WXHttpHelper.getInstance().setFalg(false);
			}
				break;
			case -3: {
				ToastUtil.showToast(getResources().getString(msg.arg1));
			}
				break;
			case -4: {
				ToastUtil.showToast((String) msg.obj);
				finish();
			}
				break;
			}
			return false;
		}
	});

	private Dialog _dialog;

	private void quicklogin(String quickId, int type) {

		_dialog = DialogUtil.createLoadingDialog(this, "正在登录...");
		_dialog.show();

		QuickLoginTask task = new QuickLoginTask(quickId, type);
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {
			@Override
			public void taskFinish(DDResult result) {
				_dialog.dismiss();
				if (result.getError() == RetError.NONE) {
					// 进入主界面
					if (!TextUtils.isEmpty(DataModule.getInstance()
							.getLoginedUserInfo().getMobile())) {
						ToastUtil.showToast("快捷登录成功");
						skip(MainTabActivity.class, true);
					} else {
						skip(BindPhoneActivity.class, true);
					}
				} else {
					// 登录失败
					ToastUtil.showToast(result.getErrorMessage());
				}
			}
		});
		task.executeParallel("");
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		setIntent(intent);
		MyApplication.mwxAPI.handleIntent(intent, this);
	}

	@Override
	public void onReq(BaseReq arg0) {

	}

	@Override
	public void onResp(BaseResp baseResp) {
		Message msg = handler.obtainMessage();
		switch (baseResp.errCode) {
		case BaseResp.ErrCode.ERR_OK: {
			if (currentAction == WX_QUICKLOGIN) {

				SendAuth.Resp resp = (Resp) baseResp;
				WXHttpHelper instance = WXHttpHelper.getInstance();
				instance.setHandler(handler);
				instance.getAccessToken(resp.code);
			} else if (currentAction == WX_SHARE) {
				ToastUtil.showToast(getString(R.string.share_success));
				finish();
			}
		}
			break;

		case BaseResp.ErrCode.ERR_AUTH_DENIED: {
			if (currentAction == WX_QUICKLOGIN) {
				msg.what = -3;
				msg.arg1 = R.string.wx_login_deny;
			} else if (currentAction == WX_SHARE) {
				ToastUtil.showToast(getString(R.string.share_deny));
				finish();
			}
		}
			break;
		case BaseResp.ErrCode.ERR_SENT_FAILED: {
			if (currentAction == WX_QUICKLOGIN) {
				msg.what = -3;
				msg.arg1 = R.string.wx_login_fail;
			} else if (currentAction == WX_SHARE) {
				ToastUtil.showToast(getString(R.string.share_deny));
				finish();
			}
		}
			break;
		case BaseResp.ErrCode.ERR_UNSUPPORT: {
			if (currentAction == WX_QUICKLOGIN) {
				msg.what = -3;
				msg.arg1 = R.string.wx_login_fail;
			} else if (currentAction == WX_SHARE) {
				ToastUtil.showToast(getString(R.string.share_deny));
				finish();
			}

		}
			break;
		case BaseResp.ErrCode.ERR_USER_CANCEL: {
			msg.what = -4;
			if (currentAction == WX_QUICKLOGIN) {
				msg.arg1 = R.string.wx_login_cancel;
			} else {
				ToastUtil.showToast(getString(R.string.share_cancel));
				finish();
			}
		}
			break;
		}

		if (currentAction == WX_QUICKLOGIN && msg.arg1 != 0) {
			handler.sendMessage(msg);
		} else {
			msg = null;
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_wxentriact);
		if (MyApplication.mwxAPI == null) {
			MyApplication.mwxAPI = WXAPIFactory.createWXAPI(this,
					AppBuildConfig.WXAPP_ID, false);
		}
		MyApplication.mwxAPI.handleIntent(getIntent(), this);
	}

}
