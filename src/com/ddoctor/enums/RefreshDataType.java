package com.ddoctor.enums;

/**
 * 
 * @author 萧
 * @Date 2015-6-14下午9:07:48
 * @TODO TODO
 */
public enum RefreshDataType {

	REFRESH_NODATA,  // 下拉刷新无数据    
	LOADMORE_NODATA, // 加载更多无数据
	NORMAL     		 // 加载数据正常 	
	
}
