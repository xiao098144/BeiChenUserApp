package com.ddoctor.enums;

import java.util.HashMap;
import java.util.Map;

import android.os.Bundle;

public enum RetError {
	NONE, 
	ERROR,
	INVALID, 
	DATABASE,
	API_INTERFACE,
	NETWORK_ERROR, 
	
	MISSING_USER_INFO,
	SERVER_ERROR,
	;

	public static Map<String, RetError> str2Error = new HashMap<String, RetError>();
	static {
		for (RetError err : RetError.values()) {
			str2Error.put(err.name(), err);
		}
	}
	public static Map<String, String> s2t = new HashMap<String, String>();
	static {
		s2t.put("NONE", "没有错误");
		s2t.put("ERROR", "未知错误");
		s2t.put("INVALID", "无效的参数");
		s2t.put("DATABASE", "数据库访问错误");
		s2t.put("API_INTERFACE", "数据错误");
		s2t.put("NETWORK_ERROR", "网络错误,请检查网络");
	}

	public static RetError convert(String err) {
		if (!str2Error.containsKey(err)) {
			return RetError.INVALID;
		} else {
			return str2Error.get(err);
		}
	}

	public static String toText(RetError err) {
		if (s2t.containsKey(err.toString())) {
			return s2t.get(err.toString());
		}
		return "未知错误";
	}
	
	public String getErrorMessage()
	{
		if( this.message.length() > 0 )
			return this.message;
		
		return RetError.toText(this);
	}

	String message = "";

	public void setErrorMessage(String message) {
		this.message = message;
	}

//	public String getMessage() {
//		return message;
//	}
	
	Bundle bundle;
	public void setBundle(Bundle bundle) {
		this.bundle = bundle;
	}
	public Bundle getBundle() {
		return bundle;
	}
	
	Object _object = null;
	public Object getObject(){
		return _object;
	}
	
	public void setObject(Object obj){
		_object = obj;
	}
}
