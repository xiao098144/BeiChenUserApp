package com.ddoctor.enums;

public enum RecordLayoutType {

	TYPE_VALUE, TYPE_CATEGORY;

	public static RecordLayoutType valueOf(int value) {
		switch (value) {
		case 1:
			return TYPE_CATEGORY;
		default:
			return TYPE_VALUE;
		}
	}

}
