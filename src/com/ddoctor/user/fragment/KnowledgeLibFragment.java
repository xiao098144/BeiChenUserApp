package com.ddoctor.user.fragment;

import java.util.ArrayList;
import java.util.List;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

import com.beichen.user.R;
import com.ddoctor.enums.RetError;
import com.ddoctor.user.activity.knowledge.KnowledegListActivity;
import com.ddoctor.user.adapter.KnowledgeCatagoryAdapter;
import com.ddoctor.user.task.GetKnowledegCategoryTask;
import com.ddoctor.user.task.TaskPostCallBack;
import com.ddoctor.user.wapi.bean.CatagoryBean;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.DialogUtil;
import com.ddoctor.utils.MyUtils;
import com.ddoctor.utils.ToastUtil;
import com.umeng.analytics.MobclickAgent;

public class KnowledgeLibFragment extends BaseFragment implements OnItemClickListener{

	private GridView _catagory_grid;
	private List<CatagoryBean> _dataList = new ArrayList<CatagoryBean>();
	private KnowledgeCatagoryAdapter _adapter;
	protected int _emptyView;  // 补足个数  满足 为3的倍数
	
	@Override
	public void onResume() {
		super.onResume();
		MyUtils.showLog("KnowledgeLibFragment onResume ");
		MobclickAgent.onPageStart("KnowledgeLibFragment");
	}
	
	@Override
	public void onPause() {
		super.onPause();
		MyUtils.showLog("KnowledgeLibFragment onPause ");
		MobclickAgent.onPageEnd("KnowledgeLibFragment");
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.act_knowledge, null);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		findViewById(view);
		getKnowledgecatagory();
	}

	public void findViewById(View view) {
		setTitle(getString(R.string.knowledgelib_title));
		_catagory_grid = (GridView) view.findViewById(R.id.knowledge_catagory_grid);
		_adapter = new KnowledgeCatagoryAdapter(getActivity());
		_catagory_grid.setAdapter(_adapter);
		_adapter.setData(_dataList);
		_catagory_grid.setOnItemClickListener(this);
		
	}
	
	private Dialog _loadingDialog =null;
	private void getKnowledgecatagory() {
		_loadingDialog = DialogUtil.createLoadingDialog(getActivity());
		_loadingDialog.show();
		GetKnowledegCategoryTask task = new GetKnowledegCategoryTask();
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {

			@Override
			public void taskFinish(DDResult result) {
				_loadingDialog.dismiss();
				if (result.getError() == RetError.NONE) {
					ArrayList<CatagoryBean> list = result.getBundle()
							.getParcelableArrayList("list");
					_dataList.addAll(list);
					_emptyView = 3 - _dataList.size() % 3;
					if (_emptyView > 0 && _emptyView %3 !=0) {
						for (int i = 0; i < _emptyView; i++) {
							CatagoryBean catagoryBean = new CatagoryBean();
							catagoryBean.setId(0);
							catagoryBean
									.setImgUrl(_dataList.get(0).getImgUrl());
							catagoryBean.setName("");
							_dataList.add(catagoryBean);
						}
					}

					_adapter.notifyDataSetChanged();
				} else {
					ToastUtil.showToast(result.getErrorMessage());
				}
			}
		});
		task.executeParallel("");
	}
	
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		int dataIdx = position;
		if (dataIdx >= _dataList.size() - _emptyView) {
			return;
		}

		Intent intent = new Intent();
		intent.setClass(getActivity(), KnowledegListActivity.class);
		intent.putExtra("catagory", _dataList.get(dataIdx).getId());
		startActivity(intent);
	}

}
