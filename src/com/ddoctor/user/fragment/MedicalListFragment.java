package com.ddoctor.user.fragment;

import java.util.ArrayList;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.user.activity.medicine.MedicineTimeActivity;
import com.ddoctor.user.adapter.MedicalListAdapter;
import com.ddoctor.user.wapi.bean.MedicalBean;
import com.ddoctor.user.wapi.bean.MedicalRecordBean;
import com.ddoctor.utils.MyUtils;
import com.ddoctor.utils.StringUtils;
import com.ddoctor.utils.ToastUtil;
import com.umeng.analytics.MobclickAgent;

public class MedicalListFragment extends BaseFragment implements
		OnItemClickListener {

	private String title;
	private int medicalType;
	private int type;

	private ArrayList<MedicalBean> dataList;

	private ListView refreshLV;
	private MedicalListAdapter adapter;

	@Override
	public void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("MedicalListFragment");
	}

	@Override
	public void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("MedicalListFragment");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Bundle bundle = getArguments();
		if (null != bundle && !bundle.isEmpty()) {
			title = bundle.getString("title");
			medicalType = bundle.getInt("medicalType");
			type = bundle.getInt("type", 0);
			dataList = bundle.getParcelableArrayList("data");
		}
		return inflater.inflate(R.layout.layout_fmlib_fragment, null);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		findViewById(view);
	}

	private void findViewById(View view) {
		refreshLV = (ListView) view.findViewById(R.id.fm_lib_frag_lv);
		refreshLV.setDivider(null);
		refreshLV.setDividerHeight(0);
		adapter = new MedicalListAdapter(getActivity());
		refreshLV.setAdapter(adapter);
		adapter.setData(dataList);
		refreshLV.setOnItemClickListener(this);

	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		int dataIndex = arg2 - refreshLV.getHeaderViewsCount();
		if (dataIndex >= dataList.size()) {
			return;
		}
		showInputDialog(dataIndex);
	}

	float count = 1;
	private float _perClick = 0.5f; // 每次点击按钮增量
	private boolean _leftlongClick = false;
	private boolean _rightlongClick = false;

	private void showInputDialog(final int position) {
		final Dialog dialog = new Dialog(getActivity(), R.style.NoTitleDialog);
		View view = LayoutInflater.from(getActivity()).inflate(
				R.layout.layout_medical_dose, null);
		TextView tv_title = (TextView) view
				.findViewById(R.id.medical_dose_dialog_tv_title);
		ImageButton left = (ImageButton) view
				.findViewById(R.id.medical_dose_left);
		ImageButton right = (ImageButton) view
				.findViewById(R.id.medical_dose_right);
		final EditText et_input = (EditText) view
				.findViewById(R.id.medical_dose_et_input);
		Button btn_confirm = (Button) view
				.findViewById(R.id.bottom_btn_confirm);
		Button btn_cancel = (Button) view.findViewById(R.id.bottom_btn_cancel);
		et_input.setText(count + "");
		tv_title.setText(title);

		et_input.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				if (start > 1) {
					float num = Float.parseFloat(s.toString());
					if (num < 1)
						s = String.valueOf(1);
					return;
				}
			}

			@Override
			public void afterTextChanged(Editable s) {
				if (s != null && !s.equals("")) {
					float markVal = count;
					try {
						markVal = Float.parseFloat(s.toString());
					} catch (NumberFormatException e) {
						markVal = count;
					}
					if (markVal < 0.5f) {
						ToastUtil.showToast("服用剂量至少半片");
						et_input.setText("0.5");
					} else if (markVal > 100) {
						et_input.setText("100");
					}
					return;
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}
		});

		OnClickListener onClickListener = new OnClickListener() {

			@Override
			public void onClick(View v) {
				switch (v.getId()) {
				case R.id.bottom_btn_cancel: {
					dialog.dismiss();
				}
					break;
				case R.id.bottom_btn_confirm: {
					String trim = et_input.getText().toString().trim();
					if (TextUtils.isEmpty(trim)) {
						ToastUtil.showToast("请输入目标数值");
					} else if (!StringUtils.pureNum(trim)) {
						ToastUtil.showToast("数据异常，请重新输入");
					} else {
						Intent intent = new Intent();
						Bundle data = new Bundle();
						MedicalRecordBean medicalRecordBean = new MedicalRecordBean();
						MedicalBean medicalBean = dataList.get(position);
						MyUtils.showLog("选择用药  " + medicalBean.toString());
						medicalRecordBean.copyFromMedicalBean(medicalBean);
						if (trim.endsWith(".0")) {
							trim = trim.substring(0, trim.length() - 2);
						}

						medicalRecordBean
								.setCount(trim
										+ ""
										+ (TextUtils.isEmpty(medicalBean
												.getUnit()) ? "" : medicalBean
												.getUnit().substring(
														medicalBean.getUnit()
																.length() - 1)));

						data.putParcelable("data", medicalRecordBean);
						data.putInt("type", type);
						intent.putExtra("data", data);
						if (type == 2) {
							getActivity().setResult(Activity.RESULT_OK, intent);
							getActivity().finish();
							// intent.setClass(getActivity(),
							// MedicineTimeActivity.class);
							// getActivity().startActivityForResult(intent,
							// 300);
						} else {
							if (type == 0) {
								intent.setClass(getActivity(),
										MedicineTimeActivity.class);
								getActivity().startActivity(intent);
							} else if (type == 1) {
								getActivity().setResult(Activity.RESULT_OK,
										intent);
							}
							getActivity().finish();
						}

						dialog.dismiss();
					}
				}
					break;
				case R.id.medical_dose_left: {
					if (count == 0.5f) {
						ToastUtil.showToast("服用剂量至少为半片");
					} else {
						count -= _perClick;
						et_input.setText(String.valueOf(count));
					}
				}
					break;
				case R.id.medical_dose_right: {
					if (count == 100) {

					} else {
						count += _perClick;
						et_input.setText(String.valueOf(count));
					}
				}
					break;

				default:
					break;
				}
			}
		};

		left.setOnClickListener(onClickListener);
		right.setOnClickListener(onClickListener);
		btn_cancel.setOnClickListener(onClickListener);
		btn_confirm.setOnClickListener(onClickListener);

		
		Window window = dialog.getWindow();
		window.setGravity(Gravity.CENTER);
		dialog.setCancelable(true);
		dialog.setCanceledOnTouchOutside(true);
		dialog.setContentView(view, new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.MATCH_PARENT,
				LinearLayout.LayoutParams.MATCH_PARENT));
		dialog.show();
	}
}
