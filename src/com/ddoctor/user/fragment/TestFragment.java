package com.ddoctor.user.fragment;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.utils.MyUtils;

public class TestFragment extends BaseFragment implements OnScrollListener{

	private ListView _listView;
	private LinearLayout _topBarLayout;
	private LinearLayout _topBarLayoutFloat;
	private ArrayList<String> _dataList = new ArrayList<String>();

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		return inflater.inflate(R.layout.fragment_test, null);

//		return super.onCreateView(inflater, container, savedInstanceState);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		showLog("onViewCreated");
		super.onViewCreated(view, savedInstanceState);
		
		
		RelativeLayout rl = (RelativeLayout)view.findViewById(R.id.titleBar);
		if( rl != null ){
			rl.setBackgroundColor(Color.argb(255, 114, 114,255));
		}
		
		this.setTitle("测试");
		
		Button rightButton = this.getRightButton();
		rightButton.setVisibility(View.VISIBLE);
		rightButton.setText("测试");
		
		
		_listView = (ListView)view.findViewById(R.id.listView);
		_topBarLayoutFloat = (LinearLayout)view.findViewById(R.id.testLayout);
		_topBarLayoutFloat.setVisibility(View.INVISIBLE);

		
		LinearLayout imageLayout = new LinearLayout(this.getActivity());
		imageLayout.setBackgroundColor(Color.GREEN);
		
		LinearLayout ll = new LinearLayout(this.getActivity());
		ll.setOrientation(LinearLayout.HORIZONTAL);
		LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(-1, 260);
		imageLayout.addView(ll, llp);
		
		
		_topBarLayout = new LinearLayout(this.getActivity());
		_topBarLayout.setBackgroundColor(Color.BLUE);
		ll = new LinearLayout(this.getActivity());
		ll.setOrientation(LinearLayout.HORIZONTAL);
		llp = new LinearLayout.LayoutParams(-1, 160);
		_topBarLayout.addView(ll, llp);
		
		
		
		
		for(int i = 0; i < 50; i++ )
		{
			String s = String.format("测试数据%d", i + 1);
			_dataList.add(s);
		}
		
		
		MyCustomAdapter adapter = new MyCustomAdapter(this.getActivity());
		_listView.setAdapter(adapter);
		_listView.addHeaderView(imageLayout);
		_listView.addHeaderView(_topBarLayout);
		
		_listView.setOnScrollListener(this);
	}
	
	
	private class MyCustomAdapter extends BaseAdapter
	{
		private LayoutInflater _inflater;

		public MyCustomAdapter(Context context) {
			_inflater = LayoutInflater.from(context);
		}
		
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return _dataList.size();
		}
		
		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}
		
		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
		
			LinearLayout ll;
			if (convertView == null) {
				ll = new LinearLayout(TestFragment.this.getActivity());
				ll.setOrientation(LinearLayout.HORIZONTAL);
				
				TextView tv = new TextView(TestFragment.this.getActivity());
				tv.setTag(100);
				tv.setTextSize(30);
				LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(-1, 150);
				llp.gravity = Gravity.CENTER_VERTICAL;
				ll.addView(tv, llp);
				
				
				convertView = ll;
		
			}
			else
				ll = (LinearLayout)convertView;
			
			
			TextView tv = (TextView)ll.findViewWithTag(100);
			if( tv != null )
			{
				tv.setText(_dataList.get(position));
			}
		
			return ll;
		}
	}


	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		// TODO Auto-generated method stub
		MyUtils.showLog("onScroll:firstVisibleItem=" + firstVisibleItem);
		if( firstVisibleItem >= 1 )
			_topBarLayoutFloat.setVisibility(View.VISIBLE);
		else
			_topBarLayoutFloat.setVisibility(View.INVISIBLE);
		
		
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		// TODO Auto-generated method stub
		MyUtils.showLog("onScrollStateChanged:"+scrollState);
		
	}
}
