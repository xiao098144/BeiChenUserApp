package com.ddoctor.user.fragment;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.PaintDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.PopupWindow;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.application.MyApplication;
import com.ddoctor.enums.RetError;
import com.ddoctor.user.activity.diet.DietActivity2;
import com.ddoctor.user.activity.knowledge.KnowledgeLibActivity;
import com.ddoctor.user.activity.medicine.MedicineActivity;
import com.ddoctor.user.activity.mine.MyRecordsActivity;
import com.ddoctor.user.activity.report.HealthReportActivity;
import com.ddoctor.user.activity.sport.SportActivity;
import com.ddoctor.user.activity.sugar.AddBloodSuagrActivity;
import com.ddoctor.user.activity.sugar.BloodPressureActivity;
import com.ddoctor.user.activity.sugar.DiscomfirtActivity;
import com.ddoctor.user.activity.sugar.DiseaseRecordActivity;
import com.ddoctor.user.activity.sugar.HWActivity;
import com.ddoctor.user.activity.sugar.Hba1cActivity;
import com.ddoctor.user.activity.sugar.HydCfyzActivity;
import com.ddoctor.user.activity.tsl.TSLOrderListActivity;
import com.ddoctor.user.activity.tsl.TSLPrescriptionListActivity;
import com.ddoctor.user.activity.tsl.TSLUserInfoStep1Activity;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.data.MyProfile;
import com.ddoctor.user.model.PicTextBean;
import com.ddoctor.user.task.BloodSugarTask;
import com.ddoctor.user.task.TaskPostCallBack;
import com.ddoctor.user.view.RecordCalendar;
import com.ddoctor.user.view.RecordCalendar.OnCalendarClickListener;
import com.ddoctor.user.view.RecordCalendar.OnCalendarDateChangedListener;
import com.ddoctor.user.wapi.bean.PatientBean;
import com.ddoctor.user.wapi.bean.SugarValueBean;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.DialogUtil;
import com.ddoctor.utils.DialogUtil.ConfirmDialog;
import com.ddoctor.utils.FileUtils;
import com.ddoctor.utils.ImageLoaderUtil;
import com.ddoctor.utils.MyUtils;
import com.ddoctor.utils.StringUtils;
import com.ddoctor.utils.TimeUtil;
import com.ddoctor.utils.ToastUtil;
import com.umeng.analytics.MobclickAgent;

public class BloodSugarFragment extends BaseFragment {

	private RelativeLayout _layout_addbs;
	private View cut;
	private TableLayout _tableLayout;
	private RelativeLayout personalLayout;
	private Button rightButton;
	private LinearLayout layout_report;
	private LinearLayout layout_sport;
	private LinearLayout layout_diet;
	private LinearLayout layout_medical;
	private LinearLayout layout_knowledge;
	private LinearLayout layout_more;
	private String currentDate;

	private String today, startDate, endDate;
	private String MODULE;
	private static final int DATARouting = -7; // 数据加载周期 即每次加载几天的数据

	// 用户信息
	private ImageView _userHeadImageView;
	private TextView _userHeightTextView;
	private TextView _userWeightTextView;
	private TextView _userBMITextView;
	private TextView _userHBA1CTextView;

	private WebView _webView;
	private ImageView _progressBarImageView;
	private TextView _chartTextView;

	private int[] more_colorRes = new int[] { R.color.color_record_hba1c,
			R.color.color_record_bloodpressure, R.color.color_record_medical,
			R.color.color_record_labsheet, R.color.color_record_rx,
			R.color.color_record_weight, R.color.color_record_dis };
	private int[] more_textRes = new int[] { R.string.sugar_more_hba1c,
			R.string.sugar_more_bloodpressure, R.string.sugar_more_medical,
			R.string.sugar_more_labsheet, R.string.sugar_more_rx,
			R.string.sugar_more_weight, R.string.sugar_more_dis };
	private int[] more_imgRes = new int[] { R.drawable.record_hba1c,
			R.drawable.record_bloodpressure, R.drawable.record_medical,
			R.drawable.record_labsheet, R.drawable.record_rx,
			R.drawable.record_weight, R.drawable.record_discomfort };

	private List<PicTextBean> record_list = new ArrayList<PicTextBean>();

	private boolean _tj = false;

	private void initData() {
		Resources res = getResources();
		for (int i = 0; i < more_colorRes.length; i++) {
			PicTextBean picTextBean = new PicTextBean();
			picTextBean.setText(res.getString(more_textRes[i]));
			picTextBean.setTextColor(res.getColor(more_colorRes[i]));
			picTextBean.setImgResId(more_imgRes[i]);
			record_list.add(picTextBean);
		}
		MODULE = res.getString(R.string.time_format_10);
		today = TimeUtil.getInstance().getStandardDate(MODULE);
		endDate = TimeUtil.getInstance().dateAddFrom(1, today, MODULE,
				Calendar.DATE);
		startDate = TimeUtil.getInstance().dateAddFrom(DATARouting, endDate,
				MODULE, Calendar.DATE);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		initData();

		// // // for test
//		 MyProfile.tsl_setAuthStatus(MyProfile.TSL_AUTH_STATUS_AUTH_FAILED);
		// MyProfile.tsl_setAuthMessage("");
		// MyProfile.tsl_setIsAllowUser(1); // for test
		// MyProfile.tsl_setAuthStatus(MyProfile.TSL_AUTH_STATUS_NONE);

		int flag = MyProfile.tsl_getIsAllowUser();
		if (flag != 0)
			_tj = true;
		else
			_tj = false;

		if (_tj)
			return inflater.inflate(R.layout.fragment_tj_home, null);
		else
			return inflater.inflate(R.layout.fragment_home, null);
	}

	@Override
	public void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("BloodSugarFragment");
		updateUserInfo();
		if (DataModule.getInstance().isUpdateSugar()) {
			startGetChartData();
			DataModule.getInstance().setUpdateSugar(false);
		}
	}

	@Override
	public void onPause() {
		super.onPause();
		MyUtils.showLog("BloodSugarFragment onPause ");
		MobclickAgent.onPageEnd("BloodSugarFragment");
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		findView(view);

		initWebView(view);

		startGetChartData();
		// getUpLowValue();
	}

	// private void getUpLowValue(){
	// GetSugarBoundTask task = new GetSugarBoundTask();
	// task.setTaskCallBack(new TaskPostCallBack<RetError>() {
	//
	// @Override
	// public void taskFinish(RetError result) {
	// if (result == RetError.NONE) {
	//
	// } else {
	//
	// }
	// }
	// });
	// task.executeParallel("");
	// }

	private void showChartLoading(boolean show) {
		AnimationDrawable ad = (AnimationDrawable) _progressBarImageView
				.getBackground();
		if (show) {
			ad.start();
			_progressBarImageView.setVisibility(View.VISIBLE);
		} else {
			_progressBarImageView.setVisibility(View.INVISIBLE);
			ad.stop();
		}
	}

	private List<SugarValueBean> dataList = new ArrayList<SugarValueBean>();
	private String loading_error = "加载失败，点击重试";

	// 加载图表的数据
	private void startGetChartData() {

		_webView.setVisibility(View.INVISIBLE);
		_chartTextView.setVisibility(View.INVISIBLE);

		showChartLoading(true);

		BloodSugarTask task = new BloodSugarTask(startDate, endDate, 1, 0);
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {
			@Override
			public void taskFinish(DDResult result) {
				// 登录成功，进入主界面
				if (result.getError() == RetError.NONE) {
					dataList = result.getBundle()
							.getParcelableArrayList("list");
					if (dataList != null && dataList.size() > 0) {
						loadChart();
					} else {
						loadChartFailed(result.getErrorMessage());
					}
				} else {
					ToastUtil.showToast(result.getErrorMessage());
					// 加载失败
					loadChartFailed(loading_error);
				}
			}
		});
		task.executeParallel("");
	}

	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		super.setUserVisibleHint(isVisibleToUser);
		if (isVisibleToUser) {
			MyUtils.showLog("", "重见天日  " + System.currentTimeMillis());
			updateUserInfo();
			startGetChartData();
		}
	}

	private void loadChart() {
		StringBuffer data = new StringBuffer(), categories = new StringBuffer();
		List<String> dateList = new ArrayList<String>();
		for (int i = 0; i < dataList.size(); i++) {
			String date = TimeUtil.getInstance().getFormatDate(
					dataList.get(i).getTime(), "MM/dd");
			dataList.get(i).setDate(date);
			if (!dateList.contains(date)) {
				dateList.add(date);
			}
		}
		Comparator<SugarValueBean> comparator = new Comparator<SugarValueBean>() {

			@Override
			public int compare(SugarValueBean lhs, SugarValueBean rhs) {
				int compare;
				compare = lhs.getValue().compareTo(rhs.getValue());
				if (compare == 0) {
					lhs.getTime().compareTo(rhs.getTime());
				}
				return 0 - compare;
			}
		};
		for (int i = dateList.size() - 1; i >= 0; i--) {
			List<SugarValueBean> list = new ArrayList<SugarValueBean>();
			for (int j = dataList.size() - 1; j >= 0; j--) {
				if (dateList.get(i).equals(dataList.get(j).getDate())) {
					list.add(dataList.get(j));
				}
			}
			Collections.sort(list, comparator);
			data.append(list.get(0).getValue());
			categories.append("'");
			categories.append(dateList.get(i));
			categories.append("'");
			if (i > 0) {
				data.append(",");
				categories.append(",");
			}
		}
		loadChart(data.toString(), categories.toString());
	}

	private void loadChartFailed(String errMsg) {
		_chartTextView.setText(errMsg);
		_chartTextView.setVisibility(View.VISIBLE);
		showChartLoading(false);
	}

	// 加载WebView里的chart
	private void loadChart(String strData, String strCategories) {
		final int screenWidth = MyUtils
				.getScreenWidth(getActivity() == null ? MyApplication
						.getInstance().getApplicationContext() : getActivity());

		String html = FileUtils
				.getFromAssets(getResources(), "home_chart.html");

		html = html.replace("{$AndroidContainerWidth}", "" + screenWidth);// _webView.getWidth());//
		html = html.replace("{$AndroidContainerHeight}",
				"" + _webView.getHeight());
		html = html.replace("{$CategoriesData}", strCategories);

		html = html.replace("{$data}", strData);

		_webView.loadDataWithBaseURL("file:///android_asset/", html,
				"text/html", "utf-8", "");
	}

	private void findView(View view) {

		RelativeLayout rl = (RelativeLayout) view.findViewById(R.id.titleBar);
		if (rl != null) {
			rl.setBackgroundColor(getResources().getColor(
					R.color.default_titlebar));
		}
		currentDate = TimeUtil.getInstance().getStandardDate("MM/dd");
		this.setTitle(currentDate);
		rightButton = this.getRightButton();
		rightButton.setVisibility(View.VISIBLE);
		rightButton.setText(getResources().getString(R.string.date));
		personalLayout = (RelativeLayout) view
				.findViewById(R.id.sugar_personal_info_layout);

		if (_tj) {
			LinearLayout ll = (LinearLayout) view
					.findViewById(R.id.sugar_item_add);
			ll.setOnClickListener(this);

			ll = (LinearLayout) view.findViewById(R.id.sugar_item_prescription);
			ll.setOnClickListener(this);

			ll = (LinearLayout) view.findViewById(R.id.sugar_item_csi);
			ll.setOnClickListener(this);
		} else {
			_layout_addbs = (RelativeLayout) view.findViewById(R.id.center);
			_layout_addbs.setOnClickListener(this);
		}

		cut = view.findViewById(R.id.cut4);
		_tableLayout = (TableLayout) view.findViewById(R.id.tableLayout);
		layout_report = (LinearLayout) view
				.findViewById(R.id.sugar_item_report);
		layout_diet = (LinearLayout) view.findViewById(R.id.sugar_item_diet);
		layout_sport = (LinearLayout) view.findViewById(R.id.sugar_item_sport);
		layout_medical = (LinearLayout) view
				.findViewById(R.id.sugar_item_medical);
		layout_knowledge = (LinearLayout) view
				.findViewById(R.id.sugar_item_knowledge);
		layout_more = (LinearLayout) view.findViewById(R.id.sugar_item_more);

		// 用户信息控件
		_userHeadImageView = (ImageView) view.findViewById(R.id.img_head);
		_userHeightTextView = (TextView) view
				.findViewById(R.id.sugar_info_height);
		_userWeightTextView = (TextView) view
				.findViewById(R.id.sugar_info_weight);
		_userBMITextView = (TextView) view.findViewById(R.id.sugar_info_bmi);
		_userHBA1CTextView = (TextView) view
				.findViewById(R.id.sugar_info_hba1c);

		_webView = (WebView) view.findViewById(R.id.webView);
		_webView.setBackgroundColor(getResources().getColor(
				R.color.default_titlebar));
		_webView.setVisibility(View.INVISIBLE);

		// 设计比例:1080 x 510
		int width = MyUtils.getScreenWidth(this.getActivity());
		int height = 510 * width / 1080;
		FrameLayout.LayoutParams fllp = (FrameLayout.LayoutParams) _webView
				.getLayoutParams();
		fllp.height = height;

		_progressBarImageView = (ImageView) view
				.findViewById(R.id.progressBarImageView);
		_chartTextView = (TextView) view.findViewById(R.id.textView);
		_chartTextView.setText("加载中...");
		_chartTextView.setVisibility(View.INVISIBLE);
		_chartTextView.setTextColor(Color.WHITE);

		setListener();
	}

	@SuppressLint("SetJavaScriptEnabled")
	private void initWebView(View view) {

		_webView.getSettings().setJavaScriptEnabled(true);
		_webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
		_webView.setWebChromeClient(new WebChromeClient());

		_webView.setWebViewClient(new WebViewClient() {
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				view.loadUrl(url);
				return true;
			}

			@Override
			public void onPageFinished(WebView view, String url) {
				_webView.setVisibility(View.VISIBLE);
				showChartLoading(false);
			}
		});

		/*
		 * ViewTreeObserver vto; final View rootView = view; vto =
		 * _webView.getViewTreeObserver(); vto.addOnGlobalLayoutListener(new
		 * OnGlobalLayoutListener(){
		 * 
		 * @Override public void onGlobalLayout() { // TODO Auto-generated
		 * method stub MyUtils.showLog("webView:" + _webView.getHeight());
		 * 
		 * loadChart(); } });
		 */
	}

	// 更新界面上的用户信息
	private void updateUserInfo() {
		PatientBean patientBean = DataModule.getInstance().getLoginedUserInfo();
		if (patientBean != null) {
			// 头像
			ImageLoaderUtil.displayRoundedCorner(patientBean.getImage(),
					_userHeadImageView, 150, R.drawable.chat_default_protrait);

			String na = "--";
			String s = na;

			if (patientBean.getHeight() > 0)
				s = String.format(Locale.CHINESE, "%dcm",
						patientBean.getHeight());
			_userHeightTextView.setText(s);

			s = na;
			if (patientBean.getWeight() > 0)
				s = String.format(Locale.CHINESE, "%.0fkg",
						patientBean.getWeight());
			_userWeightTextView.setText(s);

			s = na;

			float bmi = DataModule.getBMI(patientBean.getHeight(),
					patientBean.getWeight());
			s = String.format(Locale.CHINESE, "%.1f", bmi);
			_userBMITextView.setText(s);

			s = na;
			if (patientBean.getProtein() > 0) {
				s = String.format(Locale.CHINESE, "%.1f%%",
						patientBean.getProtein());
			}
			_userHBA1CTextView.setText(s);
		}
	}

	private void setListener() {

		personalLayout.setOnClickListener(this);
		rightButton.setOnClickListener(this);
		layout_report.setOnClickListener(this);
		layout_diet.setOnClickListener(this);
		layout_sport.setOnClickListener(this);
		layout_medical.setOnClickListener(this);
		layout_knowledge.setOnClickListener(this);
		layout_more.setOnClickListener(this);
		_chartTextView.setOnClickListener(this);
	}

	/**
	 * 处理日历选择结果 msg arg1 月份 arg2 日期
	 */
	private Handler handler = new Handler(new Handler.Callback() {

		@Override
		public boolean handleMessage(Message msg) {
			Bundle data = msg.getData();
			int year = data.getInt("year");
			int month = data.getInt("month");
			int date = data.getInt("date");
			boolean loadingData = data.getBoolean("click", false);
			MyUtils.showLog("", "月份格式化 " + year + " " + month + " " + date);
			currentDate = TimeUtil.getInstance().getFormatDate(date + "",
					"" + month, "MM/dd");
			if (loadingData) {
				endDate = TimeUtil.getInstance().dateAddFrom(
						1,
						year + "-" + StringUtils.formatnum(month, "00") + "-"
								+ StringUtils.formatnum(date, "00"), MODULE,
						Calendar.DATE);
				startDate = TimeUtil.getInstance().dateAddFrom(DATARouting,
						endDate, MODULE, Calendar.DATE);
				startGetChartData();
			}
			BloodSugarFragment.this.setTitle(currentDate);
			return false;
		}
	});

	// private void on_btn_csi()
	// {
	// int status = MyProfile.tsl_getAuthStatus();
	// if( status == MyProfile.TSL_AUTH_STATUS_AUTH_SUCCESS )
	// Intent intent = new Intent(getActivity(),
	// TSLPrescriptionDetailActivity.class);
	// getActivity().startActivity(intent);
	//
	// }

	private void on_btn_tsl(int target) {
		int status = MyProfile.tsl_getAuthStatus();
		if (status == MyProfile.TSL_AUTH_STATUS_NONE) {
			Intent intent = new Intent(getActivity(),
					TSLUserInfoStep1Activity.class);
			getActivity().startActivity(intent);
		} else if (status == MyProfile.TSL_AUTH_STATUS_WAIT_AUTH) {
			ToastUtil.showToast("您的信息正在审核中，请耐心等待！");
		} else if (status == MyProfile.TSL_AUTH_STATUS_AUTH_SUCCESS) {
			if (target == 1) {
				Intent intent = new Intent(getActivity(),
						TSLOrderListActivity.class);
				getActivity().startActivity(intent);
			} else {
				Intent intent = new Intent(getActivity(),
						TSLPrescriptionListActivity.class);
				getActivity().startActivity(intent);
			}
		} else if (status == MyProfile.TSL_AUTH_STATUS_AUTH_FAILED) {
			String msg1 = MyProfile.tsl_getAuthMessage();
			String msg = String.format(Locale.CHINESE,
					"您的信息审核未通过，原因如下：\n\n%s\n\n请修改后重新提交!", msg1);
			// 提交成功
			DialogUtil.confirmDialog(this.getActivity(), msg, "确定", "取消",
					new ConfirmDialog() {

						@Override
						public void onOKClick(Bundle data) {
							Intent intent = new Intent(getActivity(),
									TSLUserInfoStep1Activity.class);
							getActivity().startActivity(intent);
						}

						@Override
						public void onCancleClick() {
						}

					}).show();

		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.sugar_item_prescription: {
			on_btn_tsl(0);
		}
			break;
		case R.id.sugar_item_csi: {
			on_btn_tsl(1);
		}
			break;
		case R.id.sugar_item_add:
		case R.id.center: {
			Intent intent = new Intent();
			intent.setClass(getActivity(), AddBloodSuagrActivity.class);
			intent.putExtra("date", currentDate);
			getActivity().startActivity(intent);
		}
			break;
		case R.id.sugar_personal_info_layout: {
			Intent intent = new Intent();
			intent.setClass(getActivity(), MyRecordsActivity.class);
			getActivity().startActivity(intent);
		}
			break;
		case R.id.btn_right: {
			showCalendar(v, handler);

		}
			break;
		case R.id.sugar_item_report: {
			Intent intent = new Intent(getActivity(),
					HealthReportActivity.class);
			startActivity(intent);
		}
			break;
		case R.id.sugar_item_diet: {
			Intent intent = new Intent();
			// intent.setClass(getActivity(), DietActivity.class);
			intent.setClass(getActivity(), DietActivity2.class);
			getActivity().startActivity(intent);
		}
			break;
		case R.id.sugar_item_sport: {
			Intent intent = new Intent(getActivity(), SportActivity.class);
			startActivity(intent);
		}
			break;
		case R.id.sugar_item_medical: {
			Intent intent = new Intent(getActivity(), MedicineActivity.class);
			startActivity(intent);
		}
			break;
		case R.id.sugar_item_knowledge: {
			// Intent intent = new
			// Intent(getActivity(),KnowledegListActivity.class);
			Intent intent = new Intent(getActivity(),KnowledgeLibActivity.class);
			startActivity(intent);
		}
			break;
		case R.id.sugar_item_more: {
			if (_tj) {
				RelativeLayout rl = (RelativeLayout) this.getActivity()
						.findViewById(R.id.sugar_personal_info_layout);
				showMore(rl);
			} else
				showMore(cut);
		}
			break;
		case R.id.textView: {
			startGetChartData();
		}
			break;
		default:
			break;
		}

	}

	private boolean _isClick = false;

	/**
	 * 显示日历 并返回点击选择的日期 交予 handler 做处理 arg1 月份 arg2 日期
	 * 
	 * @param parent
	 * @param handler
	 */
	public void showCalendar(View parent, final Handler handler) {
		View view = View.inflate(getActivity(), R.layout.layout_calendar, null);
		view.startAnimation(AnimationUtils.loadAnimation(getActivity(),
				R.anim.fade_in));
		view.setFocusable(true);
		view.setFocusableInTouchMode(true);

		TextView tv = (TextView) view.findViewById(R.id.calendar_tips);
		tv.setText("点击选择今天以前任意一天（默认今天），查看当天之前7天以内的血糖曲线。");

		final PopupWindow popupWindow = new PopupWindow(view,
				LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		popupWindow.setBackgroundDrawable(new PaintDrawable());
		// 设置点击窗口外边窗口消失
		popupWindow.setOutsideTouchable(true);
		// 设置此参数获得焦点，否则无法点击
		popupWindow.setFocusable(true);
		popupWindow.showAsDropDown(parent, 0, 10);
		popupWindow.update();
		view.setOnKeyListener(new OnKeyListener() {

			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (keyCode == KeyEvent.KEYCODE_BACK) {
					popupWindow.dismiss();
					return true;
				}
				return false;
			}
		});
		final RecordCalendar calendar = (RecordCalendar) view
				.findViewById(R.id.popupwindow_calendar);
		final int currentMonth = TimeUtil.getInstance().getCurrentMonth();

		// 监听所选中的日期
		calendar.setOnCalendarClickListener(new OnCalendarClickListener() {

			public void onCalendarClick(int row, int col, String dateFormat) {
				MyUtils.showLog("", "点击选择 " + dateFormat);
				String[] split = dateFormat.split("-");
				int year = Integer.valueOf(split[0]);
				if (year == TimeUtil.getInstance().getCurrentYear() - 1900) {
					year += 1900;
				}
				int month = Integer.valueOf(split[1]);
				int date = Integer.valueOf(split[2]);

				if (calendar.getCalendarMonth() - month == 1// 跨年跳转
						|| calendar.getCalendarMonth() - month == -11) {
					calendar.lastMonth();

				} else if (month - calendar.getCalendarMonth() == 1 // 跨年跳转
						|| month - calendar.getCalendarMonth() == -11) {
					if (month <= currentMonth) {
						calendar.nextMonth();
					}
				} else {
					if (TimeUtil.getInstance().afterToday(year, month, date, 0,
							0, null)) {
						// ToastUtil.showToast("无法选择今天之后的日期");
					} else {
						_isClick = true;
						calendar.removeAllBgColor();
						calendar.setCalendarDayBgColor(dateFormat,
								R.drawable.calendar_date_focused);
						Bundle data = new Bundle();
						data.putInt("year", year);
						data.putInt("month", month);
						data.putInt("date", date);
						data.putBoolean("click", _isClick);
						Message msg = handler.obtainMessage();
						msg.setData(data);
						msg.sendToTarget();
						popupWindow.dismiss();
					}
				}
			}
		});

		// 监听当前月份
		calendar.setOnCalendarDateChangedListener(new OnCalendarDateChangedListener() {
			public void onCalendarDateChanged(int year, int month) {
				_isClick = false;
				Bundle data = new Bundle();
				data.putInt("year", year);
				data.putInt("month", month);
				data.putInt("date", TimeUtil.getInstance().getCurrentDay());
				data.putBoolean("click", _isClick);
				Message msg = handler.obtainMessage();
				msg.setData(data);
				msg.sendToTarget();
			}
		});

		popupWindow.setOnDismissListener(new OnDismissListener() {

			@Override
			public void onDismiss() {
				if (!_isClick) {
					Bundle data = new Bundle();
					data.putInt("year", TimeUtil.getInstance().getCurrentYear());
					data.putInt("month", TimeUtil.getInstance()
							.getCurrentMonth());
					data.putInt("date", TimeUtil.getInstance().getCurrentDay());
					data.putBoolean("click", _isClick);
					Message msg = handler.obtainMessage();
					msg.setData(data);
					msg.sendToTarget();
				}
			}
		});

	}

	public void showMore(View parent) {
		View view = View.inflate(getActivity(), R.layout.layout_sugar_more,
				null);
		view.startAnimation(AnimationUtils.loadAnimation(getActivity(),
				R.anim.fade_in));
		view.setFocusable(true);
		view.setFocusableInTouchMode(true);

		int w = _tableLayout.getWidth();
		int h = _tableLayout.getHeight();

		final PopupWindow popupWindow = new PopupWindow(view, w, h);//
		// LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		popupWindow.setBackgroundDrawable(new PaintDrawable());
		// 设置点击窗口外边窗口消失
		popupWindow.setOutsideTouchable(true);
		// 设置此参数获得焦点，否则无法点击
		popupWindow.setFocusable(true);
		popupWindow.showAsDropDown(parent, 0, 0);
		popupWindow.update();
		view.setOnKeyListener(new OnKeyListener() {

			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (keyCode == KeyEvent.KEYCODE_BACK) {
					popupWindow.dismiss();
					return true;
				}
				return false;
			}
		});

		TextView tv_cancel = (TextView) view
				.findViewById(R.id.sugar_more_tv_cancel);
//		LinearLayout layout_box = (LinearLayout) view
//				.findViewById(R.id.sugar_more_item_box);
		LinearLayout layout_hba1c = (LinearLayout) view
				.findViewById(R.id.sugar_more_item_hba1c);
		LinearLayout layout_bloodpressure = (LinearLayout) view
				.findViewById(R.id.sugar_more_item_bloodpressure);
		LinearLayout layout_medical = (LinearLayout) view
				.findViewById(R.id.sugar_more_item_medical);
		LinearLayout layout_labsheet = (LinearLayout) view
				.findViewById(R.id.sugar_more_item_labsheet);
		LinearLayout layout_rx = (LinearLayout) view
				.findViewById(R.id.sugar_more_item_rx);
		LinearLayout layout_weight = (LinearLayout) view
				.findViewById(R.id.sugar_more_item_weight);
		LinearLayout layout_dis = (LinearLayout) view
				.findViewById(R.id.sugar_more_item_discomfort);

		class ClickListener implements OnClickListener {

			@Override
			public void onClick(View v) {
				switch (v.getId()) {
				case R.id.sugar_more_tv_cancel: {
					popupWindow.dismiss();
				}
					break;
//				case R.id.sugar_more_item_box: {
//					Intent intent = new Intent();
//					intent.setClass(getActivity(), MyBoxActivity.class);
//					startActivity(intent);
//					popupWindow.dismiss();
//				}
//					break;
				case R.id.sugar_more_item_hba1c: {
					Intent intent = new Intent();
					intent.setClass(getActivity(), Hba1cActivity.class);
					startActivity(intent);
					popupWindow.dismiss();
				}
					break;
				case R.id.sugar_more_item_bloodpressure: {
					Intent intent = new Intent();
					intent.setClass(getActivity(), BloodPressureActivity.class);
					startActivity(intent);
					popupWindow.dismiss();
				}
					break;
				case R.id.sugar_more_item_medical: {
					Intent intent = new Intent();
					intent.setClass(getActivity(), DiseaseRecordActivity.class);
					startActivity(intent);
					popupWindow.dismiss();
				}
					break;
				case R.id.sugar_more_item_labsheet: {
					Intent intent = new Intent();
					intent.putExtra("type", 1);
					intent.setClass(getActivity(), HydCfyzActivity.class);
					startActivity(intent);
					popupWindow.dismiss();
				}
					break;
				case R.id.sugar_more_item_rx: {
					Intent intent = new Intent();
					intent.putExtra("type", 2);
					intent.setClass(getActivity(), HydCfyzActivity.class);
					startActivity(intent);
					popupWindow.dismiss();
				}
					break;
				case R.id.sugar_more_item_weight: {
					Intent intent = new Intent();
					intent.setClass(getActivity(), HWActivity.class);
					startActivity(intent);
					popupWindow.dismiss();
				}
					break;
				case R.id.sugar_more_item_discomfort: {
					Intent intent = new Intent();
					intent.setClass(getActivity(), DiscomfirtActivity.class);
					startActivity(intent);
					popupWindow.dismiss();
				}
					break;

				default:
					break;
				}
				// popupWindow.dismiss();
			}
		}

		ClickListener clickListener = new ClickListener();

//		layout_box.setOnClickListener(clickListener);
		layout_hba1c.setOnClickListener(clickListener);
		layout_bloodpressure.setOnClickListener(clickListener);
		layout_medical.setOnClickListener(clickListener);
		layout_labsheet.setOnClickListener(clickListener);
		layout_rx.setOnClickListener(clickListener);
		layout_weight.setOnClickListener(clickListener);
		layout_dis.setOnClickListener(clickListener);
		tv_cancel.setOnClickListener(clickListener);

	}

}
