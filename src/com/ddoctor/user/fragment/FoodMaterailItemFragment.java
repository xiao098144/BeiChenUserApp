package com.ddoctor.user.fragment;

import java.util.ArrayList;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.beichen.user.R;
import com.ddoctor.user.adapter.FMLibAdapter;
import com.ddoctor.user.model.FMLibItemBean;
import com.umeng.analytics.MobclickAgent;

public class FoodMaterailItemFragment extends BaseFragment{

	private String title;
	private int type;

	private ArrayList<FMLibItemBean> dataList;
	
	private ListView refreshLV;
	private FMLibAdapter adapter;
	
	@Override
	public void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("FoodMaterialItemFragment");
	}
	
	@Override
	public void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("FoodMaterialItemFragment");
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Bundle bundle = getArguments();
		if (null != bundle && !bundle.isEmpty()) {
			title = bundle.getString("title");
			type = bundle.getInt("type");
			dataList = bundle.getParcelableArrayList("data");
		}
		return inflater.inflate(R.layout.layout_fmlib_fragment, null);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
			super.onViewCreated(view, savedInstanceState);
			findViewById(view);
	}
	
	private void findViewById(View view) {
		refreshLV = (ListView) view.findViewById(R.id.fm_lib_frag_lv);
		adapter = new FMLibAdapter(getActivity());
		refreshLV.setAdapter(adapter);
		adapter.setData(dataList);
	}

}
