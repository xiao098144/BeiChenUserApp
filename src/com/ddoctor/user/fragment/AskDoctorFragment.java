package com.ddoctor.user.fragment;

/**
 * 问医
 * 康
 */
import java.util.ArrayList;
import java.util.List;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.enums.RecordLayoutType;
import com.ddoctor.enums.RefreshAction;
import com.ddoctor.enums.RetError;
import com.ddoctor.user.activity.ask.AskDetailsActivity;
import com.ddoctor.user.activity.ask.DoctorsActivity;
import com.ddoctor.user.activity.ask.MyAskDoctorActivity;
import com.ddoctor.user.activity.ask.adapter.AskDoctorAdapter;
import com.ddoctor.user.config.AppBuildConfig;
import com.ddoctor.user.task.GetPublicQuestionListTask;
import com.ddoctor.user.task.SearchQuesionListTask;
import com.ddoctor.user.task.TaskPostCallBack;
import com.ddoctor.user.view.DDPullToRefreshView;
import com.ddoctor.user.view.DDPullToRefreshView.OnHeaderRefreshListener;
import com.ddoctor.user.wapi.bean.QuesionBean;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.DialogUtil;
import com.ddoctor.utils.MyUtils;
import com.ddoctor.utils.StringUtils;
import com.ddoctor.utils.TimeUtil;
import com.ddoctor.utils.ToastUtil;
import com.umeng.analytics.MobclickAgent;

public class AskDoctorFragment extends BaseFragment implements
		OnHeaderRefreshListener, OnScrollListener, OnItemClickListener,
		TextWatcher {

	ImageButton ask_my_question, ask_doctor_list;
	EditText ask_et_search;
	ImageButton ask_ibtn_search;

	private ListView _listView;
	DDPullToRefreshView _refreshViewContainer;
	private View _getMoreView;
	private TextView _tv_norecord;

	private int _pageNum = 1;
	private RefreshAction _refreshAction = RefreshAction.PULLTOREFRESH;

	private AskDoctorAdapter _adapter;
	private List<QuesionBean> _dataList = new ArrayList<QuesionBean>();

	private List<QuesionBean> _resulList = new ArrayList<QuesionBean>();

	private int _searchpageNum = 1;

	private boolean isSearch = false;

	private String keyword;

	@Override
	public void onResume() {
		super.onResume();
		MyUtils.showLog("AskDoctorFragment onResume");
		MobclickAgent.onPageStart("AskDoctorFragment");
	}

	@Override
	public void onPause() {
		super.onPause();
		MyUtils.showLog("AskDoctorFragment onPause");
		MobclickAgent.onPageEnd("AskDoctorFragment");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_ask_doctor, null);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		findViewById(view);
		loadingData(true, _pageNum);
	}

	protected List<QuesionBean> formatList(List<QuesionBean> list) {
		List<QuesionBean> resultList = new ArrayList<QuesionBean>();
		for (int i = 0; i < list.size(); i++) {
			QuesionBean quesionBean = new QuesionBean();
			list.get(i).setDate(
					TimeUtil.getInstance().formatDate2(list.get(i).getTime()));
			if (i == 0
					|| (i >= 1 && !list.get(i).getDate()
							.equals(list.get(i - 1).getDate()))) {
				quesionBean.setDate(list.get(i).getDate());
				quesionBean.setLayoutType(RecordLayoutType.TYPE_CATEGORY);
				resultList.add(quesionBean);
			}
			resultList.add(list.get(i));
		}
		return resultList;
	}

	private void findViewById(View view) {
		this.setTitle("问医");

		ask_my_question = (ImageButton) view.findViewById(R.id.ask_my_question);
		ask_doctor_list = (ImageButton) view.findViewById(R.id.ask_doctor_list);
		ask_et_search = (EditText) view.findViewById(R.id.ask_et_search);
		ask_et_search.setHint(StringUtils.fromatETHint(
				getString(R.string.et_hint_knowledgelib), 13));
		ask_ibtn_search = (ImageButton) view.findViewById(R.id.ask_ibtn_search);
		ask_et_search.addTextChangedListener(this);
		ask_my_question.setOnClickListener(this);
		ask_doctor_list.setOnClickListener(this);
		ask_ibtn_search.setOnClickListener(this);

		_refreshViewContainer = (DDPullToRefreshView) view
				.findViewById(R.id.refreshViewContainer);
		_refreshViewContainer.setOnHeaderRefreshListener(this);
		_refreshViewContainer.setVisibility(View.INVISIBLE);

		_listView = (ListView) view.findViewById(R.id.listView);
		_listView.setOnItemClickListener(this);
		_listView.setOnScrollListener(this);

		_tv_norecord = (TextView) view.findViewById(R.id.tv_norecord);
		_tv_norecord.setOnClickListener(this);
		_listView.setEmptyView(_tv_norecord);
		initList();

	}

	private void initList() {
		// 获取更多
		_getMoreView = createGetMoreView();
		setGetMoreContent("已全部加载", false, false);
		_listView.addFooterView(_getMoreView);

		// 数据
		_adapter = new AskDoctorAdapter(getActivity());
		_listView.setAdapter(_adapter);
		_adapter.setData(_dataList);
	}

	// 加载更多相关函数 >>>>>>
	boolean _bGetMoreEnable = false;
	boolean _searchbGetMoreEnable = false;

	private View createGetMoreView() {
		if (_getMoreView != null)
			return _getMoreView;

		View v = (View) getActivity().getLayoutInflater().inflate(
				R.layout.refresh_footer, null);

		return v;
	}

	private void setGetMoreContent(String message, boolean showImage,
			boolean animation) {
		TextView tv = (TextView) _getMoreView
				.findViewById(R.id.pull_to_load_text);
		tv.setText(message);

		ImageView imgView = (ImageView) _getMoreView
				.findViewById(R.id.pull_to_load_image);
		AnimationDrawable ad = (AnimationDrawable) imgView.getBackground();
		if (showImage) {
			if (animation) {
				ad.start();
			} else {
				ad.stop();
				ad.selectDrawable(0);
			}

			imgView.setVisibility(View.VISIBLE);
		} else {
			ad.stop();
			ad.selectDrawable(0);

			imgView.setVisibility(View.GONE);
		}
	}

	// <<<<< 获取更多相关函数

	private Dialog _loadingDialog = null;

	private void loadingData(boolean showLoading, int page) {
		if (showLoading) {
			_loadingDialog = DialogUtil.createLoadingDialog(getActivity(),
					"加载中...");
			_loadingDialog.show();
		}
		isSearch = false;
		GetPublicQuestionListTask task = new GetPublicQuestionListTask(page);
		final int page1 = page;
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {

			@Override
			public void taskFinish(DDResult result) {
				if (result.getError() == RetError.NONE) {
					List<QuesionBean> tmpList = result.getBundle()
							.getParcelableArrayList("list");
					if (page1 > 1) // 加载更多
					{
						_resulList.addAll(tmpList);
						_dataList.clear();
						_dataList.addAll(formatList(_resulList));
						_adapter.notifyDataSetChanged();
					} else {
						// 加载第一页
						_dataList.clear();
						_resulList.clear();
						_resulList.addAll(tmpList);
						_dataList.addAll(formatList(_resulList));
						_adapter.notifyDataSetChanged();

						_refreshViewContainer.onHeaderRefreshComplete();
						_refreshViewContainer.setVisibility(View.VISIBLE);
						_refreshAction = RefreshAction.NONE;

						if (_loadingDialog != null)
							_loadingDialog.dismiss();

					}

					// 是否显示"加载更多"
					if (tmpList.size() > 0) {
						setGetMoreContent("滑动加载更多", true, false);
						_bGetMoreEnable = true;
					} else {
						if (page1 == 1) {
							_tv_norecord.setText(result.getErrorMessage());
							_tv_norecord.setTag(0);
						}
						// 没数据了，加载完成
						setGetMoreContent("已全部加载", false, false);
						_bGetMoreEnable = false;
					}

					// 确保加载成功后，再修改这个变量
					_pageNum = page1;
				} else {// 加载失败
					if (page1 > 1) {
						setGetMoreContent("滑动加载更多", true, false);
					} else {
						_refreshViewContainer.onHeaderRefreshComplete();
						_refreshAction = RefreshAction.NONE;

						if (_loadingDialog != null)
							_loadingDialog.dismiss();
						// _tv_norecord.setText("加载失败，点击重试");
						// _tv_norecord.setTag(1);
						// MyUtils.showLog("加载失败   显示页面提示  "+_tv_norecord.getText().toString()+" "+_tv_norecord.getTag());
					}
					ToastUtil.showToast(result.getErrorMessage());
				}

				_refreshAction = RefreshAction.NONE;
			}
		});

		task.executeParallel("");
	}

	private void searchQuestion(boolean showLoading, String keyword, int page) {
		if (showLoading) {
			_loadingDialog = DialogUtil.createLoadingDialog(getActivity(),
					"加载中...");
			_loadingDialog.show();
		}
		isSearch = true;
		SearchQuesionListTask task = new SearchQuesionListTask(keyword, page);
		final int page1 = page;
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {

			@Override
			public void taskFinish(DDResult result) {
				if (result.getError() == RetError.NONE) {
					List<QuesionBean> tmpList = result.getBundle()
							.getParcelableArrayList("list");
					if (page1 > 1) // 加载更多
					{
						_dataList.addAll(tmpList);
						_adapter.notifyDataSetChanged();
					} else {
						// 加载第一页
						_dataList.clear();
						_dataList.addAll(tmpList);
						_adapter.notifyDataSetChanged();

						_refreshViewContainer.onHeaderRefreshComplete();
						_refreshViewContainer.setVisibility(View.VISIBLE);
						_refreshAction = RefreshAction.NONE;

						if (_loadingDialog != null)
							_loadingDialog.dismiss();

					}

					// 是否显示"加载更多"
					if (tmpList.size() > 0) {
						setGetMoreContent("滑动加载更多", true, false);
						_bGetMoreEnable = true;
					} else {
						if (page1 == 1) {
							_tv_norecord.setText(result.getErrorMessage());
							_tv_norecord.setTag(0);
						}
						// 没数据了，加载完成
						setGetMoreContent("已全部加载", false, false);
						_bGetMoreEnable = false;
					}

					// 确保加载成功后，再修改这个变量
					_searchpageNum = page1;
				} else {// 加载失败
					if (page1 > 1) {
						setGetMoreContent("滑动加载更多", true, false);
					} else {
						// _tv_norecord.setText(result.getErrorMessage());
						// _tv_norecord.setTag(1);
						_refreshViewContainer.onHeaderRefreshComplete();
						_refreshAction = RefreshAction.NONE;

						if (_loadingDialog != null)
							_loadingDialog.dismiss();
					}

					ToastUtil.showToast(result.getErrorMessage());
				}

				_refreshAction = RefreshAction.NONE;

			}
		});
		task.executeParallel("");
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.ask_my_question: {
			Intent intent = new Intent(getActivity(), MyAskDoctorActivity.class);
			startActivity(intent);
		}
			break;
		case R.id.ask_doctor_list: {
			Intent intent = new Intent(getActivity(), DoctorsActivity.class);
			startActivity(intent);
		}
			break;
		case R.id.ask_ibtn_search: {
			keyword = ask_et_search.getText().toString().trim();
			if (TextUtils.isEmpty(keyword)) {
				isSearch = false;
				// ToastUtil.showToast("搜索关键字不能为空");
			} else {
				searchQuestion(true, keyword, _searchpageNum);
			}
		}
			break;
		case R.id.tv_norecord: {
			Integer tag = (Integer) _tv_norecord.getTag();
			if (tag != null && tag == 1) {
				if (isSearch) {
					ToastUtil.showToast("搜索加载失败，点击重新搜索");
				} else {
					ToastUtil.showToast("公共问答加载失败，点击重试");
				}
			}
		}
			break;
		default:
			break;
		}

	}

	@Override
	public void onScroll(AbsListView arg0, int firstVisibleItem, int arg2,
			int arg3) {
		if (_refreshAction == RefreshAction.NONE) {
			if (_bGetMoreEnable) {// 有加载更多
				int lastPos = _listView.getLastVisiblePosition();
				int total = _listView.getHeaderViewsCount() + _dataList.size()
						+ _listView.getFooterViewsCount();
				if (lastPos == total - 1) {
					// 加载更多显示出来了，开始加载
					_refreshAction = RefreshAction.LOADMORE;
					setGetMoreContent("正在加载...", true, true);
					if (isSearch) {
						searchQuestion(false, keyword, _searchpageNum + 1);
					} else {
						loadingData(false, _pageNum + 1);
					}
				}
			}
		}

	}

	@Override
	public void onScrollStateChanged(AbsListView arg0, int arg1) {

	}

	@Override
	public void onHeaderRefresh(DDPullToRefreshView view) {
		if (_refreshAction == RefreshAction.NONE) {
			_refreshAction = RefreshAction.PULLTOREFRESH;
			if (isSearch) {
				searchQuestion(false, keyword, 1);
			} else {
				loadingData(false, 1);
			}
		} else {
			// 正在加载，什么也不做
			view.onHeaderRefreshComplete();
		}

	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

		int dataIdx = arg2 - _listView.getHeaderViewsCount();
		if (dataIdx >= _listView.getCount()) {
			return;
		}
		QuesionBean qb = (QuesionBean) _listView.getItemAtPosition(dataIdx);
		// QuesionBean qb = _dataList.get(dataIdx);
		// MyUtils.showLog("AskDoctorFragment listview.getitem " + qb.toString()
		// + " datalist.getitem " + _dataList.get(dataIdx).toString());
		Bundle b = new Bundle();
		b.putParcelable("question", qb);
		Intent intent = new Intent(getActivity(), AskDetailsActivity.class);
		if (qb != null && qb.getImage() != null) {
			intent.putExtra("image", qb.getImage());
		}
		intent.putExtra(AppBuildConfig.BUNDLEKEY, b);
		startActivity(intent);

	}

	@Override
	public void afterTextChanged(Editable s) {
		// TODO Auto-generated method stub

	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		if (s == null || TextUtils.isEmpty(s.toString().trim())) {
			_searchpageNum = 1;
			if (_resulList.size() > 0) {
				_dataList.clear();
				_dataList.addAll(formatList(_resulList));
				_adapter.notifyDataSetChanged();
			} else {
				loadingData(false, 1);
			}
			isSearch = false;
		}
	}

}
