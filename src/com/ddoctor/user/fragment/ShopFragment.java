package com.ddoctor.user.fragment;

/**
 * 商城
 * 康
 */
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.enums.RefreshAction;
import com.ddoctor.enums.RetError;
import com.ddoctor.user.activity.mine.MyCouponActivity;
import com.ddoctor.user.activity.mine.MyIntegralActivity;
import com.ddoctor.user.activity.shop.CartActivity;
import com.ddoctor.user.activity.shop.GoodsDetailActivity;
import com.ddoctor.user.activity.shop.OrderListActivity;
import com.ddoctor.user.activity.shop.adapter.ShopListAdapter;
import com.ddoctor.user.activity.shop.adapter.ShopPageAdapter;
import com.ddoctor.user.task.GetGoodsListTask;
import com.ddoctor.user.task.GetIntegralBalanceTask;
import com.ddoctor.user.task.TaskPostCallBack;
import com.ddoctor.user.view.DDPullToRefreshView;
import com.ddoctor.user.view.DDPullToRefreshView.OnHeaderRefreshListener;
import com.ddoctor.user.wapi.bean.ProductBean;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.DialogUtil;
import com.ddoctor.utils.MyUtils;
import com.ddoctor.utils.ToastUtil;
import com.umeng.analytics.MobclickAgent;

public class ShopFragment extends BaseFragment implements
		OnHeaderRefreshListener, OnScrollListener, OnItemClickListener {
	RelativeLayout rl;
	/**
	 * ViewPager
	 */
	private ViewPager _viewPager;

	// 装点点的ImageView数组
	private ImageView[] tips;

	// 装ImageView数组
	private ImageView[][] mImageViews;

	private RelativeLayout rel_view;

	private ListView _listView;
	private LinearLayout _topBarLayoutFloat;
	DDPullToRefreshView _refreshViewContainer;

	private TextView _scoreTextView;
	private TextView _scoreTextView1;
	private int _score = -1;

	private View _getMoreView;

	// 图片资源id
	private int[] imgIdArray;

	private static final int MSG_CHANGE_PHOTO = 1;

	/** 图片自动切换时间 */
	private static final int PHOTO_CHANGE_TIME = 5000;

	private List<ProductBean> _dataList = new ArrayList<ProductBean>();

	/**
	 * 商品列表适配器
	 */
	private ShopListAdapter _adapter;

	private int _pageNum = 1;
	private RefreshAction _refreshAction = RefreshAction.PULLTOREFRESH;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_shop, null);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		init(view);
		initList(view);

		setScore(_score);

		loadingData(true, _pageNum);
	}

	private void init(View view) {
		// this.setTitle(getActivity().getResources().getString(R.string.shop_good_price));
		this.setTitle("商城");

		rl = (RelativeLayout) view.findViewById(R.id.titleBar);
		if (rl != null)
			rl.setBackgroundColor(getResources().getColor(
					R.color.default_titlebar));

		rel_view = (RelativeLayout) view.findViewById(R.id.rel_view);

		_topBarLayoutFloat = (LinearLayout) view
				.findViewById(R.id.layout_shop_window);
		showSuspend(_topBarLayoutFloat);
		_scoreTextView1 = (TextView) _topBarLayoutFloat
				.findViewById(R.id.scoreTextView);

		_refreshViewContainer = (DDPullToRefreshView) view
				.findViewById(R.id.refreshViewContainer);
		_refreshViewContainer.setOnHeaderRefreshListener(this);
		_refreshViewContainer.setVisibility(View.INVISIBLE);
	}

	@Override
	public void onPause() {
		super.onPause();
		MyUtils.showLog("ShopFragment onPause");
		MobclickAgent.onPageEnd("ShopFragment");
	}

	@Override
	public void onResume() {
		super.onResume();
		MyUtils.showLog("ShopFragment onResume");
		MobclickAgent.onPageStart("ShopFragment");
		if (_score == -1)
			getMyScoreFromServer();
	}

	private void setScore(int score) {
		String s = String.format(Locale.CHINESE, "%d", score);
		if (score < 0)
			s = "--";
		_scoreTextView.setText(s);
		_scoreTextView1.setText(s);
	}

	private void getMyScoreFromServer() {

		GetIntegralBalanceTask task = new GetIntegralBalanceTask();
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {

			@Override
			public void taskFinish(DDResult result) {
				if (result.getError() == RetError.NONE) {
					Bundle b = result.getBundle();
					_score = b.getInt("point", -1);
					setScore(_score);
				} else {
					ToastUtil.showToast(result.getErrorMessage());
				}
			}
		});
		task.executeParallel("");

	}

	private void showReloadLayout(boolean show) {
		RelativeLayout rl = (RelativeLayout) this.getActivity().findViewById(
				R.id.reloadLayout);
		if (show) {
			rl.setVisibility(View.VISIBLE);
			rl.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// 重新加载
					loadingData(true, 1);
				}

			});
		} else
			rl.setVisibility(View.GONE);
	}

	private void showContentUI(boolean show) {
		if (show) {
			_refreshViewContainer.setVisibility(View.VISIBLE);
		} else {
			_refreshViewContainer.setVisibility(View.INVISIBLE);
		}

	}

	private String loadingStr;

	private void initList(View view) {
		// 标题
		loadingStr = getActivity().getResources().getString(
				R.string.basic_loading);

		// listView
		_listView = (ListView) view.findViewById(R.id.listView);

		// listView顶部的滚动图片
		// 初始化滚动图片
		RelativeLayout scrollImageLayout = createScrollImageLayout();

		// 滚动图下面的工具栏
		LinearLayout layout_title = (LinearLayout) getActivity()
				.getLayoutInflater().inflate(R.layout.layout_shop_window, null);
		showSuspend(layout_title);

		// 滚动图和工具栏做为header加到listView中
		_listView.addHeaderView(scrollImageLayout);
		_listView.addHeaderView(layout_title);

		_scoreTextView = (TextView) layout_title
				.findViewById(R.id.scoreTextView);

		// 获取更多
		_getMoreView = createGetMoreView();
		setGetMoreContent("已全部加载", false, false);
		_listView.addFooterView(_getMoreView);

		// 数据
		_adapter = new ShopListAdapter(getActivity());
		_listView.setAdapter(_adapter);
		_listView.setOnItemClickListener(this);
		_adapter.setData(_dataList);
		_listView.setOnScrollListener(this);
	}

	private RelativeLayout createScrollImageLayout() {
		RelativeLayout rootLayout = (RelativeLayout) getActivity()
				.getLayoutInflater().inflate(R.layout.layout_shop_viewpager,
						null);

		_viewPager = (ViewPager) rootLayout.findViewById(R.id.viewPager);
		int width = MyUtils.getScreenWidth(this.getActivity());
		int height = width * 400 / 1080; // 按设计比例计算高度

		RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) _viewPager
				.getLayoutParams();
		rlp.height = height;
		_viewPager.setLayoutParams(rlp);

		_viewPager.setBackgroundColor(Color.WHITE);

		initViewPager(rootLayout);

		return rootLayout;

	}

	// 加载更多相关函数 >>>>>>
	boolean _bGetMoreEnable = false;

	private View createGetMoreView() {
		if (_getMoreView != null)
			return _getMoreView;

		View v = (View) getActivity().getLayoutInflater().inflate(
				R.layout.refresh_footer, null);

		return v;
	}

	private void setGetMoreContent(String message, boolean showImage,
			boolean animation) {
		TextView tv = (TextView) _getMoreView
				.findViewById(R.id.pull_to_load_text);
		tv.setText(message);

		ImageView imgView = (ImageView) _getMoreView
				.findViewById(R.id.pull_to_load_image);
		AnimationDrawable ad = (AnimationDrawable) imgView.getBackground();
		if (showImage) {
			if (animation) {
				ad.start();
			} else {
				ad.stop();
				ad.selectDrawable(0);
			}

			imgView.setVisibility(View.VISIBLE);
		} else {
			ad.stop();
			ad.selectDrawable(0);

			imgView.setVisibility(View.GONE);
		}
	}

	// <<<<< 获取更多相关函数

	private Dialog _loadingDialog = null;

	private void loadingData(boolean showLoading, int page) {
		if (showLoading) {
			_loadingDialog = DialogUtil.createLoadingDialog(getActivity(),
					loadingStr);
			_loadingDialog.show();
		}

		GetGoodsListTask task = new GetGoodsListTask(page);
		// if( !showLoading )
		// task.wsec = 5000;
		// task.wsec = 0;

		final int page1 = page;
		task.setTaskCallBack(new TaskPostCallBack<RetError>() {

			@Override
			public void taskFinish(RetError result) {
				if (result == RetError.NONE) {
					if (result.getBundle() == null
							|| result.getBundle()
									.getParcelableArrayList("list") == null)
						result = RetError.ERROR;
				}

				if (result == RetError.NONE) {
					List<ProductBean> tmpList = result.getBundle()
							.getParcelableArrayList("list");
					if (page1 > 1) // 加载更多
					{
						_dataList.addAll(tmpList);
						_adapter.notifyDataSetChanged();
					} else {
						// 加载第一页
						_dataList.clear();
						_dataList.addAll(tmpList);
						_adapter.notifyDataSetChanged();

						_refreshViewContainer.onHeaderRefreshComplete();
						_refreshViewContainer.setVisibility(View.VISIBLE);
						_refreshAction = RefreshAction.NONE;

						showReloadLayout(false);

						if (_loadingDialog != null)
							_loadingDialog.dismiss();

					}

					// 是否显示"加载更多"
					if (tmpList.size() > 0) {
						setGetMoreContent("滑动加载更多", true, false);
						_bGetMoreEnable = true;
					} else {
						// 没数据了，加载完成
						setGetMoreContent("已全部加载", false, false);
						_bGetMoreEnable = false;
					}

					// 确保加载成功后，再修改这个变量
					_pageNum = page1;
				} else {// 加载失败
					if (page1 > 1) {
						setGetMoreContent("滑动加载更多", true, false);
					} else {
						_refreshViewContainer.onHeaderRefreshComplete();
						_refreshAction = RefreshAction.NONE;

						if (_dataList.size() == 0) {
							showReloadLayout(true);
						}

						if (_loadingDialog != null)
							_loadingDialog.dismiss();
					}

					ToastUtil.showToast(result.getErrorMessage());
				}

				_refreshAction = RefreshAction.NONE;
			}
		});

		task.executeParallel("");
	}

	private Handler mHandler = new Handler() {
		@Override
		public void dispatchMessage(Message msg) {
			switch (msg.what) {
			case MSG_CHANGE_PHOTO:
				int index = _viewPager.getCurrentItem();
				_viewPager.setCurrentItem(index + 1);
				mHandler.sendEmptyMessageDelayed(MSG_CHANGE_PHOTO,
						PHOTO_CHANGE_TIME);
				break;
			}
			super.dispatchMessage(msg);
		}

	};

	/*
	 * private Handler handler = new Handler(new Handler.Callback() {
	 * 
	 * @Override public boolean handleMessage(Message msg) { switch
	 * (refreshAction) { case PULLTOREFRESH: { if (null != msg.getData()) {
	 * dataList.clear(); tmpList = msg.getData().getParcelableArrayList("list");
	 * if (null != tmpList && tmpList.size() > 0) { dataList.addAll(tmpList);
	 * adapter.notifyDataSetChanged(); } else { if (null != loadingDialog &&
	 * loadingDialog.isShowing()) { loadingDialog.dismiss(); } canLoadMore =
	 * false; } } ptrf_view.onHeaderRefreshComplete(); } break; case LOADMORE: {
	 * if (null != msg.getData()) { tmpList =
	 * msg.getData().getParcelableArrayList("list"); if (null != tmpList) {
	 * dataList.addAll(tmpList); adapter.notifyDataSetChanged(); } else { if
	 * (null != loadingDialog && loadingDialog.isShowing()) {
	 * loadingDialog.dismiss(); } canLoadMore = false;
	 * ToastUtil.showToast("没有更多了"); } } ptrf_view.onFooterRefreshComplete(); }
	 * break; default: break; } return false; } });
	 */

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.btn_shop_cart: { // 购物车
			Intent intent = new Intent(getActivity(), CartActivity.class);
			startActivity(intent);
		}
			break;
		case R.id.btn_shop_score: { // 积分
			Intent intent = new Intent(getActivity(), MyIntegralActivity.class);
			startActivity(intent);
		}
			break;

		case R.id.btn_shop_coupon: { // 优惠劵
			Intent intent = new Intent(getActivity(), MyCouponActivity.class);
			startActivity(intent);
		}
			break;
		case R.id.btn_shop_order: { // 订单
			Intent intent = new Intent(getActivity(), OrderListActivity.class);
			startActivity(intent);
		}
			break;
		default:
			break;
		}

	}

	// 初始化滚动图片
	private void initViewPager(View view) {
		ViewGroup pageDotGroup = (ViewGroup) view.findViewById(R.id.viewGroup);
		_viewPager = (ViewPager) view.findViewById(R.id.viewPager);

		// 载入图片资源ID, 先写死，这个应该从接口中获取
		imgIdArray = new int[] { R.drawable.shop_ad1, R.drawable.shop_ad2,
				R.drawable.shop_ad3, };

		// 将点点加入到ViewGroup中
		tips = new ImageView[imgIdArray.length];

		// 1张图片时不显示页数
		if (imgIdArray.length <= 1)
			pageDotGroup.setVisibility(View.GONE);

		for (int i = 0; i < tips.length; i++) {
			ImageView imageView = new ImageView(getActivity());
			LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
					LinearLayout.LayoutParams.WRAP_CONTENT,
					LinearLayout.LayoutParams.WRAP_CONTENT);
			lp.setMargins(4, 4, 4, 4);
			imageView.setLayoutParams(lp);

			tips[i] = imageView;
			if (i == 0) {
				tips[i].setBackgroundResource(R.drawable.medicine_tab_true);
			} else {
				tips[i].setBackgroundResource(R.drawable.medicine_tab_false);
			}

			pageDotGroup.addView(imageView);
		}

		mImageViews = new ImageView[2][];
		// 将图片装载到数组中,其中一组类似缓冲，防止图片少时出现黑色图片，即显示不出来
		mImageViews[0] = new ImageView[imgIdArray.length];
		mImageViews[1] = new ImageView[imgIdArray.length];

		for (int i = 0; i < mImageViews.length; i++) {
			for (int j = 0; j < mImageViews[i].length; j++) {
				final ImageView imageView = new ImageView(getActivity());
				LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
						LinearLayout.LayoutParams.WRAP_CONTENT,
						LinearLayout.LayoutParams.WRAP_CONTENT);
				imageView.setLayoutParams(lp);
				imageView.setImageResource(imgIdArray[j]);
				imageView.setContentDescription("R.id.item" + (j + 1));
				imageView.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						// ToastUtil.showToast(imageView.getContentDescription()
						// + "");
					}
				});
				mImageViews[i][j] = imageView;

			}
		}

		// 设置Adapter
		_viewPager.setAdapter(new ShopPageAdapter(getActivity(), mImageViews,
				imgIdArray));
		// 设置监听，主要是设置点点的背景
		_viewPager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageScrollStateChanged(int arg0) {

			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {

			}

			@Override
			public void onPageSelected(int arg0) {
				setImageBackground(arg0 % imgIdArray.length);
			}
		});

		_viewPager.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (imgIdArray.length == 0 || imgIdArray.length == 1)
					return true;
				else
					return false;
			}
		});

		// 设置ViewPager的默认项, 设置为长度的50倍，这样子开始就能往左滑动
		_viewPager.setCurrentItem((imgIdArray.length) * 50);
		if (imgIdArray.length > 1) {
			mHandler.sendEmptyMessageDelayed(MSG_CHANGE_PHOTO,
					PHOTO_CHANGE_TIME);
		}
		// ptrScroll = (PagerScrollView) view.findViewById(R.id.pagerScroll);

	}

	/**
	 * 设置选中的tip的背景
	 * 
	 * @param selectItemsIndex
	 */
	private void setImageBackground(int selectItemsIndex) {
		for (int i = 0; i < tips.length; i++) {
			if (i == selectItemsIndex) {
				tips[i].setBackgroundResource(R.drawable.medicine_tab_true);
			} else {
				tips[i].setBackgroundResource(R.drawable.medicine_tab_false);
			}
		}
	}

	/**
	 * 滚动的回调方法，当滚动的Y距离大于或者等于 menu布局距离父类布局顶部的位置，就显示menu的悬浮框 当滚动的Y的距离小于
	 * menu布局距离父类布局顶部的位置加上menu布局的高度就移除menu的悬浮框
	 * 
	 */

	/**
	 * 显示menu的悬浮框
	 */
	private void showSuspend(View view) {

		ImageButton ibtn_shop_bus = (ImageButton) view
				.findViewById(R.id.btn_shop_cart);
		ImageButton ibtn_shop_store = (ImageButton) view
				.findViewById(R.id.btn_shop_score);
		ImageButton ibtn_shop_coupon = (ImageButton) view
				.findViewById(R.id.btn_shop_coupon);
		ImageButton ibtn_shop_indent = (ImageButton) view
				.findViewById(R.id.btn_shop_order);
		ibtn_shop_bus.setOnClickListener(this);
		ibtn_shop_store.setOnClickListener(this);
		ibtn_shop_coupon.setOnClickListener(this);
		ibtn_shop_indent.setOnClickListener(this);

	}

	@Override
	public void onScroll(AbsListView arg0, int firstVisibleItem, int arg2,
			int arg3) {

		// 工具栏的显示与隐藏
		if (firstVisibleItem >= 1)
			_topBarLayoutFloat.setVisibility(View.VISIBLE);
		else
			_topBarLayoutFloat.setVisibility(View.INVISIBLE);

		if (_refreshAction == RefreshAction.NONE) {
			if (_bGetMoreEnable) {// 有加载更多
				int lastPos = _listView.getLastVisiblePosition();
				int total = _listView.getHeaderViewsCount() + _dataList.size()
						+ _listView.getFooterViewsCount();
				if (lastPos == total - 1) {
					// 加载更多显示出来了，开始加载
					_refreshAction = RefreshAction.LOADMORE;
					setGetMoreContent("正在加载...", true, true);
					loadingData(false, _pageNum + 1);
				}
			}
		}

	}

	@Override
	public void onScrollStateChanged(AbsListView arg0, int arg1) {

	}

	@Override
	public void onHeaderRefresh(DDPullToRefreshView view) {
		if (_refreshAction == RefreshAction.NONE) {
			_refreshAction = RefreshAction.PULLTOREFRESH;
			loadingData(false, 1);
		} else {
			// 正在加载，什么也不做
			view.onHeaderRefreshComplete();
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

		// 进去商品详情页
		int dataIdx = arg2 - _listView.getHeaderViewsCount();

		if (dataIdx >= _dataList.size())
			return;

		MyUtils.showLog("", "点击查看 " + arg2 + " "
				+ _dataList.get(dataIdx).toString());

		Intent intent = new Intent(getActivity(), GoodsDetailActivity.class);
		intent.putExtra("data", _dataList.get(dataIdx));
		startActivity(intent);
	}
}
