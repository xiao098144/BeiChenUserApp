package com.ddoctor.user.fragment;

import java.io.File;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.application.MyApplication;
import com.ddoctor.enums.RetError;
import com.ddoctor.user.activity.login.LoginActivity;
import com.ddoctor.user.activity.mine.MyCollectsActivity;
import com.ddoctor.user.activity.mine.MyDeviceActivity;
import com.ddoctor.user.activity.mine.MyFriendsActivity;
import com.ddoctor.user.activity.mine.MyInfoActivity;
import com.ddoctor.user.activity.mine.MyIntegralActivity;
import com.ddoctor.user.activity.mine.MyRecordsActivity;
import com.ddoctor.user.activity.mine.MySettingsActivity;
import com.ddoctor.user.activity.mine.SignRecordsActivity;
import com.ddoctor.user.activity.shop.OrderListActivity;
import com.ddoctor.user.config.AppBuildConfig;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.task.FileUploadTask;
import com.ddoctor.user.task.TaskPostCallBack;
import com.ddoctor.user.wapi.bean.PatientBean;
import com.ddoctor.user.wapi.bean.UploadBean;
import com.ddoctor.user.wapi.constant.Upload;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.DialogUtil;
import com.ddoctor.utils.FileOperationUtil;
import com.ddoctor.utils.ImageLoaderUtil;
import com.ddoctor.utils.MyUtils;
import com.ddoctor.utils.StringUtils;
import com.ddoctor.utils.ToastUtil;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.tencent.tauth.Tencent;
import com.umeng.analytics.MobclickAgent;

/**
 * 屏蔽我的设备 我的好友 
 * 
 * @author 萧
 * @Date 2015-6-24下午3:33:12
 * @TODO TODO
 */
public class MineFragment extends BaseFragment {

	private ImageView head_img;
	private ImageButton img_sign;
	private TextView tv_name;
	private TextView tv_loc;

	private RelativeLayout layout_info;
	private TextView tv_info;
	private RelativeLayout layout_integral;
	private TextView tv_integral;
	// private RelativeLayout layout_device;
	// private TextView tv_device;
	// private RelativeLayout layout_friends;
	// private TextView tv_friends;
	private RelativeLayout layout_order;
	// private TextView tv_order;
	private RelativeLayout layout_records;
	// private TextView tv_records;
	private RelativeLayout layout_collects;
	// private TextView tv_collects;
	private RelativeLayout layout_settings;
	// private TextView tv_settings;

	private Button btn_logout;

	@Override
	public void onSaveInstanceState(Bundle outState) {
		MyUtils.showLog("MineFragment onSaveInsatnceState 页面被销毁 ");
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		MyUtils.showLog(" MineFragment onCreateView ");
		return inflater.inflate(R.layout.fragment_mine, null);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		findViewById(view);
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		
		MyUtils.showLog("requestCode=" + requestCode + " resultCode=" + resultCode);
		
		if( resultCode != Activity.RESULT_CANCELED )
		{
			if( requestCode == 0 )
			{// 选择图片
				if( data != null )
					startPhotoCrop(data.getData());
			}
			else if( requestCode == 1 )
			{// 拍照
				String filename = DataModule.getTakePhotoTempFilename("head");
				File f = new File(filename);
				if( f.exists() )
				{
					MyUtils.showLog("startPhotoCrop: " + filename);
					startPhotoCrop(Uri.fromFile(f));
				}
				else
					ToastUtil.showToast("获取图片失败!");
				
//				 startPhotoCrop(_imageFilePath);
			}
			else if( requestCode == 2 )
			{
				// 剪裁
				if( data != null )
				{
					Bundle b = data.getExtras();
					if( b != null )
					{	
						Bitmap bmp = MyUtils.loadBitmapFromFile(DataModule.getTakePhotoTempFilename("head-crop"));
						if( bmp != null ){
							FileOperationUtil.deleteFile(DataModule.getTakePhotoTempFilename("head"));
							onUploadPhoto(bmp);
						}
					}
				}
			}
		}
		
		super.onActivityResult(requestCode, resultCode, data);
	}
	
	
	public void startPhotoCrop(Uri uri) {

        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri, "image/*");
        // 设置裁剪
        intent.putExtra("crop", "true");
        // aspectX aspectY 是宽高的比例
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        // outputX outputY 是裁剪图片宽高
        intent.putExtra("outputX", 300);
        intent.putExtra("outputY", 300);
        intent.putExtra("output", Uri.fromFile(new File(DataModule.getTakePhotoTempFilename("head-crop"))));
        intent.putExtra("return-data", true);
        startActivityForResult(intent, 2);
	}
	
	@Override
	public void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("MineFragment");
//		if (DataModule.getInstance().isUserInfoUpdated()) {
			PatientBean patientBean = DataModule.getInstance()
					.getLoginedUserInfo();
			if (null != patientBean) {
				MyUtils.showLog("MineFragment ShowUSerInfo "+patientBean.toString());
				ImageLoaderUtil.displayRoundedCorner(StringUtils.urlFormatRemote(patientBean.getImage()),
						head_img, 150, R.drawable.default_protrait);
				tv_name.setText(patientBean.getName());
			}
//		}
	}

	@Override
	public void onPause() {
		super.onPause();
		MyUtils.showLog("MineFragment onPause ");
		MobclickAgent.onPageEnd("MineFragment");
	}
	
	private void findViewById(View view) {
		head_img = (ImageView) view.findViewById(R.id.mine_head_img);
		img_sign = (ImageButton) view.findViewById(R.id.mine_img_sign);
		tv_name = (TextView) view.findViewById(R.id.mine_tv_name);
		tv_loc = (TextView) view.findViewById(R.id.mine_tv_loc);
		layout_info = (RelativeLayout) view.findViewById(R.id.mine_info_layout);
		tv_info = (TextView) view.findViewById(R.id.mine_info_detail);
		layout_integral = (RelativeLayout) view
				.findViewById(R.id.mine_integral_layout);
		tv_integral = (TextView) view.findViewById(R.id.mine_integral_detail);

		// layout_device = (RelativeLayout) view
		// .findViewById(R.id.mine_device_layout);
		// tv_device = (TextView) view.findViewById(R.id.mine_device_detail);
		// layout_friends = (RelativeLayout) view
		// .findViewById(R.id.mine_friends_layout);
		// tv_friends = (TextView) view.findViewById(R.id.mine_friends_detail);

		layout_order = (RelativeLayout) view
				.findViewById(R.id.mine_order_layout);
		// tv_order = (TextView) view.findViewById(R.id.mine_order_detail);
		layout_records = (RelativeLayout) view
				.findViewById(R.id.mine_records_layout);
		// tv_records = (TextView) view.findViewById(R.id.mine_records_detail);
		layout_collects = (RelativeLayout) view
				.findViewById(R.id.mine_collects_layout);
		// tv_collects = (TextView)
		// view.findViewById(R.id.mine_collects_detail);
		layout_settings = (RelativeLayout) view
				.findViewById(R.id.mine_settings_layout);
		// tv_settings = (TextView)
		// view.findViewById(R.id.mine_settings_detail);
		btn_logout = (Button) view.findViewById(R.id.bottom_red_center_btn);
		btn_logout.setText("退出当前账号");

		head_img.setOnClickListener(this);
		img_sign.setOnClickListener(this);
		layout_info.setOnClickListener(this);
		layout_integral.setOnClickListener(this);

		// layout_device.setOnClickListener(this);
		// layout_friends.setOnClickListener(this);

		layout_order.setOnClickListener(this);
		layout_records.setOnClickListener(this);
		layout_collects.setOnClickListener(this);
		layout_settings.setOnClickListener(this);
		btn_logout.setOnClickListener(this);

		PatientBean patientBean = DataModule.getInstance().getLoginedUserInfo();
		if (null != patientBean) {
			MyUtils.showLog("  "+patientBean.getImage()+"  "+StringUtils.urlFormatRemote(patientBean.getImage()));
			
			ImageLoaderUtil.displayRoundedCorner(StringUtils.urlFormatRemote(patientBean.getImage()),
					head_img, 150, R.drawable.chat_default_protrait);
			tv_name.setText(patientBean.getName());
		}
		String address = DataModule.getInstance().getAddress();
		if (null != address) {
			int indexOf = address.lastIndexOf('区');
			tv_loc.setText(address.substring(0, indexOf + 1));
		}
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.mine_head_img: {
			
			String[] items = new String[]{"本地图片","拍照"};
			DialogUtil.createListDialog(this.getActivity(), items, new DialogUtil.ListDialogCallback(){

				@Override
				public void onItemClick(int which) {
					if(which == 0 )
					{
						Intent intent = new Intent();
						intent.setType("image/*");
						intent.setAction(Intent.ACTION_GET_CONTENT);
						startActivityForResult(intent, 0);	
					}
					else
					{
						Intent intent = new Intent(
								MediaStore.ACTION_IMAGE_CAPTURE);
						String filename = DataModule
								.getTakePhotoTempFilename("head");
						File f = new File(filename);
						if (f.exists())
							f.delete();
						Uri uri = Uri.fromFile(f);
						intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
						startActivityForResult(intent, 1);
					}
				}
			}).show();
			
		}
			break;
		case R.id.mine_img_sign: {
			Intent intent = new Intent();
			intent.setClass(getActivity(), SignRecordsActivity.class);
			getActivity().startActivity(intent);
		}
			break;
		case R.id.mine_info_layout: {
			Intent intent = new Intent();
			intent.setClass(getActivity(), MyInfoActivity.class);
			getActivity().startActivity(intent);
		}
			break;
		case R.id.mine_integral_layout: {
			Intent intent = new Intent();
			intent.setClass(getActivity(), MyIntegralActivity.class);
			getActivity().startActivity(intent);
		}
			break;
		case R.id.mine_device_layout: {
			Intent intent = new Intent();
			intent.setClass(getActivity(), MyDeviceActivity.class);
			getActivity().startActivity(intent);
		}
			break;
		case R.id.mine_friends_layout: {
			Intent intent = new Intent();
			Bundle data = new Bundle();
			data.putBoolean("fromGroup", false);
			intent.putExtra(AppBuildConfig.BUNDLEKEY, data);
			intent.setClass(getActivity(), MyFriendsActivity.class);
			getActivity().startActivity(intent);
		}
			break;
		case R.id.mine_order_layout: {
			Intent intent = new Intent();
			intent.setClass(getActivity(), OrderListActivity.class);
			getActivity().startActivity(intent);
		}
			break;
		case R.id.mine_records_layout: {
			Intent intent = new Intent();
			intent.setClass(getActivity(), MyRecordsActivity.class);
			getActivity().startActivity(intent);
		}
			break;
		case R.id.mine_collects_layout: {
			Intent intent = new Intent();
			intent.setClass(getActivity(), MyCollectsActivity.class);
			getActivity().startActivity(intent);
		}
			break;
		case R.id.mine_settings_layout: {
			Intent intent = new Intent();
			intent.setClass(getActivity(), MySettingsActivity.class);
			getActivity().startActivity(intent);
		}
			break;
		case R.id.bottom_red_center_btn: {
			DataModule.getInstance().logout();
			try {
				if (MyApplication.tencent == null) {
					MyApplication.tencent = Tencent.createInstance(AppBuildConfig.TencentKey,
							MyApplication.getInstance().getApplicationContext());
				}
				MyApplication.tencent.logout(getActivity());
				if (MyApplication.mwxAPI == null) {
					MyApplication.mwxAPI = WXAPIFactory.createWXAPI(getActivity(),
							AppBuildConfig.WXAPP_ID, true);
				}
				MyApplication.mwxAPI.unregisterApp();
			} catch (Exception e) {
				// TODO: handle exception
			}
			Intent intent = new Intent();
			intent.setClass(getActivity(), LoginActivity.class);
			getActivity().startActivity(intent);
			getActivity().finish();
		}
			break;

		default:
			break;
		}
	}
	

	// 上传头像
	private Dialog _loadingDialog = null;
	private Bitmap _bitmap = null;
	
	private void onUploadPhoto(Bitmap bmp)
	{
		if( _bitmap != null )
		{
			_bitmap.recycle();
			_bitmap = null;
		}
		_bitmap = bmp;
		
		_loadingDialog = DialogUtil.createLoadingDialog(this.getActivity(), "提交中...");
		_loadingDialog.show();
		
		UploadBean uploadBean = new UploadBean();
		uploadBean.setFileType("jpg");
		
		uploadBean.setType(Upload.HEAD);
		
		byte[] data = MyUtils.Bitmap2Bytes(bmp);
		String s_data = android.util.Base64.encodeToString(data, Base64.DEFAULT);
		uploadBean.setFile(s_data);
		
		FileUploadTask task = new FileUploadTask(uploadBean);
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {
			@Override
			public void taskFinish(DDResult result) {

				if( _loadingDialog != null )
				{
					_loadingDialog.dismiss();
					_loadingDialog = null;
				}
				
				if( result.getError() == RetError.NONE )
				{
					on_task_finished(result);
				}
				else
				{
					on_task_failed(result);
				}

			}
		});
		task.executeParallel("");
	}
	
	private void on_task_finished(DDResult result)
	{
		FileOperationUtil.deleteFile(DataModule.getTakePhotoTempFilename("head-crop"));
		MyUtils.showLog("on_task_finished");
		Bundle b = result.getBundle();
		String fileUrl = b.getString("fileUrl");
		
		MyUtils.showLog(fileUrl);
		
//		Bitmap bmp1 = MyUtils.getRoundedCornerBitmap(bmp, 150);
		PatientBean patientBean = DataModule.getInstance().getLoginedUserInfo();
		patientBean.setImage(fileUrl);
		DataModule.getInstance().saveLoginedUserInfo(patientBean);
		ImageLoaderUtil.displayRoundedCorner(StringUtils.urlFormatRemote(fileUrl), head_img, 150, R.drawable.default_protrait);
	}
	
	private void on_task_failed(DDResult result)
	{
		ToastUtil.showToast(result.getErrorMessage());
	}
	

}
