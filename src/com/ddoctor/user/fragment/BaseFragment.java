package com.ddoctor.user.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.interfaces.OnConnectFragmentActivityListener;
import com.ddoctor.interfaces.OnDataSetChangedListener;
import com.ddoctor.utils.ImageLoaderUtil;
import com.ddoctor.utils.MyUtils;
import com.nostra13.universalimageloader.core.ImageLoader;

public class BaseFragment extends Fragment implements OnClickListener , OnDataSetChangedListener , OnConnectFragmentActivityListener{

	protected void showLog(String log) {
		// System.out.println(TAG + ":" + log);

		String tag = this.getClass().getName();

		MyUtils.showLog(tag, log);
	}

	protected void setTitle(String title) {
		TextView titleTextView = (TextView) this.getView().findViewById(
				R.id.title_center_txt);
		if (titleTextView != null)
			titleTextView.setText(title);
	}

	protected Button getLeftButton() {
		Button button = (Button) this.getView().findViewById(R.id.btn_left);

		return button;
	}

	protected Button getRightButton() {
		Button button = (Button) this.getView().findViewById(R.id.btn_right);

		return button;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		showLog((getArguments()==null)+"");
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		return inflater.inflate(R.layout.fragment_base, null);
	}

	@Override
	public void onStart() {
		super.onStart();

	}

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void onDetach() {
		super.onDetach();

	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
	}

	@Override
	public void onStop() {
		super.onStop();
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		if (!ImageLoader.getInstance().isInited()) {
			ImageLoaderUtil.initImageLoader(getActivity());
		}
	}

	@Override
	public void onClick(View arg0) {
		
	}

	@Override
	public void onDataSetChaned(Bundle moringData, Bundle noonData,
			Bundle nightData) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onRefreshUI(Bundle data) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void notifyFragment(Bundle data) {
		// TODO Auto-generated method stub
		
	}


}
