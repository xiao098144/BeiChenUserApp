package com.ddoctor.user.fragment;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.interfaces.OnClickCallBackListener;
import com.ddoctor.user.activity.mine.MyInfoActivity;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.data.MyProfile;
import com.ddoctor.user.wapi.bean.IllnessBean;
import com.ddoctor.user.wapi.bean.PatientBean;
import com.ddoctor.user.wapi.constant.IllnessType;
import com.ddoctor.utils.DialogUtil;
import com.ddoctor.utils.MyUtils;
import com.ddoctor.utils.StringUtils;
import com.ddoctor.utils.ToastUtil;
import com.umeng.analytics.MobclickAgent;

public class MyDetailInfoFragment extends BaseFragment implements
		OnClickCallBackListener {

	private EditText met_id;
	private EditText met_name;
	/** 糖尿病类型 */
	private TextView mtv_diabetes_type;
	/** 确诊日期 */
	private TextView mtv_diagnose_date;
	/** 起病方式 */
	private TextView mtv_onset_type;
	/** 起病症状 */
	private TextView mtv_onset_symptom;
	/** 医保类型 */
	private TextView mtv_health_insurance;
	/** 添加其它诊断 */
	private TextView mtv_add;

	@Override
	public void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("MyDetailInfoFragment");
	}

	@Override
	public void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("MyDetailInfoFragment");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.layout_info_detail, null);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		findViewById(view);
		initData();
		showUserInfo();
	}

	private List<IllnessBean> _diabetesTypeList = new ArrayList<IllnessBean>(); // 1
	private List<IllnessBean> _modeonSetList = new ArrayList<IllnessBean>(); // 2
	private List<IllnessBean> _onSetSympotomsList = new ArrayList<IllnessBean>(); // 3
	private List<IllnessBean> _healthInsuranceList = new ArrayList<IllnessBean>(); // 4
	private List<IllnessBean> _dianogticsList = new ArrayList<IllnessBean>(); // 5
	private List<IllnessBean> _illnessList;

	private List<IllnessBean> _chosedSymptomsList = new ArrayList<IllnessBean>();
	private List<IllnessBean> _chosedDiagnoticsList = new ArrayList<IllnessBean>();

	private PatientBean patientBean;

	private void initData() {
		_illnessList = DataModule.loadDict(MyProfile.DICT_ILLNESSS,
				IllnessBean.class);
		if (_illnessList == null) {
			ToastUtil.showToast("数据异常");
		} else {
			for (int i = 0; i < _illnessList.size(); i++) {
				IllnessBean illnessBean = _illnessList.get(i);
				int type = Integer.valueOf(illnessBean.getType());
				switch (type) {
				case IllnessType.TANGNIAOBINGLEIXING: {
					_diabetesTypeList.add(illnessBean);
				}
					break;
				case IllnessType.QIBINGFANGSHI: {
					_modeonSetList.add(illnessBean);
				}
					break;
				case IllnessType.QIBINGZHENGZHUANG: {
					_onSetSympotomsList.add(illnessBean);
				}
					break;
				case IllnessType.YIBAOLEIXING: {
					_healthInsuranceList.add(illnessBean);
				}
					break;
				case IllnessType.QITAZHENDUAN: {
					_dianogticsList.add(illnessBean);
				}
					break;
				default:
					break;
				}
			}
		}
	}

	private String getNameById(String id, List<IllnessBean> list, int type) {
		if (id.contains("43")) {
			return "未填写";
		}
		if (list == null || list.size() == 0) {
			return id;
		}
		String[] ids = id.split("、");
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < ids.length; i++) {
			for (int j = 0; j < list.size(); j++) {
				if (String.valueOf(list.get(j).getId()).equals(ids[i])) {
					switch (type) {
					case IllnessType.QIBINGZHENGZHUANG: {
						MyUtils.showLog("getNameById 生成起病症状已选集合  数据库元素 "
								+ ids[i] + " 元记录  "
								+ String.valueOf(list.get(j).getId()));
						_chosedSymptomsList.add(list.get(j));
					}
						break;
					case IllnessType.QITAZHENDUAN: {
						MyUtils.showLog("getNameById 生成其它诊断已选集合  数据库元素 "
								+ ids[i] + " 元记录  "
								+ String.valueOf(list.get(j).getId()));
						_chosedDiagnoticsList.add(list.get(j));
					}
						break;
					default:
						break;
					}
					sb.append(list.get(j).getName());
					sb.append("、");
				}
			}
		}
		String result = sb.toString();
		if (result.endsWith("、")) {
			result = result.substring(0, result.length() - 1);
		}
		MyUtils.showLog("getNameById 起病症状 " + _chosedSymptomsList.toString());
		MyUtils.showLog("getNameById 计算结果  " + result);
		return result;
	}

	private void showUserInfo() {
		patientBean = DataModule.getInstance().getLoginedUserInfo();
		if (patientBean != null) {
			MyUtils.showLog("MtDetailFragment " + patientBean.toString());

			met_id.setText(patientBean.getIdcard());
			met_name.setText(patientBean.getName());
			String type = patientBean.getType();
			if (!TextUtils.isEmpty(type)) {
				mtv_diabetes_type.setText(getNameById(type, _diabetesTypeList,
						IllnessType.TANGNIAOBINGLEIXING));
				mtv_diabetes_type.setTag(type);
			} else {
				mtv_diabetes_type.setTag(43);
			}
			String confirmed = patientBean.getConfirmed();
			if (!TextUtils.isEmpty(confirmed)) {
				mtv_diagnose_date.setText(confirmed);
			}
			String medicalinsurance = patientBean.getMedicalinsurance();
			if (!TextUtils.isEmpty(medicalinsurance)) {
				mtv_health_insurance.setText(getNameById(medicalinsurance,
						_healthInsuranceList, IllnessType.YIBAOLEIXING));
				mtv_health_insurance.setTag(medicalinsurance);
			} else {
				mtv_health_insurance.setTag(43);
			}
			String symptoms = patientBean.getSymptoms();
			if (!TextUtils.isEmpty(symptoms)) {
				if (_chosedSymptomsList.size() > 0) {
					_chosedSymptomsList.clear();
				}
				mtv_onset_symptom.setText(getNameById(symptoms,
						_onSetSympotomsList, IllnessType.QIBINGZHENGZHUANG));
				mtv_onset_symptom.setTag(symptoms);
			} else {
				mtv_onset_symptom.setTag(43);
			}
			String modeonset = patientBean.getModeonset();
			if (!TextUtils.isEmpty(modeonset)) {
				mtv_onset_type.setText(getNameById(modeonset, _modeonSetList,
						IllnessType.QIBINGFANGSHI));
				mtv_onset_type.setTag(modeonset);
			} else {
				mtv_onset_type.setTag(43);
			}
			String diagnostic = patientBean.getDiagnostic();
			MyUtils.showLog("其它诊断  " + diagnostic);
			if (!TextUtils.isEmpty(diagnostic)) {
				if (_chosedDiagnoticsList.size() > 0) {
					_chosedDiagnoticsList.clear();
				}
				mtv_add.setText(getNameById(diagnostic, _dianogticsList,
						IllnessType.QITAZHENDUAN));
				mtv_add.setTag(diagnostic);
			} else {
				mtv_add.setTag(43);
			}
		} else {
			MyUtils.showLog("MyDetailFragment patient == null ");
		}

	}

	private void findViewById(View view) {
		met_id = (EditText) view.findViewById(R.id.myinfo_et_id);
		met_name = (EditText) view.findViewById(R.id.myinfo_et_name);
		mtv_diabetes_type = (TextView) view
				.findViewById(R.id.myinfo_tv_diabetes_type);
		mtv_diagnose_date = (TextView) view
				.findViewById(R.id.myinfo_tv_diagnose_date);
		mtv_onset_type = (TextView) view
				.findViewById(R.id.myinfo_tv_onset_type);
		mtv_onset_symptom = (TextView) view
				.findViewById(R.id.myinfo_tv_onset_symptom);
		mtv_health_insurance = (TextView) view
				.findViewById(R.id.myinfo_tv_health_insurance);
		mtv_add = (TextView) view.findViewById(R.id.myinfo_tv_add_diagnosis);
		mtv_diabetes_type.setOnClickListener(this);
		mtv_diagnose_date.setOnClickListener(this);
		mtv_onset_type.setOnClickListener(this);
		mtv_onset_symptom.setOnClickListener(this);
		mtv_health_insurance.setOnClickListener(this);
		mtv_add.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.myinfo_tv_diabetes_type: {
			DialogUtil.showSingleChoiceDialog(getActivity(), _diabetesTypeList,
					this, IllnessType.TANGNIAOBINGLEIXING);
		}
			break;
		case R.id.myinfo_tv_diagnose_date: {
			DialogUtil.showDatePicker(getActivity(), 0,0,0, this, 22);
		}
			break;
		case R.id.myinfo_tv_onset_type: {
			DialogUtil.showSingleChoiceDialog(getActivity(), _modeonSetList,
					this, IllnessType.QIBINGFANGSHI);
		}
			break;
		case R.id.myinfo_tv_onset_symptom: {
			DialogUtil.showIllnessMultiDialog(getActivity(),
					_onSetSympotomsList, _chosedSymptomsList, this,
					IllnessType.QIBINGZHENGZHUANG);
		}
			break;
		case R.id.myinfo_tv_health_insurance: {
			DialogUtil.showSingleChoiceDialog(getActivity(),
					_healthInsuranceList, this, IllnessType.YIBAOLEIXING);
		}
			break;
		case R.id.myinfo_tv_add_diagnosis: {
			DialogUtil.showIllnessMultiDialog(getActivity(), _dianogticsList,
					_chosedDiagnoticsList, this, IllnessType.QITAZHENDUAN);
		}
			break;

		default:
			break;
		}
	}

	OnClickCallBackListener onClickCallBackListener;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		if (activity instanceof MyInfoActivity) {
			((MyInfoActivity) activity)
					.setOnConnectFragmentActivityListener(this);
		}
		try {
			onClickCallBackListener = (OnClickCallBackListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ "must implement OnArticleSelectedListener");
		}
	}

	@Override
	public void onClickCallBack(Bundle data) {
		int type = data.getInt("type");
		String name = data.getString("name");

		switch (type) {
		case IllnessType.TANGNIAOBINGLEIXING: {
			int ids = data.getInt("id");
			mtv_diabetes_type.setText(name);
			mtv_diabetes_type.setTag(ids);
		}
			break;
		case IllnessType.QIBINGFANGSHI: {
			int ids = data.getInt("id");
			mtv_onset_type.setText(name);
			mtv_onset_type.setTag(ids);
		}
			break;
		case IllnessType.YIBAOLEIXING: {
			int ids = data.getInt("id");
			mtv_health_insurance.setText(name);
			mtv_health_insurance.setTag(ids);
		}
			break;
		case IllnessType.QIBINGZHENGZHUANG: {
			String[] nameArray = name.split("、");
			String ids = data.getString("id");
			String[] id = ids.split("、");
			if (nameArray != null && nameArray.length > 0) {
				if (_chosedSymptomsList.size() > 0) {
					_chosedSymptomsList.clear();
				}
				for (int i = 0; i < nameArray.length; i++) {
					IllnessBean illnessBean = new IllnessBean();
					illnessBean.setId(Integer.valueOf(id[i]));
					illnessBean.setName(nameArray[i]);
					illnessBean.setType(String
							.valueOf(IllnessType.QIBINGZHENGZHUANG));
					_chosedSymptomsList.add(illnessBean);
				}
			}
			if (_onSetSympotomsList != null && _onSetSympotomsList.size() > 0) {
				MyUtils.showLog("", "   " + _onSetSympotomsList.size());
			}
			mtv_onset_symptom.setText(name);
			mtv_onset_symptom.setTag(ids);
		}
			break;
		case IllnessType.QITAZHENDUAN: {
			String[] nameArray = name.split("、");
			String ids = data.getString("id");
			String[] id = ids.split("、");
			if (nameArray != null && nameArray.length > 0) {
				if (_chosedDiagnoticsList.size() > 0) {
					_chosedDiagnoticsList.clear();
				}
				for (int i = 0; i < nameArray.length; i++) {
					IllnessBean illnessBean = new IllnessBean();
					illnessBean.setId(Integer.valueOf(id[i]));
					illnessBean.setName(nameArray[i]);
					illnessBean.setType(String
							.valueOf(IllnessType.QITAZHENDUAN));
					_chosedDiagnoticsList.add(illnessBean);
				}
			}
			if (_dianogticsList != null && _dianogticsList.size() > 0) {
				MyUtils.showLog("", " " + _dianogticsList.size());
			}
			mtv_add.setText(name);
			mtv_add.setTag(ids);
		}
			break;
		case 22: {
			mtv_diagnose_date.setText(name);
		}
			break;
		default:
			break;
		}
	}

	private boolean checkData() {
		String name = met_name.getText().toString().trim();
		if (TextUtils.isEmpty(name)) {
			ToastUtil.showToast("真实姓名不能为空");
			return false;
		}
		String id = met_id.getText().toString().trim();
		if (!StringUtils.checkNID(id)) {
			ToastUtil.showToast("请输入格式正常的身份证号");
			return false;
		}
		String type = String.valueOf(mtv_diabetes_type.getTag());
		if (TextUtils.isEmpty(type) || "43".equals(type)) {
			ToastUtil.showToast("请选择糖尿病类型");
			return false;
		}
		String date = mtv_diagnose_date.getText().toString();
		// if (TextUtils.isEmpty(date)) {
		// ToastUtil.showToast("请选择确诊日期");
		// return false;
		// }
		String onsetType = String.valueOf(mtv_onset_type.getTag());
		if (TextUtils.isEmpty(onsetType) || "43".equals(onsetType)) {
			ToastUtil.showToast("请选择起病方式");
			return false;
		}
		String onSetSymptom = String.valueOf(mtv_onset_symptom.getTag());
		if (TextUtils.isEmpty(onSetSymptom) || "43".equals(onSetSymptom)) {
			ToastUtil.showToast("请选择起病症状");
			return false;
		}
		String insuranceType = String.valueOf(mtv_health_insurance.getTag());
		if (TextUtils.isEmpty(insuranceType) || "43".equals(insuranceType)) {
			ToastUtil.showToast("请选择医保类型");
			return false;
		}
		String dianogtics = String.valueOf(mtv_add.getTag());
		// if (TextUtils.isEmpty(dianogtics)||"43".equals(dianogtics)) {
		// ToastUtil.showToast("请选择其它诊断");
		// return false;
		// }
		patientBean.setConfirmed(date);
		patientBean.setDiagnostic(dianogtics);
		patientBean.setIdcard(id);
		patientBean.setName(name);
		patientBean.setSymptoms(onSetSymptom);
		patientBean.setMedicalinsurance(insuranceType);
		patientBean.setModeonset(onsetType);
		patientBean.setType(type);
		Bundle data = new Bundle();
		data.putParcelable("data", patientBean);
		onClickCallBackListener.onClickCallBack(data);
		return true;
	}

	@Override
	public void notifyFragment(Bundle data) {
		super.notifyFragment(data);
		checkData();
	}

}
