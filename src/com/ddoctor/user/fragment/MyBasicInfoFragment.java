package com.ddoctor.user.fragment;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.interfaces.OnClickCallBackListener;
import com.ddoctor.user.activity.mine.MyInfoActivity;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.wapi.bean.IllnessBean;
import com.ddoctor.user.wapi.bean.PatientBean;
import com.ddoctor.utils.DialogUtil;
import com.ddoctor.utils.MyUtils;
import com.ddoctor.utils.StringUtils;
import com.ddoctor.utils.TimeUtil;
import com.ddoctor.utils.ToastUtil;
import com.umeng.analytics.MobclickAgent;

public class MyBasicInfoFragment extends BaseFragment implements
		OnClickCallBackListener {

	private EditText met_nickname;
	private TextView mtv_sex;
	private TextView mtv_year;
	private TextView mtv_height;
	private TextView mtv_weight;

	OnClickCallBackListener onClickCallBackListener;
	private PatientBean patientBean;

	@Override
	public void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("MyBasicInfoFragment");
	}

	@Override
	public void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("MyBasicInfoFragment");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.layout_info_basic, null);
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		if (activity instanceof MyInfoActivity) {
			((MyInfoActivity) activity)
					.setOnConnectFragmentActivityListener(this);
		}
		try {
			onClickCallBackListener = (OnClickCallBackListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ "must implement OnArticleSelectedListener");
		}
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		findviewById(view);
		showUserInfo();
	}

	private void showUserInfo() {

		patientBean = DataModule.getInstance().getLoginedUserInfo();
		if (patientBean != null) {
			MyUtils.showLog("MyBasicFragment PatientBean "
					+ patientBean.toString());
			met_nickname.setText(patientBean.getName());
			mtv_height.setText(String.format("%dcm", patientBean.getHeight()));
			mtv_sex.setText(patientBean.getSex() == null ? "男" : patientBean
					.getSex() == 0 ? "男" : "女");
			mtv_sex.setTag(patientBean.getSex() == null ? 0 : patientBean
					.getSex());
			mtv_weight
					.setText(String.format("%.0fkg", patientBean.getWeight()));
			String year = patientBean.getBirthday().substring(0, 4);
			if (StringUtils.pureNum(year)) {
				currentYear = Integer.valueOf(year);
			}else {
				currentYear =TimeUtil.getInstance().getCurrentYear(); 
			}
			mtv_year.setText(String.valueOf(currentYear));
			
			currentHeight = patientBean.getHeight();
			currentWeight = patientBean.getWeight();
			
			
		}else {
			MyUtils.showLog("MyBasicFragment patient == null ");
		}
	}

	private void findviewById(View view) {
		met_nickname = (EditText) view.findViewById(R.id.myinfo_et_nickname);
		mtv_sex = (TextView) view.findViewById(R.id.myinfo_tv_sex);
		mtv_year = (TextView) view.findViewById(R.id.myinfo_tv_year);
		mtv_height = (TextView) view.findViewById(R.id.myinfo_tv_height);
		mtv_weight = (TextView) view.findViewById(R.id.myinfo_tv_weight);

		mtv_sex.setOnClickListener(this);
		mtv_year.setOnClickListener(this);
		mtv_height.setOnClickListener(this);
		mtv_weight.setOnClickListener(this);

		for (int i = 0; i < 2; i++) {
			IllnessBean illnessBean = new IllnessBean();
			illnessBean.setType("0");
			illnessBean.setId(i % 2);
			illnessBean.setName(i == 0 ? "男" : "女");
			_genderList.add(illnessBean);
		}

		for (int i = 0; i <= 200; i++) {
			_heightList.add(String.valueOf(50 + i));
		}
		for (int i = 0; i <= 170; i++) {
			_weightList.add(String.valueOf(30 + i));
		}

		for (int i = 0; i <= (TimeUtil.getInstance().getCurrentYear() - 1940); i++) {
			_yearList.add(String.valueOf(1940 + i));
		}

	}

	private List<IllnessBean> _genderList = new ArrayList<IllnessBean>();
	private List<String> _heightList = new ArrayList<String>();
	private List<String> _weightList = new ArrayList<String>();
	private List<String> _yearList = new ArrayList<String>();

	private int defaultYear = 1980;
	private int defaultHeight = 160;
	private int defaultWeight = 50;
	private int currentYear = defaultYear;
	private float currentWeight = defaultWeight;
	private int currentHeight = defaultHeight;

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.myinfo_tv_sex: {
			DialogUtil.showSingleChoiceDialog(getActivity(), _genderList, this,
					1);
		}
			break;
		case R.id.myinfo_tv_year: {

			DialogUtil.showYearPickerDialog(getActivity(), this, _yearList, 2,
					currentYear - 1940);
		}
			break;
		case R.id.myinfo_tv_height: {
			DialogUtil.showYearPickerDialog(getActivity(), this, _heightList,
					3, currentHeight - 50);
		}
			break;
		case R.id.myinfo_tv_weight: {
			DialogUtil.showYearPickerDialog(getActivity(), this, _weightList,
					4, (int) (currentWeight - 30));
		}
			break;

		default:
			break;
		}
	}

	private boolean checkData() {
		String name = met_nickname.getText().toString().trim();
		if (TextUtils.isEmpty(name)) {
			ToastUtil.showToast("请填写昵称");
			return false;
		}

		String year = mtv_year.getText().toString();
		if (!StringUtils.pureNum(year)) {
			ToastUtil.showToast("请选择出生年");
			return false;
		}
		String height = mtv_height.getText().toString();
		if (TextUtils.isEmpty(height)) {
			ToastUtil.showToast("请选择身高");
			return false;
		}
		height = height.substring(0, height.length() - 2);
		String weight = mtv_weight.getText().toString();
		if (TextUtils.isEmpty(weight)) {
			ToastUtil.showToast("请选择体重");
			return false;
		}
		weight = weight.substring(0, weight.length() - 2);

		patientBean.setName(name);
		patientBean.setBirthday(year);
		patientBean.setHeight(Integer.valueOf(height));
		patientBean.setWeight(Float.valueOf(weight));
		patientBean.setSex((Integer) mtv_sex.getTag());

		Bundle data = new Bundle();
		data.putParcelable("data", patientBean);
		onClickCallBackListener.onClickCallBack(data);
		return true;
	}

	@Override
	public void onClickCallBack(Bundle data) {
		MyUtils.showLog("", "" + data.toString());
		int type = data.getInt("type");
		String name = data.getString("name");
		switch (type) {
		case 1: {
			MyUtils.showLog("MyBasicFragment " + data.toString());
			int id = data.getInt("id");
			mtv_sex.setText(name);
			mtv_sex.setTag(id);
		}
			break;
		case 2: {
			currentYear = Integer.valueOf(name);
			mtv_year.setText(name);
		}
			break;
		case 3: {
			currentHeight = Integer.valueOf(name);
			mtv_height.setText(String.format("%scm", name));
		}
			break;
		case 4: {
			currentWeight = Integer.valueOf(name);
			mtv_weight.setText(String.format("%skg", name));
		}
			break;

		default:
			break;
		}

	}

	@Override
	public void notifyFragment(Bundle data) {
		checkData();
	}

}
