package com.ddoctor.user.fragment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.content.Intent;
import android.graphics.drawable.PaintDrawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.LinearLayout.LayoutParams;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.user.activity.ask.adapter.ConversationListAdapter;
import com.ddoctor.user.activity.chatgroup.CreateGroupActivity;
import com.ddoctor.user.activity.chatgroup.FriendGroupActivity;
import com.ddoctor.user.activity.mine.MyFriendsActivity;
import com.ddoctor.user.config.AppBuildConfig;
import com.ddoctor.user.model.ConversationListBean;
import com.ddoctor.user.swiplistview.SwipeMenu;
import com.ddoctor.user.swiplistview.SwipeMenuCreator;
import com.ddoctor.user.swiplistview.SwipeMenuItem;
import com.ddoctor.user.swiplistview.SwipeMenuListView;
import com.ddoctor.user.swiplistview.SwipeMenuListView.OnMenuItemClickListener;
import com.ddoctor.utils.MyUtils;
import com.ddoctor.utils.ToastUtil;
import com.umeng.analytics.MobclickAgent;

/**
 * 尝试接收融云list 创建界面 
 * @author 萧
 * @Date 2015-5-27下午9:38:13
 * @TODO TODO
 */
public class ChatGroupFragment extends BaseFragment implements
		OnItemLongClickListener, OnItemClickListener {

	private Button rigthBtn;
	private TextView tv_online;
	private TextView tv_online_notice;
	private TextView tv_edu;
	private TextView tv_edu_notice;
	private TextView tv_friend;
	private TextView tv_friend_notice;
	private SwipeMenuListView refreshLv;
	private ConversationListAdapter adapter;
	private List<ConversationListBean> dataList;
	private String s;

	private void initData() {
//		List<Conversation> conversationList = RongIM.getInstance().getConversationList();
//		for (int i = 0; i < conversationList.size(); i++) {
//			conversationList.get(i).get
//		}
		dataList = new ArrayList<ConversationListBean>();
		for (int i = 0; i < 8; i++) {
			ConversationListBean conversationListBean = new ConversationListBean();
			conversationListBean.setLastMsgType(i % 2);
			conversationListBean.setConversationName("用户名" + i % 2 + " - " + i);
			conversationListBean.setLastMsg("最后消息内容 - " + i);
			conversationListBean.setLastMsgTime("2015-" + String.format("%02d", (i + 2)) + "-"
					+ (i + 21) + " 12:21:32");
			if (i == 3 || i == 5) {
				conversationListBean.setStick(true);
				conversationListBean.setSetStickTime("2015-" + String.format("%02d", (i + 2)) + "-"
						+ (i + 25) + " 12:43:21");
			}
			dataList.add(conversationListBean);
		}
		Collections.sort(dataList);
	}

	@Override
	public void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("ChatGroupFragment");
	}
	
	@Override
	public void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("ChatGroupFragment");
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_layout_chatgroup, null);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		findViewById(view);
		initData();
		adapter.setData(dataList);
	}

	private void findViewById(View view) {
		rigthBtn = getRightButton();
		rigthBtn.setVisibility(View.VISIBLE);
		rigthBtn.setBackgroundResource(R.drawable.btn_more_bg);
		setTitle(getResources().getString(R.string.group_title));

		tv_online = (TextView) view.findViewById(R.id.group_tv_online);
		tv_edu = (TextView) view.findViewById(R.id.group_tv_edu);
		tv_friend = (TextView) view.findViewById(R.id.group_tv_friend);
		tv_online_notice = (TextView) view
				.findViewById(R.id.group_tv_online_notice);
		tv_edu_notice = (TextView) view.findViewById(R.id.group_tv_edu_notice);
		tv_friend_notice = (TextView) view
				.findViewById(R.id.group_tv_friend_notice);
		refreshLv = (SwipeMenuListView) view.findViewById(R.id.group_lv);
		adapter = new ConversationListAdapter(getActivity());
		SwipeMenuCreator creator = new SwipeMenuCreator() {

			@Override
			public void create(SwipeMenu menu) {
				SwipeMenuItem deleteMenu = new SwipeMenuItem(getActivity());
				deleteMenu.setBackground(R.drawable.delete_small);
				deleteMenu.setWidth(MyUtils.dp2px(60, getActivity()));
				SwipeMenuItem stickItem = new SwipeMenuItem(getActivity());
				stickItem.setBackground(R.drawable.chat_stick);
				stickItem.setWidth(MyUtils.dp2px(60, getActivity()));
				menu.addMenuItem(stickItem);
				menu.addMenuItem(deleteMenu);
			}
		};
		refreshLv.setMenuCreator(creator);
		refreshLv.setAdapter(adapter);
		setListener();
	}

	private void setListener() {
		refreshLv.setOnMenuItemClickListener(new OnMenuItemClickListener() {

			@Override
			public void onMenuItemClick(int position, SwipeMenu menu, int index) {
				ToastUtil.showToast("点击position = " + position + " index "
						+ index);
			}
		});
		rigthBtn.setOnClickListener(this);
		refreshLv.setOnItemLongClickListener(this);
		refreshLv.setOnItemClickListener(this);
		tv_online.setOnClickListener(this);
		tv_edu.setOnClickListener(this);
		tv_friend.setOnClickListener(this);

	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.btn_right: {
			showMenu(v);
		}
			break;
		case R.id.group_tv_online:{
			ToastUtil.showToast("直播圈");
		}
		break;
		case R.id.group_tv_edu:{
			ToastUtil.showToast("患教圈");
		}
		break;
		case R.id.group_tv_friend:{
			Intent intent = new Intent();
			intent.setClass(getActivity(), FriendGroupActivity.class);
			getActivity().startActivity(intent);
		}
		break;
			
		default:
			break;
		}
	}

	private void showMenu(View parent) {
		View view = View.inflate(getActivity(), R.layout.layout_chatgroup_menu,
				null);
		view.startAnimation(AnimationUtils.loadAnimation(getActivity(),
				R.anim.fade_in));
		view.setFocusable(true);
		view.setFocusableInTouchMode(true);

		final PopupWindow popupWindow = new PopupWindow(view,
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		popupWindow.setBackgroundDrawable(new PaintDrawable());
		// 设置点击窗口外边窗口消失
		popupWindow.setOutsideTouchable(true);
		// 设置此参数获得焦点，否则无法点击
		popupWindow.setFocusable(true);
		popupWindow.showAsDropDown(parent, 20, 5);
		popupWindow.update();
		view.setOnKeyListener(new OnKeyListener() {

			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (keyCode == KeyEvent.KEYCODE_BACK) {
					popupWindow.dismiss();
					return true;
				}
				return false;
			}
		});

		TextView tv_create = (TextView) view
				.findViewById(R.id.chatgroup_menu_tv_create);
		TextView tv_all = (TextView) view
				.findViewById(R.id.chatgroup_menu_tv_allgroup);
		TextView tv_contact = (TextView) view
				.findViewById(R.id.chatgroup_menu_tv_contact);

		class ClickListener implements OnClickListener {

			@Override
			public void onClick(View v) {
				switch (v.getId()) {
				case R.id.chatgroup_menu_tv_create: {
					Intent intent = new Intent();
					intent.setClass(getActivity(), CreateGroupActivity.class);
					getActivity().startActivity(intent);
					// popupWindow.dismiss();
				}
					break;
				case R.id.chatgroup_menu_tv_allgroup: {
					ToastUtil.showToast("查看全部圈子");
				}
					break;
				case R.id.chatgroup_menu_tv_contact: {
					Intent intent = new Intent();
					Bundle data = new Bundle();
					data.putBoolean("fromGroup", true);
					intent.putExtra(AppBuildConfig.BUNDLEKEY, data);
					intent.setClass(getActivity(), MyFriendsActivity.class);
					getActivity().startActivity(intent);
				}
					break;

				default:
					break;
				}
				popupWindow.dismiss();
			}

		}

		ClickListener clickListener = new ClickListener();

		tv_create.setOnClickListener(clickListener);
		tv_all.setOnClickListener(clickListener);
		tv_contact.setOnClickListener(clickListener);
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> parent, View view,
			int position, long id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		ToastUtil.showToast("" +"lastMSg "+ dataList.get(position-1).getLastMsgTime()+" stick "+dataList.get(position-1).getSetStickTime());
	}

}
