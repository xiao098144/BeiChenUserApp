package com.ddoctor.user.swiplistview;

public interface OnRefreshListener {

	void onDownPullRefresh();

	void onLoadingMore();
}
