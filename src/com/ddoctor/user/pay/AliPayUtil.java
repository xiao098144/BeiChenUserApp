package com.ddoctor.user.pay;

import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.spec.PKCS8EncodedKeySpec;

import com.ddoctor.utils.MyUtils;

/**
 * @filename AliPayUtil.java
 * @TODO 支付宝 工具类
 * @date 2014-6-22下午3:36:52
 * @Administrator 萧
 * 
 */
public class AliPayUtil {

	private static final String ALGORITHM = "RSA";

	private static final String SIGN_ALGORITHMS = "SHA1WithRSA";

	private static final String DEFAULT_CHARSET = "UTF-8";

	public static String sign(String content, String privateKey) {
		try {
			PKCS8EncodedKeySpec priPKCS8 = new PKCS8EncodedKeySpec(
					Base64.decode(privateKey));
			KeyFactory keyf = KeyFactory.getInstance(ALGORITHM);
			PrivateKey priKey = keyf.generatePrivate(priPKCS8);

			java.security.Signature signature = java.security.Signature
					.getInstance(SIGN_ALGORITHMS);

			signature.initSign(priKey);
			signature.update(content.getBytes(DEFAULT_CHARSET));

			byte[] signed = signature.sign();

			return Base64.encode(signed);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}
	
	/**
	 * get the sign type we use. 获取签名方式
	 * 
	 * @return
	 */
	public static String getSignType() {
		return "sign_type=\"RSA\"";
	}

	/**
	 * 
	 * @param subject
	 * @param body
	 * @param price
	 * @param orderNo
	 * @return
	 */
	public static String getOrderInfo(String subject, String body, float price,
			String orderNo, String zfbUrl) {
		MyUtils.showLog(" 准备支付 ", price + "");

		String orderInfo = "partner=" + "\"" + AliPayConstant.PARTNER + "\"";
		orderInfo += "&";
		orderInfo += "seller_id=" + "\"" + AliPayConstant.SELLER + "\"";
		orderInfo += "&";
		orderInfo += "out_trade_no=" + "\"" + orderNo + "\"";
		orderInfo += "&";
		orderInfo += "subject=" + "\"" + subject + "\"";
		orderInfo += "&";
		orderInfo += "body=" + "\"" + body + "\"";
		orderInfo += "&";
		orderInfo += "total_fee=" + "\"" + price + "\"";
		orderInfo += "&";
		orderInfo += "notify_url=" + "\"" + zfbUrl + "\"";

		// 接口名称， 定值
		orderInfo += "&service=\"mobile.securitypay.pay\"";

		// 支付类型，定值
		orderInfo += "&payment_type=\"1\"";

		// 字符集，默认utf-8
		orderInfo += "&_input_charset=\"utf-8\"";

		// 超时时间 ，默认30分钟.
		// 设置未付款交易的超时时间，一旦超时，该笔交易就会自动被关闭。
		// 取值范围：1m～15d。
		// m-分钟，h-小时，d-天，1c-当天（无论交易何时创建，都在0点关闭）。
		// 该参数数值不接受小数点，如1.5h，可转换为90m。
		// 该功能需要联系支付宝配置关闭时间。
		orderInfo += "&it_b_pay=\"30m\"";

		// 商品展示网址,客户端可不加此参数
		orderInfo += "&show_url=\"m.alipay.com\"";

		// verify(sign, orderInfo);
		MyUtils.showLog(" 准备支付 ", " 最终价格 " + price);
		MyUtils.showLog(" 准备支付 ", " 订单信息 " + orderInfo);
		return orderInfo;
	}

}
