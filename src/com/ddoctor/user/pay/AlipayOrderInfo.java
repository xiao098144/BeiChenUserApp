package com.ddoctor.user.pay;

/***
 * 
 * 支付宝 支付时所需bean
 * 
 */
public class AlipayOrderInfo {
	/** 商品名称 */
	private String subject;
	/** 商品详情 */
	private String body;
	/** 商品价格 */
	private String price;

	public AlipayOrderInfo(String subject, String body, String price) {
		super();
		this.subject = subject;
		this.body = body;
		this.price = price;
	}

	public AlipayOrderInfo() {
		super();
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}
}