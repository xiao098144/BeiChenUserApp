package com.ddoctor.user.pay;

/**
 * @filename AliPayConstant.java
 * @TODO 支付宝常量
 * @date 2014-6-22下午3:36:02
 * @Administrator 萧
 * 
 */
public class AliPayConstant {

	public static final String PARTNER = "2088411220324838";
	public static final String SELLER = "twd@tangys.net";
	public static final String RSA_PRIVATE = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAKS2U9b1qovFviA7iNaTGq4M2Xsr3cGOK0IGM1nG6xP+jyfyTfv3jn2qVD8JmWrZZnCAUUpEslFrtDqf5GFsJ/cZtKcLJWFxJFidlZXa1L1LpZAtC8dBOfZdZb2QKvA6gmMbfEYOdAs+Z/QW4xU2tw0bESTSv05eDRTbob4pWBaNAgMBAAECgYBnlufZnH2bFE1Dtk2ULcYsypYIugjoj0IzbvTitJDp7UIZbHGRUxtBe1sqnh3nrjyX6ou1esjvHh6mdCLNBpmG80mVbwpUbals+zBPC0G+vgLQ5r6qF2+GmaSo/tKgDPIDgfFQhoyweJvaSY5/2afEDr1AHejXi6E1BY9oMAf9nQJBANBIcBZqIE5yrU6xYgByxWg3bhZLeq56s3Q3nAvgBQVK2BtoTHicFuGLf0NLfbOkuog3J/YN31CkIYaRpxV++5MCQQDKcoOu49DVfeieoHavugWnT2hhvu8HPlnqW9JN9Gf4s9drzAkwMuCvMUsfhff35Bh8JfZCPmJSs6WCcB7VFzlfAkB6CfejKovDa24grc6+dIE2j0PxzEgV888ySWMlkh1e/Hujk3k7Mjo17Yaj55e/qsWhCrBfe8Fdfs4mCzHbZowBAkBXuqg18aNd4OIdW4wTOpmYl2C0evk6OlrcpE3OWkt0CvsfS5vvtxyGzuWvE28RjVbO9eRxcHdLP25JyCyUSywhAkEAnG4vDJBenFu2zUkAYPQwosvohen/yREKHM4ZOldY7lPuFFsWUjLp7Q6/ek6uWeMXZKA8mT3rqGE0NPUVtgHEdg==";
	public static final String RSA_PUBLIC = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC2nnJD5aqWuRVotcsVQE97dXs86I5wqsx0OcSNK9l1u5q+2Hl5fkVblQfM8RW6B66XZ5Wad7Gp0tSY9saIZbxjjFh2mbN2Htyla/oGjJvwPKlL9gdHrs3fFNQL2SzDhXwQte7l8Pf8fIBywkZ+bnfbZICyG0bqJQAIzJZQoe6f4QIDAQAB";

	/** 交易状态码 */
	/** 订单支付成功 */
	public static final int SUCCESS = 9000;
	/** 正在处理中 */
	public static final int PROCESSING = 8000;
	/** 订单支付失败 */
	public static final int PAYFAILED = 4000;
	/** 用户中途取消 */
	public static final int USERCANCELD = 6001;
	/** 网络连接出错 */
	public static final int NETERROR = 6002;
}
