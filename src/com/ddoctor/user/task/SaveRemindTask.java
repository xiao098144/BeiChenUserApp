package com.ddoctor.user.task;

import com.ddoctor.enums.RetError;
import com.ddoctor.user.wapi.bean.MedicalRemindBean;
import com.ddoctor.user.wapi.bean.SportRemindBean;
import com.ddoctor.utils.MedicalDBUtil;
import com.ddoctor.utils.SportRemindDBUtil;

public class SaveRemindTask extends BaseAsyncTask<String, Void, RetError>{

	private MedicalRemindBean medicalRemindBean;
	private SportRemindBean sportRemindBean;
	private int type;
	
	public SaveRemindTask(MedicalRemindBean medicalRemindBean , int type) {
		super();
		this.medicalRemindBean = medicalRemindBean;
		this.type = type; 
	}

	public SaveRemindTask(SportRemindBean sportRemindBean , int type) {
		super();
		this.sportRemindBean = sportRemindBean;
		this.type = type;
		
	}

	@Override
	protected RetError doInBackground(String... arg0) {
		RetError ret = RetError.DATABASE;
		try{
			if (type == 1) {
				boolean remind = MedicalDBUtil.getIntance().insertMedicalRemind(medicalRemindBean);
				if (remind) {
					return RetError.NONE;
				} else {
					ret.setErrorMessage("保存用药提醒失败"); 
				}
			} else if(type == 2){
				boolean remind = SportRemindDBUtil.getInstance().managerSportRemind(sportRemindBean);
				if (remind) {
					return RetError.NONE;
				} else {
					ret.setErrorMessage("保存运动提醒失败"); 
				}
			}
		}
		catch(Exception e){
		}
		
		return ret;
	}

}
