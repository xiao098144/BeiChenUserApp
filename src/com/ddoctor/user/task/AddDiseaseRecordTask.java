package com.ddoctor.user.task;

import org.json.JSONObject;

import com.ddoctor.enums.RetError;
import com.ddoctor.user.config.AppBuildConfig;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.wapi.RespBean;
import com.ddoctor.user.wapi.WAPI;
import com.ddoctor.user.wapi.bean.DiseaseBean;
import com.ddoctor.user.wapi.constant.Action;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.HttpHelper;
import com.ddoctor.utils.MyUtils;
import com.google.gson.Gson;

public class AddDiseaseRecordTask extends BaseAsyncTask<String, Void, DDResult>{

	private DiseaseBean diseaseBean;
	
	
	public AddDiseaseRecordTask(DiseaseBean diseaseBean) {
		super();
		this.diseaseBean = diseaseBean;
	}

	@Override
	protected DDResult doInBackground(String... params) {
		try {
			return addDiseaseRecord();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return DDResult.makeResult(RetError.ERROR);
	}
	
	private DDResult addDiseaseRecord(){
		try {
			Gson gson = new Gson();
			
			JSONObject jsonObject = new JSONObject();
			jsonObject.put(AppBuildConfig.ACTID, Action.DO_DISEASE);
			jsonObject.put(AppBuildConfig.PATIENTID, DataModule.getInstance().getLoginedUserId());
			jsonObject.put("disease", gson.toJson(diseaseBean));
			
			String resp_string = HttpHelper.http_post(WAPI.WAPI_BASE_URL,
					jsonObject.toString());
			if (resp_string == null) {
				return DDResult.makeResult(RetError.NETWORK_ERROR);
			}

			MyUtils.showLog(resp_string);
			
			RespBean respBean = gson.fromJson(resp_string, RespBean.class);
			if (respBean != null) {
				if (respBean.isSuccess()) {
					DDResult ret = DDResult.makeResult(RetError.NONE);
					return ret;
				} else {
					return DDResult.makeResult(RetError.API_INTERFACE,
							respBean.getErrMsg());
				}
			} else
				return DDResult.makeResult(RetError.API_INTERFACE);
			
		} catch (Exception e) {
			
		}
		return DDResult.makeResult(RetError.ERROR);
	}

}
