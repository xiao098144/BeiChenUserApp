package com.ddoctor.user.task;

import org.json.JSONObject;

import android.os.Bundle;

import com.ddoctor.enums.RetError;
import com.ddoctor.user.config.AppBuildConfig;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.wapi.RespBean;
import com.ddoctor.user.wapi.WAPI;
import com.ddoctor.user.wapi.bean.ContentBean;
import com.ddoctor.user.wapi.constant.Action;
import com.ddoctor.user.wapi.constant.UserType;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.HttpHelper;
import com.ddoctor.utils.MyUtils;
import com.google.gson.Gson;

/**
 * 获取知识库详情
 * 
 * @author 萧
 * @Date 2015-6-2下午10:34:13
 * @TODO TODO
 */
public class GetKnowledgeDetailTask extends
		BaseAsyncTask<String, Void, DDResult> {

	private int knowledgeId;

	public GetKnowledgeDetailTask(int knowledgeId) {
		super();
		this.knowledgeId = knowledgeId;
	}

	@Override
	protected DDResult doInBackground(String... params) {
		try {
			return getKnowledge();
		} catch (Exception e) {
		}

		return DDResult.makeResult(RetError.ERROR);
	}

	private DDResult getKnowledge() {
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put(AppBuildConfig.ACTID, Action.GET_KNOWLEGECONTENT);
			jsonObject.put(AppBuildConfig.PATIENTID, DataModule.getInstance()
					.getLoginedUserId());
			jsonObject.put("doctorId", 0);
			jsonObject.put("userType", UserType.PATIENT);
			jsonObject.put("contentId", knowledgeId);

			String resp_string = HttpHelper.http_post(WAPI.WAPI_BASE_URL,
					jsonObject.toString());
			if (resp_string == null) {
				return DDResult.makeResult(RetError.NETWORK_ERROR);
			}

			MyUtils.showLog(resp_string);

			Gson gson = new Gson();
			RespBean respBean = gson.fromJson(resp_string, RespBean.class);
			if (respBean == null) {
				return DDResult.makeResult(RetError.API_INTERFACE);
			}

			if (respBean.isSuccess()) {
				JSONObject rootObj = new JSONObject(resp_string);
				if (rootObj.has("knowelge")) {
					rootObj = rootObj.optJSONObject("knowelge");
					ContentBean contentBean = gson.fromJson(rootObj.toString(),
							ContentBean.class);

					DDResult ret = DDResult.makeResult(RetError.NONE);
					Bundle b = new Bundle();
					b.putParcelable("data", contentBean);
					ret.setBundle(b);
					return ret;
				}

			} else
				return DDResult.makeResult(RetError.API_INTERFACE,
						respBean.getErrMsg());

			return DDResult.makeResult(RetError.API_INTERFACE);

		} catch (Exception e) {

		}
		return DDResult.makeResult(RetError.ERROR);
	}

}
