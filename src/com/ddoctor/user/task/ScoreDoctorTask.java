package com.ddoctor.user.task;

import org.json.JSONObject;

import com.ddoctor.enums.RetError;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.wapi.RespBean;
import com.ddoctor.user.wapi.WAPI;
import com.ddoctor.user.wapi.bean.DoctorBean;
import com.ddoctor.user.wapi.constant.Action;
import com.ddoctor.utils.HttpHelper;
import com.ddoctor.utils.MyUtils;
import com.google.gson.Gson;

public class ScoreDoctorTask extends BaseAsyncTask<String, Void, RetError> {
	int _doctorId;
	int _score;

	public ScoreDoctorTask(int doctorId, int score) {
		super();
		this._doctorId = doctorId;
		this._score = score;
	}

	@Override
	protected RetError doInBackground(String... arg0) {
		RetError ret = RetError.ERROR;
		try {

			RespBean respBean = new RespBean();
			DoctorBean db = new DoctorBean();
			ret = setScore(_doctorId, _score, respBean, db);
			if (ret == RetError.NONE) {
				if (respBean.isSuccess()) {
					ret = RetError.NONE;
					ret.setObject(db);
				} else {
					ret = RetError.API_INTERFACE;
					ret.setErrorMessage(respBean.errMsg);
				}
			}
		} catch (Exception e) {
		}

		return ret;
	}

	private RetError setScore(int doctorId, int score, RespBean respBean,
			DoctorBean db) {
		try {
			Gson gson = new Gson();
			// 构建请求json串
			JSONObject json = new JSONObject();
			json.put("doctorId", doctorId);
			json.put("patientId", DataModule.getInstance().getLoginedUserId());
			json.put("score", score);
			json.put("actId", Action.SCORE_DOCTOR);

			MyUtils.showLog(json.toString());

			// 发起http请求
			String resp_string = HttpHelper.http_post(WAPI.WAPI_BASE_URL,
					json.toString());
			MyUtils.showLog(resp_string);
			if (resp_string == null)
				return RetError.NETWORK_ERROR;

			RespBean resp = gson.fromJson(resp_string, RespBean.class);
			if (resp == null)
				return RetError.API_INTERFACE;

			respBean.copyFrom(resp);
			if (respBean.isSuccess()) {
				JSONObject respObj = new JSONObject(resp_string);
				JSONObject patientObj = respObj.getJSONObject("doctor");
				DoctorBean doctor = gson.fromJson(patientObj.toString(),
						DoctorBean.class);
				if (doctor != null)
					db.copyFrom(doctor);
			}
			return RetError.NONE;
		} catch (Exception e) {

		}

		return RetError.ERROR;
	}

}
