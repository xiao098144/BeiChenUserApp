package com.ddoctor.user.task;

import org.json.JSONObject;

import android.text.TextUtils;

import com.ddoctor.enums.RetError;
import com.ddoctor.user.config.AppBuildConfig;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.wapi.RespBean;
import com.ddoctor.user.wapi.WAPI;
import com.ddoctor.user.wapi.constant.Action;
import com.ddoctor.user.wapi.constant.UserType;
import com.ddoctor.utils.HttpHelper;
import com.ddoctor.utils.MyUtils;
import com.google.gson.Gson;

/**
 * 添加收藏
 * @author 萧
 * @Date 2015-6-2下午10:34:13
 * @TODO TODO
 */
public class AddCollectionTask extends BaseAsyncTask<String, Void, RetError>{

	private int knowledgeId;

	public AddCollectionTask(int knowledgeId) {
		this.knowledgeId = knowledgeId;
	}

	@Override
	protected RetError doInBackground(String... params) {
		RetError ret = RetError.ERROR;
		try {
			RespBean respBean = new RespBean();
			ret = addCollection(respBean);
			if (ret == RetError.NONE) {
				if (respBean.isSuccess()) {
					ret = RetError.NONE;
				} else {
					ret = RetError.API_INTERFACE;
					ret.setErrorMessage(respBean.errMsg);
				}
			}
		} catch (Exception e) {
		}

		return ret;
	}

	private RetError addCollection(RespBean respBean) {
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put(AppBuildConfig.ACTID, Action.ADD_COLLECTION);
			jsonObject.put(AppBuildConfig.PATIENTID, DataModule.getInstance()
					.getLoginedUserId());
			jsonObject.put("doctorId", 0);
			jsonObject.put("userType", UserType.PATIENT);
			jsonObject.put("knowledgeId", knowledgeId);

			MyUtils.showLog("添加收藏  "+jsonObject.toString());
			
			String resp_string = HttpHelper.http_post(WAPI.WAPI_BASE_URL,
					jsonObject.toString());
			if (TextUtils.isEmpty(resp_string)) {
				return RetError.NETWORK_ERROR;
			}
			Gson gson = new Gson();
			MyUtils.showLog("添加收藏  " + resp_string);
			RespBean resp = gson.fromJson(resp_string, RespBean.class);
			if (resp == null) {
				return RetError.API_INTERFACE;
			}
			respBean.copyFrom(resp);
			if (respBean.isSuccess()) {
				return RetError.NONE;
			} else {
				RetError.SERVER_ERROR.setErrorMessage(respBean.getErrMsg());
				return RetError.SERVER_ERROR;
			}

		} catch (Exception e) {

		}
		return RetError.ERROR;
	}
	
}
