package com.ddoctor.user.task;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.os.Bundle;

import com.ddoctor.enums.RetError;
import com.ddoctor.user.config.AppBuildConfig;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.wapi.RespBean;
import com.ddoctor.user.wapi.WAPI;
import com.ddoctor.user.wapi.bean.DeliverBean;
import com.ddoctor.user.wapi.constant.Action;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.HttpHelper;
import com.ddoctor.utils.MyUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class GetShopAddressListTask extends BaseAsyncTask<String, Void, DDResult>{

	public GetShopAddressListTask() {
		super();
	}

	@Override
	protected DDResult doInBackground(String... params) {
		try{
			
			return do_get_deliver_list_request();
		}
		catch(Exception e){
		}
		
		return DDResult.makeResult(RetError.ERROR);
	}

	private DDResult do_get_deliver_list_request(){
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put(AppBuildConfig.ACTID, Action.GET_DELIVER_LIST);
			jsonObject.put(AppBuildConfig.PATIENTID, DataModule.getInstance().getLoginedUserId());
			
			MyUtils.showLog(" 获取地址列表  "+jsonObject.toString());
			
			String resp_string = HttpHelper.http_post(WAPI.WAPI_BASE_URL, jsonObject.toString());
			if (resp_string == null) {
				return DDResult.makeResult(RetError.NETWORK_ERROR);
			}
  
			MyUtils.showLog(resp_string);
			Gson gson = new Gson();
			RespBean respBean = gson.fromJson(resp_string, RespBean.class);
			if (respBean == null) {
				return DDResult.makeResult(RetError.API_INTERFACE);
			}

			if (respBean.isSuccess()) 
			{
				ArrayList<DeliverBean> dataList = new ArrayList<DeliverBean>();
				
				JSONObject respObj = new JSONObject(resp_string);
				if( respObj.has("recordList") )
				{
					JSONArray listObj = respObj.getJSONArray("recordList");
					if (listObj != null) 
					{
						Type type = new TypeToken<ArrayList<DeliverBean>>() {}.getType();
						
						List<DeliverBean> list = gson.fromJson(listObj.toString(),type);
						if( null != list && list.size() > 0) 
						{
							dataList.addAll(list);
						}
					}
				}
				
				
				DDResult ret = DDResult.makeResult(RetError.NONE);
				Bundle data = new Bundle();
				data.putSerializable("list", dataList);
				ret.setBundle(data);
				ret.setErrorMessage(respBean.getErrMsg());
				return ret;
			}
			else 
			{
				return DDResult.makeResult(RetError.API_INTERFACE, respBean.getErrMsg());
			}
			
		} catch (Exception e) {

		}
		return DDResult.makeResult(RetError.ERROR);
	}
	
}
