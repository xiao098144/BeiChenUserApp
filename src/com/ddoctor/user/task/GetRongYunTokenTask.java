package com.ddoctor.user.task;

import org.json.JSONObject;

import com.ddoctor.enums.RetError;
import com.ddoctor.user.config.AppBuildConfig;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.wapi.RespBean;
import com.ddoctor.user.wapi.WAPI;
import com.ddoctor.user.wapi.constant.Action;
import com.ddoctor.user.wapi.constant.UserType;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.HttpHelper;
import com.ddoctor.utils.MyUtils;
import com.google.gson.Gson;

/**
 * Date:2015-6-28上午1:48:02 
 * Author:Administrator 
 * TODO:需确认字段  测试
 */
public class GetRongYunTokenTask extends BaseAsyncTask<String, Void, DDResult> {

	@Override
	protected DDResult doInBackground(String... params) {
		try {
			return getRongYunToken();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return DDResult.makeResult(RetError.ERROR);
	}

	private DDResult getRongYunToken() {
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put(AppBuildConfig.ACTID, Action.GET_RONGYUN);
			jsonObject.put(AppBuildConfig.PATIENTID, DataModule.getInstance()
					.getLoginedUserId());
			jsonObject.put("userType", UserType.PATIENT);
			jsonObject.put("doctorId", 0);

			String resp_string = HttpHelper.http_post(WAPI.WAPI_BASE_URL,
					jsonObject.toString());
			if (resp_string == null)
				return DDResult.makeResult(RetError.NETWORK_ERROR);

			MyUtils.showLog(resp_string);

			Gson gson = new Gson();
			RespBean respBean = gson.fromJson(resp_string, RespBean.class);
			if (respBean != null && respBean.isSuccess()) {
				JSONObject respObject = new JSONObject(resp_string);
				if (respObject.has("token")) {
					DataModule.getInstance().saveRYToken(respObject.optString("token"));
					DataModule.getInstance().saveRYUserId(respObject.optString("userId"));
					DDResult ret = DDResult.makeResult(RetError.NONE);
					return ret;
				}
			} else {
				if (respBean != null)
					return DDResult.makeResult(RetError.API_INTERFACE,
							respBean.getErrMsg());
				else
					return DDResult.makeResult(RetError.API_INTERFACE);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return DDResult.makeResult(RetError.ERROR);
	}
}
