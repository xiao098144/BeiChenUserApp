package com.ddoctor.user.task;

import org.json.JSONObject;

import com.ddoctor.enums.RetError;
import com.ddoctor.user.config.AppBuildConfig;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.wapi.RespBean;
import com.ddoctor.user.wapi.WAPI;
import com.ddoctor.user.wapi.bean.DeliverBean;
import com.ddoctor.user.wapi.constant.Action;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.HttpHelper;
import com.ddoctor.utils.MyUtils;
import com.google.gson.Gson;

public class AddShopAddressTask extends BaseAsyncTask<String, Void, DDResult>{
	private DeliverBean _deliverBean = null;
	
	public AddShopAddressTask(DeliverBean deliverBean) {
		super();
		
		_deliverBean = deliverBean;
	}

	@Override
	protected DDResult doInBackground(String... params) {
		try{
			return do_add_deliver_request(_deliverBean);
		}
		catch(Exception e){
		}
		
		return DDResult.makeResult(RetError.ERROR);
	}

	private DDResult do_add_deliver_request(DeliverBean deliverBean){
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put(AppBuildConfig.ACTID, Action.DO_DELIVER);
			jsonObject.put(AppBuildConfig.PATIENTID, DataModule.getInstance().getLoginedUserId());
			
			
			JSONObject jo = WAPI.beanToJSONObject(deliverBean);
			if( jo != null )
				jsonObject.put("deliver", jo);
			
			MyUtils.showLog(jsonObject.toString());
			
			String resp_string = HttpHelper.http_post(WAPI.WAPI_BASE_URL, jsonObject.toString());
			if (resp_string == null) {
				return DDResult.makeResult(RetError.NETWORK_ERROR);
			}

			MyUtils.showLog(resp_string);
			
			Gson gson = new Gson();
			RespBean respBean = gson.fromJson(resp_string, RespBean.class);
			if (respBean == null) {
				return DDResult.makeResult(RetError.API_INTERFACE);
			}

			if (respBean.isSuccess()) 
			{
				JSONObject respObj = new JSONObject(resp_string);
				if (respObj.has("id")) {
					int id = respObj.optInt("id");
					DDResult ret = DDResult.makeResult(RetError.NONE);
					ret.setObject(id);
					return ret; 
				}
			}
			else 
			{
				return DDResult.makeResult(RetError.API_INTERFACE, respBean.getErrMsg());
			}
			
		} catch (Exception e) {

		}
		return DDResult.makeResult(RetError.ERROR);
	}
	
}
