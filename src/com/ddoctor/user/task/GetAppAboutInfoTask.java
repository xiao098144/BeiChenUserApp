package com.ddoctor.user.task;

import org.json.JSONObject;

import android.os.Bundle;

import com.ddoctor.enums.RetError;
import com.ddoctor.user.config.AppBuildConfig;
import com.ddoctor.user.wapi.RespBean;
import com.ddoctor.user.wapi.WAPI;
import com.ddoctor.user.wapi.bean.AboutBean;
import com.ddoctor.user.wapi.constant.Action;
import com.ddoctor.user.wapi.constant.UserType;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.HttpHelper;
import com.ddoctor.utils.MyUtils;
import com.google.gson.Gson;

public class GetAppAboutInfoTask extends BaseAsyncTask<String, Void, DDResult> {

	@Override
	protected DDResult doInBackground(String... params) {
		try {
			return getAppAboutInfo();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return DDResult.makeResult(RetError.ERROR);
	}

	private DDResult getAppAboutInfo() {
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put(AppBuildConfig.ACTID, Action.GET_CLIENT_ABOUT);
			jsonObject.put("userType", UserType.PATIENT);
			String resp_string = HttpHelper.http_post(WAPI.WAPI_BASE_URL,
					jsonObject.toString());
			if (resp_string == null) {
				return DDResult.makeResult(RetError.NETWORK_ERROR);
			}

			MyUtils.showLog(resp_string);

			Gson gson = new Gson();
			RespBean respBean = gson.fromJson(resp_string, RespBean.class);
			if (respBean == null) {
				return DDResult.makeResult(RetError.API_INTERFACE);
			}

			if (respBean.isSuccess()) {
				JSONObject rootObj = new JSONObject(resp_string);
				if (rootObj.has("about")) {
					rootObj = rootObj.optJSONObject("about");
					AboutBean aboutBean = gson.fromJson(rootObj.toString(),
							AboutBean.class);

					DDResult ret = DDResult.makeResult(RetError.NONE);
					Bundle b = new Bundle();
					b.putSerializable("about", aboutBean);
					ret.setBundle(b);
					return ret;
				}

			} else
				return DDResult.makeResult(RetError.API_INTERFACE,
						respBean.getErrMsg());

			return DDResult.makeResult(RetError.API_INTERFACE);

		} catch (Exception e) {

		}
		return DDResult.makeResult(RetError.ERROR);
	}

}
