package com.ddoctor.user.task;

import org.json.JSONObject;

import com.ddoctor.enums.RetError;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.wapi.RespBean;
import com.ddoctor.user.wapi.WAPI;
import com.ddoctor.user.wapi.constant.Action;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.HttpHelper;
import com.ddoctor.utils.MyUtils;
import com.google.gson.Gson;


public class TSLSendVCodeTask extends BaseAsyncTask<String, Void, DDResult> {
	public TSLSendVCodeTask()
	{
	}

	@Override
	protected DDResult doInBackground(String... params) {
		try{
			return do_sendvcode();
		}
		catch(Exception e){
		}
		
		return DDResult.makeResult(RetError.ERROR);
	}
	
	// 登录接口
	public  DDResult do_sendvcode()
	{
		try{
			// 构建请求json串
			JSONObject json = new JSONObject();
			json.put("patientId", DataModule.getInstance().getLoginedUserId());
			json.put("actId", Action.SEND_TSL_VERIFICATIONCODE);
			
			MyUtils.showLog(json.toString());
			
			// 发起http请求
			String resp_string = HttpHelper.http_post(WAPI.WAPI_TSL_URL, json.toString(), -1, 30 * 1000);
			if( resp_string == null )
				return DDResult.makeResult(RetError.API_INTERFACE);
			
			MyUtils.showLog(resp_string);
			// 接口调用成功，解析返回结果
			Gson gson = new Gson();
			RespBean resp = gson.fromJson(resp_string, RespBean.class);
			if( resp == null )
				return DDResult.makeResult(RetError.API_INTERFACE);
			
			
			if( resp.isSuccess() )
			{
				JSONObject respObj = new JSONObject(resp_string);
				String vcode = respObj.optString("verificationCode");
				
				DDResult ret = DDResult.makeResult(RetError.NONE);
				ret.setObject(vcode);
				
				return ret;
			}
			
			return DDResult.makeResult(RetError.API_INTERFACE, resp.getErrMsg());
		}
		catch(Exception e)
		{
			
		}
		
		return DDResult.makeResult(RetError.ERROR);
	}

	
}
