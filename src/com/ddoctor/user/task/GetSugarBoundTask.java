package com.ddoctor.user.task;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.ddoctor.enums.RetError;
import com.ddoctor.user.config.AppBuildConfig;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.data.MyProfile;
import com.ddoctor.user.wapi.RespBean;
import com.ddoctor.user.wapi.WAPI;
import com.ddoctor.user.wapi.bean.UplowValueBean;
import com.ddoctor.user.wapi.constant.Action;
import com.ddoctor.utils.HttpHelper;
import com.ddoctor.utils.MyUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * 获取血糖上下限
 * @author 萧
 * @Date 2015-6-2下午10:34:13
 * @TODO TODO
 */
public class GetSugarBoundTask extends BaseAsyncTask<String, Void, RetError>{

	@Override
	protected RetError doInBackground(String... params) {
		RetError ret = RetError.NONE;
		try{
			RespBean respBean = new RespBean();
			ret = getSugarBound(respBean);
			if( ret == RetError.NONE )
			{
				if( respBean.isSuccess() )
				{
					ret = RetError.NONE;
				}
				else
				{
					ret = RetError.API_INTERFACE;
					ret.setErrorMessage(respBean.errMsg);
				}
			}
		}
		catch(Exception e){
		}
		
		return ret;
	}
	
	private RetError getSugarBound(RespBean respBean){
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put(AppBuildConfig.ACTID, Action.GET_UPLOWVALUE);
			jsonObject.put(AppBuildConfig.PATIENTID, DataModule.getInstance().getLoginedUserId());
			String resp_string = HttpHelper.http_post(WAPI.WAPI_BASE_URL, jsonObject.toString());
			if (resp_string == null) {
				return RetError.NETWORK_ERROR;
			}
			MyUtils.showLog("", "获取血糖上下限 "+resp_string);
			Gson gson = new Gson();
			RespBean resp = gson.fromJson(resp_string, RespBean.class);
			if (resp == null) {
				return RetError.API_INTERFACE;
			}
			respBean.copyFrom(resp);
			if (respBean.isSuccess()) {
				JSONObject respObj = new JSONObject(resp_string);
				JSONArray listObj = respObj.getJSONArray("uplowList");
				if (listObj!=null) {
					Type type = new TypeToken<ArrayList<UplowValueBean>>() {
					}.getType();
					List<UplowValueBean> list = gson.fromJson(listObj.toString(),
							type);
					if (null != list && list.size() > 0) {
						MyProfile.getInstance().setUpLowValue(list);
					}
				}
				return RetError.NONE;
			}
			
		} catch (Exception e) {
			
		}
		
		return RetError.ERROR;
	}
	
	
}
