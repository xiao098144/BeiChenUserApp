package com.ddoctor.user.task;

import java.util.List;

import org.json.JSONObject;

import android.text.TextUtils;

import com.ddoctor.enums.RetError;
import com.ddoctor.user.config.AppBuildConfig;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.wapi.RespBean;
import com.ddoctor.user.wapi.WAPI;
import com.ddoctor.user.wapi.bean.MedicalRecordBean;
import com.ddoctor.user.wapi.constant.Action;
import com.ddoctor.utils.HttpHelper;
import com.ddoctor.utils.MedicalDBUtil;
import com.ddoctor.utils.MyUtils;
import com.google.gson.Gson;

public class AddMedicalRecordTask extends BaseAsyncTask<String, Void, RetError> {

	private List<MedicalRecordBean> dataList;

	public AddMedicalRecordTask(List<MedicalRecordBean> dataList) {
		this.dataList = dataList;
	}

	@Override
	protected RetError doInBackground(String... params) {
		RetError ret = RetError.NONE;
		try {
			RespBean respBean = new RespBean();
			ret = managerMedicalRecord(respBean);
			if (ret == RetError.NONE) {
				if (respBean.isSuccess()) {
					MedicalDBUtil.getIntance().insertMedicalHistory(dataList);
					ret = RetError.NONE;

				} else {
					ret = RetError.API_INTERFACE;
					ret.setErrorMessage(respBean.errMsg);
				}
			}
		} catch (Exception e) {
		}

		return ret;
	}

	private RetError managerMedicalRecord(RespBean respBean) {
		try {
			Gson gson = new Gson();

			JSONObject jsonObject = new JSONObject();
			jsonObject.put(AppBuildConfig.ACTID, Action.DO_MEDICALRECORD);
			jsonObject.put(AppBuildConfig.PATIENTID, DataModule.getInstance()
					.getLoginedUserId());
			jsonObject.put("medical", gson.toJson(dataList));

			String resp_string = HttpHelper.http_post(WAPI.WAPI_BASE_URL,
					jsonObject.toString());
			
			MyUtils.showLog("", "添加用药记录  返回数据  "+resp_string);
			
			if (TextUtils.isEmpty(resp_string)) {
				return RetError.NETWORK_ERROR;
			}
			RespBean resp = gson.fromJson(resp_string, RespBean.class);
			if (resp == null) {
				return RetError.API_INTERFACE;
			}
			respBean.copyFrom(resp);
			if (respBean.isSuccess()) {
				return RetError.NONE;
			} else {
				RetError.SERVER_ERROR.setErrorMessage(respBean.getErrMsg());
				return RetError.SERVER_ERROR;
			}
		} catch (Exception e) {

		}
		return RetError.ERROR;
	}

}
