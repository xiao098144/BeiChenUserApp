package com.ddoctor.user.task;

import org.json.JSONObject;

import android.content.Context;

import com.ddoctor.application.MyApplication;
import com.ddoctor.enums.RetError;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.wapi.RespBean;
import com.ddoctor.user.wapi.WAPI;
import com.ddoctor.user.wapi.bean.OrderBean;
import com.ddoctor.user.wapi.constant.Action;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.HttpHelper;
import com.ddoctor.utils.MyUtils;
import com.google.gson.Gson;


public class GetOrderDetailTask extends BaseAsyncTask<String, Void, DDResult> {

	private int _orderId;
	public GetOrderDetailTask(int orderId)
	{
		_orderId = orderId;
	}
	
	@Override
	protected DDResult doInBackground(String... params) {
		try{
			return do_get_order_detail(_orderId);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		return DDResult.makeResult(RetError.ERROR);
	}
	
	
	public DDResult do_get_order_detail(int orderId)
	{
		Context ctx = MyApplication.getInstance().getApplicationContext();
		
		try{
			JSONObject json = new JSONObject();
			json.put("actId", Action.GET_ORDER);
			
			if( DataModule.getInstance().isLogined() )
				json.put("patientId", DataModule.getInstance().getLoginedUserId());

			json.put("orderId", orderId);
			
			MyUtils.showLog(json.toString());
			
			// 发起http请求
			String resp_string = HttpHelper.http_post(WAPI.WAPI_BASE_URL, json.toString());
			if( resp_string == null )
				return DDResult.makeResult(RetError.NETWORK_ERROR);
			
			MyUtils.showLog(resp_string);
			
			Gson gson = new Gson();
			RespBean respBean = gson.fromJson(resp_string, RespBean.class);
			if( respBean != null && respBean.isSuccess() )
			{
				JSONObject respObject = new JSONObject(resp_string);
				if( respObject.has("order") )
				{
					JSONObject orderObject = respObject.getJSONObject("order");
					
					OrderBean orderBean  = gson.fromJson(orderObject.toString(), OrderBean.class);
					
					DDResult ret = DDResult.makeResult(RetError.NONE);
					ret.setObject(orderBean);
					
					return ret;
				}
			}
			else
			{
				if( respBean != null )
					return DDResult.makeResult(RetError.API_INTERFACE, respBean.getErrMsg());
				else
					return DDResult.makeResult(RetError.API_INTERFACE);
			}
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return DDResult.makeResult(RetError.ERROR);
	}
}
