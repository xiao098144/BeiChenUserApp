package com.ddoctor.user.task;


import org.json.JSONObject;

import android.os.Bundle;

import com.ddoctor.application.MyApplication;
import com.ddoctor.enums.RetError;
import com.ddoctor.user.config.AppBuildConfig;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.wapi.RespBean;
import com.ddoctor.user.wapi.WAPI;
import com.ddoctor.user.wapi.bean.UploadBean;
import com.ddoctor.user.wapi.constant.Action;
import com.ddoctor.user.wapi.constant.UserType;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.HttpHelper;
import com.ddoctor.utils.MyUtils;
import com.google.gson.Gson;

public class FileUploadTask extends BaseAsyncTask<String, Void, DDResult>{
	
	private UploadBean _uploadBean = null;

	public FileUploadTask(UploadBean uploadBean) {
		super();
		_uploadBean = uploadBean;
	}

	@Override
	protected DDResult doInBackground(String... params) {
		try{
			
			return do_file_upload_request(_uploadBean);
		}
		catch(Exception e){
		}
		
		return DDResult.makeResult(RetError.ERROR);
	}

	private DDResult do_file_upload_request(UploadBean uploadBean){
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put(AppBuildConfig.ACTID, Action.UPLOAD_FILE);
			jsonObject.put(AppBuildConfig.PATIENTID, DataModule.getInstance().getLoginedUserId());
			jsonObject.put("userType", UserType.PATIENT);
			
			
			MyUtils.showMemory(MyApplication.getInstance());
			JSONObject uploadObject = WAPI.beanToJSONObject(uploadBean);
			jsonObject.put("upload", uploadObject);
			
			
			String resp_string = HttpHelper.http_post(WAPI.WAPI_BASE_URL, jsonObject.toString());
			if (resp_string == null) {
				return DDResult.makeResult(RetError.NETWORK_ERROR);
			}

			MyUtils.showLog(resp_string);
			
			Gson gson = new Gson();
			RespBean respBean = gson.fromJson(resp_string, RespBean.class);
			if (respBean == null) {
				return DDResult.makeResult(RetError.API_INTERFACE);
			}

			if (respBean.isSuccess()) 
			{
				JSONObject rootObj = new JSONObject(resp_string);
				if( rootObj.has("fileUrl") )
				{
						DDResult ret = DDResult.makeResult(RetError.NONE);
						Bundle b = new Bundle();
						b.putString("fileUrl", rootObj.optString("fileUrl"));
						ret.setBundle(b);
						return ret;
				}
				
			}
			else
				return DDResult.makeResult(RetError.API_INTERFACE, respBean.getErrMsg());
			
			return DDResult.makeResult(RetError.API_INTERFACE);
			
		} catch (Exception e) {

		}
		return DDResult.makeResult(RetError.ERROR);
	}
	
}
