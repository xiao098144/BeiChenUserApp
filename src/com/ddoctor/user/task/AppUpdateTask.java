package com.ddoctor.user.task;

import org.json.JSONObject;

import android.content.Context;
import android.os.Bundle;

import com.beichen.user.R;
import com.ddoctor.application.MyApplication;
import com.ddoctor.enums.RetError;
import com.ddoctor.user.config.AppBuildConfig;
import com.ddoctor.user.wapi.RespBean;
import com.ddoctor.user.wapi.WAPI;
import com.ddoctor.user.wapi.bean.ClientUpdateBean;
import com.ddoctor.user.wapi.constant.Action;
import com.ddoctor.user.wapi.constant.UserType;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.HttpHelper;
import com.ddoctor.utils.MyUtils;
import com.google.gson.Gson;

public class AppUpdateTask extends BaseAsyncTask<String, Void, DDResult> {

	@Override
	protected DDResult doInBackground(String... params) {
		try {
			return appUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return DDResult.makeResult(RetError.ERROR);
	}

	private DDResult appUpdate() {
		try {
			Context context = MyApplication.getInstance().getApplicationContext();
			JSONObject json = new JSONObject();
			json.put(AppBuildConfig.ACTID, Action.GET_CLIENT_UPDATE);
			json.put("version", MyUtils.getVersionName(context));
			json.put("versionCode", MyUtils.getVersionCode(context));
			json.put("channel", context.getString(R.string.channelID));
			json.put("userType", UserType.PATIENT);
			MyUtils.showLog(json.toString());

			// 发起http请求
			String resp_string = HttpHelper.http_post(WAPI.WAPI_BASE_URL,
					json.toString());
			if (resp_string == null)
				return DDResult.makeResult(RetError.NETWORK_ERROR);

			MyUtils.showLog(resp_string);

			Gson gson = new Gson();
			RespBean respBean = gson.fromJson(resp_string, RespBean.class);
			if (respBean != null) {
				if (respBean.isSuccess()) {
					DDResult ret = DDResult.makeResult(RetError.NONE);
					JSONObject respObj = new JSONObject(resp_string);
					if (respObj.has("update")) {
						respObj = respObj.getJSONObject("update");
						ClientUpdateBean clientUpdateBean = gson.fromJson(respObj.toString(), ClientUpdateBean.class);
						Bundle data = new Bundle();
						data.putSerializable("update", clientUpdateBean);
						ret.setBundle(data);
					}
					return ret;
				} else {
					return DDResult.makeResult(RetError.API_INTERFACE,
							respBean.getErrMsg());
				}
			} else
				return DDResult.makeResult(RetError.API_INTERFACE);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return DDResult.makeResult(RetError.ERROR);
	}

}
