package com.ddoctor.user.task;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.ddoctor.enums.RetError;
import com.ddoctor.user.config.AppBuildConfig;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.wapi.RespBean;
import com.ddoctor.user.wapi.WAPI;
import com.ddoctor.user.wapi.bean.TslOrderBean;
import com.ddoctor.user.wapi.constant.Action;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.HttpHelper;
import com.ddoctor.utils.MyUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class TSLGetOrderListTask extends BaseAsyncTask<String, Void, DDResult>{

	private int page ;
	public int wsec = 0;
	
	public TSLGetOrderListTask(int page) {
		super();
		this.page = page;
	}

	@Override
	protected DDResult doInBackground(String... params) {
		try{
			return do_request();
		}
		catch(Exception e){
		}
		
		return DDResult.makeResult(RetError.ERROR, null);
	}

	private DDResult do_request(){
		
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put(AppBuildConfig.ACTID, Action.GET_TSL_ORDER_LIST);
			jsonObject.put(AppBuildConfig.PATIENTID, DataModule.getInstance().getLoginedUserId());
			jsonObject.put("page", page);
			
			String resp_string = HttpHelper.http_post(WAPI.WAPI_TSL_URL, jsonObject.toString(), -1, 30 * 1000);
			if (resp_string == null) {
				return DDResult.makeResult(RetError.NETWORK_ERROR);
			}

			MyUtils.showLog(resp_string);
			Gson gson = new Gson();
			RespBean respBean = gson.fromJson(resp_string, RespBean.class);
			if (respBean == null) {
				return DDResult.makeResult(RetError.API_INTERFACE);
			}

			if( respBean.isSuccess() ) 
			{
				ArrayList<TslOrderBean> dataList = new ArrayList<TslOrderBean>();
				
				JSONObject respObj = new JSONObject(resp_string);
				if( respObj.has("tslOrderList") )
				{
					JSONArray listObj = respObj.getJSONArray("tslOrderList");
					if( listObj != null ) 
					{
						Type type = new TypeToken<ArrayList<TslOrderBean>>() {}.getType();
						try{
							List<TslOrderBean> list = gson.fromJson(listObj.toString(), type);
							if (null != list ) 
							{
								dataList.addAll(list);
							}
						}
						catch(Exception e){
							e.printStackTrace();
							return DDResult.makeResult(RetError.API_INTERFACE);
						}
					}
				}

				DDResult ret = DDResult.makeResult(RetError.NONE);
				ret.setObject(dataList);
				return ret;
			}
			else 
			{
				return DDResult.makeResult(RetError.API_INTERFACE, respBean.getErrMsg());
			}
			
		} 
		catch (Exception e) {

		}
		
		return DDResult.makeResult(RetError.ERROR);
	}
	
}
