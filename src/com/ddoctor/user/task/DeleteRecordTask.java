package com.ddoctor.user.task;

import org.json.JSONObject;

import android.text.TextUtils;

import com.ddoctor.enums.RetError;
import com.ddoctor.user.config.AppBuildConfig;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.wapi.RespBean;
import com.ddoctor.user.wapi.WAPI;
import com.ddoctor.user.wapi.constant.Action;
import com.ddoctor.user.wapi.constant.UserType;
import com.ddoctor.utils.HttpHelper;
import com.ddoctor.utils.MyUtils;
import com.google.gson.Gson;

/**
 * 删除记录
 * @author 萧
 * @Date 2015-6-4下午2:12:34
 * @TODO TODO
 */
public class DeleteRecordTask extends BaseAsyncTask<String, Void, RetError> {

	private int recordId;
	private int type;
	private String _recordIds = "";
	
	// 删除单个记录
	public DeleteRecordTask(int recordId, int type) {
		this.recordId = recordId;
		this.type = type;
	}
	
	// 批量删除记录: recordIds格式: 1,2,3,1012,12,2323
	public DeleteRecordTask(String recordIds, int type) {
		_recordIds = recordIds;
		this.type = type;
	}

	@Override
	protected RetError doInBackground(String... params) {
		RetError ret = RetError.NONE;
		try {
			if( _recordIds.length() > 0 )
				return deleteRecords(_recordIds);
			else
				return deleteRecord(recordId);
		} catch (Exception e) {
		}

		return ret;
	}
	
	private RetError deleteRecord(int recordId){
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put(AppBuildConfig.ACTID, Action.DELETE_RECORD);
			jsonObject.put(AppBuildConfig.PATIENTID , DataModule.getInstance().getLoginedUserId());
			jsonObject.put("doctorId", 0);
			jsonObject.put("recordId", recordId);
			jsonObject.put("userType", UserType.PATIENT);
			jsonObject.put("type", type);
			
			MyUtils.showLog("删除记录 提交的数据   "+jsonObject.toString());
			String resp_string = HttpHelper.http_post(WAPI.WAPI_BASE_URL, jsonObject.toString());
			MyUtils.showLog("", "删除记录返回数据  "+resp_string);
			if (TextUtils.isEmpty(resp_string)) {
				return RetError.NETWORK_ERROR;
			}
			RespBean resp = new Gson().fromJson(resp_string, RespBean.class);
			if (resp == null) {
				return RetError.API_INTERFACE;
			}
			if (resp.isSuccess()) 
			{
				return RetError.NONE;
			}
			else
			{
				RetError ret = RetError.API_INTERFACE;//.SERVER_ERROR.setErrorMessage(respBean.getErrMsg());
				ret.setErrorMessage(resp.getErrMsg());
				return ret;
			}
		} catch (Exception e) {
			
		}
		return RetError.ERROR;
	}


	private RetError deleteRecords(String recordIds){
		try {
			
			JSONObject jsonObject = new JSONObject();
			jsonObject.put(AppBuildConfig.ACTID, Action.DELETE_RECORDS);
			jsonObject.put(AppBuildConfig.PATIENTID , DataModule.getInstance().getLoginedUserId());
			jsonObject.put("doctorId", 0);
			jsonObject.put("recordId", recordIds);
			jsonObject.put("userType", UserType.PATIENT);
			jsonObject.put("type", type);
			MyUtils.showLog("批量删除   "+jsonObject.toString());
			String resp_string = HttpHelper.http_post(WAPI.WAPI_BASE_URL, jsonObject.toString());
			MyUtils.showLog("", "批量删除记录返回数据  "+resp_string);
			if (TextUtils.isEmpty(resp_string)) {
				return RetError.NETWORK_ERROR;
			}
			
			RespBean resp = new Gson().fromJson(resp_string, RespBean.class);
			if (resp == null) {
				return RetError.API_INTERFACE;
			}
			
			if (resp.isSuccess()) {
				return RetError.NONE;
			} else {
				
				RetError ret = RetError.API_INTERFACE;//.SERVER_ERROR.setErrorMessage(respBean.getErrMsg());
				ret.setErrorMessage(resp.getErrMsg());
				return ret;
			}
		} catch (Exception e) {
			
		}
		return RetError.ERROR;
	}
	
	
}
