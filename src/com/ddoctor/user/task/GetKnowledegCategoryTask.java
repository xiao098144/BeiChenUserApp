package com.ddoctor.user.task;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.os.Bundle;

import com.ddoctor.enums.RetError;
import com.ddoctor.user.config.AppBuildConfig;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.wapi.RespBean;
import com.ddoctor.user.wapi.WAPI;
import com.ddoctor.user.wapi.bean.CatagoryBean;
import com.ddoctor.user.wapi.constant.Action;
import com.ddoctor.user.wapi.constant.UserType;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.HttpHelper;
import com.ddoctor.utils.MyUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * 获取知识库目录
 * @author 萧
 * @Date 2015-6-2下午10:34:13
 * @TODO TODO
 */
public class GetKnowledegCategoryTask extends BaseAsyncTask<String, Void, DDResult>{

	@Override
	protected DDResult doInBackground(String... params) {
		try{
			return getKnowledegCatagory();
		}
		catch(Exception e){
		}
		
		return DDResult.makeResult(RetError.ERROR);
	}
	
	private DDResult getKnowledegCatagory(){
		try {
			JSONObject json = new JSONObject();
			json.put(AppBuildConfig.ACTID, Action.GET_KNOWLEGE_CATAGORY);
			json.put("userType", UserType.PATIENT);
			json.put(AppBuildConfig.PATIENTID, DataModule.getInstance().getLoginedUserId());
			json.put("doctorId", 0);
			
			MyUtils.showLog(" 获取知识库目录 "+json.toString());

			// 发起http请求
			String resp_string = HttpHelper.http_post(WAPI.WAPI_BASE_URL,
					json.toString());

			if (resp_string == null) {
				return DDResult.makeResult(RetError.NETWORK_ERROR);
			}

			MyUtils.showLog(resp_string);
			Gson gson = new Gson();
			RespBean respBean = gson.fromJson(resp_string, RespBean.class);
			if (respBean == null) {
				return DDResult.makeResult(RetError.API_INTERFACE);
			}

			if (respBean.isSuccess()) {
				DDResult ret = DDResult.makeResult(RetError.NONE);
				ArrayList<CatagoryBean> dataList = new ArrayList<CatagoryBean>();

				JSONObject respObj = new JSONObject(resp_string);
				if (respObj.has("recordList")) {
					JSONArray listObj = respObj.getJSONArray("recordList");
					if (listObj != null) {
						Type type = new TypeToken<ArrayList<CatagoryBean>>() {
						}.getType();
						List<CatagoryBean> list = gson.fromJson(
								listObj.toString(), type);
						if (null != list) {
							dataList.addAll(list);
						}
					}
				}

				Bundle data = new Bundle();
				data.putParcelableArrayList("list", dataList);
				ret.setBundle(data);
				ret.setErrorMessage(respBean.getErrMsg());
				return ret;
			} else {
				return DDResult.makeResult(RetError.API_INTERFACE,
						respBean.getErrMsg());
			}

		} catch (Exception e) {

		}

		return DDResult.makeResult(RetError.ERROR);
	}

}

