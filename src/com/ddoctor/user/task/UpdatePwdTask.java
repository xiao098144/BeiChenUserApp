package com.ddoctor.user.task;

import org.json.JSONObject;

import com.ddoctor.enums.RetError;
import com.ddoctor.user.config.AppBuildConfig;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.wapi.RespBean;
import com.ddoctor.user.wapi.WAPI;
import com.ddoctor.user.wapi.bean.PatientBean;
import com.ddoctor.user.wapi.constant.Action;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.HttpHelper;
import com.ddoctor.utils.MyUtils;
import com.google.gson.Gson;

public class UpdatePwdTask extends BaseAsyncTask<String, Void, DDResult> {

	private String _mobile;
	private String _password;
	private String _verify;

	public UpdatePwdTask(String _mobile, String _password, String _verify) {
		super();
		this._mobile = _mobile;
		this._password = _password;
		this._verify = _verify;
	}

	@Override
	protected DDResult doInBackground(String... params) {
		try {
			return updatePwd();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return DDResult.makeResult(RetError.ERROR);
	}

	private DDResult updatePwd() {
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject
					.put(AppBuildConfig.ACTID, Action.PATIENT_UPDATE_PASSWORD);
			jsonObject.put("mobile", _mobile);
			jsonObject.put("password", _password);
			jsonObject.put("verify", _verify);

			String resp_string = HttpHelper.http_post(WAPI.WAPI_BASE_URL,
					jsonObject.toString());
			if (resp_string == null) {
				return DDResult.makeResult(RetError.NETWORK_ERROR);
			}

			MyUtils.showLog(resp_string);

			Gson gson = new Gson();
			RespBean respBean = gson.fromJson(resp_string, RespBean.class);
			if (respBean == null) {
				return DDResult.makeResult(RetError.API_INTERFACE);
			}

			if (respBean.isSuccess()) {
				JSONObject rootObj = new JSONObject(resp_string);
				if (rootObj.has("patient")) {
					rootObj = rootObj.optJSONObject("patient");
					DDResult ret = DDResult.makeResult(RetError.NONE);
					DataModule.getInstance().saveLoginedUserInfo(gson.fromJson(rootObj.toString(),
							PatientBean.class));
					return ret;
				}

			} else
				return DDResult.makeResult(RetError.API_INTERFACE,
						respBean.getErrMsg());

			return DDResult.makeResult(RetError.API_INTERFACE);

		} catch (Exception e) {

		}
		return DDResult.makeResult(RetError.ERROR);
	}

}
