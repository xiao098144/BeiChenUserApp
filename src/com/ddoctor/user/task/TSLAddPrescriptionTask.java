package com.ddoctor.user.task;

import org.json.JSONObject;

import com.ddoctor.enums.RetError;
import com.ddoctor.user.config.AppBuildConfig;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.wapi.RespBean;
import com.ddoctor.user.wapi.WAPI;
import com.ddoctor.user.wapi.bean.TslPrescriptionBean;
import com.ddoctor.user.wapi.constant.Action;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.HttpHelper;
import com.ddoctor.utils.MyUtils;
import com.google.gson.Gson;

public class TSLAddPrescriptionTask extends BaseAsyncTask<String, Void, DDResult>{

	private TslPrescriptionBean _prescriptionBean = null;
	
	public TSLAddPrescriptionTask(TslPrescriptionBean bean) {
		super();
		
		_prescriptionBean = bean;
	}

	@Override
	protected DDResult doInBackground(String... params) {
		try{
			return do_request();
		}
		catch(Exception e){
		}
		
		return DDResult.makeResult(RetError.ERROR, null);
	}

	private DDResult do_request(){
		
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put(AppBuildConfig.ACTID, Action.ADD_TSL_PRESCRIPTION);
			jsonObject.put(AppBuildConfig.PATIENTID, DataModule.getInstance().getLoginedUserId());
			

			JSONObject obj = WAPI.beanToJSONObject(_prescriptionBean);
			if( obj != null )
				jsonObject.put("tslPrescription", obj);
			
			String resp_string = HttpHelper.http_post(WAPI.WAPI_TSL_URL, jsonObject.toString(), -1, 30 * 1000);
			if (resp_string == null) {
				return DDResult.makeResult(RetError.NETWORK_ERROR);
			}

			MyUtils.showLog(resp_string);

			Gson gson = new Gson();
			RespBean respBean = gson.fromJson(resp_string, RespBean.class);
			if (respBean == null) {
				return DDResult.makeResult(RetError.API_INTERFACE);
			}

			if( respBean.isSuccess() ) 
			{
				JSONObject respObject = new JSONObject(resp_string);
				if( respObject.has("tslPrescription") )
				{
					JSONObject prescriptionObject = respObject.getJSONObject("tslPrescription");
					
					TslPrescriptionBean bean  = gson.fromJson(prescriptionObject.toString(), TslPrescriptionBean.class);
					
					DDResult ret = DDResult.makeResult(RetError.NONE);
					ret.setObject(bean);
					return ret;
				}
				
				return DDResult.makeResult(RetError.API_INTERFACE);
			}
			else 
			{
				return DDResult.makeResult(RetError.API_INTERFACE, respBean.getErrMsg());
			}
			
		} 
		catch (Exception e) {

		}
		
		return DDResult.makeResult(RetError.ERROR);
	}
	
}
