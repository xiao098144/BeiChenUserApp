package com.ddoctor.user.task;

import org.json.JSONObject;

import com.ddoctor.enums.RetError;
import com.ddoctor.user.wapi.RespBean;
import com.ddoctor.user.wapi.WAPI;
import com.ddoctor.user.wapi.constant.Action;
import com.ddoctor.user.wapi.constant.UserType;
import com.ddoctor.utils.HttpHelper;
import com.ddoctor.utils.MyUtils;
import com.google.gson.Gson;
/**
 * 1.1.4	发送短信验证码
 * @author kang
 *
 */
public class SendSmsTask extends BaseAsyncTask<String, Void, RetError> {

	String _mobile;
	String _type;

	public SendSmsTask(String mobile, String type) {
		_mobile = mobile;
		_type = type;
	}

	@Override
	protected RetError doInBackground(String... arg0) {
		RetError ret = RetError.ERROR;
		try {
			Thread.sleep(1000); // for test

			RespBean respBean = new RespBean();
			String verify = "";
			ret = do_sendsms_request(_mobile, _type, respBean, verify);
			if (ret == RetError.NONE) {
				if (respBean.isSuccess()) {
					ret = RetError.NONE;
					ret.setObject(verify);
				} else {
					ret = RetError.API_INTERFACE;
					ret.setErrorMessage(respBean.errMsg);
				}
			}
		} catch (Exception e) {
		}

		return ret;
	}

	private RetError do_sendsms_request(String mobile, String type,
			RespBean respBean, String verify) {
		try {
			// 构建请求json串
			JSONObject json = new JSONObject();
			json.put("mobile", mobile);
			json.put("type", type);
			json.put("userType", UserType.PATIENT);
			json.put("actId", Action.SEND_VERIFY_SMS);

			MyUtils.showLog(json.toString());

			// 发起http请求
			String resp_string = HttpHelper.http_post(WAPI.WAPI_BASE_URL,
					json.toString());
			if (resp_string == null)
				return RetError.NETWORK_ERROR;

			// 接口调用成功，解析返回结果
			Gson gson = new Gson();
			RespBean resp = gson.fromJson(resp_string, RespBean.class);
			if (resp == null)
				return RetError.API_INTERFACE;

			respBean.copyFrom(resp);
			if (respBean.isSuccess()) {
				JSONObject respObj = new JSONObject(resp_string);
				JSONObject verifyObj = respObj.getJSONObject("verify");
				String _verify = gson.fromJson(verifyObj.toString(), String.class);
				if (_verify != null)
					verify = _verify;
			}

			return RetError.NONE;
		} catch (Exception e) {

		}

		return RetError.ERROR;
	}

}
