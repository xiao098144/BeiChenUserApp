package com.ddoctor.user.task;

import java.lang.reflect.Type;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.os.Bundle;

import com.ddoctor.enums.RetError;
import com.ddoctor.user.config.AppBuildConfig;
import com.ddoctor.user.wapi.RespBean;
import com.ddoctor.user.wapi.WAPI;
import com.ddoctor.user.wapi.bean.ProductBean;
import com.ddoctor.user.wapi.constant.Action;
import com.ddoctor.utils.HttpHelper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class GetGoodsListTask extends BaseAsyncTask<String, Void, RetError>{

	private int page ;
	public int wsec = 0;
	
	public GetGoodsListTask(int page) {
		super();
		this.page = page;
	}

	@Override
	protected RetError doInBackground(String... params) {
		try{
			if( wsec > 0 )
				Thread.sleep(wsec); // for test
			
			return getGoodsList();
		}
		catch(Exception e){
		}
		
		return RetError.ERROR;
	}

	private RetError getGoodsList()//RespBean respBean, ArrayList<ProductBean> dataList ){
	{
		try 
		{
			JSONObject jsonObject = new JSONObject();
			jsonObject.put(AppBuildConfig.ACTID, Action.GET_PRODUCT_LIST);
			jsonObject.put("page", page);
			
			String resp_string = HttpHelper.http_post(WAPI.WAPI_BASE_URL, jsonObject.toString());
			if (resp_string == null) {
				return RetError.NETWORK_ERROR;
			}
//			MyUtils.showLog("", "商城列表返回结果 "+resp_string);
			Gson gson = new Gson();
			RespBean respBean = gson.fromJson(resp_string, RespBean.class);
//			MyUtils.showLog("", "code  "+respBean.toString());
			if( respBean != null && respBean.isSuccess() ) 
			{
				JSONObject respObj = new JSONObject(resp_string);
				if( respObj.has("recordList") )
				{
					JSONArray listObj = respObj.getJSONArray("recordList");
					if( listObj != null ) 
					{
						Type type = new TypeToken<ArrayList<ProductBean>>() {}.getType();
						ArrayList<ProductBean> list = gson.fromJson(listObj.toString(),type);
						if (null != list ) 
						{
							Bundle data = new Bundle();
							data.putParcelableArrayList("list", list);
							
							RetError ret = RetError.NONE;
							ret.setBundle(data);
							ret.setErrorMessage(respBean.getErrMsg());
							return ret;
						}
					}
				}
			}

			RetError ret = RetError.API_INTERFACE;
			if( respBean != null )
				ret.setErrorMessage(respBean.getErrMsg());
			
			return ret;
		} catch (Exception e) {

		}
		return RetError.ERROR;
	}
	
}
