package com.ddoctor.user.task;

public interface TaskPostCallBack<K> {
	public void taskFinish(K result);
}
