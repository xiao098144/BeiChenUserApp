package com.ddoctor.user.task;

import org.json.JSONObject;

import com.ddoctor.enums.RetError;
import com.ddoctor.user.config.AppBuildConfig;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.wapi.RespBean;
import com.ddoctor.user.wapi.WAPI;
import com.ddoctor.user.wapi.bean.HydcfyzBean;
import com.ddoctor.user.wapi.constant.Action;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.HttpHelper;
import com.ddoctor.utils.MyUtils;
import com.google.gson.Gson;

public class AddHydCfyzRecordTask extends BaseAsyncTask<String, Void, DDResult>{

	private HydcfyzBean hydcfyzBean;
	
	
	public AddHydCfyzRecordTask(HydcfyzBean hydcfyzBean) {
		super();
		this.hydcfyzBean = hydcfyzBean;
	}

	@Override
	protected DDResult doInBackground(String... params) {
		try {
			return addHydCfyzRecord();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return DDResult.makeResult(RetError.ERROR);
	}
	
	private DDResult addHydCfyzRecord(){
		try {
			Gson gson = new Gson();
			
			JSONObject jsonObject = new JSONObject();
			jsonObject.put(AppBuildConfig.ACTID, Action.ADD_HYDCFYZ);
			jsonObject.put(AppBuildConfig.PATIENTID, DataModule.getInstance().getLoginedUserId());
			jsonObject.put("hydcfyz", gson.toJson(hydcfyzBean));
			
			String resp_string = HttpHelper.http_post(WAPI.WAPI_BASE_URL,
					jsonObject.toString());
			if (resp_string == null) {
				return DDResult.makeResult(RetError.NETWORK_ERROR);
			}

			MyUtils.showLog(resp_string);
			
			RespBean respBean = gson.fromJson(resp_string, RespBean.class);
			if (respBean != null) {
				if (respBean.isSuccess()) {
					DDResult ret = DDResult.makeResult(RetError.NONE);
					return ret;
				} else {
					return DDResult.makeResult(RetError.API_INTERFACE,
							respBean.getErrMsg());
				}
			} else
				return DDResult.makeResult(RetError.API_INTERFACE);
			
		} catch (Exception e) {
			
		}
		return DDResult.makeResult(RetError.ERROR);
	}

}
