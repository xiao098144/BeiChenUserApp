package com.ddoctor.user.task;

import org.json.JSONObject;

import com.ddoctor.enums.RetError;
import com.ddoctor.user.config.AppBuildConfig;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.wapi.RespBean;
import com.ddoctor.user.wapi.WAPI;
import com.ddoctor.user.wapi.constant.Action;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.HttpHelper;
import com.ddoctor.utils.MyUtils;
import com.google.gson.Gson;

public class DoBindMioBoxTask extends BaseAsyncTask<String, Void, DDResult> {

	private boolean _isDoBind = true; // 是解除还是绑定 默认绑定
	private String boxSn;
	private String glucoseMeterType;

	public DoBindMioBoxTask(String boxSn) {
		super();
		this.boxSn = boxSn;
		_isDoBind = false;
	}

	public DoBindMioBoxTask(String boxSn, String glucoseMeterType) {
		super();
		this.boxSn = boxSn;
		this.glucoseMeterType = glucoseMeterType;
		_isDoBind = true;
	}

	@Override
	protected DDResult doInBackground(String... arg0) {
		try {
			return doBindMioBox();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return DDResult.makeResult(RetError.ERROR);
	}

	private DDResult doBindMioBox() {
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("boxSn", boxSn);
			jsonObject.put(AppBuildConfig.PATIENTID, DataModule.getInstance()
					.getLoginedUserId());
			if (_isDoBind) {
				jsonObject.put(AppBuildConfig.ACTID, Action.MIO_BOX_BINDING);
				jsonObject.put("glucoseMeterType", glucoseMeterType);
				jsonObject.put("readNum", 20);
			} else {
				jsonObject.put(AppBuildConfig.ACTID,
						Action.MIO_BOX_BINDING_CANCEL);
			}

			MyUtils.showLog(jsonObject.toString());

			// 发起http请求
			String resp_string = HttpHelper.http_post(WAPI.WAPI_BASE_URL,
					jsonObject.toString());
			if (resp_string == null) {
				return DDResult.makeResult(RetError.NETWORK_ERROR);
			}

			MyUtils.showLog(resp_string);

			Gson gson = new Gson();
			RespBean respBean = gson.fromJson(resp_string, RespBean.class);
			if (respBean == null) {
				return DDResult.makeResult(RetError.API_INTERFACE);
			}

			if (respBean.isSuccess()) {
				return DDResult.makeResult(RetError.NONE);
			} else
				return DDResult.makeResult(RetError.API_INTERFACE,
						respBean.getErrMsg());

		} catch (Exception e) {

		}
		return DDResult.makeResult(RetError.ERROR);
	}

}
