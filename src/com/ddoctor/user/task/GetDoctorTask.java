package com.ddoctor.user.task;

import org.json.JSONObject;

import com.ddoctor.enums.RetError;
import com.ddoctor.user.config.AppBuildConfig;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.wapi.RespBean;
import com.ddoctor.user.wapi.WAPI;
import com.ddoctor.user.wapi.bean.DoctorBean;
import com.ddoctor.user.wapi.constant.Action;
import com.ddoctor.utils.HttpHelper;
import com.ddoctor.utils.MyUtils;
import com.google.gson.Gson;

public class GetDoctorTask extends BaseAsyncTask<String, Void, RetError> {

	String _doctorId;

	public GetDoctorTask(String doctorId) {
		super();
		this._doctorId = doctorId;
	}

	@Override
	protected RetError doInBackground(String... arg0) {
		RetError ret = RetError.ERROR;
		try {

			RespBean respBean = new RespBean();
			DoctorBean db = new DoctorBean();
			ret = getDoctor(_doctorId, respBean, db);
			if (ret == RetError.NONE) {
				if (respBean.isSuccess()) {
					ret = RetError.NONE;
					ret.setObject(db);
				} else {
					ret = RetError.API_INTERFACE;
					ret.setErrorMessage(respBean.errMsg);
				}
			}
		} catch (Exception e) {
		}

		return ret;
	}

	private RetError getDoctor(String doctorId, RespBean respBean, DoctorBean db) {
		try {
			Gson gson = new Gson();
			// 构建请求json串
			JSONObject json = new JSONObject();
			json.put("doctorId", doctorId);
			json.put("actId", Action.GET_DOCTOR);
			json.put(AppBuildConfig.PATIENTID,DataModule.getInstance().getLoginedUserId());

			MyUtils.showLog(json.toString());

			// 发起http请求
			String resp_string = HttpHelper.http_post(WAPI.WAPI_BASE_URL,
					json.toString());
			if (resp_string == null)
				return RetError.NETWORK_ERROR;
			MyUtils.showLog("GetDoctorTask "+resp_string);
		
			RespBean resp = gson.fromJson(resp_string, RespBean.class);
			if (resp == null)
				return RetError.API_INTERFACE;

			respBean.copyFrom(resp);
			if (respBean.isSuccess()) {
				JSONObject respObj = new JSONObject(resp_string);
				JSONObject patientObj = respObj.getJSONObject("doctor");
				DoctorBean doctor = gson.fromJson(patientObj.toString(),
						DoctorBean.class);
				if (doctor != null)
					db.copyFrom(doctor);
			}
			return RetError.NONE;
		} catch (Exception e) {

		}

		return RetError.ERROR;
	}

}
