package com.ddoctor.user.task;

import org.json.JSONObject;

import com.ddoctor.enums.RetError;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.wapi.RespBean;
import com.ddoctor.user.wapi.WAPI;
import com.ddoctor.user.wapi.bean.QuesionBean;
import com.ddoctor.user.wapi.constant.Action;
import com.ddoctor.user.wapi.constant.UserType;
import com.ddoctor.utils.HttpHelper;
import com.ddoctor.utils.MyUtils;
import com.google.gson.Gson;

public class QuestionTask extends BaseAsyncTask<String, Void, RetError> {
	String _doctorId;
	QuesionBean _qb;

	public QuestionTask(String doctorId, QuesionBean qb) {
		this._doctorId = doctorId;
		this._qb = qb;
	}

	@Override
	protected RetError doInBackground(String... arg0) {
		RetError ret = RetError.ERROR;
		try {

			RespBean respBean = new RespBean();
			ret = addQuestion(_doctorId, _qb, respBean);
			if (ret == RetError.NONE) {
				if (respBean.isSuccess()) {
					ret = RetError.NONE;

				} else {
					ret = RetError.API_INTERFACE;
					ret.setErrorMessage(respBean.errMsg);
				}
			}
		} catch (Exception e) {
		}

		return ret;
	}

	private RetError addQuestion(String doctorId, QuesionBean qb,
			RespBean respBean) {
		try {
			Gson gson = new Gson();
			// 构建请求json串
			JSONObject json = new JSONObject();
			json.put("doctorId", doctorId);
			json.put("patientId", DataModule.getInstance().getLoginedUserId());
			json.put("userType", UserType.PATIENT);
			json.put("question", WAPI.beanToJSONObject(qb));
			json.put("actId", Action.ADD_QUESTION);

			MyUtils.showLog(json.toString());

			// 发起http请求
			String resp_string = HttpHelper.http_post(WAPI.WAPI_BASE_URL,
					json.toString());
			MyUtils.showLog("resp_string",resp_string);
			if (resp_string == null)
				return RetError.NETWORK_ERROR;

			
			RespBean resp = gson.fromJson(resp_string, RespBean.class);
			if (resp == null)
				return RetError.API_INTERFACE;

			respBean.copyFrom(resp);

			return RetError.NONE;
		} catch (Exception e) {

		}

		return RetError.ERROR;
	}
}
