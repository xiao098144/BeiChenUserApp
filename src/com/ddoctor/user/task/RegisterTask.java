package com.ddoctor.user.task;

import org.json.JSONObject;

import com.ddoctor.application.MyApplication;
import com.ddoctor.enums.RetError;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.data.MyProfile;
import com.ddoctor.user.wapi.RespBean;
import com.ddoctor.user.wapi.WAPI;
import com.ddoctor.user.wapi.bean.PatientBean;
import com.ddoctor.user.wapi.constant.Action;
import com.ddoctor.utils.HttpHelper;
import com.ddoctor.utils.MyUtils;
import com.ddoctor.utils.TimeUtil;
import com.google.gson.Gson;


public class RegisterTask extends BaseAsyncTask<String, Void, RetError> {
	private String _mobile;
	private String _password;
	private String _vcode; 
	private int _sex;
	private float _weight;
	private int _height;
	private int _ageYear;
	
	public RegisterTask(String mobile, 
			String password, 
			String vcode, 
			int sex,
			float weight,
			int height,
			int ageYear
			)
	{
		_mobile = mobile;
		_password = password;
		_vcode = vcode;
		_sex = sex;
		_weight = weight;
		_height = height;
		_ageYear = ageYear;
	}

	@Override
	protected RetError doInBackground(String... params) {
		RetError ret = RetError.ERROR;
		try{
//			Thread.sleep(1000); // for test
			
			// 访问登录接口，获取返回值，解析返回值
			RespBean respBean = new RespBean();
			PatientBean patientBean = new PatientBean();
			ret = do_register_request(_mobile, _password, _vcode,  respBean, patientBean);
			if( ret == RetError.NONE )
			{
				if( respBean.isSuccess() )
				{
					
					// 注册成功，继续调用修改个人信息接口
					ret = do_update_request(patientBean.getId(),
							_sex, _weight, _height, _ageYear, respBean);
					// 这个接口暂时不用管是否成功，不成功，用户后面可以继续修改
					if( ret == RetError.NONE && respBean.isSuccess() )
					{
						patientBean.setSex(_sex);
						patientBean.setWeight(_weight);
						patientBean.setHeight(_height);
						patientBean.setBirthday(String.format("%d-01-01", _ageYear));
					}
					
					ret = RetError.NONE;
					ret.setObject(patientBean);
				}
				else
				{
					ret = RetError.API_INTERFACE;
					ret.setErrorMessage(respBean.errMsg);
				}
			}
		}
		catch(Exception e){
		}
		
		return ret;
	}
	
	
	// 注册接口
	private static RetError do_register_request(
			String mobile, String password,String vcode,
			RespBean respBean, PatientBean patientBean)
	{
		try{
			// 构建请求json串
			JSONObject json = new JSONObject();
			json.put("actId", Action.PATIENT_REGISTER);
			json.put("mobile", mobile);
			json.put("password", password);
			json.put("verify", vcode);
			json.put("uuid", DataModule.getUUID());
			json.put("channel", MyProfile.getChannelId());
			json.put("imei", MyUtils.getIMEI(MyApplication.getInstance().getApplicationContext()));
			json.put("imsi", MyUtils.getIMSI(MyApplication.getInstance().getApplicationContext()));
			MyUtils.showLog(json.toString());
			
			// 发起http请求
			String resp_string = HttpHelper.http_post(WAPI.WAPI_BASE_URL, json.toString());
			if( resp_string == null )
				return RetError.NETWORK_ERROR;
			
			MyUtils.showLog(resp_string);
			
			// 接口调用成功，解析返回结果
			Gson gson = new Gson();
			RespBean resp = gson.fromJson(resp_string, RespBean.class);
			if( resp == null )
				return RetError.API_INTERFACE;
			
			respBean.copyFrom(resp);
			if( respBean.isSuccess() )
			{
				JSONObject respObj = new JSONObject(resp_string);
				JSONObject patientObj = respObj.getJSONObject("patient");
				PatientBean patient = gson.fromJson(patientObj.toString(), PatientBean.class);
				if( patient != null )
					patientBean.copyFrom(patient);
			}
			
			return RetError.NONE;
		}
		catch(Exception e)
		{
			
		}
		
		return RetError.ERROR;
	}

	
	// 修改个人信息
	private static RetError do_update_request(
			int patientId,
			int sex, float weight, int height, int year,
			RespBean respBean)
	{
		try{
			// 构建请求json串
			JSONObject json = new JSONObject();
			json.put("actId", Action.UPDATE_PATIENT);
			json.put("patientId", patientId);
			
			PatientBean pb = new PatientBean();
			pb.setId(patientId);
			pb.setSex(sex);
			pb.setWeight(weight);
			pb.setHeight(height);
			String birthday = String.format("%d-01-01", year);
			if (birthday.startsWith("null")) {
				birthday = birthday.replace("null", String.valueOf(TimeUtil.getInstance().getCurrentYear()));
			}
			pb.setBirthday(birthday);
			
			JSONObject patientBeanJSONObject = WAPI.beanToJSONObject(pb);
			if( patientBeanJSONObject != null )
				json.put("patient", patientBeanJSONObject);
			
			MyUtils.showLog(json.toString());
			
			// 发起http请求
			String resp_string = HttpHelper.http_post(WAPI.WAPI_BASE_URL, json.toString());
			if( resp_string == null )
				return RetError.NETWORK_ERROR;
			
			MyUtils.showLog(resp_string);
			// 接口调用成功，解析返回结果
			Gson gson = new Gson();
			RespBean resp = gson.fromJson(resp_string, RespBean.class);
			if( resp == null )
				return RetError.API_INTERFACE;
			
			respBean.copyFrom(resp);
			
			return RetError.NONE;
		}
		catch(Exception e)
		{
			
		}
		
		return RetError.ERROR;
	}
}
