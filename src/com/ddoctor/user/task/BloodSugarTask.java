package com.ddoctor.user.task;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.os.Bundle;

import com.ddoctor.enums.RetError;
import com.ddoctor.user.config.AppBuildConfig;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.wapi.RespBean;
import com.ddoctor.user.wapi.WAPI;
import com.ddoctor.user.wapi.bean.SugarValueBean;
import com.ddoctor.user.wapi.constant.Action;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.HttpHelper;
import com.ddoctor.utils.MyUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class BloodSugarTask extends BaseAsyncTask<String, Void, DDResult> {

	private String startDate, endDate;
	private int page, type;

	public BloodSugarTask(String startDate, String endDate, int page, int type) {
		this.startDate = startDate;
		this.endDate = endDate;
		this.page = page;
		this.type = type;
	}

	@Override
	protected DDResult doInBackground(String... params) {
		try {
			return getSugarRecordList();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return DDResult.makeResult(RetError.ERROR);
	}

	private DDResult getSugarRecordList() {
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put(AppBuildConfig.ACTID, Action.GET_SUGARVALUE_LIST);
			jsonObject.put(AppBuildConfig.PATIENTID, DataModule.getInstance()
					.getLoginedUserId());
			jsonObject.put("startTime", startDate);
			jsonObject.put("endTime", endDate);
			jsonObject.put("page", page);
			jsonObject.put("type", type);

			String resp_string = HttpHelper.http_post(WAPI.WAPI_BASE_URL,
					jsonObject.toString());
			if (resp_string == null) {
				return DDResult.makeResult(RetError.NETWORK_ERROR);
			}

			MyUtils.showLog(resp_string);
			Gson gson = new Gson();
			RespBean respBean = gson.fromJson(resp_string, RespBean.class);
			if (respBean == null) {
				return DDResult.makeResult(RetError.API_INTERFACE);
			}

			if (respBean.isSuccess()) {
				DDResult ret = DDResult.makeResult(RetError.NONE);
				ArrayList<SugarValueBean> dataList = new ArrayList<SugarValueBean>();

				JSONObject respObj = new JSONObject(resp_string);
				if (respObj.has("recordList")) {
					JSONArray listObj = respObj.getJSONArray("recordList");
					if (listObj != null) {
						Type type = new TypeToken<ArrayList<SugarValueBean>>() {
						}.getType();
						List<SugarValueBean> list = gson.fromJson(
								listObj.toString(), type);
						if (null != list) {
							dataList.addAll(list);
						}
					}
				}

				Bundle data = new Bundle();
				data.putParcelableArrayList("list", dataList);
				ret.setBundle(data);
				ret.setErrorMessage(respBean.getErrMsg());
				return ret;
			} else {
				return DDResult.makeResult(RetError.API_INTERFACE,
						respBean.getErrMsg());
			}

		} catch (Exception e) {

		}

		return DDResult.makeResult(RetError.ERROR);
	}

}
