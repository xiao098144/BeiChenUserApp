package com.ddoctor.user.task;

import org.json.JSONObject;

import android.os.Bundle;

import com.ddoctor.enums.RetError;
import com.ddoctor.user.config.AppBuildConfig;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.wapi.RespBean;
import com.ddoctor.user.wapi.WAPI;
import com.ddoctor.user.wapi.constant.Action;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.HttpHelper;
import com.ddoctor.utils.MyUtils;
import com.google.gson.Gson;

public class GetMiOBoxStateTask extends BaseAsyncTask<String, Void, DDResult>{

	
	
	public GetMiOBoxStateTask() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	protected DDResult doInBackground(String... params) {
		try {
			return getMiOBoxState();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return DDResult.makeResult(RetError.ERROR);
	}

	private DDResult getMiOBoxState(){
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put(AppBuildConfig.ACTID, Action.MIO_BOX_BINDING_STATE);
			jsonObject.put(AppBuildConfig.PATIENTID, DataModule.getInstance().getLoginedUserId());
			
			MyUtils.showLog(" GetMioState "+jsonObject.toString());
			
			String resp_string = HttpHelper.http_post(WAPI.WAPI_BASE_URL,
					jsonObject.toString());
			if (resp_string == null) {
				return DDResult.makeResult(RetError.NETWORK_ERROR);
			}

			MyUtils.showLog(resp_string);
			Gson gson = new Gson();
			RespBean respBean = gson.fromJson(resp_string, RespBean.class);
			if (respBean == null) {
				return DDResult.makeResult(RetError.API_INTERFACE);
			}
			if (respBean.isSuccess()) {
				JSONObject respObj = new JSONObject(resp_string);
				if (respObj.has("boxSn")) {
					Bundle data = new Bundle();
					data.putString("boxSn", respObj.optString("boxSn",null));
					DDResult ret = DDResult.makeResult(RetError.NONE);
					ret.setBundle(data);
					return ret;
				}
			}
			
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return DDResult.makeResult(RetError.ERROR);
	}
	
}
