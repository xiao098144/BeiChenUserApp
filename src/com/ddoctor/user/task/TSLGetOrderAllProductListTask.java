package com.ddoctor.user.task;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.ddoctor.enums.RetError;
import com.ddoctor.user.config.AppBuildConfig;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.wapi.RespBean;
import com.ddoctor.user.wapi.WAPI;
import com.ddoctor.user.wapi.bean.TslAddressinfoBean;
import com.ddoctor.user.wapi.bean.TslProductBean;
import com.ddoctor.user.wapi.constant.Action;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.HttpHelper;
import com.ddoctor.utils.MyUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class TSLGetOrderAllProductListTask extends BaseAsyncTask<String, Void, DDResult>{

	public TSLGetOrderAllProductListTask() {
		super();
	}

	@Override
	protected DDResult doInBackground(String... params) {
		try{
			return do_request();
		}
		catch(Exception e){
		}
		
		return DDResult.makeResult(RetError.ERROR, null);
	}

	private DDResult do_request(){
		
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put(AppBuildConfig.ACTID, Action.GET_PRESCRIPTION_PRODUCT_LIST);
			jsonObject.put(AppBuildConfig.PATIENTID, DataModule.getInstance().getLoginedUserId());
			
			String resp_string = HttpHelper.http_post(WAPI.WAPI_TSL_URL, jsonObject.toString(), -1, 30 * 1000);
			if (resp_string == null) {
				return DDResult.makeResult(RetError.NETWORK_ERROR);
			}

			MyUtils.showLog(resp_string);
			Gson gson = new Gson();
			RespBean respBean = gson.fromJson(resp_string, RespBean.class);
			if (respBean == null) {
				return DDResult.makeResult(RetError.API_INTERFACE);
			}

			if( respBean.isSuccess() ) 
			{
				ArrayList<TslProductBean> dataList = new ArrayList<TslProductBean>();
				ArrayList<TslAddressinfoBean> addressList = new ArrayList<TslAddressinfoBean>();
				
				JSONObject respObj = new JSONObject(resp_string);
				if( respObj.has("productList") )
				{
					JSONArray listObj = respObj.getJSONArray("productList");
					if( listObj != null ) 
					{
						Type type = new TypeToken<ArrayList<TslProductBean>>() {}.getType();
						List<TslProductBean> list = gson.fromJson(listObj.toString(), type);
						if (null != list ) 
						{
							dataList.addAll(list);
						}
					}
				}
				
				if( respObj.has("addressList") )
				{

					JSONArray listObj = respObj.getJSONArray("addressList");
					if( listObj != null ) 
					{
						Type type = new TypeToken<ArrayList<TslAddressinfoBean>>() {}.getType();
						List<TslAddressinfoBean> list = gson.fromJson(listObj.toString(), type);
						if (null != list ) 
						{
							addressList.addAll(list);
						}
					}
				}

				DDResult ret = DDResult.makeResult(RetError.NONE);
				ret.setObject(dataList);
				ret.setObject1(addressList);
				return ret;
			}
			else 
			{
				return DDResult.makeResult(RetError.API_INTERFACE, respBean.getErrMsg());
			}
			
		} 
		catch (Exception e) {

		}
		
		return DDResult.makeResult(RetError.ERROR);
	}
	
}
