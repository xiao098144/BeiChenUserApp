package com.ddoctor.user.task;

import org.json.JSONObject;

import com.ddoctor.enums.RetError;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.wapi.RespBean;
import com.ddoctor.user.wapi.WAPI;
import com.ddoctor.user.wapi.constant.Action;
import com.ddoctor.user.wapi.constant.UserType;
import com.ddoctor.utils.HttpHelper;
import com.ddoctor.utils.MyUtils;
import com.google.gson.Gson;

/**
 * 2.12.6 申请添加医生
 * 
 * @author kang
 * 
 */
public class DoctorPatientTask extends BaseAsyncTask<String, Void, RetError> {
	String _doctorId;

	public DoctorPatientTask(String doctorId) {
		this._doctorId = doctorId;
	}

	@Override
	protected RetError doInBackground(String... arg0) {
		RetError ret = RetError.ERROR;
		try {

			RespBean respBean = new RespBean();

			ret = addDoctorPatient(_doctorId, respBean);
			if (ret == RetError.NONE) {
				if (respBean.isSuccess()) {
					ret = RetError.NONE;

				} else {
					ret = RetError.API_INTERFACE;
					ret.setErrorMessage(respBean.errMsg);
				}
			}
		} catch (Exception e) {
		}

		return ret;
	}

	private RetError addDoctorPatient(String doctorId, RespBean respBean) {
		try {
			// 构建请求json串
			JSONObject json = new JSONObject();
			json.put("doctorId", doctorId);
			json.put("patientId", DataModule.getInstance().getLoginedUserId());
			json.put("userType", UserType.PATIENT);
			json.put("type", 1);// 1添加 0 取消
			json.put("actId", Action.DO_PATIENT_DOCTOR_RELATION);

			MyUtils.showLog(json.toString());

			// 发起http请求
			String resp_string = HttpHelper.http_post(WAPI.WAPI_BASE_URL,
					json.toString());
			if (resp_string == null)
				return RetError.NETWORK_ERROR;

			// 接口调用成功，解析返回结果
			Gson gson = new Gson();
			RespBean resp = gson.fromJson(resp_string, RespBean.class);
			if (resp == null)
				return RetError.API_INTERFACE;

			respBean.copyFrom(resp);

			return RetError.NONE;
		} catch (Exception e) {

		}

		return RetError.ERROR;
	}

}
