package com.ddoctor.user.task;

import java.util.Locale;

import org.json.JSONObject;

import com.ddoctor.enums.RetError;
import com.ddoctor.user.wapi.RespBean;
import com.ddoctor.user.wapi.WAPI;
import com.ddoctor.user.wapi.constant.Action;
import com.ddoctor.user.wapi.constant.UserType;
import com.ddoctor.utils.HttpHelper;
import com.ddoctor.utils.MyUtils;
import com.google.gson.Gson;

public class SendVCodeTask extends BaseAsyncTask<String, Void, RetError> {
	private String _mobile;
	private int type;

	public SendVCodeTask(String mobile, int type) {
		this.type = type;
		_mobile = mobile;
	}

	@Override
	protected RetError doInBackground(String... params) {
		RetError ret = RetError.ERROR;
		try {
			// 访问登录接口，获取返回值，解析返回值
			RespBean respBean = new RespBean();
			ret = do_sendvcode(_mobile, respBean);
			if (ret == RetError.NONE) {
				if (respBean.isSuccess()) {
					// 解析返回的结果
					// 登录成功后获取到的信息保存到LoginResp里
				} else {
					ret = RetError.API_INTERFACE;
					ret.setErrorMessage(respBean.errMsg);
				}
			}
		} catch (Exception e) {
		}

		return ret;
	}

	// 登录接口
	public RetError do_sendvcode(String mobile, RespBean respBean) {
		try {
			// 构建请求json串
			JSONObject json = new JSONObject();
			json.put("mobile", mobile);
			json.put("actId", Action.SEND_VERIFY_SMS);
			json.put("type", type);
			json.put("userType", UserType.PATIENT);

			MyUtils.showLog(json.toString());

			// 发起http请求
			String resp_string = HttpHelper.http_post(WAPI.WAPI_BASE_URL,
					json.toString());
			if (resp_string == null)
				return RetError.NETWORK_ERROR;

			MyUtils.showLog(resp_string);
			// 接口调用成功，解析返回结果
			Gson gson = new Gson();
			RespBean resp = gson.fromJson(resp_string, RespBean.class);
			if (resp == null)
				return RetError.API_INTERFACE;

			int verify = 0;
			String vcode = "";
			respBean.copyFrom(resp);
			if (respBean.isSuccess()) {
				JSONObject respObj = new JSONObject(resp_string);
				verify = respObj.optInt("verify");
				vcode = String.format(Locale.getDefault(), "%d", verify);
				if (vcode.length() < 4) {
					int length = vcode.length();
					StringBuffer sb = new StringBuffer();
					for (int i = 0; i < 4 - length; i++) {
						sb.append("0");
					}
					sb.append(verify);
					vcode = sb.toString();
				}
			}

			RetError ret = RetError.NONE;
			ret.setObject(vcode);
			return ret;
		} catch (Exception e) {

		}

		return RetError.ERROR;
	}

}
