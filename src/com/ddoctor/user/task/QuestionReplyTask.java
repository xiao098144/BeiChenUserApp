package com.ddoctor.user.task;

import org.json.JSONObject;

import android.text.TextUtils;

import com.ddoctor.enums.RetError;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.wapi.RespBean;
import com.ddoctor.user.wapi.WAPI;
import com.ddoctor.user.wapi.bean.ReplyBean;
import com.ddoctor.user.wapi.constant.Action;
import com.ddoctor.user.wapi.constant.UserType;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.HttpHelper;
import com.ddoctor.utils.MyUtils;
import com.google.gson.Gson;

public class QuestionReplyTask extends BaseAsyncTask<String, Void, DDResult> {
	
	private int _doctorId;
	private ReplyBean _reply;

	public QuestionReplyTask(int doctorId, ReplyBean reply) {
		this._doctorId = doctorId;
		this._reply = reply;
	}

	@Override
	protected DDResult doInBackground(String... params) {
		try {
			return addQuestionReply();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return DDResult.makeResult(RetError.ERROR);
	}

	private DDResult addQuestionReply() {
		try {
			Gson gson = new Gson();
			// 构建请求json串
			JSONObject json = new JSONObject();
			json.put("doctorId", _doctorId);
			json.put("patientId", DataModule.getInstance().getLoginedUserId());
			json.put("userType", UserType.PATIENT);
			json.put("reply", WAPI.beanToJSONObject(_reply));
			json.put("actId", Action.ADD_QUESTION_REPLY);

			MyUtils.showLog(json.toString());

			// 发起http请求
			String resp_string = HttpHelper.http_post(WAPI.WAPI_BASE_URL,
					json.toString());
			if (TextUtils.isEmpty(resp_string)) {
				return DDResult.makeResult(RetError.NETWORK_ERROR);
			}
			MyUtils.showLog("添加不适记录  " + resp_string);
			RespBean respBean = gson.fromJson(resp_string, RespBean.class);
			if (respBean != null) {
				if (respBean.isSuccess()) {
					DDResult ret = DDResult.makeResult(RetError.NONE);
					return ret;
				} else {
					return DDResult.makeResult(RetError.API_INTERFACE,
							respBean.getErrMsg());
				}
			} else
				return DDResult.makeResult(RetError.API_INTERFACE);

		} catch (Exception e) {

		}
		return DDResult.makeResult(RetError.ERROR);
	}

}
