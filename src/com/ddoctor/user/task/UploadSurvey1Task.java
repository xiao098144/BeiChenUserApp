package com.ddoctor.user.task;

import org.json.JSONObject;

import com.ddoctor.enums.RetError;
import com.ddoctor.user.config.AppBuildConfig;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.wapi.RespBean;
import com.ddoctor.user.wapi.WAPI;
import com.ddoctor.user.wapi.bean.QuestionjbzlBean;
import com.ddoctor.user.wapi.constant.Action;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.HttpHelper;
import com.ddoctor.utils.MyUtils;
import com.google.gson.Gson;

/**
 * 提交调查问卷1
 * 
 * @author 萧
 * @Date 2015-6-24下午4:55:49
 * @TODO TODO
 */
public class UploadSurvey1Task extends BaseAsyncTask<String, Void, DDResult> {

	private QuestionjbzlBean surveyBean;

	public UploadSurvey1Task(QuestionjbzlBean surveyBean) {
		surveyBean.setSys2hpgResult("");
		surveyBean.setSyscrResult("");
		surveyBean.setSysfpgResult("");
		surveyBean.setSyshba1cResult("");
		surveyBean.setSysmalbResult("");
		surveyBean.setSysnsdResult("");
		surveyBean.setSystgResult("");
		surveyBean.setSystcResult("");
		surveyBean.setZfbl("");
		this.surveyBean = surveyBean;
	}

	@Override
	protected DDResult doInBackground(String... arg0) {
		try {
			return uploadSurvey1();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return DDResult.makeResult(RetError.ERROR);
	}

	private DDResult uploadSurvey1() {
		try {
			JSONObject json = new JSONObject();
			json.put(AppBuildConfig.ACTID, Action.ADD_QUESTIONJBZL);
			surveyBean.setUserId(DataModule.getInstance().getLoginedUserId());
			JSONObject clientInitJSONObject = WAPI.beanToJSONObject(surveyBean);
			if (clientInitJSONObject != null)
				json.put("questionjbzl", clientInitJSONObject);

			MyUtils.showLog(json.toString());

			// 发起http请求
			String resp_string = HttpHelper.http_post(WAPI.WAPI_BASE_URL,
					json.toString());
			if (resp_string == null)
				return DDResult.makeResult(RetError.NETWORK_ERROR);

			MyUtils.showLog(resp_string);

			Gson gson = new Gson();
			RespBean respBean = gson.fromJson(resp_string, RespBean.class);
			if (respBean != null) {
				if (respBean.isSuccess()) {
					DDResult ret = DDResult.makeResult(RetError.NONE);
					return ret;
				} else {
					return DDResult.makeResult(RetError.API_INTERFACE,
							respBean.getErrMsg());
				}
			} else
				return DDResult.makeResult(RetError.API_INTERFACE);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return DDResult.makeResult(RetError.ERROR);

	}

}
