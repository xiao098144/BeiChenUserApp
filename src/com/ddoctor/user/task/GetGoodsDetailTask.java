package com.ddoctor.user.task;

import org.json.JSONObject;

import android.os.Bundle;

import com.ddoctor.enums.RetError;
import com.ddoctor.user.config.AppBuildConfig;
import com.ddoctor.user.wapi.RespBean;
import com.ddoctor.user.wapi.WAPI;
import com.ddoctor.user.wapi.bean.ProductBean;
import com.ddoctor.user.wapi.constant.Action;
import com.ddoctor.utils.HttpHelper;
import com.ddoctor.utils.MyUtils;
import com.google.gson.Gson;

/**
 * 获取商品详情
 * @author 萧
 * @Date 2015-6-7下午11:18:10
 * @TODO TODO
 */
public class GetGoodsDetailTask extends BaseAsyncTask<String, Void, RetError>{

	private int productId;
	
	public GetGoodsDetailTask(int productId) {
		super();
		this.productId = productId;
	}


	@Override
	protected RetError doInBackground(String... params) {
		RetError ret = RetError.NONE;
		try{
			RespBean respBean = new RespBean();
			ret = getGoodsDetail(respBean);
			if( ret == RetError.NONE )
			{
				if( respBean.isSuccess() )
				{
					ret = RetError.NONE;
				}
				else
				{
					ret = RetError.API_INTERFACE;
					ret.setErrorMessage(respBean.errMsg);
				}
			}
		}
		catch(Exception e){
		}
		
		return ret;
	}

	private RetError getGoodsDetail(RespBean respBean){
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put(AppBuildConfig.ACTID, Action.GET_PRODUCT);
			jsonObject.put("productId", productId);
			
			String resp_string = HttpHelper.http_post(WAPI.WAPI_BASE_URL, jsonObject.toString());
			if (resp_string == null) {
				return RetError.NETWORK_ERROR;
			}
			MyUtils.showLog("", "商品详情返回结果 "+resp_string);
			Gson gson = new Gson();
			RespBean resp = gson.fromJson(resp_string, RespBean.class);
			if (resp == null) {
				return RetError.API_INTERFACE;
			}
			respBean.copyFrom(resp);
			MyUtils.showLog("", "code  "+respBean.toString());
			if (respBean.isSuccess()) {
				JSONObject respObj = new JSONObject(resp_string);
				if (respObj.has("product")) {
					ProductBean productBean = gson.fromJson(respObj.toString(), ProductBean.class);
					Bundle data = new Bundle();
					data.putParcelable("data", productBean);
					RetError.NONE.setBundle(data);
					return RetError.NONE;
				}else {
					RetError.SERVER_ERROR.setErrorMessage("获取商品信息失败");
					return RetError.SERVER_ERROR;
				}
			}else {
				RetError.SERVER_ERROR.setErrorMessage(respBean.getErrMsg());
				return RetError.SERVER_ERROR;
			}
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		return RetError.ERROR;
	}
	
}
