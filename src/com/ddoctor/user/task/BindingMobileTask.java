package com.ddoctor.user.task;

import org.json.JSONObject;

import com.ddoctor.enums.RetError;
import com.ddoctor.user.config.AppBuildConfig;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.wapi.RespBean;
import com.ddoctor.user.wapi.WAPI;
import com.ddoctor.user.wapi.bean.PatientBean;
import com.ddoctor.user.wapi.constant.Action;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.HttpHelper;
import com.ddoctor.utils.MyUtils;
import com.google.gson.Gson;

public class BindingMobileTask extends BaseAsyncTask<String, Void, DDResult> {

	private String _mobile;
	private String _verify;

	public BindingMobileTask(String mobile, String verify) {
		_mobile = mobile;
		_verify = verify;
	}

	@Override
	protected DDResult doInBackground(String... params) {
		try {
			return bindPhone();
		} catch (Exception e) {
		}

		return DDResult.makeResult(RetError.ERROR);
	}

	public DDResult bindPhone() {
		try {
			// 构建请求json串
			JSONObject json = new JSONObject();
			json.put("mobile", _mobile);
			json.put("verify", _verify);
			json.put(AppBuildConfig.ACTID, Action.PATIENT_BINDING_MOBILE);
			json.put(AppBuildConfig.PATIENTID, DataModule.getInstance()
					.getLoginedUserId());

			MyUtils.showLog(json.toString());

			// 发起http请求
			String resp_string = HttpHelper.http_post(WAPI.WAPI_BASE_URL,
					json.toString());
			if (resp_string == null) {
				return DDResult.makeResult(RetError.NETWORK_ERROR);
			}

			MyUtils.showLog(resp_string);

			Gson gson = new Gson();
			RespBean respBean = gson.fromJson(resp_string, RespBean.class);
			if (respBean == null) {
				return DDResult.makeResult(RetError.API_INTERFACE);
			}

			if (respBean.isSuccess()) {
				JSONObject rootObj = new JSONObject(resp_string);
				if (rootObj.has("patient")) {
					rootObj = rootObj.optJSONObject("patient");
					DDResult ret = DDResult.makeResult(RetError.NONE);
					DataModule.getInstance()
							.saveLoginedUserInfo(
									gson.fromJson(rootObj.toString(),
											PatientBean.class));
					return ret;
				}

			} else
				return DDResult.makeResult(RetError.API_INTERFACE,
						respBean.getErrMsg());

			return DDResult.makeResult(RetError.API_INTERFACE);

		} catch (Exception e) {

		}
		return DDResult.makeResult(RetError.ERROR);
	}

}
