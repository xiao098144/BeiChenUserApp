package com.ddoctor.user.task;

import org.json.JSONObject;

import android.os.Bundle;

import com.ddoctor.enums.RetError;
import com.ddoctor.user.config.AppBuildConfig;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.wapi.RespBean;
import com.ddoctor.user.wapi.WAPI;
import com.ddoctor.user.wapi.bean.DietBean;
import com.ddoctor.user.wapi.constant.Action;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.HttpHelper;
import com.ddoctor.utils.MyUtils;
import com.google.gson.Gson;

/**
 * 获取饮食记录详情
 * @author 萧
 * @Date 2015-6-2下午10:34:13
 * @TODO TODO
 */
public class GetDietDetailTask extends BaseAsyncTask<String, Void, DDResult>{

	private int dietId;
	
	
	public GetDietDetailTask(int dietId) {
		super();
		this.dietId = dietId;
	}

	@Override
	protected DDResult doInBackground(String... params) {
		try {
			return  getDietDetail();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return DDResult.makeResult(RetError.ERROR);
	}

	private DDResult getDietDetail() {
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put(AppBuildConfig.ACTID, Action.GET_DIETRECORD);
			jsonObject.put(AppBuildConfig.PATIENTID, DataModule.getInstance()
					.getLoginedUserId());
			jsonObject.put("recordId", dietId);

			MyUtils.showLog("查询饮食详情   "+jsonObject.toString());
			
			String resp_string = HttpHelper.http_post(WAPI.WAPI_BASE_URL,
					jsonObject.toString());
			if (resp_string == null) {
				return DDResult.makeResult(RetError.NETWORK_ERROR);
			}

			MyUtils.showLog(resp_string);
			Gson gson = new Gson();
			RespBean respBean = gson.fromJson(resp_string, RespBean.class);
			if (respBean == null) {
				return DDResult.makeResult(RetError.API_INTERFACE);
			}

			if( respBean.isSuccess() ) 
			{
				DDResult ret = DDResult.makeResult(RetError.NONE);
				
				JSONObject respObj = new JSONObject(resp_string);
				if( respObj.has("diet") )
				{
					respObj = respObj.getJSONObject("diet");
					if( respObj != null ) 
					{
						DietBean dietBean = gson.fromJson(respObj.toString(), DietBean.class);
						Bundle data = new Bundle();
						data.putParcelable("data", dietBean);
						ret.setBundle(data);
					}
				}
				return ret;
			}
			else 
			{
				return DDResult.makeResult(RetError.API_INTERFACE, respBean.getErrMsg());
			}
			
		} 
		catch (Exception e) {

		}
		
		return DDResult.makeResult(RetError.ERROR);
	}
	

	
}
