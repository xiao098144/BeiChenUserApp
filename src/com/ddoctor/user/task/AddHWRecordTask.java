package com.ddoctor.user.task;

import org.json.JSONObject;

import com.ddoctor.enums.RetError;
import com.ddoctor.user.config.AppBuildConfig;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.wapi.RespBean;
import com.ddoctor.user.wapi.WAPI;
import com.ddoctor.user.wapi.bean.HeightBean;
import com.ddoctor.user.wapi.constant.Action;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.HttpHelper;
import com.ddoctor.utils.MyUtils;
import com.google.gson.Gson;

public class AddHWRecordTask extends BaseAsyncTask<String, Void, DDResult>{

	private HeightBean heightBean;
	
	
	public AddHWRecordTask(HeightBean heightBean) {
		super();
		this.heightBean = heightBean;
	}

	@Override
	protected DDResult doInBackground(String... params) {
		try {
			return addHba1cRecord();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return DDResult.makeResult(RetError.ERROR);
	}
	
	private DDResult addHba1cRecord(){
		try {
			Gson gson = new Gson();
			
			JSONObject jsonObject = new JSONObject();
			jsonObject.put(AppBuildConfig.ACTID, Action.DO_HEIGHT);
			jsonObject.put(AppBuildConfig.PATIENTID, DataModule.getInstance().getLoginedUserId());
			jsonObject.put("height", gson.toJson(heightBean));
			
			String resp_string = HttpHelper.http_post(WAPI.WAPI_BASE_URL,
					jsonObject.toString());
			if (resp_string == null) {
				return DDResult.makeResult(RetError.NETWORK_ERROR);
			}

			MyUtils.showLog(resp_string);
			
			RespBean respBean = gson.fromJson(resp_string, RespBean.class);
			if (respBean != null) {
				if (respBean.isSuccess()) {
					DDResult ret = DDResult.makeResult(RetError.NONE);
					return ret;
				} else {
					return DDResult.makeResult(RetError.API_INTERFACE,
							respBean.getErrMsg());
				}
			} else
				return DDResult.makeResult(RetError.API_INTERFACE);
			
		} catch (Exception e) {
			
		}
		return DDResult.makeResult(RetError.ERROR);
	}

}
