package com.ddoctor.user.task;

import org.json.JSONObject;

import android.os.Bundle;
import android.text.TextUtils;

import com.ddoctor.enums.RetError;
import com.ddoctor.user.config.AppBuildConfig;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.wapi.RespBean;
import com.ddoctor.user.wapi.WAPI;
import com.ddoctor.user.wapi.constant.Action;
import com.ddoctor.user.wapi.constant.UserType;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.HttpHelper;
import com.ddoctor.utils.MyUtils;
import com.google.gson.Gson;

/**
 * 获取积分余额
 * @author 萧
 * @Date 2015-6-2下午10:34:13
 * @TODO TODO
 */
public class GetIntegralBalanceTask extends BaseAsyncTask<String, Void, DDResult>{

	@Override
	protected DDResult doInBackground(String... params) {
		try {
			return getIntegralBalance();
		} catch (Exception e) {
		}

		return DDResult.makeResult(RetError.ERROR);
	}
	
	private DDResult getIntegralBalance(){
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put(AppBuildConfig.ACTID, Action.GET_POINT_TOTAL);
			jsonObject.put(AppBuildConfig.PATIENTID, DataModule.getInstance()
					.getLoginedUserId());
			jsonObject.put("doctorId", 0);
			jsonObject.put("userType", UserType.PATIENT);
			
			String resp_string = HttpHelper.http_post(WAPI.WAPI_BASE_URL,
					jsonObject.toString());
			if (TextUtils.isEmpty(resp_string)) {
				return DDResult.makeResult(RetError.NETWORK_ERROR);
			}
			
			MyUtils.showLog("获取积分余额记录  "+resp_string);
			
			Gson gson = new Gson();
			RespBean respBean = gson.fromJson(resp_string, RespBean.class);
			if (respBean != null && respBean.isSuccess() )
			{
				JSONObject respObj = new JSONObject(resp_string);
				if (respObj.has("point")) {
					Bundle data = new Bundle();
					data.putInt("point", respObj.optInt("point",0));
					
					DDResult ret = DDResult.makeResult(RetError.NONE);
					ret.setBundle(data);
					return ret;
				}
			}
			
			if( respBean != null )
				return DDResult.makeResult(RetError.API_INTERFACE, respBean.getErrMsg());
			else
				return DDResult.makeResult(RetError.API_INTERFACE);
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		return DDResult.makeResult(RetError.ERROR);
				
	}
	
}
