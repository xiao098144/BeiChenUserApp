package com.ddoctor.user.task;

import org.json.JSONObject;

import com.ddoctor.enums.RetError;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.data.MyProfile;
import com.ddoctor.user.wapi.RespBean;
import com.ddoctor.user.wapi.WAPI;
import com.ddoctor.user.wapi.bean.PatientBean;
import com.ddoctor.user.wapi.constant.Action;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.HttpHelper;
import com.ddoctor.utils.MyUtils;
import com.google.gson.Gson;

public class QuickLoginTask extends BaseAsyncTask<String, Void, DDResult> {
	String quickId;
	int quickType;

	public QuickLoginTask(String quickId, int quickType) {
		super();
		this.quickId = quickId;
		this.quickType = quickType;
	}

	@Override
	protected DDResult doInBackground(String... params) {
		try {
			return quickLogin();
		} catch (Exception e) {
		}

		return DDResult.makeResult(RetError.ERROR);
	}

	public DDResult quickLogin() {
		try {
			JSONObject json = new JSONObject();
			json.put("actId", Action.PATIENT_QUICK_LOGIN);
			json.put("quickId", quickId);
			json.put("quickType", quickType);
			json.put("channel", MyProfile.getChannelId());
			json.put("uuid", DataModule.getUUID());

			MyUtils.showLog(json.toString());

			String resp_string = HttpHelper.http_post(WAPI.WAPI_BASE_URL,
					json.toString());
			if (resp_string == null) {
				return DDResult.makeResult(RetError.NETWORK_ERROR);
			}

			MyUtils.showLog(resp_string);

			Gson gson = new Gson();
			RespBean respBean = gson.fromJson(resp_string, RespBean.class);
			if (respBean == null) {
				return DDResult.makeResult(RetError.API_INTERFACE);
			}

			if (respBean.isSuccess()) {
				JSONObject rootObj = new JSONObject(resp_string);
				if (rootObj.has("patient")) {
					rootObj = rootObj.optJSONObject("patient");
					DDResult ret = DDResult.makeResult(RetError.NONE);
					DataModule.getInstance()
							.saveLoginedUserInfo(
									gson.fromJson(rootObj.toString(),
											PatientBean.class));
					return ret;
				}

			} else
				return DDResult.makeResult(RetError.API_INTERFACE,
						respBean.getErrMsg());

			return DDResult.makeResult(RetError.API_INTERFACE);

		} catch (Exception e) {

		}
		return DDResult.makeResult(RetError.ERROR);
	}

}
