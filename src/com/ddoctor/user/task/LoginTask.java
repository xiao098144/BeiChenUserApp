package com.ddoctor.user.task;

import org.json.JSONObject;

import com.ddoctor.enums.RetError;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.data.MyProfile;
import com.ddoctor.user.wapi.RespBean;
import com.ddoctor.user.wapi.WAPI;
import com.ddoctor.user.wapi.bean.PatientBean;
import com.ddoctor.user.wapi.constant.Action;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.HttpHelper;
import com.ddoctor.utils.MyUtils;
import com.google.gson.Gson;


public class LoginTask extends BaseAsyncTask<String, Void, DDResult> {
	private String _mobile;
	private String _password;
	
	public LoginTask(String mobile, String password)
	{
		_mobile = mobile;
		_password = password;
	}

	@Override
	protected DDResult doInBackground(String... params) {
		DDResult ret = DDResult.makeResult(RetError.ERROR);
		try{
			
			// 访问登录接口，获取返回值，解析返回值
			RespBean respBean = new RespBean();
			PatientBean patientBean = new PatientBean();
			ret = do_login_request(_mobile, _password, respBean, patientBean);
			if( ret.getError() == RetError.NONE )
			{
				if( respBean.isSuccess() )
				{
					// 解析返回的结果
					// 登录成功后获取到的信息保存到LoginResp里
					
					ret = DDResult.makeResult(RetError.NONE);
					ret.setObject(patientBean);
				}
				else
				{
					ret = DDResult.makeResult(RetError.API_INTERFACE);
					ret.setErrorMessage(respBean.errMsg);
				}
			}
		}
		catch(Exception e){
		}
		
		return ret;
	}
	
	// 登录接口
	public static DDResult do_login_request(String mobile, String password, RespBean respBean, PatientBean patientBean)
	{
		try{
			// 构建请求json串
			JSONObject json = new JSONObject();
			json.put("mobile", mobile);
			json.put("password", password);
			json.put("actId", Action.PATIENT_LOGIN);
			
			MyUtils.showLog(json.toString());
			
			// 发起http请求
			String resp_string = HttpHelper.http_post(WAPI.WAPI_BASE_URL, json.toString());
			if( resp_string == null )
				return DDResult.makeResult(RetError.NETWORK_ERROR);
			MyUtils.showLog("","登陆反馈的数据 "+resp_string);
			// 接口调用成功，解析返回结果
			Gson gson = new Gson();
			RespBean resp = gson.fromJson(resp_string, RespBean.class);
			if( resp == null )
				return DDResult.makeResult(RetError.API_INTERFACE);
			
			respBean.copyFrom(resp);
			if( respBean.isSuccess() )
			{
				JSONObject respObj = new JSONObject(resp_string);
				JSONObject patientObj = respObj.getJSONObject("patient");
				PatientBean patient = gson.fromJson(patientObj.toString(), PatientBean.class);
				if( patient != null )
					patientBean.copyFrom(patient);
				
				// 天士力
				// 需要先存一个userid
				MyProfile.setLoginedUserId(patient.getId());
				DataModule.getInstance().updateTSLUserInfo(respObj);
			}
			
			return DDResult.makeResult(RetError.NONE);
		}
		catch(Exception e)
		{
			
		}
		
		return DDResult.makeResult(RetError.ERROR);
	}

	
}
