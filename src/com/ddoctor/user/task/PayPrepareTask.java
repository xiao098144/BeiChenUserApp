package com.ddoctor.user.task;

import java.util.List;

import org.json.JSONObject;

import android.os.Bundle;
import android.text.TextUtils;

import com.ddoctor.enums.RetError;
import com.ddoctor.user.config.AppBuildConfig;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.wapi.RespBean;
import com.ddoctor.user.wapi.WAPI;
import com.ddoctor.user.wapi.bean.OrderBean;
import com.ddoctor.user.wapi.bean.ProductBean;
import com.ddoctor.user.wapi.constant.Action;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.HttpHelper;
import com.ddoctor.utils.MyUtils;
import com.google.gson.Gson;

public class PayPrepareTask extends BaseAsyncTask<String, Void, DDResult>{

	List<ProductBean> list;
	
	
	public PayPrepareTask(List<ProductBean> list) {
		super();
		this.list = list;
	}


	@Override
	protected DDResult doInBackground(String... params) {
		try {
			return payPrepare();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return DDResult.makeResult(RetError.ERROR);
	}

	private DDResult payPrepare(){
		try {
			Gson gson = new Gson();
			JSONObject jsonObject = new JSONObject();
			jsonObject.put(AppBuildConfig.ACTID, Action.GET_PAYPREPARE);
			jsonObject.put(AppBuildConfig.PATIENTID, DataModule.getInstance().getLoginedUserId());
			jsonObject.put("recordList", gson.toJson(list));
			
			MyUtils.showLog("提交的数据   "+jsonObject.toString());
			
			String resp_string = HttpHelper.http_post(WAPI.WAPI_BASE_URL, jsonObject.toString());
			if (TextUtils.isEmpty(resp_string)) {
				return DDResult.makeResult(RetError.NETWORK_ERROR);
			}
			
			MyUtils.showLog(resp_string);
			
			RespBean respBean = gson.fromJson(resp_string, RespBean.class);
			if( respBean != null )
			{
				if( respBean.isSuccess() )
				{
					JSONObject respObject = new JSONObject(resp_string);
					OrderBean orderBean = gson.fromJson(respObject.optJSONObject("order").toString(),OrderBean.class);
					
					DDResult ret = DDResult.makeResult(RetError.NONE);
					
					Bundle b = new Bundle();
					b.putParcelable("order",orderBean);
					ret.setBundle(b);
					
					return ret;
				}
				else
				{
					return DDResult.makeResult(RetError.API_INTERFACE, respBean.getErrMsg());
				}
			}
			else
				return DDResult.makeResult(RetError.API_INTERFACE);
			
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return DDResult.makeResult(RetError.ERROR);
	}
}
