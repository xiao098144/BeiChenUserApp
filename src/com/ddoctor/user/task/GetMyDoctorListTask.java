package com.ddoctor.user.task;

import com.ddoctor.enums.RetError;
import com.ddoctor.user.wapi.RespBean;

/**
 * 获取我的医生 
 * @author 萧
 * @Date 2015-6-2下午10:34:13
 * @TODO TODO
 */
public class GetMyDoctorListTask extends BaseAsyncTask<String, Void, RetError>{

	@Override
	protected RetError doInBackground(String... params) {
		RetError ret = RetError.NONE;
		try{
			RespBean respBean = new RespBean();
			if( ret == RetError.NONE )
			{
				if( respBean.isSuccess() )
				{
					ret = RetError.NONE;
//					Bundle data = new Bundle();
//					data.putParcelableArrayList("list", dataList);
//					ret.setBundle(data);
				}
				else
				{
					ret = RetError.API_INTERFACE;
					ret.setErrorMessage(respBean.errMsg);
				}
			}
		}
		catch(Exception e){
		}
		
		return ret;
	}
	
	
}
