package com.ddoctor.user.task;

import org.json.JSONObject;

import android.text.TextUtils;

import com.ddoctor.enums.RetError;
import com.ddoctor.user.config.AppBuildConfig;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.wapi.RespBean;
import com.ddoctor.user.wapi.WAPI;
import com.ddoctor.user.wapi.bean.BloodBean;
import com.ddoctor.user.wapi.constant.Action;
import com.ddoctor.utils.HttpHelper;
import com.ddoctor.utils.MyUtils;
import com.google.gson.Gson;

public class AddBPRecordTask extends BaseAsyncTask<String, Void, RetError>{

	private BloodBean bloodBean;
	
	
	public AddBPRecordTask(BloodBean bloodBean) {
		super();
		this.bloodBean = bloodBean;
	}

	@Override
	protected RetError doInBackground(String... params) {
		RetError ret = RetError.ERROR;
		try {
			RespBean respBean = new RespBean();
			ret = addHba1cRecord(respBean);
			if (ret == RetError.NONE) {
				if (respBean.isSuccess()) {
					ret = RetError.NONE;
				} else {
					ret = RetError.API_INTERFACE;
					ret.setErrorMessage(respBean.errMsg);
				}
			}
		} catch (Exception e) {
		}

		return ret;
	}
	
	private RetError addHba1cRecord(RespBean respBean){
		try {
			Gson gson = new Gson();
			
			JSONObject jsonObject = new JSONObject();
			jsonObject.put(AppBuildConfig.ACTID, Action.DO_BLOODPRESSURE);
			jsonObject.put(AppBuildConfig.PATIENTID, DataModule.getInstance().getLoginedUserId());
			jsonObject.put("blood", gson.toJson(bloodBean));
			
			String resp_string = HttpHelper.http_post(WAPI.WAPI_BASE_URL,
					jsonObject.toString());
			if (TextUtils.isEmpty(resp_string)) {
				return RetError.NETWORK_ERROR;
			}
			MyUtils.showLog("添加血压记录  "+resp_string);
			RespBean resp = gson.fromJson(resp_string, RespBean.class);
			if (resp == null) {
				return RetError.API_INTERFACE;
			}
			respBean.copyFrom(resp);
			if (respBean.isSuccess()) {
				return RetError.NONE;
			} else {
				RetError.SERVER_ERROR.setErrorMessage(respBean.getErrMsg());
				return RetError.SERVER_ERROR;
			}
		} catch (Exception e) {
			
		}
		return RetError.ERROR;
	}

}
