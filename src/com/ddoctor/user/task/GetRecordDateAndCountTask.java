package com.ddoctor.user.task;

import org.json.JSONObject;

import android.os.Bundle;
import android.text.TextUtils;

import com.ddoctor.enums.RetError;
import com.ddoctor.user.config.AppBuildConfig;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.wapi.RespBean;
import com.ddoctor.user.wapi.WAPI;
import com.ddoctor.user.wapi.constant.Action;
import com.ddoctor.user.wapi.constant.UserType;
import com.ddoctor.utils.HttpHelper;
import com.ddoctor.utils.MyUtils;
import com.google.gson.Gson;

/**
 * 获取记录总天数与总条数
 * @author 萧
 * @Date 2015-6-3下午10:53:26
 * @TODO TODO
 */
public class GetRecordDateAndCountTask extends BaseAsyncTask<String, Void, RetError>{

	private int type;
	
	public GetRecordDateAndCountTask(int type) {
		this.type = type;
	}


	@Override
	protected RetError doInBackground(String... params) {
		RetError ret = RetError.NONE;
		try{
			RespBean respBean = new RespBean();
			int date = 0;
			int count = 0;
			ret = getRecordDateAndCount(respBean , date , count);
			if( ret == RetError.NONE )
			{
				if( respBean.isSuccess() )
				{
					ret = RetError.NONE;
				}
				else
				{
					ret = RetError.API_INTERFACE;
					ret.setErrorMessage(respBean.errMsg);
				}
			}
		}
		catch(Exception e){
		}
		
		return ret;
	}

	private RetError getRecordDateAndCount(RespBean respBean, int date, int count){
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put(AppBuildConfig.ACTID, Action.GET_RECORD_TOTAL);
			jsonObject.put(AppBuildConfig.PATIENTID, DataModule.getInstance().getLoginedUserId());
			jsonObject.put("userType", UserType.PATIENT);
			jsonObject.put("type", type);
			
			String resp_string = HttpHelper.http_post(WAPI.WAPI_BASE_URL,jsonObject.toString());
			if (TextUtils.isEmpty(resp_string)) {
				return RetError.NETWORK_ERROR;
			}
			Gson gson = new Gson();
			RespBean resp = gson.fromJson(resp_string, RespBean.class);
			if( resp == null ){
				return RetError.API_INTERFACE;
			}
			MyUtils.showLog("", "记录总数   返回的数据 "+resp_string);
			respBean.copyFrom(resp);
			if( respBean.isSuccess() )
			{
				JSONObject jsonObject2 = new JSONObject(resp_string);
				Bundle data = new Bundle();
				data.putInt("date", jsonObject2.optInt("totalDay",0));
				data.putFloat("count", jsonObject2.optInt("totalRecord",0));
				RetError.NONE.setBundle(data);
				return RetError.NONE;
			}
		} catch (Exception e) {
			
		}
		return RetError.ERROR;
	}
	
}
