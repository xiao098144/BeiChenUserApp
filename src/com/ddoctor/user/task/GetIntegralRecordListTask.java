package com.ddoctor.user.task;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.os.Bundle;

import com.ddoctor.enums.RetError;
import com.ddoctor.user.config.AppBuildConfig;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.wapi.RespBean;
import com.ddoctor.user.wapi.WAPI;
import com.ddoctor.user.wapi.bean.PointBean;
import com.ddoctor.user.wapi.constant.Action;
import com.ddoctor.user.wapi.constant.UserType;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.HttpHelper;
import com.ddoctor.utils.MyUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * 获取积分列表
 * 
 * @author 萧
 * @Date 2015-6-2下午10:34:13
 * @TODO TODO
 */
public class GetIntegralRecordListTask extends
		BaseAsyncTask<String, Void, DDResult> {

	private int page;

	public GetIntegralRecordListTask(int page) {
		super();
		this.page = page;
	}

	@Override
	protected DDResult doInBackground(String... params) {
		try{
			return do_get_integralRecord_list_request();
		}
		catch(Exception e){
		}
		
		return DDResult.makeResult(RetError.ERROR, null);
	}

	private DDResult do_get_integralRecord_list_request() {

		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put(AppBuildConfig.ACTID, Action.GET_POINT_LIST);
			jsonObject.put(AppBuildConfig.PATIENTID, DataModule.getInstance()
					.getLoginedUserId());
			jsonObject.put("page", page);
			jsonObject.put("doctorId", 0);
			jsonObject.put("userType", UserType.PATIENT);

			String resp_string = HttpHelper.http_post(WAPI.WAPI_BASE_URL,
					jsonObject.toString());
			if (resp_string == null) {
				return DDResult.makeResult(RetError.NETWORK_ERROR);
			}

			MyUtils.showLog(resp_string);
			Gson gson = new Gson();
			RespBean respBean = gson.fromJson(resp_string, RespBean.class);
			if (respBean == null) {
				return DDResult.makeResult(RetError.API_INTERFACE);
			}

			if (respBean.isSuccess()) {
				DDResult ret = DDResult.makeResult(RetError.NONE);
				ArrayList<PointBean> dataList = new ArrayList<PointBean>();

				JSONObject respObj = new JSONObject(resp_string);
				if (respObj.has("recordList")) {
					JSONArray listObj = respObj.getJSONArray("recordList");
					if (listObj != null) {
						Type type = new TypeToken<ArrayList<PointBean>>() {
						}.getType();
						List<PointBean> list = gson.fromJson(
								listObj.toString(), type);
						if (null != list) {
							dataList.addAll(list);
						}
					}
				}

				Bundle data = new Bundle();
				data.putParcelableArrayList("list", dataList);
				ret.setBundle(data);
				ret.setErrorMessage(respBean.getErrMsg());
				return ret;
			} else {
				return DDResult.makeResult(RetError.API_INTERFACE, respBean.getErrMsg());
			}

		} catch (Exception e) {

		}

		return DDResult.makeResult(RetError.ERROR);
	}

}
