package com.ddoctor.user.task;

import org.json.JSONObject;

import android.content.Context;
import android.os.Bundle;

import com.ddoctor.application.MyApplication;
import com.ddoctor.enums.RetError;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.wapi.RespBean;
import com.ddoctor.user.wapi.WAPI;
import com.ddoctor.user.wapi.bean.OrderBean;
import com.ddoctor.user.wapi.constant.Action;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.HttpHelper;
import com.ddoctor.utils.MyUtils;
import com.google.gson.Gson;


public class OrderSubmitTask extends BaseAsyncTask<String, Void, DDResult> {

	private OrderBean _orderBean;
	public OrderSubmitTask(OrderBean orderBean)
	{
		_orderBean = orderBean;
	}
	
	@Override
	protected DDResult doInBackground(String... params) {
		try{
			return do_order_submit_request();
			
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		return DDResult.makeResult(RetError.ERROR);
	}
	
	
	public DDResult do_order_submit_request()
	{
		Context ctx = MyApplication.getInstance().getApplicationContext();
		
		try{
			JSONObject json = new JSONObject();
			json.put("actId", Action.ADD_ORDER);
			
			if( DataModule.getInstance().isLogined() )
				json.put("patientId", DataModule.getInstance().getLoginedUserId());

			
			
			JSONObject clientInitJSONObject = WAPI.beanToJSONObject(_orderBean);
			if( clientInitJSONObject != null )
				json.put("order", clientInitJSONObject);
			
			MyUtils.showLog(json.toString());
			
			// 发起http请求
			String resp_string = HttpHelper.http_post(WAPI.WAPI_BASE_URL, json.toString());
			if( resp_string == null )
				return DDResult.makeResult(RetError.NETWORK_ERROR);
			
			MyUtils.showLog(resp_string);
			
			Gson gson = new Gson();
			RespBean respBean = gson.fromJson(resp_string, RespBean.class);
			if( respBean != null )
			{
				if( respBean.isSuccess() )
				{
					JSONObject respObject = new JSONObject(resp_string);
					int orderId = 0;
					if( respObject.has("orderId") )
						orderId = respObject.optInt("orderId");

					DDResult ret = DDResult.makeResult(RetError.NONE);
					ret.setObject(respBean);
					
					Bundle b = new Bundle();
					b.putInt("orderId", orderId);
					ret.setBundle(b);
					
					return ret;
				}
				else
				{
					return DDResult.makeResult(RetError.API_INTERFACE, respBean.getErrMsg());
				}
			}
			else
				return DDResult.makeResult(RetError.API_INTERFACE);
			
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return DDResult.makeResult(RetError.ERROR);
	}
}
