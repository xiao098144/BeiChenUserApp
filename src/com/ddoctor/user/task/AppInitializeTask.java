package com.ddoctor.user.task;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;

import com.ddoctor.application.MyApplication;
import com.ddoctor.enums.RetError;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.data.MyProfile;
import com.ddoctor.user.wapi.RespBean;
import com.ddoctor.user.wapi.WAPI;
import com.ddoctor.user.wapi.bean.ClientInitBean;
import com.ddoctor.user.wapi.bean.ClientUpdateBean;
import com.ddoctor.user.wapi.bean.MedicalBean;
import com.ddoctor.user.wapi.bean.PatientBean;
import com.ddoctor.user.wapi.constant.Action;
import com.ddoctor.user.wapi.constant.UserType;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.HttpHelper;
import com.ddoctor.utils.MyUtils;
import com.ddoctor.utils.SharedUtils;
import com.google.gson.Gson;


public class AppInitializeTask extends BaseAsyncTask<String, Void, DDResult> {

	@Override
	protected DDResult doInBackground(String... params) {
		DDResult ret;
		try{
			// 这里加入初始化操作，比如数据加载，网络访问等等耗时操作
			
			//访问init接口，如果已经登录，传入userid获取用户信息
			RespBean respBean = new RespBean();
			Map<String, Object> dataMap = new HashMap<String, Object>();
			ret = do_app_init(respBean, dataMap);
			if( ret.getError() != RetError.NONE )
			{
				ret.setObject(dataMap);
				return ret;
			}
			
			if( !respBean.isSuccess() )
			{
				ret = DDResult.makeResult(RetError.API_INTERFACE);
				ret.setErrorMessage(respBean.errMsg);
				ret.setObject(dataMap);
				return ret;
			}
			
			// 接口调用成功，设置isfirst=1 // 接口里1标识非首次 0标识首次
			MyProfile.setIntValue(MyProfile.KEY_ISFIRST, 1);
			
			ret = DDResult.makeResult(RetError.NONE);
			ret.setObject(dataMap);

			return ret;
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		return DDResult.makeResult(RetError.ERROR);
	}
	
	
	// 初始化接口
	public static DDResult do_app_init(RespBean aRespBean, Map<String, Object> dataMap)
	{
		Context ctx = MyApplication.getInstance().getApplicationContext();
		
		try{
			JSONObject json = new JSONObject();
			json.put("actId", Action.INIT_CLIENT);
			if( DataModule.getInstance().isLogined() )
				json.put("patientId", DataModule.getInstance().getLoginedUserId());
			json.put("userType", UserType.PATIENT);
			
			ClientInitBean initBean = new ClientInitBean();
			
			// isfirst: 0_第一次 1_非第一次
			int isFirst = MyProfile.getIntValue(MyProfile.KEY_ISFIRST, 0);
			initBean.setIsfirst(isFirst);
			
			// dicsn
			int dicsn = MyProfile.getIntValue(MyProfile.KEY_DICSN,  0);
			initBean.setDicsn(dicsn);
			
			if( !DataModule.checkDictFileExists() )
				initBean.setDicsn(0); // 有的字典文件不存在，强制更新全部字典文件

			// uuid
			initBean.setUuid(DataModule.getUUID());
			
			// versionName
			initBean.setVersionName(MyUtils.getVersionName(ctx));
			
			// versionCode
			initBean.setVersionCode(MyUtils.getVersionCode(ctx));
			
			// sdk
			initBean.setSdk(android.os.Build.VERSION.SDK_INT);
			
			// brand
			initBean.setBrand(android.os.Build.BRAND);
			
			//device
			initBean.setDevice(android.os.Build.DEVICE);
			
			initBean.setVersionrelease(android.os.Build.VERSION.RELEASE);
			
			initBean.setOs("android_user");
			
			initBean.setWidth(MyUtils.getScreenWidth(ctx));
			initBean.setHeight(MyUtils.getScreenHeight(ctx));
			initBean.setChannel(MyProfile.getChannelId()); // 修改成渠道ID
			
			initBean.setImei(MyUtils.getIMEI(ctx));
			initBean.setImsi(MyUtils.getIMSI(ctx));
			initBean.setNettype(MyUtils.getNetType(ctx));
			initBean.setUa(DataModule.getUserAgent());
			initBean.setMobile(SharedUtils.getMobile());
			
			initBean.setLocation(DataModule.getInstance().getAddress());
			
			initBean.setClientsn("patient");
			

			JSONObject clientInitJSONObject = WAPI.beanToJSONObject(initBean);
			if( clientInitJSONObject != null )
				json.put("clientInit", clientInitJSONObject);
			
			MyUtils.showLog(json.toString());
			
			// 发起http请求
			String resp_string = HttpHelper.http_post(WAPI.WAPI_BASE_URL, json.toString());
			if( resp_string == null )
				return DDResult.makeResult(RetError.NETWORK_ERROR);
			
			MyUtils.showLog("resp_string",resp_string);

			Gson gson = new Gson();
			RespBean respBean = gson.fromJson(resp_string, RespBean.class);
			if( respBean != null )
				aRespBean.copyFrom(respBean);
			
			if( aRespBean.isSuccess() )
			{
				JSONObject respObj = new JSONObject(resp_string);
				if(respObj.has("serverInit") )
				{
					respObj = respObj.getJSONObject("serverInit");
					
					
					/////////////////////////////////////////////////////////////////
					// 客户端升级标记
					if( respObj.has("updateTag") && respObj.optInt("updateTag") == 1 && respObj.has("update") )
					{
						// 有升级信息
						JSONObject updateObj = respObj.getJSONObject("update");
						if( updateObj != null )
						{
							ClientUpdateBean updateBean = gson.fromJson(updateObj.toString(), ClientUpdateBean.class);
							if( updateBean != null && updateBean.getIsUpdate() == 1 ) // add by alex, 加入isUpdate字段判断 2015/07/24
							{
								dataMap.put("update",  updateBean);
							}
						}
					}
					if (respObj.has("zfbBackUrl")) {
						DataModule.getInstance().saveZfbURl(respObj.optString("zfbBackUrl","http://api.ddoctor.cn/zfb"));
					}
					/////////////////////////////////////////////////////////////////
					// 字典更新
					if( respObj.has("dictag") && respObj.optInt("dictag") == 1 )
					{
						// 字典需要更新
						updateDict(respObj);
						// 将服务器端dicsn 替换本地值
						MyProfile.setIntValue(MyProfile.KEY_DICSN, respObj.optInt(MyProfile.KEY_DICSN));
					}
					
					
					/////////////////////////////////////////////////////////////////
					// 用户信息
					if( DataModule.getInstance().isLogined() )
					{
						if( respObj.has("patient") )
						{
							JSONObject patientObj = respObj.getJSONObject("patient");
							if( patientObj != null )
							{
								PatientBean patient = gson.fromJson(patientObj.toString(), PatientBean.class);
								if( patient != null )
								{
									DataModule.getInstance().saveLoginedUserInfo(patient);
								}
							}
						}
						else
						{
							// 获取用户信息出错，就当做没有登录吧，返回这个标记，让页面直接跳转到登录界面
							return DDResult.makeResult(RetError.MISSING_USER_INFO, "获取用户信息失败!");
						}
					}
					
					// 天士力
					DataModule.getInstance().updateTSLUserInfo(respObj);

				}
				else
				{
					return DDResult.makeResult(RetError.API_INTERFACE);
				}
			}
			
			MyUtils.showLog(respBean.errMsg);
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return DDResult.makeResult(RetError.NONE);
	}

	private static void updateDict(JSONObject rootObj, String dictName)
	{
//		MyUtils.showLog("updateDict "+rootObj+"  "+dictName);
		// 血糖仪
		if( rootObj.has(dictName) )
		{
			try{
				JSONArray ja = rootObj.getJSONArray(dictName);
				if( ja != null && ja.length() > 0 )
				{
					// 保存到文件

					String filename = String.format(Locale.CHINESE, "%s/%s.%s", MyProfile.getDictFilePath(), dictName, MyProfile.DICT_FILE_EXT);
					MyUtils.writeFile(filename, ja.toString());
					
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			
		}
	}
	
	// 更新字典，把读取到的json类型保存成文件，使用的时候直接加载就行
	private static void updateDict(JSONObject rootObj)
	{
		// 血糖仪
		updateDict(rootObj, MyProfile.DICT_GLUCOMETERS);//"glucometers");
		// 病症类型
		updateDict(rootObj, MyProfile.DICT_ILLNESSS);//"illnesss");
		// 不适记录类型
		updateDict(rootObj, MyProfile.DICT_TROUBLEITEMS);//"troubleitems");
		// 地区列表 
		updateDict(rootObj, MyProfile.DICT_DISTRICTS);//"districts");
		// 药品列表
		updateDict(rootObj, MyProfile.DICT_MEDICALS); // medicals
		// 医院列表
		updateDict(rootObj , MyProfile.DICT_HOSPITAL); // hospitals
		// 职位列表
		updateDict(rootObj , MyProfile.DICT_LEVELS);  // levels
		// 饮食列表
		updateDict(rootObj, MyProfile.DICT_FOOD);    // foods
		
		// for test
//		List<GlucometerBean> a = DataModule.loadDict(MyProfile.DICT_GLUCOMETERS, GlucometerBean.class);
//		if( a != null )
//		{
//			MyUtils.showLog("length=" + a.size());
//		}
	}
	
}
