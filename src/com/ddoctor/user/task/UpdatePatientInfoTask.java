package com.ddoctor.user.task;

import org.json.JSONObject;

import android.text.TextUtils;

import com.ddoctor.enums.RetError;
import com.ddoctor.user.config.AppBuildConfig;
import com.ddoctor.user.wapi.RespBean;
import com.ddoctor.user.wapi.WAPI;
import com.ddoctor.user.wapi.bean.PatientBean;
import com.ddoctor.user.wapi.constant.Action;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.HttpHelper;
import com.ddoctor.utils.MyUtils;
import com.google.gson.Gson;

/**
 * 修改用户个人信息
 * @author 萧
 * @Date 2015-6-24下午9:06:24
 * @TODO TODO
 */
public class UpdatePatientInfoTask extends BaseAsyncTask<String, Void, DDResult>{

	private PatientBean patientBean;
	
	
	public UpdatePatientInfoTask(PatientBean patientBean) {
		super();
		String image = patientBean.getImage();
		if (!TextUtils.isEmpty(image)) {
			if (image.startsWith(AppBuildConfig.SERVER)) {
				patientBean.setImage(image.replace(AppBuildConfig.SERVER, ""));
			}
		}
		this.patientBean = patientBean;
	}

	@Override
	protected DDResult doInBackground(String... params) {
		try {
			return do_update_request(patientBean);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return DDResult.makeResult(RetError.ERROR);
	}
	
	// 修改个人信息
		private static DDResult do_update_request(PatientBean patientBean)
		{
			try{
				// 构建请求json串
				JSONObject json = new JSONObject();
				json.put("actId", Action.UPDATE_PATIENT);
				json.put("patientId", patientBean.getId());
				JSONObject patientBeanJSONObject = WAPI.beanToJSONObject(patientBean);
				if( patientBeanJSONObject != null )
					json.put("patient", patientBeanJSONObject);
				
				MyUtils.showLog(json.toString());
				
				// 发起http请求
				String resp_string = HttpHelper.http_post(WAPI.WAPI_BASE_URL,
						json.toString());
				if (resp_string == null)
					return DDResult.makeResult(RetError.NETWORK_ERROR);

				MyUtils.showLog(resp_string);

				Gson gson = new Gson();
				RespBean respBean = gson.fromJson(resp_string, RespBean.class);
				if (respBean != null) {
					if (respBean.isSuccess()) {
						DDResult ret = DDResult.makeResult(RetError.NONE);
						return ret;
					} else {
						return DDResult.makeResult(RetError.API_INTERFACE,
								respBean.getErrMsg());
					}
				} else
					return DDResult.makeResult(RetError.API_INTERFACE);

			} catch (Exception e) {
				e.printStackTrace();
			}

			return DDResult.makeResult(RetError.ERROR);
		}

}
