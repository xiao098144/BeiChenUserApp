package com.ddoctor.user.task;

import org.json.JSONObject;

import android.text.TextUtils;

import com.ddoctor.enums.RetError;
import com.ddoctor.user.config.AppBuildConfig;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.wapi.RespBean;
import com.ddoctor.user.wapi.WAPI;
import com.ddoctor.user.wapi.bean.SugarValueBean;
import com.ddoctor.user.wapi.constant.Action;
import com.ddoctor.utils.HttpHelper;
import com.ddoctor.utils.MyUtils;
import com.google.gson.Gson;

/**
 * 添加或修改血糖记录
 * 
 * @author 萧
 * @Date 2015-6-2下午10:20:28
 * @TODO TODO
 */
public class AddUpdateSugarRecordTask extends
		BaseAsyncTask<String, Void, RetError> {

	private SugarValueBean sugarValueBean;

	public AddUpdateSugarRecordTask(SugarValueBean sugarValueBean) {
		this.sugarValueBean = sugarValueBean;
	}

	@Override
	protected RetError doInBackground(String... params) {
		RetError ret = RetError.NONE;
		try {
			RespBean respBean = new RespBean();
			ret = managerSugarRecord(respBean);
			if (ret == RetError.NONE) {
				if (respBean.isSuccess()) {
					ret = RetError.NONE;
				} else {
					ret = RetError.API_INTERFACE;
					ret.setErrorMessage(respBean.errMsg);
				}
			}
		} catch (Exception e) {
		}

		return ret;
	}

	private RetError managerSugarRecord(RespBean respBean) {
		try {
			Gson gson = new Gson();

			JSONObject jsonObject = new JSONObject();
			jsonObject.put(AppBuildConfig.ACTID, Action.DO_SUGARVALUE);
			jsonObject.put(AppBuildConfig.PATIENTID, DataModule.getInstance()
					.getLoginedUserId());
			jsonObject.put("sugar", gson.toJson(sugarValueBean));

			MyUtils.showLog(" 修改血糖  提交数据  "+jsonObject.toString());
			
			String resp_string = HttpHelper.http_post(WAPI.WAPI_BASE_URL,
					jsonObject.toString());
			MyUtils.showLog("血糖反馈数据  "+resp_string);
			if (TextUtils.isEmpty(resp_string)) {
				return RetError.NETWORK_ERROR;
			}
			RespBean resp = gson.fromJson(resp_string, RespBean.class);
			if (resp == null) {
				return RetError.API_INTERFACE;
			}
			respBean.copyFrom(resp);
			if (respBean.isSuccess()) {
				return RetError.NONE;
			} else {
				RetError.SERVER_ERROR.setErrorMessage(respBean.getErrMsg());
				return RetError.SERVER_ERROR;
			}
		} catch (Exception e) {

		}
		return RetError.ERROR;
	}

}
