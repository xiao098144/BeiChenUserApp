package com.ddoctor.user.task;

import org.json.JSONObject;

import android.os.Bundle;

import com.ddoctor.enums.RetError;
import com.ddoctor.user.config.AppBuildConfig;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.wapi.RespBean;
import com.ddoctor.user.wapi.WAPI;
import com.ddoctor.user.wapi.bean.EvaluationBean;
import com.ddoctor.user.wapi.constant.Action;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.HttpHelper;
import com.ddoctor.utils.MyUtils;
import com.google.gson.Gson;

/**
 *Date:2015-6-28上午2:06:11
 *Author:Administrator
 *TODO:TODO
 */
public class GetHealthEvaluationReportTask extends BaseAsyncTask<String, Void, DDResult>{

	private int count;
	
	
	public GetHealthEvaluationReportTask(int count) {
		super();
		this.count = count;
	}

	@Override
	protected DDResult doInBackground(String... params) {
		try {
			return getHealthEvaluationReport();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return DDResult.makeResult(RetError.ERROR);
	}

	private DDResult getHealthEvaluationReport() {
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put(AppBuildConfig.ACTID, Action.GET_EVALUATION);
			jsonObject.put(AppBuildConfig.PATIENTID, DataModule.getInstance().getLoginedUserId());
			jsonObject.put("totalMedicalRemind", count);
			
			MyUtils.showLog("提交的数据  "+jsonObject.toString());
			
			String resp_string = HttpHelper.http_post(WAPI.WAPI_BASE_URL, jsonObject.toString());
			MyUtils.showLog("健康报告反馈数据  "+String.valueOf(resp_string));
			if (resp_string == null) {
				return DDResult.makeResult(RetError.NETWORK_ERROR);
			}

			Gson gson = new Gson();
			RespBean respBean = gson.fromJson(resp_string, RespBean.class);
			if (respBean == null) {
				return DDResult.makeResult(RetError.API_INTERFACE);
			}
			if (respBean.isSuccess()) 
			{
				JSONObject rootObj = new JSONObject(resp_string);
				if( rootObj.has("evalueat") )
				{
					 	rootObj = rootObj.getJSONObject("evalueat");
					 	EvaluationBean evaluationBean = gson.fromJson(rootObj.toString(), EvaluationBean.class);
						DDResult ret = DDResult.makeResult(RetError.NONE);
						Bundle b = new Bundle();
						b.putParcelable("data", evaluationBean);
						ret.setBundle(b);
						ret.setErrorMessage(respBean.getErrMsg());
						return ret;
				}
				
			}
			else
				return DDResult.makeResult(RetError.API_INTERFACE, respBean.getErrMsg());
			
			return DDResult.makeResult(RetError.API_INTERFACE);
			
		} catch (Exception e) {

		}
		return DDResult.makeResult(RetError.ERROR);
	}
	
}

		