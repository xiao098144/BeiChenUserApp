package com.ddoctor.user.view;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.utils.MyUtils;

public class DDPullToRefreshView extends LinearLayout {
	private static final String TAG = "DDPullToRefreshView";

	// refresh states
	private static final int REFRESH_STATE_NONE = 0;	// 下拉刷新
	private static final int REFRESH_STATE_PULL_TO_REFRESH = 1;	// 下拉刷新
	private static final int REFRESH_STATE_RELEASE_TO_REFRESH = 2; // 释放刷新
	private static final int REFRESH_STATE_REFRESHING = 3;	// 正在刷新...
	private int _refreshState = REFRESH_STATE_NONE;

	// pull state
	
	private static final int PULL_STATE_NONE = 0;		// 上拉
	private static final int PULL_STATE_DOWN = 1; // 下拉
	private static final int PULL_STATE_UP = 2;		// 上拉
	private int _pullState = PULL_STATE_NONE;
	
	private boolean _bDownEvent = false;
		
	private int _lastMotionX;
	private int _lastMotionY;
	private View _headerView;

	private AdapterView<?> _adapterView;
	private ScrollView _scrollView;
	
	private int _headerViewHeight;
	private ImageView _headerImageView;
	private TextView _headerTextView;
	private LayoutInflater _inflater;

	
	private OnHeaderRefreshListener _onHeaderRefreshListener;

	public DDPullToRefreshView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public DDPullToRefreshView(Context context) {
		super(context);
		init();
	}

	private void init() {
		_inflater = LayoutInflater.from(getContext());
		// header view 在此添加,保证是第一个添加到linearlayout的最上端
		addHeaderView();
	}
	
	private void addHeaderView() {
		// header view
		_headerView = _inflater.inflate(R.layout.refresh_header, this, false);
//		_headerView.setBackgroundColor(Color.RED);

		_headerImageView = (ImageView) _headerView
				.findViewById(R.id.pull_to_refresh_image);
		
		// 这里要停止一下动画,在某些小米机器上会自动播放动画
		AnimationDrawable ad = (AnimationDrawable) _headerImageView.getBackground();
		ad.stop();
		ad.selectDrawable(0);
		
		
		_headerTextView = (TextView) _headerView
				.findViewById(R.id.pull_to_refresh_text);
		
		
		// header layout
		measureView(_headerView);
		_headerViewHeight = _headerView.getMeasuredHeight();
		LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, _headerViewHeight);
		// 设置topMargin的值为负的header View高度,即将其隐藏在最上方
		params.topMargin = -(_headerViewHeight);
		addView(_headerView, params);
	}


	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();
		// footer view 在此添加保证添加到linearlayout中的最后
		
		initContentAdapterView();
	}

	/**
	 * init AdapterView like ListView,GridView and so on;or init ScrollView
	 * 
	 * @description hylin 2012-7-30下午8:48:12
	 */
	private void initContentAdapterView() {
		int count = getChildCount();
		if (count < 2) {
			throw new IllegalArgumentException(
					"this layout must contain 2 child views,and AdapterView or ScrollView must in the second position!");
		}
		
		View view = null;
		for (int i = 0; i < count; i++) {
			view = getChildAt(i);
			if (view instanceof AdapterView<?>) {
				_adapterView = (AdapterView<?>) view;
			}
			if (view instanceof ScrollView) {
				// finish later
				_scrollView = (ScrollView) view;
			}
		}
		
		if (_adapterView == null && _scrollView == null) {
			throw new IllegalArgumentException(
					"must contain a AdapterView or ScrollView in this layout!");
		}
	}

	private void measureView(View child) {
		ViewGroup.LayoutParams p = child.getLayoutParams();
		if (p == null) {
			p = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
					ViewGroup.LayoutParams.WRAP_CONTENT);
		}

		int childWidthSpec = ViewGroup.getChildMeasureSpec(0, 0 + 0, p.width);
		int lpHeight = p.height;
		int childHeightSpec;
		if (lpHeight > 0) {
			childHeightSpec = MeasureSpec.makeMeasureSpec(lpHeight,
					MeasureSpec.EXACTLY);
		} else {
			childHeightSpec = MeasureSpec.makeMeasureSpec(0,
					MeasureSpec.UNSPECIFIED);
		}
		child.measure(childWidthSpec, childHeightSpec);
	}

	@Override
	public boolean onInterceptTouchEvent(MotionEvent e) {
		int y = (int) e.getRawY();
		int x = (int) e.getRawX();
		switch (e.getAction()) {
		case MotionEvent.ACTION_DOWN:
			// 首先拦截down事件,记录y坐标
			_lastMotionY = y;
			_lastMotionX = x;
			_pullState = PULL_STATE_NONE;
			break;
		case MotionEvent.ACTION_MOVE:
			// deltaY > 0 是向下运动,< 0是向上运动
			int deltaY = y - _lastMotionY;
			int deltaX = x - _lastMotionX;
			if( Math.abs(deltaY) > Math.abs(deltaX) ) // 上下滑动距离必须大于左右滑动距离
			{
				if (isRefreshViewScroll(deltaY)) {
					return true; // 自己处理，不传递给listView了， listView不会滚动了
				}
			}
			break;
		case MotionEvent.ACTION_UP:
		case MotionEvent.ACTION_CANCEL:
			break;
		}
		return false;
	}

	/*
	 * 如果在onInterceptTouchEvent()方法中没有拦截(即onInterceptTouchEvent()方法中 return
	 * false)则由PullToRefreshView 的子View来处理;否则由下面的方法来处理(即由PullToRefreshView自己来处理)
	 */
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		int x = (int)event.getRawX();
		int y = (int) event.getRawY();
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			break;
		case MotionEvent.ACTION_MOVE:
			int deltaY = y - _lastMotionY;
			if (_pullState == PULL_STATE_DOWN) {
				// PullToRefreshView执行下拉
				headerPrepareToRefresh(deltaY);
			}
			else
			{
				if( _refreshState == REFRESH_STATE_REFRESHING )
				{
					return false;
				}
			}
			_lastMotionY = y;
			break;
		case MotionEvent.ACTION_UP:
		case MotionEvent.ACTION_CANCEL:
			int topMargin = getHeaderTopMargin();
			if (_pullState == PULL_STATE_DOWN) {

				if (topMargin >= 0) {
					// 开始刷新
					headerRefreshing();
				} else {
					// 还没有执行刷新，重新隐藏
					setHeaderTopMargin(-_headerViewHeight);
				}
			}
			else
				MyUtils.showLog("onTouchEvent:up: 2222");

			break;
		}
		return super.onTouchEvent(event);
	}

	/**
	 * 是否应该到了父View,即PullToRefreshView滑动
	 * 
	 * @param deltaY
	 *            , deltaY > 0 是向下运动,< 0是向上运动
	 * @return
	 */
	private boolean isRefreshViewScroll(int deltaY) {
		if (_refreshState == REFRESH_STATE_REFRESHING ) {
			return true;
		}
		
		// 对于ListView和GridView
		if (_adapterView != null) 
		{
			// 子view(ListView or GridView)滑动到最顶端
			if (deltaY > 0) 
			{ // 向下滑动
				View child = _adapterView.getChildAt(0);
				if (child == null) {
					// 如果mAdapterView中没有数据,不拦截
					return false;
				}
				
				if (_adapterView.getFirstVisiblePosition() == 0
						&& child.getTop() == 0) {
					_pullState = PULL_STATE_DOWN; // 开始下拉
					return true;
				}
				
//				int top = child.getTop();
//				int padding = mAdapterView.getPaddingTop();
//				if (mAdapterView.getFirstVisiblePosition() == 0
//						&& Math.abs(top - padding) <= 11) {// 这里之前用3可以判断,但现在不行,还没找到原因
//					mPullState = PULL_DOWN_STATE;
//					return true;
//				}

			}
		}
		
		// 对于ScrollView
		if (_scrollView != null) {
			// 子scroll view滑动到最顶端
			if (deltaY > 0 && _scrollView.getScrollY() == 0) {
				_pullState = PULL_STATE_DOWN;
				return true;
			}
		}
		return false;
	}

	/**
	 * header 准备刷新,手指移动过程,还没有释放
	 * 
	 * @param deltaY
	 *            ,手指滑动的距离
	 */
	private void headerPrepareToRefresh(int deltaY) 
	{
		int newTopMargin = changingHeaderViewTopMargin(deltaY);
		
		// 当header view的topMargin>=0时，说明已经完全显示出来了,修改header view 的提示状态
		if( _refreshState != REFRESH_STATE_REFRESHING )
		{
			if (newTopMargin >= 0 && _refreshState != REFRESH_STATE_RELEASE_TO_REFRESH) {
				_headerTextView.setText("释放刷新...");//R.string.pull_to_refresh_release_label);
				_refreshState = REFRESH_STATE_RELEASE_TO_REFRESH;
			} else if (newTopMargin < 0 && newTopMargin > -_headerViewHeight) {// 拖动时没有释放
				_headerTextView.setText("下拉刷新...");//R.string.pull_to_refresh_pull_label);
				_refreshState = REFRESH_STATE_PULL_TO_REFRESH;
			}
		}
		// else 正在刷新中，不改变什么...
	}


	/**
	 * 修改Header view top margin的值
	 * 
	 * @description
	 * @param deltaY
	 * @return hylin 2012-7-31下午1:14:31
	 */
	private int changingHeaderViewTopMargin(int deltaY) {
		LayoutParams params = (LayoutParams) _headerView.getLayoutParams();
		float newTopMargin;
		
		if( deltaY > 0 )
			newTopMargin = params.topMargin + deltaY * 0.3f; // *0.3 加一个下拉阻力，会有个橡皮筋的效果
		else
			newTopMargin = params.topMargin + deltaY;
		
		
		if( (int)newTopMargin < -_headerViewHeight )
			newTopMargin = -_headerViewHeight;
		
//		if( Math.abs(newTopMargin) > mHeaderViewHeight + mHeaderViewHeight/2 )
//			newTopMargin = mHeaderViewHeight + mHeaderViewHeight/2;
		
		// 这里对上拉做一下限制,因为当前上拉后然后不释放手指直接下拉,会把下拉刷新给触发了,感谢网友yufengzungzhe的指出
//		// 表示如果是在上拉后一段距离,然后直接下拉
//		if (deltaY > 0 && mPullState == PULL_UP_STATE
//				&& Math.abs(params.topMargin) <= mHeaderViewHeight) {
//			return params.topMargin;
//		}
//		
//		// 同样地,对下拉做一下限制,避免出现跟上拉操作时一样的bug
//		// 限制下拉距离，
//		if (deltaY < 0 && mPullState == PULL_DOWN_STATE
//				&& Math.abs(params.topMargin) >= mHeaderViewHeight) {
//			return params.topMargin;
//		}
		
		params.topMargin = (int) newTopMargin;
		_headerView.setLayoutParams(params);
		invalidate();
		return params.topMargin;
	}

	/**
	 * header refreshing
	 * 
	 * @description hylin 2012-7-31上午9:10:12
	 */
	public void headerRefreshing() {
		_refreshState = REFRESH_STATE_REFRESHING;
		setHeaderTopMargin(0);
		
		AnimationDrawable ad = (AnimationDrawable) _headerImageView.getBackground();
		ad.start();
		
		_headerTextView.setText("正在加载...");//R.string.pull_to_refresh_refreshing_label);
		if (_onHeaderRefreshListener != null) {
			_onHeaderRefreshListener.onHeaderRefresh(this);
		}
	}


	/**
	 * 设置header view 的topMargin的值
	 * 
	 * @description
	 * @param topMargin
	 *            ，为0时，说明header view 刚好完全显示出来； 为-mHeaderViewHeight时，说明完全隐藏了
	 *            hylin 2012-7-31上午11:24:06
	 */
	private void setHeaderTopMargin(int topMargin) {
		LayoutParams params = (LayoutParams) _headerView.getLayoutParams();
		params.topMargin = topMargin;
		_headerView.setLayoutParams(params);
		invalidate();
	}
	
	private void setHeaderTopMarginWithAnimation(int topMargin) {
//		LayoutParams params = (LayoutParams) mHeaderView.getLayoutParams();
//		params.topMargin = topMargin;
//		mHeaderView.setLayoutParams(params);
//		invalidate();
//		
		
		TranslateAnimation ta = new TranslateAnimation(0, 0, 0, topMargin);
		ta.setDuration(500);
		ta.setRepeatCount(1);
		final int tm = topMargin;
		ta.setAnimationListener(new AnimationListener(){

			@Override
			public void onAnimationEnd(Animation animation) {
				// TODO Auto-generated method stub
				setHeaderTopMargin(tm);
				
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onAnimationStart(Animation animation) {
				// TODO Auto-generated method stub
				
			}
			
		});
		_headerView.startAnimation(ta);
		
	}
	

	/**
	 * header view 完成更新后恢复初始状态
	 * 
	 * @description hylin 2012-7-31上午11:54:23
	 */
	public void onHeaderRefreshComplete() {
		setHeaderTopMargin(-_headerViewHeight);
		_headerTextView.setText("加载完成!");//R.string.pull_to_refresh_pull_label);
		
		// 停止动画
		AnimationDrawable ad = (AnimationDrawable) _headerImageView.getBackground();
		ad.stop();
		ad.selectDrawable(0);
		
		_refreshState = REFRESH_STATE_PULL_TO_REFRESH;
	}

	/**
	 * Resets the list to a normal state after a refresh.
	 * 
	 * @param lastUpdated
	 *            Last updated at.
	 */
	public void onHeaderRefreshComplete(CharSequence lastUpdated) {
		onHeaderRefreshComplete();
	}

	public void refreshError(String errMsg){
	}
	
	/**
	 * 获取当前header view 的topMargin
	 * 
	 * @description
	 * @return hylin 2012-7-31上午11:22:50
	 */
	private int getHeaderTopMargin() {
		LayoutParams params = (LayoutParams) _headerView.getLayoutParams();
		return params.topMargin;
	}

	// /**
	// * lock
	// *
	// * @description hylin 2012-7-27下午6:52:25
	// */
	// private void lock() {
	// mLock = true;
	// }
	//
	// /**
	// * unlock
	// *
	// * @description hylin 2012-7-27下午6:53:18
	// */
	// private void unlock() {
	// mLock = false;
	// }

	/**
	 * set headerRefreshListener
	 * 
	 * @description
	 * @param headerRefreshListener
	 *            hylin 2012-7-31上午11:43:58
	 */
	public void setOnHeaderRefreshListener(
			OnHeaderRefreshListener headerRefreshListener) {
		_onHeaderRefreshListener = headerRefreshListener;
	}


	/**
	 * Interface definition for a callback to be invoked when list/grid header
	 * view should be refreshed.
	 */
	public interface OnHeaderRefreshListener {
		public void onHeaderRefresh(DDPullToRefreshView view);
	}
}
