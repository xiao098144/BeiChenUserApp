package com.ddoctor.user.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;

import com.beichen.user.R;

/**
 * 仿iphone带进度的进度条，线程安全的View，可直接在线程中更新进度 康
 */
public class RoundProgressBar extends View {
	/**
	 * 画笔对象的引用
	 */
	private Paint paint;

	/**
	 * 圆环的颜色
	 */
	private int roundColor;

	/**
	 * 圆环进度的颜色
	 */
	private int roundProgressColor;

	/**
	 * 中间进度百分比的字符串的颜色
	 */
	private int textColor;

	/**
	 * 中间进度百分比的字符串的字体
	 */
	private float textSize;

	/**
	 * 圆环进度条的宽度
	 */
	private float roundWidth;
	/**
	 * 圆的宽度
	 */
	private float circleWidth = 1.5f;

	/**
	 * 最大进度
	 */
	private int max;

	/**
	 * 当前进度
	 */
	private int progress;

	/**
	 * 进度的风格，实心或者空心
	 */
	private int style;

	/**
	 * 显示进度或者文字 默认显示进度
	 */
	private int showTextOrProgress;

	/** 进度圆环与本圆的距离 */
	private float distance;
	/**
	 * 起始角度
	 */
	private int startAngle;

	/**
	 * 目标步数
	 */
	private int goal;
	private StringBuffer goalStr;

	/**
	 * 当前步数
	 */
	private int currentStepNum;

	private Bitmap bitmap;

	private int mDefaultRoundColor;
	private int mDefaultRoundProgressColor;
	private int mDefaultTextColor;
	private int mDefaultTextSize = 16;
	private int mDefaultDistance = 0;
	private int mDefaultMax = 100;
	private int mDefaultGoal = 6000;
	private int mDefaultStartAngle = 90;
	private int mDefaultRoundWidth = 4;

	public static final int SHOWPROGRESS = 0; // 显示进度
	public static final int SHOWTEXT = 1; // 显示文字
	public static final int STROKE = 0; // 空心
	public static final int FILL = 1; // 实心

	public RoundProgressBar(Context context) {
		this(context, null);
	}

	public RoundProgressBar(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public RoundProgressBar(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);

		paint = new Paint();
		mDefaultRoundColor = Color.parseColor("#c2c2c2");
		mDefaultRoundProgressColor = Color.YELLOW;
		mDefaultTextColor = Color.GREEN;
		TypedArray mTypedArray = context.obtainStyledAttributes(attrs,
				R.styleable.RoundProgressBar);

		// 获取自定义属性和默认值
		int integer = mTypedArray.getResourceId(
				R.styleable.RoundProgressBar_bitmap, R.drawable.report_circle);
		bitmap = BitmapFactory.decodeResource(getResources(), integer);
		roundColor = mTypedArray.getColor(
				R.styleable.RoundProgressBar_roundColor, mDefaultRoundColor);
		roundProgressColor = mTypedArray.getColor(
				R.styleable.RoundProgressBar_roundProgressColor,
				mDefaultRoundProgressColor);
		textColor = mTypedArray.getColor(
				R.styleable.RoundProgressBar_textColor, mDefaultTextColor);
		textSize = mTypedArray.getDimension(
				R.styleable.RoundProgressBar_textSize, mDefaultTextSize);
		roundWidth = mTypedArray.getDimension(
				R.styleable.RoundProgressBar_roundWidth, mDefaultRoundWidth);
		max = mTypedArray.getInteger(R.styleable.RoundProgressBar_max,
				mDefaultMax);
		showTextOrProgress = mTypedArray.getInt(
				R.styleable.RoundProgressBar_showTextOrProgress, SHOWPROGRESS);

		style = mTypedArray.getInt(R.styleable.RoundProgressBar_style, STROKE);
		distance = mTypedArray.getDimension(
				R.styleable.RoundProgressBar_distance, mDefaultDistance);
		startAngle = mTypedArray.getInteger(
				R.styleable.RoundProgressBar_startAngle, mDefaultStartAngle);
		goal = mTypedArray.getInteger(R.styleable.RoundProgressBar_goal,
				mDefaultGoal);
		goalStr = new StringBuffer();
		goalStr.append("目标 ");
		goalStr.append(goal);
		mTypedArray.recycle();
	}

	@SuppressLint("DrawAllocation")
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
//		Log.e("", "onDraw " + progress + " " + currentStepNum);

		/**
		 * 画最外层的大圆环
		 */
		int center = getWidth() / 2; // 获取圆心的x坐标
		int radius;
		if (bitmap != null) {
			radius = (int) (center - distance - bitmap.getWidth() / 2); // 圆环的半径
		} else {
			radius = (int) (center - distance - roundWidth / 2); // 圆环的半径
		}

		paint.setColor(roundColor); // 设置圆环的颜色
		paint.setStyle(Paint.Style.STROKE); // 设置空心
		paint.setStrokeWidth(circleWidth); // 设置圆环的宽度
		paint.setAntiAlias(true); // 消除锯齿
		canvas.drawCircle(center, center, radius, paint); // 画出圆环

		/**
		 * 画进度百分比
		 */
		paint.setColor(textColor);
		paint.setTextSize(textSize);
		paint.setStyle(Paint.Style.FILL);
		paint.setTypeface(Typeface.DEFAULT_BOLD); // 设置字体

		if (style == STROKE) {
			float textWidth;
			String text;
			if (showTextOrProgress == SHOWPROGRESS) {
				int percent = (int) (((float) progress / (float) max) * 100);
//				if (percent != 0) {
					text = percent + "%";
					textWidth = paint.measureText(text); // 测量字体宽度
					canvas.drawText(text, center - textWidth / 2, center
							+ textSize / 2, paint); // 画出进度百分比
//				}
			} else if (showTextOrProgress == SHOWTEXT) {
				if (currentStepNum >= 0) {
					text = String.valueOf(currentStepNum);
					textWidth = paint.measureText(text); // 测量字体宽度，我们需要根据字体的宽度设置在圆环中间
					drawText(canvas, text, center, center + textSize / 2, paint);

					paint.setTextSize(textSize / 2);

					drawText(canvas, goalStr.toString(), center, center
							+ textSize * 1.25f, paint);
					drawText(canvas, "已运动", center, center - textSize * 3 / 4,
							paint);
				}
			}
		}

		if (progress > 0) {
			/**
			 * 画圆弧 ，画圆环的进度
			 */
			// 设置进度是实心还是空心
			paint.setStrokeWidth(roundWidth); // 设置圆环的宽度
			paint.setColor(roundProgressColor); // 设置进度的颜色

			RectF oval = new RectF(center - radius - distance, center - radius
					- distance, center + radius + distance, center + radius
					+ distance); // 用于定义的圆弧的形状和大小的界限
			switch (style) {
			case STROKE: {
				paint.setStyle(Paint.Style.STROKE);
				canvas.drawArc(oval, startAngle, 360 * progress / max, false,
						paint); // 根据进度画圆弧
				// paint.setStyle(Paint.Style.FILL);
				double degree = 360 * progress / max + startAngle;// 圆环角度
				float x, y;// 圆环上的圆x，y坐标
				// 已知圆心，半径，角度，求圆上的点坐标
				x = (float) (center + (radius + distance)
						* Math.cos(degree * Math.PI / 180));
				y = (float) (center + (radius + distance)
						* Math.sin(degree * Math.PI / 180));
				if (null != bitmap) {
					canvas.drawBitmap(bitmap, x - bitmap.getWidth() / 2, y
							- bitmap.getHeight() / 2, paint);
				} else {
					paint.setStyle(Paint.Style.FILL);
					canvas.drawCircle(x, y, 5, paint);
				}
				break;
			}
			case FILL: {
				paint.setStyle(Paint.Style.FILL_AND_STROKE);
				if (progress != 0)
					canvas.drawArc(oval, startAngle, 360 * progress / max,
							true, paint); // 根据进度画圆弧
				break;
			}
			}
		}

	}

	private void drawText(Canvas canvas, String text, int center, float y,
			Paint paint) {
		float x = center - paint.measureText(text) / 2;
		canvas.drawText(text, x, y, paint);
	}

	public synchronized int getMax() {
		return max;
	}

	/**
	 * 设置进度的最大值
	 * 
	 * @param max
	 */
	public synchronized void setMax(int max) {
		if (max < 0) {
			throw new IllegalArgumentException("max not less than 0");
		}
		this.max = max;
	}

	/**
	 * 获取进度.需要同步
	 * 
	 * @return
	 */
	public synchronized int getProgress() {
		return progress;
	}

	/**
	 * 设置进度，此为线程安全控件，由于考虑多线的问题，需要同步 刷新界面调用postInvalidate()能在非UI线程刷新
	 * 
	 * @param progress
	 */
	public synchronized void setProgress(int progress) {
		if (progress < 0) {
			throw new IllegalArgumentException("progress not less than 0");
		}
		if (progress > max) {
			progress = max;
		}
		if (progress <= max) {
			this.progress = progress;
			postInvalidate();
		}
	}

	public synchronized int getCurrentStepNum() {
		return currentStepNum;
	}

	public void setCurrentStepNum(int currentStepNum) {
		if (currentStepNum < 0) {
			throw new IllegalArgumentException(
					"currentStepNum can not less than 0");
		}
		if (currentStepNum > goal) {
			currentStepNum = goal;
		}
		if (currentStepNum <= goal) {
			this.currentStepNum = currentStepNum;
			progress = (int) ((float) currentStepNum / (float) goal * 100);
			if (showTextOrProgress != SHOWTEXT) {
				showTextOrProgress = SHOWTEXT;
			}
			postInvalidate();
		}

	}

	public synchronized void setGoal(int goal) {
		if (goal <= 0) {
			throw new IllegalArgumentException("goal can not less than 0");
		}
		this.goal = goal;
		goalStr = new StringBuffer();
		goalStr.append("目标");
		goalStr.append(goal);
	}

	public synchronized int getGoal() {
		return goal;
	}

	public int getCricleColor() {
		return roundColor;
	}

	public void setCricleColor(int cricleColor) {
		this.roundColor = cricleColor;
	}

	public int getCricleProgressColor() {
		return roundProgressColor;
	}

	public void setCricleProgressColor(int cricleProgressColor) {
		this.roundProgressColor = cricleProgressColor;
	}

	public int getTextColor() {
		return textColor;
	}

	public void setTextColor(int textColor) {
		this.textColor = textColor;
	}

	public float getTextSize() {
		return textSize;
	}

	public void setTextSize(float textSize) {
		this.textSize = textSize;
	}

	public float getRoundWidth() {
		return roundWidth;
	}

	public void setRoundWidth(float roundWidth) {
		this.roundWidth = roundWidth;
	}

	public int getRoundColor() {
		return roundColor;
	}

	public void setRoundColor(int roundColor) {
		this.roundColor = roundColor;
	}

	public int getRoundProgressColor() {
		return roundProgressColor;
	}

	public void setRoundProgressColor(int roundProgressColor) {
		this.roundProgressColor = roundProgressColor;
	}

	public int getStyle() {
		return style;
	}

	public void setStyle(int style) {
		this.style = style;
	}

	public int getShowTextOrProgress() {
		return showTextOrProgress;
	}

	public void setShowTextOrProgress(int showTextOrProgress) {
		this.showTextOrProgress = showTextOrProgress;
	}

	public float getDistance() {
		return distance;
	}

	public void setDistance(float distance) {
		this.distance = distance;
	}

	public int getStartAngle() {
		return startAngle;
	}

	public void setStartAngle(int startAngle) {
		this.startAngle = startAngle;
	}
}
