package com.ddoctor.user.view;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ImageView;

public class DiscView extends ImageView {

	private final static float fBaseValue = 7.0f;// 14.0f;//7.0f; // 原点对应的值
	private final static float fUnitValue = 0.1f;// 0.1f; // 每一个刻度对应的值
	private final static float fUnitDegree = 1.5f; // 每一个刻度对应的角度

	private final static float MAX_VALUE = 25.0f;
	private final static float MIN_VALUE = 2.0f;

	// private final static float MIN_DEGREE = -180f;//0f;
	// private final static float MAX_DEGREE = 180f;//360f;

	// private final static float MIN_DEGREE = -180f;//0f;
	// private final static float MAX_DEGREE = 180f;//360f;
	//

	private float getMinDegree() {
		return -(MAX_VALUE - fBaseValue) * 10 * fUnitDegree;
	}

	private float getMaxDegree() {
		return (fBaseValue - MIN_VALUE) * 10 * fUnitDegree;
	}

	private DiscViewOnChangedListener _onChangedListener = null;

	private Matrix m;

	private float saveX; // 当前保存的x
	private float saveY; // 当前保存的y
	private float curTouchX; // 当前触屏的x
	private float curTouchY; // 当前触摸的y
	private float centerX; // 中心点x
	private float centerY; // 中心点y
	private float curDegree; // 当前角度
	private float changeDegree;

	public DiscView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setScaleType(ScaleType.MATRIX);// 重点
		m = new Matrix();
	}

	public DiscView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		// TODO Auto-generated constructor stub
	}

	public DiscView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	public void setDiscViewOnChangedListener(DiscViewOnChangedListener listener) {
		_onChangedListener = listener;
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		centerX = this.getWidth() / 2;
		centerY = this.getHeight() / 2;

		int dw = this.getDrawable().getBounds().width();
		int dh = this.getDrawable().getBounds().height();
		centerX = dw / 2;
		centerY = dh / 2;
	}

	public boolean onTouchEvent(MotionEvent event) {
		handleTouch(event);
		return true;
	}

	private void handleTouch(MotionEvent event) {
		curTouchX = event.getX();
		curTouchY = event.getY();

		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			saveTouchPoint();
			break;
		case MotionEvent.ACTION_MOVE:
			handleTouchMove();
			break;
		case MotionEvent.ACTION_UP:
			// 可以使用访问者模式这里让访问者获得当前角度
			break;
		}
	}

	private void handleTouchMove() {
		changeDegree = (float) getActionDegrees(centerX, centerY, saveX, saveY,
				curTouchX, curTouchY);

		float tempDegree = (float) curDegree + changeDegree;
		if (tempDegree >= getMinDegree() - 0.5
				&& tempDegree <= getMaxDegree() + 0.5) {
			this.curDegree = tempDegree;
			m.setRotate(curDegree, centerX, centerY);
			setImageMatrix(m);// 此方法会 调用invalidate() 从而重绘界面
			if (_onChangedListener != null)
				_onChangedListener.onDegreeChanged(this.curDegree);
		}

		saveTouchPoint();
	}

	private void saveTouchPoint() {
		saveX = curTouchX;
		saveY = curTouchY;

	}

	/**
	 * 获取两点到第三点的夹角。
	 * 
	 * @param x
	 * @param y
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 * @return
	 */
	private double getActionDegrees(float x, float y, float x1, float y1,
			float x2, float y2) {

		double a = Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
		double b = Math.sqrt((x - x2) * (x - x2) + (y - y2) * (y - y2));
		double c = Math.sqrt((x1 - x) * (x1 - x) + (y1 - y) * (y1 - y));
		// 余弦定理
		double cosA = (b * b + c * c - a * a) / (2 * b * c);
		// 返回余弦值为指定数字的角度，Math函数为我们提供的方法
		double arcA = Math.acos(cosA);
		double degree = arcA * 180 / Math.PI;

		// 接下来我们要讨论正负值的关系了，也就是求出是顺时针还是逆时针。
		// 第1、2象限
		if (y1 < y && y2 < y) {
			if (x1 < x && x2 > x) {// 由2象限向1象限滑动
				return degree;
			}
			// 由1象限向2象限滑动
			else if (x1 >= x && x2 <= x) {
				return -degree;
			}
		}
		// 第3、4象限
		if (y1 > y && y2 > y) {
			// 由3象限向4象限滑动
			if (x1 < x && x2 > x) {
				return -degree;
			}
			// 由4象限向3象限滑动
			else if (x1 > x && x2 < x) {
				return degree;
			}

		}
		// 第2、3象限
		if (x1 < x && x2 < x) {
			// 由2象限向3象限滑动
			if (y1 < y && y2 > y) {
				return -degree;
			}
			// 由3象限向2象限滑动
			else if (y1 > y && y2 < y) {
				return degree;
			}
		}
		// 第1、4象限
		if (x1 > x && x2 > x) {
			// 由4向1滑动
			if (y1 > y && y2 < y) {
				return -degree;
			}
			// 由1向4滑动
			else if (y1 < y && y2 > y) {
				return degree;
			}
		}

		// 在特定的象限内
		float tanB = (y1 - y) / (x1 - x);
		float tanC = (y2 - y) / (x2 - x);
		if ((x1 > x && y1 > y && x2 > x && y2 > y && tanB > tanC)// 第一象限
				|| (x1 > x && y1 < y && x2 > x && y2 < y && tanB > tanC)// 第四象限
				|| (x1 < x && y1 < y && x2 < x && y2 < y && tanB > tanC)// 第三象限
				|| (x1 < x && y1 > y && x2 < x && y2 > y && tanB > tanC))// 第二象限
			return -degree;
		return degree;
	}

	public float getCurDegree() {
		return curDegree;
	}

	public void setCurDegree(float curDegree) {
		if (curDegree >= getMinDegree() && curDegree <= getMaxDegree()) {
			this.curDegree = curDegree;
			m.setRotate(curDegree, centerX, centerY);
			setImageMatrix(m);
		}

	}

	public interface DiscViewOnChangedListener {
		public void onDegreeChanged(float degree);
	}

	public void setDegree(float degree, boolean animation) {
		if (animation) {
			ValueAnimator animator = ValueAnimator.ofFloat(this.curDegree,
					degree);
			animator.addUpdateListener(new AnimatorUpdateListener() {

				@Override
				public void onAnimationUpdate(ValueAnimator animation) {
					float degree1 = (Float) animation.getAnimatedValue();
					// Log.v("alex", "degree=" + degree1);
					m.setRotate(degree1, centerX, centerY);
					setImageMatrix(m);
				}

			});
			animator.setDuration(500);
			animator.setStartDelay(200);
			animator.start();
			this.curDegree = degree;
		} else {
			setCurDegree(degree);
		}
	}

	// 调用这个函数来设置转盘的值
	public static float value2degree(float value) {
		// value转成角度

		// 0.1f: 每个刻度为0.1
		// 3.0f: 每个刻度对应的3度
		// 这2个数值是根据设计图来的
		return ((fBaseValue - value) / fUnitValue) * fUnitDegree;
	}

	public static float degree2value(float degree) {
		// int n = (int) (degree / 3.0f);
		// float f = fBaseValue - n * baseValue;

		// 修改: 刻度四舍五入
		int n;
		if (degree > 0)
			n = (int) ((degree + fUnitDegree / 2) / fUnitDegree);
		else
			n = (int) ((degree - fUnitDegree / 2) / fUnitDegree);
		float f = fBaseValue - n * fUnitValue;

		return f;
	}

}
