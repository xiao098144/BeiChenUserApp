package com.ddoctor.user.view;

import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.TextView;

public class JumpTextView extends TextView {

	// 动画时长 ms
	int duration = 1800;
	float number;
  
	public JumpTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB) 
	public void showNumberWithAnimation(int number) {
		if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB) {
			// 修改number属性，会调用setNumber方法
			ObjectAnimator objectAnimator = ObjectAnimator.ofInt(this, "number",0,number);
			objectAnimator.setDuration(duration);
			// 加速器，从慢到快到再到慢
			objectAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
			objectAnimator.start();
		}else {
			setNumber(number);
		}
	}

	public float getNumber() {
		return number;
	}

	public void setNumber(float number) {
		this.number = number;
		setText(String.format("%1$f", number));
	}
}
