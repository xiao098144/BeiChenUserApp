package com.ddoctor.user.view;

import android.content.Context;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.beichen.user.R;

/**
 * 用药导航圆点
 * 
 * @author 康
 * 
 */
public class PageControl {

	private LinearLayout layout;
	private ImageView[] images;
	private ImageView image;
	private int pageSize;

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	private int currentPage = 0;
	private Context mContext;

	public PageControl(Context context, LinearLayout layout) {
		this.mContext = context;
		this.layout = layout;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
		if (pageSize >1) {
			initDots();
		}
	}
	
	void initDots() {
		if (null != layout && layout.getChildCount()>0) {
			layout.removeAllViews();
		}
		images = new ImageView[pageSize];
		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.WRAP_CONTENT);
		lp.setMargins(10, 5, 10, 5);
		for (int i = 0; i < pageSize; i++) {
			image = new ImageView(mContext);
			image.setLayoutParams(lp);

			images[i] = image;
			if (i == 0) {
				// 默认进入程序后第一张图片被选中;
				images[i].setImageResource(R.drawable.medicine_tab_true);
			} else {
				images[i].setImageResource(R.drawable.medicine_tab_false);
			}
			layout.addView(images[i]);
		}
	}

	boolean isFirst() {
		return this.currentPage == 0;
	}

	boolean isLast() {
		return this.currentPage == pageSize;
	}

	public void selectPage(int current) {
		for (int i = 0; i < images.length; i++) {
			images[current].setImageResource(R.drawable.medicine_tab_true);
			if (current != i) {
				images[i].setImageResource(R.drawable.medicine_tab_false);
			}
		}
	}

	void turnToNextPage() {
		if (!isLast()) {
			currentPage++;
			selectPage(currentPage);
		}
	}

	void turnToPrePage() {
		if (!isFirst()) {
			currentPage--;
			selectPage(currentPage);
		}
	}
}
