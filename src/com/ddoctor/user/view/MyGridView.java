package com.ddoctor.user.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.GridView;

/**
 * @filename MyGridView.java
 * @TODO 重写OnMeasure方法 解决ScrollView 嵌套的问题
 * @date 2014-8-4上午10:26:25
 * @Administrator 萧
 * 
 */
public class MyGridView extends GridView {

	public MyGridView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public MyGridView(Context context) {
		super(context);
	}

	public MyGridView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

		int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2,
				MeasureSpec.AT_MOST);
		super.onMeasure(widthMeasureSpec, expandSpec);
	}
}