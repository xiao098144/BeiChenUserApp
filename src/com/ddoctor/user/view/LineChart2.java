package com.ddoctor.user.view;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathEffect;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.View;

import com.beichen.user.R;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.wapi.bean.SugarValueBean;
import com.ddoctor.utils.MyUtils;
import com.ddoctor.utils.StringUtils;
import com.ddoctor.utils.TimeUtil;

/**
 * 多日血糖数据图 无折线
 * 
 * @author 萧
 * @Date 2015-5-21下午10:24:04
 * @TODO TODO
 */
public class LineChart2 extends View {

	public LineChart2(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init(context);
	}

	public LineChart2(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	public LineChart2(Context context) {
		super(context);
		init(context);
	}

	private int[] imgRes = new int[8];
	// private int normalImgRes;
	private Bitmap bitmap;
	private List<SugarValueBean> list;
	private float highValue = DataModule.getInstance().getSugarUpBound();
	private float lowValue = DataModule.getInstance().getSugarDownBound();

	private int COLOR_HIGH; // 偏高线颜色
	private int COLOR_LOW; // 偏低线颜色
	private int COLOR_LINE_LC = Color.parseColor("#FF0000"); // 凌晨连线颜色
	private int COLOR_LINE_KF = Color.parseColor("#FF8000"); // 空腹连线颜色
	private int COLOR_LINE_ZH = Color.parseColor("#FFFF00");; // 早后连线颜色
	private int COLOR_LINE_WUQ = Color.parseColor("#00FF00"); // 午前连线颜色
	private int COLOR_LINE_WUH = Color.parseColor("#00FFFF");// 午后连线颜色
	private int COLOR_LINE_WANQ = Color.parseColor("#0000FF");; // 晚前连线颜色
	private int COLOR_LINE_WANH = Color.parseColor("#BF00FF");; // 晚后连线颜色
	private int COLOR_LINE_SQ = Color.parseColor("#000000");; // 睡前连线颜色
	private int COLOR_FRAME; // 框架颜色

	private void init(Context context) {
		// normalImgRes = R.drawable.sugar_normal;
		imgRes[0] = R.drawable.lingchen;
		imgRes[1] = R.drawable.kongfu;
		imgRes[2] = R.drawable.zaohou;
		imgRes[3] = R.drawable.wuqian;
		imgRes[4] = R.drawable.wuhou;
		imgRes[5] = R.drawable.wanqian;
		imgRes[6] = R.drawable.wanhou;
		imgRes[7] = R.drawable.shuiqian;
		COLOR_HIGH = context.getResources().getColor(
				R.color.color_sugar_standard_high);
		COLOR_LOW = context.getResources().getColor(
				R.color.color_sugar_standard_low);
		COLOR_FRAME = context.getResources().getColor(R.color.white);
		marginInPx = MyUtils.dip2px(getContext(), margin);

	}

	private final static int YDIVIDE = 6;
	private float max = 10;
	private float min = 0;
	float maxValue;
	float minValue;
	/** Y轴坐标 */
	private float[] yLoc = new float[YDIVIDE + 1];
	private float[] yValue = new float[YDIVIDE + 1];

	public void setData(List<SugarValueBean> list, float high_value,
			float low_value) {
		this.list = list;
		Collections.sort(list);
		initData();
		maxValue = (float) Math.ceil(getMax());
		minValue = (float) Math.floor(getMin());
		if (maxValue != minValue) {
			max = maxValue;
			min = minValue;
		} else {
			if (maxValue > max) {
				max = maxValue;
			}
		}
//		this.highValue = high_value;
//		this.lowValue = low_value;
	}

	SparseArray<String> date;
	private List<float[]> lcList = new ArrayList<float[]>();
	private List<float[]> kfList = new ArrayList<float[]>();
	private List<float[]> zhList = new ArrayList<float[]>();
	private List<float[]> wuqList = new ArrayList<float[]>();
	private List<float[]> wuhList = new ArrayList<float[]>();
	private List<float[]> wanqList = new ArrayList<float[]>();
	private List<float[]> wanhList = new ArrayList<float[]>();
	private List<float[]> sqList = new ArrayList<float[]>();

	private float getMin() {
		float min = list.get(0).getValue();
		for (int i = 0; i < list.size(); i++) {
			if (min > list.get(i).getValue()) {
				min = list.get(i).getValue();
			}
		}
		return min;
	}

	private float getMax() {
		float max = list.get(0).getValue();
		for (int i = 0; i < list.size(); i++) {
			if (max < list.get(i).getValue()) {
				max = list.get(i).getValue();
			}
		}
		return max;
	}

	/**
	 * 处理数据 得到日期数组
	 * 
	 * @return
	 */
	private void initData() {
		date = new SparseArray<String>();
		int index = 0;

		for (int i = 0; i < list.size(); i++) {
			list.get(i).setDate(
					TimeUtil.getInstance().timeStrFormat(list.get(i).getTime(),
							"yyyy-MM-dd HH:mm:ss", "MM/dd"));
			if (i == 0) {
				date.append(index, list.get(i).getDate());
			}
			if (i > 0) {
				if (!list.get(i).getDate().equals(list.get(i - 1).getDate())) {
					index++;
					date.append(index, list.get(i).getDate());
				}
			}
		}

		XCood = new float[date.size()];
	}

	private Paint framePaint, linePaint;

	private int margin = 5;
	private int marginInPx;
	float XCood[]; // 时间点X轴坐标 数组
	int viewWidth;
	int viewHeight;
	float XLength; // X轴长度
	float XStart; // X轴起点
	int XEnd; // X轴终点
	float xScaleHeight; // X轴刻度高度
	int YLength; // Y轴长度
	int YStart; // Y轴起点
	int YEnd; // Y轴终点
	float Xoffset; // X轴偏移量
	float YOffset;

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		if (list == null || list.size() == 0) {
			return;
		}
		viewHeight = getHeight();
		viewWidth = getWidth();
		drawFrame(canvas);
		drawPoint(canvas);
	}

	private void drawFrame(Canvas canvas) {
		framePaint = new Paint();
		framePaint.setAntiAlias(true);
		framePaint.setStrokeWidth(2);
		framePaint.setColor(COLOR_FRAME);
		framePaint.setStyle(Paint.Style.FILL);
		framePaint.setTextSize(23);

		float textWidth = framePaint.measureText("00.0");
		XStart = textWidth + 2 * marginInPx;
		XEnd = (int) (viewWidth - 4 * marginInPx - textWidth);
		XLength = XEnd - XStart;
		Xoffset = 5 * textWidth / 7;
		xScaleHeight = textWidth / 4;

		YStart = (int) (viewHeight - 2 * marginInPx - textWidth / 2);
		YEnd = (int) (textWidth / 2);
		YLength = YStart - YEnd;
		YOffset = textWidth;

		if (date.size() == 1) {
			XCood[0] = XLength / 2;
		} else {
			for (int i = 0; i < date.size(); i++) {
				XCood[i] = XStart + i * XLength / (date.size() - 1);
			}
		}
		lcList.clear();
		kfList.clear();
		zhList.clear();
		wuqList.clear();
		wuhList.clear();
		wanqList.clear();
		wanhList.clear();
		sqList.clear();

		for (int i = 0; i < list.size(); i++) {
			SugarValueBean sugarBean = list.get(i);
			switch (sugarBean.getType()) {
			case 0: {
				kfList.add(new float[] { calculateWidth(sugarBean),
						calculateHeight(sugarBean.getValue()),
						sugarBean.getValue() });
			}
				break;
			case 1: {
				zhList.add(new float[] { calculateWidth(sugarBean),
						calculateHeight(sugarBean.getValue()),
						sugarBean.getValue() });
			}
				break;
			case 2: {
				wuqList.add(new float[] { calculateWidth(sugarBean),
						calculateHeight(sugarBean.getValue()),
						sugarBean.getValue() });
			}
				break;
			case 3: {
				wuhList.add(new float[] { calculateWidth(sugarBean),
						calculateHeight(sugarBean.getValue()),
						sugarBean.getValue() });
			}
				break;
			case 4: {
				wanqList.add(new float[] { calculateWidth(sugarBean),
						calculateHeight(sugarBean.getValue()),
						sugarBean.getValue() });
			}
				break;
			case 5: {
				wanhList.add(new float[] { calculateWidth(sugarBean),
						calculateHeight(sugarBean.getValue()),
						sugarBean.getValue() });
			}
				break;
			case 6: {
				sqList.add(new float[] { calculateWidth(sugarBean),
						calculateHeight(sugarBean.getValue()),
						sugarBean.getValue() });
			}
				break;
			case 7: {
				lcList.add(new float[] { calculateWidth(sugarBean),
						calculateHeight(sugarBean.getValue()),
						sugarBean.getValue() });
			}
				break;
			default:
				break;
			}
		}

		MyUtils.showLog("凌晨  " + lcList.toString());
		MyUtils.showLog("空腹   " + kfList.toString());
		MyUtils.showLog(" 早后 " + zhList.toString());
		MyUtils.showLog(" 午前 " + wuqList.toString());
		MyUtils.showLog(" 午后 " + wuhList.toString());
		MyUtils.showLog(" 晚前 " + wanqList.toString());
		MyUtils.showLog(" 晚后 " + wanhList.toString());
		MyUtils.showLog(" 睡前 " + sqList.toString());

		float ylength = (max - min) / YDIVIDE; // 由记录最大值与最小值动态计算得到Y轴坐标
		for (int i = 0; i < yLoc.length; i++) {
			yValue[i] = Float.valueOf(StringUtils.formatnum(min + ylength * i,
					".0f"));
			yLoc[i] = calculateHeight(yValue[i]) + marginInPx / 2;
		}

		canvas.drawLine(XStart, YStart, XStart, YEnd, framePaint); // 画Y轴
		canvas.drawLine(XStart, YStart, XEnd, YStart, framePaint); // 画X轴

		float lowHeight = calculateHeight(lowValue);
		float highHeight = calculateHeight(highValue); // 计算上下限Y坐标
		MyUtils.showLog("", "highHeight " + highHeight + " " + YEnd);
		MyUtils.showLog("", "lowHeight " + lowHeight + " " + YStart);
		if (lowHeight <= YStart) {
			drawText(canvas, String.valueOf(lowValue), XEnd + 3,
					lowHeight + 10, framePaint);
			drawDashLine(XStart, lowHeight, XEnd, lowHeight, framePaint,
					COLOR_LOW, canvas); // 绘制上下限标准虚线
		}
		if (highHeight >= YEnd) {
			drawText(canvas, String.valueOf(highValue), XEnd + 3,
					highHeight + 10, framePaint);
			drawDashLine(XStart, highHeight, XEnd, highHeight, framePaint,
					COLOR_HIGH, canvas);
		}

		/** 画X坐标 */
		for (int i = 0; i < date.size(); i++) {
			drawText(canvas, date.get(i), XCood[i] - Xoffset, viewHeight
					- textWidth / 4, framePaint);
			if (i > 0) {
				canvas.drawLine(XCood[i], YStart, XCood[i], YStart
						- xScaleHeight, framePaint);
			}
		}

		for (int i = 0; i < yLoc.length; i++) {
			drawText(canvas, String.valueOf(yValue[i]),
					XStart - framePaint.measureText(String.valueOf(yValue[i]))
							- marginInPx, yLoc[i], framePaint);
		}
		// /** 绘制上方图例 */
		// for (int i = 0; i < 8; i++) {
		// Bitmap bitmap = BitmapFactory.decodeResource(getResources(),
		// imgRes[i]);
		// canvas.drawBitmap(bitmap, XStart + YOffset + i
		// * (XLength - YOffset) / 8 - bitmap.getWidth() - 3,
		// textHeight - 3 * marginInPx / 2 - bitmap.getHeight(),
		// framePaint);
		// drawText(canvas, TYPE[i], XStart + YOffset + i
		// * (XLength - YOffset) / 8, textHeight - marginInPx,
		// framePaint);
		// }
	}

	private void drawPoint(Canvas canvas) {
		linePaint = new Paint();
		linePaint.setStyle(Paint.Style.FILL);
		linePaint.setStrokeWidth(1.5f);
		linePaint.setAntiAlias(true);

		if (misDrawLCLine) {
			if (lcList.size() > 0) {
				linePaint.setColor(COLOR_LINE_LC);
				bitmap = BitmapFactory
						.decodeResource(getResources(), imgRes[0]);
				if (lcList.size() == 1) {
					canvas.drawBitmap(bitmap,
							lcList.get(0)[0] - bitmap.getWidth() / 2,
							lcList.get(0)[1] - bitmap.getHeight() / 2,
							linePaint);
				} else {
					for (int i = 0; i < lcList.size() - 1; i++) {
						float value = lcList.get(i)[2];
						if (value > 0) { // 忽略值为0的点
							/** 描点连线 */
							canvas.drawLine(lcList.get(i)[0], lcList.get(i)[1],
									lcList.get(i + 1)[0], lcList.get(i + 1)[1],
									linePaint);
							canvas.drawBitmap(bitmap,
									lcList.get(i)[0] - bitmap.getWidth() / 2,
									lcList.get(i)[1] - bitmap.getHeight() / 2,
									linePaint);

							if (i == lcList.size() - 2) {
								canvas.drawBitmap(
										bitmap,
										lcList.get(i + 1)[0]
												- bitmap.getWidth() / 2,
										lcList.get(i + 1)[1]
												- bitmap.getHeight() / 2,
										linePaint);
							}

						}
					}
				}
			}
		}

		if (misDrawKFLine) {
			if (kfList.size() > 0) {
				bitmap = BitmapFactory
						.decodeResource(getResources(), imgRes[1]);
				linePaint.setColor(COLOR_LINE_KF);
				if (kfList.size() == 1) {
					canvas.drawBitmap(bitmap,
							kfList.get(0)[0] - bitmap.getWidth() / 2,
							kfList.get(0)[1] - bitmap.getHeight() / 2,
							linePaint);
				} else {
					for (int i = 0; i < kfList.size() - 1; i++) {
						float value = kfList.get(i)[2];
						if (value > 0) { // 忽略值为0的点
							/** 描点连线 */
							canvas.drawLine(kfList.get(i)[0], kfList.get(i)[1],
									kfList.get(i + 1)[0], kfList.get(i + 1)[1],
									linePaint);
							canvas.drawBitmap(bitmap,
									kfList.get(i)[0] - bitmap.getWidth() / 2,
									kfList.get(i)[1] - bitmap.getHeight() / 2,
									linePaint);
							if (i == kfList.size() - 2) {
								canvas.drawBitmap(
										bitmap,
										kfList.get(i + 1)[0]
												- bitmap.getWidth() / 2,
										kfList.get(i + 1)[1]
												- bitmap.getHeight() / 2,
										linePaint);
							}
						}
					}
				}
			}
		}

		if (misDrawZHLine) {
			if (zhList.size() > 0) {
				bitmap = BitmapFactory
						.decodeResource(getResources(), imgRes[2]);
				linePaint.setColor(COLOR_LINE_ZH);
				if (zhList.size() == 1) {
					canvas.drawBitmap(bitmap,
							zhList.get(0)[0] - bitmap.getWidth() / 2,
							zhList.get(0)[1] - bitmap.getHeight() / 2,
							linePaint);
				} else {
					for (int i = 0; i < zhList.size() - 1; i++) {
						float value = zhList.get(i)[2];
						if (value > 0) { // 忽略值为0的点
							/** 描点连线 */
							canvas.drawLine(zhList.get(i)[0], zhList.get(i)[1],
									zhList.get(i + 1)[0], zhList.get(i + 1)[1],
									linePaint);
							canvas.drawBitmap(bitmap,
									zhList.get(i)[0] - bitmap.getWidth() / 2,
									zhList.get(i)[1] - bitmap.getHeight() / 2,
									linePaint);
							if (i == zhList.size() - 2) {
								canvas.drawBitmap(
										bitmap,
										zhList.get(i + 1)[0]
												- bitmap.getWidth() / 2,
										zhList.get(i + 1)[1]
												- bitmap.getHeight() / 2,
										linePaint);
							}
						}
					}
				}
			}
		}
		if (misDrawWUQLine) {
			if (wuqList.size() > 0) {
				bitmap = BitmapFactory
						.decodeResource(getResources(), imgRes[3]);
				linePaint.setColor(COLOR_LINE_WUQ);
				if (wuqList.size() == 1) {
					canvas.drawBitmap(bitmap, wuqList.get(0)[0]
							- bitmap.getWidth() / 2, wuqList.get(0)[1]
							- bitmap.getHeight() / 2, linePaint);
				} else {
					for (int i = 0; i < wuqList.size() - 1; i++) {
						float value = wuqList.get(i)[2];
						if (value > 0) { // 忽略值为0的点
							/** 描点连线 */
							canvas.drawLine(wuqList.get(i)[0],
									wuqList.get(i)[1], wuqList.get(i + 1)[0],
									wuqList.get(i + 1)[1], linePaint);
							canvas.drawBitmap(bitmap, wuqList.get(i)[0]
									- bitmap.getWidth() / 2, wuqList.get(i)[1]
									- bitmap.getHeight() / 2, linePaint);
							if (i == wuqList.size() - 2) {
								canvas.drawBitmap(
										bitmap,
										wuqList.get(i + 1)[0]
												- bitmap.getWidth() / 2,
										wuqList.get(i + 1)[1]
												- bitmap.getHeight() / 2,
										linePaint);
							}
						}
					}
				}
			}
		}

		if (misDrawWUHLine) {
			if (wuhList.size() > 0) {
				bitmap = BitmapFactory
						.decodeResource(getResources(), imgRes[4]);
				linePaint.setColor(COLOR_LINE_WUH);
				if (wuhList.size() == 1) {
					canvas.drawBitmap(bitmap, wuhList.get(0)[0]
							- bitmap.getWidth() / 2, wuhList.get(0)[1]
							- bitmap.getHeight() / 2, linePaint);
				} else {
					for (int i = 0; i < wuhList.size() - 1; i++) {
						float value = wuhList.get(i)[2];
						if (value > 0) { // 忽略值为0的点
							/** 描点连线 */
							canvas.drawLine(wuhList.get(i)[0],
									wuhList.get(i)[1], wuhList.get(i + 1)[0],
									wuhList.get(i + 1)[1], linePaint);
							canvas.drawBitmap(bitmap, wuhList.get(i)[0]
									- bitmap.getWidth() / 2, wuhList.get(i)[1]
									- bitmap.getHeight() / 2, linePaint);
							if (i == wuhList.size() - 2) {
								canvas.drawBitmap(
										bitmap,
										wuhList.get(i + 1)[0]
												- bitmap.getWidth() / 2,
										wuhList.get(i + 1)[1]
												- bitmap.getHeight() / 2,
										linePaint);
							}
						}
					}
				}
			}
		}

		if (misDrawWANQLine) {
			if (wanqList.size() > 0) {
				bitmap = BitmapFactory
						.decodeResource(getResources(), imgRes[5]);
				linePaint.setColor(COLOR_LINE_WANQ);
				if (wanqList.size() == 1) {
					canvas.drawBitmap(bitmap, wanqList.get(0)[0]
							- bitmap.getWidth() / 2, wanqList.get(0)[1]
							- bitmap.getHeight() / 2, linePaint);
				} else {
					for (int i = 0; i < wanqList.size() - 1; i++) {
						float value = wanqList.get(i)[2];
						if (value > 0) { // 忽略值为0的点
							/** 描点连线 */
							canvas.drawLine(wanqList.get(i)[0],
									wanqList.get(i)[1], wanqList.get(i + 1)[0],
									wanqList.get(i + 1)[1], linePaint);
							canvas.drawBitmap(bitmap, wanqList.get(i)[0]
									- bitmap.getWidth() / 2, wanqList.get(i)[1]
									- bitmap.getHeight() / 2, linePaint);
							if (i == wanqList.size() - 2) {
								canvas.drawBitmap(
										bitmap,
										wanqList.get(i + 1)[0]
												- bitmap.getWidth() / 2,
										wanqList.get(i + 1)[1]
												- bitmap.getHeight() / 2,
										linePaint);
							}
						}
					}
				}
			}
		}

		if (misDrawWANHLine) {
			if (wanhList.size() > 0) {
				bitmap = BitmapFactory
						.decodeResource(getResources(), imgRes[6]);
				linePaint.setColor(COLOR_LINE_WANH);
				if (wanhList.size() == 1) {
					canvas.drawBitmap(bitmap, wanhList.get(0)[0]
							- bitmap.getWidth() / 2, wanhList.get(0)[1]
							- bitmap.getHeight() / 2, linePaint);
				} else {
					for (int i = 0; i < wanhList.size() - 1; i++) {
						float value = wanhList.get(i)[2];
						if (value > 0) { // 忽略值为0的点
							/** 描点连线 */
							canvas.drawLine(wanhList.get(i)[0],
									wanhList.get(i)[1], wanhList.get(i + 1)[0],
									wanhList.get(i + 1)[1], linePaint);
							canvas.drawBitmap(bitmap, wanhList.get(i)[0]
									- bitmap.getWidth() / 2, wanhList.get(i)[1]
									- bitmap.getHeight() / 2, linePaint);
							if (i == wanhList.size() - 2) {
								canvas.drawBitmap(
										bitmap,
										wanhList.get(i + 1)[0]
												- bitmap.getWidth() / 2,
										wanhList.get(i + 1)[1]
												- bitmap.getHeight() / 2,
										linePaint);
							}
						}
					}
				}
			}
		}
		if (misDrawSQLine) {
			if (sqList.size() > 0) {
				bitmap = BitmapFactory
						.decodeResource(getResources(), imgRes[7]);
				linePaint.setColor(COLOR_LINE_SQ);
				if (sqList.size() == 1) {
					canvas.drawBitmap(bitmap,
							sqList.get(0)[0] - bitmap.getWidth() / 2,
							sqList.get(0)[1] - bitmap.getHeight() / 2,
							linePaint);
				} else {
					for (int i = 0; i < sqList.size() - 1; i++) {
						float value = sqList.get(i)[2];
						if (value > 0) { // 忽略值为0的点
							/** 描点连线 */
							canvas.drawLine(sqList.get(i)[0], sqList.get(i)[1],
									sqList.get(i + 1)[0], sqList.get(i + 1)[1],
									linePaint);
							canvas.drawBitmap(bitmap,
									sqList.get(i)[0] - bitmap.getWidth() / 2,
									sqList.get(i)[1] - bitmap.getHeight() / 2,
									linePaint);
							if (i == sqList.size() - 2) {
								canvas.drawBitmap(
										bitmap,
										sqList.get(i + 1)[0]
												- bitmap.getWidth() / 2,
										sqList.get(i + 1)[1]
												- bitmap.getHeight() / 2,
										linePaint);
							}
						}
					}
				}
			}
		}

	}

	/**
	 * 判断血糖值是否在正常值范围
	 * 
	 * @param value
	 * @return
	 */
	private boolean isValueValid(float value) {
		return value <= highValue && value >= lowValue;
	}

	/**
	 * 绘制虚线
	 * 
	 * @param xStart
	 *            x轴起点
	 * @param yStart
	 *            y轴起点
	 * @param xEnd
	 *            x轴终点
	 * @param yEnd
	 *            y轴终点
	 * @param paint
	 *            paint 必须设置Style Style.Stroke
	 * @param canvas
	 */
	private void drawDashLine(float xStart, float yStart, float xEnd,
			float yEnd, Paint paint, int color, Canvas canvas) {
		Path path = new Path();
		path.moveTo(xStart, yStart);
		path.lineTo(xEnd, yEnd);
		PathEffect effects = new DashPathEffect(new float[] { 5, 5, 5, 5 }, 2);

		Paint pt = new Paint(paint);
		pt.setPathEffect(effects);
		pt.setColor(color);
		pt.setStyle(Paint.Style.STROKE);
		
		canvas.drawPath(path, pt);
	}

	/**
	 * 绘制文字
	 * 
	 * @param canvas
	 * @param text
	 * @param x
	 * @param y
	 * @param paint
	 */
	private void drawText(Canvas canvas, String text, float x, float y,
			Paint paint) {
		paint.setStyle(Paint.Style.FILL);
		paint.setColor(COLOR_FRAME);
		canvas.drawText(text, x, y, paint);
	}

	/**
	 * 根据记录时间计算对应X轴坐标
	 * 
	 * @param sugar
	 * @return
	 */
	private float calculateWidth(SugarValueBean sugar) {
		if (TextUtils.isEmpty(sugar.getDate())) {
			return 0;
		}
		int index = 0;
		for (int i = 0; i < date.size(); i++) {
			if (sugar.getDate().equals(date.get(i))) {
				index = i;
				break;
			}
		}
		return XCood[index];
	}

	/** 由最大值计算Y轴几等分 从而计算得到最高限 最低限 与 中轴的位置 */
	private float calculateHeight(float value) {
		return (float) (YStart - (value - min) / (max - min) * YLength);
	}

	private boolean misDrawLCLine = true; // 是否绘制凌晨曲线 默认为 true
	private boolean misDrawKFLine = true; // 是否绘制空腹曲线 默认为 true
	private boolean misDrawZHLine = true; // 是否绘制早后曲线 默认为 true
	private boolean misDrawWUQLine = true; // 是否绘制午前曲线 默认为 true
	private boolean misDrawWUHLine = true; // 是否绘制午后曲线 默认为 true
	private boolean misDrawWANQLine = true; // 是否绘制晚前曲线 默认为 true
	private boolean misDrawWANHLine = true; // 是否绘制晚后曲线 默认为 true
	private boolean misDrawSQLine = true; // 是否绘制睡前曲线 默认为 true

	public void setDrawLCLine(boolean isDrawLCLine) {
		this.misDrawLCLine = isDrawLCLine;
		this.invalidate();
	}

	public void setDrawKFLine(boolean isDrawKFLine) {
		this.misDrawKFLine = isDrawKFLine;
		this.invalidate();
	}

	public void setDrawZHLine(boolean isDrawLine) {
		this.misDrawZHLine = isDrawLine;
		this.invalidate();
	}

	public void setDrawWUQLine(boolean isDrawLine) {
		this.misDrawWUQLine = isDrawLine;
		this.invalidate();
	}

	public void setDrawWUHLine(boolean isDrawLine) {
		this.misDrawWUHLine = isDrawLine;
		this.invalidate();
	}

	public void setDrawWANQLine(boolean isDrawLine) {
		this.misDrawWANQLine = isDrawLine;
		this.invalidate();
	}

	public void setDrawWANHLine(boolean isDrawLine) {
		this.misDrawWANHLine = isDrawLine;
		this.invalidate();
	}

	public void setDrawSQLine(boolean isDrawLine) {
		this.misDrawSQLine = isDrawLine;
		this.invalidate();
	}

}
