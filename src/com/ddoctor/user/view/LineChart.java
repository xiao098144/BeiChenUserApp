package com.ddoctor.user.view;

import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathEffect;
import android.graphics.Shader;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.View;

import com.beichen.user.R;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.wapi.bean.SugarValueBean;
import com.ddoctor.utils.MyUtils;
import com.ddoctor.utils.StringUtils;
import com.ddoctor.utils.TimeUtil;

/**
 * 血糖折线图
 * 
 * @author 萧
 * @Date 2015-5-21下午10:22:23
 * @TODO 欠缺异常情况处理 考虑添加点击事件
 */
public class LineChart extends View {

	public LineChart(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init(context);
	}

	public LineChart(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	public LineChart(Context context) {
		super(context);
		init(context);
	}

	private String time[] = new String[9];
	private String[] TYPE = new String[] { "空腹", "早后", "午前", "午后", "晚前",
			"晚后", "睡前" ,"凌晨"};
	private int highImg;
	private int normalImg;
	private int lowImg;
	private List<SugarValueBean> list;
	private SparseArray<float[]> points = new SparseArray<float[]>();
	/** 上限值 */
	private float highValue = DataModule.getInstance().getSugarUpBound();
	/** 下限值 */
	private float lowValue = DataModule.getInstance().getSugarDownBound();

	private int COLOR_HIGH; // 偏高线颜色
	private int COLOR_LOW; // 偏低线颜色
	private int COLOR_DASHLINE; // 虚线颜色
	private int COLOR_REALLINE; // 连线颜色
	private int COLOR_TEXT; // 字体颜色

	private final static int YDIVIDE = 6;

	private void init(Context context) {
		for (int i = 0; i < 9; i++) {
			time[i] = i * 3 + ":00";
		}
		highImg = R.drawable.sugar_high_bg;
		lowImg = R.drawable.sugar_low_bg;
		normalImg = R.drawable.sugar_normal_bg;
		COLOR_HIGH = context.getResources().getColor(
				R.color.color_sugar_standard_high);
		COLOR_LOW = context.getResources().getColor(
				R.color.color_sugar_standard_low);
		COLOR_DASHLINE = context.getResources().getColor(
				R.color.color_sugar_standard_dashline);
		COLOR_REALLINE = context.getResources().getColor(R.color.white);
		COLOR_TEXT = context.getResources().getColor(R.color.white);
		marginInPx = MyUtils.dip2px(getContext(), margin);

	}

	private Path shadowPath; // 绘制阴影shadow
	private Paint framePaint, linePaint, textPaint, shaderPaint;

	private int margin = 5;
	private int marginInPx;
	private float max = 10;
	private float min = 0;
	private float maxValue; // 根据最大值计算得到Y轴最高点
	private float minValue; // 根据记录最小值计算得到Y轴起点

	/** Y轴坐标 */
	private float[] yLoc = new float[YDIVIDE + 1];
	private float[] yValue = new float[YDIVIDE + 1];
	
	public void setData(List<SugarValueBean> list, float high_value,
			float low_value) {
		this.list = list;
//		this.highValue = high_value;
//		this.lowValue = low_value;
		maxValue = (float) Math.ceil(getMax());  
		minValue = (float) Math.floor(getMin());
		if (maxValue != minValue) {
			max = maxValue;
			min = minValue;
		} else {
			if (maxValue > max) {
				max = maxValue;
			}
		} 
		MyUtils.showLog(" LineChart setData list " + list.toString()
				+ " max = " + max + " min = " + min+" highValue = "+high_value+" low_value = "+low_value);
	}

	private float getMin() {
		if (list == null || list.size() == 0) {
			return 0;
		}
		float min = list.get(0).getValue();
		for (int i = 0; i < list.size(); i++) {
			if (min > list.get(i).getValue()) {
				min = list.get(i).getValue();
			}
		}
		return min;
	}

	private float getMax() {
		if (list == null || list.size() == 0) {
			return 10;
		}
		float max = list.get(0).getValue();
		for (int i = 0; i < list.size(); i++) {
			if (max < list.get(i).getValue()) {
				max = list.get(i).getValue();
			}
		}
		return max;
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		viewWidth = getWidth();
		viewHeight = getHeight();
		drawFrame(canvas);
		drawChartLine(canvas);
	}

	int viewWidth;
	int viewHeight;
	int XLength; // X轴长度
	int XStart; // X轴起点
	int XEnd; // X轴终点
	int YLength; // Y轴长度
	int YStart; // Y轴起点
	int YEnd; // Y轴终点
	int textDashHeight; // 文字下方虚线Y轴坐标
	int textHeight; // 文字Y轴坐标
	float Xoffset; // X轴偏移量
	float XCood[] = new float[time.length]; // 时间点X轴坐标 数组

	/**
	 * 绘制整体框架 初始化基本参数
	 * 
	 * @param canvas
	 */
	private void drawFrame(Canvas canvas) {
		framePaint = new Paint();
		framePaint.setAntiAlias(true);
		framePaint.setStrokeWidth(1.5f);
		framePaint.setColor(COLOR_TEXT);
		framePaint.setStyle(Paint.Style.FILL);
		framePaint.setTextSize(23);

		float textWidth = framePaint.measureText("00.0");
		XStart = (int) (textWidth + 2 * marginInPx);
		XEnd = (int) (viewWidth - 2 * marginInPx - textWidth);
		XLength = XEnd - XStart;
		Xoffset = 3 * textWidth / 4;

		YStart = (int) (viewHeight - 2 * marginInPx - textWidth / 2);
		YEnd = (int) (6 * marginInPx + textWidth / 2);
		YLength = YStart - YEnd;
		textDashHeight = YEnd - 3 * marginInPx;
		textHeight = textDashHeight - marginInPx;

		float ylength = (max - min) / YDIVIDE; // 由记录最大值与最小值动态计算得到Y轴坐标
		MyUtils.showLog("", "max " + max + " min = " + min + " " + ylength
				+ " Y轴 总长度  " + YLength);
		for (int i = 0; i < yLoc.length; i++) {
			yValue[i] = Float.valueOf(StringUtils.formatnum(min + ylength * i,
					".0f"));
			yLoc[i] = calculateHeight(yValue[i]) + marginInPx / 2;
			MyUtils.showLog("", "i = " + i + " 坐标  " + yLoc[i] + " 值  "
					+ yValue[i]);
		}

		canvas.drawLine(XStart, YStart, XStart, YEnd, framePaint); // 画Y轴
		canvas.drawLine(XStart, YStart, XEnd, YStart, framePaint); // 画X轴

		float lowHeight = calculateHeight(lowValue);
		float highHeight = calculateHeight(highValue); // 计算上下限Y坐标
		
		MyUtils.showLog("LineChart drawFrame 画上下限  lowValue "+lowValue+" highValue "+highValue+" lowHeight = "+lowHeight+" highHeight = "+highHeight+" YStart = "+YStart+" YEnd "+YEnd);
		
		if (lowHeight <= YStart) {
			drawText(canvas, String.valueOf(lowValue), XEnd, lowHeight + 10,
					framePaint);
			drawDashLine(XStart, lowHeight, XEnd, lowHeight, framePaint,
					COLOR_LOW, canvas); // 画 下限 分割线
		}
		if (highHeight >= YEnd) {
			drawText(canvas, String.valueOf(highValue), XEnd, highHeight + 10,
					framePaint);
			drawDashLine(XStart, highHeight, XEnd, highHeight, framePaint,
					COLOR_HIGH, canvas); // 画上限 分割线
		}

		for (int i = 0; i < time.length; i++) {
			XCood[i] = XStart - Xoffset + i * ((float) XLength / 8);

			drawText(canvas, time[i], XCood[i], viewHeight - textWidth / 4,
					framePaint);
		}
		for (int i = 0; i < yLoc.length; i++) {
			drawText(canvas, String.valueOf(yValue[i]),
					XStart - framePaint.measureText(String.valueOf(yValue[i]))
							- marginInPx, yLoc[i], framePaint);
		}

	}

	/**
	 * 绘制折线图
	 */
	private void drawChartLine(Canvas canvas) {
		if (list == null || list.size() == 0) {
			return;
		}
		for (int i = 0; i < list.size(); i++) { // 遍历集合 去除值为0的点
												// 根据记录时间与记录值计算对应的X、Y坐标
			if (list.get(i).getValue() == 0) {
				continue;
			}
			MyUtils.showLog("计算坐标   i = " + i + " X轴 "
					+ calculateWidth(list.get(i)) + "  Y轴  "
					+ calculateHeight(list.get(i).getValue()));
			points.append(i, new float[] { calculateWidth(list.get(i)),
					calculateHeight(list.get(i).getValue()),
					list.get(i).getType(), list.get(i).getValue() });
		}
		if (points.size() == 0) {
			return;
		}
		/** 绘制文字下方黑色虚线 */
		drawDashLine(XStart, textDashHeight, XEnd, textDashHeight, framePaint,
				COLOR_TEXT, canvas);

		textPaint = new Paint();
		textPaint.setColor(COLOR_TEXT);
		textPaint.setStyle(Paint.Style.FILL);
		textPaint.setTextSize(30);

		linePaint = new Paint();
		linePaint.setAntiAlias(true);
		linePaint.setStrokeWidth(2);
		linePaint.setColor(COLOR_REALLINE);

		if (points.size() == 1) {
			/** 每点根据类型画图 */
			int imgRes = normalImg;
			if (points.get(0)[3] >= highValue) {
				imgRes = highImg;
			} else if (points.get(0)[3] <= lowValue) {
				imgRes = lowImg;
			} else {
				imgRes = normalImg;
			}
			Bitmap bitmap = BitmapFactory
					.decodeResource(getResources(), imgRes);
			/** 绘制垂直虚线 */
			drawDashLine(points.get(0)[0],
					points.get(0)[1] - bitmap.getHeight(), points.get(0)[0],
					textHeight, linePaint, COLOR_DASHLINE, canvas);
			drawText(canvas, TYPE[(int) points.get(0)[2]], points.get(0)[0]
					- textPaint.measureText(TYPE[(int) points.get(0)[2]]) / 2,
					textHeight - marginInPx, textPaint);

			canvas.drawBitmap(bitmap, points.get(0)[0] - bitmap.getWidth() / 2,
					points.get(0)[1] - bitmap.getHeight() / 2, linePaint);
		} else {

			Shader mShader = new LinearGradient(0, 0, 0, getHeight(),
					new int[] { Color.argb(205, 222, 222, 222),
							Color.argb(105, 222, 222, 222),
							Color.argb(30, 222, 222, 222) }, null,
					Shader.TileMode.CLAMP);
			shaderPaint = new Paint();
			shaderPaint.setAntiAlias(true);
			shaderPaint.setShader(mShader);
			shaderPaint.setStrokeWidth(2);
			shadowPath = new Path();

			for (int i = 0; i < points.size() - 1; i++) {
				/** 绘制阴影 */
				if (i == 0) {
					shadowPath.moveTo(points.get(i)[0], YStart);
				}
				shadowPath.lineTo(points.get(i)[0], points.get(i)[1]);
				if (i == points.size() - 2) {
					shadowPath.lineTo(points.get(i)[0], points.get(i)[1]);
					shadowPath.lineTo(points.get(i + 1)[0],
							points.get(i + 1)[1]);
					shadowPath.lineTo(points.get(i + 1)[0], YStart);
					shadowPath.close();
					canvas.drawPath(shadowPath, shaderPaint);
				}
			}

			for (int i = 0; i < points.size() - 1; i++) {

				/** 描点连线 */
				canvas.drawLine(points.get(i)[0], points.get(i)[1],
						points.get(i + 1)[0], points.get(i + 1)[1], linePaint);

				/** 每点根据类型画图 */
				int imgRes = normalImg;
				if (points.get(i)[3] >= highValue) {
					imgRes = highImg;
				} else if (points.get(i)[3] <= lowValue) {
					imgRes = lowImg;
				} else {
					imgRes = normalImg;
				}
				MyUtils.showLog(" points.get(i)[3] = " + points.get(i)[3]
						+ " highvalue " + highValue + " lowValue " + lowValue
						+ "  imgRes " + imgRes);
				Bitmap bitmap = BitmapFactory.decodeResource(getResources(),
						imgRes);

				/** 绘制垂直虚线 */

				drawDashLine(points.get(i)[0],
						points.get(i)[1] - bitmap.getHeight(),
						points.get(i)[0], textHeight, linePaint,
						COLOR_DASHLINE, canvas);
				drawText(canvas, TYPE[(int) points.get(i)[2]], points.get(i)[0]
						- textPaint.measureText(TYPE[(int) points.get(i)[2]])
						/ 2, textHeight - marginInPx, textPaint);

				canvas.drawBitmap(bitmap, points.get(i)[0] - bitmap.getWidth()
						/ 2, points.get(i)[1] - bitmap.getHeight() / 2,
						linePaint);

				if (i == points.size() - 2) {
					if (points.get(i+1)[3] >= highValue) {
						imgRes = highImg;
					} else if (points.get(i+1)[3] <= lowValue) {
						imgRes = lowImg;
					} else {
						imgRes = normalImg;
					}
					Bitmap bitmap2 = BitmapFactory.decodeResource(
							getResources(), imgRes);
					canvas.drawBitmap(bitmap2,
							points.get(i + 1)[0] - bitmap2.getWidth() / 2,
							points.get(i + 1)[1] - bitmap2.getHeight() / 2,
							linePaint);
					drawDashLine(points.get(i + 1)[0], points.get(i + 1)[1]
							- bitmap.getHeight(), points.get(i + 1)[0],
							textHeight, linePaint, COLOR_DASHLINE, canvas);
					drawText(
							canvas,
							TYPE[(int) points.get(i + 1)[2]],
							points.get(i + 1)[0]
									- textPaint.measureText(TYPE[(int) points
											.get(i + 1)[2]]) / 2, textHeight
									- marginInPx, textPaint);
				}
			}
		}

	}

	/**
	 * 绘制虚线
	 * 
	 * @param xStart
	 *            x轴起点
	 * @param yStart
	 *            y轴起点
	 * @param xEnd
	 *            x轴终点
	 * @param yEnd
	 *            y轴终点
	 * @param paint
	 *            paint 必须设置Style Style.Stroke
	 * @param canvas
	 */
	private void drawDashLine(float xStart, float yStart, float xEnd,
			float yEnd, Paint paint, int color, Canvas canvas) {
		Path path = new Path();
		path.moveTo(xStart, yStart);
		path.lineTo(xEnd, yEnd);
		PathEffect effects = new DashPathEffect(new float[] { 5, 5, 5, 5 }, 2);
		
		Paint pt = new Paint(paint);
		pt.setPathEffect(effects);
		pt.setColor(color);
		pt.setStyle(Paint.Style.STROKE);
		
		canvas.drawPath(path, pt);
	}

	/**
	 * 绘制文字
	 * 
	 * @param canvas
	 * @param text
	 * @param x
	 * @param y
	 * @param paint
	 */
	private void drawText(Canvas canvas, String text, float x, float y,
			Paint paint) {
		paint.setStyle(Paint.Style.FILL);
		paint.setColor(COLOR_TEXT);
		canvas.drawText(text, x, y, paint);
	}

	/**
	 * 根据记录时间计算对应X轴坐标
	 * 
	 * @param sugar
	 * @return
	 */
	private float calculateWidth(SugarValueBean sugar) {
		if (TextUtils.isEmpty(sugar.getTime())) {
			return 0;
		}
		String time = TimeUtil.getInstance().formatTime(sugar.getTime());
		String[] split = time.split(":");
		Integer hour = Integer.valueOf(split[0]);
		int i = hour / 3;
		int minute = Integer.valueOf(split[1]);
		float xxlength = (float) (minute +(hour-i*3)*60) / 180
				* ((float) XLength / 8) + Xoffset;
		return XCood[i] + xxlength;
	}

	/** 由最大值计算Y轴几等分 从而计算得到最高限 最低限 与 中轴的位置 */
	private float calculateHeight(float value) {
		// if (max <= highValue) {
		// max = 12;
		// }
		return (float) (YStart - (value - min) / (max - min) * YLength);
	}

	private boolean isValueValid(float value) {
		return value <= highValue && value >= lowValue;
	}

}
