package com.ddoctor.user.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class PagerScrollView extends WindowScrollView {
	private float xDistance, yDistance, xLast, yLast;

	public PagerScrollView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	public boolean onInterceptTouchEvent(MotionEvent ev) {
		switch (ev.getAction()) {
		case MotionEvent.ACTION_DOWN:
			xDistance = yDistance = 0f;
			xLast = ev.getX();
			yLast = ev.getY();
			break;
		case MotionEvent.ACTION_MOVE:
			final float curX = ev.getX();
			final float curY = ev.getY();

			xDistance += Math.abs(curX - xLast);
			yDistance += Math.abs(curY - yLast);
			xLast = curX;
			yLast = curY;

			if (xDistance > yDistance) {
				return false;
			}
		}

		return super.onInterceptTouchEvent(ev);
	}

//	@Override
//	public void fling(int velocityY) {
//		super.fling(velocityY / 4);
//	}
}
