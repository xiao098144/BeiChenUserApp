package com.ddoctor.user.wapi;

import org.json.JSONObject;

import com.google.gson.Gson;


public class WAPI {

	// 正式地址
	public final static String WAPI_BASE_URL = "http://api.ddoctor.cn/s";
	// 天士力
	public final static String WAPI_TSL_URL = "http://api.ddoctor.cn/tsl";
	
	// 测试接口
//	public final static String WAPI_BASE_URL = "http://ddoctortest.jialer.com.cn/ddoctor/p";
//	public final static String WAPI_BASE_URL = "http://test2.ddoctor.cn/p";
	
//	public final static String WAPI_BASE_URL = "http://test.ddoctor.cn/s";
//	// 天士力接口
//	public final static String WAPI_TSL_URL = "http://test.ddoctor.cn/tsl";
	
	
	
	public final static String TSL_URL_ORDER_SUCCESS = "http://www.ddoctor.cn/tsl/success-orders.html";
	public final static String TSL_URL_MENTE = "http://www.ddoctor.cn/tsl/mentoNote.htm";
	
	// bean转换成JSONObject
	public static JSONObject beanToJSONObject(Object obj)
	{
		JSONObject jsonObj = null;
		try{
			Gson gson = new Gson();
			String json_string = gson.toJson(obj);
			jsonObj = new JSONObject(json_string);
		}catch(Exception e){
			e.printStackTrace();
		}
		return jsonObj;
	}
}
