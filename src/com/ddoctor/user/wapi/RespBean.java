package com.ddoctor.user.wapi;

import org.json.JSONObject;

public class RespBean {
	public int code = -1;
	public String errMsg = "";

	public void setCode(int code)
	{
		this.code = code;
	}

	public int getCode()
	{
		return this.code;
	}

	public void setErrMsg(String errMsg)
	{
		this.errMsg = errMsg;
	}

	public String getErrMsg()
	{
		return this.errMsg;
	}
	
	public void copyFrom(RespBean vo) {
		this.code = vo.code;
		this.errMsg = vo.errMsg;
	}

	
	@Override
	public String toString() {
		return "RespBean [code=" + code + ", errMsg=" + errMsg + "]";
	}

	public boolean isSuccess()
	{
		return (code == 1);
	}
	
	public int parse(JSONObject json)
	{
		try{
			this.code = json.optInt("code");
			this.errMsg = json.optString("errMsg");
			
			return 0;
		}
		catch(Exception e){
		}
		
		return 1;
	}
	
}
