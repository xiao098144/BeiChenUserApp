package com.ddoctor.user.wapi.bean;

/**
 * <p>Title: Value code</p>
 * <p>Description: data view</p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: ddoctor</p>
 * @author tian jiale
 * @version 1.0
 */

import android.os.Parcel;
import android.os.Parcelable;

import com.ddoctor.enums.RecordLayoutType;

public class HydcfyzBean implements Parcelable , Comparable<HydcfyzBean>{
	/**
	 * 定义本类的私有变量
	 */
	private Integer id;
	private String name;
	private String file;
	private String remark;
	private String time;
	private Integer patientId;
	private Integer type;
	private String date; // yyyy-MM-dd

	private RecordLayoutType layoutType = RecordLayoutType.TYPE_VALUE;

	/**
	 * 本类的实例化方法
	 */
	public HydcfyzBean() {
	}

	/**
	 * 本类的带参数的实例化方法
	 */

	public HydcfyzBean(Integer id, String name, String file, String remark,
			String time, Integer patientId, Integer type, String date,
			RecordLayoutType layoutType) {
		super();
		this.id = id;
		this.name = name;
		this.file = file;
		this.remark = remark;
		this.time = time;
		this.patientId = patientId;
		this.type = type;
		this.date = date;
		this.layoutType = layoutType;
	}

	/**
	 * 以本类为参数传入时，实例化本类的方法
	 */
	public void copyFrom(HydcfyzBean vo) {
		id = vo.id;
		name = vo.name;
		file = vo.file;
		remark = vo.remark;
		time = vo.time;
		patientId = vo.patientId;
		type = vo.type;
		date = vo.date;
		layoutType = vo.layoutType;
	}

	/**
	 * 本类的赋值方法
	 */
	public void setData(HydcfyzBean vo) {
		this.copyFrom(vo);
	}

	/**
	 * 本类的取值方法
	 */
	public HydcfyzBean getData() {
		HydcfyzBean result = new HydcfyzBean();
		result.copyFrom(this);
		return result;
	}

	/**
	 * id的赋值方法
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * name的赋值方法
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * file的赋值方法
	 */
	public void setFile(String file) {
		this.file = file;
	}

	/**
	 * remark的赋值方法
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

	/**
	 * time的赋值方法
	 */
	public void setTime(String time) {
		this.time = time;
	}

	/**
	 * patientId的赋值方法
	 */
	public void setPatientId(Integer patientId) {
		this.patientId = patientId;
	}

	/**
	 * type的赋值方法
	 */
	public void setType(Integer type) {
		this.type = type;
	}

	/**
	 * id的取值方法
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * name的取值方法
	 */
	public String getName() {
		return name;
	}

	/**
	 * file的取值方法
	 */
	public String getFile() {
		return file;
	}

	/**
	 * remark的取值方法
	 */
	public String getRemark() {
		return remark;
	}

	/**
	 * time的取值方法
	 */
	public String getTime() {
		return time;
	}

	/**
	 * patientId的取值方法
	 */
	public Integer getPatientId() {
		return patientId;
	}

	/**
	 * type的取值方法
	 */
	public Integer getType() {
		return type;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getDate() {
		return date;
	}

	public void setLayoutType(RecordLayoutType layoutType) {
		this.layoutType = layoutType;
	}

	public RecordLayoutType getLayoutType() {
		return layoutType;
	}

	@Override
	public String toString() {
		return "HydcfyzBean [id=" + id + ", name=" + name + ", file=" + file
				+ ", remark=" + remark + ", time=" + time + ", patientId="
				+ patientId + ", type=" + type + ", date=" + date
				+ ", layoutType=" + layoutType + "]";
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeValue(this.id);
		dest.writeString(this.name);
		dest.writeString(this.file);
		dest.writeString(this.remark);
		dest.writeString(this.time);
		dest.writeValue(this.patientId);
		dest.writeValue(this.type);
		dest.writeString(this.date);
		dest.writeInt(this.layoutType == RecordLayoutType.TYPE_VALUE ? 1 : 2);
	}

	protected HydcfyzBean(Parcel in) {
		this.id = (Integer) in.readValue(Integer.class.getClassLoader());
		this.name = in.readString();
		this.file = in.readString();
		this.remark = in.readString();
		this.time = in.readString();
		this.patientId = (Integer) in.readValue(Integer.class.getClassLoader());
		this.type = (Integer) in.readValue(Integer.class.getClassLoader());
		this.date = in.readString();
		int tmpLayoutType = in.readInt();
		this.layoutType = tmpLayoutType == 1 ? RecordLayoutType.TYPE_VALUE
				: RecordLayoutType.TYPE_CATEGORY;
	}

	public static final Parcelable.Creator<HydcfyzBean> CREATOR = new Parcelable.Creator<HydcfyzBean>() {
		public HydcfyzBean createFromParcel(Parcel source) {
			return new HydcfyzBean(source);
		}

		public HydcfyzBean[] newArray(int size) {
			return new HydcfyzBean[size];
		}
	};

	@Override
	public int compareTo(HydcfyzBean another) {
		int compare;
		compare = getTime().compareTo(another.getTime());
		return 0 - compare;
	}
}