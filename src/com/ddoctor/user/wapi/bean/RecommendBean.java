package com.ddoctor.user.wapi.bean;

/**
 * <p>Title: Value code</p>
 * <p>Description: data view</p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: ddoctor</p>
 * @author tian jiale
 * @version 1.0
 */

public class RecommendBean implements java.io.Serializable {
/**
*定义本类的私有变量
*/
   private Integer id;
   private String mobile;
   private String status;
   private String time;
   private String name;
   private Integer userid;
   private String image;
   private Integer isdoctor;
/**
*本类的实例化方法
*/
   public RecommendBean() {
   }
/**
*本类的带参数的实例化方法
*/
   public RecommendBean(Integer id,String mobile,String status,String time,String name,Integer userid,String image,Integer isdoctor) {
      this.id = id;
      this.mobile = mobile;
      this.status = status;
      this.time = time;
      this.name = name;
      this.userid = userid;
      this.image = image;
      this.isdoctor = isdoctor;
   }
/**
*以本类为参数传入时，实例化本类的方法
*/
   public void copyFrom(RecommendBean vo) {
      id = vo.id;
      mobile = vo.mobile;
      status = vo.status;
      time = vo.time;
      name = vo.name;
      userid = vo.userid;
      image = vo.image;
      isdoctor = vo.isdoctor;
   }
/**
*本类的赋值方法
*/
   public void setData(RecommendBean vo) {
      this.copyFrom(vo);
   }
/**
*本类的取值方法
*/
   public RecommendBean getData(){
      RecommendBean result = new RecommendBean();
      result.copyFrom(this);
      return result;
   }
/**
*id的赋值方法
*/
   public void setId(Integer id){
      this.id = id;
   }
/**
*mobile的赋值方法
*/
   public void setMobile(String mobile){
      this.mobile = mobile;
   }
/**
*status的赋值方法
*/
   public void setStatus(String status){
      this.status = status;
   }
/**
*time的赋值方法
*/
   public void setTime(String time){
      this.time = time;
   }
/**
*name的赋值方法
*/
   public void setName(String name){
      this.name = name;
   }
/**
*userid的赋值方法
*/
   public void setUserid(Integer userid){
      this.userid = userid;
   }
/**
*image的赋值方法
*/
   public void setImage(String image){
      this.image = image;
   }
/**
*isdoctor的赋值方法
*/
   public void setIsdoctor(Integer isdoctor){
      this.isdoctor = isdoctor;
   }
/**
*id的取值方法
*/
   public Integer getId() {
      return id;
   }
/**
*mobile的取值方法
*/
   public String getMobile() {
      return mobile;
   }
/**
*status的取值方法
*/
   public String getStatus() {
      return status;
   }
/**
*time的取值方法
*/
   public String getTime() {
      return time;
   }
/**
*name的取值方法
*/
   public String getName() {
      return name;
   }
/**
*userid的取值方法
*/
   public Integer getUserid() {
      return userid;
   }
/**
*image的取值方法
*/
   public String getImage() {
      return image;
   }
/**
*isdoctor的取值方法
*/
   public Integer getIsdoctor() {
      return isdoctor;
   }
/**
*id取值方法
*/
}