package com.ddoctor.user.wapi.bean;

/**
 * <p>Title: Value code</p>
 * <p>Description: data view</p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: ddoctor</p>
 * @author tian jiale
 * @version 1.0
 */

import java.util.List;

public class GlucometerBean implements java.io.Serializable {
	/**
	 * 定义本类的私有变量
	 */
	private Integer id;
	private String product;
	private String type;
	private String name;
	private String remark;
	private List<GlucometerBean> subglucometers;

	/**
	 * 本类的实例化方法
	 */
	public GlucometerBean() {
	}

	/**
	 * 本类的带参数的实例化方法
	 */
	public GlucometerBean(Integer id, String product, String type,
			String name, String remark, List<GlucometerBean> subglucometers) {
		this.id = id;
		this.product = product;
		this.type = type;
		this.name = name;
		this.remark = remark;
		this.subglucometers = subglucometers;
	}

	/**
	 * 以本类为参数传入时，实例化本类的方法
	 */
	public void copyFrom(GlucometerBean vo) {
		id = vo.id;
		product = vo.product;
		type = vo.type;
		name = vo.name;
		remark = vo.remark;
		subglucometers = vo.subglucometers;
	}

	/**
	 * 本类的赋值方法
	 */
	public void setData(GlucometerBean vo) {
		this.copyFrom(vo);
	}

	/**
	 * 本类的取值方法
	 */
	public GlucometerBean getData() {
		GlucometerBean result = new GlucometerBean();
		result.copyFrom(this);
		return result;
	}

	/**
	 * id的赋值方法
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * product的赋值方法
	 */
	public void setProduct(String product) {
		this.product = product;
	}

	/**
	 * type的赋值方法
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * name的赋值方法
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * remark的赋值方法
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

	/**
	 * subglucometers的赋值方法
	 */
	public void setSubglucometers(List<GlucometerBean> subglucometers) {
		this.subglucometers = subglucometers;
	}
	
	/**
	 * id的取值方法
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * product的取值方法
	 */
	public String getProduct() {
		return product;
	}

	/**
	 * type的取值方法
	 */
	public String getType() {
		return type;
	}

	/**
	 * name的取值方法
	 */
	public String getName() {
		return name;
	}

	/**
	 * remark的取值方法
	 */
	public String getRemark() {
		return remark;
	}
	
	/**
	 * remark的取值方法
	 */
	public List<GlucometerBean> getSubglucometers() {
		return subglucometers;
	}
	
	/**
	 * id取值方法
	 */
}