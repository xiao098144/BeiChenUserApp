package com.ddoctor.user.wapi.bean;

/**
 * <p>Title: Value code</p>
 * <p>Description: data view</p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: ddoctor</p>
 * @author tian jiale
 * @version 1.0
 */


public class DeliverBean implements java.io.Serializable {
	/**
	 * 定义本类的私有变量
	 */
	private Integer id;
	private Integer patientId;
	private String name;
	private String mobile;
	private String address;
	private Float fee;
	private Integer status;
	private Integer province;
	private Integer city;
	private Integer area;
	private String code;
	private String street;
	private String provinceName;
	private String cityName;
	private String areaName;

	/**
	 * 本类的实例化方法
	 */
	public DeliverBean() {
	}

	/**
	 * 本类的带参数的实例化方法
	 */
	public DeliverBean(Integer id, Integer patientId,String name, String mobile,
			String address, Float fee, Integer status, Integer province,
			Integer city, Integer area, String code, String street,
			String provinceName, String cityName, String areaName) {
		this.id = id;
		this.patientId = patientId;
		this.name = name;
		this.mobile = mobile;
		this.address = address;
		this.fee = fee;
		this.status = status;
		this.province = province;
		this.city = city;
		this.area = area;
		this.code = code;
		this.street = street;
		this.provinceName = provinceName;
		this.cityName = cityName;
		this.areaName = areaName;
	}

	/**
	 * 以本类为参数传入时，实例化本类的方法
	 */
	public void copyFrom(DeliverBean vo) {
		id = vo.id;
		patientId = vo.patientId;
		name = vo.name;
		mobile = vo.mobile;
		address = vo.address;
		fee = vo.fee;
		status = vo.status;
		province = vo.province;
		city = vo.city;
		area = vo.area;
		code = vo.code;
		street = vo.street;
		provinceName = vo.provinceName;
		cityName = vo.cityName;
		areaName = vo.areaName;
	}

	/**
	 * 本类的赋值方法
	 */
	public void setData(DeliverBean vo) {
		this.copyFrom(vo);
	}

	/**
	 * 本类的取值方法
	 */
	public DeliverBean getData() {
		DeliverBean result = new DeliverBean();
		result.copyFrom(this);
		return result;
	}

	/**
	 * id的赋值方法
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * patientId的赋值方法
	 */
	public void setPatientId(Integer patientId) {
		this.patientId = patientId;
	}

	/**
	 * name的赋值方法
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * mobile的赋值方法
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	/**
	 * address的赋值方法
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * fee的赋值方法
	 */
	public void setFee(Float fee) {
		this.fee = fee;
	}

	/**
	 * status的赋值方法
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * province的赋值方法
	 */
	public void setProvince(Integer province) {
		this.province = province;
	}

	/**
	 * city的赋值方法
	 */
	public void setCity(Integer city) {
		this.city = city;
	}

	/**
	 * area的赋值方法
	 */
	public void setArea(Integer area) {
		this.area = area;
	}

	/**
	 * code的赋值方法
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * street的赋值方法
	 */
	public void setStreet(String street) {
		this.street = street;
	}

	/**
	 * provinceName的赋值方法
	 */
	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}

	/**
	 * cityName的赋值方法
	 */
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	/**
	 * areaName的赋值方法
	 */
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	/**
	 * id的取值方法
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * name的取值方法
	 */
	public Integer getPatientId() {
		return patientId;
	}

	/**
	 * name的取值方法
	 */
	public String getName() {
		return name;
	}

	/**
	 * mobile的取值方法
	 */
	public String getMobile() {
		return mobile;
	}

	/**
	 * address的取值方法
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * fee的取值方法
	 */
	public Float getFee() {
		return fee;
	}

	/**
	 * status的取值方法
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * province的取值方法
	 */
	public Integer getProvince() {
		return province;
	}

	/**
	 * city的取值方法
	 */
	public Integer getCity() {
		return city;
	}

	/**
	 * area的取值方法
	 */
	public Integer getArea() {
		return area;
	}

	/**
	 * code的取值方法
	 */
	public String getCode() {
		return code;
	}

	/**
	 * street的取值方法
	 */
	public String getStreet() {
		return street;
	}

	/**
	 * provinceName的取值方法
	 */
	public String getProvinceName() {
		return provinceName;
	}

	/**
	 * cityName的取值方法
	 */
	public String getCityName() {
		return cityName;
	}

	/**
	 * areaName的取值方法
	 */
	public String getAreaName() {
		return areaName;
	}
	/**
	 * id取值方法
	 */
}