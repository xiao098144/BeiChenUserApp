package com.ddoctor.user.wapi.bean;

/**
 * <p>Title: 不适</p>
 * <p>Description: data view</p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: ddoctor</p>
 * @author tian jiale
 * @version 1.0
 */

import android.os.Parcel;
import android.os.Parcelable;

import com.ddoctor.enums.RecordLayoutType;

public class TroubleBean implements Parcelable, Comparable<TroubleBean> {
	/**
	 * 定义本类的私有变量
	 */
	private Integer id;
	private String item;
	private String time;
	private String remark;
	private Integer patientId;
	private String date; // yyyy-MM-dd

	private RecordLayoutType layoutType = RecordLayoutType.TYPE_VALUE;

	/**
	 * 本类的实例化方法
	 */
	public TroubleBean() {
	}

	/**
	 * 本类的带参数的实例化方法
	 */

	public TroubleBean(Integer id, String item, String time, String remark,
			Integer patientId, String date, RecordLayoutType layoutType) {
		super();
		this.id = id;
		this.item = item;
		this.time = time;
		this.remark = remark;
		this.patientId = patientId;
		this.date = date;
		this.layoutType = layoutType;
	}

	/**
	 * 以本类为参数传入时，实例化本类的方法
	 */
	public void copyFrom(TroubleBean vo) {
		id = vo.id;
		item = vo.item;
		time = vo.time;
		remark = vo.remark;
		patientId = vo.patientId;
		date = vo.date;
		layoutType = vo.layoutType;
	}

	/**
	 * 本类的赋值方法
	 */
	public void setData(TroubleBean vo) {
		this.copyFrom(vo);
	}

	/**
	 * 本类的取值方法
	 */
	public TroubleBean getData() {
		TroubleBean result = new TroubleBean();
		result.copyFrom(this);
		return result;
	}

	/**
	 * id的赋值方法
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * item的赋值方法
	 */
	public void setItem(String item) {
		this.item = item;
	}

	/**
	 * time的赋值方法
	 */
	public void setTime(String time) {
		this.time = time;
	}

	/**
	 * remark的赋值方法
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

	/**
	 * patientId的赋值方法
	 */
	public void setPatientId(Integer patientId) {
		this.patientId = patientId;
	}

	/**
	 * id的取值方法
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * item的取值方法
	 */
	public String getItem() {
		return item;
	}

	/**
	 * time的取值方法
	 */
	public String getTime() {
		return time;
	}

	/**
	 * remark的取值方法
	 */
	public String getRemark() {
		return remark;
	}

	/**
	 * patientId的取值方法
	 */
	public Integer getPatientId() {
		return patientId;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getDate() {
		return date;
	}

	public void setLayoutType(RecordLayoutType layoutType) {
		this.layoutType = layoutType;
	}

	public RecordLayoutType getLayoutType() {
		return layoutType;
	}

	@Override
	public String toString() {
		return "TroubleBean [id=" + id + ", item=" + item + ", time=" + time
				+ ", remark=" + remark + ", patientId=" + patientId + ", date="
				+ date + ", layoutType=" + layoutType + "]";
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeValue(this.id);
		dest.writeString(this.item);
		dest.writeString(this.time);
		dest.writeString(this.remark);
		dest.writeValue(this.patientId);
		dest.writeString(this.date);
		dest.writeInt(this.layoutType == RecordLayoutType.TYPE_VALUE ? 1 : 2);
	}

	protected TroubleBean(Parcel in) {
		this.id = (Integer) in.readValue(Integer.class.getClassLoader());
		this.item = in.readString();
		this.time = in.readString();
		this.remark = in.readString();
		this.patientId = (Integer) in.readValue(Integer.class.getClassLoader());
		this.date = in.readString();
		int tmpLayoutType = in.readInt();
		this.layoutType = tmpLayoutType == 1 ? RecordLayoutType.TYPE_VALUE
				: RecordLayoutType.TYPE_CATEGORY;
	}

	public static final Parcelable.Creator<TroubleBean> CREATOR = new Parcelable.Creator<TroubleBean>() {
		public TroubleBean createFromParcel(Parcel source) {
			return new TroubleBean(source);
		}

		public TroubleBean[] newArray(int size) {
			return new TroubleBean[size];
		}
	};

	@Override
	public int compareTo(TroubleBean another) {
		int compare;
		compare = getTime().compareTo(another.getTime());
		return 0 - compare;
	}
}