package com.ddoctor.user.wapi.bean;

/**
 * <p>Title: Value code</p>
 * <p>Description: data view</p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: ddoctor</p>
 * @author tian jiale
 * @version 1.0
 */


public class OutpatientBean implements java.io.Serializable {
	/**
	 * 定义本类的私有变量
	 */
	private Integer id;
	private String content;
	private Integer weekType;
	private Integer timeType;
	private String remark;

	/**
	 * 本类的实例化方法
	 */
	public OutpatientBean() {
	}

	/**
	 * 本类的带参数的实例化方法
	 */
	public OutpatientBean(Integer id, String content, Integer weekType,
			Integer timeType, String remark) {
		this.id = id;
		this.content = content;
		this.weekType = weekType;
		this.timeType = timeType;
		this.remark = remark;
	}

	/**
	 * 以本类为参数传入时，实例化本类的方法
	 */
	public void copyFrom(OutpatientBean vo) {
		id = vo.id;
		content = vo.content;
		weekType = vo.weekType;
		timeType = vo.timeType;
		remark = vo.remark;
	}

	/**
	 * 本类的赋值方法
	 */
	public void setData(OutpatientBean vo) {
		this.copyFrom(vo);
	}

	/**
	 * 本类的取值方法
	 */
	public OutpatientBean getData() {
		OutpatientBean result = new OutpatientBean();
		result.copyFrom(this);
		return result;
	}

	/**
	 * id的赋值方法
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * content的赋值方法
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * weekType的赋值方法
	 */
	public void setWeekType(Integer weekType) {
		this.weekType = weekType;
	}

	/**
	 * timeType的赋值方法
	 */
	public void setTimeType(Integer timeType) {
		this.timeType = timeType;
	}

	/**
	 * remark的赋值方法
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

	/**
	 * id的取值方法
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * content的取值方法
	 */
	public String getContent() {
		return content;
	}

	/**
	 * weekType的取值方法
	 */
	public Integer getWeekType() {
		return weekType;
	}

	/**
	 * timeType的取值方法
	 */
	public Integer getTimeType() {
		return timeType;
	}

	/**
	 * remark的取值方法
	 */
	public String getRemark() {
		return remark;
	}
	/**
	 * id取值方法
	 */
}