package com.ddoctor.user.wapi.bean;

import android.os.Parcel;
import android.os.Parcelable;

import com.ddoctor.enums.RecordLayoutType;

public class RecordBean implements Parcelable {

	private Integer id;
	private Integer valueType;
	private String time;
	private String col1;
	private String col2;
	private String col3;
	private String date; // yyyy-MM-dd

	private RecordLayoutType layoutType = RecordLayoutType.TYPE_VALUE;

	public RecordBean() {
		super();
		// TODO Auto-generated constructor stub
	}

	public RecordBean(Integer id, String time, String col1, String col2,
			String col3, String date, RecordLayoutType layoutType , Integer valueType) {
		super();
		this.id = id;
		this.time = time;
		this.col1 = col1;
		this.col2 = col2;
		this.col3 = col3;
		this.date = date;
		this.layoutType = layoutType;
		this.valueType = valueType;
	}

	public void copyFrom(RecordBean vo) {
		id = vo.id;
		time = vo.time;
		col1 = vo.col1;
		col2 = vo.col2;
		col3 = vo.col3;
		date = vo.date;
		layoutType = vo.layoutType;
		valueType = vo.valueType;
	}

	/**
	 * 本类的赋值方法
	 */
	public void setData(RecordBean vo) {
		this.copyFrom(vo);
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getCol1() {
		return col1;
	}

	public void setCol1(String col1) {
		this.col1 = col1;
	}

	public String getCol2() {
		return col2;
	}

	public void setCol2(String col2) {
		this.col2 = col2;
	}

	public String getCol3() {
		return col3;
	}

	public void setCol3(String col3) {
		this.col3 = col3;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public RecordLayoutType getLayoutType() {
		return layoutType;
	}

	public void setLayoutType(RecordLayoutType layoutType) {
		this.layoutType = layoutType;
	}

	public Integer getValueType() {
		return valueType;
	}
	
	public void setValueType(Integer valueType) {
		this.valueType = valueType;
	}
	
	@Override
	public String toString() {
		return "RecordBean [id=" + id + ", valueType=" + valueType + ", time="
				+ time + ", col1=" + col1 + ", col2=" + col2 + ", col3=" + col3
				+ ", date=" + date + ", layoutType=" + layoutType + "]";
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeValue(this.id);
		dest.writeString(this.time);
		dest.writeString(this.col1);
		dest.writeString(this.col2);
		dest.writeString(this.col3);
		dest.writeString(this.date);
		dest.writeInt(this.layoutType == null ? -1 : this.layoutType.ordinal());
		dest.writeValue(this.valueType);
	}

	protected RecordBean(Parcel in) {
		this.id = (Integer) in.readValue(Integer.class.getClassLoader());
		this.time = in.readString();
		this.col1 = in.readString();
		this.col2 = in.readString();
		this.col3 = in.readString();
		this.date = in.readString();
		int tmpLayoutType = in.readInt();
		this.layoutType = tmpLayoutType == -1 ? null : RecordLayoutType.values()[tmpLayoutType];
		this.valueType = (Integer) in.readValue(Integer.class.getClassLoader());
	}

	public static final Parcelable.Creator<RecordBean> CREATOR = new Parcelable.Creator<RecordBean>() {
		public RecordBean createFromParcel(Parcel source) {
			return new RecordBean(source);
		}

		public RecordBean[] newArray(int size) {
			return new RecordBean[size];
		}
	};
}
