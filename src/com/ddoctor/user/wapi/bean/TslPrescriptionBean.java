package com.ddoctor.user.wapi.bean;

import java.util.List;

/**
 * <p>Title: Value code</p>
 * <p>Description: tsl处方bean</p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: ddoctor</p>
 * @author tian jiale
 * @version 1.0
 */

public class TslPrescriptionBean implements java.io.Serializable {
/**
	 * 
	 */
	private static final long serialVersionUID = 7982408325182129968L;
/**
*定义本类的私有变量
*/
   private Integer id;
   private Integer tysPatientId;
   private String tslUserId;
   private String tslPrescriptionId;
   private String name;
   private Integer parentId;
   private String hospital;
   private String doctor;
   private String symptom;
   private String prescriptionInfo;
   private String prescriptionValue;
   private String prescriptionImage;
   private String prescriptionTime;
   private Integer tslPrescriptionState;
   private String  tslPrescriptionMsg;
   private String createTime;
   private String state;
   private List<TslProductBean> productList;
/**
*本类的实例化方法
*/
   public TslPrescriptionBean() {
   }
/**
*本类的带参数的实例化方法
*/
   public TslPrescriptionBean(Integer id,Integer tysPatientId,String tslUserId,String tslPrescriptionId,String name,Integer parentId,String hospital,String doctor,String symptom,String prescriptionInfo,String prescriptionValue,String prescriptionImage,String prescriptionTime,Integer tslPrescriptionState, String tslPrescriptionMsg,String createTime, String state,List<TslProductBean> productList) {
      this.id = id;
      this.tysPatientId = tysPatientId;
      this.tslUserId = tslUserId;
      this.tslPrescriptionId = tslPrescriptionId;
      this.name = name;
      this.parentId = parentId;
      this.hospital = hospital;
      this.doctor = doctor;
      this.symptom = symptom;
      this.prescriptionInfo = prescriptionInfo;
      this.prescriptionValue = prescriptionValue;
      this.prescriptionImage = prescriptionImage;
      this.prescriptionTime = prescriptionTime;
      this.tslPrescriptionState = tslPrescriptionState;
      this.tslPrescriptionMsg = tslPrescriptionMsg;
      this.createTime = createTime;
      this.state = state;
      this.productList = productList;
   }
/**
*以本类为参数传入时，实例化本类的方法
*/
   public void copyFrom(TslPrescriptionBean vo) {
      id = vo.id;
      tysPatientId = vo.tysPatientId;
      tslUserId = vo.tslUserId;
      tslPrescriptionId = vo.tslPrescriptionId;
      name = vo.name;
      parentId = vo.parentId;
      hospital = vo.hospital;
      doctor = vo.doctor;
      symptom = vo.symptom;
      prescriptionInfo = vo.prescriptionInfo;
      prescriptionValue = vo.prescriptionValue;
      prescriptionImage = vo.prescriptionImage;
      prescriptionTime = vo.prescriptionTime;
      tslPrescriptionState = vo.tslPrescriptionState;
      tslPrescriptionMsg = vo.tslPrescriptionMsg;
      createTime = vo.createTime;
      state = vo.state;
      productList = vo.productList;
   }
/**
*本类的赋值方法
*/
   public void setData(TslPrescriptionBean vo) {
      this.copyFrom(vo);
   }
/**
*本类的取值方法
*/
   public TslPrescriptionBean getData(){
      TslPrescriptionBean result = new TslPrescriptionBean();
      result.copyFrom(this);
      return result;
   }
/**
*id的赋值方法
*/
   public void setId(Integer id){
      this.id = id;
   }
/**
*tslUserId的赋值方法
*/
   public void setTslUserId(String tslUserId){
      this.tslUserId = tslUserId;
   }
/**
*tslPrescriptionId的赋值方法
*/
   public void setTslPrescriptionId(String tslPrescriptionId){
      this.tslPrescriptionId = tslPrescriptionId;
   }
/**
*name的赋值方法
*/
   public void setName(String name){
      this.name = name;
   }
/**
*parentId的赋值方法
*/
   public void setParentId(Integer parentId){
      this.parentId = parentId;
   }
/**
*hospital的赋值方法
*/
   public void setHospital(String hospital){
      this.hospital = hospital;
   }
/**
*doctor的赋值方法
*/
   public void setDoctor(String doctor){
      this.doctor = doctor;
   }
/**
*symptom的赋值方法
*/
   public void setSymptom(String symptom){
      this.symptom = symptom;
   }
/**
*prescriptionInfo的赋值方法
*/
   public void setPrescriptionInfo(String prescriptionInfo){
      this.prescriptionInfo = prescriptionInfo;
   }
/**
*prescriptionValue的赋值方法
*/
   public void setPrescriptionValue(String prescriptionValue){
      this.prescriptionValue = prescriptionValue;
   }
/**
*prescriptionImage的赋值方法
*/
   public void setPrescriptionImage(String prescriptionImage){
      this.prescriptionImage = prescriptionImage;
   }

/**
*tslPrescriptionState的赋值方法
*/
   public void setTslPrescriptionState(Integer tslPrescriptionState){
      this.tslPrescriptionState = tslPrescriptionState;
   }

/**
*id的取值方法
*/
   public Integer getId() {
      return id;
   }
/**
*tslUserId的取值方法
*/
   public String getTslUserId() {
      return tslUserId;
   }
/**
*tslPrescriptionId的取值方法
*/
   public String getTslPrescriptionId() {
      return tslPrescriptionId;
   }
/**
*name的取值方法
*/
   public String getName() {
      return name;
   }
/**
*parentId的取值方法
*/
   public Integer getParentId() {
      return parentId;
   }
/**
*hospital的取值方法
*/
   public String getHospital() {
      return hospital;
   }
/**
*doctor的取值方法
*/
   public String getDoctor() {
      return doctor;
   }
/**
*symptom的取值方法
*/
   public String getSymptom() {
      return symptom;
   }
/**
*prescriptionInfo的取值方法
*/
   public String getPrescriptionInfo() {
      return prescriptionInfo;
   }
/**
*prescriptionValue的取值方法
*/
   public String getPrescriptionValue() {
      return prescriptionValue;
   }
/**
*prescriptionImage的取值方法
*/
   public String getPrescriptionImage() {
      return prescriptionImage;
   }

/**
*tslPrescriptionState的取值方法
*/
   public Integer getTslPrescriptionState() {
      return tslPrescriptionState;
   }

/**
*id取值方法
*/
public Integer getTysPatientId() {
	return tysPatientId;
}
public void setTysPatientId(Integer tysPatientId) {
	this.tysPatientId = tysPatientId;
}
public String getState() {
	return state;
}
public void setState(String state) {
	this.state = state;
}
public List<TslProductBean> getProductList() {
	return productList;
}
public void setProductList(List<TslProductBean> productList) {
	this.productList = productList;
}
public String getPrescriptionTime() {
	return prescriptionTime;
}
public void setPrescriptionTime(String prescriptionTime) {
	this.prescriptionTime = prescriptionTime;
}
public String getCreateTime() {
	return createTime;
}
public void setCreateTime(String createTime) {
	this.createTime = createTime;
}
public String getTslPrescriptionMsg() {
	return tslPrescriptionMsg;
}
public void setTslPrescriptionMsg(String tslPrescriptionMsg) {
	this.tslPrescriptionMsg = tslPrescriptionMsg;
}
}