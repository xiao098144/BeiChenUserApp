package com.ddoctor.user.wapi.bean;

/**
 * <p>Title: Value code</p>
 * <p>Description: data view</p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: ddoctor</p>
 * @author tian jiale
 * @version 1.0
 */

import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;

import com.ddoctor.enums.RecordLayoutType;

public class DietBean implements Parcelable, Comparable<DietBean> {
	/**
	 * 定义本类的私有变量
	 */
	private Integer id;
	private Integer type;
	private String name;
	private String time;
	private String file;
	private Integer sugar;
	private Integer heat;
	private String remark;
	private List<FoodBean> guShuList;
	private List<FoodBean> shuCaiList;
	private List<FoodBean> shuiGuoList;
	private List<FoodBean> daDouList;
	private List<FoodBean> ruList;
	private List<FoodBean> rouDanList;
	private List<FoodBean> yingGuoList;
	private List<FoodBean> youZhiList;
	private Integer patientId;

	private String date;
	private RecordLayoutType layoutType = RecordLayoutType.TYPE_VALUE;

	/**
	 * 本类的实例化方法
	 */
	public DietBean() {
	}

	public DietBean(Integer id, Integer type, String name, String time,
			String file, Integer sugar, Integer heat, String remark,
			List<FoodBean> guShuList, List<FoodBean> shuCaiList,
			List<FoodBean> shuiGuoList, List<FoodBean> daDouList,
			List<FoodBean> ruList, List<FoodBean> rouDanList,
			List<FoodBean> yingGuoList, List<FoodBean> youZhiList,
			Integer patientId, String date, RecordLayoutType layoutType) {
		super();
		this.id = id;
		this.type = type;
		this.name = name;
		this.time = time;
		this.file = file;
		this.sugar = sugar;
		this.heat = heat;
		this.remark = remark;
		this.guShuList = guShuList;
		this.shuCaiList = shuCaiList;
		this.shuiGuoList = shuiGuoList;
		this.daDouList = daDouList;
		this.ruList = ruList;
		this.rouDanList = rouDanList;
		this.yingGuoList = yingGuoList;
		this.youZhiList = youZhiList;
		this.patientId = patientId;
		this.date = date;
		this.layoutType = layoutType;
	}

	/**
	 * 以本类为参数传入时，实例化本类的方法
	 */
	public void copyFrom(DietBean vo) {
		id = vo.id;
		type = vo.type;
		name = vo.name;
		time = vo.time;
		file = vo.file;
		sugar = vo.sugar;
		heat = vo.heat;
		remark = vo.remark;
		guShuList = vo.guShuList;
		shuCaiList = vo.shuCaiList;
		shuiGuoList = vo.shuiGuoList;
		daDouList = vo.daDouList;
		ruList = vo.ruList;
		rouDanList = vo.rouDanList;
		yingGuoList = vo.yingGuoList;
		youZhiList = vo.youZhiList;
		patientId = vo.patientId;
		layoutType = vo.layoutType;
		date = vo.date;
	}

	/**
	 * 本类的赋值方法
	 */
	public void setData(DietBean vo) {
		this.copyFrom(vo);
	}

	/**
	 * 本类的取值方法
	 */
	public DietBean getData() {
		DietBean result = new DietBean();
		result.copyFrom(this);
		return result;
	}

	/**
	 * id的赋值方法
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * type的赋值方法
	 */
	public void setType(Integer type) {
		this.type = type;
	}

	/**
	 * name的赋值方法
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * time的赋值方法
	 */
	public void setTime(String time) {
		this.time = time;
	}

	/**
	 * file的赋值方法
	 */
	public void setFile(String file) {
		this.file = file;
	}

	/**
	 * sugar的赋值方法
	 */
	public void setSugar(Integer sugar) {
		this.sugar = sugar;
	}

	/**
	 * heat的赋值方法
	 */
	public void setHeat(Integer heat) {
		this.heat = heat;
	}

	/**
	 * remark的赋值方法
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

	/**
	 * patientId的赋值方法
	 */
	public void setPatientId(Integer patientId) {
		this.patientId = patientId;
	}

	/**
	 * id的取值方法
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * type的取值方法
	 */
	public Integer getType() {
		return type;
	}

	/**
	 * name的取值方法
	 */
	public String getName() {
		return name;
	}

	/**
	 * time的取值方法
	 */
	public String getTime() {
		return time;
	}

	/**
	 * file的取值方法
	 */
	public String getFile() {
		return file;
	}

	/**
	 * sugar的取值方法
	 */
	public Integer getSugar() {
		return sugar;
	}

	/**
	 * heat的取值方法
	 */
	public Integer getHeat() {
		return heat;
	}

	/**
	 * remark的取值方法
	 */
	public String getRemark() {
		return remark;
	}

	/**
	 * heat的取值方法
	 */
	public Integer getPatientId() {
		return patientId;
	}

	public RecordLayoutType getLayoutType() {
		return layoutType;
	}

	public void setLayoutType(RecordLayoutType layoutType) {
		this.layoutType = layoutType;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public List<FoodBean> getGuShuList() {
		return guShuList;
	}

	public void setGuShuList(List<FoodBean> guShuList) {
		this.guShuList = guShuList;
	}

	public List<FoodBean> getShuCaiList() {
		return shuCaiList;
	}

	public void setShuCaiList(List<FoodBean> shuCaiList) {
		this.shuCaiList = shuCaiList;
	}

	public List<FoodBean> getShuiGuoList() {
		return shuiGuoList;
	}

	public void setShuiGuoList(List<FoodBean> shuiGuoList) {
		this.shuiGuoList = shuiGuoList;
	}

	public List<FoodBean> getDaDouList() {
		return daDouList;
	}

	public void setDaDouList(List<FoodBean> daDouList) {
		this.daDouList = daDouList;
	}

	public List<FoodBean> getRuList() {
		return ruList;
	}

	public void setRuList(List<FoodBean> ruList) {
		this.ruList = ruList;
	}

	public List<FoodBean> getRouDanList() {
		return rouDanList;
	}

	public void setRouDanList(List<FoodBean> rouDanList) {
		this.rouDanList = rouDanList;
	}

	public List<FoodBean> getYingGuoList() {
		return yingGuoList;
	}

	public void setYingGuoList(List<FoodBean> yingGuoList) {
		this.yingGuoList = yingGuoList;
	}

	public List<FoodBean> getYouZhiList() {
		return youZhiList;
	}

	public void setYouZhiList(List<FoodBean> youZhiList) {
		this.youZhiList = youZhiList;
	}

	@Override
	public String toString() {
		return "DietBean [id=" + id + ", type=" + type + ", name=" + name
				+ ", time=" + time + ", file=" + file + ", sugar=" + sugar
				+ ", heat=" + heat + ", remark=" + remark + "]";
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeValue(this.id);
		dest.writeValue(this.type);
		dest.writeString(this.name);
		dest.writeString(this.time);
		dest.writeString(this.file);
		dest.writeValue(this.sugar);
		dest.writeValue(this.heat);
		dest.writeString(this.remark);
		dest.writeTypedList(guShuList);
		dest.writeTypedList(shuCaiList);
		dest.writeTypedList(shuiGuoList);
		dest.writeTypedList(daDouList);
		dest.writeTypedList(ruList);
		dest.writeTypedList(rouDanList);
		dest.writeTypedList(yingGuoList);
		dest.writeTypedList(youZhiList);
		dest.writeValue(this.patientId);
		dest.writeInt(layoutType == RecordLayoutType.TYPE_VALUE ? 0 : 1);
		dest.writeString(this.date);
	}

	protected DietBean(Parcel in) {
		this.id = (Integer) in.readValue(Integer.class.getClassLoader());
		this.type = (Integer) in.readValue(Integer.class.getClassLoader());
		this.name = in.readString();
		this.time = in.readString();
		this.file = in.readString();
		this.sugar = (Integer) in.readValue(Integer.class.getClassLoader());
		this.heat = (Integer) in.readValue(Integer.class.getClassLoader());
		this.remark = in.readString();
		this.guShuList = in.createTypedArrayList(FoodBean.CREATOR);
		this.shuCaiList = in.createTypedArrayList(FoodBean.CREATOR);
		this.shuiGuoList = in.createTypedArrayList(FoodBean.CREATOR);
		this.ruList = in.createTypedArrayList(FoodBean.CREATOR);
		this.rouDanList = in.createTypedArrayList(FoodBean.CREATOR);
		this.yingGuoList = in.createTypedArrayList(FoodBean.CREATOR);
		this.youZhiList = in.createTypedArrayList(FoodBean.CREATOR);
		this.patientId = (Integer) in.readValue(Integer.class.getClassLoader());
		this.layoutType = in.readInt() == 0 ? RecordLayoutType.TYPE_VALUE
				: RecordLayoutType.TYPE_CATEGORY;
		this.date = in.readString();
	}

	public static final Parcelable.Creator<DietBean> CREATOR = new Parcelable.Creator<DietBean>() {
		public DietBean createFromParcel(Parcel source) {
			return new DietBean(source);
		}

		public DietBean[] newArray(int size) {
			return new DietBean[size];
		}
	};

	@Override
	public int compareTo(DietBean another) {
		int compare;
		compare = getDate().compareTo(another.getDate());
		if (compare == 0) {
			if (null == getHeat()) {
				compare = 1;
			}
			if (null == another.getHeat()) {
				compare = -1;
			}
			if (getHeat() != null && another.getHeat() != null) {
				compare = getHeat().compareTo(another.getHeat());
			}
		}
		return 0 - compare;
	}
}