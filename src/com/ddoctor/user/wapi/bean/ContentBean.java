package com.ddoctor.user.wapi.bean;

import android.os.Parcel;
import android.os.Parcelable;

public class ContentBean implements Parcelable {
	/**
	 * 定义本类的私有变量
	 */
	private Integer contentId;
	private Integer classId;
	private String title;
	private String urlTitle;
	private String fontColor;
	private Integer fontBold;
	private Integer fontEm;
	private String position;
	private String keyWords;
	private String description;
	private String time;
	private String image;
	private String url;
	private Integer sequence;
	private Integer status;
	private String copyFrom;
	private Integer views;
	private Integer tagLink;
	private String tpl;
	private Integer site;

	/**
	 * 本类的实例化方法
	 */
	public ContentBean() {

	}

	/**
	 * 本类的带参数的实例化方法
	 */

	public ContentBean(Integer contentId, Integer classId, String title,
			String urlTitle, String fontColor, Integer fontBold,
			Integer fontEm, String position, String keyWords,
			String description, String time, String image, String url,
			Integer sequence, Integer status, String copyFrom, Integer views,
			Integer tagLink, String tpl, Integer site) {
		this.contentId = contentId;
		this.classId = classId;
		this.title = title;
		this.urlTitle = urlTitle;
		this.fontColor = fontColor;
		this.fontBold = fontBold;
		this.fontEm = fontEm;
		this.position = position;
		this.keyWords = keyWords;
		this.description = description;
		this.time = time;
		this.image = image;
		this.url = url;
		this.sequence = sequence;
		this.status = status;
		this.copyFrom = copyFrom;
		this.views = views;
		this.tagLink = tagLink;
		this.tpl = tpl;
		this.site = site;
	}

	/**
	 * 以本类为参数传入时，实例化本类的方法
	 */
	public void copyFrom(ContentBean vo) {
		contentId = vo.contentId;
		classId = vo.classId;
		title = vo.title;
		urlTitle = vo.urlTitle;
		fontColor = vo.fontColor;
		fontBold = vo.fontBold;
		fontEm = vo.fontEm;
		position = vo.position;
		keyWords = vo.keyWords;
		description = vo.description;
		time = vo.time;
		image = vo.image;
		url = vo.url;
		sequence = vo.sequence;
		status = vo.status;
		copyFrom = vo.copyFrom;
		views = vo.views;
		tagLink = vo.tagLink;
		tpl = vo.tpl;
		site = vo.site;
	}

	/**
	 * 本类的赋值方法
	 */
	public void setData(ContentBean vo) {
		this.copyFrom(vo);
	}

	/**
	 * 本类的取值方法
	 */

	public ContentBean getData() {
		ContentBean result = new ContentBean();
		result.copyFrom(this);
		return result;
	}

	public Integer getContentId() {
		return contentId;
	}

	public void setContentId(Integer contentId) {
		this.contentId = contentId;
	}

	public Integer getClassId() {
		return classId;
	}

	public void setClassId(Integer classId) {
		this.classId = classId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUrlTitle() {
		return urlTitle;
	}

	public void setUrlTitle(String urlTitle) {
		this.urlTitle = urlTitle;
	}

	public String getFontColor() {
		return fontColor;
	}

	public void setFontColor(String fontColor) {
		this.fontColor = fontColor;
	}

	public Integer getFontBold() {
		return fontBold;
	}

	public void setFontBold(Integer fontBold) {
		this.fontBold = fontBold;
	}

	public Integer getFontEm() {
		return fontEm;
	}

	public void setFontEm(Integer fontEm) {
		this.fontEm = fontEm;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getKeyWords() {
		return keyWords;
	}

	public void setKeyWords(String keyWords) {
		this.keyWords = keyWords;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Integer getSequence() {
		return sequence;
	}

	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getCopyFrom() {
		return copyFrom;
	}

	public void setCopyFrom(String copyFrom) {
		this.copyFrom = copyFrom;
	}

	public Integer getViews() {
		return views;
	}

	public void setViews(Integer views) {
		this.views = views;
	}

	public Integer getTagLink() {
		return tagLink;
	}

	public void setTagLink(Integer tagLink) {
		this.tagLink = tagLink;
	}

	public String getTpl() {
		return tpl;
	}

	public void setTpl(String tpl) {
		this.tpl = tpl;
	}

	public Integer getSite() {
		return site;
	}

	public void setSite(Integer site) {
		this.site = site;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeValue(this.contentId);
		dest.writeValue(this.classId);
		dest.writeString(this.title);
		dest.writeString(this.urlTitle);
		dest.writeString(this.fontColor);
		dest.writeValue(this.fontBold);
		dest.writeValue(this.fontEm);
		dest.writeString(this.position);
		dest.writeString(this.keyWords);
		dest.writeString(this.description);
		dest.writeString(this.time);
		dest.writeString(this.image);
		dest.writeString(this.url);
		dest.writeValue(this.sequence);
		dest.writeValue(this.status);
		dest.writeString(this.copyFrom);
		dest.writeValue(this.views);
		dest.writeValue(this.tagLink);
		dest.writeString(this.tpl);
		dest.writeValue(this.site);
	}

	protected ContentBean(Parcel in) {
		this.contentId = (Integer) in.readValue(Integer.class.getClassLoader());
		this.classId = (Integer) in.readValue(Integer.class.getClassLoader());
		this.title = in.readString();
		this.urlTitle = in.readString();
		this.fontColor = in.readString();
		this.fontBold = (Integer) in.readValue(Integer.class.getClassLoader());
		this.fontEm = (Integer) in.readValue(Integer.class.getClassLoader());
		this.position = in.readString();
		this.keyWords = in.readString();
		this.description = in.readString();
		this.time = in.readString();
		this.image = in.readString();
		this.url = in.readString();
		this.sequence = (Integer) in.readValue(Integer.class.getClassLoader());
		this.status = (Integer) in.readValue(Integer.class.getClassLoader());
		this.copyFrom = in.readString();
		this.views = (Integer) in.readValue(Integer.class.getClassLoader());
		this.tagLink = (Integer) in.readValue(Integer.class.getClassLoader());
		this.tpl = in.readString();
		this.site = (Integer) in.readValue(Integer.class.getClassLoader());
	}

	public static final Parcelable.Creator<ContentBean> CREATOR = new Parcelable.Creator<ContentBean>() {
		public ContentBean createFromParcel(Parcel source) {
			return new ContentBean(source);
		}

		public ContentBean[] newArray(int size) {
			return new ContentBean[size];
		}
	};
}
