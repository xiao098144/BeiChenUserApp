package com.ddoctor.user.wapi.bean;

/**
 * <p>Title: 科室</p>
 * <p>Description: data view</p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: ddoctor</p>
 * @author tian jiale
 * @version 1.0
 */


public class DepartmentBean implements java.io.Serializable {
	/**
	 * 定义本类的私有变量
	 */
	private Integer id;
	private String hospitalId;
	private String name;

	/**
	 * 本类的实例化方法
	 */
	public DepartmentBean() {
	}

	/**
	 * 本类的带参数的实例化方法
	 */
	public DepartmentBean(Integer id, String hospitalId, String name) {
		this.id = id;
		this.hospitalId = hospitalId;
		this.name = name;
	}

	/**
	 * 以本类为参数传入时，实例化本类的方法
	 */
	public void copyFrom(DepartmentBean vo) {
		id = vo.id;
		hospitalId = vo.hospitalId;
		name = vo.name;
	}

	/**
	 * 本类的赋值方法
	 */
	public void setData(DepartmentBean vo) {
		this.copyFrom(vo);
	}

	/**
	 * 本类的取值方法
	 */
	public DepartmentBean getData() {
		DepartmentBean result = new DepartmentBean();
		result.copyFrom(this);
		return result;
	}

	/**
	 * id的赋值方法
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * hospitalId的赋值方法
	 */
	public void setHospitalId(String hospitalId) {
		this.hospitalId = hospitalId;
	}

	/**
	 * name的赋值方法
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * id的取值方法
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * hospitalId的取值方法
	 */
	public String getHospitalId() {
		return hospitalId;
	}

	/**
	 * name的取值方法
	 */
	public String getName() {
		return name;
	}
	/**
	 * id取值方法
	 */
}