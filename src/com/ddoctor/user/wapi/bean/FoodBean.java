package com.ddoctor.user.wapi.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * <p>Title: Value code</p>
 * <p>Description: data view</p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: ddoctor</p>
 * @author tian jiale
 * @version 1.0
 */


public class FoodBean implements Comparable<FoodBean>,Parcelable {
	/**
	 * 定义本类的私有变量
	 */
	private Integer id;
	private String name;
	private Integer count;
	private String unit;
	private String remark;
	private Integer dietId;
	private Integer type;
	private String typeName;

	/**
	 * 本类的实例化方法
	 */
	public FoodBean() {
	}

	/**
	 * 本类的带参数的实例化方法
	 */
	public FoodBean(Integer id, String name, Integer count, String unit,
					String remark, Integer dietId, Integer type , String typeName) {
		this.id = id;
		this.name = name;
		this.count = count;
		this.unit = unit;
		this.remark = remark;
		this.dietId = dietId;
		this.type = type;
		this.typeName = typeName;
	}

	/**
	 * 以本类为参数传入时，实例化本类的方法
	 */
	public void copyFrom(FoodBean vo) {
		id = vo.id;
		name = vo.name;
		count = vo.count;
		unit = vo.unit;
		remark = vo.remark;
		dietId = vo.dietId;
		type = vo.type;
		typeName = vo.typeName;
	}

	/**
	 * 本类的赋值方法
	 */
	public void setData(FoodBean vo) {
		this.copyFrom(vo);
	}

	/**
	 * 本类的取值方法
	 */
	public FoodBean getData() {
		FoodBean result = new FoodBean();
		result.copyFrom(this);
		return result;
	}

	/**
	 * id的赋值方法
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * name的赋值方法
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * count的赋值方法
	 */
	public void setCount(Integer count) {
		this.count = count;
	}

	/**
	 * unit的赋值方法
	 */
	public void setUnit(String unit) {
		this.unit = unit;
	}

	/**
	 * remark的赋值方法
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

	/**
	 * dietId的赋值方法
	 */
	public void setDietId(Integer dietId) {
		this.dietId = dietId;
	}

	/**
	 * id的取值方法
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * name的取值方法
	 */
	public String getName() {
		return name;
	}

	/**
	 * count的取值方法
	 */
	public Integer getCount() {
		return count;
	}

	/**
	 * unit的取值方法
	 */
	public String getUnit() {
		return unit;
	}

	/**
	 * remark的取值方法
	 */
	public String getRemark() {
		return remark;
	}

	/**
	 * dietId的取值方法
	 */
	public Integer getDietId() {
		return dietId;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}
	
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	public String getTypeName() {
		return typeName;
	}
	

	@Override
	public String toString() {
		return "FoodBean [id = "+id+" name=" + name + ", count=" + count + ", unit=" + unit
				+ ", type=" + type + "]";
	}
	
	@Override
	public int compareTo(FoodBean another) {
		int compare;
		compare = getType().compareTo(another.getType());
		if (compare == 0) {
			compare = getName().compareTo(another.getName());
		}
		return compare;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeValue(this.id);
		dest.writeString(this.name);
		dest.writeValue(this.count);
		dest.writeString(this.unit);
		dest.writeString(this.remark);
		dest.writeValue(this.dietId);
		dest.writeValue(this.type);
		dest.writeString(this.typeName);
	}

	protected FoodBean(Parcel in) {
		this.id = (Integer) in.readValue(Integer.class.getClassLoader());
		this.name = in.readString();
		this.count = (Integer) in.readValue(Integer.class.getClassLoader());
		this.unit = in.readString();
		this.remark = in.readString();
		this.dietId = (Integer) in.readValue(Integer.class.getClassLoader());
		this.type = (Integer) in.readValue(Integer.class.getClassLoader());
		this.typeName = in.readString();
	}

	public static final Parcelable.Creator<FoodBean> CREATOR = new Parcelable.Creator<FoodBean>() {
		public FoodBean createFromParcel(Parcel source) {
			return new FoodBean(source);
		}

		public FoodBean[] newArray(int size) {
			return new FoodBean[size];
		}
	};
}