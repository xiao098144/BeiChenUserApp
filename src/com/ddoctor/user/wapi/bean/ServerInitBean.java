package com.ddoctor.user.wapi.bean;
/**
 * <p>Title: Value code</p>
 * <p>Description: data view</p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: ddoctor</p>
 * @author tian jiale
 * @version 1.0
 */

import java.util.List;


public class ServerInitBean implements java.io.Serializable {
	/**
	 * 定义本类的私有变量
	 */
	private Integer updateTag;
	private ClientUpdateBean update;
	private String zfbBackUrl;
	private Integer dictag;
	private Integer dicsn;
	private List<GlucometerBean> glucometers;
	private List<IllnessBean> illnesss;
	private List<MedicalBean> medicals;
	private List<TroubleitemBean> troubleitems;
	private List<DistrictBean> districts;
	private List<HospitalBean> hospitals;
	private List<DepartmentBean> departments;
	private List<LevelBean> levels;
	private PatientBean patient;
	private DoctorBean doctor;
	private TslPatientBean tslPatient;
	private Integer mobileArea;

	/**
	 * 本类的实例化方法
	 */
	public ServerInitBean() {
	}

	/**
	 * 本类的带参数的实例化方法
	 */
	public ServerInitBean(Integer updateTag, ClientUpdateBean update,String zfbBackUrl,
			Integer dictag, Integer dicsn, List<GlucometerBean> glucometers,
			List<IllnessBean> illnesss,List<MedicalBean> medicals, List<TroubleitemBean> troubleitems,
			List<DistrictBean> districts, List<HospitalBean> hospitals,
			List<DepartmentBean> departments, List<LevelBean> levels,
			PatientBean patient, DoctorBean doctor,TslPatientBean tslPatient) {
		this.updateTag = updateTag;
		this.update = update;
		this.zfbBackUrl = zfbBackUrl;
		this.dictag = dictag;
		this.dicsn = dicsn;
		this.glucometers = glucometers;
		this.illnesss = illnesss;
		this.medicals = medicals;
		this.troubleitems = troubleitems;
		this.districts = districts;
		this.hospitals = hospitals;
		this.departments = departments;
		this.levels = levels;
		this.patient = patient;
		this.doctor = doctor;
		this.tslPatient = tslPatient;
	}

	/**
	 * 以本类为参数传入时，实例化本类的方法
	 */
	public void copyFrom(ServerInitBean vo) {
		updateTag = vo.updateTag;
		update = vo.update;
		zfbBackUrl = vo.zfbBackUrl;
		dictag = vo.dictag;
		dicsn = vo.dicsn;
		glucometers = vo.glucometers;
		illnesss = vo.illnesss;
		medicals = vo.medicals;
		troubleitems = vo.troubleitems;
		districts = vo.districts;
		hospitals = vo.hospitals;
		departments = vo.departments;
		levels = vo.levels;
		patient = vo.patient;
		doctor = vo.doctor;
		tslPatient = vo.tslPatient;
	}

	/**
	 * 本类的赋值方法
	 */
	public void setData(ServerInitBean vo) {
		this.copyFrom(vo);
	}

	/**
	 * 本类的取值方法
	 */
	public ServerInitBean getData() {
		ServerInitBean result = new ServerInitBean();
		result.copyFrom(this);
		return result;
	}

	/**
	 * updateTag的赋值方法
	 */
	public void setUpdateTag(Integer updateTag) {
		this.updateTag = updateTag;
	}

	/**
	 * update的赋值方法
	 */
	public void setUpdate(ClientUpdateBean update) {
		this.update = update;
	}

	/**
	 * dictag的赋值方法
	 */
	public void setDictag(Integer dictag) {
		this.dictag = dictag;
	}

	/**
	 * dicsn的赋值方法
	 */
	public void setDicsn(Integer dicsn) {
		this.dicsn = dicsn;
	}

	/**
	 * glucometers的赋值方法
	 */
	public void setGlucometers(List<GlucometerBean> glucometers) {
		this.glucometers = glucometers;
	}

	/**
	 * illnesss的赋值方法
	 */
	public void setIllnesss(List<IllnessBean> illnesss) {
		this.illnesss = illnesss;
	}

	/**
	 * illnesss的赋值方法
	 */
	public void setMedicals(List<MedicalBean> medicals) {
		this.medicals = medicals;
	}
	
	/**
	 * troubleitems的赋值方法
	 */
	public void setTroubleitems(List<TroubleitemBean> troubleitems) {
		this.troubleitems = troubleitems;
	}

	/**
	 * districts的赋值方法
	 */
	public void setDistricts(List<DistrictBean> districts) {
		this.districts = districts;
	}

	/**
	 * hospitals的赋值方法
	 */
	public void setHospitals(List<HospitalBean> hospitals) {
		this.hospitals = hospitals;
	}

	/**
	 * departments的赋值方法
	 */
	public void setDepartments(List<DepartmentBean> departments) {
		this.departments = departments;
	}

	/**
	 * levels的赋值方法
	 */
	public void setLevels(List<LevelBean> levels) {
		this.levels = levels;
	}

	/**
	 * patient的赋值方法
	 */
	public void setPatient(PatientBean patient) {
		this.patient = patient;
	}

	/**
	 * doctor的赋值方法
	 */
	public void setDoctor(DoctorBean doctor) {
		this.doctor = doctor;
	}

	/**
	 * updateTag的取值方法
	 */
	public Integer getUpdateTag() {
		return updateTag;
	}

	/**
	 * update的取值方法
	 */
	public ClientUpdateBean getUpdate() {
		return update;
	}

	/**
	 * dictag的取值方法
	 */
	public Integer getDictag() {
		return dictag;
	}

	/**
	 * dicsn的取值方法
	 */
	public Integer getDicsn() {
		return dicsn;
	}

	/**
	 * glucometers的取值方法
	 */
	public List<GlucometerBean> getGlucometers() {
		return glucometers;
	}

	/**
	 * illnesss的取值方法
	 */
	public List<IllnessBean> getIllnesss() {
		return illnesss;
	}

	/**
	 * medicals的取值方法
	 */
	public List<MedicalBean> getMedicals() {
		return  medicals;
	}

	/**
	 * troubleitems的取值方法
	 */
	public List<TroubleitemBean> getTroubleitems() {
		return troubleitems;
	}

	/**
	 * districts的取值方法
	 */
	public List<DistrictBean> getDistricts() {
		return districts;
	}

	/**
	 * hospitals的取值方法
	 */
	public List<HospitalBean> getHospitals() {
		return hospitals;
	}

	/**
	 * departments的取值方法
	 */
	public List<DepartmentBean> getDepartments() {
		return departments;
	}

	/**
	 * levels的取值方法
	 */
	public List<LevelBean> getLevels() {
		return levels;
	}

	/**
	 * patient的取值方法
	 */
	public PatientBean getPatient() {
		return patient;
	}

	/**
	 * doctor的取值方法
	 */
	public DoctorBean getDoctor() {
		return doctor;
	}
	/**
	 * updateTag取值方法
	 */

	public TslPatientBean getTslPatient() {
		return tslPatient;
	}

	public void setTslPatient(TslPatientBean tslPatient) {
		this.tslPatient = tslPatient;
	}

	public String getZfbBackUrl() {
		return zfbBackUrl;
	}

	public void setZfbBackUrl(String zfbBackUrl) {
		this.zfbBackUrl = zfbBackUrl;
	}

	public Integer getMobileArea() {
		return mobileArea;
	}

	public void setMobileArea(Integer mobileArea) {
		this.mobileArea = mobileArea;
	}
}