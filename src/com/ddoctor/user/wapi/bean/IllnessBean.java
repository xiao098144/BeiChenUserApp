package com.ddoctor.user.wapi.bean;

/**
 * <p>
 * Title: Value code
 * </p>
 * <p>
 * Description: data view
 * </p>
 * <p>
 * Copyright: Copyright (c) 2007
 * </p>
 * <p>
 * Company: ddoctor
 * </p>
 * 
 * @author tian jiale
 * @version 1.0
 */

public class IllnessBean implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 定义本类的私有变量
	 */
	private Integer id;
	private String type;
	private String name;

	/**
	 * 本类的实例化方法
	 */
	public IllnessBean() {
	}

	/**
	 * 本类的带参数的实例化方法
	 */
	public IllnessBean(Integer id, String type, String name) {
		this.id = id;
		this.type = type;
		this.name = name;
	}

	/**
	 * 以本类为参数传入时，实例化本类的方法
	 */
	public void copyFrom(IllnessBean vo) {
		id = vo.id;
		type = vo.type;
		name = vo.name;
	}

	/**
	 * 本类的赋值方法
	 */
	public void setData(IllnessBean vo) {
		this.copyFrom(vo);
	}

	/**
	 * 本类的取值方法
	 */
	public IllnessBean getData() {
		IllnessBean result = new IllnessBean();
		result.copyFrom(this);
		return result;
	}

	/**
	 * id的赋值方法
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * type的赋值方法
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * name的赋值方法
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * id的取值方法
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * type的取值方法
	 */
	public String getType() {
		return type;
	}

	/**
	 * name的取值方法
	 */
	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return "IllnessBean [id=" + id + ", type=" + type + ", name=" + name
				+ "]";
	}

}