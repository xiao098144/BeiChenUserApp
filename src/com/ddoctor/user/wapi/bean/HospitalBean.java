package com.ddoctor.user.wapi.bean;

/**
 * <p>Title: Value code</p>
 * <p>Description: data view</p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: ddoctor</p>
 * @author tian jiale
 * @version 1.0
 */


public class HospitalBean implements java.io.Serializable {
	/**
	 * 定义本类的私有变量
	 */
	private Integer id;
	private String districtId;
	private String name;

	/**
	 * 本类的实例化方法
	 */
	public HospitalBean() {
	}

	/**
	 * 本类的带参数的实例化方法
	 */
	public HospitalBean(Integer id, String districtId, String name) {
		this.id = id;
		this.districtId = districtId;
		this.name = name;
	}

	/**
	 * 以本类为参数传入时，实例化本类的方法
	 */
	public void copyFrom(HospitalBean vo) {
		id = vo.id;
		districtId = vo.districtId;
		name = vo.name;
	}

	/**
	 * 本类的赋值方法
	 */
	public void setData(HospitalBean vo) {
		this.copyFrom(vo);
	}

	/**
	 * 本类的取值方法
	 */
	public HospitalBean getData() {
		HospitalBean result = new HospitalBean();
		result.copyFrom(this);
		return result;
	}

	/**
	 * id的赋值方法
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * districtId的赋值方法
	 */
	public void setDistrictId(String districtId) {
		this.districtId = districtId;
	}

	/**
	 * name的赋值方法
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * id的取值方法
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * districtId的取值方法
	 */
	public String getDistrictId() {
		return districtId;
	}

	/**
	 * name的取值方法
	 */
	public String getName() {
		return name;
	}
	/**
	 * id取值方法
	 */
}