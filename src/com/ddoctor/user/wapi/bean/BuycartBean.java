package com.ddoctor.user.wapi.bean;

/**
 * <p>Title: Value code</p>
 * <p>Description: data view</p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: ddoctor</p>
 * @author tian jiale
 * @version 1.0
 */

public class BuycartBean implements java.io.Serializable {
/**
*定义本类的私有变量
*/
   private Integer id;
   private Integer count;
   private Integer status;
   private Integer orderId;
   private Integer patientId;
/**
*本类的实例化方法
*/
   public BuycartBean() {
   }
/**
*本类的带参数的实例化方法
*/
   public BuycartBean(Integer id,Integer count,Integer status,Integer orderId,Integer patientId) {
      this.id = id;
      this.count = count;
      this.status = status;
      this.orderId = orderId;
      this.patientId = patientId;
   }
/**
*以本类为参数传入时，实例化本类的方法
*/
   public void copyFrom(BuycartBean vo) {
      id = vo.id;
      count = vo.count;
      status = vo.status;
      orderId = vo.orderId;
      patientId = vo.patientId;
   }
/**
*本类的赋值方法
*/
   public void setData(BuycartBean vo) {
      this.copyFrom(vo);
   }
/**
*本类的取值方法
*/
   public BuycartBean getData(){
      BuycartBean result = new BuycartBean();
      result.copyFrom(this);
      return result;
   }
/**
*id的赋值方法
*/
   public void setId(Integer id){
      this.id = id;
   }
/**
*count的赋值方法
*/
   public void setCount(Integer count){
      this.count = count;
   }
/**
*status的赋值方法
*/
   public void setStatus(Integer status){
      this.status = status;
   }
/**
*orderId的赋值方法
*/
   public void setOrderId(Integer orderId){
      this.orderId = orderId;
   }
/**
*patientId的赋值方法
*/
   public void setPatientId(Integer patientId){
      this.patientId = patientId;
   }
/**
*id的取值方法
*/
   public Integer getId() {
      return id;
   }
/**
*count的取值方法
*/
   public Integer getCount() {
      return count;
   }
/**
*status的取值方法
*/
   public Integer getStatus() {
      return status;
   }
/**
*orderId的取值方法
*/
   public Integer getOrderId() {
      return orderId;
   }
/**
*patientId的取值方法
*/
   public Integer getPatientId() {
      return patientId;
   }
/**
*id取值方法
*/
}