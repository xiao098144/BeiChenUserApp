package com.ddoctor.user.wapi.bean;

import android.os.Parcel;
import android.os.Parcelable;

public class KnowlegeBean implements Parcelable {
	/**
	 * 定义本类的私有变量
	 */
	private Integer id;
	private String title;
	private String time;
	private String content;
	private String author;
	private String keyword;
	private Integer praise;
	private Integer recommend;
	private String image;
	private Integer type;

	/**
	 * 本类的实例化方法
	 */
	public KnowlegeBean() {
	}

	/**
	 * 本类的带参数的实例化方法
	 */
	public KnowlegeBean(Integer id, String title, String time, String content,
			String author, String keyword, Integer praise, Integer recommend,
			String image, Integer type) {
		this.id = id;
		this.title = title;
		this.time = time;
		this.content = content;
		this.author = author;
		this.keyword = keyword;
		this.praise = praise;
		this.recommend = recommend;
		this.image = image;
		this.type = type;
	}

	/**
	 * 以本类为参数传入时，实例化本类的方法
	 */
	public void copyFrom(KnowlegeBean vo) {
		id = vo.id;
		title = vo.title;
		time = vo.time;
		content = vo.content;
		author = vo.author;
		keyword = vo.keyword;
		praise = vo.praise;
		recommend = vo.recommend;
		image = vo.image;
		type = vo.type;
	}

	/**
	 * 本类的赋值方法
	 */
	public void setData(KnowlegeBean vo) {
		this.copyFrom(vo);
	}

	/**
	 * 本类的取值方法
	 */
	public KnowlegeBean getData() {
		KnowlegeBean result = new KnowlegeBean();
		result.copyFrom(this);
		return result;
	}

	/**
	 * id的赋值方法
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * title的赋值方法
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * time的赋值方法
	 */
	public void setTime(String time) {
		this.time = time;
	}

	/**
	 * content的赋值方法
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * author的赋值方法
	 */
	public void setAuthor(String author) {
		this.author = author;
	}

	/**
	 * keyword的赋值方法
	 */
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	/**
	 * praise的赋值方法
	 */
	public void setPraise(Integer praise) {
		this.praise = praise;
	}

	/**
	 * recommend的赋值方法
	 */
	public void setRecommend(Integer recommend) {
		this.recommend = recommend;
	}

	/**
	 * image的赋值方法
	 */
	public void setImage(String image) {
		this.image = image;
	}

	/**
	 * type的赋值方法
	 */
	public void setType(Integer type) {
		this.type = type;
	}

	/**
	 * id的取值方法
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * title的取值方法
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * time的取值方法
	 */
	public String getTime() {
		return time;
	}

	/**
	 * content的取值方法
	 */
	public String getContent() {
		return content;
	}

	/**
	 * author的取值方法
	 */
	public String getAuthor() {
		return author;
	}

	/**
	 * keyword的取值方法
	 */
	public String getKeyword() {
		return keyword;
	}

	/**
	 * praise的取值方法
	 */
	public Integer getPraise() {
		return praise;
	}

	/**
	 * recommend的取值方法
	 */
	public Integer getRecommend() {
		return recommend;
	}

	/**
	 * image的取值方法
	 */
	public String getImage() {
		return image;
	}

	/**
	 * type的取值方法
	 */
	public Integer getType() {
		return type;
	}

	/**
	 * id取值方法
	 */

	@Override
	public String toString() {
		return "KnowlegeBean [id=" + id + ", title=" + title + "]";
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(getId());
		dest.writeString(getTitle());
		dest.writeString(getTime());
		dest.writeString(getContent());
		dest.writeString(getAuthor());
		dest.writeString(getKeyword());
		dest.writeInt(getPraise());
		dest.writeInt(getRecommend());
		dest.writeString(getImage());
		dest.writeInt(getType());
	}

	public static final Parcelable.Creator<KnowlegeBean> CREATOR = new Parcelable.Creator<KnowlegeBean>() {

		@Override
		public KnowlegeBean createFromParcel(Parcel source) {
			KnowlegeBean knowlegeBean = new KnowlegeBean();
			knowlegeBean.setAuthor(source.readString());
			knowlegeBean.setContent(source.readString());
			knowlegeBean.setId(source.readInt());
			knowlegeBean.setImage(source.readString());
			knowlegeBean.setKeyword(source.readString());
			knowlegeBean.setPraise(source.readInt());
			knowlegeBean.setRecommend(source.readInt());
			knowlegeBean.setTime(source.readString());
			knowlegeBean.setTitle(source.readString());
			knowlegeBean.setType(source.readInt());
			return knowlegeBean;
		}

		@Override
		public KnowlegeBean[] newArray(int size) {
			// TODO Auto-generated method stub
			return new KnowlegeBean[size];
		}
	};

}