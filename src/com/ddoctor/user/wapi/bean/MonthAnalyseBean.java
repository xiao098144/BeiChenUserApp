package com.ddoctor.user.wapi.bean;

/**
 * <p>Title: Value code</p>
 * <p>Description: data view</p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: ddoctor</p>
 * @author tian jiale
 * @version 1.0
 */


public class MonthAnalyseBean implements java.io.Serializable {
	/**
	 * 定义本类的私有变量
	 */
	private Integer test;
	private Integer high;
	private Integer low;
	private Float highest;
	private Float lowest;
	private Integer normal;
	private Integer standdard;
	private String latelyDate;
	private String latelyValue;

	/**
	 * 本类的实例化方法
	 */
	public MonthAnalyseBean() {
	}

	/**
	 * 本类的带参数的实例化方法
	 */
	public MonthAnalyseBean(Integer test, Integer high, Integer low,
			Float highest, Float lowest, Integer normal, Integer standdard,
			String latelyDate, String latelyValue) {
		this.test = test;
		this.high = high;
		this.low = low;
		this.highest = highest;
		this.lowest = lowest;
		this.normal = normal;
		this.standdard = standdard;
		this.latelyDate = latelyDate;
		this.latelyValue = latelyValue;
	}

	/**
	 * 以本类为参数传入时，实例化本类的方法
	 */
	public void copyFrom(MonthAnalyseBean vo) {
		test = vo.test;
		high = vo.high;
		low = vo.low;
		highest = vo.highest;
		lowest = vo.lowest;
		normal = vo.normal;
		standdard = vo.standdard;
		latelyDate = vo.latelyDate;
		latelyValue = vo.latelyValue;
	}

	/**
	 * 本类的赋值方法
	 */
	public void setData(MonthAnalyseBean vo) {
		this.copyFrom(vo);
	}

	/**
	 * 本类的取值方法
	 */
	public MonthAnalyseBean getData() {
		MonthAnalyseBean result = new MonthAnalyseBean();
		result.copyFrom(this);
		return result;
	}

	/**
	 * test的赋值方法
	 */
	public void setTest(Integer test) {
		this.test = test;
	}

	/**
	 * high的赋值方法
	 */
	public void setHigh(Integer high) {
		this.high = high;
	}

	/**
	 * low的赋值方法
	 */
	public void setLow(Integer low) {
		this.low = low;
	}

	/**
	 * highest的赋值方法
	 */
	public void setHighest(Float highest) {
		this.highest = highest;
	}

	/**
	 * lowest的赋值方法
	 */
	public void setLowest(Float lowest) {
		this.lowest = lowest;
	}

	/**
	 * normal的赋值方法
	 */
	public void setNormal(Integer normal) {
		this.normal = normal;
	}

	/**
	 * standdard的赋值方法
	 */
	public void setStanddard(Integer standdard) {
		this.standdard = standdard;
	}

	/**
	 * latelyDate的赋值方法
	 */
	public void setLatelyDate(String latelyDate) {
		this.latelyDate = latelyDate;
	}

	/**
	 * latelyValue的赋值方法
	 */
	public void setLatelyValue(String latelyValue) {
		this.latelyValue = latelyValue;
	}

	/**
	 * test的取值方法
	 */
	public Integer getTest() {
		return test;
	}

	/**
	 * high的取值方法
	 */
	public Integer getHigh() {
		return high;
	}

	/**
	 * low的取值方法
	 */
	public Integer getLow() {
		return low;
	}

	/**
	 * highest的取值方法
	 */
	public Float getHighest() {
		return highest;
	}

	/**
	 * lowest的取值方法
	 */
	public Float getLowest() {
		return lowest;
	}

	/**
	 * normal的取值方法
	 */
	public Integer getNormal() {
		return normal;
	}

	/**
	 * standdard的取值方法
	 */
	public Integer getStanddard() {
		return standdard;
	}

	/**
	 * latelyDate的取值方法
	 */
	public String getLatelyDate() {
		return latelyDate;
	}

	/**
	 * latelyValue的取值方法
	 */
	public String getLatelyValue() {
		return latelyValue;
	}
	/**
	 * test取值方法
	 */
}