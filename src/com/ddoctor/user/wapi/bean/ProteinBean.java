package com.ddoctor.user.wapi.bean;

/**
 * <p>Title: 糖化血红蛋白</p>
 * <p>Description: data view</p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: ddoctor</p>
 * @author tian jiale
 * @version 1.0
 */

import android.os.Parcel;
import android.os.Parcelable;

import com.ddoctor.enums.RecordLayoutType;

public class ProteinBean implements Parcelable , Comparable<ProteinBean>{
	/**
	 * 定义本类的私有变量
	 */
	private Integer id;
	private Float value;
	private String remark;
	private String time; // yyyy-MM-dd HH:mm:ss

	private String date; // yyyy-MM-dd

	private RecordLayoutType layoutType = RecordLayoutType.TYPE_VALUE;

	/**
	 * 本类的实例化方法
	 */
	public ProteinBean() {
	}

	/**
	 * 本类的带参数的实例化方法
	 */

	public ProteinBean(Integer id, Float value, String remark, String time,
			String date, RecordLayoutType layoutType) {
		super();
		this.id = id;
		this.value = value;
		this.remark = remark;
		this.time = time;
		this.date = date;
		this.layoutType = layoutType;
	}

	/**
	 * 以本类为参数传入时，实例化本类的方法
	 */
	public void copyFrom(ProteinBean vo) {
		id = vo.id;
		value = vo.value;
		remark = vo.remark;
		time = vo.time;
		date = vo.date;
		layoutType = vo.layoutType;
	}

	/**
	 * 本类的赋值方法
	 */
	public void setData(ProteinBean vo) {
		this.copyFrom(vo);
	}

	/**
	 * 本类的取值方法
	 */
	public ProteinBean getData() {
		ProteinBean result = new ProteinBean();
		result.copyFrom(this);
		return result;
	}

	/**
	 * id的赋值方法
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * value的赋值方法
	 */
	public void setValue(Float value) {
		this.value = value;
	}

	/**
	 * remark的赋值方法
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

	/**
	 * time的赋值方法
	 */
	public void setTime(String time) {
		this.time = time;
	}

	/**
	 * id的取值方法
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * value的取值方法
	 */
	public Float getValue() {
		return value;
	}

	/**
	 * remark的取值方法
	 */
	public String getRemark() {
		return remark;
	}

	/**
	 * time的取值方法
	 */
	public String getTime() {
		return time;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getDate() {
		return date;
	}

	public void setLayoutType(RecordLayoutType layoutType) {
		this.layoutType = layoutType;
	}

	public RecordLayoutType getLayoutType() {
		return layoutType;
	}

	@Override
	public String toString() {
		return "ProteinBean [id=" + id + ", value=" + value + ", remark="
				+ remark + ", time=" + time + ", date=" + date
				+ ", layoutType=" + layoutType + "]";
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeValue(this.id);
		dest.writeValue(this.value);
		dest.writeString(this.remark);
		dest.writeString(this.time);
		dest.writeString(this.date);
		dest.writeInt(this.layoutType == RecordLayoutType.TYPE_VALUE ? 1 : 2);
	}

	protected ProteinBean(Parcel in) {
		this.id = (Integer) in.readValue(Integer.class.getClassLoader());
		this.value = (Float) in.readValue(Float.class.getClassLoader());
		this.remark = in.readString();
		this.time = in.readString();
		this.date = in.readString();
		int tmpLayoutType = in.readInt();
		this.layoutType = tmpLayoutType == 1 ? RecordLayoutType.TYPE_VALUE
				: RecordLayoutType.TYPE_CATEGORY;
	}

	public static final Parcelable.Creator<ProteinBean> CREATOR = new Parcelable.Creator<ProteinBean>() {
		public ProteinBean createFromParcel(Parcel source) {
			return new ProteinBean(source);
		}

		public ProteinBean[] newArray(int size) {
			return new ProteinBean[size];
		}
	};

	@Override
	public int compareTo(ProteinBean another) {
		int compare;
		compare = getTime().compareTo(another.getTime());
		return 0 -compare;
	}
}