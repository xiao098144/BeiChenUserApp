package com.ddoctor.user.wapi.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * <p>
 * Title: Value code
 * </p>
 * <p>
 * Description: data view
 * </p>
 * <p>
 * Copyright: Copyright (c) 2007
 * </p>
 * <p>
 * Company: ddoctor
 * </p>
 * 
 * @author tian jiale
 * @version 1.0
 */

public class DoctorBean implements java.io.Serializable, Parcelable {
	/**
    *
    */
	private static final long serialVersionUID = -7317548753365051959L;
	/**
	 * 定义本类的私有变量
	 */
	private Integer id;
	private String mobile;
	private String image;
	private String name;
	private Integer age;
	private String birthday;
	private String sex;
	private String hospital;
	private String department;
	private String level;
	private String desc;
	private Integer reply;
	private Integer relation;
	private Integer provinceId;
	private Integer cityId;
	private Integer districtId;
	private Integer hospitalId;
	private Integer departmentId;
	private Integer levelId;
	private String idcard;
	private String depPhone;
	private String certificate;
	private String permits;
	private String skill;
	private String experience;
	private String uuid;
	private Integer channel;
	private Integer totalscore;
	private Float averscore;
	private Integer isscore;
	private Integer countPatient;
	private Integer score;
	private Integer isverify;

	/**
	 * 本类的实例化方法
	 */
	public DoctorBean() {
	}

	/**
	 * 本类的带参数的实例化方法
	 */
	public DoctorBean(Integer id, String mobile, String image, String name,
			Integer age, String birthday, String sex, String hospital,
			String department, String level, String desc, Integer reply,
			Integer relation, Integer provinceId, Integer cityId,
			Integer districtId, Integer hospitalId, Integer departmentId,
			Integer levelId, String idcard, String depPhone,
			String certificate, String permits, String skill,
			String experience, String uuid, Integer channel,
			Integer totalscore, Float averscore, Integer isscore,
			Integer countPatient, Integer score, Integer isverify) {
		this.id = id;
		this.mobile = mobile;
		this.image = image;
		this.name = name;
		this.age = age;
		this.birthday = birthday;
		this.sex = sex;
		this.hospital = hospital;
		this.department = department;
		this.level = level;
		this.desc = desc;
		this.reply = reply;
		this.relation = relation;
		this.provinceId = provinceId;
		this.cityId = cityId;
		this.districtId = districtId;
		this.hospitalId = hospitalId;
		this.departmentId = departmentId;
		this.levelId = levelId;
		this.idcard = idcard;
		this.depPhone = depPhone;
		this.certificate = certificate;
		this.permits = permits;
		this.skill = skill;
		this.experience = experience;
		this.uuid = uuid;
		this.channel = channel;
		this.totalscore = totalscore;
		this.averscore = averscore;
		this.isscore = isscore;
		this.countPatient = countPatient;
		this.score = score;
		this.isverify = isverify;
	}

	/**
	 * 以本类为参数传入时，实例化本类的方法
	 */
	public void copyFrom(DoctorBean vo) {
		id = vo.id;
		mobile = vo.mobile;
		image = vo.image;
		name = vo.name;
		age = vo.age;
		birthday = vo.birthday;
		sex = vo.sex;
		hospital = vo.hospital;
		department = vo.department;
		level = vo.level;
		desc = vo.desc;
		reply = vo.reply;
		relation = vo.relation;
		provinceId = vo.provinceId;
		cityId = vo.cityId;
		districtId = vo.districtId;
		hospitalId = vo.hospitalId;
		departmentId = vo.departmentId;
		levelId = vo.levelId;
		idcard = vo.idcard;
		depPhone = vo.depPhone;
		certificate = vo.certificate;
		permits = vo.permits;
		skill = vo.skill;
		experience = vo.experience;
		uuid = vo.uuid;
		channel = vo.channel;
		totalscore = vo.totalscore;
		averscore = vo.averscore;
		isscore = vo.isscore;
		countPatient = vo.countPatient;
		score = vo.score;
		isverify = vo.isverify;
	}

	/**
	 * 本类的赋值方法
	 */
	public void setData(DoctorBean vo) {
		this.copyFrom(vo);
	}

	/**
	 * 本类的取值方法
	 */
	public DoctorBean getData() {
		DoctorBean result = new DoctorBean();
		result.copyFrom(this);
		return result;
	}

	/**
	 * id的赋值方法
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * image的赋值方法
	 */
	public void setImage(String image) {
		this.image = image;
	}

	/**
	 * name的赋值方法
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * age的赋值方法
	 */
	public void setAge(Integer age) {
		this.age = age;
	}

	/**
	 * sex的赋值方法
	 */
	public void setSex(String sex) {
		this.sex = sex;
	}

	/**
	 * hospital的赋值方法
	 */
	public void setHospital(String hospital) {
		this.hospital = hospital;
	}

	/**
	 * department的赋值方法
	 */
	public void setDepartment(String department) {
		this.department = department;
	}

	/**
	 * level的赋值方法
	 */
	public void setLevel(String level) {
		this.level = level;
	}

	/**
	 * desc的赋值方法
	 */
	public void setDesc(String desc) {
		this.desc = desc;
	}

	/**
	 * reply的赋值方法
	 */
	public void setReply(Integer reply) {
		this.reply = reply;
	}

	/**
	 * relation的赋值方法
	 */
	public void setRelation(Integer relation) {
		this.relation = relation;
	}

	/**
	 * provinceId的赋值方法
	 */
	public void setProvinceId(Integer provinceId) {
		this.provinceId = provinceId;
	}

	/**
	 * cityId的赋值方法
	 */
	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}

	/**
	 * districtId的赋值方法
	 */
	public void setDistrictId(Integer districtId) {
		this.districtId = districtId;
	}

	/**
	 * hospitalId的赋值方法
	 */
	public void setHospitalId(Integer hospitalId) {
		this.hospitalId = hospitalId;
	}

	/**
	 * departmentId的赋值方法
	 */
	public void setDepartmentId(Integer departmentId) {
		this.departmentId = departmentId;
	}

	/**
	 * levelId的赋值方法
	 */
	public void setLevelId(Integer levelId) {
		this.levelId = levelId;
	}

	/**
	 * idcard的赋值方法
	 */
	public void setIdcard(String idcard) {
		this.idcard = idcard;
	}

	/**
	 * depPhone的赋值方法
	 */
	public void setDepPhone(String depPhone) {
		this.depPhone = depPhone;
	}

	/**
	 * certificate的赋值方法
	 */
	public void setCertificate(String certificate) {
		this.certificate = certificate;
	}

	/**
	 * permits的赋值方法
	 */
	public void setPermits(String permits) {
		this.permits = permits;
	}

	/**
	 * skill的赋值方法
	 */
	public void setSkill(String skill) {
		this.skill = skill;
	}

	/**
	 * experience的赋值方法
	 */
	public void setExperience(String experience) {
		this.experience = experience;
	}

	/**
	 * uuid的赋值方法
	 */
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	/**
	 * channel的赋值方法
	 */
	public void setChannel(Integer channel) {
		this.channel = channel;
	}

	/**
	 * totalscore的赋值方法
	 */
	public void setTotalscore(Integer totalscore) {
		this.totalscore = totalscore;
	}

	/**
	 * averscore的赋值方法
	 */
	public void setAverscore(Float averscore) {
		this.averscore = averscore;
	}

	/**
	 * isscore的赋值方法
	 */
	public void setIsscore(Integer isscore) {
		this.isscore = isscore;
	}

	/**
	 * id的取值方法
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * image的取值方法
	 */
	public String getImage() {
		return image;
	}

	/**
	 * name的取值方法
	 */
	public String getName() {
		return name;
	}

	/**
	 * age的取值方法
	 */
	public Integer getAge() {
		return age;
	}

	/**
	 * sex的取值方法
	 */
	public String getSex() {
		return sex;
	}

	/**
	 * hospital的取值方法
	 */
	public String getHospital() {
		return hospital;
	}

	/**
	 * department的取值方法
	 */
	public String getDepartment() {
		return department;
	}

	/**
	 * level的取值方法
	 */
	public String getLevel() {
		return level;
	}

	/**
	 * desc的取值方法
	 */
	public String getDesc() {
		return desc;
	}

	/**
	 * reply的取值方法
	 */
	public Integer getReply() {
		return reply;
	}

	/**
	 * relation的取值方法
	 */
	public Integer getRelation() {
		return relation;
	}

	/**
	 * provinceId的取值方法
	 */
	public Integer getProvinceId() {
		return provinceId;
	}

	/**
	 * cityId的取值方法
	 */
	public Integer getCityId() {
		return cityId;
	}

	/**
	 * districtId的取值方法
	 */
	public Integer getDistrictId() {
		return districtId;
	}

	/**
	 * hospitalId的取值方法
	 */
	public Integer getHospitalId() {
		return hospitalId;
	}

	/**
	 * departmentId的取值方法
	 */
	public Integer getDepartmentId() {
		return departmentId;
	}

	/**
	 * levelId的取值方法
	 */
	public Integer getLevelId() {
		return levelId;
	}

	/**
	 * idcard的取值方法
	 */
	public String getIdcard() {
		return idcard;
	}

	/**
	 * depPhone的取值方法
	 */
	public String getDepPhone() {
		return depPhone;
	}

	/**
	 * certificate的取值方法
	 */
	public String getCertificate() {
		return certificate;
	}

	/**
	 * permits的取值方法
	 */
	public String getPermits() {
		return permits;
	}

	/**
	 * skill的取值方法
	 */
	public String getSkill() {
		return skill;
	}

	/**
	 * experience的取值方法
	 */
	public String getExperience() {
		return experience;
	}

	/**
	 * uuid的取值方法
	 */
	public String getUuid() {
		return uuid;
	}

	/**
	 * channel的取值方法
	 */
	public Integer getChannel() {
		return channel;
	}

	/**
	 * totalscore的取值方法
	 */
	public Integer getTotalscore() {
		return totalscore;
	}

	/**
	 * averscore的取值方法
	 */
	public Float getAverscore() {
		return averscore;
	}

	/**
	 * isscore的取值方法
	 */
	public Integer getIsscore() {
		return isscore;
	}

	/**
	 * id取值方法
	 */
	public Integer getCountPatient() {
		return countPatient;
	}

	public void setCountPatient(Integer countPatient) {
		this.countPatient = countPatient;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public Integer getIsverify() {
		return isverify;
	}

	public void setIsverify(Integer isverify) {
		this.isverify = isverify;
	}

	@Override
	public String toString() {
		return "DoctorBean [id=" + id + ", image=" + image + ", name=" + name
				+ ", isverify=" + isverify + "]";
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeValue(this.id);
		dest.writeString(this.mobile);
		dest.writeString(this.image);
		dest.writeString(this.name);
		dest.writeValue(this.age);
		dest.writeString(this.birthday);
		dest.writeString(this.sex);
		dest.writeString(this.hospital);
		dest.writeString(this.department);
		dest.writeString(this.level);
		dest.writeString(this.desc);
		dest.writeValue(this.reply);
		dest.writeValue(this.relation);
		dest.writeValue(this.provinceId);
		dest.writeValue(this.cityId);
		dest.writeValue(this.districtId);
		dest.writeValue(this.hospitalId);
		dest.writeValue(this.departmentId);
		dest.writeValue(this.levelId);
		dest.writeString(this.idcard);
		dest.writeString(this.depPhone);
		dest.writeString(this.certificate);
		dest.writeString(this.permits);
		dest.writeString(this.skill);
		dest.writeString(this.experience);
		dest.writeString(this.uuid);
		dest.writeValue(this.channel);
		dest.writeValue(this.totalscore);
		dest.writeValue(this.averscore);
		dest.writeValue(this.isscore);
		dest.writeValue(this.countPatient);
		dest.writeValue(this.score);
		dest.writeValue(this.isverify);
	}

	protected DoctorBean(Parcel in) {
		this.id = (Integer) in.readValue(Integer.class.getClassLoader());
		this.mobile = in.readString();
		this.image = in.readString();
		this.name = in.readString();
		this.age = (Integer) in.readValue(Integer.class.getClassLoader());
		this.birthday = in.readString();
		this.sex = in.readString();
		this.hospital = in.readString();
		this.department = in.readString();
		this.level = in.readString();
		this.desc = in.readString();
		this.reply = (Integer) in.readValue(Integer.class.getClassLoader());
		this.relation = (Integer) in.readValue(Integer.class.getClassLoader());
		this.provinceId = (Integer) in
				.readValue(Integer.class.getClassLoader());
		this.cityId = (Integer) in.readValue(Integer.class.getClassLoader());
		this.districtId = (Integer) in
				.readValue(Integer.class.getClassLoader());
		this.hospitalId = (Integer) in
				.readValue(Integer.class.getClassLoader());
		this.departmentId = (Integer) in.readValue(Integer.class
				.getClassLoader());
		this.levelId = (Integer) in.readValue(Integer.class.getClassLoader());
		this.idcard = in.readString();
		this.depPhone = in.readString();
		this.certificate = in.readString();
		this.permits = in.readString();
		this.skill = in.readString();
		this.experience = in.readString();
		this.uuid = in.readString();
		this.channel = (Integer) in.readValue(Integer.class.getClassLoader());
		this.totalscore = (Integer) in
				.readValue(Integer.class.getClassLoader());
		this.averscore = (Float) in.readValue(Float.class.getClassLoader());
		this.isscore = (Integer) in.readValue(Integer.class.getClassLoader());
		this.countPatient = (Integer) in.readValue(Integer.class
				.getClassLoader());
		this.score = (Integer) in.readValue(Integer.class.getClassLoader());
		this.isverify = (Integer) in.readValue(Integer.class.getClassLoader());
	}

	public static final Parcelable.Creator<DoctorBean> CREATOR = new Parcelable.Creator<DoctorBean>() {
		public DoctorBean createFromParcel(Parcel source) {
			return new DoctorBean(source);
		}

		public DoctorBean[] newArray(int size) {
			return new DoctorBean[size];
		}
	};
}