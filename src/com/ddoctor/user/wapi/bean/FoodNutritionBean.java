package com.ddoctor.user.wapi.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * <p>
 * Title: Value code
 * </p>
 * <p>
 * Description: data view
 * </p>
 * <p>
 * Copyright: Copyright (c) 2007
 * </p>
 * <p>
 * Company: ddoctor
 * </p>
 * 
 * @author tian jiale
 * @version 1.0
 */

public class FoodNutritionBean implements Parcelable {
	/**
	 * 定义本类的私有变量
	 */
	private Integer id;
	private String foodName;
	private Integer foodWeight;
	private Integer foodProtein;
	private Integer foodFat;
	private Integer foodCarbohydrate;
	private Integer foodEnergy;
	private Integer foodType;

	/**
	 * 本类的实例化方法
	 */
	public FoodNutritionBean() {
	}

	/**
	 * 本类的带参数的实例化方法
	 */
	public FoodNutritionBean(Integer id, String foodName, Integer foodWeight,
			Integer foodProtein, Integer foodFat, Integer foodCarbohydrate,
			Integer foodEnergy, Integer foodType) {
		this.id = id;
		this.foodName = foodName;
		this.foodWeight = foodWeight;
		this.foodProtein = foodProtein;
		this.foodFat = foodFat;
		this.foodCarbohydrate = foodCarbohydrate;
		this.foodEnergy = foodEnergy;
		this.foodType = foodType;
	}

	/**
	 * 以本类为参数传入时，实例化本类的方法
	 */
	public void copyFrom(FoodNutritionBean vo) {
		id = vo.id;
		foodName = vo.foodName;
		foodWeight = vo.foodWeight;
		foodProtein = vo.foodProtein;
		foodFat = vo.foodFat;
		foodCarbohydrate = vo.foodCarbohydrate;
		foodEnergy = vo.foodEnergy;
		foodType = vo.foodType;
	}

	/**
	 * 本类的赋值方法
	 */
	public void setData(FoodNutritionBean vo) {
		this.copyFrom(vo);
	}

	/**
	 * 本类的取值方法
	 */
	public FoodNutritionBean getData() {
		FoodNutritionBean result = new FoodNutritionBean();
		result.copyFrom(this);
		return result;
	}

	/**
	 * id的赋值方法
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	public String getFoodName() {
		return foodName;
	}

	public void setFoodName(String foodName) {
		this.foodName = foodName;
	}

	public Integer getFoodWeight() {
		return foodWeight;
	}

	public void setFoodWeight(Integer foodWeight) {
		this.foodWeight = foodWeight;
	}

	public Integer getFoodProtein() {
		return foodProtein;
	}

	public void setFoodProtein(Integer foodProtein) {
		this.foodProtein = foodProtein;
	}

	public Integer getFoodFat() {
		return foodFat;
	}

	public void setFoodFat(Integer foodFat) {
		this.foodFat = foodFat;
	}

	public Integer getFoodCarbohydrate() {
		return foodCarbohydrate;
	}

	public void setFoodCarbohydrate(Integer foodCarbohydrate) {
		this.foodCarbohydrate = foodCarbohydrate;
	}

	public Integer getFoodEnergy() {
		return foodEnergy;
	}

	public void setFoodEnergy(Integer foodEnergy) {
		this.foodEnergy = foodEnergy;
	}

	public Integer getFoodType() {
		return foodType;
	}

	public void setFoodType(Integer foodType) {
		this.foodType = foodType;
	}

	public Integer getId() {
		return id;
	}

	@Override
	public String toString() {
		return "FoodNutritionBean [id=" + id + ", foodName=" + foodName
				+ ", foodType=" + foodType + "]";
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeValue(this.id);
		dest.writeString(this.foodName);
		dest.writeValue(this.foodWeight);
		dest.writeValue(this.foodProtein);
		dest.writeValue(this.foodFat);
		dest.writeValue(this.foodCarbohydrate);
		dest.writeValue(this.foodEnergy);
		dest.writeValue(this.foodType);
	}

	protected FoodNutritionBean(Parcel in) {
		this.id = (Integer) in.readValue(Integer.class.getClassLoader());
		this.foodName = in.readString();
		this.foodWeight = (Integer) in
				.readValue(Integer.class.getClassLoader());
		this.foodProtein = (Integer) in.readValue(Integer.class
				.getClassLoader());
		this.foodFat = (Integer) in.readValue(Integer.class.getClassLoader());
		this.foodCarbohydrate = (Integer) in.readValue(Integer.class
				.getClassLoader());
		this.foodEnergy = (Integer) in
				.readValue(Integer.class.getClassLoader());
		this.foodType = (Integer) in.readValue(Integer.class.getClassLoader());
	}

	public static final Parcelable.Creator<FoodNutritionBean> CREATOR = new Parcelable.Creator<FoodNutritionBean>() {
		public FoodNutritionBean createFromParcel(Parcel source) {
			return new FoodNutritionBean(source);
		}

		public FoodNutritionBean[] newArray(int size) {
			return new FoodNutritionBean[size];
		}
	};
}