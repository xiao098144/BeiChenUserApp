package com.ddoctor.user.wapi.bean;

/**
 * <p>Title: Value code</p>
 * <p>Description: data view</p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: ddoctor</p>
 * @author tian jiale
 * @version 1.0
 */

import java.util.List;

public class DoctorIndexBean implements java.io.Serializable {
	/**
	 * 定义本类的私有变量
	 */
	private Integer isunread;
	private Integer isadd;
	private List<QuesionBean> questions;

	/**
	 * 本类的实例化方法
	 */
	public DoctorIndexBean() {
	}

	/**
	 * 本类的带参数的实例化方法
	 */
	public DoctorIndexBean(Integer isunread, Integer isadd,
			List<QuesionBean> questions) {
		this.isunread = isunread;
		this.isadd = isadd;
		this.questions = questions;
	}

	/**
	 * 以本类为参数传入时，实例化本类的方法
	 */
	public void copyFrom(DoctorIndexBean vo) {
		isunread = vo.isunread;
		isadd = vo.isadd;
		questions = vo.questions;
	}

	/**
	 * 本类的赋值方法
	 */
	public void setData(DoctorIndexBean vo) {
		this.copyFrom(vo);
	}

	/**
	 * 本类的取值方法
	 */
	public DoctorIndexBean getData() {
		DoctorIndexBean result = new DoctorIndexBean();
		result.copyFrom(this);
		return result;
	}

	/**
	 * isunread的赋值方法
	 */
	public void setIsunread(Integer isunread) {
		this.isunread = isunread;
	}

	/**
	 * isadd的赋值方法
	 */
	public void setIsadd(Integer isadd) {
		this.isadd = isadd;
	}

	/**
	 * questions的赋值方法
	 */
	public void setQuestions(List<QuesionBean> questions) {
		this.questions = questions;
	}

	/**
	 * isunread的取值方法
	 */
	public Integer getIsunread() {
		return isunread;
	}

	/**
	 * isadd的取值方法
	 */
	public Integer getIsadd() {
		return isadd;
	}

	/**
	 * questions的取值方法
	 */
	public List<QuesionBean> getQuestions() {
		return questions;
	}
	/**
	 * isunread取值方法
	 */
}