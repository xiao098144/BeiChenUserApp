package com.ddoctor.user.wapi.bean;

/**
 * <p>Title: Value code</p>
 * <p>Description: data view</p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: ddoctor</p>
 * @author tian jiale
 * @version 1.0
 */


public class LogBean implements java.io.Serializable {
	/**
	 * 定义本类的私有变量
	 */
	private Integer id;
	private String time;
	private String content1;
	private String content2;
	private String content3;
	private String remark;

	/**
	 * 本类的实例化方法
	 */
	public LogBean() {
	}

	/**
	 * 本类的带参数的实例化方法
	 */
	public LogBean(Integer id, String time, String content1, String content2,
			String content3, String remark) {
		this.id = id;
		this.time = time;
		this.content1 = content1;
		this.content2 = content2;
		this.content3 = content3;
		this.remark = remark;
	}

	/**
	 * 以本类为参数传入时，实例化本类的方法
	 */
	public void copyFrom(LogBean vo) {
		id = vo.id;
		time = vo.time;
		content1 = vo.content1;
		content2 = vo.content2;
		content3 = vo.content3;
		remark = vo.remark;
	}

	/**
	 * 本类的赋值方法
	 */
	public void setData(LogBean vo) {
		this.copyFrom(vo);
	}

	/**
	 * 本类的取值方法
	 */
	public LogBean getData() {
		LogBean result = new LogBean();
		result.copyFrom(this);
		return result;
	}

	/**
	 * id的赋值方法
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * time的赋值方法
	 */
	public void setTime(String time) {
		this.time = time;
	}

	/**
	 * content1的赋值方法
	 */
	public void setContent1(String content1) {
		this.content1 = content1;
	}

	/**
	 * content2的赋值方法
	 */
	public void setContent2(String content2) {
		this.content2 = content2;
	}

	/**
	 * content3的赋值方法
	 */
	public void setContent3(String content3) {
		this.content3 = content3;
	}

	/**
	 * remark的赋值方法
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

	/**
	 * id的取值方法
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * time的取值方法
	 */
	public String getTime() {
		return time;
	}

	/**
	 * content1的取值方法
	 */
	public String getContent1() {
		return content1;
	}

	/**
	 * content2的取值方法
	 */
	public String getContent2() {
		return content2;
	}

	/**
	 * content3的取值方法
	 */
	public String getContent3() {
		return content3;
	}

	/**
	 * remark的取值方法
	 */
	public String getRemark() {
		return remark;
	}
	/**
	 * id取值方法
	 */
}