package com.ddoctor.user.wapi.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * <p>Title:商品</p>
 * <p>Description: data view</p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: ddoctor</p>
 * @author tian jiale
 * @version 1.0
 */


public class ProductBean implements Parcelable {
	/**
	 * 定义本类的私有变量
	 */
	private Integer id;
	private String name;
	private Float price;
	private Float discount;
	private String info;
	private String moreinfo;
	private Integer quantity;
	private String brand;
	private String model;
	private String image;
	private String image2;
	private String image3;
	private String image4;
	private String affinfo;
	private Integer count;

	/**
	 * 本类的实例化方法
	 */
	public ProductBean() {
	}

	/**
	 * 本类的带参数的实例化方法
	 */
	public ProductBean(Integer id, String name, Float price, Float discount,
			String info, String moreinfo, Integer quantity, String brand,
			String model, String image, String image2, String image3,
			String image4, String affinfo, Integer count) {
		this.id = id;
		this.name = name;
		this.price = price;
		this.discount = discount;
		this.info = info;
		this.moreinfo = moreinfo;
		this.quantity = quantity;
		this.brand = brand;
		this.model = model;
		this.image = image;
		this.image2 = image2;
		this.image3 = image3;
		this.image4 = image4;
		this.affinfo = affinfo;
		this.count = count;
	}

	/**
	 * 以本类为参数传入时，实例化本类的方法
	 */
	public void copyFrom(ProductBean vo) {
		id = vo.id;
		name = vo.name;
		price = vo.price;
		discount = vo.discount;
		info = vo.info;
		moreinfo = vo.moreinfo;
		quantity = vo.quantity;
		brand = vo.brand;
		model = vo.model;
		image = vo.image;
		image2 = vo.image2;
		image3 = vo.image3;
		image4 = vo.image4;
		affinfo = vo.affinfo;
		count = vo.count;
	}

	/**
	 * 本类的赋值方法
	 */
	public void setData(ProductBean vo) {
		this.copyFrom(vo);
	}

	/**
	 * 本类的取值方法
	 */
	public ProductBean getData() {
		ProductBean result = new ProductBean();
		result.copyFrom(this);
		return result;
	}

	/**
	 * id的赋值方法
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * name的赋值方法
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * price的赋值方法
	 */
	public void setPrice(Float price) {
		this.price = price;
	}

	/**
	 * discount的赋值方法
	 */
	public void setDiscount(Float discount) {
		this.discount = discount;
	}

	/**
	 * info的赋值方法
	 */
	public void setInfo(String info) {
		this.info = info;
	}

	/**
	 * moreinfo的赋值方法
	 */
	public void setMoreinfo(String moreinfo) {
		this.moreinfo = moreinfo;
	}

	/**
	 * quantity的赋值方法
	 */
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	/**
	 * brand的赋值方法
	 */
	public void setBrand(String brand) {
		this.brand = brand;
	}

	/**
	 * model的赋值方法
	 */
	public void setModel(String model) {
		this.model = model;
	}

	/**
	 * image的赋值方法
	 */
	public void setImage(String image) {
		this.image = image;
	}

	/**
	 * image2的赋值方法
	 */
	public void setImage2(String image2) {
		this.image2 = image2;
	}

	/**
	 * image3的赋值方法
	 */
	public void setImage3(String image3) {
		this.image3 = image3;
	}

	/**
	 * image4的赋值方法
	 */
	public void setImage4(String image4) {
		this.image4 = image4;
	}

	/**
	 * affinfo的赋值方法
	 */
	public void setAffinfo(String affinfo) {
		this.affinfo = affinfo;
	}

	/**
	 * count的赋值方法
	 */
	public void setCount(Integer count) {
		this.count = count;
	}

	/**
	 * id的取值方法
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * name的取值方法
	 */
	public String getName() {
		return name;
	}

	/**
	 * price的取值方法
	 */
	public Float getPrice() {
		return price;
	}

	/**
	 * discount的取值方法
	 */
	public Float getDiscount() {
		return discount;
	}

	/**
	 * info的取值方法
	 */
	public String getInfo() {
		return info;
	}

	/**
	 * moreinfo的取值方法
	 */
	public String getMoreinfo() {
		return moreinfo;
	}

	/**
	 * quantity的取值方法
	 */
	public Integer getQuantity() {
		return quantity;
	}

	/**
	 * brand的取值方法
	 */
	public String getBrand() {
		return brand;
	}

	/**
	 * model的取值方法
	 */
	public String getModel() {
		return model;
	}

	/**
	 * image的取值方法
	 */
	public String getImage() {
		return image;
	}

	/**
	 * image2的取值方法
	 */
	public String getImage2() {
		return image2;
	}

	/**
	 * image3的取值方法
	 */
	public String getImage3() {
		return image3;
	}

	/**
	 * image4的取值方法
	 */
	public String getImage4() {
		return image4;
	}

	/**
	 * affinfo的取值方法
	 */
	public String getAffinfo() {
		return affinfo;
	}

	/**
	 * count的取值方法
	 */
	public Integer getCount() {
		return count;
	}
	
	
	
	@Override
	public String toString() {
		return "ProductBean [id=" + id + ", name=" + name + ", price=" + price
				+ ", discount=" + discount + ", image=" + image + ", affinfo="
				+ affinfo + "]";
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeValue(this.id);
		dest.writeString(this.name);
		dest.writeValue(this.price);
		dest.writeValue(this.discount);
		dest.writeString(this.info);
		dest.writeString(this.moreinfo);
		dest.writeValue(this.quantity);
		dest.writeString(this.brand);
		dest.writeString(this.model);
		dest.writeString(this.image);
		dest.writeString(this.image2);
		dest.writeString(this.image3);
		dest.writeString(this.image4);
		dest.writeString(this.affinfo);
		dest.writeValue(this.count);
	}

	protected ProductBean(Parcel in) {
		this.id = (Integer) in.readValue(Integer.class.getClassLoader());
		this.name = in.readString();
		this.price = (Float) in.readValue(Float.class.getClassLoader());
		this.discount = (Float) in.readValue(Float.class.getClassLoader());
		this.info = in.readString();
		this.moreinfo = in.readString();
		this.quantity = (Integer) in.readValue(Integer.class.getClassLoader());
		this.brand = in.readString();
		this.model = in.readString();
		this.image = in.readString();
		this.image2 = in.readString();
		this.image3 = in.readString();
		this.image4 = in.readString();
		this.affinfo = in.readString();
		this.count = (Integer) in.readValue(Integer.class.getClassLoader());
	}

	public static final Parcelable.Creator<ProductBean> CREATOR = new Parcelable.Creator<ProductBean>() {
		public ProductBean createFromParcel(Parcel source) {
			return new ProductBean(source);
		}

		public ProductBean[] newArray(int size) {
			return new ProductBean[size];
		}
	};
}