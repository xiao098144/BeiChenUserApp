package com.ddoctor.user.wapi.bean;

/**
 * <p>Title: Value code</p>
 * <p>Description: data view</p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: ddoctor</p>
 * @author tian jiale
 * @version 1.0
 */


public class ExpressBean implements java.io.Serializable {
	/**
	 * 定义本类的私有变量
	 */
	private Integer id;
	private String name;
	private String expressNO;

	/**
	 * 本类的实例化方法
	 */
	public ExpressBean() {
	}

	/**
	 * 本类的带参数的实例化方法
	 */
	public ExpressBean(Integer id, String name, String expressNO) {
		this.id = id;
		this.name = name;
		this.expressNO = expressNO;
	}

	/**
	 * 以本类为参数传入时，实例化本类的方法
	 */
	public void copyFrom(ExpressBean vo) {
		id = vo.id;
		name = vo.name;
		expressNO = vo.expressNO;
	}

	/**
	 * 本类的赋值方法
	 */
	public void setData(ExpressBean vo) {
		this.copyFrom(vo);
	}

	/**
	 * 本类的取值方法
	 */
	public ExpressBean getData() {
		ExpressBean result = new ExpressBean();
		result.copyFrom(this);
		return result;
	}

	/**
	 * id的赋值方法
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * name的赋值方法
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * expressNO的赋值方法
	 */
	public void setExpressNO(String expressNO) {
		this.expressNO = expressNO;
	}

	/**
	 * id的取值方法
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * name的取值方法
	 */
	public String getName() {
		return name;
	}

	/**
	 * expressNO的取值方法
	 */
	public String getExpressNO() {
		return expressNO;
	}
	/**
	 * id取值方法
	 */
}