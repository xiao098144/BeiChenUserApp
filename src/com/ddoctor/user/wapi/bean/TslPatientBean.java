package com.ddoctor.user.wapi.bean;

/**
 * <p>Title: Value code</p>
 * <p>Description: data view</p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: ddoctor</p>
 * @author tian jiale
 * @version 1.0
 */

public class TslPatientBean implements java.io.Serializable {
/**
	 * 
	 */
	private static final long serialVersionUID = 3244537064113996195L;
/**
*定义本类的私有变量
*/
   private Integer id;
   private Integer tysPatientId ;
   private String tslUserId;
   private String tslName;
   private String tslPassword;
   private String tslNickName;
   private String tslShenfenzhengnum;
   private String tslShebaonum;
   private String tslPhone1;
   private String tslPhonetype1;
   private String tslPhone2;
   private String tslPhonetype2;
   private String tslSex;
   private String tslEmail;
   private String tslAddressarea;
   private String tslAddressdetail;
   private String tslPhotoshebao;
   private String tslPhotoshebaoPath;
   private String tslPhotoshenfenzheng;
   private String tslPhotoshenfenzhengPath;
   private String tslPhotoshenfenzhengf;
   private String tslPhotoshenfenzhengfPath;
   private String tslPhotomente;
   private String tslPhotomentePath;
   private String tslPhotohead;
   private String tslPhotoheadPath;
   private Integer flagstate;
   private String flagmsg;
   private java.util.Date createTime;
   private Integer status;
   private String yongYaoShi;
   private String allergy;
   private String bingZhong;
   private String time;
   private String prescriptionImage;
/**
*本类的实例化方法
*/
   public TslPatientBean() {
   }
/**
*本类的带参数的实例化方法
*/
   public TslPatientBean(Integer id, Integer tysPatientId,String tslUserId,String tslName,String tslPassword,String tslNickName,String tslShenfenzhengnum,String tslShebaonum,String tslPhone1,String tslPhonetype1,String tslPhone2,String tslPhonetype2,String tslSex,String tslEmail,String tslAddressarea,String tslAddressdetail,String tslPhotoshebao,String tslPhotoshebaoPath,String tslPhotoshenfenzheng,String tslPhotoshenfenzhengPath,String tslPhotoshenfenzhengf,String tslPhotoshenfenzhengfPath,String tslPhotomente,String tslPhotomentePath,String tslPhotohead,String tslPhotoheadPath,Integer flagstate,String flagmsg,java.util.Date createTime,Integer status,String yongYaoShi, String allergy, String bingZhong, String time,
		   String prescriptionImage) {
      this.id = id;
      this.tysPatientId = tysPatientId;
      this.tslUserId = tslUserId;
      this.tslName = tslName;
      this.tslPassword = tslPassword;
      this.tslNickName = tslNickName;
      this.tslShenfenzhengnum = tslShenfenzhengnum;
      this.tslShebaonum = tslShebaonum;
      this.tslPhone1 = tslPhone1;
      this.tslPhonetype1 = tslPhonetype1;
      this.tslPhone2 = tslPhone2;
      this.tslPhonetype2 = tslPhonetype2;
      this.tslSex = tslSex;
      this.tslEmail = tslEmail;
      this.tslAddressarea = tslAddressarea;
      this.tslAddressdetail = tslAddressdetail;
      this.tslPhotoshebao = tslPhotoshebao;
      this.tslPhotoshebaoPath = tslPhotoshebaoPath;
      this.tslPhotoshenfenzheng = tslPhotoshenfenzheng;
      this.tslPhotoshenfenzhengPath = tslPhotoshenfenzhengPath;
      this.tslPhotoshenfenzhengf = tslPhotoshenfenzhengf;
      this.tslPhotoshenfenzhengfPath = tslPhotoshenfenzhengfPath;
      this.tslPhotomente = tslPhotomente;
      this.tslPhotomentePath = tslPhotomentePath;
      this.tslPhotohead = tslPhotohead;
      this.tslPhotoheadPath = tslPhotoheadPath;
      this.flagstate = flagstate;
      this.flagmsg = flagmsg;
      this.createTime = createTime;
      this.status = status;
      this.yongYaoShi = yongYaoShi;
      this.allergy = allergy;
      this.bingZhong = bingZhong;
      this.time = time;
      this.prescriptionImage = prescriptionImage;
   }
/**
*以本类为参数传入时，实例化本类的方法
*/
   public void copyFrom(TslPatientBean vo) {
      id = vo.id;
      tysPatientId = vo.tysPatientId;
      tslUserId = vo.tslUserId;
      tslName = vo.tslName;
      tslPassword = vo.tslPassword;
      tslNickName = vo.tslNickName;
      tslShenfenzhengnum = vo.tslShenfenzhengnum;
      tslShebaonum = vo.tslShebaonum;
      tslPhone1 = vo.tslPhone1;
      tslPhonetype1 = vo.tslPhonetype1;
      tslPhone2 = vo.tslPhone2;
      tslPhonetype2 = vo.tslPhonetype2;
      tslSex = vo.tslSex;
      tslEmail = vo.tslEmail;
      tslAddressarea = vo.tslAddressarea;
      tslAddressdetail = vo.tslAddressdetail;
      tslPhotoshebao = vo.tslPhotoshebao;
      tslPhotoshebaoPath = vo.tslPhotoshebaoPath;
      tslPhotoshenfenzheng = vo.tslPhotoshenfenzheng;
      tslPhotoshenfenzhengPath = vo.tslPhotoshenfenzhengPath;
      tslPhotoshenfenzhengf = vo.tslPhotoshenfenzhengf;
      tslPhotoshenfenzhengfPath = vo.tslPhotoshenfenzhengfPath;
      tslPhotomente = vo.tslPhotomente;
      tslPhotomentePath = vo.tslPhotomentePath;
      tslPhotohead = vo.tslPhotohead;
      tslPhotoheadPath = vo.tslPhotoheadPath;
      flagstate = vo.flagstate;
      flagmsg = vo.flagmsg;
      createTime = vo.createTime;
      status = vo.status;
      yongYaoShi = vo.yongYaoShi;
      allergy = vo.allergy;
      bingZhong = vo.bingZhong;
      time = vo.time;
      prescriptionImage = vo.prescriptionImage;
   }
/**
*本类的赋值方法
*/
   public void setData(TslPatientBean vo) {
      this.copyFrom(vo);
   }
/**
*本类的取值方法
*/
   public TslPatientBean getData(){
      TslPatientBean result = new TslPatientBean();
      result.copyFrom(this);
      return result;
   }
/**
*id的赋值方法
*/
   public void setId(Integer id){
      this.id = id;
   }
/**
*tysPatientId的赋值方法
*/
   public void setTysPatientId(Integer tysPatientId){
      this.tysPatientId = tysPatientId;
   }
/**
*tslUserId的赋值方法
*/
   public void setTslUserId(String tslUserId){
      this.tslUserId = tslUserId;
   }
/**
*tslName的赋值方法
*/
   public void setTslName(String tslName){
      this.tslName = tslName;
   }
/**
*tslPassword的赋值方法
*/
   public void setTslPassword(String tslPassword){
      this.tslPassword = tslPassword;
   }
/**
*tslNickName的赋值方法
*/
   public void setTslNickName(String tslNickName){
      this.tslNickName = tslNickName;
   }
/**
*tslShenfenzhengnum的赋值方法
*/
   public void setTslShenfenzhengnum(String tslShenfenzhengnum){
      this.tslShenfenzhengnum = tslShenfenzhengnum;
   }
/**
*tslShebaonum的赋值方法
*/
   public void setTslShebaonum(String tslShebaonum){
      this.tslShebaonum = tslShebaonum;
   }
/**
*tslPhone1的赋值方法
*/
   public void setTslPhone1(String tslPhone1){
      this.tslPhone1 = tslPhone1;
   }
/**
*tslPhonetype1的赋值方法
*/
   public void setTslPhonetype1(String tslPhonetype1){
      this.tslPhonetype1 = tslPhonetype1;
   }
/**
*tslPhone2的赋值方法
*/
   public void setTslPhone2(String tslPhone2){
      this.tslPhone2 = tslPhone2;
   }
/**
*tslPhonetype2的赋值方法
*/
   public void setTslPhonetype2(String tslPhonetype2){
      this.tslPhonetype2 = tslPhonetype2;
   }
/**
*tslSex的赋值方法
*/
   public void setTslSex(String tslSex){
      this.tslSex = tslSex;
   }
/**
*tslEmail的赋值方法
*/
   public void setTslEmail(String tslEmail){
      this.tslEmail = tslEmail;
   }
/**
*tslAddressarea的赋值方法
*/
   public void setTslAddressarea(String tslAddressarea){
      this.tslAddressarea = tslAddressarea;
   }
/**
*tslAddressdetail的赋值方法
*/
   public void setTslAddressdetail(String tslAddressdetail){
      this.tslAddressdetail = tslAddressdetail;
   }
/**
*tslPhotoshebao的赋值方法
*/
   public void setTslPhotoshebao(String tslPhotoshebao){
      this.tslPhotoshebao = tslPhotoshebao;
   }
/**
*tslPhotoshenfenzheng的赋值方法
*/
   public void setTslPhotoshenfenzheng(String tslPhotoshenfenzheng){
      this.tslPhotoshenfenzheng = tslPhotoshenfenzheng;
   }
/**
*tslPhotoshenfenzhengf的赋值方法
*/
   public void setTslPhotoshenfenzhengf(String tslPhotoshenfenzhengf){
      this.tslPhotoshenfenzhengf = tslPhotoshenfenzhengf;
   }
/**
*tslPhotomente的赋值方法
*/
   public void setTslPhotomente(String tslPhotomente){
      this.tslPhotomente = tslPhotomente;
   }
/**
*tslPhotohead的赋值方法
*/
   public void setTslPhotohead(String tslPhotohead){
      this.tslPhotohead = tslPhotohead;
   }
/**
*flagstate的赋值方法
*/
   public void setFlagstate(Integer flagstate){
      this.flagstate = flagstate;
   }
/**
*flagmsg的赋值方法
*/
   public void setFlagmsg(String flagmsg){
      this.flagmsg = flagmsg;
   }
/**
*createTime的赋值方法
*/
   public void setCreateTime(java.util.Date createTime){
      this.createTime = createTime;
   }
/**
*status的赋值方法
*/
   public void setStatus(Integer status){
      this.status = status;
   }
/**
*id的取值方法
*/
   public Integer getId() {
      return id;
   }
/**
*patient的取值方法
*/
   public Integer getTysPatientId() {
      return tysPatientId;
   }
/**
*tslUserId的取值方法
*/
   public String getTslUserId() {
      return tslUserId;
   }
/**
*tslName的取值方法
*/
   public String getTslName() {
      return tslName;
   }
/**
*tslPassword的取值方法
*/
   public String getTslPassword() {
      return tslPassword;
   }
/**
*tslNickName的取值方法
*/
   public String getTslNickName() {
      return tslNickName;
   }
/**
*tslShenfenzhengnum的取值方法
*/
   public String getTslShenfenzhengnum() {
      return tslShenfenzhengnum;
   }
/**
*tslShebaonum的取值方法
*/
   public String getTslShebaonum() {
      return tslShebaonum;
   }
/**
*tslPhone1的取值方法
*/
   public String getTslPhone1() {
      return tslPhone1;
   }
/**
*tslPhonetype1的取值方法
*/
   public String getTslPhonetype1() {
      return tslPhonetype1;
   }
/**
*tslPhone2的取值方法
*/
   public String getTslPhone2() {
      return tslPhone2;
   }
/**
*tslPhonetype2的取值方法
*/
   public String getTslPhonetype2() {
      return tslPhonetype2;
   }
/**
*tslSex的取值方法
*/
   public String getTslSex() {
      return tslSex;
   }
/**
*tslEmail的取值方法
*/
   public String getTslEmail() {
      return tslEmail;
   }
/**
*tslAddressarea的取值方法
*/
   public String getTslAddressarea() {
      return tslAddressarea;
   }
/**
*tslAddressdetail的取值方法
*/
   public String getTslAddressdetail() {
      return tslAddressdetail;
   }
/**
*tslPhotoshebao的取值方法
*/
   public String getTslPhotoshebao() {
      return tslPhotoshebao;
   }
/**
*tslPhotoshenfenzheng的取值方法
*/
   public String getTslPhotoshenfenzheng() {
      return tslPhotoshenfenzheng;
   }
/**
*tslPhotoshenfenzhengf的取值方法
*/
   public String getTslPhotoshenfenzhengf() {
      return tslPhotoshenfenzhengf;
   }
/**
*tslPhotomente的取值方法
*/
   public String getTslPhotomente() {
      return tslPhotomente;
   }
/**
*tslPhotohead的取值方法
*/
   public String getTslPhotohead() {
      return tslPhotohead;
   }
/**
*flagstate的取值方法
*/
   public Integer getFlagstate() {
      return flagstate;
   }
/**
*flagmsg的取值方法
*/
   public String getFlagmsg() {
      return flagmsg;
   }
/**
*createTime的取值方法
*/
   public java.util.Date getCreateTime() {
      return createTime;
   }
/**
*status的取值方法
*/
   public Integer getStatus() {
      return status;
   }
/**
*id取值方法
*/
public String getYongYaoShi() {
	return yongYaoShi;
}
public void setYongYaoShi(String yongYaoShi) {
	this.yongYaoShi = yongYaoShi;
}
public String getAllergy() {
	return allergy;
}
public void setAllergy(String allergy) {
	this.allergy = allergy;
}
public String getBingZhong() {
	return bingZhong;
}
public void setBingZhong(String bingZhong) {
	this.bingZhong = bingZhong;
}
public String getTime() {
	return time;
}
public void setTime(String time) {
	this.time = time;
}
public String getTslPhotoshebaoPath() {
	return tslPhotoshebaoPath;
}
public void setTslPhotoshebaoPath(String tslPhotoshebaoPath) {
	this.tslPhotoshebaoPath = tslPhotoshebaoPath;
}
public String getTslPhotoshenfenzhengPath() {
	return tslPhotoshenfenzhengPath;
}
public void setTslPhotoshenfenzhengPath(String tslPhotoshenfenzhengPath) {
	this.tslPhotoshenfenzhengPath = tslPhotoshenfenzhengPath;
}
public String getTslPhotoshenfenzhengfPath() {
	return tslPhotoshenfenzhengfPath;
}
public void setTslPhotoshenfenzhengfPath(String tslPhotoshenfenzhengfPath) {
	this.tslPhotoshenfenzhengfPath = tslPhotoshenfenzhengfPath;
}
public String getTslPhotomentePath() {
	return tslPhotomentePath;
}
public void setTslPhotomentePath(String tslPhotomentePath) {
	this.tslPhotomentePath = tslPhotomentePath;
}
public String getTslPhotoheadPath() {
	return tslPhotoheadPath;
}
public void setTslPhotoheadPath(String tslPhotoheadPath) {
	this.tslPhotoheadPath = tslPhotoheadPath;
}

public void setPrescriptionImage(String prescriptionImage){
	this.prescriptionImage = prescriptionImage;
}

public String getPrescriptionImage(){
	return this.prescriptionImage;
}

}