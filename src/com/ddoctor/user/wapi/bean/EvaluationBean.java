package com.ddoctor.user.wapi.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Date:2015-6-28上午2:30:13 Author:萧 TODO:健康评估报告
 */
public class EvaluationBean implements Parcelable {

	private String total; // 格式 XX% 以下相同
	private String sugar;
	private String diet;  
	private String sport;
	private String medical;
	private String evaluationTime;

	public EvaluationBean() {
		super();
		// TODO Auto-generated constructor stub
	}

	public EvaluationBean(String total, String sugar, String diet,
			String sport, String medical , String evaluationTime) {
		super();
		this.total = total;
		this.sugar = sugar;
		this.diet = diet;
		this.sport = sport;
		this.medical = medical;
		this.evaluationTime = evaluationTime;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public String getSugar() {
		return sugar;
	}

	public void setSugar(String sugar) {
		this.sugar = sugar;
	}

	public String getDiet() {
		return diet;
	}

	public void setDiet(String diet) {
		this.diet = diet;
	}

	public String getSport() {
		return sport;
	}

	public void setSport(String sport) {
		this.sport = sport;
	}

	public String getMedical() {
		return medical;
	}

	public void setMedical(String medical) {
		this.medical = medical;
	}

	public String getEvaluationTime() {
		return evaluationTime;
	}
	
	public void setEvaluationTime(String evaluationTime) {
		this.evaluationTime = evaluationTime;
	}

	@Override
	public String toString() {
		return "EvaluationBean [total=" + total + ", sugar=" + sugar
				+ ", diet=" + diet + ", sport=" + sport + ", medical="
				+ medical + "]";
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.total);
		dest.writeString(this.sugar);
		dest.writeString(this.diet);
		dest.writeString(this.sport);
		dest.writeString(this.medical);
		dest.writeString(this.evaluationTime);
	}

	protected EvaluationBean(Parcel in) {
		this.total = in.readString();
		this.sugar = in.readString();
		this.diet = in.readString();
		this.sport = in.readString();
		this.medical = in.readString();
		this.evaluationTime = in.readString();
	}

	public static final Parcelable.Creator<EvaluationBean> CREATOR = new Parcelable.Creator<EvaluationBean>() {
		public EvaluationBean createFromParcel(Parcel source) {
			return new EvaluationBean(source);
		}

		public EvaluationBean[] newArray(int size) {
			return new EvaluationBean[size];
		}
	};
}
