package com.ddoctor.user.wapi.bean;

/**
 * <p>Title: 药品列表</p>
 * <p>Description: data view</p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: ddoctor</p>
 * @author tian jiale
 * @version 1.0
 */

import android.os.Parcel;
import android.os.Parcelable;

public class MedicalBean implements Parcelable {
	/**
	 * 定义本类的私有变量
	 */
	private Integer id;
	private String name;
	private String unit;
	private Integer type;

	/**
	 * 本类的实例化方法
	 */
	public MedicalBean() {
	}

	/**
	 * 本类的带参数的实例化方法
	 */
	public MedicalBean(Integer id, String name, String unit, Integer type) {
		this.id = id;
		this.name = name;
		this.unit = unit;
		this.type = type;
	}

	/**
	 * 以本类为参数传入时，实例化本类的方法
	 */
	public void copyFrom(MedicalBean vo) {
		id = vo.id;
		name = vo.name;
		unit = vo.unit;
		type = vo.type;
	}

	/**
	 * 本类的赋值方法
	 */
	public void setData(MedicalBean vo) {
		this.copyFrom(vo);
	}

	/**
	 * 本类的取值方法
	 */
	public MedicalBean getData() {
		MedicalBean result = new MedicalBean();
		result.copyFrom(this);
		return result;
	}

	/**
	 * id的赋值方法
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * mame的赋值方法
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * unit的赋值方法
	 */
	public void setUnit(String unit) {
		this.unit = unit;
	}

	/**
	 * type的赋值方法
	 */
	public void setType(Integer type) {
		this.type = type;
	}

	/**
	 * id的取值方法
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * mame的取值方法
	 */
	public String getName() {
		return name;
	}

	/**
	 * unit的取值方法
	 */
	public String getUnit() {
		return unit;
	}

	/**
	 * type的取值方法
	 */
	public Integer getType() {
		return type;
	}

	@Override
	public String toString() {
		return "MedicalBean [id=" + id + ", name=" + name + ", unit=" + unit
				+ ", type=" + type + "]";
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeValue(this.id);
		dest.writeString(this.name);
		dest.writeString(this.unit);
		dest.writeValue(this.type);
	}

	protected MedicalBean(Parcel in) {
		this.id = (Integer) in.readValue(Integer.class.getClassLoader());
		this.name = in.readString();
		this.unit = in.readString();
		this.type = (Integer) in.readValue(Integer.class.getClassLoader());
	}

	public static final Parcelable.Creator<MedicalBean> CREATOR = new Parcelable.Creator<MedicalBean>() {
		public MedicalBean createFromParcel(Parcel source) {
			return new MedicalBean(source);
		}

		public MedicalBean[] newArray(int size) {
			return new MedicalBean[size];
		}
	};
}