package com.ddoctor.user.wapi.bean;

/*
 * 注意！注意！注意！注意！注意！注意！注意！注意！注意！注意！注意！注意！注意！注意！注意！注意！
 *	********************： 此文件有自定义字段，不要直接覆盖 ************************ 
 * 
 */

/**
 * <p>Title: Value code</p>
 * <p>Description: data view</p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: ddoctor</p>
 * @author tian jiale
 * @version 1.0
 */

public class TslProductBean implements java.io.Serializable {
/**
	 * 
	 */
	private static final long serialVersionUID = -5124141012284118604L;
/**
*定义本类的私有变量
*/
   private Integer id;
   private String tslProductId;
   private String name;
   private String hxname;
   private String guige;
   private String yongfa;
   private String yongliang;
   private String unit;
   private Float price;
   private String factory;
   private String type;
   private java.util.Date createTime;
   private Integer maxDay;
   private Integer maxNum;
   private String message;
   private Float geRenPrice;
   private Float sheBaoPrice;
   
   //***** 这是自定义字段
   private int buyNum;
/**
*本类的实例化方法
*/
   public TslProductBean() {
   }
/**
*本类的带参数的实例化方法
*/
   public TslProductBean(Integer id,String tslProductId,String name,String hxname,String guige,String yongfa,String yongliang,String unit,Float price,String factory,String type,java.util.Date createTime, Integer maxDay, Integer maxNum, String message, Float geRenPrice, Float sheBaoPrice) {
      this.id = id;
      this.tslProductId = tslProductId;
      this.name = name;
      this.hxname = hxname;
      this.guige = guige;
      this.yongfa = yongfa;
      this.yongliang = yongliang;
      this.unit = unit;
      this.price = price;
      this.factory = factory;
      this.type = type;
      this.createTime = createTime;
      this.maxDay = maxDay;
      this.maxNum = maxNum;
      this.message = message;
      this.geRenPrice = geRenPrice;
      this.sheBaoPrice = sheBaoPrice;
      
      this.buyNum = 0;
   }
/**
*以本类为参数传入时，实例化本类的方法
*/
   public void copyFrom(TslProductBean vo) {
      id = vo.id;
      tslProductId = vo.tslProductId;
      name = vo.name;
      hxname = vo.hxname;
      guige = vo.guige;
      yongfa = vo.yongfa;
      yongliang = vo.yongliang;
      unit = vo.unit;
      price = vo.price;
      factory = vo.factory;
      type = vo.type;
      createTime = vo.createTime;
      maxDay = vo.maxDay;
      maxNum = vo.maxNum;
      message = vo.message;
      geRenPrice = vo.geRenPrice;
      sheBaoPrice = vo.sheBaoPrice;
      
      buyNum = vo.buyNum;
   }
/**
*本类的赋值方法
*/
   public void setData(TslProductBean vo) {
      this.copyFrom(vo);
   }
/**
*本类的取值方法
*/
   public TslProductBean getData(){
      TslProductBean result = new TslProductBean();
      result.copyFrom(this);
      return result;
   }
/**
*id的赋值方法
*/
   public void setId(Integer id){
      this.id = id;
   }
/**
*tslProductId的赋值方法
*/
   public void setTslProductId(String tslProductId){
      this.tslProductId = tslProductId;
   }
/**
*name的赋值方法
*/
   public void setName(String name){
      this.name = name;
   }
/**
*hxname的赋值方法
*/
   public void setHxname(String hxname){
      this.hxname = hxname;
   }
/**
*guige的赋值方法
*/
   public void setGuige(String guige){
      this.guige = guige;
   }
/**
*yongfa的赋值方法
*/
   public void setYongfa(String yongfa){
      this.yongfa = yongfa;
   }
/**
*yongliang的赋值方法
*/
   public void setYongliang(String yongliang){
      this.yongliang = yongliang;
   }
/**
*unit的赋值方法
*/
   public void setUnit(String unit){
      this.unit = unit;
   }
/**
*price的赋值方法
*/
   public void setPrice(Float price){
      this.price = price;
   }
/**
*factory的赋值方法
*/
   public void setFactory(String factory){
      this.factory = factory;
   }
/**
*type的赋值方法
*/
   public void setType(String type){
      this.type = type;
   }
/**
*createTime的赋值方法
*/
   public void setCreateTime(java.util.Date createTime){
      this.createTime = createTime;
   }
/**
*id的取值方法
*/
   public Integer getId() {
      return id;
   }
/**
*tslProductId的取值方法
*/
   public String getTslProductId() {
      return tslProductId;
   }
/**
*name的取值方法
*/
   public String getName() {
      return name;
   }
/**
*hxname的取值方法
*/
   public String getHxname() {
      return hxname;
   }
/**
*guige的取值方法
*/
   public String getGuige() {
      return guige;
   }
/**
*yongfa的取值方法
*/
   public String getYongfa() {
      return yongfa;
   }
/**
*yongliang的取值方法
*/
   public String getYongliang() {
      return yongliang;
   }
/**
*unit的取值方法
*/
   public String getUnit() {
      return unit;
   }
/**
*price的取值方法
*/
   public Float getPrice() {
      return price;
   }
/**
*factory的取值方法
*/
   public String getFactory() {
      return factory;
   }
/**
*type的取值方法
*/
   public String getType() {
      return type;
   }
/**
*createTime的取值方法
*/
   public java.util.Date getCreateTime() {
      return createTime;
   }
/**
*id取值方法
*/
public Integer getMaxDay() {
	return maxDay;
}
public void setMaxDay(Integer maxDay) {
	this.maxDay = maxDay;
}
public Integer getMaxNum() {
	return maxNum;
}
public void setMaxNum(Integer maxNum) {
	this.maxNum = maxNum;
}
public String getMessage() {
	return message;
}
public void setMessage(String message) {
	this.message = message;
}
public Float getGeRenPrice() {
	return geRenPrice;
}
public void setGeRenPrice(Float geRenPrice) {
	this.geRenPrice = geRenPrice;
}
public Float getSheBaoPrice() {
	return sheBaoPrice;
}
public void setSheBaoPrice(Float sheBaoPrice) {
	this.sheBaoPrice = sheBaoPrice;
}


public int getBuyNum() {
	return buyNum;
}

public void setBuyNum(int buyNum) {
	this.buyNum = buyNum;
}

}