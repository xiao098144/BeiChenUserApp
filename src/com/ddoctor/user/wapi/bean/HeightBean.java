package com.ddoctor.user.wapi.bean;

/**
 * <p>Title: Value code</p>
 * <p>Description: data view</p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: ddoctor</p>
 * @author tian jiale
 * @version 1.0
 */

import android.os.Parcel;
import android.os.Parcelable;

import com.ddoctor.enums.RecordLayoutType;

public class HeightBean implements Parcelable , Comparable<HeightBean>{
   /**
    * 定义本类的私有变量
    */
   private Integer id;
   private Integer height;
   private Float weight;
   private String time;
   private String remark;
   private Float bmi;
   private Integer patientId;
   private String date; // yyyy-MM-dd

   private RecordLayoutType layoutType = RecordLayoutType.TYPE_VALUE;

   /**
    * 本类的实例化方法
    */
   public HeightBean() {
   }

   /**
    * 本类的带参数的实例化方法
    */

   public HeightBean(Integer id, Integer height, Float weight, String time,
                     String remark, Float bmi, Integer patientId, String date,
                     RecordLayoutType layoutType) {
      super();
      this.id = id;
      this.height = height;
      this.weight = weight;
      this.time = time;
      this.remark = remark;
      this.bmi = bmi;
      this.patientId = patientId;
      this.date = date;
      this.layoutType = layoutType;
   }

   /**
    * 以本类为参数传入时，实例化本类的方法
    */
   public void copyFrom(HeightBean vo) {
      id = vo.id;
      height = vo.height;
      weight = vo.weight;
      time = vo.time;
      remark = vo.remark;
      bmi = vo.bmi;
      patientId = vo.patientId;
   }

   /**
    * 本类的赋值方法
    */
   public void setData(HeightBean vo) {
      this.copyFrom(vo);
   }

   /**
    * 本类的取值方法
    */
   public HeightBean getData() {
      HeightBean result = new HeightBean();
      result.copyFrom(this);
      return result;
   }

   /**
    * id的赋值方法
    */
   public void setId(Integer id) {
      this.id = id;
   }

   /**
    * height的赋值方法
    */
   public void setHeight(Integer height) {
      this.height = height;
   }

   /**
    * weight的赋值方法
    */
   public void setWeight(Float weight) {
      this.weight = weight;
   }

   /**
    * time的赋值方法
    */
   public void setTime(String time) {
      this.time = time;
   }

   /**
    * remark的赋值方法
    */
   public void setRemark(String remark) {
      this.remark = remark;
   }

   /**
    * bmi的赋值方法
    */
   public void setBmi(Float bmi) {
      this.bmi = bmi;
   }

   /**
    * patientId的赋值方法
    */
   public void setPatientId(Integer patientId) {
      this.patientId = patientId;
   }

   /**
    * id的取值方法
    */
   public Integer getId() {
      return id;
   }

   /**
    * height的取值方法
    */
   public Integer getHeight() {
      return height;
   }

   /**
    * weight的取值方法
    */
   public Float getWeight() {
      return weight;
   }

   /**
    * time的取值方法
    */
   public String getTime() {
      return time;
   }

   /**
    * remark的取值方法
    */
   public String getRemark() {
      return remark;
   }

   /**
    * bmi的取值方法
    */
   public Float getBmi() {
      return bmi;
   }

   /**
    * patientId的取值方法
    */
   public Integer getPatientId() {
      return patientId;
   }

   public void setDate(String date) {
      this.date = date;
   }

   public String getDate() {
      return date;
   }

   public void setLayoutType(RecordLayoutType layoutType) {
      this.layoutType = layoutType;
   }

   public RecordLayoutType getLayoutType() {
      return layoutType;
   }

   @Override
   public String toString() {
      return "HeightBean [id=" + id + ", height=" + height + ", weight="
              + weight + ", time=" + time + ", remark=" + remark + ", bmi="
              + bmi + ", patientId=" + patientId + ", date=" + date
              + ", layoutType=" + layoutType + "]";
   }

   @Override
   public int describeContents() {
      return 0;
   }

   @Override
   public void writeToParcel(Parcel dest, int flags) {
      dest.writeValue(this.id);
      dest.writeValue(this.height);
      dest.writeValue(this.weight);
      dest.writeString(this.time);
      dest.writeString(this.remark);
      dest.writeValue(this.bmi);
      dest.writeValue(this.patientId);
      dest.writeString(this.date);
      dest.writeInt(this.layoutType == RecordLayoutType.TYPE_VALUE ? 1 : 2);
   }

   protected HeightBean(Parcel in) {
      this.id = (Integer) in.readValue(Integer.class.getClassLoader());
      this.height = (Integer) in.readValue(Integer.class.getClassLoader());
      this.weight = (Float) in.readValue(Float.class.getClassLoader());
      this.time = in.readString();
      this.remark = in.readString();
      this.bmi = (Float) in.readValue(Float.class.getClassLoader());
      this.patientId = (Integer) in.readValue(Integer.class.getClassLoader());
      this.date = in.readString();
      int tmpLayoutType = in.readInt();
      this.layoutType = tmpLayoutType == 1 ? RecordLayoutType.TYPE_VALUE
				: RecordLayoutType.TYPE_CATEGORY;
   }

   public static final Parcelable.Creator<HeightBean> CREATOR = new Parcelable.Creator<HeightBean>() {
      public HeightBean createFromParcel(Parcel source) {
         return new HeightBean(source);
      }

      public HeightBean[] newArray(int size) {
         return new HeightBean[size];
      }
   };

@Override
public int compareTo(HeightBean another) {
	int compare;
	compare = getTime().compareTo(another.getTime());
	return 0 - compare;
}
}