package com.ddoctor.user.wapi.bean;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.ddoctor.enums.RecordLayoutType;

/**
 * <p>
 * Title: Value code
 * </p>
 * <p>
 * Description: data view
 * </p>
 * <p>
 * Copyright: Copyright (c) 2007
 * </p>
 * <p>
 * Company: ddoctor
 * </p>
 * 
 * @author tian jiale
 * @version 1.0
 */

public class MedicalRecordBean implements Parcelable,
		Comparable<MedicalRecordBean> {
	/**
	 * 定义本类的私有变量
	 */
	private Integer id;
	private String name;
	private String count;
	private String unit;
	private String remark;
	private String date;
	private String time;
	private Integer isuse;
	private Integer medicalId;

	private RecordLayoutType layoutType = RecordLayoutType.TYPE_VALUE;

	/**
	 * 本类的实例化方法
	 */
	public MedicalRecordBean() {
	}

	public MedicalRecordBean(String date, String time) {
		super();
		this.date = date;
		this.time = time;
	}

	public MedicalRecordBean(Integer id, String name, String count,
			String unit, String remark, String date, String time,
			Integer isuse, Integer medicalId) {
		super();
		this.id = id;
		this.name = name;
		this.count = count;
		this.unit = unit;
		this.remark = remark;
		this.date = date;
		this.time = time;
		this.isuse = isuse;
		this.medicalId = medicalId;
	}

	/**
	 * 以本类为参数传入时，实例化本类的方法
	 */
	public void copyFrom(MedicalRecordBean vo) {
		id = vo.id;
		name = vo.name;
		count = vo.count;
		unit = vo.unit;
		remark = vo.remark;
		date = vo.date;
		time = vo.time;
		isuse = vo.isuse;
		medicalId = vo.medicalId;
	}

	/**
	 * 本类的赋值方法
	 */
	public void setData(MedicalRecordBean vo) {
		this.copyFrom(vo);
	}

	/**
	 * 本类的取值方法
	 */
	public MedicalRecordBean getData() {
		MedicalRecordBean result = new MedicalRecordBean();
		result.copyFrom(this);
		return result;
	}

	/**
	 * id的赋值方法
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * name的赋值方法
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * count的赋值方法
	 */
	public void setCount(String count) {
		this.count = count;
	}

	/**
	 * unit的赋值方法
	 */
	public void setUnit(String unit) {
		this.unit = unit;
	}

	/**
	 * remark的赋值方法
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

	/**
	 * date的赋值方法
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * time的赋值方法
	 */
	public void setTime(String time) {
		this.time = time;
	}

	/**
	 * isuse的赋值方法
	 */
	public void setIsuse(Integer isuse) {
		this.isuse = isuse;
	}

	/**
	 * id的取值方法
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * name的取值方法
	 */
	public String getName() {
		return name;
	}

	/**
	 * count的取值方法
	 */
	public String getCount() {
		return count;
	}

	/**
	 * unit的取值方法
	 */
	public String getUnit() {
		return unit;
	}

	/**
	 * remark的取值方法
	 */
	public String getRemark() {
		return remark;
	}

	/**
	 * date的取值方法
	 */
	public String getDate() {
		return date;
	}

	/**
	 * time的取值方法
	 */
	public String getTime() {
		return time;
	}

	/**
	 * isuse的取值方法
	 */
	public Integer getIsuse() {
		return isuse;
	}

	public Integer getMedicalId() {
		return medicalId;
	}

	public void setMedicalId(Integer medicalId) {
		this.medicalId = medicalId;
	}

	public RecordLayoutType getLayoutType() {
		return layoutType;
	}

	public void setLayoutType(RecordLayoutType layoutType) {
		this.layoutType = layoutType;
	}

	@Override
	public String toString() {
		return "MedicalRecordBean [id=" + id + ", name=" + name + ", count="
				+ count + ", unit=" + unit + ", remark=" + remark + ", date="
				+ date + ", time=" + time + ", medicalId=" + medicalId + "]";
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeValue(this.id);
		dest.writeString(this.name);
		dest.writeString(this.count);
		dest.writeString(this.unit);
		dest.writeString(this.remark);
		dest.writeString(this.date);
		dest.writeString(this.time);
		dest.writeValue(this.isuse);
		dest.writeValue(this.medicalId);
		dest.writeInt(layoutType == RecordLayoutType.TYPE_VALUE ? 0 : 1);
	}

	protected MedicalRecordBean(Parcel in) {
		this.id = (Integer) in.readValue(Integer.class.getClassLoader());
		this.name = in.readString();
		this.count = in.readString();
		this.unit = in.readString();
		this.remark = in.readString();
		this.date = in.readString();
		this.time = in.readString();
		this.isuse = (Integer) in.readValue(Integer.class.getClassLoader());
		this.medicalId = (Integer) in.readValue(Integer.class.getClassLoader());
		this.layoutType = in.readInt() == 0 ? RecordLayoutType.TYPE_VALUE
				: RecordLayoutType.TYPE_CATEGORY;
	}

	public static final Parcelable.Creator<MedicalRecordBean> CREATOR = new Parcelable.Creator<MedicalRecordBean>() {
		public MedicalRecordBean createFromParcel(Parcel source) {
			return new MedicalRecordBean(source);
		}

		public MedicalRecordBean[] newArray(int size) {
			return new MedicalRecordBean[size];
		}
	};

	public void copyFromMedicalBean(MedicalBean medicalBean) {
		this.setId(0);
		this.setIsuse(1);
		this.setName(medicalBean.getName());
		this.setUnit(medicalBean.getUnit());
		this.setMedicalId(medicalBean.getId());
	}

	@Override
	public int compareTo(MedicalRecordBean another) {
		int compare;
		compare = getDate().compareTo(another.getDate());
		if (compare == 0) {
			if (!TextUtils.isEmpty(getTime()) &&(!TextUtils.isEmpty(another.getTime()))) {
				compare = getTime().compareTo(another.getTime());
			}
		}
		return 0 - compare;
	}
}