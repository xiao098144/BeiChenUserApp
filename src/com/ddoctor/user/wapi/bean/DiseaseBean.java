package com.ddoctor.user.wapi.bean;

/**
 * <p>Title: Value code</p>
 * <p>Description: data view</p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: ddoctor</p>
 * @author tian jiale
 * @version 1.0
 */

import android.os.Parcel;
import android.os.Parcelable;

import com.ddoctor.enums.RecordLayoutType;

public class DiseaseBean implements Parcelable, Comparable<DiseaseBean> {
	/**
	 * 定义本类的私有变量
	 */
	private Integer id;
	private String time;
	private Integer type;
	private String content;
	private String remark;
	private String file;
	private String fileId;
	private Integer patientId;
	private Integer doctorId;
	private String date; // yyyy-MM-dd

	private RecordLayoutType layoutType = RecordLayoutType.TYPE_VALUE;

	/**
	 * 本类的实例化方法
	 */
	public DiseaseBean() {
	}

	/**
	 * 本类的带参数的实例化方法
	 */

	public DiseaseBean(Integer id, String time, Integer type, String content,
			String remark, String file, String fileId, Integer patientId,
			Integer doctorId, String date, RecordLayoutType layoutType) {
		super();
		this.id = id;
		this.time = time;
		this.type = type;
		this.content = content;
		this.remark = remark;
		this.file = file;
		this.fileId = fileId;
		this.patientId = patientId;
		this.doctorId = doctorId;
		this.date = date;
		this.layoutType = layoutType;
	}

	/**
	 * 以本类为参数传入时，实例化本类的方法
	 */
	public void copyFrom(DiseaseBean vo) {
		id = vo.id;
		time = vo.time;
		type = vo.type;
		content = vo.content;
		remark = vo.remark;
		file = vo.file;
		fileId = vo.fileId;
		patientId = vo.patientId;
		doctorId = vo.doctorId;
		date = vo.date;
		layoutType = vo.layoutType;
	}

	/**
	 * 本类的赋值方法
	 */
	public void setData(DiseaseBean vo) {
		this.copyFrom(vo);
	}

	/**
	 * 本类的取值方法
	 */
	public DiseaseBean getData() {
		DiseaseBean result = new DiseaseBean();
		result.copyFrom(this);
		return result;
	}

	/**
	 * id的赋值方法
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * time的赋值方法
	 */
	public void setTime(String time) {
		this.time = time;
	}

	/**
	 * type的赋值方法
	 */
	public void setType(Integer type) {
		this.type = type;
	}

	/**
	 * content的赋值方法
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * remark的赋值方法
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

	/**
	 * file的赋值方法
	 */
	public void setFile(String file) {
		this.file = file;
	}

	/**
	 * fileId的赋值方法
	 */
	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

	/**
	 * patientId的赋值方法
	 */
	public void setPatientId(Integer patientId) {
		this.patientId = patientId;
	}

	/**
	 * doctorId的赋值方法
	 */
	public void setDoctorId(Integer doctorId) {
		this.doctorId = doctorId;
	}

	/**
	 * id的取值方法
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * time的取值方法
	 */
	public String getTime() {
		return time;
	}

	/**
	 * type的取值方法
	 */
	public Integer getType() {
		return type;
	}

	/**
	 * content的取值方法
	 */
	public String getContent() {
		return content;
	}

	/**
	 * remark的取值方法
	 */
	public String getRemark() {
		return remark;
	}

	/**
	 * file的取值方法
	 */
	public String getFile() {
		return file;
	}

	/**
	 * fileId的取值方法
	 */
	public String getFileId() {
		return fileId;
	}

	/**
	 * patientId的取值方法
	 */
	public Integer getPatientId() {
		return patientId;
	}

	/**
	 * doctorId的取值方法
	 */
	public Integer getDoctorId() {
		return doctorId;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getDate() {
		return date;
	}

	public void setLayoutType(RecordLayoutType layoutType) {
		this.layoutType = layoutType;
	}

	public RecordLayoutType getLayoutType() {
		return layoutType;
	}

	@Override
	public String toString() {
		return "DiseaseBean [id=" + id + ", time=" + time + ", type=" + type
				+ ", content=" + content + ", remark=" + remark + ", file="
				+ file + ", fileId=" + fileId + ", patientId=" + patientId
				+ ", doctorId=" + doctorId + ", date=" + date + ", layoutType="
				+ layoutType + "]";
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeValue(this.id);
		dest.writeString(this.time);
		dest.writeValue(this.type);
		dest.writeString(this.content);
		dest.writeString(this.remark);
		dest.writeString(this.file);
		dest.writeString(this.fileId);
		dest.writeValue(this.patientId);
		dest.writeValue(this.doctorId);
		dest.writeString(this.date);
		dest.writeInt(this.layoutType == RecordLayoutType.TYPE_VALUE ? 1 : 2);
	}

	protected DiseaseBean(Parcel in) {
		this.id = (Integer) in.readValue(Integer.class.getClassLoader());
		this.time = in.readString();
		this.type = (Integer) in.readValue(Integer.class.getClassLoader());
		this.content = in.readString();
		this.remark = in.readString();
		this.file = in.readString();
		this.fileId = in.readString();
		this.patientId = (Integer) in.readValue(Integer.class.getClassLoader());
		this.doctorId = (Integer) in.readValue(Integer.class.getClassLoader());
		this.date = in.readString();
		int tmpLayoutType = in.readInt();
		this.layoutType = tmpLayoutType == 1 ? RecordLayoutType.TYPE_VALUE
				: RecordLayoutType.TYPE_CATEGORY;
	}

	public static final Parcelable.Creator<DiseaseBean> CREATOR = new Parcelable.Creator<DiseaseBean>() {
		public DiseaseBean createFromParcel(Parcel source) {
			return new DiseaseBean(source);
		}

		public DiseaseBean[] newArray(int size) {
			return new DiseaseBean[size];
		}
	};

	@Override
	public int compareTo(DiseaseBean another) {
		int compare;
		compare = getTime().compareTo(another.getTime());
		return 0 - compare;
	}
}