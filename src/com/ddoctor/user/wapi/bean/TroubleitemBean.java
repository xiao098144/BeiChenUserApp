package com.ddoctor.user.wapi.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * <p>Title: 不适  选项</p>
 * <p>Description: data view</p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: ddoctor</p>
 * @author tian jiale
 * @version 1.0
 */


public class TroubleitemBean implements java.io.Serializable, Parcelable {
	/**
	 * 定义本类的私有变量
	 */
	private Integer id;
	private String name;

	/**
	 * 本类的实例化方法
	 */
	public TroubleitemBean() {
	}

	/**
	 * 本类的带参数的实例化方法
	 */
	public TroubleitemBean(Integer id, String name) {
		this.id = id;
		this.name = name;
	}

	/**
	 * 以本类为参数传入时，实例化本类的方法
	 */
	public void copyFrom(TroubleitemBean vo) {
		id = vo.id;
		name = vo.name;
	}

	/**
	 * 本类的赋值方法
	 */
	public void setData(TroubleitemBean vo) {
		this.copyFrom(vo);
	}

	/**
	 * 本类的取值方法
	 */
	public TroubleitemBean getData() {
		TroubleitemBean result = new TroubleitemBean();
		result.copyFrom(this);
		return result;
	}

	/**
	 * id的赋值方法
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * name的赋值方法
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * id的取值方法
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * name的取值方法
	 */
	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return "TroubleitemBean [id=" + id + ", name=" + name + "]";
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeValue(this.id);
		dest.writeString(this.name);
	}

	protected TroubleitemBean(Parcel in) {
		this.id = (Integer) in.readValue(Integer.class.getClassLoader());
		this.name = in.readString();
	}

	public static final Parcelable.Creator<TroubleitemBean> CREATOR = new Parcelable.Creator<TroubleitemBean>() {
		public TroubleitemBean createFromParcel(Parcel source) {
			return new TroubleitemBean(source);
		}

		public TroubleitemBean[] newArray(int size) {
			return new TroubleitemBean[size];
		}
	};
}