package com.ddoctor.user.wapi.bean;

import android.os.Parcel;
import android.os.Parcelable;

public class SportRemindBean implements Parcelable {

	private Integer id;
	private int userid;
	private Integer type;
	private String content;
	private String time; // mm:ss
	private String routing;
	private Integer parentid;
	private Integer state;
	private String remark;

	public SportRemindBean() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SportRemindBean(Integer id, int userid, Integer type,
			String content, String time, String routing, Integer parentid,
			Integer state, String remark) {
		super();
		this.id = id;
		this.userid = userid;
		this.type = type;
		this.content = content;
		this.time = time;
		this.routing = routing;
		this.parentid = parentid;
		this.state = state;
		this.remark = remark;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getRouting() {
		return routing;
	}

	public void setRouting(String routing) {
		this.routing = routing;
	}

	public Integer getParentid() {
		return parentid;
	}

	public void setParentid(Integer parentid) {
		this.parentid = parentid;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Override
	public String toString() {
		return "SportRemindBean [id=" + id + ", userid=" + userid + ", type="
				+ type + ", content=" + content + ", time=" + time
				+ ", routing=" + routing + ", parentid=" + parentid
				+ ", state=" + state + ", remark=" + remark + "]";
	}


	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeValue(this.id);
		dest.writeInt(this.userid);
		dest.writeValue(this.type);
		dest.writeString(this.content);
		dest.writeString(this.time);
		dest.writeString(this.routing);
		dest.writeValue(this.parentid);
		dest.writeValue(this.state);
		dest.writeString(this.remark);
	}

	protected SportRemindBean(Parcel in) {
		this.id = (Integer) in.readValue(Integer.class.getClassLoader());
		this.userid = in.readInt();
		this.type = (Integer) in.readValue(Integer.class.getClassLoader());
		this.content = in.readString();
		this.time = in.readString();
		this.routing = in.readString();
		this.parentid = (Integer) in.readValue(Integer.class.getClassLoader());
		this.state = (Integer) in.readValue(Integer.class.getClassLoader());
		this.remark = in.readString();
	}

	public static final Parcelable.Creator<SportRemindBean> CREATOR = new Parcelable.Creator<SportRemindBean>() {
		public SportRemindBean createFromParcel(Parcel source) {
			return new SportRemindBean(source);
		}

		public SportRemindBean[] newArray(int size) {
			return new SportRemindBean[size];
		}
	};
}
