package com.ddoctor.user.wapi.bean;

import android.os.Parcel;
import android.os.Parcelable;

import com.ddoctor.enums.RecordLayoutType;

/**
 * Title: 单条血糖记录 
 * @author tian jiale
 * @version 1.0
 */

public class SugarValueBean implements Parcelable , Comparable<SugarValueBean>{
	/**
	 * 定义本类的私有变量
	 */
	private Integer id;
	private String time;
	private Float value;
	private Integer unit;
	private String remark;
	private Integer glucometerId;
	private Integer patientId;
	private int type;
	private String date;
	private Integer sourceType;
	
	private RecordLayoutType layoutType = RecordLayoutType.TYPE_VALUE;

	/**
	 * 本类的实例化方法
	 */
	public SugarValueBean() {
	}

	/**
	 * 本类的带参数的实例化方法
	 */
	public SugarValueBean(Integer id, String time, Float value, Integer unit,
			String remark, Integer glucometerId, Integer patientId, int type,
			String date , Integer sourceType) {
		super();
		this.id = id;
		this.time = time;
		this.value = value;
		this.unit = unit;
		this.remark = remark;
		this.glucometerId = glucometerId;
		this.patientId = patientId;
		this.type = type;
		this.date = date;
		this.sourceType = sourceType;
	}

	/**
	 * 以本类为参数传入时，实例化本类的方法
	 */
	public void copyFrom(SugarValueBean vo) {
		id = vo.id;
		time = vo.time;
		value = vo.value;
		unit = vo.unit;
		remark = vo.remark;
		glucometerId = vo.glucometerId;
		date = vo.date;
		sourceType = vo.sourceType;
	}

	/**
	 * 本类的赋值方法
	 */
	public void setData(SugarValueBean vo) {
		this.copyFrom(vo);
	}

	/**
	 * 本类的取值方法
	 */
	public SugarValueBean getData() {
		SugarValueBean result = new SugarValueBean();
		result.copyFrom(this);
		return result;
	}

	/**
	 * id的赋值方法
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * time的赋值方法
	 */
	public void setTime(String time) {
		this.time = time;
	}

	/**
	 * value的赋值方法
	 */
	public void setValue(Float value) {
		this.value = value;
	}

	/**
	 * unit的赋值方法
	 */
	public void setUnit(Integer unit) {
		this.unit = unit;
	}

	/**
	 * remark的赋值方法
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

	/**
	 * glucometerId的赋值方法
	 */
	public void setGlucometerId(Integer glucometerId) {
		this.glucometerId = glucometerId;
	}

	/**
	 * patientId的赋值方法
	 */
	public void setPatientId(Integer patientId) {
		this.patientId = patientId;
	}

	/**
	 * type的赋值方法
	 */
	public void setType(Integer type) {
		this.type = type;
	}

	/**
	 * id的取值方法
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * time的取值方法
	 */
	public String getTime() {
		return time;
	}

	/**
	 * value的取值方法
	 */
	public Float getValue() {
		return value;
	}

	/**
	 * unit的取值方法
	 */
	public Integer getUnit() {
		return unit;
	}

	/**
	 * remark的取值方法
	 */
	public String getRemark() {
		return remark;
	}

	/**
	 * glucometerId的取值方法
	 */
	public Integer getGlucometerId() {
		return glucometerId;
	}

	/**
	 * patientId的取值方法
	 */
	public Integer getPatientId() {
		return patientId;
	}

	/**
	 * type的取值方法
	 */
	public Integer getType() {
		return type;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public RecordLayoutType getLayoutType() {
		return layoutType;
	}
	
	public void setLayoutType(RecordLayoutType layoutType) {
		this.layoutType = layoutType;
	}
	
	public Integer getSourceType() {
		return sourceType;
	}
	
	public void setSourceType(Integer sourceType) {
		this.sourceType = sourceType;
	}
	
	@Override
	public String toString() {
		return "SugarValueBean [id=" + id + ", time=" + time + ", value="
				+ value + ", unit=" + unit + ", remark=" + remark
				+ ", glucometerId=" + glucometerId + ", type=" + type
				+ ", date=" + date + "]";
	}

	@Override
	public int compareTo(SugarValueBean another) {
		int compare = 0;
		compare = getTime().compareTo(another.getTime());
		if (compare == 0) {
			compare = getType().compareTo(another.getType());
		}
		return compare; 
	}
	
	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeValue(this.id);
		dest.writeString(this.time);
		dest.writeValue(this.value);
		dest.writeValue(this.unit);
		dest.writeString(this.remark);
		dest.writeValue(this.glucometerId);
		dest.writeValue(this.patientId);
		dest.writeInt(this.type);
		dest.writeString(this.date);
		dest.writeValue(this.sourceType);
	}

	private SugarValueBean(Parcel in) {
		this.id = (Integer) in.readValue(Integer.class.getClassLoader());
		this.time = in.readString();
		this.value = (Float) in.readValue(Float.class.getClassLoader());
		this.unit = (Integer) in.readValue(Integer.class.getClassLoader());
		this.remark = in.readString();
		this.glucometerId = (Integer) in.readValue(Integer.class
				.getClassLoader());
		this.patientId = (Integer) in.readValue(Integer.class.getClassLoader());
		this.type = in.readInt();
		this.date = in.readString();
		this.sourceType = (Integer) in.readValue(Integer.class.getClassLoader());
	}

	public static final Parcelable.Creator<SugarValueBean> CREATOR = new Parcelable.Creator<SugarValueBean>() {
		public SugarValueBean createFromParcel(Parcel source) {
			return new SugarValueBean(source);
		}

		public SugarValueBean[] newArray(int size) {
			return new SugarValueBean[size];
		}
	};

}