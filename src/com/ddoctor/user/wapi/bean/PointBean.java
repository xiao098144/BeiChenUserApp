package com.ddoctor.user.wapi.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * <p>
 * Title: 积分记录
 * </p>
 * <p>
 * Description: data view
 * </p>
 * <p>
 * Copyright: Copyright (c) 2007
 * </p>
 * <p>
 * Company: ddoctor
 * </p>
 * 
 * @author tian jiale
 * @version 1.0
 */

public class PointBean implements Parcelable {
	/**
	 * 定义本类的私有变量
	 */
	private Integer id;
	private Integer addValue;
	private Integer reduceValue;
	private String operation;
	private String remark;
	private String time;

	/**
	 * 本类的实例化方法
	 */
	public PointBean() {
	}

	/**
	 * 本类的带参数的实例化方法
	 */
	public PointBean(Integer id, Integer addValue, Integer reduceValue,
			String operation, String remark, String time) {
		this.id = id;
		this.addValue = addValue;
		this.reduceValue = reduceValue;
		this.operation = operation;
		this.remark = remark;
		this.time = time;
	}

	/**
	 * 以本类为参数传入时，实例化本类的方法
	 */
	public void copyFrom(PointBean vo) {
		id = vo.id;
		addValue = vo.addValue;
		reduceValue = vo.reduceValue;
		operation = vo.operation;
		remark = vo.remark;
		time = vo.time;
	}

	/**
	 * 本类的赋值方法
	 */
	public void setData(PointBean vo) {
		this.copyFrom(vo);
	}

	/**
	 * 本类的取值方法
	 */
	public PointBean getData() {
		PointBean result = new PointBean();
		result.copyFrom(this);
		return result;
	}

	/**
	 * id的赋值方法
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * addValue的赋值方法
	 */
	public void setAddValue(Integer addValue) {
		this.addValue = addValue;
	}

	/**
	 * reduceValue的赋值方法
	 */
	public void setReduceValue(Integer reduceValue) {
		this.reduceValue = reduceValue;
	}

	/**
	 * operation的赋值方法
	 */
	public void setOperation(String operation) {
		this.operation = operation;
	}

	/**
	 * remark的赋值方法
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

	/**
	 * time的赋值方法
	 */
	public void setTime(String time) {
		this.time = time;
	}

	/**
	 * id的取值方法
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * addValue的取值方法
	 */
	public Integer getAddValue() {
		return addValue;
	}

	/**
	 * reduceValue的取值方法
	 */
	public Integer getReduceValue() {
		return reduceValue;
	}

	/**
	 * operation的取值方法
	 */
	public String getOperation() {
		return operation;
	}

	/**
	 * remark的取值方法
	 */
	public String getRemark() {
		return remark;
	}

	/**
	 * time的取值方法
	 */
	public String getTime() {
		return time;
	}

	@Override
	public String toString() {
		return "PointBean [id=" + id + ", addValue=" + addValue
				+ ", reduceValue=" + reduceValue + ", operation=" + operation
				+ ", remark=" + remark + ", time=" + time + "]";
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeValue(this.id);
		dest.writeValue(this.addValue);
		dest.writeValue(this.reduceValue);
		dest.writeString(this.operation);
		dest.writeString(this.remark);
		dest.writeString(this.time);
	}

	protected PointBean(Parcel in) {
		this.id = (Integer) in.readValue(Integer.class.getClassLoader());
		this.addValue = (Integer) in.readValue(Integer.class.getClassLoader());
		this.reduceValue = (Integer) in.readValue(Integer.class
				.getClassLoader());
		this.operation = in.readString();
		this.remark = in.readString();
		this.time = in.readString();
	}

	public static final Parcelable.Creator<PointBean> CREATOR = new Parcelable.Creator<PointBean>() {
		public PointBean createFromParcel(Parcel source) {
			return new PointBean(source);
		}

		public PointBean[] newArray(int size) {
			return new PointBean[size];
		}
	};
}