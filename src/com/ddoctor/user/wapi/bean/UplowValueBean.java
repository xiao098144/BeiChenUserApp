package com.ddoctor.user.wapi.bean;

/**
 * <p>Title: Value code</p>
 * <p>Description: data view</p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: ddoctor</p>
 * @author tian jiale
 * @version 1.0
 */


public class UplowValueBean implements java.io.Serializable {
	/**
	 * 定义本类的私有变量
	 */
	private Float upper;
	private Float lower;
	private Integer limit;
	private Integer type;

	/**
	 * 本类的实例化方法
	 */
	public UplowValueBean() {
	}

	/**
	 * 本类的带参数的实例化方法
	 */
	public UplowValueBean(Float upper, Float lower, Integer limit,
			Integer type) {
		this.upper = upper;
		this.lower = lower;
		this.limit = limit;
		this.type = type;
	}

	/**
	 * 以本类为参数传入时，实例化本类的方法
	 */
	public void copyFrom(UplowValueBean vo) {
		upper = vo.upper;
		lower = vo.lower;
		limit = vo.limit;
		type = vo.type;
	}

	/**
	 * 本类的赋值方法
	 */
	public void setData(UplowValueBean vo) {
		this.copyFrom(vo);
	}

	/**
	 * 本类的取值方法
	 */
	public UplowValueBean getData() {
		UplowValueBean result = new UplowValueBean();
		result.copyFrom(this);
		return result;
	}

	/**
	 * upper的赋值方法
	 */
	public void setUpper(Float upper) {
		this.upper = upper;
	}

	/**
	 * lower的赋值方法
	 */
	public void setLower(Float lower) {
		this.lower = lower;
	}

	/**
	 * "limit"的赋值方法
	 */
	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	/**
	 * type的赋值方法
	 */
	public void setType(Integer type) {
		this.type = type;
	}

	/**
	 * upper的取值方法
	 */
	public Float getUpper() {
		return upper;
	}

	/**
	 * lower的取值方法
	 */
	public Float getLower() {
		return lower;
	}

	/**
	 * "limit"的取值方法
	 */
	public Integer getLimit() {
		return limit;
	}

	/**
	 * type的取值方法
	 */
	public Integer getType() {
		return type;
	}
	/**
	 * upper取值方法
	 */
}