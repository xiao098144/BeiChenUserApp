package com.ddoctor.user.wapi.bean;

import android.os.Parcel;
import android.os.Parcelable;

import com.ddoctor.enums.RecordLayoutType;

/**
 * <p>
 * Title: Value code
 * </p>
 * <p>
 * Description: data view
 * </p>
 * <p>
 * Copyright: Copyright (c) 2007
 * </p>
 * <p>
 * Company: ddoctor
 * </p>
 * 
 * @author tian jiale
 * @version 1.0
 */

public class SportBean implements Parcelable , Comparable<SportBean>{
	/**
	 * 定义本类的私有变量
	 */
	private Integer id;
	private Integer step;
	private Integer type;
	private Integer distance;
	private Integer kcal;
	private String date;
	private String time;
	private Integer patientId;
	
	private String recorddate;
	private RecordLayoutType layoutType = RecordLayoutType.TYPE_VALUE;

	/**
	 * 本类的实例化方法
	 */
	public SportBean() {
	}

	/**
	 * 本类的带参数的实例化方法
	 */
	public SportBean(Integer id, Integer step, Integer type, Integer distance,
			Integer kcal, String date, String time, Integer patientId,
			RecordLayoutType layoutType , String recorddate) {
		super();
		this.id = id;
		this.step = step;
		this.type = type;
		this.distance = distance;
		this.kcal = kcal;
		this.date = date;
		this.time = time;
		this.patientId = patientId;
		this.layoutType = layoutType;
		this.recorddate = recorddate;
	}

	/**
	 * 以本类为参数传入时，实例化本类的方法
	 */
	public void copyFrom(SportBean vo) {
		id = vo.id;
		step = vo.step;
		type = vo.type;
		distance = vo.distance;
		kcal = vo.kcal;
		date = vo.date;
		time = vo.time;
		patientId = vo.patientId;
		layoutType = vo.layoutType;
		recorddate = vo.recorddate;
	}

	/**
	 * 本类的赋值方法
	 */
	public void setData(SportBean vo) {
		this.copyFrom(vo);
	}

	/**
	 * 本类的取值方法
	 */
	public SportBean getData() {
		SportBean result = new SportBean();
		result.copyFrom(this);
		return result;
	}

	/**
	 * id的赋值方法
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * step的赋值方法
	 */
	public void setStep(Integer step) {
		this.step = step;
	}

	/**
	 * type的赋值方法
	 */
	public void setType(Integer type) {
		this.type = type;
	}

	/**
	 * distance的赋值方法
	 */
	public void setDistance(Integer distance) {
		this.distance = distance;
	}

	/**
	 * kcal的赋值方法
	 */
	public void setKcal(Integer kcal) {
		this.kcal = kcal;
	}

	/**
	 * date的赋值方法
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * time的赋值方法
	 */
	public void setTime(String time) {
		this.time = time;
	}

	/**
	 * patientId的赋值方法
	 */
	public void setPatientId(Integer patientId) {
		this.patientId = patientId;
	}

	/**
	 * id的取值方法
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * step的取值方法
	 */
	public Integer getStep() {
		return step;
	}

	/**
	 * type的取值方法
	 */
	public Integer getType() {
		return type;
	}

	/**
	 * distance的取值方法
	 */
	public Integer getDistance() {
		return distance;
	}

	/**
	 * kcal的取值方法
	 */
	public Integer getKcal() {
		return kcal;
	}

	/**
	 * date的取值方法
	 */
	public String getDate() {
		return date;
	}

	/**
	 * time的取值方法
	 */
	public String getTime() {
		return time;
	}

	/**
	 * patientId的取值方法
	 */
	public Integer getPatientId() {
		return patientId;
	}

	public void setLayoutType(RecordLayoutType layoutType) {
		this.layoutType = layoutType;
	}

	public RecordLayoutType getLayoutType() {
		return layoutType;
	}

	public void setRecorddate(String recorddate) {
		this.recorddate = recorddate;
	}
	
	public String getRecorddate() {
		return recorddate;
	};
	
	@Override
	public String toString() {
		return "SportBean [id=" + id + ", step=" + step + ", type=" + type
				+ ", distance=" + distance + ", kcal=" + kcal + ", date="
				+ date + ", time=" + time + ", layoutType=" + layoutType + "]";
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeValue(this.id);
		dest.writeValue(this.step);
		dest.writeValue(this.type);
		dest.writeValue(this.distance);
		dest.writeValue(this.kcal);
		dest.writeString(this.date);
		dest.writeString(this.time);
		dest.writeValue(this.patientId);
		dest.writeInt(this.layoutType == RecordLayoutType.TYPE_VALUE ? 1 : 2);
		dest.writeString(this.recorddate);
	}

	protected SportBean(Parcel in) {
		this.id = (Integer) in.readValue(Integer.class.getClassLoader());
		this.step = (Integer) in.readValue(Integer.class.getClassLoader());
		this.type = (Integer) in.readValue(Integer.class.getClassLoader());
		this.distance = (Integer) in.readValue(Integer.class.getClassLoader());
		this.kcal = (Integer) in.readValue(Integer.class.getClassLoader());
		this.date = in.readString();
		this.time = in.readString();
		this.patientId = (Integer) in.readValue(Integer.class.getClassLoader());
		int tmpLayoutType = in.readInt();
		this.layoutType = tmpLayoutType == 1 ? RecordLayoutType.TYPE_VALUE
				: RecordLayoutType.TYPE_CATEGORY;
		this.recorddate = in.readString();
	}

	public static final Parcelable.Creator<SportBean> CREATOR = new Parcelable.Creator<SportBean>() {
		public SportBean createFromParcel(Parcel source) {
			return new SportBean(source);
		}

		public SportBean[] newArray(int size) {
			return new SportBean[size];
		}
	};

	@Override
	public int compareTo(SportBean another) {
		int compare;
		compare = getDate().compareTo(another.getDate());
		return 0 - compare;
	}
}