package com.ddoctor.user.wapi.bean;

/**
 * <p>Title: Value code</p>
 * <p>Description: data view</p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: ddoctor</p>
 * @author tian jiale
 * @version 1.0
 */


public class ClientUpdateBean implements java.io.Serializable {
	/**
	 * 定义本类的私有变量
	 */
	private Integer isUpdate;
	private String serverVersion;
	private String updateUrl;
	private Integer isMust;
	private String remark;

	/**
	 * 本类的实例化方法
	 */
	public ClientUpdateBean() {
	}

	/**
	 * 本类的带参数的实例化方法
	 */
	public ClientUpdateBean(Integer isUpdate, String serverVersion,
			String updateUrl, Integer isMust, String remark) {
		this.isUpdate = isUpdate;
		this.serverVersion = serverVersion;
		this.updateUrl = updateUrl;
		this.isMust = isMust;
		this.remark = remark;
	}

	/**
	 * 以本类为参数传入时，实例化本类的方法
	 */
	public void copyFrom(ClientUpdateBean vo) {
		isUpdate = vo.isUpdate;
		serverVersion = vo.serverVersion;
		updateUrl = vo.updateUrl;
		isMust = vo.isMust;
		remark = vo.remark;
	}

	/**
	 * 本类的赋值方法
	 */
	public void setData(ClientUpdateBean vo) {
		this.copyFrom(vo);
	}

	/**
	 * 本类的取值方法
	 */
	public ClientUpdateBean getData() {
		ClientUpdateBean result = new ClientUpdateBean();
		result.copyFrom(this);
		return result;
	}

	/**
	 * isUpdate的赋值方法
	 */
	public void setIsUpdate(Integer isUpdate) {
		this.isUpdate = isUpdate;
	}

	/**
	 * serverVersion的赋值方法
	 */
	public void setServerVersion(String serverVersion) {
		this.serverVersion = serverVersion;
	}

	/**
	 * updateUrl的赋值方法
	 */
	public void setUpdateUrl(String updateUrl) {
		this.updateUrl = updateUrl;
	}

	/**
	 * isMust的赋值方法
	 */
	public void setIsMust(Integer isMust) {
		this.isMust = isMust;
	}

	/**
	 * remark的赋值方法
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

	/**
	 * isUpdate的取值方法
	 */
	public Integer getIsUpdate() {
		return isUpdate;
	}

	/**
	 * serverVersion的取值方法
	 */
	public String getServerVersion() {
		return serverVersion;
	}

	/**
	 * updateUrl的取值方法
	 */
	public String getUpdateUrl() {
		return updateUrl;
	}

	/**
	 * isMust的取值方法
	 */
	public Integer getIsMust() {
		return isMust;
	}

	/**
	 * remark的取值方法
	 */
	public String getRemark() {
		return remark;
	}
	/**
	 * isUpdate取值方法
	 */
}