package com.ddoctor.user.wapi.bean;

import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;
/**
 * 用药提醒
 * @author 萧
 * @Date 2015-6-9下午9:14:35
 * @TODO TODO
 */
public class MedicalRemindBean implements Parcelable {

	private Integer id;
	private int userid;
	private List<MedicalRecordBean> dataList;
	private String time; // 格式 HH:mm
	private String date; // 格式 yyyy-MM-dd
	private String routing;
	private int parentId;
	private Integer state;
	private String remark;

	public MedicalRemindBean() {
		super();
		// TODO Auto-generated constructor stub
	}

	public MedicalRemindBean(Integer id, int userid,
			List<MedicalRecordBean> dataList, String time, String date,
			String routing, int parentId, Integer state, String remark) {
		super();
		this.id = id;
		this.userid = userid;
		this.dataList = dataList;
		this.time = time;
		this.date = date;
		this.routing = routing;
		this.parentId = parentId;
		this.state = state;
		this.remark = remark;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public List<MedicalRecordBean> getDataList() {
		return dataList;
	}

	public void setDataList(List<MedicalRecordBean> dataList) {
		this.dataList = dataList;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getRouting() {
		return routing;
	}

	public void setRouting(String routing) {
		this.routing = routing;
	}

	public int getParentId() {
		return parentId;
	}

	public void setParentId(int parentId) {
		this.parentId = parentId;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Override
	public String toString() {
		return "MedicalRemindBean [id=" + id + ", userid=" + userid
				+ ", dataList=" + dataList
				+ ", time=" + time + ", date=" + date + ", routing=" + routing
				+ ", parentId=" + parentId + ", state=" + state + ", remark="
				+ remark + "]";
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeValue(this.id);
		dest.writeInt(this.userid);
		dest.writeTypedList(dataList);
		dest.writeString(this.time);
		dest.writeString(this.date);
		dest.writeString(this.routing);
		dest.writeInt(this.parentId);
		dest.writeValue(this.state);
		dest.writeString(this.remark);
	}

	protected MedicalRemindBean(Parcel in) {
		this.id = (Integer) in.readValue(Integer.class.getClassLoader());
		this.userid = in.readInt();
		this.dataList = in.createTypedArrayList(MedicalRecordBean.CREATOR);
		this.time = in.readString();
		this.date = in.readString();
		this.routing = in.readString();
		this.parentId = in.readInt();
		this.state = (Integer) in.readValue(Integer.class.getClassLoader());
		this.remark = in.readString();
	}

	public static final Parcelable.Creator<MedicalRemindBean> CREATOR = new Parcelable.Creator<MedicalRemindBean>() {
		public MedicalRemindBean createFromParcel(Parcel source) {
			return new MedicalRemindBean(source);
		}

		public MedicalRemindBean[] newArray(int size) {
			return new MedicalRemindBean[size];
		}
	};
}
