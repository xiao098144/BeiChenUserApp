package com.ddoctor.user.wapi.bean;

/**
 * <p>Title: Value code</p>
 * <p>Description: data view</p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: ddoctor</p>
 * @author tian jiale
 * @version 1.0
 */

public class TslAddressinfoBean implements java.io.Serializable {
/**
*定义本类的私有变量
*/
   private Integer id;
   private String tslUserId;
   private String tslAddressid;
   private String name;
   private String detail;
   private String phone;
   private String postal;
   private String createTime;
/**
*本类的实例化方法
*/
   public TslAddressinfoBean() {
   }
/**
*本类的带参数的实例化方法
*/
   public TslAddressinfoBean(Integer id,String tslUserId,String tslAddressid,String name,String detail,String phone,String postal,String createTime) {
      this.id = id;
      this.tslUserId = tslUserId;
      this.tslAddressid = tslAddressid;
      this.name = name;
      this.detail = detail;
      this.phone = phone;
      this.postal = postal;
      this.createTime = createTime;
   }
/**
*以本类为参数传入时，实例化本类的方法
*/
   public void copyFrom(TslAddressinfoBean vo) {
      id = vo.id;
      tslUserId = vo.tslUserId;
      tslAddressid = vo.tslAddressid;
      name = vo.name;
      detail = vo.detail;
      phone = vo.phone;
      postal = vo.postal;
      createTime = vo.createTime;
   }
/**
*本类的赋值方法
*/
   public void setData(TslAddressinfoBean vo) {
      this.copyFrom(vo);
   }
/**
*本类的取值方法
*/
   public TslAddressinfoBean getData(){
      TslAddressinfoBean result = new TslAddressinfoBean();
      result.copyFrom(this);
      return result;
   }
/**
*id的赋值方法
*/
   public void setId(Integer id){
      this.id = id;
   }
/**
*tslAddressid的赋值方法
*/
   public void setTslAddressid(String tslAddressid){
      this.tslAddressid = tslAddressid;
   }
/**
*name的赋值方法
*/
   public void setName(String name){
      this.name = name;
   }
/**
*detail的赋值方法
*/
   public void setDetail(String detail){
      this.detail = detail;
   }
/**
*phone的赋值方法
*/
   public void setPhone(String phone){
      this.phone = phone;
   }
/**
*postal的赋值方法
*/
   public void setPostal(String postal){
      this.postal = postal;
   }
/**
*createTime的赋值方法
*/
   public void setCreateTime(String createTime){
      this.createTime = createTime;
   }
/**
*id的取值方法
*/
   public Integer getId() {
      return id;
   }
/**
*tslAddressid的取值方法
*/
   public String getTslAddressid() {
      return tslAddressid;
   }
/**
*name的取值方法
*/
   public String getName() {
      return name;
   }
/**
*detail的取值方法
*/
   public String getDetail() {
      return detail;
   }
/**
*phone的取值方法
*/
   public String getPhone() {
      return phone;
   }
/**
*postal的取值方法
*/
   public String getPostal() {
      return postal;
   }
/**
*createTime的取值方法
*/
   public String getCreateTime() {
      return createTime;
   }
/**
*id取值方法
*/
public String getTslUserId() {
	return tslUserId;
}
public void setTslUserId(String tslUserId) {
	this.tslUserId = tslUserId;
}
}