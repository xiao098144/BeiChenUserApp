package com.ddoctor.user.wapi.bean;

public class QuestionjbzlBean {

	/**
	 * 定义本类的私有变量
	 */
	private Integer userId;
	private String date;
	private String name;
	private String mobile;
	private String visitTime;
	private String sex;
	private String age;
	private String marriage;
	private String education;
	private String occupation;
	private String smokeCount;
	private String drinkCount;
	private String tnbsbs;
	private String kfysysj;
	private String ydssysj;
	private String ddbyssj;
	private String tnblesson;
	private String tnbglfc;
	private String tnbglzwjc;
	private String tnbglddbys;
	private String height;
	private String weight;
	private String bmi;
	private String shshya;
	private String shzhya;
	private String sysfpg;
	private String sys2hpg;
	private String syshba1c;
	private String syscr;
	private String systc;
	private String systg;
	private String sysmalb;
	private String sysnsd;
	private String ylhf;
	private String jjfd;
	private String sysfpgResult;
	private String sys2hpgResult;
	private String syshba1cResult;
	private String syscrResult;
	private String systcResult;
	private String systgResult;
	private String sysmalbResult;
	private String sysnsdResult;
	private String zfbl;
	
	/**
	 * 本类的实例化方法
	 */
	public QuestionjbzlBean() {
		
	}
	
	/**
	 * 本类的带参数的实例化方法
	 */
	public QuestionjbzlBean(Integer userId, String date, String name,
			String mobile, String visitTime, String sex, String age,
			String marriage, String education, String occupation,
			String smokeCount, String drinkCount, String tnbsbs,
			String kfysysj, String ydssysj, String ddbyssj, String tnblesson,
			String tnbglfc, String tnbglzwjc, String tnbglddbys, String height,
			String weight, String bmi, String shshya, String shzhya,
			String sysfpg, String sys2hpg, String syshba1c, String syscr,
			String systc, String systg, String sysmalb, String sysnsd,
			String ylhf, String jjfd, String sysfpgResult,
			String sys2hpgResult, String syshba1cResult, String syscrResult,
			String systcResult, String systgResult, String sysmalbResult,
			String sysnsdResult, String zfbl) {
		this.userId = userId;
		this.date = date;
		this.name = name;
		this.mobile = mobile;
		this.visitTime = visitTime;
		this.sex = sex;
		this.age = age;
		this.marriage = marriage;
		this.education = education;
		this.occupation = occupation;
		this.smokeCount = smokeCount;
		this.drinkCount = drinkCount;
		this.tnbsbs = tnbsbs;
		this.kfysysj = kfysysj;
		this.ydssysj = ydssysj;
		this.ddbyssj = ddbyssj;
		this.tnblesson = tnblesson;
		this.tnbglfc = tnbglfc;
		this.tnbglzwjc = tnbglzwjc;
		this.tnbglddbys = tnbglddbys;
		this.height = height;
		this.weight = weight;
		this.bmi = bmi;
		this.shshya = shshya;
		this.shzhya = shzhya;
		this.sysfpg = sysfpg;
		this.sys2hpg = sys2hpg;
		this.syshba1c = syshba1c;
		this.syscr = syscr;
		this.systc = systc;
		this.systg = systg;
		this.sysmalb = sysmalb;
		this.sysnsd = sysnsd;
		this.ylhf = ylhf;
		this.jjfd = jjfd;
		this.sysfpgResult = sysfpgResult;
		this.sys2hpgResult = sys2hpgResult;
		this.syshba1cResult = syshba1cResult;
		this.syscrResult = syscrResult;
		this.systcResult = systcResult;
		this.systgResult = systgResult;
		this.sysmalbResult = sysmalbResult;
		this.sysnsdResult = sysnsdResult;
		this.zfbl = zfbl;
	}

	/**
	 * 以本类为参数传入时，实例化本类的方法
	 */
	public void copyFrom(QuestionjbzlBean vo)
	{
		userId = vo.userId;
		date = vo.date;
		name = vo.name;
		mobile = vo.mobile;
		visitTime = vo.visitTime;
		sex = vo.sex;
		age = vo.age;
		marriage = vo.marriage;
		education = vo.education;
		occupation = vo.occupation;
		smokeCount = vo.smokeCount;
		drinkCount = vo.drinkCount;
		tnbsbs = vo.tnbsbs;
		kfysysj = vo.kfysysj;
		ydssysj = vo.ydssysj;
		ddbyssj = vo.ddbyssj;
		tnblesson = vo.tnblesson;
		tnbglfc = vo.tnbglfc;
		tnbglzwjc = vo.tnbglzwjc;
		tnbglddbys = vo.tnbglddbys;
		height = vo.height;
		weight = vo.weight;
		bmi = vo.bmi;
		shshya = vo.shshya;
		shzhya = vo.shzhya;
		sysfpg = vo.sysfpg;
		sys2hpg = vo.sys2hpg;
		syshba1c = vo.syshba1c;
		syscr = vo.syscr;
		systc = vo.systc;
		systg = vo.systg;
		sysmalb = vo.sysmalb;
		sysnsd = vo.sysnsd;
		ylhf = vo.ylhf;
		jjfd = vo.jjfd;
		sysfpgResult = vo.sysfpgResult;
		sys2hpgResult = vo.sys2hpgResult;
		syshba1cResult = vo.syshba1cResult;
		syscrResult = vo.syscrResult;
		systcResult = vo.systcResult;
		systgResult = vo.systgResult;
		sysmalbResult = vo.sysmalbResult;
		sysnsdResult = vo.sysnsdResult;
		zfbl = vo.zfbl;
	}
	
	
	/**
	 * 本类的赋值方法
	 */
	public void setData(QuestionjbzlBean vo)
	{
		this.copyFrom(vo);
	}
	
	/**
	 * 本类的取值方法
	 */
	public QuestionjbzlBean getData()
	{
		QuestionjbzlBean result = new QuestionjbzlBean();
		result.copyFrom(this);
		return result;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getVisitTime() {
		return visitTime;
	}

	public void setVisitTime(String visitTime) {
		this.visitTime = visitTime;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getMarriage() {
		return marriage;
	}

	public void setMarriage(String marriage) {
		this.marriage = marriage;
	}

	public String getEducation() {
		return education;
	}

	public void setEducation(String education) {
		this.education = education;
	}

	public String getOccupation() {
		return occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	public String getSmokeCount() {
		return smokeCount;
	}

	public void setSmokeCount(String smokeCount) {
		this.smokeCount = smokeCount;
	}

	public String getDrinkCount() {
		return drinkCount;
	}

	public void setDrinkCount(String drinkCount) {
		this.drinkCount = drinkCount;
	}

	public String getTnbsbs() {
		return tnbsbs;
	}

	public void setTnbsbs(String tnbsbs) {
		this.tnbsbs = tnbsbs;
	}

	public String getKfysysj() {
		return kfysysj;
	}

	public void setKfysysj(String kfysysj) {
		this.kfysysj = kfysysj;
	}

	public String getYdssysj() {
		return ydssysj;
	}

	public void setYdssysj(String ydssysj) {
		this.ydssysj = ydssysj;
	}

	public String getDdbyssj() {
		return ddbyssj;
	}

	public void setDdbyssj(String ddbyssj) {
		this.ddbyssj = ddbyssj;
	}

	public String getTnblesson() {
		return tnblesson;
	}

	public void setTnblesson(String tnblesson) {
		this.tnblesson = tnblesson;
	}

	public String getTnbglfc() {
		return tnbglfc;
	}

	public void setTnbglfc(String tnbglfc) {
		this.tnbglfc = tnbglfc;
	}

	public String getTnbglzwjc() {
		return tnbglzwjc;
	}

	public void setTnbglzwjc(String tnbglzwjc) {
		this.tnbglzwjc = tnbglzwjc;
	}

	public String getTnbglddbys() {
		return tnbglddbys;
	}

	public void setTnbglddbys(String tnbglddbys) {
		this.tnbglddbys = tnbglddbys;
	}

	public String getHeight() {
		return height;
	}

	public void setHeight(String height) {
		this.height = height;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	public String getBmi() {
		return bmi;
	}

	public void setBmi(String bmi) {
		this.bmi = bmi;
	}

	public String getShshya() {
		return shshya;
	}

	public void setShshya(String shshya) {
		this.shshya = shshya;
	}

	public String getShzhya() {
		return shzhya;
	}

	public void setShzhya(String shzhya) {
		this.shzhya = shzhya;
	}

	public String getSysfpg() {
		return sysfpg;
	}

	public void setSysfpg(String sysfpg) {
		this.sysfpg = sysfpg;
	}

	public String getSys2hpg() {
		return sys2hpg;
	}

	public void setSys2hpg(String sys2hpg) {
		this.sys2hpg = sys2hpg;
	}

	public String getSyshba1c() {
		return syshba1c;
	}

	public void setSyshba1c(String syshba1c) {
		this.syshba1c = syshba1c;
	}

	public String getSyscr() {
		return syscr;
	}

	public void setSyscr(String syscr) {
		this.syscr = syscr;
	}

	public String getSystc() {
		return systc;
	}

	public void setSystc(String systc) {
		this.systc = systc;
	}

	public String getSystg() {
		return systg;
	}

	public void setSystg(String systg) {
		this.systg = systg;
	}

	public String getSysmalb() {
		return sysmalb;
	}

	public void setSysmalb(String sysmalb) {
		this.sysmalb = sysmalb;
	}

	public String getSysnsd() {
		return sysnsd;
	}

	public void setSysnsd(String sysnsd) {
		this.sysnsd = sysnsd;
	}

	public String getYlhf() {
		return ylhf;
	}

	public void setYlhf(String ylhf) {
		this.ylhf = ylhf;
	}

	public String getJjfd() {
		return jjfd;
	}

	public void setJjfd(String jjfd) {
		this.jjfd = jjfd;
	}

	public String getSysfpgResult() {
		return sysfpgResult;
	}

	public void setSysfpgResult(String sysfpgResult) {
		this.sysfpgResult = sysfpgResult;
	}

	public String getSys2hpgResult() {
		return sys2hpgResult;
	}

	public void setSys2hpgResult(String sys2hpgResult) {
		this.sys2hpgResult = sys2hpgResult;
	}

	public String getSyshba1cResult() {
		return syshba1cResult;
	}

	public void setSyshba1cResult(String syshba1cResult) {
		this.syshba1cResult = syshba1cResult;
	}

	public String getSyscrResult() {
		return syscrResult;
	}

	public void setSyscrResult(String syscrResult) {
		this.syscrResult = syscrResult;
	}

	public String getSystcResult() {
		return systcResult;
	}

	public void setSystcResult(String systcResult) {
		this.systcResult = systcResult;
	}

	public String getSystgResult() {
		return systgResult;
	}

	public void setSystgResult(String systgResult) {
		this.systgResult = systgResult;
	}

	public String getSysmalbResult() {
		return sysmalbResult;
	}

	public void setSysmalbResult(String sysmalbResult) {
		this.sysmalbResult = sysmalbResult;
	}

	public String getSysnsdResult() {
		return sysnsdResult;
	}

	public void setSysnsdResult(String sysnsdResult) {
		this.sysnsdResult = sysnsdResult;
	}

	public String getZfbl() {
		return zfbl;
	}

	public void setZfbl(String zfbl) {
		this.zfbl = zfbl;
	}

	
	
	
}
