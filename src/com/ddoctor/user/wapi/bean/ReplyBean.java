package com.ddoctor.user.wapi.bean;

/**
 * <p>Title: Value code</p>
 * <p>Description: data view</p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: ddoctor</p>
 * @author tian jiale
 * @version 1.0
 */

import android.os.Parcel;
import android.os.Parcelable;

public class ReplyBean implements java.io.Serializable, Parcelable {
	/**
	 * 定义本类的私有变量
	 */
	private Integer id;
	private Integer questionId;
	private Integer doctorId;
	private Integer patientId;
	private String content;
	private String image;
	private String audio;

	/**
	 * 本类的实例化方法
	 */
	public ReplyBean() {
	}

	/**
	 * 本类的带参数的实例化方法
	 */
	public ReplyBean(Integer id, Integer questionId, Integer doctorId,
			Integer patientId, String content, String image, String audio) {
		this.id = id;
		this.questionId = questionId;
		this.doctorId = doctorId;
		this.patientId = patientId;
		this.content = content;
		this.image = image;
		this.audio = audio;
	}

	/**
	 * 以本类为参数传入时，实例化本类的方法
	 */
	public void copyFrom(ReplyBean vo) {
		id = vo.id;
		questionId = vo.questionId;
		doctorId = vo.doctorId;
		patientId = vo.patientId;
		content = vo.content;
		image = vo.image;
		audio = vo.audio;
	}

	/**
	 * 本类的赋值方法
	 */
	public void setData(ReplyBean vo) {
		this.copyFrom(vo);
	}

	/**
	 * 本类的取值方法
	 */
	public ReplyBean getData() {
		ReplyBean result = new ReplyBean();
		result.copyFrom(this);
		return result;
	}

	/**
	 * id的赋值方法
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * questionId的赋值方法
	 */
	public void setQuestionId(Integer questionId) {
		this.questionId = questionId;
	}

	/**
	 * doctorId的赋值方法
	 */
	public void setDoctorId(Integer doctorId) {
		this.doctorId = doctorId;
	}

	/**
	 * patientId的赋值方法
	 */
	public void setPatientId(Integer patientId) {
		this.patientId = patientId;
	}

	/**
	 * content的赋值方法
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * image的赋值方法
	 */
	public void setImage(String image) {
		this.image = image;
	}

	/**
	 * audio的赋值方法
	 */
	public void setAudio(String audio) {
		this.audio = audio;
	}

	/**
	 * id的取值方法
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * questionId的取值方法
	 */
	public Integer getQuestionId() {
		return questionId;
	}

	/**
	 * doctorId的取值方法
	 */
	public Integer getDoctorId() {
		return doctorId;
	}

	/**
	 * patientId的取值方法
	 */
	public Integer getPatientId() {
		return patientId;
	}

	/**
	 * content的取值方法
	 */
	public String getContent() {
		return content;
	}

	/**
	 * image的取值方法
	 */
	public String getImage() {
		return image;
	}

	/**
	 * audio的取值方法
	 */
	public String getAudio() {
		return audio;
	}
	/**
	 * id取值方法
	 */
	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeValue(this.id);
		dest.writeValue(this.questionId);
		dest.writeValue(this.doctorId);
		dest.writeValue(this.patientId);
		dest.writeString(this.content);
		dest.writeString(this.image);
		dest.writeString(this.audio);
	}

	private ReplyBean(Parcel in) {
		this.id = (Integer) in.readValue(Integer.class.getClassLoader());
		this.questionId = (Integer) in.readValue(Integer.class.getClassLoader());
		this.doctorId = (Integer) in.readValue(Integer.class.getClassLoader());
		this.patientId = (Integer) in.readValue(Integer.class.getClassLoader());
		this.content = in.readString();
		this.image = in.readString();
		this.audio = in.readString();
	}

	public static final Parcelable.Creator<ReplyBean> CREATOR = new Parcelable.Creator<ReplyBean>() {
		public ReplyBean createFromParcel(Parcel source) {
			return new ReplyBean(source);
		}

		public ReplyBean[] newArray(int size) {
			return new ReplyBean[size];
		}
	};
}