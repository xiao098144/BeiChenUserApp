package com.ddoctor.user.wapi.bean;

/**
 * <p>Title: Value code</p>
 * <p>Description: data view</p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: ddoctor</p>
 * @author tian jiale
 * @version 1.0
 */

public class AboutBean implements java.io.Serializable {
/**
*定义本类的私有变量
*/
   private Integer id;
   private String info;
   private String copyright;
   private String pointrule;
   private Integer pointrate;
   private String service;
/**
*本类的实例化方法
*/
   public AboutBean() {
   }
/**
*本类的带参数的实例化方法
*/
   public AboutBean(Integer id,String info,String copyright,String pointrule,Integer pointrate,String service) {
      this.id = id;
      this.info = info;
      this.copyright = copyright;
      this.pointrule = pointrule;
      this.pointrate = pointrate;
      this.service = service;
   }
/**
*以本类为参数传入时，实例化本类的方法
*/
   public void copyFrom(AboutBean vo) {
      id = vo.id;
      info = vo.info;
      copyright = vo.copyright;
      pointrule = vo.pointrule;
      pointrate = vo.pointrate;
      service = vo.service;
   }
/**
*本类的赋值方法
*/
   public void setData(AboutBean vo) {
      this.copyFrom(vo);
   }
/**
*本类的取值方法
*/
   public AboutBean getData(){
      AboutBean result = new AboutBean();
      result.copyFrom(this);
      return result;
   }
/**
*id的赋值方法
*/
   public void setId(Integer id){
      this.id = id;
   }
/**
*info的赋值方法
*/
   public void setInfo(String info){
      this.info = info;
   }
/**
*copyright的赋值方法
*/
   public void setCopyright(String copyright){
      this.copyright = copyright;
   }
/**
*pointrule的赋值方法
*/
   public void setPointrule(String pointrule){
      this.pointrule = pointrule;
   }
/**
*pointrate的赋值方法
*/
   public void setPointrate(Integer pointrate){
      this.pointrate = pointrate;
   }
/**
*service的赋值方法
*/
   public void setService(String service){
      this.service = service;
   }
/**
*id的取值方法
*/
   public Integer getId() {
      return id;
   }
/**
*info的取值方法
*/
   public String getInfo() {
      return info;
   }
/**
*copyright的取值方法
*/
   public String getCopyright() {
      return copyright;
   }
/**
*pointrule的取值方法
*/
   public String getPointrule() {
      return pointrule;
   }
/**
*pointrate的取值方法
*/
   public Integer getPointrate() {
      return pointrate;
   }
/**
*service的取值方法
*/
   public String getService() {
      return service;
   }
/**
*id取值方法
*/
}