package com.ddoctor.user.wapi.bean;

/**
 * <p>Title: Value code</p>
 * <p>Description: data view</p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: ddoctor</p>
 * @author tian jiale
 * @version 1.0
 */


public class LevelBean implements java.io.Serializable {
	/**
	 * 定义本类的私有变量
	 */
	private Integer id;
	private String name;
	private Integer recommend;

	/**
	 * 本类的实例化方法
	 */
	public LevelBean() {
	}

	/**
	 * 本类的带参数的实例化方法
	 */
	public LevelBean(Integer id, String name, Integer recommend) {
		this.id = id;
		this.name = name;
		this.recommend = recommend;
	}

	/**
	 * 以本类为参数传入时，实例化本类的方法
	 */
	public void copyFrom(LevelBean vo) {
		id = vo.id;
		name = vo.name;
		recommend = vo.recommend;
	}

	/**
	 * 本类的赋值方法
	 */
	public void setData(LevelBean vo) {
		this.copyFrom(vo);
	}

	/**
	 * 本类的取值方法
	 */
	public LevelBean getData() {
		LevelBean result = new LevelBean();
		result.copyFrom(this);
		return result;
	}

	/**
	 * id的赋值方法
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * name的赋值方法
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * recommend的赋值方法
	 */
	public void setRecommend(Integer recommend) {
		this.recommend = recommend;
	}

	/**
	 * id的取值方法
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * name的取值方法
	 */
	public String getName() {
		return name;
	}

	/**
	 * recommend的取值方法
	 */
	public Integer getRecommend() {
		return recommend;
	}
	/**
	 * id取值方法
	 */
}