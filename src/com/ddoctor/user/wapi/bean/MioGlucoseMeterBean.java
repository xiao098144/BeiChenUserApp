package com.ddoctor.user.wapi.bean;

import android.os.Parcel;
import android.os.Parcelable;

public class MioGlucoseMeterBean implements Parcelable {

	/**
	 * 定义本类的私有变量
	 */
	private Integer id;
	private String cname;
	private String ename;
	private String glucoseMeterId;
	private String priority;
	private String algorithm;
	private String glucoseMeterImg;

	/**
	 * 本类的实例化方法
	 */
	public MioGlucoseMeterBean() {
	}

	/**
	 * 本类的带参数的实例化方法
	 */
	public MioGlucoseMeterBean(Integer id, String cname, String ename,
			String glucoseMeterId, String priority, String algorithm , String glucoseMeterImg) {
		this.id = id;
		this.cname = cname;
		this.ename = ename;
		this.glucoseMeterId = glucoseMeterId;
		this.priority = priority;
		this.algorithm = algorithm;
		this.glucoseMeterImg = glucoseMeterImg;
	}

	/**
	 * 以本类为参数传入时，实例化本类的方法
	 */
	public void copyFrom(MioGlucoseMeterBean vo) {
		id = vo.id;
		cname = vo.cname;
		ename = vo.ename;
		glucoseMeterId = vo.glucoseMeterId;
		priority = vo.priority;
		algorithm = vo.algorithm;
		glucoseMeterImg = glucoseMeterImg;
	}

	/**
	 * 本类的赋值方法
	 */
	public void setData(MioGlucoseMeterBean vo) {
		this.copyFrom(vo);
	}

	/**
	 * 本类的取值方法
	 */
	public MioGlucoseMeterBean getData() {
		MioGlucoseMeterBean result = new MioGlucoseMeterBean();
		result.copyFrom(this);
		return result;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEname() {
		return ename;
	}

	public void setEname(String ename) {
		this.ename = ename;
	}

	public String getCname() {
		return cname;
	}

	public void setCname(String cname) {
		this.cname = cname;
	}

	public String getGlucoseMeterId() {
		return glucoseMeterId;
	}

	public void setGlucoseMeterId(String glucoseMeterId) {
		this.glucoseMeterId = glucoseMeterId;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getAlgorithm() {
		return algorithm;
	}

	public void setAlgorithm(String algorithm) {
		this.algorithm = algorithm;
	}

	public String getGlucoseMeterImg() {
		return glucoseMeterImg;
	}
	
	public void setGlucoseMeterImg(String glucoseMeterImg) {
		this.glucoseMeterImg = glucoseMeterImg;
	}
	
	@Override
	public String toString() {
		return "MioGlucoseMeterBean [id=" + id + ", ename=" + ename
				+ ", cname=" + cname + ", glucoseMeterId=" + glucoseMeterId
				+ ", priority=" + priority + ", algorithm=" + algorithm + "]";
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeValue(this.id);
		dest.writeString(this.ename);
		dest.writeString(this.cname);
		dest.writeString(this.glucoseMeterId);
		dest.writeString(this.priority);
		dest.writeString(this.algorithm);
		dest.writeString(this.glucoseMeterImg);
	}

	protected MioGlucoseMeterBean(Parcel in) {
		this.id = (Integer) in.readValue(Integer.class.getClassLoader());
		this.ename = in.readString();
		this.cname = in.readString();
		this.glucoseMeterId = in.readString();
		this.priority = in.readString();
		this.algorithm = in.readString();
		this.glucoseMeterImg = in.readString();
	}

	public static final Parcelable.Creator<MioGlucoseMeterBean> CREATOR = new Parcelable.Creator<MioGlucoseMeterBean>() {
		public MioGlucoseMeterBean createFromParcel(Parcel source) {
			return new MioGlucoseMeterBean(source);
		}

		public MioGlucoseMeterBean[] newArray(int size) {
			return new MioGlucoseMeterBean[size];
		}
	};
}
