package com.ddoctor.user.wapi.bean;

/**
 * <p>Title: Value code</p>
 * <p>Description: data view</p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: ddoctor</p>
 * @author tian jiale
 * @version 1.0
 */

import java.util.List;

public class DistrictBean implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2918129249560229942L;
	/**
	 * 定义本类的私有变量
	 */
	private Integer id;
	private String parentId;
	private String name;
	private List<DistrictBean> citys;
	private List<DistrictBean> areas;

	/**
	 * 本类的实例化方法
	 */
	public DistrictBean() {
	}

	/**
	 * 本类的带参数的实例化方法
	 */
	public DistrictBean(Integer id, String parentId, String name) {
		this.id = id;
		this.parentId = parentId;
		this.name = name;
	}

	/**
	 * 以本类为参数传入时，实例化本类的方法
	 */
	public void copyFrom(DistrictBean vo) {
		id = vo.id;
		parentId = vo.parentId;
		name = vo.name;
	}

	/**
	 * 本类的赋值方法
	 */
	public void setData(DistrictBean vo) {
		this.copyFrom(vo);
	}

	/**
	 * 本类的取值方法
	 */
	public DistrictBean getData() {
		DistrictBean result = new DistrictBean();
		result.copyFrom(this);
		return result;
	}

	/**
	 * id的赋值方法
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * parentId的赋值方法
	 */
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	/**
	 * name的赋值方法
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * citys的赋值方法
	 */
	public void setCitys(List<DistrictBean> citys) {
		this.citys = citys;
	}

	/**
	 * areas的赋值方法
	 */
	public void setAreas(List<DistrictBean> areas) {
		this.areas = areas;
	}

	/**
	 * id的取值方法
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * parentId的取值方法
	 */
	public String getParentId() {
		return parentId;
	}

	/**
	 * name的取值方法
	 */
	public String getName() {
		return name;
	}

	/**
	 * citys的取值方法
	 */
	public List<DistrictBean> getCitys() {
		return citys;
	}

	/**
	 * areas的取值方法
	 */
	public List<DistrictBean> getAreas() {
		return areas;
	}

	@Override
	public String toString() {
		return "DistrictBean [id=" + id + ", parentId=" + parentId + ", name="
				+ name + ", citys=" + citys + ", areas=" + areas + "]";
	}
	
}