package com.ddoctor.user.wapi.bean;

/**
 * <p>Title: Value code</p>
 * <p>Description: data view</p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: ddoctor</p>
 * @author tian jiale
 * @version 1.0
 */

import java.util.List;
public class TslOrderBean implements java.io.Serializable {
/**
	 * 
	 */
	private static final long serialVersionUID = -627666721417543109L;
/**
*定义本类的私有变量
*/
   private Integer id;
   private String tslOrderId;
   private String tslOrderNo;
   private String tslUserId;
   private Float bloodsugar1;
   private Float bloodsugar2;
   private String shouyizhang;
   private String ercibaoxiao;
   private Float gerenprice;
   private Float shebaoprice;
   private Float price;
   private String address;
   private String name;
   private String postal;
   private String phone;
   private int state;
   private String recordTime;
   private String peisongDate;
   private String createTime;
   private List<TslProductBean> productList;
/**
*本类的实例化方法
*/
   public TslOrderBean() {
   }
/**
*本类的带参数的实例化方法
*/
   public TslOrderBean(Integer id,String tslOrderId,String tslOrderNo,String tslUserId,Float bloodsugar1,Float bloodsugar2,
		   String shouyizhang,String ercibaoxiao,Float gerenprice,Float shebaoprice,Float price,String address,String name,
		   String postal,String phone,int state,
		   String recordTime,String peisongDate,String createTime, List<TslProductBean> productList) {
      this.id = id;
      this.tslOrderId = tslOrderId;
      this.tslOrderNo = tslOrderNo;
      this.tslUserId = tslUserId;
      this.bloodsugar1 = bloodsugar1;
      this.bloodsugar2 = bloodsugar2;
      this.shouyizhang = shouyizhang;
      this.ercibaoxiao = ercibaoxiao;
      this.gerenprice = gerenprice;
      this.shebaoprice = shebaoprice;
      this.price = price;
      this.address = address;
      this.name = name;
      this.postal = postal;
      this.phone = phone;
      this.state = state;
      this.recordTime = recordTime;
      this.peisongDate = peisongDate;
      this.createTime = createTime;
      this.productList = productList;
   }
/**
*以本类为参数传入时，实例化本类的方法
*/
   public void copyFrom(TslOrderBean vo) {
      id = vo.id;
      tslOrderId = vo.tslOrderId;
      tslOrderNo = vo.tslOrderNo;
      tslUserId = vo.tslUserId;
      bloodsugar1 = vo.bloodsugar1;
      bloodsugar2 = vo.bloodsugar2;
      shouyizhang = vo.shouyizhang;
      ercibaoxiao = vo.ercibaoxiao;
      gerenprice = vo.gerenprice;
      shebaoprice = vo.shebaoprice;
      price = vo.price;
      address = vo.address;
      name = vo.name;
      postal = vo.postal;
      phone = vo.phone;
      state = vo.state;
      recordTime = vo.recordTime;
      peisongDate = vo.peisongDate;
      createTime = vo.createTime;
      productList = vo.productList;
   }
/**
*本类的赋值方法
*/
   public void setData(TslOrderBean vo) {
      this.copyFrom(vo);
   }
/**
*本类的取值方法
*/
   public TslOrderBean getData(){
      TslOrderBean result = new TslOrderBean();
      result.copyFrom(this);
      return result;
   }
/**
*id的赋值方法
*/
   public void setId(Integer id){
      this.id = id;
   }
/**
*tslOrderId的赋值方法
*/
   public void setTslOrderId(String tslOrderId){
      this.tslOrderId = tslOrderId;
   }
   
   public void setTslOrderNo(String tslOrderNo){
	      this.tslOrderNo = tslOrderNo;
	   }
   
/**
*tslUserId的赋值方法
*/
   public void setTslUserId(String tslUserId){
      this.tslUserId = tslUserId;
   }
/**
*bloodsugar1的赋值方法
*/
   public void setBloodsugar1(Float bloodsugar1){
      this.bloodsugar1 = bloodsugar1;
   }
/**
*bloodsugar2的赋值方法
*/
   public void setBloodsugar2(Float bloodsugar2){
      this.bloodsugar2 = bloodsugar2;
   }
/**
*shouyizhang的赋值方法
*/
   public void setShouyizhang(String shouyizhang){
      this.shouyizhang = shouyizhang;
   }
/**
*ercibaoxiao的赋值方法
*/
   public void setErcibaoxiao(String ercibaoxiao){
      this.ercibaoxiao = ercibaoxiao;
   }
/**
*gerenprice的赋值方法
*/
   public void setGerenprice(Float gerenprice){
      this.gerenprice = gerenprice;
   }
/**
*shebaoprice的赋值方法
*/
   public void setShebaoprice(Float shebaoprice){
      this.shebaoprice = shebaoprice;
   }
/**
*price的赋值方法
*/
   public void setPrice(Float price){
      this.price = price;
   }
/**
*address的赋值方法
*/
   public void setAddress(String address){
      this.address = address;
   }
/**
*name的赋值方法
*/
   public void setName(String name){
      this.name = name;
   }
/**
*postal的赋值方法
*/
   public void setPostal(String postal){
      this.postal = postal;
   }
/**
*phone的赋值方法
*/
   public void setPhone(String phone){
      this.phone = phone;
   }
/**
*state的赋值方法
*/
   public void setState(int state){
      this.state = state;
   }
/**
*recordTime的赋值方法
*/
   public void setRecordTime(String recordTime){
      this.recordTime = recordTime;
   }
/**
*peisongDate的赋值方法
*/
   public void setPeisongDate(String peisongDate){
      this.peisongDate = peisongDate;
   }
/**
*createTime的赋值方法
*/
   public void setCreateTime(String createTime){
      this.createTime = createTime;
   }
/**
*id的取值方法
*/
   public Integer getId() {
      return id;
   }
/**
*tslOrderId的取值方法
*/
   public String getTslOrderId() {
      return tslOrderId;
   }
   public String getTslOrderNo() {
	      return tslOrderNo;
	   }
/**
*tslUserId的取值方法
*/
   public String getTslUserId() {
      return tslUserId;
   }
/**
*bloodsugar1的取值方法
*/
   public Float getBloodsugar1() {
      return bloodsugar1;
   }
/**
*bloodsugar2的取值方法
*/
   public Float getBloodsugar2() {
      return bloodsugar2;
   }
/**
*shouyizhang的取值方法
*/
   public String getShouyizhang() {
      return shouyizhang;
   }
/**
*ercibaoxiao的取值方法
*/
   public String getErcibaoxiao() {
      return ercibaoxiao;
   }
/**
*gerenprice的取值方法
*/
   public Float getGerenprice() {
      return gerenprice;
   }
/**
*shebaoprice的取值方法
*/
   public Float getShebaoprice() {
      return shebaoprice;
   }
/**
*price的取值方法
*/
   public Float getPrice() {
      return price;
   }
/**
*address的取值方法
*/
   public String getAddress() {
      return address;
   }
/**
*name的取值方法
*/
   public String getName() {
      return name;
   }
/**
*postal的取值方法
*/
   public String getPostal() {
      return postal;
   }
/**
*phone的取值方法
*/
   public String getPhone() {
      return phone;
   }
/**
*state的取值方法
*/
   public int getState() {
      return state;
   }
/**
*recordTime的取值方法
*/
   public String getRecordTime() {
      return recordTime;
   }
/**
*peisongDate的取值方法
*/
   public String getPeisongDate() {
      return peisongDate;
   }
/**
*createTime的取值方法
*/
   public String getCreateTime() {
      return createTime;
   }
/**
*id取值方法
*/
public List<TslProductBean> getProductList() {
	return productList;
}
public void setProductList(List<TslProductBean> productList) {
	this.productList = productList;
}
}