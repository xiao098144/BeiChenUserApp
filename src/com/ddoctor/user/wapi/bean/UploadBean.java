package com.ddoctor.user.wapi.bean;

/**
 * <p>Title: Value code</p>
 * <p>Description: data view</p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: ddoctor</p>
 * @author tian jiale
 * @version 1.0
 */


public class UploadBean implements java.io.Serializable {
	/**
	 * 定义本类的私有变量
	 */
	private String file;
	private String fileType;
	private Integer type;

	/**
	 * 本类的实例化方法
	 */
	public UploadBean() {
	}

	/**
	 * 本类的带参数的实例化方法
	 */
	public UploadBean(String file, String fileType, Integer type) {
		this.file = file;
		this.fileType = fileType;
		this.type = type;
	}

	/**
	 * 以本类为参数传入时，实例化本类的方法
	 */
	public void copyFrom(UploadBean vo) {
		file = vo.file;
		fileType = vo.fileType;
		type = vo.type;
	}

	/**
	 * 本类的赋值方法
	 */
	public void setData(UploadBean vo) {
		this.copyFrom(vo);
	}

	/**
	 * 本类的取值方法
	 */
	public UploadBean getData() {
		UploadBean result = new UploadBean();
		result.copyFrom(this);
		return result;
	}

	/**
	 * file的赋值方法
	 */
	public void setFile(String file) {
		this.file = file;
	}

	/**
	 * fileType的赋值方法
	 */
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	/**
	 * type的赋值方法
	 */
	public void setType(Integer type) {
		this.type = type;
	}

	/**
	 * file的取值方法
	 */
	public String getFile() {
		return file;
	}

	/**
	 * fileType的取值方法
	 */
	public String getFileType() {
		return fileType;
	}

	/**
	 * type的取值方法
	 */
	public Integer getType() {
		return type;
	}
	/**
	 * file取值方法
	 */
}