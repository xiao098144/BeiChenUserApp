package com.ddoctor.user.wapi.bean;

/**
 * <p>Title: Value code</p>
 * <p>Description: data view</p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: ddoctor</p>
 * @author tian jiale
 * @version 1.0
 */

import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;

public class OrderBean implements Parcelable {
	/**
	 * 定义本类的私有变量
	 */
	private Integer id;
	private Integer patientId;
	private String orderNO;
	private String time;
	private Integer payStatus;
	private Integer orderStatus;
	private String message;
	private String fullname;
	private Float totalPrice;
	private Float totalPayPrice;
	private Integer totalCount;
	private Float totalDiscount;
	private Integer voucherId;
	private Integer payMethod;
	private DeliverBean deliver;
	private ExpressBean express;
	private List<ProductBean> products;

	/**
	 * 本类的实例化方法
	 */
	public OrderBean() {
	}

	/**
	 * 本类的带参数的实例化方法
	 */
	public OrderBean(Integer id,Integer patientId,String orderNO, String time, Integer payStatus,
					 Integer orderStatus, String message, String fullname,
					 Float totalPrice, Float totalPayPrice, Integer totalCount,
					 Float totalDiscount, Integer voucherId, Integer payMethod,DeliverBean deliver,
					 ExpressBean express, List<ProductBean> products) {
		this.id = id;
		this.patientId = patientId;
		this.orderNO = orderNO;
		this.time = time;
		this.payStatus = payStatus;
		this.orderStatus = orderStatus;
		this.message = message;
		this.fullname = fullname;
		this.totalPrice = totalPrice;
		this.totalPayPrice = totalPayPrice;
		this.totalCount = totalCount;
		this.totalDiscount = totalDiscount;
		this.voucherId = voucherId;
		this.payMethod = payMethod;
		this.deliver = deliver;
		this.express = express;
		this.products = products;
	}

	/**
	 * 以本类为参数传入时，实例化本类的方法
	 */
	public void copyFrom(OrderBean vo) {
		id = vo.id;
		patientId = vo.patientId;
		orderNO = vo.orderNO;
		time = vo.time;
		payStatus = vo.payStatus;
		orderStatus = vo.orderStatus;
		message = vo.message;
		fullname = vo.fullname;
		totalPrice = vo.totalPrice;
		totalPayPrice = vo.totalPayPrice;
		totalCount = vo.totalCount;
		totalDiscount = vo.totalDiscount;
		voucherId = vo.voucherId;
		payMethod = vo.payMethod;
		deliver = vo.deliver;
		express = vo.express;
		products = vo.products;
	}

	/**
	 * 本类的赋值方法
	 */
	public void setData(OrderBean vo) {
		this.copyFrom(vo);
	}

	/**
	 * 本类的取值方法
	 */
	public OrderBean getData() {
		OrderBean result = new OrderBean();
		result.copyFrom(this);
		return result;
	}

	/**
	 * id的赋值方法
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * time的赋值方法
	 */
	public void setPatientId(Integer patientId) {
		this.patientId = patientId;
	}

	/**
	 * time的赋值方法
	 */
	public void setOrderNO(String orderNO) {
		this.orderNO = orderNO;
	}

	/**
	 * time的赋值方法
	 */
	public void setTime(String time) {
		this.time = time;
	}

	/**
	 * payStatus的赋值方法
	 */
	public void setPayStatus(Integer payStatus) {
		this.payStatus = payStatus;
	}

	/**
	 * orderStatus的赋值方法
	 */
	public void setOrderStatus(Integer orderStatus) {
		this.orderStatus = orderStatus;
	}

	/**
	 * message的赋值方法
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * fullname的赋值方法
	 */
	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	/**
	 * totalPrice的赋值方法
	 */
	public void setTotalPrice(Float totalPrice) {
		this.totalPrice = totalPrice;
	}

	/**
	 * totalPayPrice的赋值方法
	 */
	public void setTotalPayPrice(Float totalPayPrice) {
		this.totalPayPrice = totalPayPrice;
	}

	/**
	 * totalCount的赋值方法
	 */
	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

	/**
	 * totalDiscount的赋值方法
	 */
	public void setTotalDiscount(Float totalDiscount) {
		this.totalDiscount = totalDiscount;
	}

	/**
	 * voucherId的赋值方法
	 */
	public void setVoucherId(Integer voucherId) {
		this.voucherId = voucherId;
	}

	/**
	 * deliver的赋值方法
	 */
	public void setDeliver(DeliverBean deliver) {
		this.deliver = deliver;
	}

	/**
	 * express的赋值方法
	 */
	public void setExpress(ExpressBean express) {
		this.express = express;
	}

	/**
	 * products的赋值方法
	 */
	public void setProducts(List<ProductBean> products) {
		this.products = products;
	}

	/**
	 * products的赋值方法
	 */
	public void setPayMethod(Integer payMethod) {
		this.payMethod = payMethod;
	}

	/**
	 * id的取值方法
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * time的取值方法
	 */
	public Integer getPatientId() {
		return patientId;
	}

	/**
	 * time的取值方法
	 */
	public String getOrderNO() {
		return orderNO;
	}

	/**
	 * time的取值方法
	 */
	public String getTime() {
		return time;
	}

	/**
	 * payStatus的取值方法
	 */
	public Integer getPayStatus() {
		return payStatus;
	}

	/**
	 * orderStatus的取值方法
	 */
	public Integer getOrderStatus() {
		return orderStatus;
	}

	/**
	 * message的取值方法
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * fullname的取值方法
	 */
	public String getFullname() {
		return fullname;
	}

	/**
	 * totalPrice的取值方法
	 */
	public Float getTotalPrice() {
		return totalPrice;
	}

	/**
	 * totalPayPrice的取值方法
	 */
	public Float getTotalPayPrice() {
		return totalPayPrice;
	}

	/**
	 * totalCount的取值方法
	 */
	public Integer getTotalCount() {
		return totalCount;
	}

	/**
	 * totalDiscount的取值方法
	 */
	public Float getTotalDiscount() {
		return totalDiscount;
	}

	/**
	 * voucherId的取值方法
	 */
	public Integer getVoucherId() {
		return voucherId;
	}

	/**
	 * voucherId的取值方法
	 */
	public Integer getPayMethod() {
		return payMethod;
	}

	/**
	 * deliver的取值方法
	 */
	public DeliverBean getDeliver() {
		return deliver;
	}

	/**
	 * express的取值方法
	 */
	public ExpressBean getExpress() {
		return express;
	}

	/**
	 * products的取值方法
	 */
	public List<ProductBean> getProducts() {
		return products;
	}
	
	
	
	@Override
	public String toString() {
		return "OrderBean [id=" + id + ", patientId=" + patientId
				+ ", orderNO=" + orderNO + ", time=" + time + ", payStatus="
				+ payStatus + ", orderStatus=" + orderStatus + ", message="
				+ message + ", fullname=" + fullname + ", totalPrice="
				+ totalPrice + ", totalPayPrice=" + totalPayPrice
				+ ", totalCount=" + totalCount + ", totalDiscount="
				+ totalDiscount + ", voucherId=" + voucherId + ", payMethod="
				+ payMethod + ", deliver=" + deliver + ", express=" + express
				+ ", products=" + products + "]";
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeValue(this.id);
		dest.writeValue(this.patientId);
		dest.writeString(this.orderNO);
		dest.writeString(this.time);
		dest.writeValue(this.payStatus);
		dest.writeValue(this.orderStatus);
		dest.writeString(this.message);
		dest.writeString(this.fullname);
		dest.writeValue(this.totalPrice);
		dest.writeValue(this.totalPayPrice);
		dest.writeValue(this.totalCount);
		dest.writeValue(this.totalDiscount);
		dest.writeValue(this.voucherId);
		dest.writeValue(this.payMethod);
		dest.writeSerializable(this.deliver);
		dest.writeSerializable(this.express);
		dest.writeTypedList(products);
	}

	protected OrderBean(Parcel in) {
		this.id = (Integer) in.readValue(Integer.class.getClassLoader());
		this.patientId = (Integer) in.readValue(Integer.class.getClassLoader());
		this.orderNO = in.readString();
		this.time = in.readString();
		this.payStatus = (Integer) in.readValue(Integer.class.getClassLoader());
		this.orderStatus = (Integer) in.readValue(Integer.class.getClassLoader());
		this.message = in.readString();
		this.fullname = in.readString();
		this.totalPrice = (Float) in.readValue(Float.class.getClassLoader());
		this.totalPayPrice = (Float) in.readValue(Float.class.getClassLoader());
		this.totalCount = (Integer) in.readValue(Integer.class.getClassLoader());
		this.totalDiscount = (Float) in.readValue(Float.class.getClassLoader());
		this.voucherId = (Integer) in.readValue(Integer.class.getClassLoader());
		this.payMethod = (Integer) in.readValue(Integer.class.getClassLoader());
		this.deliver = (DeliverBean) in.readSerializable();
		this.express = (ExpressBean) in.readSerializable();
		this.products = in.createTypedArrayList(ProductBean.CREATOR);
	}

	public static final Parcelable.Creator<OrderBean> CREATOR = new Parcelable.Creator<OrderBean>() {
		public OrderBean createFromParcel(Parcel source) {
			return new OrderBean(source);
		}

		public OrderBean[] newArray(int size) {
			return new OrderBean[size];
		}
	};
}