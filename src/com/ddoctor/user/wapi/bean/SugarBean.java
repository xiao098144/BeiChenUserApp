package com.ddoctor.user.wapi.bean;

/**
 * <p>Title: 日期血糖记录</p>
 * <p>Description: data view</p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: ddoctor</p>
 * @author tian jiale
 * @version 1.0
 */

import android.os.Parcel;
import android.os.Parcelable;

import com.ddoctor.enums.RecordLayoutType;


public class SugarBean implements Parcelable {
	/**
	 * 定义本类的私有变量
	 */
	private Integer tag;
	private String date;
	private Integer id;
	private SugarValueBean midNight;
	private SugarValueBean beforeBreakfast;
	private SugarValueBean afterBreakfast;
	private SugarValueBean beforeLunch;
	private SugarValueBean afterLunch;
	private SugarValueBean beforeDinner;
	private SugarValueBean afterDinner;
	private SugarValueBean sleepRecord;
	
	private RecordLayoutType layoutType = RecordLayoutType.TYPE_VALUE;

	/**
	 * 本类的实例化方法
	 */
	public SugarBean() {
	}

	/**
	 * 本类的带参数的实例化方法
	 */
	public SugarBean(Integer tag, String date, Integer id,
			SugarValueBean midNight, SugarValueBean beforeBreakfast,
			SugarValueBean afterBreakfast, SugarValueBean beforeLunch,
			SugarValueBean afterLunch, SugarValueBean beforeDinner,
			SugarValueBean afterDinner, SugarValueBean sleepRecord) {
		this.tag = tag;
		this.date = date;
		this.id = id;
		this.midNight = midNight;
		this.beforeBreakfast = beforeBreakfast;
		this.afterBreakfast = afterBreakfast;
		this.beforeLunch = beforeLunch;
		this.afterLunch = afterLunch;
		this.beforeDinner = beforeDinner;
		this.afterDinner = afterDinner;
		this.sleepRecord = sleepRecord;
	}

	/**
	 * 以本类为参数传入时，实例化本类的方法
	 */
	public void copyFrom(SugarBean vo) {
		tag = vo.tag;
		date = vo.date;
		id = vo.id;
		midNight = vo.midNight;
		beforeBreakfast = vo.beforeBreakfast;
		afterBreakfast = vo.afterBreakfast;
		beforeLunch = vo.beforeLunch;
		afterLunch = vo.afterLunch;
		beforeDinner = vo.beforeDinner;
		afterDinner = vo.afterDinner;
		sleepRecord = vo.sleepRecord;
	}

	/**
	 * 本类的赋值方法
	 */
	public void setData(SugarBean vo) {
		this.copyFrom(vo);
	}

	/**
	 * 本类的取值方法
	 */
	public SugarBean getData() {
		SugarBean result = new SugarBean();
		result.copyFrom(this);
		return result;
	}

	/**
	 * tag的赋值方法
	 */
	public void setTag(Integer tag) {
		this.tag = tag;
	}

	/**
	 * date的赋值方法
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * id的赋值方法
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * midNight的赋值方法
	 */
	public void setMidNight(SugarValueBean midNight) {
		this.midNight = midNight;
	}

	/**
	 * beforeBreakfast的赋值方法
	 */
	public void setBeforeBreakfast(SugarValueBean beforeBreakfast) {
		this.beforeBreakfast = beforeBreakfast;
	}

	/**
	 * afterBreakfast的赋值方法
	 */
	public void setAfterBreakfast(SugarValueBean afterBreakfast) {
		this.afterBreakfast = afterBreakfast;
	}

	/**
	 * beforeLunch的赋值方法
	 */
	public void setBeforeLunch(SugarValueBean beforeLunch) {
		this.beforeLunch = beforeLunch;
	}

	/**
	 * afterLunch的赋值方法
	 */
	public void setAfterLunch(SugarValueBean afterLunch) {
		this.afterLunch = afterLunch;
	}

	/**
	 * beforeDinner的赋值方法
	 */
	public void setBeforeDinner(SugarValueBean beforeDinner) {
		this.beforeDinner = beforeDinner;
	}

	/**
	 * afterDinner的赋值方法
	 */
	public void setAfterDinner(SugarValueBean afterDinner) {
		this.afterDinner = afterDinner;
	}

	/**
	 * sleepRecord的赋值方法
	 */
	public void setSleepRecord(SugarValueBean sleepRecord) {
		this.sleepRecord = sleepRecord;
	}

	/**
	 * tag的取值方法
	 */
	public Integer getTag() {
		return tag;
	}

	/**
	 * date的取值方法
	 */
	public String getDate() {
		return date;
	}

	/**
	 * id的取值方法
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * midNight的取值方法
	 */
	public SugarValueBean getMidNight() {
		return midNight;
	}

	/**
	 * beforeBreakfast的取值方法
	 */
	public SugarValueBean getBeforeBreakfast() {
		return beforeBreakfast;
	}

	/**
	 * afterBreakfast的取值方法
	 */
	public SugarValueBean getAfterBreakfast() {
		return afterBreakfast;
	}

	/**
	 * beforeLunch的取值方法
	 */
	public SugarValueBean getBeforeLunch() {
		return beforeLunch;
	}

	/**
	 * afterLunch的取值方法
	 */
	public SugarValueBean getAfterLunch() {
		return afterLunch;
	}

	/**
	 * beforeDinner的取值方法
	 */
	public SugarValueBean getBeforeDinner() {
		return beforeDinner;
	}

	/**
	 * afterDinner的取值方法
	 */
	public SugarValueBean getAfterDinner() {
		return afterDinner;
	}

	/**
	 * sleepRecord的取值方法
	 */
	public SugarValueBean getSleepRecord() {
		return sleepRecord;
	}
	
	public void setLayoutType(RecordLayoutType layoutType) {
		this.layoutType = layoutType;
	}
	
	public RecordLayoutType getLayoutType() {
		return layoutType;
	}
	
	/**
	 * tag取值方法
	 */
	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeValue(this.tag);
		dest.writeString(this.date);
		dest.writeValue(this.id);
		dest.writeParcelable(this.midNight, 0);
		dest.writeParcelable(this.beforeBreakfast, 0);
		dest.writeParcelable(this.afterBreakfast, 0);
		dest.writeParcelable(this.beforeLunch, 0);
		dest.writeParcelable(this.afterLunch, 0);
		dest.writeParcelable(this.beforeDinner, 0);
		dest.writeParcelable(this.afterDinner, 0);
		dest.writeParcelable(this.sleepRecord, 0);
	}

	private SugarBean(Parcel in) {
		this.tag = (Integer) in.readValue(Integer.class.getClassLoader());
		this.date = in.readString();
		this.id = (Integer) in.readValue(Integer.class.getClassLoader());
		this.midNight = in.readParcelable(SugarValueBean.class.getClassLoader());
		this.beforeBreakfast = in.readParcelable(SugarValueBean.class.getClassLoader());
		this.afterBreakfast = in.readParcelable(SugarValueBean.class.getClassLoader());
		this.beforeLunch = in.readParcelable(SugarValueBean.class.getClassLoader());
		this.afterLunch = in.readParcelable(SugarValueBean.class.getClassLoader());
		this.beforeDinner = in.readParcelable(SugarValueBean.class.getClassLoader());
		this.afterDinner = in.readParcelable(SugarValueBean.class.getClassLoader());
		this.sleepRecord = in.readParcelable(SugarValueBean.class.getClassLoader());
	}

	public static final Parcelable.Creator<SugarBean> CREATOR = new Parcelable.Creator<SugarBean>() {
		public SugarBean createFromParcel(Parcel source) {
			return new SugarBean(source);
		}

		public SugarBean[] newArray(int size) {
			return new SugarBean[size];
		}
	};
}