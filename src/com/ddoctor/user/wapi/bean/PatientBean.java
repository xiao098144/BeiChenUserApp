package com.ddoctor.user.wapi.bean;

/**
 * <p>Title: Value code</p>
 * <p>Description: data view</p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: ddoctor</p>
 * @author tian jiale
 * @version 1.0
 */

import java.util.Calendar;
import java.util.Date;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.ddoctor.utils.TimeUtil;

public class PatientBean implements Parcelable,java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7849694769423867701L;
	/**
	 * 定义本类的私有变量
	 */
	private Integer id;
	private String mobile;
	private String name;
	private String image;
	private Integer age;
	private Integer sex;
	private String idcard;
	private Integer height;
	private Float weight;
	private String birthday;
	private String confirmed;
	private String type;
	private String modeonset;
	private String symptoms;
	private String medicalinsurance;
	private String diagnostic;
	private Integer relation;
	private float protein;
	private Integer mobileArea;

	/**
	 * 本类的实例化方法
	 */
	public PatientBean() {
	}

	/**
	 * 本类的带参数的实例化方法
	 */
	public PatientBean(Integer id, String mobile, String name, String image,
			Integer age, Integer sex, String idcard, Integer height,
			Float weight, String birthday, String confirmed, String type,
			String modeonset, String symptoms, String medicalinsurance,
			String diagnostic, Integer relation,Integer mobileArea) {
		this.id = id;
		this.mobile = mobile;
		this.name = name;
		this.image = image;
		this.age = age;
		this.sex = sex;
		this.idcard = idcard;
		this.height = height;
		this.weight = weight;
		this.birthday = birthday;
		this.confirmed = confirmed;
		this.type = type;
		this.modeonset = modeonset;
		this.symptoms = symptoms;
		this.medicalinsurance = medicalinsurance;
		this.diagnostic = diagnostic;
		this.relation = relation;
		this.mobileArea = mobileArea;
	}

	/**
	 * 以本类为参数传入时，实例化本类的方法
	 */
	public void copyFrom(PatientBean vo) {
		id = vo.id;
		mobile = vo.mobile;
		name = vo.name;
		image = vo.image;
		age = vo.age;
		sex = vo.sex;
		idcard = vo.idcard;
		height = vo.height;
		weight = vo.weight;
		birthday = vo.birthday==null?String.valueOf(TimeUtil.getInstance().getCurrentYear()):vo.birthday;
		confirmed = vo.confirmed;
		type = vo.type;
		modeonset = vo.modeonset;
		symptoms = vo.symptoms;
		medicalinsurance = vo.medicalinsurance;
		diagnostic = vo.diagnostic;
		relation = vo.relation;
		protein = vo.protein;
		mobileArea = vo.mobileArea;
	}

	/**
	 * 本类的赋值方法
	 */
	public void setData(PatientBean vo) {
		this.copyFrom(vo);
	}

	/**
	 * 本类的取值方法
	 */
	public PatientBean getData() {
		PatientBean result = new PatientBean();
		result.copyFrom(this);
		return result;
	}

	/**
	 * id的赋值方法
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * mobile的赋值方法
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	/**
	 * name的赋值方法
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * image的赋值方法
	 */
	public void setImage(String image) {
		this.image = image;
	}

	/**
	 * age的赋值方法
	 */
	public void setAge(Integer age) {
		this.age = age;
	}

	/**
	 * sex的赋值方法
	 */
	public void setSex(Integer sex) {
		this.sex = sex;
	}

	/**
	 * idcard的赋值方法
	 */
	public void setIdcard(String idcard) {
		this.idcard = idcard;
	}

	/**
	 * height的赋值方法
	 */
	public void setHeight(Integer height) {
		this.height = height;
	}

	/**
	 * weight的赋值方法
	 */
	public void setWeight(Float weight) {
		this.weight = weight;
	}

	/**
	 * birthday的赋值方法
	 */
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	/**
	 * confirmed的赋值方法
	 */
	public void setConfirmed(String confirmed) {
		this.confirmed = confirmed;
	}

	/**
	 * type的赋值方法
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * modeonset的赋值方法
	 */
	public void setModeonset(String modeonset) {
		this.modeonset = modeonset;
	}

	/**
	 * symptoms的赋值方法
	 */
	public void setSymptoms(String symptoms) {
		this.symptoms = symptoms;
	}

	/**
	 * medicalinsurance的赋值方法
	 */
	public void setMedicalinsurance(String medicalinsurance) {
		this.medicalinsurance = medicalinsurance;
	}

	/**
	 * diagnostic的赋值方法
	 */
	public void setDiagnostic(String diagnostic) {
		this.diagnostic = diagnostic;
	}

	/**
	 * relation的赋值方法
	 */
	public void setRelation(Integer relation) {
		this.relation = relation;
	}

	/**
	 * id的取值方法
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * mobile的取值方法
	 */
	public String getMobile() {
		return mobile;
	}

	/**
	 * name的取值方法
	 */
	public String getName() {
		return name;
	}

	/**
	 * image的取值方法
	 */
	public String getImage() {
		return image;
	}

	/**
	 * age的取值方法
	 */
	public Integer getAge() {
		return age;
	}

	/**
	 * sex的取值方法
	 */
	public Integer getSex() {
		return sex;
	}

	/**
	 * idcard的取值方法
	 */
	public String getIdcard() {
		return idcard;
	}

	/**
	 * height的取值方法
	 */
	public Integer getHeight() {
		return height;
	}

	/**
	 * weight的取值方法
	 */
	public Float getWeight() {
		return weight;
	}

	/**
	 * birthday的取值方法
	 */
	public String getBirthday() {
		return birthday;
	}

	/**
	 * confirmed的取值方法
	 */
	public String getConfirmed() {
		return confirmed;
	}

	/**
	 * type的取值方法
	 */
	public String getType() {
		return type;
	}

	/**
	 * modeonset的取值方法
	 */
	public String getModeonset() {
		return modeonset;
	}

	/**
	 * symptoms的取值方法
	 */
	public String getSymptoms() {
		return symptoms;
	}

	/**
	 * medicalinsurance的取值方法
	 */
	public String getMedicalinsurance() {
		return medicalinsurance;
	}

	/**
	 * diagnostic的取值方法
	 */
	public String getDiagnostic() {
		return diagnostic;
	}

	/**
	 * relation的取值方法
	 */
	public Integer getRelation() {
		return relation;
	}

	public float getProtein() {
		return protein;
	}

	public void setProtein(float protein) {
		this.protein = protein;
	}
	
	public float getMobileArea() {
		return mobileArea;
	}

	public void setMobileArea(Integer mobileArea) {
		this.mobileArea = mobileArea;
	}

	
	public int getAgeByBirthday(String birthday) {
		Calendar cal = Calendar.getInstance();

		Date date = TimeUtil.getInstance().strToDate(birthday, "yyyy-MM-dd");
		
		if (cal.before(date)) {
			throw new IllegalArgumentException(
					"The birthDay is before Now.It's unbelievable!");
		}

		int yearNow = cal.get(Calendar.YEAR);
		
		cal.setTime(date);
		int yearBirth = cal.get(Calendar.YEAR);

		return yearNow - yearBirth;
	}
	
	/**	基本信息是否完善	*/
	public boolean isBasicInfoComplete(){
		if (height == null || height == 0) {
			return false;
		}
		if (weight == null || weight == 0) {
			return false;
		}
		return true;
	}
	
	/**	详细信息是否完善	*/
	public boolean isDetailInfoComplete(){
		if (TextUtils.isEmpty(name)) {
			return false;
		}
		if (TextUtils.isEmpty(idcard)) {
			return false;
		}
		if (TextUtils.isEmpty(type)) {
			return false;
		}
//		if (condition) {
//			
//		}
		return true;
	}
	
	@Override
	public String toString() {
//		return "PatientBean [id=" + id + ", mobile=" + mobile + ", name="
//				+ name + ", image=" + image + ", age=" + age + ", sex=" + sex
//				+ ", idcard=" + idcard + ", height=" + height + ", weight="
//				+ weight + ", birthday=" + birthday + ", confirmed="
//				+ confirmed + ", type=" + type + ", modeonset=" + modeonset
//				+ ", symptoms=" + symptoms + ", medicalinsurance="
//				+ medicalinsurance + ", diagnostic=" + diagnostic
//				+ ", relation=" + relation + ", protein=" + protein + ", mobileArea=" + mobileArea + "]";
		return "PatientBean [id=" + id + ", name="
		+ name + ", image=" + image + ", age=" + age + ", sex=" + sex;
	}

	/**
	 * id取值方法
	 */
	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeValue(this.id);
		dest.writeString(this.mobile);
		dest.writeString(this.name);
		dest.writeString(this.image);
		dest.writeValue(this.age);
		dest.writeValue(this.sex);
		dest.writeString(this.idcard);
		dest.writeValue(this.height);
		dest.writeValue(this.weight);
		dest.writeString(this.birthday);
		dest.writeString(this.confirmed);
		dest.writeString(this.type);
		dest.writeString(this.modeonset);
		dest.writeString(this.symptoms);
		dest.writeString(this.medicalinsurance);
		dest.writeString(this.diagnostic);
		dest.writeValue(this.relation);
		dest.writeValue(this.mobileArea);
	}

	private PatientBean(Parcel in) {
		this.id = (Integer) in.readValue(Integer.class.getClassLoader());
		this.mobile = in.readString();
		this.name = in.readString();
		this.image = in.readString();
		this.age = (Integer) in.readValue(Integer.class.getClassLoader());
		this.sex = (Integer) in.readValue(Integer.class.getClassLoader());
		this.idcard = in.readString();
		this.height = (Integer) in.readValue(Integer.class.getClassLoader());
		this.weight = (Float) in.readValue(Float.class.getClassLoader());
		this.birthday = in.readString();
		this.confirmed = in.readString();
		this.type = in.readString();
		this.modeonset = in.readString();
		this.symptoms = in.readString();
		this.medicalinsurance = in.readString();
		this.diagnostic = in.readString();
		this.relation = (Integer) in.readValue(Integer.class.getClassLoader());
		this.mobileArea = (Integer) in.readValue(Integer.class.getClassLoader());
	}

	public static final Parcelable.Creator<PatientBean> CREATOR = new Parcelable.Creator<PatientBean>() {
		public PatientBean createFromParcel(Parcel source) {
			return new PatientBean(source);
		}

		public PatientBean[] newArray(int size) {
			return new PatientBean[size];
		}
	};
}