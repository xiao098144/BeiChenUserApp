package com.ddoctor.user.wapi.bean;

/**
 * <p>Title: Value code</p>
 * <p>Description: data view</p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: ddoctor</p>
 * @author tian jiale
 * @version 1.0
 */

import android.os.Parcel;
import android.os.Parcelable;

public class CatagoryBean implements java.io.Serializable, Parcelable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 定义本类的私有变量
	 */
	private Integer id;
	private String name;
	private Integer sequence;
	private String imgUrl;

	/**
	 * 本类的实例化方法
	 */
	public CatagoryBean() {
	}

	/**
	 * 本类的带参数的实例化方法
	 */
	public CatagoryBean(Integer id, String name, Integer sequence, String imgUrl) {
		super();
		this.id = id;
		this.name = name;
		this.sequence = sequence;
		this.imgUrl = imgUrl;
	}

	/**
	 * 以本类为参数传入时，实例化本类的方法
	 */
	public void copyFrom(CatagoryBean vo) {
		id = vo.id;
		name = vo.name;
		sequence = vo.sequence;
		imgUrl = vo.imgUrl;
	}

	/**
	 * 本类的赋值方法
	 */
	public void setData(CatagoryBean vo) {
		this.copyFrom(vo);
	}

	/**
	 * 本类的取值方法
	 */
	public CatagoryBean getData() {
		CatagoryBean result = new CatagoryBean();
		result.copyFrom(this);
		return result;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getSequence() {
		return sequence;
	}

	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeValue(this.id);
		dest.writeString(this.name);
		dest.writeValue(this.sequence);
		dest.writeString(this.imgUrl);
	}

	private CatagoryBean(Parcel in) {
		this.id = (Integer) in.readValue(Integer.class.getClassLoader());
		this.name = in.readString();
		this.sequence = (Integer) in.readValue(Integer.class.getClassLoader());
		this.imgUrl = in.readString();
	}

	public static final Parcelable.Creator<CatagoryBean> CREATOR = new Parcelable.Creator<CatagoryBean>() {
		public CatagoryBean createFromParcel(Parcel source) {
			return new CatagoryBean(source);
		}

		public CatagoryBean[] newArray(int size) {
			return new CatagoryBean[size];
		}
	};
}