package com.ddoctor.user.wapi.bean;

/**
 * <p>Title: Value code</p>
 * <p>Description: data view</p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: ddoctor</p>
 * @author tian jiale
 * @version 1.0
 */


public class LogCountBean implements java.io.Serializable {
	/**
	 * 定义本类的私有变量
	 */
	private Integer food;
	private Integer blood;
	private Integer bmi;
	private Integer trouble;
	private Integer patientcase;
	private Integer recipe;
	private Integer sheet;
	private Integer cruorin;
	private Integer medicine;

	/**
	 * 本类的实例化方法
	 */
	public LogCountBean() {
	}

	/**
	 * 本类的带参数的实例化方法
	 */
	public LogCountBean(Integer food, Integer blood, Integer bmi,
			Integer trouble, Integer patientcase, Integer recipe,
			Integer sheet, Integer cruorin, Integer medicine) {
		this.food = food;
		this.blood = blood;
		this.bmi = bmi;
		this.trouble = trouble;
		this.patientcase = patientcase;
		this.recipe = recipe;
		this.sheet = sheet;
		this.cruorin = cruorin;
		this.medicine = medicine;
	}

	/**
	 * 以本类为参数传入时，实例化本类的方法
	 */
	public void copyFrom(LogCountBean vo) {
		food = vo.food;
		blood = vo.blood;
		bmi = vo.bmi;
		trouble = vo.trouble;
		patientcase = vo.patientcase;
		recipe = vo.recipe;
		sheet = vo.sheet;
		cruorin = vo.cruorin;
		medicine = vo.medicine;
	}

	/**
	 * 本类的赋值方法
	 */
	public void setData(LogCountBean vo) {
		this.copyFrom(vo);
	}

	/**
	 * 本类的取值方法
	 */
	public LogCountBean getData() {
		LogCountBean result = new LogCountBean();
		result.copyFrom(this);
		return result;
	}

	/**
	 * food的赋值方法
	 */
	public void setFood(Integer food) {
		this.food = food;
	}

	/**
	 * blood的赋值方法
	 */
	public void setBlood(Integer blood) {
		this.blood = blood;
	}

	/**
	 * bmi的赋值方法
	 */
	public void setBmi(Integer bmi) {
		this.bmi = bmi;
	}

	/**
	 * trouble的赋值方法
	 */
	public void setTrouble(Integer trouble) {
		this.trouble = trouble;
	}

	/**
	 * patientcase的赋值方法
	 */
	public void setpatientcase(Integer patientcase) {
		this.patientcase = patientcase;
	}

	/**
	 * recipe的赋值方法
	 */
	public void setRecipe(Integer recipe) {
		this.recipe = recipe;
	}

	/**
	 * sheet的赋值方法
	 */
	public void setSheet(Integer sheet) {
		this.sheet = sheet;
	}

	/**
	 * cruorin的赋值方法
	 */
	public void setCruorin(Integer cruorin) {
		this.cruorin = cruorin;
	}

	/**
	 * medicine的赋值方法
	 */
	public void setMedicine(Integer medicine) {
		this.medicine = medicine;
	}

	/**
	 * food的取值方法
	 */
	public Integer getFood() {
		return food;
	}

	/**
	 * blood的取值方法
	 */
	public Integer getBlood() {
		return blood;
	}

	/**
	 * bmi的取值方法
	 */
	public Integer getBmi() {
		return bmi;
	}

	/**
	 * trouble的取值方法
	 */
	public Integer getTrouble() {
		return trouble;
	}

	/**
	 * patientcase的取值方法
	 */
	public Integer getpatientcase() {
		return patientcase;
	}

	/**
	 * recipe的取值方法
	 */
	public Integer getRecipe() {
		return recipe;
	}

	/**
	 * sheet的取值方法
	 */
	public Integer getSheet() {
		return sheet;
	}

	/**
	 * cruorin的取值方法
	 */
	public Integer getCruorin() {
		return cruorin;
	}

	/**
	 * medicine的取值方法
	 */
	public Integer getMedicine() {
		return medicine;
	}
	/**
	 * food取值方法
	 */
}