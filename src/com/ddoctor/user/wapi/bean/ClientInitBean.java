package com.ddoctor.user.wapi.bean;

/**
 * <p>Title: Value code</p>
 * <p>Description: 增加location字段 - 萧 06/25</p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: ddoctor</p>
 * @author tian jiale
 * @version 1.0
 */


public class ClientInitBean implements java.io.Serializable {
	/**
	 * 定义本类的私有变量
	 */
	private String uuid;
	private String ua;
	private String ip;
	private String nettype;
	private String versionName;
	private Integer versionCode;
	private Integer sdk;
	private String brand;
	private String device;
	private String versionrelease;
	private String os;
	private Integer height;
	private Integer width;
	private Integer channel;
	private Integer isfirst;
	private String imei;
	private String imsi;
	private String mobile;
	private int dicsn;
	private String clientsn;
	private String location;

	/**
	 * 本类的实例化方法
	 */
	public ClientInitBean() {
	}

	/**
	 * 本类的带参数的实例化方法
	 */
	public ClientInitBean(String uuid, String ua, String ip, String nettype,
			String versionName, Integer versionCode, Integer sdk, String brand,
			String device, String versionrelease, String os, Integer height,
			Integer width, Integer channel, Integer isfirst, String imei,
			String imsi, String mobile, int dicsn ,String clientsn,String location) {
		this.uuid = uuid;
		this.ua = ua;
		this.ip = ip;
		this.nettype = nettype;
		this.versionName = versionName;
		this.versionCode = versionCode;
		this.sdk = sdk;
		this.brand = brand;
		this.device = device;
		this.versionrelease = versionrelease;
		this.os = os;
		this.height = height;
		this.width = width;
		this.channel = channel;
		this.isfirst = isfirst;
		this.imei = imei;
		this.imsi = imsi;
		this.mobile = mobile;
		this.dicsn = dicsn;
		this.clientsn = clientsn;
		this.location = location;
	}

	/**
	 * 以本类为参数传入时，实例化本类的方法
	 */
	public void copyFrom(ClientInitBean vo) {
		uuid = vo.uuid;
		ua = vo.ua;
		ip = vo.ip;
		nettype = vo.nettype;
		versionName = vo.versionName;
		versionCode = vo.versionCode;
		sdk = vo.sdk;
		brand = vo.brand;
		device = vo.device;
		versionrelease = vo.versionrelease;
		os = vo.os;
		height = vo.height;
		width = vo.width;
		channel = vo.channel;
		isfirst = vo.isfirst;
		imei = vo.imei;
		imsi = vo.imsi;
		mobile = vo.mobile;
		dicsn = vo.dicsn;
		clientsn = vo.clientsn;
		location = vo.location;
	}

	/**
	 * 本类的赋值方法
	 */
	public void setData(ClientInitBean vo) {
		this.copyFrom(vo);
	}

	/**
	 * 本类的取值方法
	 */
	public ClientInitBean getData() {
		ClientInitBean result = new ClientInitBean();
		result.copyFrom(this);
		return result;
	}

	/**
	 * uuid的赋值方法
	 */
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	/**
	 * ua的赋值方法
	 */
	public void setUa(String ua) {
		this.ua = ua;
	}

	/**
	 * ip的赋值方法
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}

	/**
	 * nettype的赋值方法
	 */
	public void setNettype(String nettype) {
		this.nettype = nettype;
	}

	/**
	 * versionName的赋值方法
	 */
	public void setVersionName(String versionName) {
		this.versionName = versionName;
	}

	/**
	 * versionCode的赋值方法
	 */
	public void setVersionCode(Integer versionCode) {
		this.versionCode = versionCode;
	}

	/**
	 * sdk的赋值方法
	 */
	public void setSdk(Integer sdk) {
		this.sdk = sdk;
	}

	/**
	 * brand的赋值方法
	 */
	public void setBrand(String brand) {
		this.brand = brand;
	}

	/**
	 * device的赋值方法
	 */
	public void setDevice(String device) {
		this.device = device;
	}

	/**
	 * versionrelease的赋值方法
	 */
	public void setVersionrelease(String versionrelease) {
		this.versionrelease = versionrelease;
	}

	/**
	 * os的赋值方法
	 */
	public void setOs(String os) {
		this.os = os;
	}

	/**
	 * height的赋值方法
	 */
	public void setHeight(Integer height) {
		this.height = height;
	}

	/**
	 * width的赋值方法
	 */
	public void setWidth(Integer width) {
		this.width = width;
	}

	/**
	 * channel的赋值方法
	 */
	public void setChannel(Integer channel) {
		this.channel = channel;
	}

	/**
	 * isfirst的赋值方法
	 */
	public void setIsfirst(Integer isfirst) {
		this.isfirst = isfirst;
	}

	/**
	 * imei的赋值方法
	 */
	public void setImei(String imei) {
		this.imei = imei;
	}

	/**
	 * imsi的赋值方法
	 */
	public void setImsi(String imsi) {
		this.imsi = imsi;
	}

	/**
	 * mobile的赋值方法
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	/**
	 * mobile的赋值方法
	 */
	public void setDicsn(int dicsn) {
		this.dicsn = dicsn;
	}

	/**
	 * mobile的赋值方法
	 */
	public void setClientsn(String clientsn) {
		this.clientsn = clientsn;
	}
	
	public void setLocation(String location) {
		this.location = location;
	}
	
	public String getLocation() {
		return location;
	}
	
	/**
	 * uuid的取值方法
	 */
	public String getUuid() {
		return uuid;
	}

	/**
	 * ua的取值方法
	 */
	public String getUa() {
		return ua;
	}

	/**
	 * ip的取值方法
	 */
	public String getIp() {
		return ip;
	}

	/**
	 * nettype的取值方法
	 */
	public String getNettype() {
		return nettype;
	}

	/**
	 * versionName的取值方法
	 */
	public String getVersionName() {
		return versionName;
	}

	/**
	 * versionCode的取值方法
	 */
	public Integer getVersionCode() {
		return versionCode;
	}

	/**
	 * sdk的取值方法
	 */
	public Integer getSdk() {
		return sdk;
	}

	/**
	 * brand的取值方法
	 */
	public String getBrand() {
		return brand;
	}

	/**
	 * device的取值方法
	 */
	public String getDevice() {
		return device;
	}

	/**
	 * versionrelease的取值方法
	 */
	public String getVersionrelease() {
		return versionrelease;
	}

	/**
	 * os的取值方法
	 */
	public String getOs() {
		return os;
	}

	/**
	 * height的取值方法
	 */
	public Integer getHeight() {
		return height;
	}

	/**
	 * width的取值方法
	 */
	public Integer getWidth() {
		return width;
	}

	/**
	 * channel的取值方法
	 */
	public Integer getChannel() {
		return channel;
	}

	/**
	 * isfirst的取值方法
	 */
	public Integer getIsfirst() {
		return isfirst;
	}

	/**
	 * imei的取值方法
	 */
	public String getImei() {
		return imei;
	}

	/**
	 * imsi的取值方法
	 */
	public String getImsi() {
		return imsi;
	}

	/**
	 * mobile的取值方法
	 */
	public String getMobile() {
		return mobile;
	}

	
	/**
	 * mobile的取值方法
	 */
	public int getDicsn() {
		return dicsn;
	}
	
	/**
	 * mobile的取值方法
	 */
	public String getClientsn() {
		return clientsn;
	}
	
}