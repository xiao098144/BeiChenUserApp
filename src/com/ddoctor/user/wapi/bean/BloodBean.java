package com.ddoctor.user.wapi.bean;

/**
 * <p>Title: Value code</p>
 * <p>Description: data view</p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: ddoctor</p>
 * @author tian jiale
 * @version 1.0
 */

import android.os.Parcel;
import android.os.Parcelable;

import com.ddoctor.enums.RecordLayoutType;

public class BloodBean implements Parcelable, Comparable<BloodBean> {
	/**
	 * 定义本类的私有变量
	 */
	private Integer id;
	private Integer high;
	private Integer low;
	private String time;
	private String remark;
	private Integer patientId;

	private String date; // yyyy-MM-dd

	private RecordLayoutType layoutType = RecordLayoutType.TYPE_VALUE;

	/**
	 * 本类的实例化方法
	 */
	public BloodBean() {
	}

	/**
	 * 本类的带参数的实例化方法
	 */
	public BloodBean(Integer id, Integer high, Integer low, String time,
			String remark, Integer patientId, String date,
			RecordLayoutType layoutType) {
		super();
		this.id = id;
		this.high = high;
		this.low = low;
		this.time = time;
		this.remark = remark;
		this.patientId = patientId;
		this.date = date;
		this.layoutType = layoutType;
	}

	/**
	 * 以本类为参数传入时，实例化本类的方法
	 */
	public void copyFrom(BloodBean vo) {
		id = vo.id;
		high = vo.high;
		low = vo.low;
		time = vo.time;
		remark = vo.remark;
		patientId = vo.patientId;
		date = vo.date;
		layoutType = vo.layoutType;
	}

	/**
	 * 本类的赋值方法
	 */
	public void setData(BloodBean vo) {
		this.copyFrom(vo);
	}

	/**
	 * 本类的取值方法
	 */
	public BloodBean getData() {
		BloodBean result = new BloodBean();
		result.copyFrom(this);
		return result;
	}

	/**
	 * id的赋值方法
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * high的赋值方法
	 */
	public void setHigh(Integer high) {
		this.high = high;
	}

	/**
	 * low的赋值方法
	 */
	public void setLow(Integer low) {
		this.low = low;
	}

	/**
	 * time的赋值方法
	 */
	public void setTime(String time) {
		this.time = time;
	}

	/**
	 * remark的赋值方法
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

	/**
	 * patientId的赋值方法
	 */
	public void setPatientId(Integer patientId) {
		this.patientId = patientId;
	}

	/**
	 * id的取值方法
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * high的取值方法
	 */
	public Integer getHigh() {
		return high;
	}

	/**
	 * low的取值方法
	 */
	public Integer getLow() {
		return low;
	}

	/**
	 * time的取值方法
	 */
	public String getTime() {
		return time;
	}

	/**
	 * remark的取值方法
	 */
	public String getRemark() {
		return remark;
	}

	/**
	 * patientId的取值方法
	 */
	public Integer getPatientId() {
		return patientId;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getDate() {
		return date;
	}

	public void setLayoutType(RecordLayoutType layoutType) {
		this.layoutType = layoutType;
	}

	public RecordLayoutType getLayoutType() {
		return layoutType;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeValue(this.id);
		dest.writeValue(this.high);
		dest.writeValue(this.low);
		dest.writeString(this.time);
		dest.writeString(this.remark);
		dest.writeValue(this.patientId);
		dest.writeString(this.date);
		dest.writeInt(this.layoutType == RecordLayoutType.TYPE_VALUE ? 1 : 2);
	}

	protected BloodBean(Parcel in) {
		this.id = (Integer) in.readValue(Integer.class.getClassLoader());
		this.high = (Integer) in.readValue(Integer.class.getClassLoader());
		this.low = (Integer) in.readValue(Integer.class.getClassLoader());
		this.time = in.readString();
		this.remark = in.readString();
		this.patientId = (Integer) in.readValue(Integer.class.getClassLoader());
		this.date = in.readString();
		int tmpLayoutType = in.readInt();
		this.layoutType = tmpLayoutType == 1 ? RecordLayoutType.TYPE_VALUE
				: RecordLayoutType.TYPE_CATEGORY;
	}

	public static final Parcelable.Creator<BloodBean> CREATOR = new Parcelable.Creator<BloodBean>() {
		public BloodBean createFromParcel(Parcel source) {
			return new BloodBean(source);
		}

		public BloodBean[] newArray(int size) {
			return new BloodBean[size];
		}
	};

	@Override
	public int compareTo(BloodBean another) {
		int compare;
		compare = getTime().compareTo(another.getTime());
		return 0 - compare;
	}
}