package com.ddoctor.user.wapi.constant;

public class Record {
	
	public final static int SUGAR_VALUE = 1;//血糖记录
	public final static int GLUCOMETER = 2;//血糖仪记录
	public final static int MEDICAL_RECORD = 3;//用药记录
	public final static int DIET_RECORD = 4;//饮食记录
	public final static int FOOD_RECORD = 5;//食物记录
	public final static int SPORT_RECORD = 6;//运动记录
	public final static int BLOOD_PRESSURE = 7;//血压记录
	public final static int HEIGHT_WEIGHT = 8;//身高体重记录
	public final static int TROUBLE_RECORD = 9;//不适记录
	public final static int PROTEIN = 10;//血红蛋白记录
	public final static int DISEASE = 11;//病历记录
	public final static int HYD = 12;//化验单记录
	public final static int CFYZ = 13;//处方医嘱
	public final static int VOICE = 14;//问医的提问语音
	public final static int IMAGE = 15;//问医的提问图片
	public final static int HEAD_PICTURE = 16;//个人头像
	public final static int DELIVER = 17;//收货地址
	public final static int QUESTION = 18;//患者提问问题
	
}
