package com.ddoctor.user.wapi.constant;

public class FoodType
{
	public final static int PRICIPAL = 1;//主食
	public final static int MEAT = 2;//肉类
	public final static int VEGETABLES = 3;//蔬菜
	public final static int FRUITS = 4;//水果
	public final static int DRINKS = 5;//饮料

	public final static int CATEGORY_GUSHU = 1;//谷薯类
	public final static int CATEGORY_SHUCAI = 2; //蔬菜类
	public final static int CATEGORY_SHUIGUO = 3; //水果类
	public final static int CATEGORY_DADOU = 4;   //大豆类
	public final static int CATEGORY_RULEI = 5;   //乳类
	public final static int CATEGORY_ROUDAN = 6;  //肉蛋类
	public final static int CATEGORY_YINGGUO = 7; //硬果类
	public final static int CATEGORY_YOUZHI = 8; //油脂类
	
}
