package com.ddoctor.user.wapi.constant;

public class Action
{
	public final static int INIT_CLIENT = 10101;//客户端初始化
	public final static int GET_CLIENT_UPDATE = 10102;//版本升级
	public final static int GET_CLIENT_ABOUT = 10103;//关于信息获取
	public final static int SEND_VERIFY_SMS = 10104;//发送短信验证码
	public final static int ADD_RECOMENT = 10105;//推荐好友
	public final static int UPLOAD_FILE = 10106;//上传文件
	public final static int DELETE_RECORD = 10107;//删除记录
	public final static int GET_RECORD_TOTAL = 10108;//查询记录总天数与总记录数
	public final static int DELETE_RECORDS = 10110;//删除记录（一个或批量）
	public final static int ADD_FEEDBACK = 101010; //用户反馈
	public final static int GET_KNOWLEGE_CATAGORY = 10201;//查询知识库目录
	public final static int GET_KNOWLEGE_LIST = 10202;//查看知识库列表
	public final static int GET_KNOWLEGE = 10203;//查看知识库详情
	public final static int GET_KNOWLEGE_SEARCH = 10204;//搜索知识库文章
	public final static int ADD_COLLECTION = 10205;//添加收藏记录
	public final static int GET_COLLECTION_LIST = 10206;//查看收藏列表
	public final static int ADD_SHARE = 10207;//添加文章分享记录
	public final static int GET_POINT_TOTAL = 10301;//查询积分余额
	public final static int GET_POINT_LIST = 10302;//查看积分列表
	public final static int GET_WECHAT_PACKAGE = 10401;//微信支付获取package
	public final static int GET_WECHAT_SIGN = 10402;//微信支付获取sign
	public final static int PATIENT_REGISTER = 20101;//患者注册
	public final static int PATIENT_LOGIN = 20102;//患者登录
	public final static int PATIENT_QUICK_LOGIN = 20103;//患者快捷登录
	public final static int PATIENT_UPDATE_PASSWORD = 20104;//患者重置密码
	public final static int PATIENT_BINDING_MOBILE = 20105;//患者绑定手机号码
	public final static int GET_PATIENT = 20106;//查询患者信息
	public final static int UPDATE_PATIENT = 20107;//修改患者信息
	public final static int GET_SUGARVALUE_LIST = 20201;//查询血糖记录
	public final static int DO_SUGARVALUE = 20202;//添加/修改血糖记录
	public final static int ADD_GLUCOMETER = 20203;//添加/设置血糖仪
	public final static int GET_SUGARVALUE_MONTH = 20204;//查询月血糖分析记录
	public final static int GET_UPLOWVALUE = 20205;//查询用户血糖上下限
	public final static int UPDATE_UPLOWVALUE = 20206;//修改用户血糖上下限
	public final static int GET_EVALUATION = 20207; //计算健康报告
	public final static int GET_MEDICALRECORD_LIST = 20301;//查询用药记录
	public final static int DO_MEDICALRECORD = 20302;//添加用药记录
	public final static int GET_DIETRECORD_LIST = 20401;//查询饮食记录列表
	public final static int DO_DIETRECORD = 20402;//添加/修改饮食记录
	public final static int GET_DIETRECORD = 20403;//查询饮食记录详情
	public final static int ADD_DIETRECORD = 20404;//添加主食/肉/蔬菜记录
	public final static int GET_SPORTRECORD_LIST = 20501;//查询运动记录列表
	public final static int ADD_SPORTRECORD = 20502;//添加运动记录
	public final static int GET_BLOODPRESSURE_LIST= 20601;//查询血压记录列表
	public final static int DO_BLOODPRESSURE = 20602;//添加/修改血压记录
	public final static int GET_HEIGHT_LIST = 20701;//查询身高体重记录列表
	public final static int DO_HEIGHT = 20702;//添加/修改身高体重记录
	public final static int GET_TROUBLERECORD_LIST = 20801;//查询不适记录列表
	public final static int DO_TROUBLERECORD = 20802;//添加/修改不适记录
	public final static int GET_HYDCFYZ_LIST = 20901;//查询化验单/处方医嘱列表
	public final static int ADD_HYDCFYZ = 20902;//添加化验单/处方医嘱记录
	public final static int GET_SUGARPROTEIN_LIST = 21001;//查询糖化血红蛋白记录列表
	public final static int DO_SUGARPROTEIN = 21002;//添加/修改糖化血红蛋白记录
	public final static int GET_DISEASE_LIST = 21101;//查询病历列表
	public final static int DO_DISEASE = 21102;//添加/修改病历
	public final static int GET_PUBLICQUESTION_PATIENT_LIST = 21201;//查询公共区提问列表
	public final static int GET_PUBLICQUESTION_PATIENT_SEARCH = 21202;//搜索公共区提问列表
	public final static int GET_PUBLICQUESTION_PATIENT_MINE = 21203;//查询我的提问与回复列表
	public final static int GET_PUBLICQUESTION_PATIENT_REPLY =21204;//查看提问详情及回复列表
	public final static int GET_DOCTOR_LIST = 21205;//查询医生列表
	public final static int GET_DOCTOR_SEARCH = 21206;//搜索医生列表
	public final static int SCORE_DOCTOR = 21207;//为医生评星
	public final static int ADD_DOCTOR_PATIENT = 21208;//申请添加医生    ...此为补填
	public final static int ADD_QUESTION = 21209;//添加问医记录
	public final static int ADD_QUESTION_REPLY = 21210;//	添加问医的问题回复
	public final static int ADD_QUESTIONJBZL = 21303;//提交基本调查信息
	public final static int GET_PRODUCT_LIST = 21401;//查询商品列表
	public final static int GET_PRODUCT = 21402;//查询商品详情
	public final static int GET_PAYPREPARE = 21403;//订单支付前的确认
	public final static int ADD_BUYCART = 21404;//同步购物车记录
	public final static int GET_ORDER_LIST = 21405;//查询订单列表
	public final static int GET_ORDER = 21406;//查询订单详情
	public final static int ADD_ORDER = 21407;//新增订单
	public final static int UPDATE_ORDER = 21408;//支付状态同步
	public final static int GET_DELIVER_LIST = 21409;//查询收货地址列表
	public final static int GET_DELIVER = 21410;//查询收货地址详情
	public final static int DO_DELIVER = 21411;//添加/修改收货地址
	public final static int DOCTOR_REGISTER = 30101;//医生注册
	public final static int DOCTOR_LOGIN = 30102;//医生登录
	public final static int DOCTOR_QUICK_LOGIN = 30103;//医生快捷登录
	public final static int DOCTOR_UPDATE_PASSWORD = 30104;//医生重置密码
	public final static int DOCTOR_BINDING_MOBILE = 30105;//医生绑定手机号码
	public final static int GET_DOCTOR = 30106;//医生查询个人信息
	public final static int UPDATE_DOCTOR = 30107;//医生修改个人信息
	public final static int GET_UNCONFIRMED_PATIENT_LIST = 30201;//查询等待确认添加的患者列表
	public final static int GET_PATIENT_LIST = 30202;//查询我的患者列表
	public final static int GET_PATIENT_SEARCH_LIST = 30203;//搜索我的患者列表
	public final static int DO_PATIENT_DOCTOR_RELATION =30204;//设置医患关系
	public final static int GET_SUGARVALUE_MONTH_LIST = 30205;//查询我的患者月血糖分析记录列表
	public final static int GET_PATIENT_LOG_COUNT = 30206;//查询患者所有日志记录
	public final static int GET_PATIENT_LOG = 30207;//查询患者日志记录详情
	public final static int GET_RELATION_DISEASE_LIST = 30208;//查询医生记录的患者病历列表
	public final static int ADD_DOCTOR_DISEASE = 30209;//医生添加/修改患者病历
	public final static int GET_PUBLICQUESTION_DOCTOR_LIST = 30301;//按回复查看患者公共提问列表
	public final static int GET_PUBLICQUESTION_DOCTOR_REPLY = 30302;//查看医生的已回复列表
	public final static int ADD_REPLY = 30303;//回复公共区提问
	public final static int GET_RECOMMENT_LIST = 30401;//查询医生推荐过的患者列表
	public final static int GET_OUTPATIENT = 30402;//查询医生的我的门诊
	public final static int UPDATE_OUTPATIENT = 30403;//修改医生的我的门诊
	public final static int GET_MYRECORD_CORD = 30500; //我的记录
	
	public final static int COMPLETE_TSL_PATIENT = 50101; //完善医保信息
	public final static int ADD_TSL_PRESCRIPTION = 50102; //添加处方
	public final static int GET_PRESCRIPTION_LIST = 50103; //获取处方列表
	public final static int GET_PRESCRIPTION_DETAIL = 50104; //查询单个处方详情
	public final static int GET_PRESCRIPTION_PRODUCT_LIST = 50105; //获取某个人的所有处方下的药品
	public final static int GET_TSL_ORDER_LIST = 50106; //获取订单列表
	public final static int ADD_TSL_ORDER = 50107; //添加订单
	public final static int ADD_TSL_ADDRESS = 50108; //添加tsl收货地址
	public final static int UPDATE_TSL_ADDRESS = 50109;//修改tsl收货地址
	public final static int GET_TSL_ADDRESS = 50110; //获取tsl收货地址
	public final static int DELETE_TSL_ADDRESS = 50111;//删除tsl收货地址
	public final static int SEND_TSL_VERIFICATIONCODE = 50112;//发送短信验证码
	public final static int GET_TSL_ORDER_DETAIL = 50113; //获取订单详情
	
	public final static int GET_RONGYUN = 60101;//融云
	public final static int GET_KNOWLEGECONTENT_LIST = 60102;  // 获取知识库列表
	public final static int GET_KNOWLEGECONTENT = 60103;  // 获取知识库详情
	public final static int GET_KNOWLEGECONTENT_SEARCH = 60104;  // 搜索知识库列表
	public final static int GET_MYDIT_HEAT_LIST = 101011; //	饮食能量列表
	public final static int GET_COLLECTION_CONTENT_LIST  = 60106; // 我的收藏
	
	public final static int GET_MIO_BOX_GLUCOSEMETER = 10501; // 盒子支持的血糖仪
	public final static int MIO_BOX_BINDING = 10502; // 绑定盒子
	public final static int MIO_BOX_BINDING_CANCEL = 10503; // 解绑
	public final static int MIO_BOX_BINDING_STATE = 10504; // 查询盒子绑定状态



	
}

