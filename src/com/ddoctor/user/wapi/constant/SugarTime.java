package com.ddoctor.user.wapi.constant;

public class SugarTime {

	public final static int BEFORE_BREAKFAST = 0;//早餐前
	public final static int AFTER_BREAKFAST = 1;//早餐后
	public final static int BEFORE_LUNCH = 2;//午餐前
	public final static int AFTER_LUNCH = 3;//午餐后
	public final static int BEFORE_DINNER = 4;//晚餐前
	public final static int AFTER_DINNER = 5;//晚餐后
	public final static int BEFORE_SLEEP = 6;//睡前
	public final static int IN_MIDNIGHT = 7;//凌晨
}
