package com.ddoctor.user.wapi.constant;

public class LogType {

	public final static int DIET = 1;//饮食
	public final static int BLOOD = 2;//血压
	public final static int HEIGHT = 3;//身高体重
	public final static int TROUBLE = 4;//不适记录
	public final static int PROTEIN = 5;//糖化血红蛋白
}
