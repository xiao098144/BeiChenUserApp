package com.ddoctor.user.wapi.constant;

public class MedicalType
{
	public final static int JIANGTANGYAO = 1;//降糖药
	public final static int JIANGYAYAO = 2;//降压药
	public final static int TIAOZHIYAO = 3;//调脂药
	public final static int YIDAOSU = 4;//胰岛素

}
