package com.ddoctor.user.wapi.constant;

public class OrderStatus
{
	public final static int UNKNOW = 0;
	public final static int NOPAY = 1; //1=未支付，未发货；
	public final static int PAYED = 2; //2=已支付，未发货；
	public final static int DELIVERED = 3; //3=已支付，已发货；
	public final static int POSTPAY_UNDELIVERED = 4; //4=货到付款，未发货；
	public final static int POSTPAY_DELIVERED = 5;//=货到付款，已发货；
	public final static int DONE = 6; //6=已完结；
	public final static int TIMEOUT = 7;//7=超过时限，订单已删除	
}
