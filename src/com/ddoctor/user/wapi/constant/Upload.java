package com.ddoctor.user.wapi.constant;

public class Upload
{
	public final static int HEAD = 1;//头像
	public final static int CERTIFICATE = 2;//资格证
	public final static int CARD = 3;//工作证
	public final static int QUESTION_IMAGE = 4;//问医图片
	public final static int QUESTION_VOICE = 5;//问医语音
	public final static int DISEASE_IMAGE = 6;//病历图片
	public final static int SHEBAO_IMAGE = 10;//社保图片
	public final static int SHENFENZHENG_IMAGE = 11;//身份证图片
	public final static int SHENFENZHENGF_IMAGE = 12;//身份证反面图片
	public final static int MENTE_IMAGE = 13;//门特图片
	public final static int HEAD_IMAGE = 14;//一寸头像图片
	public final static int PRESCRIPTION_IMAGE = 15;//处方图片
	public final static int DIET_IMAGE = 16;//饮食图片
	
}
