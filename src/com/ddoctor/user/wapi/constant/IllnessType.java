package com.ddoctor.user.wapi.constant;

public class IllnessType {

	public final static int TANGNIAOBINGLEIXING = 1;//糖尿病类型
	public final static int QIBINGFANGSHI = 2;//起病方式
	public final static int QIBINGZHENGZHUANG = 3;//起病症状
	public final static int YIBAOLEIXING = 4;//医保类型
	public final static int QITAZHENDUAN = 5;//其他诊断
	public final static int WEIXUANZE = 6;//未选择
}
