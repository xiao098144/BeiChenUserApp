package com.ddoctor.user.wapi.constant;

public class PayMethod {

	public final static int WECHAT = 1;//微信支付
	public final static int ALIPAY = 2;//支付宝
	public final static int UNIPAY = 3;//银联支付
	public final static int CASH = 4;//银联支付
}
