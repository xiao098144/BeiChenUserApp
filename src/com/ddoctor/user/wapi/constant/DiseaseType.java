package com.ddoctor.user.wapi.constant;

public class DiseaseType {

	public final static int SHOUZHEN = 1;//首诊
	public final static int FUZHEN = 2;//复诊
	public final static int HUAYANDAN = 3;//化验单
	public final static int BINGLI = 4;//病历
	public final static int CHUFANGYIZHU = 5;//处方医嘱
	public final static int XUECHANGGUI = 6;//化验单-血常规
}
