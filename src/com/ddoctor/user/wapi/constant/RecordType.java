package com.ddoctor.user.wapi.constant;

public class RecordType {

	/** 血糖记录 1 */
	public final static int TYPE_SUGAR = 1;
	/** 运动记录 2 */
	public final static int TYPE_SPORT = 2;
	/** 用药记录 3 */
	public final static int TYPE_MEDICINE = 3;
	/** 饮食记录 4 */
	public final static int TYPE_DIET = 4;
	/** 血压记录 5 */
	public final static int TYPE_BLOODPRESSURE = 5;
	/** 身高体重记录 6 */
	public final static int TYPE_HEIGHT = 6;
	/** 糖化血红蛋白记录 7 */
	public final static int TYPE_HBA1C = 7;

}
