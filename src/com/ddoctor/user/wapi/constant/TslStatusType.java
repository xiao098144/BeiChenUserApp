package com.ddoctor.user.wapi.constant;

// ***************** 注意，请勿直接覆盖，有自定义函数 ***************************
/**
 * 处方状态
 * @author zcli
 *
 */
public class TslStatusType {

	public final static int COMPLETEINFO_AUDIT_FAIL = 0;// 审核未过
	public final static int COMPLETEINFO_AUDIT_ING = 1;// 审核中
	public final static int COMPLETEINFO_AUDIT_SUCCESS = 2;// 审核通过
	
	public final static int PRESCRIPTION_AUDIT_ING = 1;// 审核中
	public final static int PRESCRIPTION_AUDIT_SUCCESS = 2;// 审核通过
	public final static int PRESCRIPTION_AUDIT_FAIL = 3;// 审核失败
	
	public final static int ORDER_CUSTOMER_CANCEL = 0;// 客户取消
	public final static int ORDER_NEW_IN = 1;// 新进订单
	public final static int ORDER_NEW_WAIT = 2;// 新单待审
	public final static int ORDER_AUDIT_SUCCESS = 3;// 已审待开
	public final static int ORDER_ALREADY_BILLING = 4;// 已经开单
	public final static int ORDER_ALREADY_DELIVER = 5;// 已经发货
	public final static int ORDER_AUDIT_FAIL = 6;// 已审未过
	public final static int ORDER_SERVIER_WASTE = 7;// 客服废单
	public final static int ORDER_CUSTOMER_REJECTION = 8;// 客户拒收
	public final static int ORDER_RETURN_PRODUCT = 9;// 退换货物
	public final static int ORDER_ALREADY_PRINT = 10;// 已经打印
	public final static int ORDER_CHECKOUT_AUDIT = 11;// 签出审核
	public final static int ORDER_ALREADY_SIGN = 12;// 已经签收
	
	
	public final static int SYNC_DATA_FAIL = -1;// 未同步或同步失败
	public final static int SYNC_DATA_SUCCESS = 0;// 同步成功
	
	
	public static String getOrderStatusName(int status) {
		String[] nameList = new String[] { "客户取消", "新进订单", "新单待审", "已审待开", "已经开单",
				"已经发货", "已审未过", "客服废单","客户拒收","退换货物","已经打印","签出审核","已经签收"};

		if (status < 0 || status >= nameList.length)
			return "未知状态";

		return nameList[status];
	}
	public static int getOrderStatusGroup(int status)
	{
		if( status == ORDER_AUDIT_SUCCESS 
				|| status == ORDER_ALREADY_BILLING
				|| status == ORDER_ALREADY_DELIVER
				|| status == ORDER_ALREADY_PRINT
				|| status == ORDER_CHECKOUT_AUDIT
				|| status == ORDER_ALREADY_SIGN )
			return 2; // green
		else if( status == ORDER_CUSTOMER_CANCEL 
				|| status == ORDER_AUDIT_FAIL
				|| status == ORDER_SERVIER_WASTE
				|| status == ORDER_CUSTOMER_REJECTION
				)
			return 1; // red
		else
			return 0; // blue
	}
	
	
}
