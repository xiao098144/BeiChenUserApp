package com.ddoctor.user.wapi.constant;

public class PayStatus {
	public final static int NOPAY = 0; // 0未支付
	public final static int UNCONFIRMED = 1; //1=待确认；
	public final static int PAYED = 2; //2=已支付；
}
