package com.ddoctor.user.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.user.wapi.bean.KnowlegeBean;

public class KnowledgeTypeListItemAdapter extends BaseAdapter<KnowlegeBean> {

	public KnowledgeTypeListItemAdapter(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if (null == convertView) {
			convertView = inflater.inflate(R.layout.layout_knowledgelist_item,
					parent, false);
			holder = new ViewHolder();
			holder.tv_name = (TextView) convertView
					.findViewById(R.id.knowledgelist_item_tv_name);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.tv_name.setText(dataList.get(position).getTitle());

		return convertView;
	}

	private class ViewHolder {
		private TextView tv_name;
	}

}
