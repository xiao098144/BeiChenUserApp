package com.ddoctor.user.adapter;

import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MultiChoiceHolder {

	public RelativeLayout layout;
	public TextView tv_name;
	public CheckBox cb;
	
}
