package com.ddoctor.user.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.user.model.FriendGroupListBean;

public class FriendGroupListAdapter extends BaseAdapter<FriendGroupListBean> {

	public FriendGroupListAdapter(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if (null == convertView) {
			convertView = inflater.inflate(
					R.layout.layout_friendgrouplist_item, parent, false);
			holder = new ViewHolder();
			holder.protrait = (ImageView) convertView
					.findViewById(R.id.friendgroup_item_protrait);
			holder.tv_group_name = (TextView) convertView
					.findViewById(R.id.friendgroup_item_name);
			holder.tv_time = (TextView) convertView
					.findViewById(R.id.friendgroup_item_time);
			holder.tv_describe = (TextView) convertView
					.findViewById(R.id.friendgroup_item_describe);
			holder.img_layout = (RelativeLayout) convertView
					.findViewById(R.id.friendgroup_item_img_layout);
			holder.img1 = (ImageView) convertView
					.findViewById(R.id.friendgroup_item_img1);
			holder.img2 = (ImageView) convertView
					.findViewById(R.id.friendgroup_item_img2);
			holder.tv_count = (TextView) convertView
					.findViewById(R.id.friendgroup_item_tv_count);
			holder.lastmsg_layout = (LinearLayout) convertView
					.findViewById(R.id.friendgroup_item_lastmsg_layout);
			holder.tv_lastmsg_name = (TextView) convertView
					.findViewById(R.id.friendgroup_item_lastmsg_name);
			holder.tv_lastmsg_content = (TextView) convertView
					.findViewById(R.id.friendgroup_item_lastmsg_content);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.tv_group_name.setText(dataList.get(position).getGroup_name());
		holder.tv_describe.setText(dataList.get(position).getDescribe());
		holder.tv_time.setText(dataList.get(position).getMsg_time());
		if (TextUtils.isEmpty(dataList.get(position).getPicUrl())) {
			holder.img_layout.setVisibility(View.GONE);
		}else {
			holder.img_layout.setVisibility(View.VISIBLE);
		}
		if (TextUtils.isEmpty(dataList.get(position).getLastmsg_name())) {
			holder.lastmsg_layout.setVisibility(View.GONE);
		}else {
			holder.lastmsg_layout.setVisibility(View.VISIBLE);
			holder.tv_lastmsg_name.setText(dataList.get(position).getLastmsg_name());
			holder.tv_lastmsg_content.setText(dataList.get(position).getLastmsg_content());
		}
		
		return convertView;
	}

	private class ViewHolder {

		private ImageView protrait;
		private TextView tv_group_name;
		private TextView tv_time;
		private TextView tv_describe;
		private RelativeLayout img_layout;
		private ImageView img1;
		private ImageView img2;
		private TextView tv_count;
		private LinearLayout lastmsg_layout;
		private TextView tv_lastmsg_name;
		private TextView tv_lastmsg_content;

	}

}
