package com.ddoctor.user.adapter;

import java.util.List;

import android.content.Context;
import android.text.TextUtils;
import android.util.SparseBooleanArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.user.wapi.bean.MioGlucoseMeterBean;
import com.ddoctor.utils.ImageLoaderUtil;
import com.ddoctor.utils.StringUtils;

public class MiOBoxSingleAdapter extends SingleChoiceAdapter<MioGlucoseMeterBean>{

	public MiOBoxSingleAdapter(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}
	
	public void setData(List<MioGlucoseMeterBean> dataList , String id) {
		this.setData(dataList);
		init(id);
	}
	
	
	private void init(String id) {
		isSelected = new SparseBooleanArray();
		if (TextUtils.isEmpty(id)) {
			id = "0";
		}
		if (dataList != null) {
			for (int i = 0; i < dataList.size(); i++) {
				if (id.equals(dataList.get(i).getId())) {
					isSelected.put(i, true);
					selectedData.put("typeId",dataList.get(i).getGlucoseMeterId());
					selectedData.put("position", i + "");
					selectedData.put("name", dataList.get(i).getCname());
				} else {
					isSelected.put(i, false);
					continue;
				}
			}
		}
		notifyDataSetChanged();
	}
	
	@Override
	public View getView(final int position, View view, ViewGroup parent) {
		SingleViewHolder holder = null;
		if (view == null) {
			view = inflater.inflate(R.layout.layout_miobox_glulist_item, parent, false);
			holder = new SingleViewHolder();
			holder.rb_check = (CheckBox) view.findViewById(R.id.cb);
			holder.tv_cname = (TextView) view.findViewById(R.id.item_tv_cz);
			holder.tv_enname = (TextView) view.findViewById(R.id.item_tv_en);
			holder.tv_enname.setVisibility(View.GONE);
			holder.img = (ImageView) view.findViewById(R.id.single_item_img);
			view.setTag(holder);
		} else {
			holder = (SingleViewHolder) view.getTag();
		}

		holder.rb_check
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						if (isChecked) {
							selectedData.clear();
							selectedData.put("typeId",dataList.get(position).getGlucoseMeterId());
							selectedData.put("position", position + "");
							selectedData.put("name", dataList.get(position)
									.getCname());
						}

					}
				});

		ImageLoaderUtil.display(StringUtils.urlFormatRemote(dataList.get(position).getGlucoseMeterImg()), holder.img, R.drawable.default_image);
		holder.tv_cname.setText(dataList.get(position).getCname()+"  "+dataList.get(position).getEname());
		holder.tv_cname.setTag(dataList.get(position).getId());
//		holder.tv_enname.setText(dataList.get(position).getEname());

		holder.rb_check.setChecked(isSelected.get(position));
		return view;
	}
	
	public class SingleViewHolder{
		public CheckBox rb_check;
		public TextView tv_cname;
		public TextView tv_enname;
		public ImageView img;
	}

}
