package com.ddoctor.user.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.user.wapi.bean.ContentBean;
import com.ddoctor.utils.ImageLoaderUtil;
import com.ddoctor.utils.StringUtils;
import com.ddoctor.utils.TimeUtil;

public class KnowledgeListAdapter extends BaseAdapter<ContentBean> {

	public KnowledgeListAdapter(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		ViewHolder holder = null;
		if (view == null) {
			view = inflater.inflate(R.layout.layout_knowlist_item, parent,
					false);
			holder = new ViewHolder();
			holder.img = (ImageView) view.findViewById(R.id.knowlist_item_img);
			holder.img.setScaleType(ScaleType.FIT_CENTER);
			holder.tv_title = (TextView) view
					.findViewById(R.id.knowlist_item_tv_title);
			holder.tv_time = (TextView) view
					.findViewById(R.id.knowlist_item_tv_time);
			view.setTag(holder);
		} else {
			holder = (ViewHolder) view.getTag();
		}

		if (!TextUtils.isEmpty(dataList.get(position).getImage())) {  
			holder.img.setImageDrawable(null);
			ImageLoaderUtil.display(StringUtils.urlFormatRemote(dataList.get(
					position).getImage()), holder.img,R.drawable.default_image);
		}

		holder.tv_title.setText(dataList.get(position).getTitle());
		holder.tv_time.setText(TimeUtil.getInstance().formatReplyTime2(
				dataList.get(position).getTime()));
		return view;
	}

	private class ViewHolder {
		private ImageView img;
		private TextView tv_title;
		private TextView tv_time;
	}

}
