package com.ddoctor.user.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.user.wapi.bean.IllnessBean;

public class SingleAdapter extends BaseAdapter<IllnessBean> {

	public SingleAdapter(Context context) {
		super(context);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		SingleHolder holder = null;
		if (null == convertView) {
			convertView = inflater.inflate(R.layout.layout_singlechoice_lvitem,
					parent, false);
			holder = new SingleHolder();
			holder.tv_name = (TextView) convertView
					.findViewById(R.id.singlechoice_tv);
			convertView.setTag(holder);
		} else {
			holder = (SingleHolder) convertView.getTag();
		}
		holder.tv_name.setText(dataList.get(position).getName());

		return convertView;
	}

}
