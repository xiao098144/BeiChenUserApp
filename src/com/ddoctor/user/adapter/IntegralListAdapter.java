package com.ddoctor.user.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.user.wapi.bean.PointBean;
import com.ddoctor.utils.MyUtils;
import com.ddoctor.utils.TimeUtil;

public class IntegralListAdapter extends BaseAdapter<PointBean> {

	public IntegralListAdapter(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		MyUtils.showLog("IntegralAdapter getView "+dataList.get(position).toString());
		ValueHolder holder = null;
		if (null == convertView) {
			convertView = inflater.inflate(
					R.layout.layout_record_item_nodelete, parent, false);
			holder = new ValueHolder();
			holder.tv_left = (TextView) convertView
					.findViewById(R.id.record_item_nodelete_tv_left);
			holder.tv_left.setVisibility(View.VISIBLE);
			holder.tv_middle = (TextView) convertView
					.findViewById(R.id.record_item_nodelete_tv_middle);
			holder.tv_middle.setVisibility(View.VISIBLE);
			holder.tv_right = (TextView) convertView
					.findViewById(R.id.record_item_nodelete_tv_right);
			holder.tv_right.setVisibility(View.VISIBLE);
			convertView.setTag(holder);
		} else {
			holder = (ValueHolder) convertView.getTag();
		}
		holder.tv_left.setText(TimeUtil.getInstance().timeStrFormat(
				dataList.get(position).getTime(), "yyyy-MM-dd HH:mm:ss",
				"MM/dd"));
		holder.tv_middle.setText(dataList.get(position).getOperation());
		Integer addValue = dataList.get(position).getAddValue();
		Integer reduceValue = dataList.get(position).getReduceValue();
		if ((addValue==null || addValue==0)&&(reduceValue!=null)) {
			holder.tv_right.setText(String.format("-%d", reduceValue));
		} else if((reduceValue==null || reduceValue==0)&&(addValue!=null)){
			holder.tv_right.setText(String.format("+%d", addValue));
		}
		return convertView;
	}

}
