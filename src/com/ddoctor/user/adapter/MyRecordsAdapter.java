package com.ddoctor.user.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.enums.RecordLayoutType;
import com.ddoctor.user.wapi.bean.RecordBean;
import com.ddoctor.user.wapi.constant.RecordType;
import com.ddoctor.utils.MyUtils;

public class MyRecordsAdapter extends BaseAdapter<RecordBean> {

	public MyRecordsAdapter(Context context) {
		super(context);
	}

	@Override
	public int getItemViewType(int position) {
		if (RecordLayoutType.TYPE_CATEGORY == dataList.get(position)
				.getLayoutType()) {
			return RecordLayoutType.TYPE_CATEGORY.ordinal();
		} else {
			return RecordLayoutType.TYPE_VALUE.ordinal();
		}
	}

	@Override
	public int getViewTypeCount() {
		return 2;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		RecordLayoutType layoutType = RecordLayoutType.values()[getItemViewType(position)];
		switch (layoutType) {
		case TYPE_CATEGORY: {
			CategoryHolder category = null;
			if (null == convertView) {
				convertView = inflater.inflate(
						R.layout.layout_recordlist_date_item, parent, false);
				category = new CategoryHolder();
				category.tv_date = (TextView) convertView
						.findViewById(R.id.recordlist_date_item_tv_date);
				convertView.setTag(category);
			} else {
				category = (CategoryHolder) convertView.getTag();
			}
			category.tv_date.setText(dataList.get(position).getDate());
		}
			break;
		case TYPE_VALUE: {
			ValueHolder valueHolder = null;
			if (null == convertView) {
				convertView = inflater.inflate(R.layout.layout_recordlist_item,
						parent, false);
				valueHolder = new ValueHolder();
				valueHolder.img = (ImageView) convertView
						.findViewById(R.id.record_item_img);
				valueHolder.tv_left = (TextView) convertView
						.findViewById(R.id.record_item_tv_left);
				valueHolder.tv_middle = (TextView) convertView
						.findViewById(R.id.record_item_tv_middle);
				valueHolder.tv_right = (TextView) convertView
						.findViewById(R.id.record_item_tv_right);
				convertView.setTag(valueHolder);
			} else {
				valueHolder = (ValueHolder) convertView.getTag();
			}

			Integer valueType = dataList.get(position).getValueType();
			MyUtils.showLog("Adapter  getView valueType == "+String.valueOf(valueType));
			int imgRes = 0;
			if (valueType != null) {
				switch (valueType) {
				case RecordType.TYPE_SUGAR: {
					imgRes = R.drawable.type_sugar;
				}
					break;
				case RecordType.TYPE_SPORT: {
					imgRes = R.drawable.type_sport;
				}
					break;
				case RecordType.TYPE_MEDICINE: {
					RelativeLayout.LayoutParams params =  (LayoutParams) valueHolder.tv_middle.getLayoutParams();
					params.addRule(RelativeLayout.RIGHT_OF, valueHolder.tv_left.getId());
					params.addRule(RelativeLayout.LEFT_OF, valueHolder.tv_right.getId());
					imgRes = R.drawable.type_medicine;
				}
					break;
				case RecordType.TYPE_DIET: {
					imgRes = R.drawable.type_diet;
				}
					break;
				case RecordType.TYPE_BLOODPRESSURE: {
					imgRes = R.drawable.type_bloodpressure;
				}
					break;
				case RecordType.TYPE_HEIGHT: {
					imgRes = R.drawable.type_height;
				}
					break;
				case RecordType.TYPE_HBA1C: {
					imgRes = R.drawable.type_hba1c;
				}
					break;

				default:
					break;
				}
			}
			valueHolder.img.setBackgroundResource(0);
			if (imgRes != 0) {
				valueHolder.img.setBackgroundResource(imgRes);
			}else {
				valueHolder.img.setBackgroundResource(R.drawable.default_image);
			}
			valueHolder.img.setVisibility(View.VISIBLE);
			
			if (valueType != null && valueType == RecordType.TYPE_DIET) {
				valueHolder.tv_left.setText(dataList.get(position).getCol1());
				valueHolder.tv_right.setText(dataList.get(position).getCol2());
				valueHolder.tv_middle.setText("");
			} else {
				valueHolder.tv_left.setText(dataList.get(position).getCol1());
				valueHolder.tv_middle.setText(dataList.get(position).getCol2());
				valueHolder.tv_right.setText(dataList.get(position).getCol3());
			}
			

		}
			break;
		default:
			break;
		}
		return convertView;
	}

	private class ValueHolder {
		private ImageView img;
		private TextView tv_left;
		private TextView tv_middle;
		private TextView tv_right;
	}

	@Override
	public boolean areAllItemsEnabled() {
		// TODO Auto-generated method stub
		return super.areAllItemsEnabled();
	}

	@Override
	public boolean isEnabled(int position) {
		if (RecordLayoutType.TYPE_CATEGORY == dataList.get(position)
				.getLayoutType()) {
			return false;
		}
		return super.isEnabled(position);
	}

}
