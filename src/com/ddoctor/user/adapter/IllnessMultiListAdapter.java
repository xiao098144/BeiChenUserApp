package com.ddoctor.user.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.SparseArray;
import android.util.SparseBooleanArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.user.wapi.bean.IllnessBean;

/**
 *Date:2015-6-23下午10:34:10
 *Author:Administrator
 *TODO:TODO
 */
public class IllnessMultiListAdapter extends MultiChoiceAdapter<IllnessBean> {

	public IllnessMultiListAdapter(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void setData(List<IllnessBean> dataList,
			List<IllnessBean> chosed) {
		super.setData(dataList, chosed);
		init();
	}

	private void init() {
		isSelected = new SparseBooleanArray();
		if (dataList != null && dataList.size() > 0) {
			for (int i = 0; i < dataList.size(); i++) {
				isSelected.put(i, false);
				if (chosed != null && chosed.size() > 0) {
					for (int j = 0; j < chosed.size(); j++) {
						if (dataList.get(i).getId()
								.equals(chosed.get(j).getId())) {
							list.put(i, dataList.get(i));
							isSelected.put(i, true);
						} else {
							continue;
						}
					}
				}
			}
		}
		notifyDataSetChanged();
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		MultiChoiceHolder holder = null;
		if (null == convertView) {
			convertView = inflater.inflate(R.layout.layout_multichoice, parent,
					false);
			holder = new MultiChoiceHolder();
			holder.layout = (RelativeLayout) convertView
					.findViewById(R.id.multi_layout);
			holder.tv_name = (TextView) convertView
					.findViewById(R.id.multi_tv_name);
			holder.cb = (CheckBox) convertView.findViewById(R.id.multi_cb);
			convertView.setTag(holder);
		} else {
			holder = (MultiChoiceHolder) convertView.getTag();
		}
		holder.tv_name.setText(dataList.get(position).getName());
		holder.tv_name.setTag(R.id.click_type, dataList.get(position).getId());
		holder.cb.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				isSelected.put(position, isChecked);
				if (isChecked) {
					list.put(position, dataList.get(position));
				} else {
					list.delete(position);
				}
			}
		});
		holder.cb.setChecked(isSelected.get(position));
		
		return convertView;
	}

	@Override
	public SparseBooleanArray getIsSelected() {
		// TODO Auto-generated method stub
		return isSelected;
	}

	@Override
	public void setIsSelected(SparseBooleanArray isSelected) {
		this.isSelected = isSelected;
	}

	@Override
	public SparseArray<IllnessBean> getSelectedItemSparseArray() {
		// TODO Auto-generated method stub
		return list;
	}

	@Override
	public ArrayList<IllnessBean> getSelectedItemList() {
		// TODO Auto-generated method stub
		return super.getSelectedItemList();
	}

//	public List<IllnessBean> getSelectedItem(){
//		List<IllnessBean> list = new ArrayList<IllnessBean>();
//		for (int i = 0; i < dataList.size(); i++) {
//			if (isSelected.get(i)) {
//				list.add(dataList.get(i));
//			}
//		}
//		return list;
//	}
	
	public String getSelectedText() {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < dataList.size(); i++) {
			if (isSelected.get(i)) {
				sb.append(dataList.get(i).getName());
				sb.append("、");
			}
		}
		String str = sb.toString();
		if (str.endsWith("、")) {
			str = str.substring(0,str.length() - 1);
		}
		return str;
	}
	
}

		