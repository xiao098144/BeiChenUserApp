package com.ddoctor.user.adapter;

import java.util.Locale;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.enums.RecordLayoutType;
import com.ddoctor.user.wapi.bean.SportBean;
import com.ddoctor.utils.MyUtils;

public class SportRecordAdapter extends BaseAdapter<SportBean> {

	private String[] type;

	public SportRecordAdapter(Context context) {
		super(context);
		type = new String[] { "散步", "慢跑" };
	}

	@Override
	public int getViewTypeCount() {
		return 2;
	}

	@Override
	public int getItemViewType(int position) {
		if (RecordLayoutType.TYPE_CATEGORY == dataList.get(position)
				.getLayoutType()) {
			return RecordLayoutType.TYPE_CATEGORY.ordinal();
		} else {
			return RecordLayoutType.TYPE_VALUE.ordinal();
		}
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		RecordLayoutType layoutType = RecordLayoutType.values()[getItemViewType(position)];
		switch (layoutType) {
		case TYPE_CATEGORY: {
			CategoryHolder category = null;
			if (null == convertView) {
				convertView = inflater.inflate(
						R.layout.layout_recordlist_date_item, parent, false);
				category = new CategoryHolder();
				category.tv_date = (TextView) convertView
						.findViewById(R.id.recordlist_date_item_tv_date);
				convertView.setTag(category);
			} else {
				category = (CategoryHolder) convertView.getTag();
			}
			category.tv_date.setText(dataList.get(position).getRecorddate());
		}
			break;
		case TYPE_VALUE: {
			ValueHolder valueHolder = null;
			if (null == convertView) {
				convertView = inflater.inflate(R.layout.layout_recordlist_item,
						parent, false);
				valueHolder = new ValueHolder();
				valueHolder.tv_left = (TextView) convertView
						.findViewById(R.id.record_item_tv_left);
				valueHolder.tv_middle = (TextView) convertView
						.findViewById(R.id.record_item_tv_middle);
				valueHolder.tv_right = (TextView) convertView
						.findViewById(R.id.record_item_tv_right);
				convertView.setTag(valueHolder);
			} else {
				valueHolder = (ValueHolder) convertView.getTag();
			}
			Integer type2 = dataList.get(position).getType();
			if (type2 ==null || type2>2) {
				type2 =0;
			}
			if (type2==2) {
				type2=1;
			}
			valueHolder.tv_left.setText(type[type2]);
			valueHolder.tv_middle.setText(getTime(dataList.get(position)
					.getTime()));
			Integer distance = dataList.get(position).getDistance();
			String value ;
			if (distance == null) {
				distance = 1;
			}
			if (distance>1000) {
				value = String.format(Locale.CHINA,"%.2f千米", ((float)distance)/1000);
			}else {
				value = String.format(Locale.CHINA,"%d米", distance);
			}
			valueHolder.tv_right.setText(value);
		}
			break;
		default:
			break;
		}

		return convertView;
	}

	private String getTime(String timeStr) {
		if (TextUtils.isEmpty(timeStr)) {
			return "";
		}
		MyUtils.showLog("时间转分秒  " + timeStr);
		String[] split = timeStr.split(":");

		int minute = Integer.valueOf(split[0]);
		int second = 0;
		if (split.length > 1) {
			second = Integer.valueOf(split[1]);
		}  
		SpannableStringBuilder sb = new SpannableStringBuilder();
		String s = null;
		if (minute > 0) {
			s = minute + "";
			sb.append(s);
			sb.append("分");
			sb.setSpan(new AbsoluteSizeSpan(13), 0, s.length() - 1,
					Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		}
		String sa = second + "";
		sb.append(sa);
		sb.append("秒");

		sb.setSpan(new AbsoluteSizeSpan(13), s == null ? 0 : s.length() + 1,
				s == null ? sa.length() - 1 : s.length() + sa.length(),
				Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		return sb.toString();
	}

	@Override
	public boolean areAllItemsEnabled() {
		return false;
	}

	@Override
	public boolean isEnabled(int position) {
		if (RecordLayoutType.TYPE_CATEGORY == dataList.get(position)
				.getLayoutType()) {
			return false;
		} else {
			return true;
		}
	}

	private class CategoryHolder {
		private TextView tv_date;
	}

	private class ValueHolder {
		private TextView tv_left;
		private TextView tv_middle;
		private TextView tv_right;
	}

}
