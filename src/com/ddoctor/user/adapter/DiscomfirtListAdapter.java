package com.ddoctor.user.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.enums.RecordLayoutType;
import com.ddoctor.user.wapi.bean.TroubleBean;

public class DiscomfirtListAdapter extends BaseAdapter<TroubleBean> {

	private Context context;

	public DiscomfirtListAdapter(Context context) {
		super(context);
		this.context = context;
	}

	@Override
	public int getItemViewType(int position) {
		if (dataList != null
				&& dataList.size() > 0
				&& RecordLayoutType.TYPE_CATEGORY == dataList.get(position)
						.getLayoutType()) {
			return RecordLayoutType.TYPE_CATEGORY.ordinal();
		} else {
			return RecordLayoutType.TYPE_VALUE.ordinal();
		}
	}

	@Override
	public int getViewTypeCount() {
		return 2;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		RecordLayoutType layoutType = RecordLayoutType.values()[getItemViewType(position)];
		switch (layoutType) {
		case TYPE_CATEGORY: {
			CategoryHolder category = null;
			if (null == convertView) {
				convertView = inflater.inflate(
						R.layout.layout_recordlist_date_item, parent, false);
				category = new CategoryHolder();
				category.tv_date = (TextView) convertView
						.findViewById(R.id.recordlist_date_item_tv_date);
				convertView.setTag(category);
			} else {
				category = (CategoryHolder) convertView.getTag();
			}
			category.tv_date.setText(dataList.get(position).getDate());
		}
			break;
		case TYPE_VALUE: {
			ViewHolder holder = null;
			if (null == convertView) {
				convertView = inflater.inflate(
						R.layout.layout_discomfirtlist_item, parent, false);
				holder = new ViewHolder();
				holder.tv = (TextView) convertView
						.findViewById(R.id.discomfirt_item_tv);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			String item = dataList.get(position).getItem();
			if (item.contains("|")) {
				item = item.replace("|", ",");
			}
			holder.tv.setText(String.format(
					context.getResources().getString(
							R.string.discomfirt_fortmat), item));
		}
			break;
		default:
			break;
		}
		return convertView;
	}

	private class ViewHolder {
		private TextView tv;
	}

	@Override
	public boolean areAllItemsEnabled() {
		// TODO Auto-generated method stub
		return super.areAllItemsEnabled();
	}

	@Override
	public boolean isEnabled(int position) {

		if (dataList != null
				&& dataList.size() > 0
				&& RecordLayoutType.TYPE_CATEGORY == dataList.get(position)
						.getLayoutType()) {
			return false;
		}
		return super.isEnabled(position);
	}

}
