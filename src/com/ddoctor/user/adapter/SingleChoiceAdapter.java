package com.ddoctor.user.adapter;

import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import android.util.SparseBooleanArray;
import android.view.View;
import android.view.ViewGroup;

public class SingleChoiceAdapter<T> extends BaseAdapter<T> {

	public SparseBooleanArray isSelected;

	public Map<String, String> selectedData = new HashMap<String, String>();
	
	public SingleChoiceAdapter(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return super.getView(position, convertView, parent);
	}
	
	public Map<String, String> getSelectedData() {
		return selectedData;
	}

	public SparseBooleanArray getIsSelected() {
		return isSelected;
	}

	public void setSelected(int position) {
		for (int i = 0; i < isSelected.size(); i++) {
			isSelected.put(i, false);
		}
		// 再将当前选择CB的实际状态
		isSelected.put(position, true);
		notifyDataSetChanged();
	}

	public void setIsSelected(SparseBooleanArray isSelected) {
		this.isSelected = isSelected;
	}

}
