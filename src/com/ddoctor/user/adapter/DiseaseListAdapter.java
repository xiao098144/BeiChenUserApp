package com.ddoctor.user.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.enums.RecordLayoutType;
import com.ddoctor.user.wapi.bean.DiseaseBean;
import com.ddoctor.utils.ImageLoaderUtil;
import com.ddoctor.utils.StringUtils;

public class DiseaseListAdapter extends BaseAdapter<DiseaseBean> {

	public DiseaseListAdapter(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	@Override
	public int getItemViewType(int position) {
		if (dataList != null
				&& dataList.size() > 0
				&& RecordLayoutType.TYPE_CATEGORY == dataList.get(position)
						.getLayoutType()) {
			return RecordLayoutType.TYPE_CATEGORY.ordinal();
		} else {
			return RecordLayoutType.TYPE_VALUE.ordinal();
		}
	}

	@Override
	public int getViewTypeCount() {
		return 2;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		RecordLayoutType layoutType = RecordLayoutType.values()[getItemViewType(position)];
		switch (layoutType) {
		case TYPE_CATEGORY: {
			CategoryHolder category = null;
			if (null == convertView) {
				convertView = inflater.inflate(
						R.layout.layout_recordlist_date_item, parent, false);
				category = new CategoryHolder();
				category.tv_date = (TextView) convertView
						.findViewById(R.id.recordlist_date_item_tv_date);
				convertView.setTag(category);
			} else {
				category = (CategoryHolder) convertView.getTag();
			}
			category.tv_date.setText(dataList.get(position).getDate());
		}
			break;
		case TYPE_VALUE: {
			ViewHolder holder = null;
			if (null == convertView) {
				convertView = inflater.inflate(
						R.layout.layout_diseaselist_item, parent, false);
				holder = new ViewHolder();
				holder.tv_type = (TextView) convertView
						.findViewById(R.id.disease_item_tv_type);
				holder.tv_content = (TextView) convertView
						.findViewById(R.id.disease_item_tv_content);
				holder.hsc = (HorizontalScrollView) convertView
						.findViewById(R.id.disease_item_lv);
				holder.img1 = (ImageView) convertView
						.findViewById(R.id.disease_item_img1);
				holder.img2 = (ImageView) convertView
						.findViewById(R.id.disease_item_img2);
				holder.img3 = (ImageView) convertView
						.findViewById(R.id.disease_item_img3);
				holder.img4 = (ImageView) convertView
						.findViewById(R.id.disease_item_img4);
				holder.img5 = (ImageView) convertView
						.findViewById(R.id.disease_item_img5);
				holder.img6 = (ImageView) convertView
						.findViewById(R.id.disease_item_img6);
				holder.img7 = (ImageView) convertView
						.findViewById(R.id.disease_item_img7);
				holder.img8 = (ImageView) convertView
						.findViewById(R.id.disease_item_img8);
				holder.img9 = (ImageView) convertView
						.findViewById(R.id.disease_item_img9);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

//			holder.tv_type.setText(String.format("病程分类：%1s",
//					getTypeNameById(dataList.get(position).getType())));
			holder.tv_content.setText(dataList.get(position).getContent());

			ImageView[] img = new ImageView[] { holder.img1, holder.img2,
					holder.img3, holder.img4, holder.img5, holder.img6,
					holder.img7, holder.img8, holder.img9 };

			for (int i = 0; i < img.length; i++) {
				img[i].setVisibility(View.GONE);
			}

			String file = dataList.get(position).getFile();
			if (TextUtils.isEmpty(file)) {
				holder.hsc.setVisibility(View.GONE);
			} else {
				holder.hsc.setVisibility(View.VISIBLE);
				String[] url = file.split("\\|");
				for (int i = 0; i < url.length; i++) {
					img[i].setVisibility(View.VISIBLE);
					ImageLoaderUtil.display(
							StringUtils.urlFormatThumbnail(url[i]), img[i],
							R.drawable.rc_image_download_failure);
				}
			}
		}
			break;
		default:
			break;
		}
		return convertView;
	}

	private class ViewHolder {
		private TextView tv_type, tv_content;
		private HorizontalScrollView hsc;
		private ImageView img1, img2, img3, img4, img5, img6, img7, img8, img9;
	}

//	private String getTypeNameById(int typeId) {
//		switch (typeId) {
//		case DiseaseType.SHOUZHEN:
//			return "首诊";
//		case DiseaseType.FUZHEN:
//			return "复诊";
//		case DiseaseType.HUAYANDAN:
//			return "化验单";
//		case DiseaseType.BINGLI:
//			return "病历";
//		case DiseaseType.CHUFANGYIZHU:
//			return "处方医嘱";
//		case DiseaseType.XUECHANGGUI:
//			return "化验单-血常规";
//		default:
//			break;
//		}
//		return "";
//	}

	@Override
	public boolean areAllItemsEnabled() {
		// TODO Auto-generated method stub
		return super.areAllItemsEnabled();
	}

	@Override
	public boolean isEnabled(int position) {

		if (dataList != null
				&& dataList.size() > 0
				&& RecordLayoutType.TYPE_CATEGORY == dataList.get(position)
						.getLayoutType()) {
			return false;
		}
		return super.isEnabled(position);
	}

}
