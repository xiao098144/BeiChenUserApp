package com.ddoctor.user.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * 基础适配器 （纯文字）
 * @author 萧
 * @Date 2015-5-8下午2:13:54
 * @TODO TODO
 */
public class BaseAdapter<T> extends android.widget.BaseAdapter {
	public LayoutInflater inflater;
	protected Context _context = null;

	public BaseAdapter(Context context) {
		_context = context;
		inflater = LayoutInflater.from(context);
	}
	
	public Context getContext(){
		return _context;
	}

	public List<T> dataList;

	public void setData(List<T> dataList) {
		this.dataList = dataList;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return dataList == null ? 0 : dataList.size();
	}

	@Override
	public Object getItem(int position) {
		return dataList == null ? null : dataList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		return convertView;
	}

}
