package com.ddoctor.user.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.enums.RecordLayoutType;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.wapi.bean.SugarValueBean;
import com.ddoctor.utils.MyUtils;
import com.ddoctor.utils.TimeUtil;

public class SugarRecordAdapter extends BaseAdapter<SugarValueBean> {

	private Drawable high;
	private Drawable low;
	private int color_error;
	private float high_value ;
	private float low_value ;
	
	private String[] sugarType ;

	private Context context;
	@SuppressWarnings("deprecation")
	public SugarRecordAdapter(Context context) {
		super(context);
		this.context =context;
		high_value = DataModule.getInstance().getSugarUpBound();
		low_value = DataModule.getInstance().getSugarDownBound();
		high = context.getResources().getDrawable(R.drawable.sugar_high);
		low = context.getResources().getDrawable(R.drawable.sugar_low);
		color_error = context.getResources().getColor(
				R.color.color_sugar_standard_high);
		sugarType = context.getResources().getStringArray(R.array.sugar_type);
	}

	@Override
	public int getViewTypeCount() {
		return 2;
	}

	@Override
	public int getItemViewType(int position) {
		if (dataList==null || dataList.size()==0) {
			return RecordLayoutType.TYPE_VALUE.ordinal();
		}
		if (RecordLayoutType.TYPE_CATEGORY == dataList.get(position).getLayoutType()) {
			return RecordLayoutType.TYPE_CATEGORY.ordinal();
		} else {
			return RecordLayoutType.TYPE_VALUE.ordinal();
		}
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		RecordLayoutType layoutType = RecordLayoutType.values()[getItemViewType(position)];
		switch (layoutType) {
		case TYPE_CATEGORY: {
			CategoryHolder category = null;
			if (null == convertView) {
				convertView = inflater.inflate(
						R.layout.layout_recordlist_date_item, parent, false);
				category = new CategoryHolder();
				category.tv_date = (TextView) convertView
						.findViewById(R.id.recordlist_date_item_tv_date);
				convertView.setTag(category);
			} else {
				category = (CategoryHolder) convertView.getTag();
			}
			category.tv_date.setText(dataList.get(position).getDate());
		}
			break;
		case TYPE_VALUE: {
			ValueHolder valueHolder = null;
			if (null == convertView) {
				convertView = inflater.inflate(R.layout.layout_sugarlist_item,
						parent, false);
				valueHolder = new ValueHolder();
				valueHolder.tv_left = (TextView) convertView
						.findViewById(R.id.sugarlist_item_tv_left);
				valueHolder.tv_middle = (TextView) convertView
						.findViewById(R.id.sugarlist_item_tv_middle);
				valueHolder.tv_right = (TextView) convertView
						.findViewById(R.id.sugarlist_item_tv_right);
				valueHolder.tv_unit = (TextView) convertView.findViewById(R.id.sugarlist_item_unit);
				convertView.setTag(valueHolder);
			} else {
				valueHolder = (ValueHolder) convertView.getTag();
			}
			valueHolder.tv_left.setText(TimeUtil.getInstance().formatTime(dataList.get(position).getTime()));
			
			Float value = dataList.get(position).getValue();
			if (value > high_value) {
				valueHolder.tv_right.setCompoundDrawablesWithIntrinsicBounds(
						high, null, null, null);
				valueHolder.tv_right.setTextColor(color_error);
			} else if (value < low_value) {
				valueHolder.tv_right.setCompoundDrawablesWithIntrinsicBounds(
						low, null, null, null);
				valueHolder.tv_right.setTextColor(color_error);
			}else {
				valueHolder.tv_right.setTextColor(context.getResources().getColor(R.color.color_text_gray_dark));
				valueHolder.tv_right.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
			}  
			valueHolder.tv_right.setText(value+"");
			valueHolder.tv_unit.setText(1 == dataList.get(position).getUnit()?"mg.dl":"mmol/L");
			
			if (dataList.get(position).getSourceType()!=null && dataList.get(position).getSourceType()==1) {
				String time = dataList.get(position).getTime();
				Integer hour = Integer.valueOf(time.substring(11, 13));
				valueHolder.tv_middle.setText(getTimeTypeIndex(hour));
			} else {
				valueHolder.tv_middle.setText(getTypeById(dataList.get(position).getType()==null?0:dataList.get(position).getType()));
			}
			
		}
			break;
		default:
			break;
		}

		return convertView;
	}

	private String getTypeById(int id){
		id = Math.abs(id);
		if (id==7) {
			id=0;
		}else if (id<7) {
			id+=1;
		}
		MyUtils.showLog(" getTypeById id == "+id+" sugarType[id] == "+sugarType[id]);
		return sugarType[id];
	}
	
	/** 根据时间值获取对应索引值 */
	public String getTimeTypeIndex(int hour) {

		if (hour >= 5 && hour <= 8) { // 早餐前
			return "空腹";
		} else if (hour > 8 && hour < 10) { // 早餐后
			return "早餐后";
		} else if (hour >= 10 && hour < 12) { // 午餐前
			return "午餐前";
		} else if (hour >= 12 && hour < 15) { // 午餐后
			return "午餐后";
		} else if (hour >= 15 && hour <= 18) { // 晚餐前
			return "晚餐前";
		} else if (hour > 18 && hour <= 21) { // 晚餐后
			return "晚餐后";
		} else if (hour > 21 && hour < 24) {
			return "睡前"; // 睡前
		}
		return "凌晨"; // 凌晨
	}
	
	@Override
	public boolean areAllItemsEnabled() {
		return false;
	}

	@Override
	public boolean isEnabled(int position) {
		if (RecordLayoutType.TYPE_CATEGORY == dataList.get(position).getLayoutType()) {
			return false;
		} else {
			return true;
		}
	}

	private class ValueHolder {
		private TextView tv_left;
		private TextView tv_middle;
		private TextView tv_right;
		private TextView tv_unit;
	}

}
