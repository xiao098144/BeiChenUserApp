package com.ddoctor.user.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.user.wapi.bean.CatagoryBean;
import com.ddoctor.utils.ImageLoaderUtil;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

public class KnowledgeCatagoryAdapter extends BaseAdapter<CatagoryBean> {

	public KnowledgeCatagoryAdapter(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if (null == convertView) {
			convertView = inflater.inflate(R.layout.layout_tvimg_item, parent,
					false);
			holder = new ViewHolder();
			holder.img = (ImageView) convertView
					.findViewById(R.id.tvimg_item_img);
			holder.tv = (TextView) convertView.findViewById(R.id.tvimg_item_tv);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.tv.setText(dataList.get(position).getName());
		holder.tv.setTag(R.id.click_type, dataList.get(position).getId());

		ImageLoaderUtil.displayListener(dataList.get(position).getImgUrl(),
				holder.img, R.drawable.default_image,
				new ImageLoadingListener() {

					@Override
					public void onLoadingStarted(String arg0, View arg1) {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void onLoadingFailed(String arg0, View arg1,
							FailReason arg2) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onLoadingComplete(String arg0, View arg1,
							Bitmap arg2) {
//						MyUtils.showLog("准备设置 KnowledgeLibAdaptter onLoadingComplete width == "+arg2.getWidth()+" height == "+arg2.getHeight()+" view view.getWidth == "+arg1.getWidth()+" view.getHeight == "+arg1.getHeight());
						if (arg1!=null && arg2!=null && arg2.getWidth() == 200 && arg2.getHeight() == 200) {
							ViewGroup.LayoutParams params = (ViewGroup.LayoutParams) arg1
									.getLayoutParams();
							params.width = arg2.getWidth() / 2;
							params.height = arg2.getHeight() / 2;
						}
						
//						MyUtils.showLog(" 设置完毕   onLoadingComplete width == "+arg2.getWidth()+" height == "+arg2.getHeight()+  " view  view.getWidth == "+arg1.getWidth()+" view.getHeight == "+arg1.getHeight());
						if (arg1!=null && (dataList.get(position).getId()==null || dataList.get(position).getId() == 0)) {
							arg1.setVisibility(View.INVISIBLE);
						}
					}

					@Override
					public void onLoadingCancelled(String arg0, View arg1) {
						// TODO Auto-generated method stub

					}
				});

		return convertView;
	}

	private class ViewHolder {
		private TextView tv;
		private ImageView img;
	}

}
