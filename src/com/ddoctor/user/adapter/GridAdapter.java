package com.ddoctor.user.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.user.model.PicTextBean;

public class GridAdapter extends BaseAdapter<PicTextBean> {

	public GridAdapter(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if (null == convertView) {
			convertView = inflater.inflate(R.layout.layout_grid_item, parent,
					false);
			holder = new ViewHolder();
			holder.tv = (TextView) convertView
					.findViewById(R.id.grid_item_tv_title);
			holder.img = (ImageView) convertView
					.findViewById(R.id.grid_item_img);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.tv.setText(dataList.get(position).getText());
		holder.tv
				.setTextColor((dataList.get(position).getTextColor() == 0 ? Color.BLACK
						: dataList.get(position).getTextColor()));
		holder.img.setBackgroundResource(dataList.get(position).getImgResId());
		return convertView;
	}

	private class ViewHolder {

		TextView tv;
		ImageView img;

	}

}
