package com.ddoctor.user.adapter;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.enums.RecordLayoutType;
import com.ddoctor.user.wapi.bean.DietBean;
import com.ddoctor.user.wapi.bean.FoodBean;
import com.ddoctor.utils.ImageLoaderUtil;
import com.ddoctor.utils.StringUtils;
import com.ddoctor.utils.TimeUtil;

public class DietRecordAdapter extends BaseAdapter<DietBean> {

	private String[] mealType;

	public DietRecordAdapter(Context context) {
		super(context);
		mealType = context.getResources().getStringArray(R.array.meal_type);
	}

	@Override
	public int getItemViewType(int position) {
		if (dataList != null
				&& dataList.size() > 0
				&& RecordLayoutType.TYPE_CATEGORY == dataList.get(position)
						.getLayoutType()) {
			return RecordLayoutType.TYPE_CATEGORY.ordinal();
		} else {
			return RecordLayoutType.TYPE_VALUE.ordinal();
		}
	}

	@Override
	public int getViewTypeCount() {
		return 2;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		RecordLayoutType layoutType = RecordLayoutType.values()[getItemViewType(position)];
		switch (layoutType) {
		case TYPE_CATEGORY: {
			CategoryHolder category = null;
			if (null == convertView) {
				convertView = inflater.inflate(
						R.layout.layout_recordlist_date_item, parent, false);
				category = new CategoryHolder();
				category.tv_date = (TextView) convertView
						.findViewById(R.id.recordlist_date_item_tv_date);
				convertView.setTag(category);
			} else {
				category = (CategoryHolder) convertView.getTag();
			}
			category.tv_date.setText(dataList.get(position).getDate());
		}
			break;
		case TYPE_VALUE: {
			ValueHolder valueHolder = null;
			if (null == convertView) {
				convertView = inflater.inflate(R.layout.layout_dietrecord_item,
						parent, false);
				valueHolder = new ValueHolder();
				valueHolder.tv_meal_type = (TextView) convertView
						.findViewById(R.id.dietrecord_item_tv_meal_type);
				valueHolder.tv_record_time = (TextView) convertView
						.findViewById(R.id.dietrecord_item_tv_record_time);
				valueHolder.mealImg = (ImageView) convertView
						.findViewById(R.id.diet_base_img);
				valueHolder.tv_hot = (TextView) convertView
						.findViewById(R.id.diet_base_tv_hot_content);
				valueHolder.tv_mealDescribe = (TextView) convertView
						.findViewById(R.id.diet_base_tv_describe);
				convertView.setTag(valueHolder);
			} else {
				valueHolder = (ValueHolder) convertView.getTag();
			}
			if (dataList.get(position).getType()!=null) {
				valueHolder.tv_meal_type.setText(mealType[dataList.get(position).getType()-1]);
			}

			valueHolder.tv_mealDescribe.setText(getDescribe(position));

			valueHolder.tv_record_time
					.setText(TimeUtil.getInstance().formatReplyTime2(dataList.get(position).getTime()));
			
			Integer heat = dataList.get(position).getHeat();
			if (heat == null) {
				heat = 0;
			}
			valueHolder.tv_hot.setText(String.format("%dkcal", heat));
			ImageLoaderUtil.display(StringUtils.urlFormatThumbnail(dataList.get(position).getFile()),
					valueHolder.mealImg, R.drawable.food_pic);
		}
			break;
		default:
			break;
		}

		return convertView;
	}

	private String getDescribe(int position) {
		StringBuffer sb = new StringBuffer();

		List<FoodBean> gushuList = dataList.get(position).getGuShuList();
		List<FoodBean> shucaiList = dataList.get(position).getShuCaiList();
		List<FoodBean> shuiguoList = dataList.get(position).getShuiGuoList();
		List<FoodBean> dadouList = dataList.get(position).getDaDouList();
		List<FoodBean> rouDanList = dataList.get(position).getRouDanList();
		List<FoodBean> ruList = dataList.get(position).getRuList();
		List<FoodBean> yingguoList = dataList.get(position).getYingGuoList();
		List<FoodBean> youzhiList = dataList.get(position).getYouZhiList();
		
		if (gushuList != null && gushuList.size() > 0) {
			for (int i = 0; i < gushuList.size(); i++) {
				sb.append(gushuList.get(i).getName());
				sb.append(gushuList.get(i).getCount()+gushuList.get(i).getUnit());
				sb.append("、");
			}
		}

		if (shucaiList != null && shucaiList.size() > 0) {
			for (int i = 0; i < shucaiList.size(); i++) {
				sb.append(shucaiList.get(i).getName());
				sb.append(shucaiList.get(i).getCount()+shucaiList.get(i).getUnit());
				sb.append("、");
			}
		}
		if (shuiguoList != null && shuiguoList.size() > 0) {
			for (int i = 0; i < shuiguoList.size(); i++) {
				sb.append(shuiguoList.get(i).getName());
				sb.append(shuiguoList.get(i).getCount()+shuiguoList.get(i).getUnit());
				sb.append("、");
			}
		}
		if (dadouList != null && dadouList.size() > 0) {
			for (int i = 0; i < dadouList.size(); i++) {
				sb.append(dadouList.get(i).getName());
				sb.append(dadouList.get(i).getCount()+dadouList.get(i).getUnit());
				sb.append("、");
			}
		}
		if (ruList != null && ruList.size() > 0) {
			for (int i = 0; i < ruList.size(); i++) {
				sb.append(ruList.get(i).getName());
				sb.append(ruList.get(i).getCount()+ruList.get(i).getUnit());
				sb.append("、");
			}
		}
		if (rouDanList != null && rouDanList.size() > 0) {
			for (int i = 0; i < rouDanList.size(); i++) {
				sb.append(rouDanList.get(i).getName());
				sb.append(rouDanList.get(i).getCount()+rouDanList.get(i).getUnit());
				sb.append("、");
			}
		}
		if (yingguoList != null && yingguoList.size() > 0) {
			for (int i = 0; i < yingguoList.size(); i++) {
				sb.append(yingguoList.get(i).getName());
				sb.append(yingguoList.get(i).getCount()+yingguoList.get(i).getUnit());
				sb.append("、");
			}
		}
		if (youzhiList != null && youzhiList.size() > 0) {
			for (int i = 0; i < youzhiList.size(); i++) {
				sb.append(youzhiList.get(i).getName());
				sb.append(youzhiList.get(i).getCount()+youzhiList.get(i).getUnit());
				sb.append("、");
			}
		}
		if (sb != null && sb.toString().length()>2&&sb.toString().endsWith("、")) {
			return sb.toString().substring(0, sb.toString().length() - 1);
		}
		return sb.toString();
	}

	@Override  
	public boolean areAllItemsEnabled() {
		// TODO Auto-generated method stub
		return super.areAllItemsEnabled();
	}

	@Override
	public boolean isEnabled(int position) {

		if (dataList != null
				&& dataList.size() > 0
				&& RecordLayoutType.TYPE_CATEGORY == dataList.get(position)
						.getLayoutType()) {
			return false;
		}
		return super.isEnabled(position);
	}

	private class ValueHolder {
		private TextView tv_meal_type;
		/** 记录时间 */
		private TextView tv_record_time;
		/** 仅显示第一张图片 */
		private ImageView mealImg;
		/** 具体描述 */
		private TextView tv_mealDescribe;
		private TextView tv_hot;
	}

}
