package com.ddoctor.user.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.SparseArray;
import android.util.SparseBooleanArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.user.model.MyFriendBean;
import com.ddoctor.user.view.CircleImageView;
import com.ddoctor.utils.MyUtils;

public class MyFriendsListAdapter extends BaseAdapter<MyFriendBean> {

	public MyFriendsListAdapter(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	private SparseBooleanArray isSelected;
	private SparseArray<MyFriendBean> list = new SparseArray<MyFriendBean>();
	// private ArrayList<MyFriendBean> list = new ArrayList<MyFriendBean>();
	private List<MyFriendBean> chosed = new ArrayList<MyFriendBean>();

	boolean showCheckBox = false;

	public MyFriendsListAdapter(Context context, boolean showCheckBox) {
		this(context);
		this.showCheckBox = showCheckBox;
	}

	public void setData(List<MyFriendBean> dataList, List<MyFriendBean> chosed) {
		this.setData(dataList);
		this.chosed = chosed;
		init();
	}

	private void init() {
		isSelected = new SparseBooleanArray();
		if (dataList != null && chosed != null) {
			for (int i = 0; i < dataList.size(); i++) {
				isSelected.put(i, false);
				for (int j = 0; j < chosed.size(); j++) {
					if (dataList.get(i).getId().equals(chosed.get(j).getId())) {
						list.put(i, (MyFriendBean) dataList.get(i).clone());
						// list.add(i, (MyFriendBean) dataList.get(i).clone());
						isSelected.put(i, true);
					} else {
						continue;
					}
				}
			}
		}
		notifyDataSetChanged();
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		FriendListViewHolder holder = null;
		if (null == convertView) {
			convertView = inflater.inflate(R.layout.layout_myfriendlist_item,
					parent, false);
			holder = new FriendListViewHolder();
			holder.img = (CircleImageView) convertView
					.findViewById(R.id.myfriend_item_img);
			holder.tv_name = (TextView) convertView
					.findViewById(R.id.myfriend_item_tv_name);
			holder.tv_content = (TextView) convertView
					.findViewById(R.id.myfriend_item_tv_content);
			holder.cb = (CheckBox) convertView
					.findViewById(R.id.myfriend_item_cb);
			if (showCheckBox) {
				holder.cb.setVisibility(View.VISIBLE);
			} else {
				holder.cb.setVisibility(View.GONE);
			}
			convertView.setTag(holder);
		} else {
			holder = (FriendListViewHolder) convertView.getTag();
		}

		holder.tv_name.setText(dataList.get(position).getDocName());
		holder.tv_content.setText(dataList.get(position).getContent());
		if (showCheckBox) {
			holder.cb.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				@Override
				public void onCheckedChanged(CompoundButton buttonView,
						boolean isChecked) {
					isSelected.put(position, isChecked);
					if (isChecked) {
						list.put(position, (MyFriendBean) dataList
								.get(position).clone());
					} else {
						list.delete(position);
					}
				}
			});

			holder.cb.setChecked(isSelected.get(position));
		}

		return convertView;
	}

	public class FriendListViewHolder {
		private CircleImageView img;
		private TextView tv_name;
		private TextView tv_content;
		public CheckBox cb;
	}

	public SparseBooleanArray getIsSelected() {
		return isSelected;
	}

	public void setIsSelected(SparseBooleanArray isSelected) {
		this.isSelected = isSelected;
	}

	public ArrayList<MyFriendBean> getSelectedTextList() {
		ArrayList<MyFriendBean> arrayList = new ArrayList<MyFriendBean>();
		for (int i = 0; i < dataList.size(); i++) {
			if (isSelected.get(i)) {
				arrayList.add(list.get(i));
			}
		}
		MyUtils.showLog("", "适配器 处理完毕  " + arrayList.size());
		return arrayList;
	}

	public SparseArray<MyFriendBean> getSelectedTextSparseArray() {
		return list;
	}

	public int getPositionForSection(int section) {
		for (int i = 0; i < getCount(); i++) {
			String sortStr = dataList.get(i).getSortLetter();
			char firstChar = sortStr.charAt(0);
			if (firstChar == section) {
				return i;
			}
		}

		return -1;
	}

}
