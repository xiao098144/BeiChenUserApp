package com.ddoctor.user.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.SparseArray;
import android.util.SparseBooleanArray;
import android.view.View;
import android.view.ViewGroup;

import com.ddoctor.utils.MyUtils;

public class MultiChoiceAdapter<T> extends BaseAdapter<T>{

	public MultiChoiceAdapter(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	public SparseBooleanArray isSelected;
	public SparseArray<T> list = new SparseArray<T>();
	public List<T> chosed = new ArrayList<T>();
	
	public void setData(List<T> dataList, List<T> chosed) {
		this.setData(dataList);
		this.chosed = chosed;
		
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return super.getView(position, convertView, parent);
	}
	
	public SparseBooleanArray getIsSelected() {
		return isSelected;
	}

	public void setIsSelected(SparseBooleanArray isSelected) {
		this.isSelected = isSelected;
	}
	
	public ArrayList<T> getSelectedItemList() {
		ArrayList<T> arrayList = new ArrayList<T>();
		for (int i = 0; i < dataList.size(); i++) {
			if (isSelected.get(i)) {
				arrayList.add(list.get(i));
			}
		}
		MyUtils.showLog("", "适配器 处理完毕  " + arrayList.size());
		return arrayList;
	}
	
	public SparseArray<T> getSelectedItemSparseArray() {
		return list;
	}
	
}
