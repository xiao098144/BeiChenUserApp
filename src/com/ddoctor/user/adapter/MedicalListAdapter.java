package com.ddoctor.user.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.user.wapi.bean.MedicalBean;
import com.ddoctor.utils.MyUtils;

public class MedicalListAdapter extends BaseAdapter<MedicalBean>{

	public MedicalListAdapter(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if (null == convertView) {
			convertView = inflater.inflate(R.layout.layout_record_item_nodelete, parent , false);
			holder = new ViewHolder();
			holder.tv_name = (TextView) convertView.findViewById(R.id.record_item_nodelete_tv_left);
			holder.tv_value = (TextView) convertView.findViewById(R.id.record_item_nodelete_tv_right);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.tv_name.setText(dataList.get(position).getName());
		holder.tv_name.setVisibility(View.VISIBLE);
		holder.tv_name.setTag(R.id.medical_id , dataList.get(position).getId());
		String unit = dataList.get(position).getUnit();
		if (TextUtils.isEmpty(unit)) {
			unit = "";
		}
		if (unit.endsWith("\r")) {
//			MyUtils.showLog("unit endsWidth "+"\\r");
			unit = unit.replace("\r", "");
		}else if (unit.endsWith("\r\n")) {
//			MyUtils.showLog("unit endsWidth "+"\\r\\n");
			unit = unit.replace("\r\n", "");
		}else if(unit.endsWith("\n")){
//			MyUtils.showLog("unit endsWidth "+"\\n");
			unit = unit.replace("\n", "");
		}else if (unit.endsWith("\n\r")) {
//			MyUtils.showLog("unit endsWidth "+"\\n\\r");
			unit = unit.replace("\n\r", "");
		}
		MyUtils.showLog(" position "+position+" name == "+dataList.get(position).getName()+" name.length == "+(dataList.get(position).getName().length())+" value == "+dataList.get(position).getUnit()+" value.length == "+(dataList.get(position).getUnit()==null?"unit is null ":dataList.get(position).getUnit().length()));
		holder.tv_value.setText(unit);
		holder.tv_value.setVisibility(View.VISIBLE);
		return convertView;
	}
	
	private class ViewHolder{
		private TextView tv_name;
		private TextView tv_value;
	}
	
}
