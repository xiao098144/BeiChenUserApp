package com.ddoctor.user.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.user.model.FMLibItemBean;

public class FMLibAdapter extends BaseAdapter<FMLibItemBean> {


	public FMLibAdapter(Context context) {
		super(context);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if (null == convertView) {
			convertView = inflater.inflate(R.layout.layout_fmlib_lv_item,
					parent, false);
			holder = new ViewHolder();
			holder.img = (ImageView) convertView
					.findViewById(R.id.fmlib_lv_item_img);
			holder.tv_hot = (TextView) convertView
					.findViewById(R.id.fmlib_lv_item__tv_value_hot);
			holder.ratingBar = (RatingBar) convertView
					.findViewById(R.id.fmlib_lv_item_rb);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		int picUrl = Integer.valueOf(dataList.get(position).getPicUrl());
		switch (picUrl) {
		case 0: {
			holder.img.setBackgroundResource(R.drawable.rice);
		}
			break;
		case 1: {
			holder.img.setBackgroundResource(R.drawable.food_green);
		}
			break;
		case 2: {
			holder.img.setBackgroundResource(R.drawable.meat);
		}
			break;
		case 3: {
			holder.img.setBackgroundResource(R.drawable.food_fruit);
		}
			break;
		case 4: {
			holder.img.setBackgroundResource(R.drawable.drink);
		}
			break;

		default:
			break;
		}

		holder.tv_hot.setText(String.valueOf(dataList.get(position)
				.getHotContent()));
		holder.ratingBar.setRating(dataList.get(position).getRating());

		return convertView;
	}

	private class ViewHolder {
		private ImageView img;
		private TextView tv_hot;
		private RatingBar ratingBar;
	}

}
