package com.ddoctor.user.adapter;

import java.util.List;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class TypeIndicatorPagerAdapter extends FragmentPagerAdapter {

	private String[] tabTitle;
	private List<Fragment>fragmentList;
	public TypeIndicatorPagerAdapter(String[] tabTitle, List<Fragment>fragmentList,FragmentManager fm) {
		this(fm);
		this.tabTitle = tabTitle;
		this.fragmentList = fragmentList;
	}

	public TypeIndicatorPagerAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int position) {
		return fragmentList.get(position);
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return tabTitle[position%tabTitle.length];
	}
	
	@Override
	public int getCount() {
		return tabTitle.length;
	}

}
