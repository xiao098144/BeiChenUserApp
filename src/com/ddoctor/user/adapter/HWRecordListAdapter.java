package com.ddoctor.user.adapter;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.enums.RecordLayoutType;
import com.ddoctor.user.wapi.bean.HeightBean;

public class HWRecordListAdapter extends BaseAdapter<HeightBean>{

	private Context context;
	
	public HWRecordListAdapter(Context context) {
		super(context);
		this.context =context;
	}

	@Override
	public int getItemViewType(int position) {
		if (dataList != null
				&& dataList.size() > 0
				&& RecordLayoutType.TYPE_CATEGORY == dataList.get(position)
						.getLayoutType()) {
			return RecordLayoutType.TYPE_CATEGORY.ordinal();
		} else {
			return RecordLayoutType.TYPE_VALUE.ordinal();
		}
	}

	@Override
	public int getViewTypeCount() {
		return 2;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		RecordLayoutType layoutType = RecordLayoutType.values()[getItemViewType(position)];
		switch (layoutType) {
		case TYPE_CATEGORY: {
			CategoryHolder category = null;
			if (null == convertView) {
				convertView = inflater.inflate(
						R.layout.layout_recordlist_date_item, parent, false);
				category = new CategoryHolder();
				category.tv_date = (TextView) convertView
						.findViewById(R.id.recordlist_date_item_tv_date);
				convertView.setTag(category);
			} else {
				category = (CategoryHolder) convertView.getTag();
			}
			category.tv_date.setText(dataList.get(position).getDate());
		}
			break;
		case TYPE_VALUE: {
			ValueHolder valueHolder = null;
			if (null == convertView) {
				convertView = inflater.inflate(
						R.layout.layout_record_item_nodelete, parent, false);
				valueHolder = new ValueHolder();
				valueHolder.tv_left = (TextView) convertView
						.findViewById(R.id.record_item_nodelete_tv_left);
				valueHolder.tv_right = (TextView) convertView
						.findViewById(R.id.record_item_nodelete_tv_right);
				valueHolder.tv_middle = (TextView) convertView
						.findViewById(R.id.record_item_nodelete_tv_middle);
				convertView.setTag(valueHolder);
			} else {
				valueHolder = (ValueHolder) convertView.getTag();
			}
			valueHolder.tv_left.setVisibility(View.VISIBLE);
			valueHolder.tv_right.setVisibility(View.VISIBLE);
			valueHolder.tv_middle.setVisibility(View.VISIBLE);
			
			valueHolder.tv_left.setText(String.format(context.getString(R.string.hw_height_format), dataList.get(position).getHeight()));
			valueHolder.tv_middle.setText(String.format(
					context.getString(R.string.hw_weight_format),
					dataList.get(position).getWeight()));
			valueHolder.tv_right.setText(String.format(
					context.getString(R.string.hw_bmi_format),dataList.get(position).getBmi()));
			valueHolder.tv_right.setGravity(Gravity.CENTER);
		}
			break;
		default:
			break;
		}

		return convertView;
	}

	@Override
	public boolean areAllItemsEnabled() {
		// TODO Auto-generated method stub
		return super.areAllItemsEnabled();
	}

	@Override
	public boolean isEnabled(int position) {

		if (dataList != null
				&& dataList.size() > 0
				&& RecordLayoutType.TYPE_CATEGORY == dataList.get(position)
						.getLayoutType()) {
			return false;
		}
		return super.isEnabled(position);
	}
	
}
