package com.ddoctor.user.data;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.text.TextUtils;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.ddoctor.application.MyApplication;
import com.ddoctor.user.wapi.bean.BloodBean;
import com.ddoctor.user.wapi.bean.DietBean;
import com.ddoctor.user.wapi.bean.EvaluationBean;
import com.ddoctor.user.wapi.bean.PatientBean;
import com.ddoctor.user.wapi.bean.TslPatientBean;
import com.ddoctor.user.wapi.constant.TslStatusType;
import com.ddoctor.utils.FilePathUtil;
import com.ddoctor.utils.MD5Util;
import com.ddoctor.utils.MyUtils;
import com.ddoctor.utils.SharedUtils;
import com.ddoctor.utils.TimeUtil;
import com.google.gson.Gson;
import com.zhuge.analysis.stat.ZhugeSDK;

public class DataModule {
	private static DataModule _instance = null;

	public HashMap<String, Object> registerInfoMap = new HashMap<String, Object>();

	public HashMap<String, Object> activityMap = new HashMap<String, Object>();

	public static DataModule getInstance() {
		if (_instance == null) {
			_instance = new DataModule();
		}
		return _instance;
	}

	// public IWXAPI mwxAPI;

	// TODO: 这个数据应该保存到文件中，DataModule对象可能会被回收
	private PatientBean _patientBean = null;

	public PatientBean getLoginedUserInfo() {
		if (_patientBean == null) {
			MyUtils.showLog("_patientBean == null");
			PatientBean userInfo = MyProfile.getLoginedUserInfo();
			if (userInfo != null) {
				MyUtils.showLog("userInfo != null");
				_patientBean = new PatientBean();
				_patientBean.setData(userInfo);
			}
		} else
			MyUtils.showLog("_patientBean != null");

		return _patientBean;
	}
	
	private static String userAget = null;
	
	public static void setUserAgent(Context context)
	{
		try {
			WebView webview;
			webview = new WebView(context);
			webview.layout(0, 0, 0, 0);
			WebSettings settings = webview.getSettings();
			userAget = settings.getUserAgentString();

		} catch (Exception e) {

		}
	}
	
	public static String getUserAgent()
	{
		return userAget;
	}

	public void saveLoginedUserInfo(PatientBean patientBean) {
		if (patientBean == null) {
			_patientBean = null;
			MyProfile.setLoginedUserId(0);
		} else {
			
			try {
				JSONObject personObject = new JSONObject();
				personObject.put("avatar", patientBean.getImage());
				personObject.put("name", patientBean.getName());
				personObject.put("gender", "0".equals(patientBean.getSex())?"男":"女");
				personObject.put("birthday",patientBean.getBirthday());
				personObject.put("身高", patientBean.getHeight());
				personObject.put("体重", patientBean.getWeight());
				ZhugeSDK.getInstance().identify(MyApplication.getInstance().getApplicationContext(), String.valueOf(patientBean.getId()), personObject);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			// 保存内存变量
			if (_patientBean == null)
				_patientBean = new PatientBean();
			_patientBean.copyFrom(patientBean);
			// 保存到配置文件 下次进入不需要再登录，除非过期(暂时不处理过期)
			MyProfile.setLoginedUserId(patientBean.getId());
			MyProfile.setLoginedUserInfo(patientBean);
			
			//设置手机号码
			if(patientBean!=null && patientBean.getMobile()!=null)
			{
				SharedUtils.setMobile(patientBean.getMobile());				
			}
		}
	}

	public void logout() {
		removeloginedUserInfo();
		removeLogineduserId();
		removeTencentQuickLoginInfo();
	}

	public void removeloginedUserInfo() {
		_patientBean = null;
		MyProfile.removeLoginedUserInfo();
	}

	public void removeLogineduserId() {
		MyProfile.removeLoginedUserId();
	}

	public int getLoginedUserId() {
		return MyProfile.getLoginedUserId();
	}

	public boolean isLogined() {
		int userId = getLoginedUserId();
		if (userId > 0)
			return true;

		return false;
	}

	public static float getBMI(int height, float weight) {

		if (height == 0 || weight == 0)
			return 0;

		float bmi = weight / (((float) height / 100) * ((float) height / 100));
		return bmi;
	}

	public static String getUUID() {
		String uuid = SharedUtils.getUUID();
		if (TextUtils.isEmpty(uuid)) {
			String imei = MyUtils.getIMEI(MyApplication.getInstance()
					.getApplicationContext());
			String imsi = MyUtils.getIMSI(MyApplication.getInstance()
					.getApplicationContext());
			uuid = MD5Util.getMD5Encoding(imei + imsi
					+ TimeUtil.getInstance().getStandardDate("yyyyMMddHHmmss"));
			SharedUtils.setUUID(uuid);
		}
		return uuid;

	}

	public void setSugarUpBound(float value) {
		MyProfile.setSugarUpBound(value);
	}

	public void setSugarDownBound(float value) {
		MyProfile.setSugarDownBound(value);
	}

	public float getSugarUpBound() {
		return MyProfile.getSugarUpBound();
	}

	public float getSugarDownBound() {
		return MyProfile.getSugarDownBound();
	}

	// 获取字典数据
	/*
	 * public static List<GlucometerBean> loadGlucometerList(){ String
	 * json_string = MyUtils.readPrivateFile(MyApplication.getInstance(),
	 * MyProfile.DICT_GLUCOMETERS); if( json_string == null ) return null;
	 * 
	 * 
	 * try{ Gson gson = new Gson(); List<GlucometerBean> dataList = new
	 * ArrayList<GlucometerBean>(); JSONArray ja = new JSONArray(json_string);
	 * for(int i = 0; i < ja.length(); i ++ ) { JSONObject jo =
	 * ja.getJSONObject(i);
	 * 
	 * GlucometerBean gb = gson.fromJson(jo.toString(), GlucometerBean.class);
	 * if( gb != null ) dataList.add(gb); }
	 * 
	 * return dataList;
	 * 
	 * }catch(Exception e){}
	 * 
	 * return null; }
	 */

	/*
	 * 字典： 调用方法：List<GlucometerBean> a =
	 * DataModule.loadDict(MyProfile.DICT_GLUCOMETERS, GlucometerBean.class);
	 */
	public static <T> List<T> loadDict(String dictName, Class<T> classOfT) {

		String filename = String.format(Locale.CHINESE, "%s/%s.%s",
				MyProfile.getDictFilePath(), dictName, MyProfile.DICT_FILE_EXT);
		String json_string = MyUtils.readFile(filename);
//		MyUtils.showLog("读取  "+dictName+" 结果  "+json_string);
		if (json_string == null)
			return null;
		

		try {
			Gson gson = new Gson();
			List<T> dataList = new ArrayList<T>();
			JSONArray ja = new JSONArray(json_string);
			for (int i = 0; i < ja.length(); i++) {
				JSONObject jo = ja.getJSONObject(i);

				T gb = gson.fromJson(jo.toString(), classOfT);
				if (gb != null)
					dataList.add(gb);
			}

			return dataList;

		} catch (Exception e) {
		}

		return null;
	}

	// 检查字典文件是否存在
	public static boolean checkDictFileExists() {
		if (!checkDictFileExists(MyProfile.DICT_GLUCOMETERS))
			return false;
		if (!checkDictFileExists(MyProfile.DICT_DISTRICTS))
			return false;
		if (!checkDictFileExists(MyProfile.DICT_ILLNESSS))
			return false;
		if (!checkDictFileExists(MyProfile.DICT_DISTRICTS))
			return false;
		if (!checkDictFileExists(MyProfile.DICT_MEDICALS))
			return false;
		if (!checkDictFileExists(MyProfile.DICT_HOSPITAL))
			return false;
		if (!checkDictFileExists(MyProfile.DICT_LEVELS))
			return false;
		if (!checkDictFileExists(MyProfile.DICT_FOOD))
			return false;

		
		return true;
	}

	public static boolean checkDictFileExists(String dictName) {
		try {
			String filename = String.format(Locale.CHINESE, "%s/%s.%s",
					MyProfile.getDictFilePath(), dictName,
					MyProfile.DICT_FILE_EXT);
			File file = new File(filename);
			if (file.exists())
				return true;
		} catch (Exception e) {
		}

		return false;
	}

	private boolean isUpdateSugar = false;

	public boolean isUpdateSugar() {
		return isUpdateSugar;
	}

	public void setUpdateSugar(boolean isUpdateSugar) {
		this.isUpdateSugar = isUpdateSugar;
	}

	private List<BloodBean> sugarList;

	public void setSugarList(List<BloodBean> sugarList) {
		this.sugarList = sugarList;
	}

	public List<BloodBean> getSugarList() {
		return sugarList;
	}

	private boolean isUpdateDiet = false;

	public boolean isUpdateDiet() {
		return isUpdateDiet;
	}

	public void setUpdateDiet(boolean isUpdateDiet) {
		this.isUpdateDiet = isUpdateDiet;
	}

	private List<DietBean> dietList;

	public void setDietList(List<DietBean> dietList) {
		this.dietList = dietList;
	}

	public List<DietBean> getDietList() {
		return dietList;
	}

	private boolean isUpdateMedical = false;

	public boolean isUpdateMedical() {
		return isUpdateMedical;
	}

	public void setUpdateMedical(boolean isUpdateMedical) {
		this.isUpdateMedical = isUpdateMedical;
	}

	// 根据订单状态的中文名
	public static String getOrderStatusName(int status) {
		String[] nameList = new String[] { "未知", "未支付", "已支付", "已发货", "未发货",
				"已发货", "已完结", "已超时" };

		if (status < 0 || status >= nameList.length)
			status = 0;

		return nameList[status];
	}

	public static String getPayStatusName(int payStatus) {
		String[] nameList = new String[] { "未知", "未支付", "待确认", "已支付" };
		payStatus += 1;
		if (payStatus < 0 || payStatus >= nameList.length)
			payStatus = 0;

		return nameList[payStatus];
	}

	public void setGPSState(boolean state) {
		MyProfile.setGPSState(state);
	}

	public boolean isGPSEnable() {
		return MyProfile.isGPSEnable();
	}

	/**
	 * 设置调查问卷1状态 true 已完成
	 * 
	 * @param b
	 */
	public void setSurvey1State(boolean state) {
		MyProfile.setSurvey1State(state);
	}

	public boolean getSurvey1State() {
		return MyProfile.getSurvey1State();
	}

	// private boolean isUserInfoUpdated = false;
	//
	// public boolean isUserInfoUpdated() {
	// return isUserInfoUpdated;
	// }
	//
	// public void setUserInfoUpdated(boolean isUserInfoUpdated) {
	// MyUtils.showLog("DataModule instance == "+_instance.toString()+" 设置个人信息更新状态  "+isUserInfoUpdated);
	// this.isUserInfoUpdated = isUserInfoUpdated;
	// }

	/**
	 * 保存地理位置信息 地址、经纬度
	 * 
	 * @param addrStr
	 * @param latitude
	 * @param longitude
	 */
	public void saveLocation(String addrStr, double latitude, double longitude) {
		MyProfile.saveLocation(addrStr, latitude, longitude);
	}

	public String getAddress() {
		return MyProfile.getAddress();
	}

	public double getLatitude() {
		return MyProfile.getLatitude();
	}

	public double getLongtitude() {
		return MyProfile.getLongtitude();
	}

	/**
	 * 保存支付宝回调地址
	 * 
	 * @param optString
	 */
	public void saveZfbURl(String zfbUrl) {
		MyProfile.saveZfbURl(zfbUrl);
	}

	public String getZfbUrl() {
		return MyProfile.getZfbUrl();
	}

	public void saveRYToken(String ryToken) {
		MyProfile.saveRYToken(ryToken);
	}

	public String getRYToken() {
		return MyProfile.getRYToken();
	}

	public void saveRYUserId(String ryUserId) {
		MyProfile.saveRYUserId(ryUserId);
	}

	public String getRYUserId() {
		return MyProfile.getRYUserId();
	}

	private boolean _stepGoalUpdated = false;

	public void setStepGoalUpdated(boolean b) {
		_stepGoalUpdated = b;
	}

	public boolean isStepGoalUpdated() {
		return _stepGoalUpdated;
	}

	public String getOpenId() {
		return MyProfile.getOpenId();
	}

	public long getTencentExpires() {
		return MyProfile.getTencentExpires();
	}

	public String getTencentAccessToken() {
		return MyProfile.getTencentAccessToken();
	}

	public void saveTencentQuickLoginInfo(String openid, long expires,
			String access_token) {
		MyProfile.saveTentcentQuickLoginInfo(openid, expires, access_token);
	}

	public void removeTencentQuickLoginInfo() {
		MyProfile.removeTencentQuickLoginInfo();
	}

	public static String getTakePhotoTempFilename(String tag) {
		return String.format(Locale.CHINESE, "%s/tmp-%s.jpg", FilePathUtil
				.getImageCachePath().getAbsolutePath(), tag);
	}

	/**
	 * 保存健康报告评估结果
	 * 
	 * @param evaluationBean
	 */
	public void saveHealthReportResult(EvaluationBean evaluationBean) {
		MyProfile.saveHealthReportResult(evaluationBean);
	}

	/**
	 * 获取上次评估结果 没有为null
	 * 
	 * @return
	 */
	public EvaluationBean getHealthReportResult() {
		return MyProfile.getHealthReport();
	}
	
	
	public void updateTSLUserInfo(JSONObject respObj){
		
		try{
			// 先预置一个默认的值:没填写过
			if( respObj.has("mobileArea") && respObj.optInt("mobileArea") == 22 )
			{
				MyProfile.tsl_setAuthStatus(MyProfile.TSL_AUTH_STATUS_NONE);
				MyProfile.tsl_setIsAllowUser(1);
				// 22是天津号段，暂时先写死
				if( respObj.has("tslPatient") )
				{
					JSONObject patientObj = respObj.getJSONObject("tslPatient");
					if( patientObj != null )
					{
						Gson gson = new Gson();
						TslPatientBean patient = gson.fromJson(patientObj.toString(), TslPatientBean.class);
						if( patient != null )
						{
							// 认证信息填写过
							int state = patient.getFlagstate();
							if( state == TslStatusType.COMPLETEINFO_AUDIT_SUCCESS )
							{
								MyProfile.tsl_setAuthStatus(MyProfile.TSL_AUTH_STATUS_AUTH_SUCCESS);
							}
							else if( state == TslStatusType.COMPLETEINFO_AUDIT_FAIL )
							{
								MyProfile.tsl_setAuthStatus(MyProfile.TSL_AUTH_STATUS_AUTH_FAILED);
								if( patient.getFlagmsg() == null )
									MyProfile.tsl_setAuthMessage("未知错误");
								else
									MyProfile.tsl_setAuthMessage(patient.getFlagmsg());
							}
							else// if( state == TslStatusType.COMPLETEINFO_AUDIT_ING )
							{
								MyProfile.tsl_setAuthStatus(MyProfile.TSL_AUTH_STATUS_WAIT_AUTH);
								
//								// for test
//								MyProfile.tsl_setAuthStatus(MyProfile.TSL_AUTH_STATUS_AUTH_SUCCESS);
								
	
							}
	
							// 写用户信息到配置文件，后面需要用到
							MyProfile.setTslUserInfo(patient);
						}
					}
				}
				
				
			}
			else
				MyProfile.tsl_setIsAllowUser(0);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
	}

}
