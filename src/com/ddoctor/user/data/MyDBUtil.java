package com.ddoctor.user.data;

import java.util.Locale;

import android.database.Cursor;

import com.ddoctor.application.MyApplication;
import com.ddoctor.user.wapi.bean.ProductBean;
import com.ddoctor.utils.DBUtil;

public class MyDBUtil extends DBUtil {
	private static MyDBUtil _dbInstance = null;

	public static MyDBUtil getInstance() {
		if (_dbInstance == null) {
			_dbInstance = new MyDBUtil();
			_dbInstance.setDBName("ddoctor.db");
		}

		if (!_dbInstance.isOpen()) {
			_dbInstance.db_open(MyApplication.getInstance()
					.getApplicationContext());
		}

		return _dbInstance;
	}

	@Override
	public void onDatabaseOpend() {

		String sql;
		
		// 购物车
		sql = "create table if not exists shopcart("
				+"sid integer primary key autoincrement,"
				+"uid integer not null,"
				+ "id integer not null,"
				+"name varchar(255) not null,"
				+"discount decimal(10,2) not null default 0,"
				+"image varchar(255) null,"
				+"num integer not null default 0,"
				+"createtime timestamp not null default (datetime('now','localtime'))"
				+");";
		execSQL(sql);
		
		
		
		/*
		String sql = "create table if not exists downloadtask("
				+ "sid integer primary key,"
				+ "taskid varchar(40) not null,"
				+ "videoid integer not null,"
				+ "title varchar(256) not null,"
				+ "source varchar(256) null,"
				+ "duration integer,"
				+ "thumb varchar(255),"
				+ "playurl varchar(256),"
				+ "downloadurl varchar(256),"
				+ "videofile varchar(256),"
				+ "totalsize integer,"
				+ "read integer,"
				+ "state integer," // 0_download 1_download ok
				+ "desc varchar(512) null,"
				+ "publishtime varchar(50) null,"
				+ "viewtime TimeStamp NOT NULL DEFAULT (datetime('now','localtime'))"
				+ ");";

		/*
		 * String sql = "create table if not exists downloadtask(" +
		 * "sid integer primary key," + "taskid varchar(40) not null," +
		 * "videoid integer not null," + "title varchar(256) not null," +
		 * "source varchar(256) null," + "duration integer," +
		 * "thumb varchar(255)," + "playurl varchar(256)," +
		 * "downloadurl varchar(256)," + "videofile varchar(256)," +
		 * "totalsize integer," + "read integer," + "state integer," //
		 * 0_download 1_download ok + "desc varchar(512) null," +
		 * "publishtime varchar(50) null," +
		 * "viewtime TimeStamp NOT NULL DEFAULT (datetime('now','localtime'))" +
		 * ");";
		 * 
		 * execSQL(sql);
		 */

		/**
		 * 常用药记录 字段描述：id 主键 自增 userid 用户id medicalid 药品id name 药品名称 unit 药品单位 如
		 * 5毫克/片 count 服用剂量 如 4片 date 服药时间 默认当前日期 yyyy-MM-dd date距今30天 则记录失效
		 * 删除该记录
		 */
		String createMedicalHistory = "CREATE TABLE if not exists medicalhistory(id integer primary key autoincrement,"
				+ "userid integer not null,medicalid integer not null,"
				+ "name varchar(50) not null,unit varchar(10) not null,"
				+ "count varchar(10) not null,"
				+ "date char(10) NOT NULL DEFAULT (date('now','localtime')))";
		execSQL(createMedicalHistory);
		// 药品名称、药品单位、服用剂量拼接而成 如 单个药品数据之间以空格分隔 多条记录则以中文顿号分隔 格列苯脲 2.5毫克/片 4片
		/**
		 * 用药提醒 字段描述： id 主键 自增 ， userid 用户id ， content 提醒内容（直接将list集合转 json
		 * 存储相应json串 需要时再进行解析显示） time 提醒时间 ， routing 提醒周期 7位 01字符串 1代表有 默认全1 ，
		 * parentid 父记录id 鉴别该条提醒是用户手动还是代码自动添加，用于延缓提醒 state 提醒状态 鉴别提醒时间是否已过去
		 * 是否正在进行中 理论每天0点以后 所有记录状态值回复默认值 默认1 提醒时间尚未到达 openremind 定时提醒开关
		 * 添加提醒时由用户选择 默认开启 remark 备注 最多200字符
		 */
		String createMedicalRemind = "CREATE TABLE if not exists medicalremind ( id integer primary key autoincrement , "
				+ "userid integer not null , "
				+ "content text not null , "
				+ "medicalsize integer default 0 , "
				+ "time char(4) not null, routing char(7) not null default('1111111') ,  "
				+ "parentid integer default 0 , state integer default  1, "
				+ "remark varchar(200) , " 
				+ "createtime timestamp not null default (datetime('now','localtime')) " 
				+ ")";

		execSQL(createMedicalRemind);

		String createSportRemind = "CREATE TABLE if not exists sportremind ( " +
				" id integer primary key autoincrement , " +
				" userid integer not null ," +
				" type integer not null default 1, " +
				" content varchar(20) not null , " +
				" time char(4) not null ," +
				" routing char(7) not null default('1111111') ," +
				" parentid integer default 0 , " +
				" state integer default  1,remark varchar(200) ," 
				+ " createtime timestamp not null default (datetime('now','localtime')) " 
				+ " );";
		
		execSQL(createSportRemind);

	}
	
	
	
	// 添加到购物车
	public boolean addToCart(ProductBean productBean, int num)
	{
		int uid = DataModule.getInstance().getLoginedUserId();
		int pid = productBean.getId();
		
		String sql = String.format(Locale.CHINESE,"select * from shopcart where uid=%d and id=%d", uid, pid);
		Cursor cursor = querySQL(sql);
		if( cursor == null )
			return false;
		
		if( cursor.getCount() > 0 )
		{// 已经有了, 更新数量
			sql = String.format(Locale.CHINESE,"update shopcart set num = num + %d,createtime= datetime('now','localtime') "
					+" where uid = %d and id = %d", 
					num, uid, pid);
		}
		else
		{// 新加的
			sql = String.format(Locale.CHINESE, "insert into shopcart(uid, id, name, discount, image, num) values("
					+" %d, %d, '%s', %f, '%s', %d)",
					uid, pid, productBean.getName(), productBean.getDiscount(), 
					productBean.getImage(), num);
			
		}
		cursor.close();
		
		if( !execSQL(sql) )
			return false;
		
		return true;
	}
	
	public boolean removeFromCart(int productId)
	{
		int uid = DataModule.getInstance().getLoginedUserId();
		String sql = String.format(Locale.CHINESE, "delete from shopcart where uid=%d and id = %d", uid, productId);
		return execSQL(sql);
	}
	
	public boolean emptyCart()
	{
		int uid = DataModule.getInstance().getLoginedUserId();
		String sql = String.format(Locale.CHINESE, "delete from shopcart where uid=%d", uid);
		return execSQL(sql);
	}
	
	public boolean addCartProductNum(int productId, int add_num)
	{
		int uid = DataModule.getInstance().getLoginedUserId();
		String sql = String.format(Locale.CHINESE, "update shopcart set num = num + %d where uid=%d and id = %d", 
				add_num, uid, productId);
		return execSQL(sql);
		
	}

}
