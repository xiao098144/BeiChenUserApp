package com.ddoctor.user.data;

import java.util.List;

import android.text.TextUtils;

import com.beichen.user.R;
import com.ddoctor.application.MyApplication;
import com.ddoctor.user.wapi.bean.EvaluationBean;
import com.ddoctor.user.wapi.bean.PatientBean;
import com.ddoctor.user.wapi.bean.TslPatientBean;
import com.ddoctor.user.wapi.bean.UplowValueBean;
import com.ddoctor.utils.SharedUtils;
import com.ddoctor.utils.TimeUtil;

public class MyProfile {
	private static final String KEY_LOGINED_USERID = "session_userid";
	public static final String KEY_ISFIRST = "isfirst";
	public static final String KEY_DICSN = "dicsn";

	// =========================================================================
	// 天士力相关
	public static final String KEY_TSL_AUTH_STATUS = "tsl_auth_status_userid_";
	public static final String KEY_TSL_AUTH_MESSAGE = "tsl_auth_message_userid_";
	public static final String KEY_TSL_IS_ALLOW_USER = "tsl_is_allow_userid_";
	public static final String KEY_TSL_USERINFO = "tsl_userinfo_userid_";

	public static final int TSL_AUTH_STATUS_NONE = 0; // 未完善资料
	public static final int TSL_AUTH_STATUS_WAIT_AUTH = 1; // 等待审核
	public static final int TSL_AUTH_STATUS_AUTH_SUCCESS = 10; // 已经审核通过
	public static final int TSL_AUTH_STATUS_AUTH_FAILED = 20; // 审核失败

	// 血糖仪
	public static final String DICT_GLUCOMETERS = "glucometers";
	// 病症类型
	public static final String DICT_ILLNESSS = "illnesss";
	// 不适记录类型
	public static final String DICT_TROUBLEITEMS = "troubleitems";
	// 地区列表
	public static final String DICT_DISTRICTS = "districts";
	// 药品列表
	public static final String DICT_MEDICALS = "medicals";
	// 医院列表
	public static final String DICT_HOSPITAL = "hospitals";
	// 部门列表
	public static final String DICT_LEVELS = "levels";
	// 饮食记录
	public static final String DICT_FOOD = "foods";

	public static final String DICT_FILE_EXT = "dic";

	private static MyProfile _instance = null;

	public static MyProfile getInstance() {
		if (_instance == null) {
			_instance = new MyProfile();
		}

		return _instance;
	}

	private final static String UPBOUND = "upbound";
	private final static String DOWNBOUND = "downbound";

	public static void setSugarUpBound(float upBound) {
		SharedUtils.setFloat(UPBOUND, upBound);
	}

	public static void setSugarDownBound(float downBound) {
		SharedUtils.setFloat(DOWNBOUND, downBound);
	}

	public static float getSugarUpBound() {
		return SharedUtils.getFloat(UPBOUND, 6.1f);
	}

	public static float getSugarDownBound() {
		return SharedUtils.getFloat(DOWNBOUND, 3.9f);
	}

	public static int getIntValue(String key, int defaultValue) {
		return SharedUtils.getInt(key, defaultValue);
	}

	public static void setIntValue(String key, int value) {
		SharedUtils.setInt(key, value);
	}

	public static void removeLoginedUserId() {
		SharedUtils.removeData(KEY_LOGINED_USERID);
	}

	public static int getLoginedUserId() {
		return SharedUtils.getInt(KEY_LOGINED_USERID, 0);
	}

	public static void setLoginedUserId(int userId) {
		SharedUtils.setInt(KEY_LOGINED_USERID, userId);
	}

	public static int getChannelId() {
		String s = MyApplication.getInstance().getResources()
				.getString(R.string.channelID);
		if (TextUtils.isEmpty(s)) {
			return 0;
		}
		if (s.startsWith("100")) {
			if (s.length() > 7) {
				StringBuffer sb = new StringBuffer();
				sb.append(s.substring(0, s.indexOf("0")));
				String last = s.substring(s.lastIndexOf("0") + 1);
				int ss = 7 - last.length() - sb.length();
				for (int i = 0; i < ss; i++) {
					sb.append("0");
				}
				sb.append(last);
				s = sb.toString();
			}
		}
		try {
			return Integer.parseInt(s);
		} catch (Exception e) {
		}

		return 0;
	}

	// 字典文件存储路径
	public static String getDictFilePath() {
		return MyApplication.getInstance().getFilesDir().toString();
	}

	List<UplowValueBean> list;

	public void setUpLowValue(List<UplowValueBean> list) {
		this.list = list;
	}

	public List<UplowValueBean> getupLowValue() {
		return list;
	}

	public static void setGPSState(boolean state) {
		SharedUtils.setBoolean(GPS, state);
	}

	private final static String GPS = "GPS";

	public static boolean isGPSEnable() {
		return SharedUtils.getBoolean("GPS", true);
	}

	private final static String USER = "user";

	public static void setLoginedUserInfo(PatientBean patientBean) {
		SharedUtils.setLoginUserInfo(USER + getLoginedUserId(), patientBean);
	}

	public static PatientBean getLoginedUserInfo() {
		return SharedUtils.getLoginUserInfo(USER + getLoginedUserId());
	}

	public static void removeLoginedUserInfo() {
		SharedUtils.removeData(USER + getLoginedUserId());
	}

	private final static String SURVEY1 = "survey1";

	public static void setSurvey1State(boolean state) {
		SharedUtils.setBoolean(SURVEY1, state);
	}

	public static boolean getSurvey1State() {
		return SharedUtils.getBoolean(SURVEY1, false);
	}

	private final static String ADDRESS = "address";
	private final static String LATITUDE = "latitude";
	private final static String LONGTITUDE = "longtitude";

	public static void saveLocation(String addrStr, double latitude,
			double longtitude) {
		SharedUtils.setString(ADDRESS, addrStr);
		SharedUtils.setString(LATITUDE, String.valueOf(latitude));
		SharedUtils.setString(LONGTITUDE, String.valueOf(longtitude));
	}

	public static String getAddress() {
		return SharedUtils.getString(ADDRESS, "");
	}

	public static double getLatitude() {
		return Double.valueOf(SharedUtils.getString(LATITUDE, "0"));
	}

	public static double getLongtitude() {
		return Double.valueOf(SharedUtils.getString(LONGTITUDE, "0"));
	}

	protected static void saveZfbURl(String zfbUrl) {
		SharedUtils.setString("zfbUrl", zfbUrl);
	}

	protected static String getZfbUrl() {
		return SharedUtils.getString("zfbUrl", "");
	}

	public static void saveRYToken(String ryToken) {
		SharedUtils.setString("token" + getLoginedUserId(), ryToken);
	}

	public static String getRYToken() {
		return SharedUtils.getString("token" + getLoginedUserId(), "");
	}

	public static void saveRYUserId(String ryUserId) {
		SharedUtils.setString("ryUserId" + getLoginedUserId(), ryUserId);
	}

	public static String getRYUserId() {
		return SharedUtils.getString("ryUserId" + getLoginedUserId(), "");
	}

	public static String getOpenId() {
		return SharedUtils.getString("openId" + getLoginedUserId(), "");
	}

	public static void saveTentcentQuickLoginInfo(String openid, long expires,
			String access_token) {
		SharedUtils.setString("tencent_openId" + getLoginedUserId(), openid);
		SharedUtils.setLong("tencent_expires" + getLoginedUserId(), expires);
		SharedUtils.setString("tencent_access_token" + getLoginedUserId(),
				access_token);
	}

	public static long getTencentExpires() {
		return SharedUtils.getLong("tencent_expires" + getLoginedUserId(), 0);
	}

	public static String getTencentAccessToken() {
		return SharedUtils.getString("tencent_access_token"
				+ getLoginedUserId(), "");
	}

	public static void removeTencentQuickLoginInfo() {
		SharedUtils.removeData("tencent_openId" + getLoginedUserId());
		SharedUtils.removeData("tencent_expires" + getLoginedUserId());
		SharedUtils.removeData("tencent_access_token" + getLoginedUserId());
	}

	// 天士力审核状态设置及获取
	public static int tsl_getAuthStatus() {
		return SharedUtils.getInt(KEY_TSL_AUTH_STATUS + getLoginedUserId(),
				TSL_AUTH_STATUS_NONE);
	}

	public static void tsl_setAuthStatus(int status) {
		SharedUtils.setInt(KEY_TSL_AUTH_STATUS + getLoginedUserId(), status);
	}

	public static boolean tsl_isAuthed() {
		int status = tsl_getAuthStatus();
		if (status == TSL_AUTH_STATUS_AUTH_SUCCESS)
			return true;

		return false;
	}

	public static String tsl_getAuthMessage() {
		return SharedUtils.getString(KEY_TSL_AUTH_MESSAGE + getLoginedUserId(),
				"");
	}

	public static void tsl_setAuthMessage(String msg) {
		SharedUtils.setString(KEY_TSL_AUTH_MESSAGE + getLoginedUserId(), msg);
	}

	public static int tsl_getIsAllowUser() {
		return SharedUtils
				.getInt(KEY_TSL_IS_ALLOW_USER + getLoginedUserId(), 0);
	}

	public static void tsl_setIsAllowUser(int allow) {
		SharedUtils.setInt(KEY_TSL_IS_ALLOW_USER + getLoginedUserId(), allow);
	}

	public static void saveHealthReportResult(EvaluationBean evaluationBean) {
		SharedUtils.setString("total"+getLoginedUserId(), evaluationBean.getTotal());
		SharedUtils.setString("diet"+getLoginedUserId(), evaluationBean.getDiet());
		SharedUtils.setString("sport"+getLoginedUserId(), evaluationBean.getSport());
		SharedUtils.setString("sugar"+getLoginedUserId(), evaluationBean.getSugar());
		SharedUtils.setString("medical"+getLoginedUserId(), evaluationBean.getMedical());
		SharedUtils.setString("time"+getLoginedUserId(), evaluationBean.getEvaluationTime());
	}

	public static EvaluationBean getHealthReport() {
		String total = SharedUtils.getString("total"+getLoginedUserId(), null);
		if (TextUtils.isEmpty(total)) {
			return null;
		}
		EvaluationBean evaluationBean = new EvaluationBean();
		evaluationBean.setTotal(total);
		evaluationBean.setDiet(SharedUtils.getString("diet"+getLoginedUserId(), "0%"));
		evaluationBean.setMedical(SharedUtils.getString("medical"+getLoginedUserId(), "0%"));
		evaluationBean.setSport(SharedUtils.getString("sport"+getLoginedUserId(), "0%"));
		evaluationBean.setSugar(SharedUtils.getString("sugar"+getLoginedUserId(), "0%"));
		evaluationBean.setEvaluationTime(SharedUtils.getString("time"+getLoginedUserId(), TimeUtil
				.getInstance().getStandardDate("yyyy-MM-dd HH:mm:ss")));
		return evaluationBean;
	}

	// ==================================================================
	// 天士力相关
	public static void setTslUserInfo(TslPatientBean patientBean) {
		SharedUtils.saveSerializeObject(KEY_TSL_USERINFO + getLoginedUserId(),
				patientBean);
	}

	public static TslPatientBean getTslUserInfo() {
		return (TslPatientBean) SharedUtils.getSerializeObject(KEY_TSL_USERINFO
				+ getLoginedUserId());
	}

}
