package com.ddoctor.user.component;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.utils.FilePathUtil;
import com.ddoctor.utils.MyUtils;

public class ClientUpgrade {

	private Context _context;
	private Thread _download_thread;

	private String _downloadApkUrl = null;
	private String _savePath = null;
	private String _saveFilename = null;
	private boolean _interceptFlag = false;
	private int _progress = 0;
	private ProgressBar _progressBar;

	private Dialog _downloadDialog;

	private final static int DOWN_UPDATE = 1;
	private final static int DOWN_OVER = 2;
	private final static int DOWN_ERROR = 10;

	private ClientUpgradeCallback _callback;

	public ClientUpgrade(Context context) {
		_context = context;
	}

	public void downloadApk(String downloadUrl, ClientUpgradeCallback callback) {
		_downloadApkUrl = downloadUrl;
		_callback = callback;

		// if (MyUtils.checkSdCardExists()) {
		// _savePath = String.format("%s%s",
		// Environment.getExternalStorageDirectory(),
		// FilePathUtil.getMainRootPath());
		// _saveFilename = String.format("%s/upgrade.apk", _savePath);
		// } else {
		// _savePath = "";
		// _saveFilename = String.format("%s/upgrade.apk",
		// _context.getFilesDir());
		// }

		_savePath = String.format("%s", FilePathUtil.getDownloadPath());
		_saveFilename = String.format("%s%s", _savePath, "user-upgrade.apk");

		MyUtils.showLog("path=" + _savePath);
		MyUtils.showLog("filename=" + _saveFilename);

		showDownloadDialog();

		_download_thread = new Thread(mDownloadRunnable);
		_download_thread.start();
	}

	private Runnable mDownloadRunnable = new Runnable() {

		@Override
		public void run() {
			try {
				URL url = new URL(_downloadApkUrl);

				HttpURLConnection conn = (HttpURLConnection) url
						.openConnection();
				conn.setReadTimeout(30 * 1000);
				conn.connect();

				int length = conn.getContentLength();
				InputStream is = conn.getInputStream();

				if (_savePath.length() > 0) {
					File file = new File(_savePath);
					if (!file.exists())
						file.mkdirs();
				}

				String apkFilename = _saveFilename;
				File apkFile = new File(apkFilename);
				FileOutputStream fos = new FileOutputStream(apkFile);

				// FileOutputStream fos = _context.openFileOutput(_saveFilename,
				// Context.MODE_PRIVATE);
				int count = 0;
				byte buf[] = new byte[1024];

				do {
					int numread = is.read(buf);
					count += numread;

					// Log.i("", "download: " + count + " total: " + length);
					_progress = (int) (((float) count / length) * 100);

					mHandler.sendEmptyMessage(DOWN_UPDATE);
					if (numread <= 0) {
						mHandler.sendEmptyMessage(DOWN_OVER);
						break;
					}

					fos.write(buf, 0, numread);
				} while (!_interceptFlag);

				fos.close();
				is.close();

			} catch (Exception e) {
				e.printStackTrace();
				mHandler.sendEmptyMessage(DOWN_ERROR);
			}
		}

	};

	private Handler mHandler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case DOWN_UPDATE:
				_progressBar.setProgress(_progress);
				tv_title.setText(String.format("正在下载：%d%%", _progress));
//				_downloadDialog.setTitle(String.format("正在下载：%d%%", _progress));
				break;
			case DOWN_OVER:
				_progress = 100;
				_progressBar.setProgress(_progress);
				tv_title.setText(String.format("正在下载：%d%%", _progress));
				_downloadDialog.dismiss();
				// MainActivity.getInstance().finish();
				installApk();
				break;
			case DOWN_ERROR:
				_downloadDialog.dismiss();
				if (_callback != null)
					_callback.onFailed();
				break;
			default:
				break;
			}
		}
	};
	private TextView tv_title;

	private int installApk() {
		File file = new File(_saveFilename);
		if (!file.exists())
			return 1;

		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setDataAndType(Uri.parse("file://" + file.toString()),
				"application/vnd.android.package-archive");
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

		// String s = String.format("%s/%s", _context.getFilesDir(),
		// _saveFilename);
		// Log.d("", s);
		// intent.setDataAndType(Uri.parse("file://" + s),
		// "application/vnd.android.package-archive");
		_context.startActivity(intent);

		if (_callback != null)
			_callback.onSuccess();

		return 0;
	}

	private void showDownloadDialog() {
		
		_downloadDialog = new Dialog(_context, R.style.NoTitleDialog);
		View ll = LayoutInflater.from(_context).inflate(
				R.layout.client_upgrade, null);
		tv_title = (TextView) ll.findViewById(R.id.update_tv_title);
		tv_title.setText("正在下载");
		_progressBar = (ProgressBar) ll.findViewById(R.id.progressBar1);
		Button btn_cancel = (Button) ll.findViewById(R.id.update_btn_cancel);
		
		btn_cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				_downloadDialog.dismiss();
				_interceptFlag = true;

				if (_callback != null)
					_callback.onCancel();

			}
		});

		_downloadDialog.setContentView(ll);
		_downloadDialog.setCanceledOnTouchOutside(false);
		_downloadDialog.setCancelable(false);
		_downloadDialog.show();

	}

	public interface ClientUpgradeCallback {
		public void onCancel();

		public void onFailed();

		public void onSuccess();
	}
}
