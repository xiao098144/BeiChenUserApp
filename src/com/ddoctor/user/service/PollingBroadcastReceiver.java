package com.ddoctor.user.service;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.ddoctor.user.data.DataModule;

public class PollingBroadcastReceiver extends BroadcastReceiver {

	private boolean isServiceRunning = false;

	@Override
	public void onReceive(Context context, Intent intent) {
		if (Intent.ACTION_TIME_TICK.equals(intent.getAction())) {
			if (DataModule.getInstance().getLoginedUserId() != 0) {
				ActivityManager manager = (ActivityManager) context
						.getSystemService(Context.ACTIVITY_SERVICE);
				for (RunningServiceInfo service : manager
						.getRunningServices(Integer.MAX_VALUE)) {
					if ("com.ddoctor.user.service.PollingService"
							.equals(service.service.getClassName())) {
						isServiceRunning = true;
						break;
					}
				}
				
				if (!isServiceRunning) {
					Intent in = new Intent();
					in.setClass(context, PollingService.class);
					context.startService(in);
				}
				
			}
		}
	}

}
