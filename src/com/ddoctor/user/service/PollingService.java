package com.ddoctor.user.service;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;

import com.beichen.user.R;
import com.ddoctor.user.activity.medicine.RemindDialogActivity;
import com.ddoctor.user.activity.sport.SportRemindDialogActivity;
import com.ddoctor.user.wapi.bean.MedicalRemindBean;
import com.ddoctor.user.wapi.bean.SportRemindBean;
import com.ddoctor.utils.MedicalDBUtil;
import com.ddoctor.utils.MyUtils;
import com.ddoctor.utils.SportRemindDBUtil;
import com.ddoctor.utils.TimeUtil;

/**
 * @filename PollingService.java
 * @TODO 定时服务 定期查询数据库闹钟记录
 * @date 2014-5-7上午9:26:19
 * @Administrator 萧
 * 
 */

@SuppressLint("InflateParams")
public class PollingService extends Service {

	private final static int SLEEPTIME = 60 * 1000;
//	private final static int REMINDLATER = 1; // 稍候提醒时间
	private Context context;
	private String[] selectlikes;
	
//	private AlertDialog _medicaldialog = null;
//	private AlertDialog _sportdialog;
	
	
	private final int NOTIFYID = 0;

	private NotificationManager nm;
	private Notification notification;
	
	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		MyUtils.showLog(
				"服务启动时间  "
						+ TimeUtil.getInstance().getStandardDate(
								"yyyy-MM-dd HH:mm:ss.S"));
		context = getApplicationContext();
		selectlikes = context.getResources()
				.getStringArray(R.array.day_in_week);
		handler = new Handler();
//		_medicaldialog = new AlertDialog.Builder(context).create();
//		_sportdialog = new AlertDialog.Builder(context).create();
	}

	public void initNotification() {
		nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		notification = new Notification();
		notification.icon = R.drawable.ic_launcher;
		notification.tickerText = "用药提醒";
		notification.defaults = Notification.DEFAULT_ALL;
		// notification.flags = Notification.FLAG_AUTO_CANCEL;
		notification.flags = Notification.FLAG_INSISTENT;

	}
	
	private Runnable pollingRunnable = new Runnable() {

		@Override
		public void run() {
			MyUtils.showLog("启动runnable 线程  ");
//			MyUtils.showLog("", "服务进行中准备查询  "
//					+ TimeUtil.getInstance()
//							.getStandardDate("MM-dd HH:mm:ss.S"));
			String hourMinute = TimeUtil.getInstance().getStandardDate("HH:mm");
			int dayOfWeek = TimeUtil.getInstance().getCurrentDayOfWeek();
			String selectString = selectlikes[dayOfWeek - 1];
			
			ArrayList<SportRemindBean> sportList = SportRemindDBUtil.getInstance().selectSportRemind(hourMinute, selectString);
//			MyUtils.showLog("查询运动提醒  "+sportList.size()+"   "+sportList.toString());
			if (null != sportList && sportList.size()>0) {
//				MyUtils.showLog("", "服务查询结果  " + sportList.toString());
				showSportAlarm(sportList,"关闭提醒","稍后提醒");
//				vibrator.vibrate(new long[]{200,600,200,600}, 2);
			}
			
			
			ArrayList<MedicalRemindBean> list = MedicalDBUtil.getIntance()
					.selectMedicalRemindByTime(hourMinute, selectString);
//			MyUtils.showLog("查询用药提醒    "+list.size()+"  "+list.toString());
			if (null != list&& list.size()>0) { // 查询有数据
				MyUtils.showLog("", "服务查询结果  " + list.toString());
				showMedicalAlarm(list,"关闭提醒","稍后提醒");
//				vibrator.vibrate(new long[]{200,600,200,600}, 2);
			}
			handler.postDelayed(pollingRunnable, SLEEPTIME);
		}
	};

	public void showMedicalAlarm(final ArrayList<MedicalRemindBean> list , String leftStr , String rightStr) {
		

		if( RemindDialogActivity.getInstance() != null )
			RemindDialogActivity.getInstance().finish();
		
		Intent intent = new Intent(getApplicationContext(), RemindDialogActivity.class);
		Bundle b = new Bundle();
		b.putParcelableArrayList("data", list);
		intent.putExtras(b);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);

	}

	public void showSportAlarm(final ArrayList<SportRemindBean> list , String leftStr , String rightStr) {
		if( SportRemindDialogActivity.getInstance() != null )
			SportRemindDialogActivity.getInstance().finish();
		
		Intent intent = new Intent(getApplicationContext(), SportRemindDialogActivity.class);
		Bundle b = new Bundle();
		b.putParcelableArrayList("data", list);
		intent.putExtras(b);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);

	}
	
	private Handler handler;

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		handler.postDelayed(pollingRunnable, SLEEPTIME);
		return Service.START_STICKY;
	}

	@Override
	public boolean stopService(Intent name) {
		return super.stopService(name);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

}