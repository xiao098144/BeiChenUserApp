package com.ddoctor.user.service;

//
//import io.rong.imkit.RongIM;
//import io.rong.imlib.RongIMClient;
import io.rong.imkit.RongIM;
import io.rong.imlib.RongIMClient;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.ddoctor.user.data.DataModule;
import com.ddoctor.utils.MyUtils;
import com.rongcloud.RCProvider;
import com.rongcloud.RongCloudEvent;

public class Connect2RongCloud extends Service {

	private int userId;

	@Override
	public void onCreate() {
		super.onCreate();

	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		if (userId == 0) {
			userId = DataModule.getInstance().getLoginedUserId();
		}
		if (userId == 0) {
			stopSelf();
			return super.onStartCommand(intent, flags, startId);
		}
		if (null != intent) {
			String token = DataModule.getInstance().getRYToken();
			try {
				RongIM.connect(token, new RongIMClient.ConnectCallback() {

					@Override
					public void onSuccess(String arg0) {
						MyUtils.showLog("链接融云成功  " + arg0);
						RCProvider.getInstance().setUserId(arg0);
						RongCloudEvent.getInstance().setUserId(arg0);
						RongCloudEvent.getInstance().setOtherListener();
						// try {
						// PatientBean patient = (PatientBean) spUtil
						// .deSerialize("user" + userId);
						// RCProvider.getInstance().setPatient(patient);
						// } catch (StreamCorruptedException e) {
						// e.printStackTrace();
						// } catch (ClassNotFoundException e) {
						// e.printStackTrace();
						// } catch (IOException e) {
						// e.printStackTrace();
						// }
						//
						// }
						// RCProvider.getInstance().getAllMyDocList();
					}

					@Override
					public void onError(ErrorCode errorCode) {
						if (errorCode == ErrorCode.TOKEN_INCORRECT) {
							RCProvider.getInstance().getRCToken();
						}
						
						MyUtils.showLog("", "errcode " + errorCode + "  "
								+ errorCode.getValue());
						MyUtils.showLog("", "连接融云失败");
					}
				});
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				stopSelf();
			}
		} else {
			stopSelf();
		}
		return super.onStartCommand(intent, flags, startId);
	}

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

}
