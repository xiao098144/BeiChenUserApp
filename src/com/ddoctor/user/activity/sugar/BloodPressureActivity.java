package com.ddoctor.user.activity.sugar;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.app.Dialog;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.enums.RecordLayoutType;
import com.ddoctor.enums.RefreshAction;
import com.ddoctor.enums.RetError;
import com.ddoctor.user.activity.BaseActivity;
import com.ddoctor.user.adapter.BPRecordListAdapter;
import com.ddoctor.user.task.AddBPRecordTask;
import com.ddoctor.user.task.GetBPRecordListTask;
import com.ddoctor.user.task.TaskPostCallBack;
import com.ddoctor.user.view.DDPullToRefreshView;
import com.ddoctor.user.view.DDPullToRefreshView.OnHeaderRefreshListener;
import com.ddoctor.user.wapi.bean.BloodBean;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.DialogUtil;
import com.ddoctor.utils.TimeUtil;
import com.ddoctor.utils.ToastUtil;
import com.umeng.analytics.MobclickAgent;
import com.zhuge.analysis.stat.ZhugeSDK;

public class BloodPressureActivity extends BaseActivity implements
		OnHeaderRefreshListener, OnScrollListener {

	private Button mleftBtn;
	private Button mbtn_confirm;

	private EditText met_high, met_low;

	private ListView _listView;
	DDPullToRefreshView _refreshViewContainer;
	private View _getMoreView;
	private TextView _tv_norecord;

	private int _pageNum = 1;
	private RefreshAction _refreshAction = RefreshAction.PULLTOREFRESH;

	private List<BloodBean> _dataList = new ArrayList<BloodBean>(); // 传递到adapter
	private List<BloodBean> _resultList = new ArrayList<BloodBean>(); // 服务器反馈总数据集

	private BPRecordListAdapter _adapter;

	private int mhigh_value, mlow_value; // 血糖高低压值

	private BloodBean bloodBean;

	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("BloodPressureActivity");
		MobclickAgent.onResume(BloodPressureActivity.this);
		ZhugeSDK.getInstance().init(getApplicationContext());
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("BloodPressureActivity");
		MobclickAgent.onPause(BloodPressureActivity.this);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_bloodpressure);
		findViewById();
		loadingData(true, _pageNum);
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.btn_left: {
			finishThisActivity();
		}
			break;
		case R.id.center_btn_confirm: {
			if (checkData()) {
				addBPRecord();
			}
		}
			break;
		default:
			break;
		}
	}

	private boolean checkData() {
		String highValue = met_high.getText().toString().trim();
		String lowValue = met_low.getText().toString().trim();
		if (TextUtils.isEmpty(highValue)) {
			ToastUtil.showToast("请先填写血压高压值");
			return false;
		}
		if (TextUtils.isEmpty(lowValue)) {
			ToastUtil.showToast("请先填写血压低压值");
			return false;
		}
		mhigh_value = Integer.valueOf(highValue);
		mlow_value = Integer.valueOf(lowValue);
		if (mhigh_value * mlow_value == 0) {
			ToastUtil.showToast("血压值不能为0,请重新填写");
			return false;
		}
		if (mlow_value > mhigh_value) {
			ToastUtil.showToast("血压低压值不能超出血压高压值，请重新填写");
			return false;
		}
		bloodBean = new BloodBean();
		bloodBean.setHigh(mhigh_value);
		bloodBean.setLow(mlow_value);
		bloodBean.setId(0);
		bloodBean.setTime(TimeUtil.getInstance().getStandardDate(
				getString(R.string.time_format_19)));

		return true;
	}

	/**
	 * 添加自定义事件
	 * @param eventId
	 * @param eventName
	 */
	private void addEvent(String eventId , String eventName){
		MobclickAgent.onEvent(this, eventId);
		ZhugeSDK.getInstance().onEvent(this, eventName);
	}
	
	private void addBPRecord() {
		addEvent("100007", getString(R.string.event_xy_100007));
		AddBPRecordTask task = new AddBPRecordTask(bloodBean);
		task.setTaskCallBack(new TaskPostCallBack<RetError>() {

			@Override
			public void taskFinish(RetError result) {
				if (result == RetError.NONE) {
					met_high.setText("");
					met_low.setText("");
					ToastUtil.showToast("记录保存成功");
					_resultList.add(bloodBean);
					if (_dataList.size() > 0) {
						_dataList.clear();
					}
					Collections.sort(_resultList);
					_dataList.addAll(formatList(_resultList));
					_adapter.notifyDataSetChanged();
				} else {
					ToastUtil.showToast(result.getErrorMessage());
				}
			}
		});
		task.executeParallel("");
	}

	protected List<BloodBean> formatList(List<BloodBean> list) {
		List<BloodBean> resultList = new ArrayList<BloodBean>();
		for (int i = 0; i < list.size(); i++) {
			BloodBean bloodBean = new BloodBean();
			list.get(i).setDate(
					TimeUtil.getInstance().formatDate2(list.get(i).getTime()));
			if (i == 0
					|| (i >= 1 && !list.get(i).getDate()
							.equals(list.get(i - 1).getDate()))) {
				bloodBean.setDate(list.get(i).getDate());
				bloodBean.setLayoutType(RecordLayoutType.TYPE_CATEGORY);
				resultList.add(bloodBean);
			}
			resultList.add(list.get(i));
		}
		return resultList;
	}

	protected void findViewById() {
		mleftBtn = getLeftButtonText(getString(R.string.basic_back));
		setTitle(getString(R.string.bloodpressure_title));
		mbtn_confirm = (Button) findViewById(R.id.center_btn_confirm);

		met_high = (EditText) findViewById(R.id.bloodpressure_et_high);
		met_low = (EditText) findViewById(R.id.bloodpressure_et_low);
		mleftBtn.setOnClickListener(this);
		mbtn_confirm.setOnClickListener(this);

		// met_high.addTextChangedListener(new MyTextWatcher(ETTYPE_HIGH));
		// met_low.addTextChangedListener(new MyTextWatcher(ETTYPE_LOW));

		_refreshViewContainer = (DDPullToRefreshView) findViewById(R.id.refreshViewContainer);
		_refreshViewContainer.setOnHeaderRefreshListener(this);
		_refreshViewContainer.setVisibility(View.INVISIBLE);

		_listView = (ListView) findViewById(R.id.listView);
		_listView.setOnScrollListener(this);
		_tv_norecord = (TextView) findViewById(R.id.tv_norecord);
		_tv_norecord.setVisibility(View.INVISIBLE);
		_listView.setEmptyView(_tv_norecord);
		initList();

	}

	private void initList() {
		// 获取更多
		_getMoreView = createGetMoreView();
		setGetMoreContent("已全部加载", false, false);
		_listView.addFooterView(_getMoreView);

		// 数据
		_adapter = new BPRecordListAdapter(this);
		_listView.setAdapter(_adapter);
		_adapter.setData(_dataList);
	}

	// 加载更多相关函数 >>>>>>
	boolean _bGetMoreEnable = false;

	private View createGetMoreView() {
		if (_getMoreView != null)
			return _getMoreView;

		View v = (View) getLayoutInflater().inflate(R.layout.refresh_footer,
				null);

		return v;
	}

	private void setGetMoreContent(String message, boolean showImage,
			boolean animation) {
		TextView tv = (TextView) _getMoreView
				.findViewById(R.id.pull_to_load_text);
		tv.setText(message);

		ImageView imgView = (ImageView) _getMoreView
				.findViewById(R.id.pull_to_load_image);
		AnimationDrawable ad = (AnimationDrawable) imgView.getBackground();
		if (showImage) {
			if (animation) {
				ad.start();
			} else {
				ad.stop();
				ad.selectDrawable(0);
			}

			imgView.setVisibility(View.VISIBLE);
		} else {
			ad.stop();
			ad.selectDrawable(0);

			imgView.setVisibility(View.GONE);
		}
	}

	private Dialog _loadingDialog = null;

	private void loadingData(boolean showLoading, int page) {
		if (showLoading) {
			_loadingDialog = DialogUtil.createLoadingDialog(this, "加载中...");
			_loadingDialog.show();
		}

		GetBPRecordListTask task = new GetBPRecordListTask(page);

		final int page1 = page;
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {

			@Override
			public void taskFinish(DDResult result) {
				if (result.getError() == RetError.NONE) {
					List<BloodBean> tmpList = result.getBundle()
							.getParcelableArrayList("list");
					if (page1 > 1) // 加载更多
					{
						_resultList.addAll(tmpList);
						Collections.sort(_resultList);
						_dataList.clear();
						_dataList.addAll(formatList(_resultList));
						_adapter.notifyDataSetChanged();
					} else {
						// 加载第一页
						_dataList.clear();
						_resultList.clear();
						_resultList.addAll(tmpList);
						Collections.sort(_resultList);
						_dataList.addAll(formatList(_resultList));
						_adapter.notifyDataSetChanged();

						_refreshViewContainer.onHeaderRefreshComplete();
						_refreshViewContainer.setVisibility(View.VISIBLE);
						_refreshAction = RefreshAction.NONE;

						if (_loadingDialog != null)
							_loadingDialog.dismiss();

					}

					// 是否显示"加载更多"
					if (tmpList.size() > 0) {
						setGetMoreContent("滑动加载更多", true, false);
						_bGetMoreEnable = true;
					} else {
						if (page1 == 1) {
							_tv_norecord.setText(result.getErrorMessage());
							_tv_norecord.setVisibility(View.VISIBLE);
							_tv_norecord.setTag(0);
						}
						// 没数据了，加载完成
						setGetMoreContent("已全部加载", false, false);
						_bGetMoreEnable = false;
					}

					// 确保加载成功后，再修改这个变量
					_pageNum = page1;
				} else {// 加载失败
					if (page1 > 1) {
						setGetMoreContent("滑动加载更多", true, false);
					} else {
						_refreshViewContainer.onHeaderRefreshComplete();
						_refreshAction = RefreshAction.NONE;

						if (_loadingDialog != null)
							_loadingDialog.dismiss();
					}

					ToastUtil.showToast(result.getErrorMessage());
				}

				_refreshAction = RefreshAction.NONE;
			}
		});

		task.executeParallel("");
	}

	@Override
	public void onScroll(AbsListView arg0, int firstVisibleItem, int arg2,
			int arg3) {

		// 工具栏的显示与隐藏
		if (_refreshAction == RefreshAction.NONE) {
			if (_bGetMoreEnable) {// 有加载更多
				int lastPos = _listView.getLastVisiblePosition();
				int total = _listView.getHeaderViewsCount() + _dataList.size()
						+ _listView.getFooterViewsCount();
				if (lastPos == total - 1) {
					// 加载更多显示出来了，开始加载
					_refreshAction = RefreshAction.LOADMORE;
					setGetMoreContent("正在加载...", true, true);
					loadingData(false, _pageNum + 1);
				}
			}
		}

	}

	@Override
	public void onScrollStateChanged(AbsListView arg0, int arg1) {

	}

	@Override
	public void onHeaderRefresh(DDPullToRefreshView view) {
		if (_refreshAction == RefreshAction.NONE) {
			_refreshAction = RefreshAction.PULLTOREFRESH;
			loadingData(false, 1);
		} else {
			// 正在加载，什么也不做
			view.onHeaderRefreshComplete();
		}
	}

	// private final static int ETTYPE_HIGH = 1;
	// private final static int ETTYPE_LOW = 2;
	//
	// private class MyTextWatcher implements TextWatcher {
	//
	// private int type;
	//
	// public MyTextWatcher(int type) {
	// super();
	// this.type = type;
	// }
	//
	// @Override
	// public void beforeTextChanged(CharSequence s, int start, int count,
	// int after) {
	// if (type == ETTYPE_HIGH) {
	//
	// } else if(type == ETTYPE_LOW){
	//
	// }
	// }
	//
	// @Override
	// public void onTextChanged(CharSequence s, int start, int before,
	// int count) {
	// // TODO Auto-generated method stub
	//
	// }
	//
	// @Override
	// public void afterTextChanged(Editable s) {
	// if (type == ETTYPE_HIGH) {
	//
	// } else if(type == ETTYPE_LOW){
	//
	// }
	// }
	//
	// }

}
