package com.ddoctor.user.activity.sugar;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.enums.RetError;
import com.ddoctor.interfaces.OnClickCallBackListener;
import com.ddoctor.user.activity.BaseActivity;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.data.MyProfile;
import com.ddoctor.user.task.AddUpdateSugarRecordTask;
import com.ddoctor.user.task.TaskPostCallBack;
import com.ddoctor.user.view.DiscView;
import com.ddoctor.user.view.DiscView.DiscViewOnChangedListener;
import com.ddoctor.user.wapi.bean.SugarValueBean;
import com.ddoctor.user.wapi.bean.TroubleitemBean;
import com.ddoctor.utils.DialogUtil;
import com.ddoctor.utils.DialogUtil.OnTimeSelectedListener;
import com.ddoctor.utils.MyUtils;
import com.ddoctor.utils.StringUtils;
import com.ddoctor.utils.TimeUtil;
import com.ddoctor.utils.ToastUtil;
import com.pageindicator.SugarTabIndicator;
import com.pageindicator.SugarTabIndicator.TabSelectedListener;
import com.umeng.analytics.MobclickAgent;
import com.zhuge.analysis.stat.ZhugeSDK;

public class AddBloodSuagrActivity extends BaseActivity implements
		OnCheckedChangeListener, DiscViewOnChangedListener,
		TabSelectedListener, OnTimeSelectedListener, OnClickCallBackListener {

	private SugarTabIndicator indicator;
	private TextView tv_time;
	private TextView tv_notice;
	private CheckBox cb_unit;
	private EditText et_remark;
	private Button btn_common;
	private TextView tv_value_mmol;
	private TextView tv_value_mg;
	private DiscView discView;
	private Button leftBtn;
	private Button rightBtn;

	private String notice = "";

	private String clickArea;
	private String[] type;

	private SugarValueBean sugarValueBean;
	private SugarValueBean updateBean = new SugarValueBean();
	private boolean isUpdate = false;

	private void getIntentInfo() {
		try {
			Intent intent = getIntent();
			if (intent!=null&&intent.hasExtra("data")) {
				sugarValueBean = getIntent().getParcelableExtra("data");
				isUpdate = true;
			}
		} catch (Exception e) {
			isUpdate = false;
			e.printStackTrace();
		}
	}

	private void initData() {
		getIntentInfo();
		clickArea = getResources().getString(R.string.clickArea);
		type = getResources().getStringArray(R.array.sugar_type);
		screenWidth = MyUtils.getScreenWidth(AddBloodSuagrActivity.this);
	}

	int screenWidth;

	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("AddBloodSuagrActivity");
		MobclickAgent.onResume(AddBloodSuagrActivity.this);
		ZhugeSDK.getInstance().init(getApplicationContext());
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("AddBloodSuagrActivity");
		MobclickAgent.onPause(AddBloodSuagrActivity.this);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_addbloodsugar);
		initData();
		findViewById();
	}

	protected void findViewById() {
		leftBtn = getLeftButtonText(getResources().getString(
				R.string.basic_back));
		rightBtn = getRightButtonText(getResources().getString(
				R.string.basic_save));
		TextView tv_center_title = getTopTitle();
		tv_center_title.setOnClickListener(this);
		
		indicator = (SugarTabIndicator) findViewById(R.id.addbs_sugar_indicator);
		tv_time = (TextView) findViewById(R.id.addbs_tv_time);
		tv_notice = (TextView) findViewById(R.id.addbs_tv_notice);
		tv_notice.setMovementMethod(LinkMovementMethod.getInstance());
		tv_value_mmol = (TextView) findViewById(R.id.addbs_tv_value_mmol);
		tv_value_mg = (TextView) findViewById(R.id.addbs_tv_value_mg);
		cb_unit = (CheckBox) findViewById(R.id.addbs_check);
		et_remark = (EditText) findViewById(R.id.addbs_et_remark);
		btn_common = (Button) findViewById(R.id.addbs_btn_normal);
		et_remark.setHint(StringUtils.fromatETHint(
				getResources().getString(R.string.sugar_addbs_remark), 15));
		discView = (DiscView) findViewById(R.id.discView);
		discView.setDiscViewOnChangedListener(this);
		if (isUpdate) {
			setTitle(TimeUtil.getInstance().getFormatDate(
					sugarValueBean.getTime(), "MM/dd"));
			tv_time.setText(TimeUtil.getInstance().formatReplyTime2(sugarValueBean.getTime()));
			if (!TextUtils.isEmpty(sugarValueBean.getRemark())) {
				et_remark.setText(sugarValueBean.getRemark());
			}
			tv_value_mmol.setText(String.valueOf(sugarValueBean.getValue()));
			if (sugarValueBean.getUnit()==1) {
				cb_unit.setChecked(true);
			} 
			setDiscViewValue(sugarValueBean.getValue());
		} else {
			tv_time.setText(TimeUtil.getInstance().getStandardDate(
					getResources().getString(R.string.time_format_16)));
			setTitle(TimeUtil.getInstance().getStandardDate("MM/dd"));
		}

		changeNotice(notice);
		indicator.setTabSelectedListener(this);
		indicator.setTabWidth(screenWidth);
		if (isUpdate) {
			int tabIndex = sugarValueBean.getType();
			if (tabIndex==7) {
				tabIndex = 1;
			}else if(tabIndex<7){
				tabIndex +=2; 
			}
			indicator.setTitle(type , tabIndex);
		} else {
			indicator.setTitle(type, 1 + getTimeTypeIndex(TimeUtil.getInstance()
					.getCurrentHour()));
		}
		
		tv_time.setOnClickListener(this);
		cb_unit.setOnCheckedChangeListener(this);
		btn_common.setOnClickListener(this);
		leftBtn.setOnClickListener(this);
		rightBtn.setOnClickListener(this);
	}

	/** 根据时间值获取对应索引值 */
	public int getTimeTypeIndex(int hour) {

		if (hour >= 5 && hour <= 8) { // 早餐前
			return 1;
		} else if (hour > 8 && hour < 10) { // 早餐后
			return 2;
		} else if (hour >= 10 && hour < 12) { // 午餐前
			return 3;
		} else if (hour >= 12 && hour < 15) { // 午餐后
			return 4;
		} else if (hour >= 15 && hour <= 18) { // 晚餐前
			return 5;
		} else if (hour > 18 && hour <= 21) { // 晚餐后
			return 6;
		} else if (hour > 21 && hour < 24) {
			return 7; // 睡前
		}
		return 0; // 凌晨
	}

	private ClickableSpan clickableSpan = new ClickableSpan() {

		@Override
		public void onClick(View widget) {
			ToastUtil.showToast("点击小贴士 查看更多");
		}
	};

	private void changeNotice(String notice) {
		SpannableStringBuilder str = new SpannableStringBuilder(notice);
		str.append(clickArea);
		str.setSpan(new UnderlineSpan(), str.length() - clickArea.length(),
				str.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		str.setSpan(clickableSpan, str.length() - clickArea.length(),
				str.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		str.setSpan(new ForegroundColorSpan(Color.RED), str.length()
				- clickArea.length(), str.length(),
				Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		tv_notice.setText(str);
	}

	private List<TroubleitemBean> _chosedList = new ArrayList<TroubleitemBean>();
	
	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.addbs_btn_normal: {
			DialogUtil.showDiscomfirtMultiDialog(AddBloodSuagrActivity.this,
					DataModule.loadDict(MyProfile.DICT_TROUBLEITEMS,
							TroubleitemBean.class), _chosedList, this);
		}
			break;
		case R.id.title_center_txt:
		case R.id.addbs_tv_time: {
			DialogUtil.dateTimePicker(0, AddBloodSuagrActivity.this, this);
		}
			break;
		case R.id.btn_right: {
			checkData();
			managerSugarRecord();
		}
			break;
		case R.id.btn_left: {
			finishThisActivity();
		}
			break;

		default:
			break;
		}
	}

	private Dialog loadingDialog;

	private void checkData() {
		if (sugarValueBean == null) {
			updateBean.setId(0);
		} else {
			updateBean.setId(sugarValueBean.getId());
		}
		updateBean.setTime(tv_time.getText().toString() + ":00");
		MyUtils.showLog(" checkdata position "+position);
		int type = position;
		if (position - 1 == 0) {
			type = 7;
		} else {
			type -= 2;
		}
		updateBean.setType(type);
		updateBean.setGlucometerId(0);
		updateBean.setRemark("");
		if (cb_unit.isChecked()) {
			updateBean.setUnit(1);
		} else {
			updateBean.setUnit(2);
		}
		updateBean.setValue(Float.valueOf(tv_value_mmol.getText()
				.toString()));
		updateBean.setRemark(_chosedResult);
	}

	/**
	 * 添加自定义事件
	 * @param eventId
	 * @param eventName
	 */
	private void addEvent(String eventId , String eventName){
		MobclickAgent.onEvent(this, eventId);
		ZhugeSDK.getInstance().onEvent(this, eventName);
	}
	
	private void managerSugarRecord() {
		loadingDialog = DialogUtil.createLoadingDialog(this, "正在加载中，请稍候...");
		loadingDialog.show();
		addEvent("100001", getString(R.string.event_xt_100001));
		AddUpdateSugarRecordTask task = new AddUpdateSugarRecordTask(updateBean);
		task.setTaskCallBack(new TaskPostCallBack<RetError>() {

			@Override
			public void taskFinish(RetError result) {
				loadingDialog.dismiss();
				if (result == RetError.NONE) {
					StringBuffer sb = new StringBuffer();
					sb.append(type[position-1]);
					sb.append(" ");
					sb.append(TimeUtil.getInstance().formatTime(updateBean.getTime()));
					StringBuffer sb2 = new StringBuffer();
					Float value = updateBean.getValue();
					
					sb2.append(String.format("血糖值为：%1$.1f%2$s",value , updateBean.getUnit()==2?"mmol/L":"mg/dl"));
					
					StringBuffer sb3 = new StringBuffer();
					int color = 0;
					if (value<=DataModule.getInstance().getSugarDownBound()) {
						sb3.append(getString(R.string.sugar_notice_low));
						color = Color.parseColor("#f0b423");
					}else if (value>=DataModule.getInstance().getSugarUpBound()) {
						sb3.append(getString(R.string.sugar_notice_high));
						color = Color.parseColor("#ff7373");
					}else {
						sb3.append(getString(R.string.sugar_notice_normal));
						color = Color.parseColor("#469502");
					} 
					
					showNoticeDialog(sb.toString() , sb2.toString() , sb3.toString() , color);
				} else {
					ToastUtil.showToast(result.getErrorMessage());
				}
			}
		});
		task.executeParallel("");
	}

	private void showNoticeDialog(String title , String value , String notice, int color ){
		final Dialog dialog = new Dialog(AddBloodSuagrActivity.this, R.style.NoTitleDialog);
		View view = LayoutInflater.from(AddBloodSuagrActivity.this).inflate(
				R.layout.layout_notice, null);
		TextView tv_title = (TextView) view
				.findViewById(R.id.tv_title);
		TextView tv_notice = (TextView) view
				.findViewById(R.id.tv_notice);
		TextView tv_value = (TextView) view
				.findViewById(R.id.tv_value);
		Button btn_confirm = (Button) view
				.findViewById(R.id.center_btn_confirm);
		btn_confirm.setText("知道了");
		
		tv_title.setText(title);
		tv_value.setText(value);
		tv_notice.setText(notice);
		if (color == 0) {
			color = getResources().getColor(R.color.color_text_gray_dark);
		}
		tv_notice.setTextColor(color);
		btn_confirm.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
				if (isUpdate) {
					ToastUtil.showToast("修改成功");
					setResult(RESULT_OK);
				} else {
					ToastUtil.showToast("保存成功");
				}
				DataModule.getInstance().setUpdateSugar(true);
				finishThisActivity();
			}
		});
		
		Window window = dialog.getWindow();
		window.setGravity(Gravity.CENTER);
		window.setLayout(-2, -2);
		dialog.setContentView(view);
		dialog.setCancelable(false);
		dialog.setCanceledOnTouchOutside(false);
		dialog.show();

	}
	
	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

		if (isChecked) {
			String valueInmmol = tv_value_mmol.getText().toString().trim();
			if (!TextUtils.isEmpty(valueInmmol)) {
				if (StringUtils.pureNum(valueInmmol)) {
					setValue2mg(valueInmmol);
				}
			}
			tv_value_mg.setVisibility(View.VISIBLE);

		} else {
			tv_value_mg.setVisibility(View.GONE);
		}
	}

	private void setValue2mg(String valueInmmol) {
		float valueInmg = Float.valueOf(valueInmmol)
				* Integer.valueOf(getResources().getString(R.string.mmol2mg));

		StringBuilder sb = new StringBuilder();
		sb.append(StringUtils.formatnum(valueInmg, "#.0"));
		sb.append("mg/dl");
		tv_value_mg.setText(sb.toString());
	}

	@Override
	public void onDegreeChanged(float degree) {
		String s = String.format(Locale.CHINESE, "%.01f", DiscView.degree2value(degree));
		tv_value_mmol.setText(s);
		if (cb_unit.isChecked()) {
			setValue2mg(s);
			tv_value_mg.setVisibility(View.VISIBLE);
		}
	}
	
	// 调用这个函数来设置转盘的值
	private void setDiscViewValue(float value)
	{
		// value转成角度

		// 0.1f: 每个刻度为0.1
		// 3.0f: 每个刻度对应的3度
		// 这2个数值是根据设计图来的
		
		float degree = DiscView.value2degree(value);
		
		discView.setDegree(degree, true);
	}

	
	private int position;

	@Override
	public void onSelected(int position) {
		this.position = position;
		changeNotice(type[position - 1]);
	}

	@Override
	public int getSelectedPosiiton() {
		return position;
	}

	@Override
	public void ontimeSelected(int year, int month, int day, int hour,
			int minute) {
		StringBuffer sb = new StringBuffer();
		sb.append(year);
		sb.append("-");
		String monthStr = StringUtils.formatnum(month, "00");
		sb.append(monthStr);
		sb.append("-");
		String dayStr = StringUtils.formatnum(day, "00");
		sb.append(dayStr);
		sb.append(" ");
		sb.append(StringUtils.formatnum(hour, "00") + ":");
		sb.append(StringUtils.formatnum(minute, "00"));
		tv_time.setText(sb.toString());
		setTitle(monthStr + "/" + dayStr);
	}

	private String _chosedResult = "";
	
	@Override
	public void onClickCallBack(Bundle data) {
		ArrayList<TroubleitemBean> list = data.getParcelableArrayList("list");
		if (null != list && list.size()>0) {
			_chosedList.addAll(list);
		}
		_chosedResult = data.getString("data");
		et_remark.setText(_chosedResult.replace("|", "、"));
	}
	
}
