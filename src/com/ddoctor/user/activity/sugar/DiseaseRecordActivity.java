package com.ddoctor.user.activity.sugar;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Base64;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.enums.RecordLayoutType;
import com.ddoctor.enums.RefreshAction;
import com.ddoctor.enums.RetError;
import com.ddoctor.user.activity.BaseActivity;
import com.ddoctor.user.adapter.DiseaseListAdapter;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.task.AddDiseaseRecordTask;
import com.ddoctor.user.task.FileUploadTask;
import com.ddoctor.user.task.GetDiseaseListTask;
import com.ddoctor.user.task.TaskPostCallBack;
import com.ddoctor.user.view.DDPullToRefreshView;
import com.ddoctor.user.view.DDPullToRefreshView.OnHeaderRefreshListener;
import com.ddoctor.user.wapi.bean.DiseaseBean;
import com.ddoctor.user.wapi.bean.UploadBean;
import com.ddoctor.user.wapi.constant.Upload;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.DialogUtil;
import com.ddoctor.utils.FileOperationUtil;
import com.ddoctor.utils.ImageLoaderUtil;
import com.ddoctor.utils.MyUtils;
import com.ddoctor.utils.StringUtils;
import com.ddoctor.utils.TimeUtil;
import com.ddoctor.utils.ToastUtil;
import com.umeng.analytics.MobclickAgent;
import com.zhuge.analysis.stat.ZhugeSDK;

public class DiseaseRecordActivity extends BaseActivity implements
		OnHeaderRefreshListener, OnScrollListener {

	private TextView mtv_type;
	private EditText met_content;

	private HorizontalScrollView _hsc;
	private ImageView[] _img = new ImageView[9];
	private ArrayList<String> _urlList = new ArrayList<String>();
  
	private Button mbtn_pic;
  
	private ListView _listView;
	DDPullToRefreshView _refreshViewContainer;
	private View _getMoreView;
	private TextView _tv_norecord;

	private int _pageNum = 1;
	private RefreshAction _refreshAction = RefreshAction.PULLTOREFRESH;

	private List<DiseaseBean> _dataList = new ArrayList<DiseaseBean>(); // 传递到adapter
	private List<DiseaseBean> _resultList = new ArrayList<DiseaseBean>(); // 服务器反馈总数据集

	private DiseaseListAdapter _adapter;

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		MyUtils.showLog("DiseaseRecordActivity onSave ---------  保存页面  ");
		String content = met_content.getText().toString().trim();
		if (!TextUtils.isEmpty(content)) {
			outState.putString("remark", content);
		}
		outState.putStringArrayList("url", _urlList);
		 super.onSaveInstanceState(outState);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		MyUtils.showLog("diseaseRecortdActivity  onRestore =====  页面销毁重建  "+savedInstanceState.toString());
		MyUtils.showLog(" onRestoreInstanceState "+_urlList.toString());
		super.onRestoreInstanceState(savedInstanceState);
	}

	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("DiseaseRecordActivity");
		MobclickAgent.onResume(DiseaseRecordActivity.this);
		ZhugeSDK.getInstance().init(getApplicationContext());
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("DiseaseRecordActivity");
		MobclickAgent.onPause(DiseaseRecordActivity.this);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		if (null != savedInstanceState) {
			MyUtils.showLog(" DiseaseRecordActivity onCreate 销毁重建    "+savedInstanceState.toString());
		
		}
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_diseaselist);
		findViewById();
		loadingData(true, _pageNum);
	}

	protected List<DiseaseBean> formatList(List<DiseaseBean> list) {
		List<DiseaseBean> resultList = new ArrayList<DiseaseBean>();
		for (int i = 0; i < list.size(); i++) {
			DiseaseBean diseaseBean = new DiseaseBean();
			list.get(i).setDate(
					TimeUtil.getInstance().formatDate2(list.get(i).getTime()));
			if (i == 0
					|| (i >= 1 && !list.get(i).getDate()
							.equals(list.get(i - 1).getDate()))) {
				diseaseBean.setDate(list.get(i).getDate());
				diseaseBean.setLayoutType(RecordLayoutType.TYPE_CATEGORY);
				resultList.add(diseaseBean);
			}
			resultList.add(list.get(i));
		}
		return resultList;
	}

	private Dialog _loadingDialog = null;

	private void loadingData(boolean showLoading, int page) {
		if (showLoading) {
			_loadingDialog = DialogUtil.createLoadingDialog(this, "加载中...");
			_loadingDialog.show();
		}

		GetDiseaseListTask task = new GetDiseaseListTask(page);

		final int page1 = page;
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {

			@Override
			public void taskFinish(DDResult result) {
				if (result.getError() == RetError.NONE) {
					List<DiseaseBean> tmpList = result.getBundle()
							.getParcelableArrayList("list");
					if (page1 > 1) // 加载更多
					{
						_resultList.addAll(tmpList);
						Collections.sort(_resultList);
						_dataList.clear();
						_dataList.addAll(formatList(_resultList));
						_adapter.notifyDataSetChanged();
					} else {
						// 加载第一页
						_dataList.clear();
						_resultList.clear();
						_resultList.addAll(tmpList);
						Collections.sort(_resultList);
						_dataList.addAll(formatList(_resultList));
						_adapter.notifyDataSetChanged();

						_refreshViewContainer.onHeaderRefreshComplete();
						_refreshViewContainer.setVisibility(View.VISIBLE);
						_refreshAction = RefreshAction.NONE;

						if (_loadingDialog != null)
							_loadingDialog.dismiss();

					}

					// 是否显示"加载更多"
					if (tmpList.size() > 0) {
						setGetMoreContent("滑动加载更多", true, false);
						_bGetMoreEnable = true;
					} else {
						if (page1 == 1) {
							_tv_norecord.setText(result.getErrorMessage());
							_tv_norecord.setVisibility(View.VISIBLE);
							_tv_norecord.setTag(0);
						}
						// 没数据了，加载完成
						setGetMoreContent("已全部加载", false, false);
						_bGetMoreEnable = false;
					}

					// 确保加载成功后，再修改这个变量
					_pageNum = page1;
				} else {// 加载失败
					if (page1 > 1) {
						setGetMoreContent("滑动加载更多", true, false);
					} else {
						_refreshViewContainer.onHeaderRefreshComplete();
						_refreshAction = RefreshAction.NONE;

						if (_loadingDialog != null)
							_loadingDialog.dismiss();
					}

					ToastUtil.showToast(result.getErrorMessage());
				}

				_refreshAction = RefreshAction.NONE;
			}
		});

		task.executeParallel("");
	}

	/**
	 * 添加自定义事件
	 * @param eventId
	 * @param eventName
	 */
	private void addEvent(String eventId , String eventName){
		MobclickAgent.onEvent(this, eventId);
		ZhugeSDK.getInstance().onEvent(this, eventName);
	}
	
	private void addDiseaseRecord() {
		addEvent("100008", getString(R.string.event_bl_100008));
		AddDiseaseRecordTask task = new AddDiseaseRecordTask(diseaseBean);
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {

			@Override
			public void taskFinish(DDResult result) {
				if (result.getError() == RetError.NONE) {
					ToastUtil.showToast("记录保存成功");
					met_content.setText("");
					_urlList.clear();
					for (int i = 0; i < _urlList.size(); i++) {
						_img[i].setBackgroundResource(0);
						_img[i].setVisibility(View.GONE);
					}
					_hsc.setVisibility(View.GONE);
					_resultList.add(diseaseBean);
					Collections.sort(_resultList);
					if (_dataList.size() > 0) {
						_dataList.clear();
					}
					_dataList.addAll(formatList(_resultList));
					_adapter.notifyDataSetChanged();
				} else {
					ToastUtil.showToast(result.getErrorMessage());
				}
			}
		});
		task.executeParallel("");

	}

	private DiseaseBean diseaseBean;

	private boolean checkdata() {
		diseaseBean = new DiseaseBean();
		diseaseBean.setId(0);
		String trim = met_content.getText().toString().trim();
		if (TextUtils.isEmpty(trim)) {
			ToastUtil.showToast("病历内容不能为空");
			return false;
		}
		diseaseBean.setContent(trim);
		diseaseBean.setRemark("");
		diseaseBean.setTime(TimeUtil.getInstance().getStandardDate(
				getString(R.string.time_format_19)));
		diseaseBean.setType(4);

		String fileUrl = "";
		if (_urlList != null && _urlList.size() > 0) {
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < _urlList.size(); i++) {
				sb.append(_urlList.get(i));
				sb.append("|");
			}
			fileUrl = sb.toString();
		}
		if (fileUrl != null && fileUrl.endsWith("|")) {
			fileUrl = fileUrl.substring(0, fileUrl.length() - 1);
		}
		diseaseBean.setFile(fileUrl);

		return true;
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.btn_left: {
			finishThisActivity();
		}
			break;
		case R.id.btn_right: {
			if (checkdata()) {
				addDiseaseRecord();
			}
		}
			break;
		case R.id.center_btn_confirm: {
			if (_currentImgIndex < 8) {
				String[] items = new String[]{"本地图片","拍照"};
				DialogUtil.createListDialog(DiseaseRecordActivity.this, items, new DialogUtil.ListDialogCallback(){

					@Override
					public void onItemClick(int which) {
						if(which == 0 )
						{
							Intent intent = new Intent();
							intent.setType("image/*");
							intent.setAction(Intent.ACTION_GET_CONTENT);
							startActivityForResult(intent, 0);	
						}
						else
						{
							Intent intent = new Intent(
									MediaStore.ACTION_IMAGE_CAPTURE);
							String filename = DataModule
									.getTakePhotoTempFilename("disease");
							File f = new File(filename);
							if (f.exists())
								f.delete();
							Uri uri = Uri.fromFile(f);
							intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
							startActivityForResult(intent, 1);
						}
					}
				}).show();
			} else {
				ToastUtil.showToast("很抱歉，最多添加九张图片");
			}

		}
			break;
		case R.id.disease_tv_type: {

		}
			break;

		default:
			break;
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		MyUtils.showLog("requestCode=" + requestCode + " resultCode="
				+ resultCode);

		if (resultCode != Activity.RESULT_CANCELED) {
			if (requestCode == 0) {// 选择图片
				if (data != null) {
					MyUtils.showLog("  " + data.getData());

					String img_path = MyUtils.getPathFromUri(
							DiseaseRecordActivity.this, data.getData());
					try {
						Bitmap bitmap = MyUtils.loadBitmapFromFile(img_path);
						onUploadPhoto(MyUtils.resizeImage(bitmap, 500));
					} catch (Exception e) {
						ToastUtil.showToast("获取图片失败!");
					}
				}
			} else if (requestCode == 1) {// 拍照
				String filename = DataModule.getTakePhotoTempFilename("disease");
				File f = new File(filename);
				if (f.exists()) {
					try {
						Bitmap bitmap = MyUtils.loadBitmapFromFile(f
								.getAbsolutePath());
						onUploadPhoto(MyUtils.resizeImage(bitmap, 500));
					} catch (Exception e) {
						ToastUtil.showToast("获取图片失败!");
					}
				} else
					ToastUtil.showToast("获取图片失败!");
			}
		}

		super.onActivityResult(requestCode, resultCode, data);
	}

	// 上传头像
	private Bitmap _bitmap = null;

	private void onUploadPhoto(Bitmap bmp) {
		if (_bitmap != null) {
			_bitmap.recycle();
			_bitmap = null;
		}
		_bitmap = bmp;

		_loadingDialog = DialogUtil.createLoadingDialog(
				DiseaseRecordActivity.this, "提交中...");
		_loadingDialog.show();

		UploadBean uploadBean = new UploadBean();
		uploadBean.setFileType("jpg");

		uploadBean.setType(Upload.DISEASE_IMAGE);

		byte[] data = MyUtils.Bitmap2Bytes(bmp);
		String s_data = android.util.Base64
				.encodeToString(data, Base64.DEFAULT);
		uploadBean.setFile(s_data);

		FileUploadTask task = new FileUploadTask(uploadBean);
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {
			@Override
			public void taskFinish(DDResult result) {

				if (_loadingDialog != null) {
					_loadingDialog.dismiss();
					_loadingDialog = null;
				}

				if (result.getError() == RetError.NONE) {
					on_task_finished(result);
				} else {
					on_task_failed(result);
				}

			}
		});
		task.executeParallel("");
	}

	private void on_task_finished(DDResult result) {
		FileOperationUtil.deleteFile(DataModule.getTakePhotoTempFilename("disease"));
		MyUtils.showLog("on_task_finished");
		Bundle b = result.getBundle();
		String fileUrl = b.getString("fileUrl");
		MyUtils.showLog(fileUrl);
		_urlList.add(_currentImgIndex, fileUrl);
		if (_hsc.getVisibility() != View.VISIBLE) {
			_hsc.setVisibility(View.VISIBLE);
		}
		_img[_currentImgIndex].setVisibility(View.VISIBLE);
		ImageLoaderUtil.display(StringUtils.urlFormatThumbnail(fileUrl),
				_img[_currentImgIndex], R.drawable.rc_image_download_failure);
		_currentImgIndex++;
	}

	private int _currentImgIndex = 0;

	private void on_task_failed(DDResult result) {
		ToastUtil.showToast(result.getErrorMessage());
	}

	protected void findViewById() {
		Button leftBtn = getLeftButtonText(getString(R.string.basic_back));
		Button rightBtn = getRightButtonText(getString(R.string.basic_save));
		setTitle("病历");

		mtv_type = (TextView) findViewById(R.id.disease_tv_type);
		mtv_type.setText("病历");
		met_content = (EditText) findViewById(R.id.disease_et_content);

		mbtn_pic = (Button) findViewById(R.id.center_btn_confirm);
		mbtn_pic.setText("添加图片");
		findViewById(R.id.center_btn_cancel).setVisibility(View.GONE);

		_hsc = (HorizontalScrollView) findViewById(R.id.disease_lv);
		_img[0] = (ImageView) findViewById(R.id.disease_img1);
		_img[1] = (ImageView) findViewById(R.id.disease_img2);
		_img[2] = (ImageView) findViewById(R.id.disease_img3);
		_img[3] = (ImageView) findViewById(R.id.disease_img4);
		_img[4] = (ImageView) findViewById(R.id.disease_img5);
		_img[5] = (ImageView) findViewById(R.id.disease_img6);
		_img[6] = (ImageView) findViewById(R.id.disease_img7);
		_img[7] = (ImageView) findViewById(R.id.disease_img8);
		_img[8] = (ImageView) findViewById(R.id.disease_img9);

		leftBtn.setOnClickListener(this);
		rightBtn.setOnClickListener(this);
		mbtn_pic.setOnClickListener(this);
		mtv_type.setOnClickListener(this);

		_refreshViewContainer = (DDPullToRefreshView) findViewById(R.id.refreshViewContainer);
		_refreshViewContainer.setOnHeaderRefreshListener(this);
		_refreshViewContainer.setVisibility(View.INVISIBLE);

		_listView = (ListView) findViewById(R.id.listView);
		_listView.setOnScrollListener(this);
		_tv_norecord = (TextView) findViewById(R.id.tv_norecord);
		_tv_norecord.setVisibility(View.INVISIBLE);
		_listView.setEmptyView(_tv_norecord);
		initList();
	}

	private void initList() {
		// 获取更多
		_getMoreView = createGetMoreView();
		setGetMoreContent("已全部加载", false, false);
		_listView.addFooterView(_getMoreView);

		// 数据
		_adapter = new DiseaseListAdapter(this);
		_listView.setAdapter(_adapter);
		_adapter.setData(_dataList);
	}

	// 加载更多相关函数 >>>>>>
	boolean _bGetMoreEnable = false;

	private View createGetMoreView() {
		if (_getMoreView != null)
			return _getMoreView;

		View v = (View) getLayoutInflater().inflate(R.layout.refresh_footer,
				null);

		return v;
	}

	private void setGetMoreContent(String message, boolean showImage,
			boolean animation) {
		TextView tv = (TextView) _getMoreView
				.findViewById(R.id.pull_to_load_text);
		tv.setText(message);

		ImageView imgView = (ImageView) _getMoreView
				.findViewById(R.id.pull_to_load_image);
		AnimationDrawable ad = (AnimationDrawable) imgView.getBackground();
		if (showImage) {
			if (animation) {
				ad.start();
			} else {
				ad.stop();
				ad.selectDrawable(0);
			}

			imgView.setVisibility(View.VISIBLE);
		} else {
			ad.stop();
			ad.selectDrawable(0);

			imgView.setVisibility(View.GONE);
		}
	}

	@Override
	public void onScroll(AbsListView arg0, int firstVisibleItem, int arg2,
			int arg3) {

		// 工具栏的显示与隐藏
		if (_refreshAction == RefreshAction.NONE) {
			if (_bGetMoreEnable) {// 有加载更多
				int lastPos = _listView.getLastVisiblePosition();
				int total = _listView.getHeaderViewsCount() + _dataList.size()
						+ _listView.getFooterViewsCount();
				if (lastPos == total - 1) {
					// 加载更多显示出来了，开始加载
					_refreshAction = RefreshAction.LOADMORE;
					setGetMoreContent("正在加载...", true, true);
					loadingData(false, _pageNum + 1);
				}
			}
		}

	}

	@Override
	public void onScrollStateChanged(AbsListView arg0, int arg1) {

	}

	@Override
	public void onHeaderRefresh(DDPullToRefreshView view) {
		if (_refreshAction == RefreshAction.NONE) {
			_refreshAction = RefreshAction.PULLTOREFRESH;
			loadingData(false, 1);
		} else {
			// 正在加载，什么也不做
			view.onHeaderRefreshComplete();
		}
	}

}
