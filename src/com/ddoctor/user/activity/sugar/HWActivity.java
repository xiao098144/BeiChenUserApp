package com.ddoctor.user.activity.sugar;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.app.Dialog;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.enums.RecordLayoutType;
import com.ddoctor.enums.RefreshAction;
import com.ddoctor.enums.RetError;
import com.ddoctor.user.activity.BaseActivity;
import com.ddoctor.user.adapter.HWRecordListAdapter;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.task.AddHWRecordTask;
import com.ddoctor.user.task.GetHWRecordListTask;
import com.ddoctor.user.task.TaskPostCallBack;
import com.ddoctor.user.view.DDPullToRefreshView;
import com.ddoctor.user.view.DDPullToRefreshView.OnHeaderRefreshListener;
import com.ddoctor.user.view.RulerWheel;
import com.ddoctor.user.view.RulerWheel.OnValueChangeListener;
import com.ddoctor.user.wapi.bean.HeightBean;
import com.ddoctor.user.wapi.bean.PatientBean;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.DialogUtil;
import com.ddoctor.utils.TimeUtil;
import com.ddoctor.utils.ToastUtil;
import com.umeng.analytics.MobclickAgent;
import com.zhuge.analysis.stat.ZhugeSDK;

public class HWActivity extends BaseActivity implements
		OnHeaderRefreshListener, OnScrollListener {

	private Button mleftBtn;
	private Button mbtn_confirm;

	private TextView mtv_height, mtv_weight;
	private RulerWheel mruler_height, mruler_weight;

	private ListView _listView;
	DDPullToRefreshView _refreshViewContainer;
	private View _getMoreView;
	private TextView _tv_norecord;

	private int _pageNum = 1;
	private RefreshAction _refreshAction = RefreshAction.PULLTOREFRESH;

	private HWRecordListAdapter _adapter;

	private HeightBean heightBean;

	private List<HeightBean> _dataList = new ArrayList<HeightBean>();
	private List<HeightBean> _resultList = new ArrayList<HeightBean>();

	/**
	 * 体重最大值
	 */
	private int WEIGHT_MAX = 200;
	/**
	 * 体重初始值
	 */
	private int WEIGHT_VALUE = 60;
	/**
	 * 身高最大值
	 */
	private int HEIGHT_MAX = 250;
	/**
	 * 身高初始值
	 */
	private int HEIGHT_VALUE = 160;

	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("HWActivity");
		MobclickAgent.onResume(HWActivity.this);
		ZhugeSDK.getInstance().init(getApplicationContext());
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("HWActivity");
		MobclickAgent.onPause(HWActivity.this);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_heightweight);
		findViewById();
		loadingData(true, _pageNum);
	}

	private boolean checkData() {
		heightBean = new HeightBean();
		heightBean.setId(0);
		heightBean.setHeight((int) mruler_height.getValue());
		heightBean.setWeight(mruler_weight.getValue());
		heightBean.setBmi(DataModule.getBMI((int) mruler_height.getValue(),
				mruler_weight.getValue()));
		heightBean.setTime(TimeUtil.getInstance().getStandardDate(
				getString(R.string.time_format_19)));
		return true;
	}

	/**
	 * 添加自定义事件
	 * @param eventId
	 * @param eventName
	 */
	private void addEvent(String eventId , String eventName){
		MobclickAgent.onEvent(this, eventId);
		ZhugeSDK.getInstance().onEvent(this, eventName);
	}
	
	private void addHWRecord() {
		addEvent("100011", getString(R.string.event_tz_100011));
		AddHWRecordTask task = new AddHWRecordTask(heightBean);
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {

			@Override
			public void taskFinish(DDResult result) {
				if (result.getError() == RetError.NONE) {
					ToastUtil.showToast("记录保存成功");
					PatientBean patientBean = DataModule.getInstance().getLoginedUserInfo();
					patientBean.setHeight(heightBean.getHeight());
					patientBean.setWeight(heightBean.getWeight());
					DataModule.getInstance().saveLoginedUserInfo(patientBean);
					_resultList.add(heightBean);
					Collections.sort(_resultList);
					if (_dataList.size() > 0) {
						_dataList.clear();
					}
					_dataList.addAll(formatList(_resultList));
					_adapter.notifyDataSetChanged();
				} else {
					ToastUtil.showToast(result.getErrorMessage());
				}
			}
		});
		task.executeParallel("");
	}

	private Dialog _loadingDialog = null;

	private void loadingData(boolean showLoading, int page) {
		if (showLoading) {
			_loadingDialog = DialogUtil.createLoadingDialog(this, "加载中...");
			_loadingDialog.show();
		}

		GetHWRecordListTask task = new GetHWRecordListTask(page);

		final int page1 = page;
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {

			@Override
			public void taskFinish(DDResult result) {
				if (result.getError() == RetError.NONE) {
					List<HeightBean> tmpList = result.getBundle()
							.getParcelableArrayList("list");
					if (page1 > 1) // 加载更多
					{
						_resultList.addAll(tmpList);
						Collections.sort(_resultList);
						_dataList.clear();
						_dataList.addAll(formatList(_resultList));
						_adapter.notifyDataSetChanged();
					} else {
						// 加载第一页
						_dataList.clear();
						_resultList.clear();
						_resultList.addAll(tmpList);
						Collections.sort(_resultList);
						_dataList.addAll(formatList(_resultList));
						_adapter.notifyDataSetChanged();

						_refreshViewContainer.onHeaderRefreshComplete();
						_refreshViewContainer.setVisibility(View.VISIBLE);
						_refreshAction = RefreshAction.NONE;

						if (_loadingDialog != null)
							_loadingDialog.dismiss();

					}

					// 是否显示"加载更多"
					if (tmpList.size() > 0) {
						setGetMoreContent("滑动加载更多", true, false);
						_bGetMoreEnable = true;
					} else {
						if (page1 == 1) {
							_tv_norecord.setText(result.getErrorMessage());
							_tv_norecord.setVisibility(View.VISIBLE);
							_tv_norecord.setTag(0);
						}
						// 没数据了，加载完成
						setGetMoreContent("已全部加载", false, false);
						_bGetMoreEnable = false;
					}

					// 确保加载成功后，再修改这个变量
					_pageNum = page1;
				} else {// 加载失败
					if (page1 > 1) {
						setGetMoreContent("滑动加载更多", true, false);
					} else {
						_refreshViewContainer.onHeaderRefreshComplete();
						_refreshAction = RefreshAction.NONE;

						if (_loadingDialog != null)
							_loadingDialog.dismiss();
					}

					ToastUtil.showToast(result.getErrorMessage());
				}

				_refreshAction = RefreshAction.NONE;
			}
		});

		task.executeParallel("");
	}

	protected List<HeightBean> formatList(List<HeightBean> list) {
		List<HeightBean> resultList = new ArrayList<HeightBean>();
		for (int i = 0; i < list.size(); i++) {
			HeightBean heightBean = new HeightBean();
			list.get(i).setDate(
					TimeUtil.getInstance().formatDate2(list.get(i).getTime()));
			if (i == 0
					|| (i >= 1 && !list.get(i).getDate()
							.equals(list.get(i - 1).getDate()))) {
				heightBean.setDate(list.get(i).getDate());
				heightBean.setLayoutType(RecordLayoutType.TYPE_CATEGORY);
				resultList.add(heightBean);
			}
			resultList.add(list.get(i));
		}
		return resultList;
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.btn_left: {
			finishThisActivity();
		}
			break;
		case R.id.center_btn_confirm: {
			if (checkData()) {
				addHWRecord();
			}
		}
			break;
		default:
			break;
		}
	}

	protected void findViewById() {
		mleftBtn = getLeftButtonText(getString(R.string.basic_back));
		setTitle(getString(R.string.hw_title));

		mbtn_confirm = (Button) findViewById(R.id.center_btn_confirm);

		mtv_height = (TextView) findViewById(R.id.hw_tv_height);
		mtv_weight = (TextView) findViewById(R.id.hw_tv_weight);
		mruler_height = (RulerWheel) findViewById(R.id.hw_height_ruler);
		mruler_weight = (RulerWheel) findViewById(R.id.hw_weight_ruler);

		initRuler();

		mleftBtn.setOnClickListener(this);
		mbtn_confirm.setOnClickListener(this);

		_refreshViewContainer = (DDPullToRefreshView) findViewById(R.id.refreshViewContainer);
		_refreshViewContainer.setOnHeaderRefreshListener(this);
		_refreshViewContainer.setVisibility(View.INVISIBLE);

		_listView = (ListView) findViewById(R.id.listView);
		_listView.setOnScrollListener(this);
		_tv_norecord = (TextView) findViewById(R.id.tv_norecord);
		_tv_norecord.setVisibility(View.INVISIBLE);
		_listView.setEmptyView(_tv_norecord);
		initList();

	}

	private void initRuler() {
		mruler_weight.setChange(WEIGHT_VALUE, WEIGHT_MAX, 10, 10,
				getResources().getColor(R.color.color_text_gray_light));
		mruler_height.setChange(HEIGHT_VALUE, HEIGHT_MAX, 10, 10,
				getResources().getColor(R.color.color_text_gray_light));
		mtv_height.setText(String.format("%d", HEIGHT_VALUE));
		mtv_weight.setText(String.format("%d", WEIGHT_VALUE));
		mruler_height.setValueChangeListener(new OnValueChangeListener() {

			@Override
			public void onValueChange(float value) {
				mtv_height.setText(String.format("%d", (int)value));
			}
		});
		mruler_weight.setValueChangeListener(new OnValueChangeListener() {

			@Override
			public void onValueChange(float value) {
				mtv_weight.setText(String.format("%d", (int)value));
			}
		});
	}

	private void initList() {
		// 获取更多
		_getMoreView = createGetMoreView();
		setGetMoreContent("已全部加载", false, false);
		_listView.addFooterView(_getMoreView);

		// 数据
		_adapter = new HWRecordListAdapter(this);
		_listView.setAdapter(_adapter);
		_adapter.setData(_dataList);  
	}

	// 加载更多相关函数 >>>>>>
	boolean _bGetMoreEnable = false;

	private View createGetMoreView() {
		if (_getMoreView != null)
			return _getMoreView;

		View v = (View) getLayoutInflater().inflate(R.layout.refresh_footer,
				null);

		return v;
	}

	private void setGetMoreContent(String message, boolean showImage,
			boolean animation) {
		TextView tv = (TextView) _getMoreView
				.findViewById(R.id.pull_to_load_text);
		tv.setText(message);

		ImageView imgView = (ImageView) _getMoreView
				.findViewById(R.id.pull_to_load_image);
		AnimationDrawable ad = (AnimationDrawable) imgView.getBackground();
		if (showImage) {
			if (animation) {
				ad.start();
			} else {
				ad.stop();
				ad.selectDrawable(0);
			}

			imgView.setVisibility(View.VISIBLE);
		} else {
			ad.stop();
			ad.selectDrawable(0);

			imgView.setVisibility(View.GONE);
		}
	}

	@Override
	public void onScroll(AbsListView arg0, int firstVisibleItem, int arg2,
			int arg3) {

		// 工具栏的显示与隐藏
		if (_refreshAction == RefreshAction.NONE) {
			if (_bGetMoreEnable) {// 有加载更多
				int lastPos = _listView.getLastVisiblePosition();
				int total = _listView.getHeaderViewsCount() + _dataList.size()
						+ _listView.getFooterViewsCount();
				if (lastPos == total - 1) {
					// 加载更多显示出来了，开始加载
					_refreshAction = RefreshAction.LOADMORE;
					setGetMoreContent("正在加载...", true, true);
					loadingData(false, _pageNum + 1);
				}
			}
		}

	}

	@Override
	public void onScrollStateChanged(AbsListView arg0, int arg1) {

	}

	@Override
	public void onHeaderRefresh(DDPullToRefreshView view) {
		if (_refreshAction == RefreshAction.NONE) {
			_refreshAction = RefreshAction.PULLTOREFRESH;
			loadingData(false, 1);
		} else {
			// 正在加载，什么也不做
			view.onHeaderRefreshComplete();
		}
	}

}
