package com.ddoctor.user.activity.sugar;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.app.Dialog;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ViewFlipper;

import com.beichen.user.R;
import com.ddoctor.enums.RetError;
import com.ddoctor.user.activity.BaseActivity;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.task.BloodSugarTask;
import com.ddoctor.user.task.TaskPostCallBack;
import com.ddoctor.user.view.LineChart;
import com.ddoctor.user.view.LineChart2;
import com.ddoctor.user.wapi.bean.SugarValueBean;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.DialogUtil;
import com.ddoctor.utils.MyUtils;
import com.ddoctor.utils.TimeUtil;
import com.ddoctor.utils.ToastUtil;
import com.umeng.analytics.MobclickAgent;
import com.zhuge.analysis.stat.ZhugeSDK;

public class SugarLineChartActivity extends BaseActivity implements
		OnCheckedChangeListener, android.widget.RadioGroup.OnCheckedChangeListener {

	private Button leftBtn;
	private LinearLayout _rightLayout;
	private ViewFlipper flipper;
	private FrameLayout layout1;
	private FrameLayout layout2;

	LineChart line;
	LineChart2 chart;
	private CheckBox mcb_lc, mcb_kf, mcb_zh, mcb_wuq, mcb_wuh, mcb_wanq,
			mcb_wanh, mcb_sq;

	private String today, startDate, endDate;
	private String MODULE;

	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("SugarLineChartActivity");
		MobclickAgent.onResume(SugarLineChartActivity.this);
		ZhugeSDK.getInstance().init(getApplicationContext());
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("SugarLineChartActivity");
		MobclickAgent.onPause(SugarLineChartActivity.this);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_sugarchart);
		findViewById();
		high = DataModule.getInstance().getSugarUpBound();
		low = DataModule.getInstance().getSugarDownBound();
		loadingData(startDate, endDate, true);
	}

	private float high;
	private float low;

	private List<SugarValueBean> dataList = new ArrayList<SugarValueBean>();
	private RadioGroup _rg;

	private Dialog _loadingDialog;
	private void loadingData(final String startDate, final String endDate,
			boolean showDialog) {
		if (showDialog) {
			_loadingDialog = DialogUtil.createLoadingDialog(SugarLineChartActivity.this);
			_loadingDialog.show();
		}
		
		BloodSugarTask task = new BloodSugarTask(startDate, endDate, 1, 0);
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {
			@Override
			public void taskFinish(DDResult result) {
				_loadingDialog.dismiss();
				if (result.getError() == RetError.NONE) {
					dataList = result.getBundle()
							.getParcelableArrayList("list");
					if (dataList != null && dataList.size() > 0) {
						MyUtils.showLog("isloadingToday " + _loadingToday + " "
								+ startDate + " " + endDate);
						if (_loadingToday) {
							MyUtils.showLog("SugarLine 加载今日曲线图 ");
							line.setData(dataList, high, low);
							line.invalidate();
						} else {
							MyUtils.showLog(" SugarLine 加载多组曲线图 ");
							chart.setData(dataList, high, low);
							chart.invalidate();
						}
					} else {
						ToastUtil.showToast(result.getErrorMessage());
					}
				} else {
					ToastUtil.showToast(result.getErrorMessage());
				}
			}
		});
		task.executeParallel("");
	}

	protected void findViewById() {
		MODULE = getString(R.string.time_format_10);
		today = TimeUtil.getInstance().getStandardDate(MODULE);
		startDate = today;
		endDate = TimeUtil.getInstance().dateAddFrom(1, today, MODULE,
				Calendar.DAY_OF_MONTH);

		leftBtn = getLeftButtonText(getString(R.string.basic_back));
		_rightLayout = (LinearLayout) findViewById(R.id.titlebar_right_layout);
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, 1);
		params.setMargins(3, 3, 3, 3);
		_rg = new RadioGroup(SugarLineChartActivity.this);
		RadioGroup.LayoutParams rgParams = new RadioGroup.LayoutParams(android.widget.RadioGroup.LayoutParams.WRAP_CONTENT,android.widget.RadioGroup.LayoutParams.WRAP_CONTENT, 1);
		rgParams.setMargins(2, 2, 2, 2);
		_rg.setGravity(Gravity.CENTER_VERTICAL);
		_rg.setOrientation(RadioGroup.HORIZONTAL);
		_rg.addView(generateRadioBtn(R.drawable.sugar_check_totay_bg, R.id.sugar_today),rgParams);
		_rg.addView(generateRadioBtn(R.drawable.sugar_check_week_bg, R.id.sugar_week),rgParams);
		_rg.addView(generateRadioBtn(R.drawable.sugar_check_month_bg, R.id.sugar_month),rgParams);
		((RadioButton)_rg.getChildAt(0)).setChecked(true);
		_rg.setOnCheckedChangeListener(this);
		_rightLayout.addView(_rg, params);
		_rightLayout.setVisibility(View.VISIBLE);
		
		
		findViewById(R.id.titleBar).setBackgroundColor(
				getResources().getColor(R.color.color_report_titlebar));
		setTitle(getResources().getString(R.string.sugar_chart));

		flipper = (ViewFlipper) findViewById(R.id.sugarchart_layout);
		layout1 = (FrameLayout) findViewById(R.id.sugarchart_line);
		// layout_chart = (FrameLayout) findViewById(R.id.sugarchart_chart);
		layout2 = (FrameLayout) findViewById(R.id.sugarchart_chart_layout);
		mcb_lc = (CheckBox) findViewById(R.id.sugarchart_cb_lc);
		mcb_kf = (CheckBox) findViewById(R.id.sugarchart_cb_kf);
		mcb_zh = (CheckBox) findViewById(R.id.sugarchart_cb_zh);
		mcb_wuq = (CheckBox) findViewById(R.id.sugarchart_cb_wuq);
		mcb_wuh = (CheckBox) findViewById(R.id.sugarchart_cb_wuh);
		mcb_wanq = (CheckBox) findViewById(R.id.sugarchart_cb_wanq);
		mcb_wanh = (CheckBox) findViewById(R.id.sugarchart_cb_wanh);
		mcb_sq = (CheckBox) findViewById(R.id.sugarchart_cb_sq);

		line = new LineChart(this);
		chart = new LineChart2(this);
		layout1.addView(line);
		layout2.addView(chart);
		leftBtn.setOnClickListener(this);

		mcb_lc.setOnCheckedChangeListener(this);
		mcb_kf.setOnCheckedChangeListener(this);
		mcb_zh.setOnCheckedChangeListener(this);
		mcb_wuq.setOnCheckedChangeListener(this);
		mcb_wuh.setOnCheckedChangeListener(this);
		mcb_wanq.setOnCheckedChangeListener(this);
		mcb_wanh.setOnCheckedChangeListener(this);
		mcb_sq.setOnCheckedChangeListener(this);
	}

	private RadioButton generateRadioBtn (int resId , int id){
		RadioButton rb = new RadioButton(SugarLineChartActivity.this);
		rb.setBackgroundResource(0);
		rb.setId(id);
		rb.setButtonDrawable(resId);
		return rb;
	}

	private boolean _loadingToday = true;

	private int DateRouting = 0;

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.btn_left: {
			finishThisActivity();
		}
			break;

		default:
			break;
		}
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		switch (buttonView.getId()) {
		case R.id.sugarchart_cb_lc: {
			chart.setDrawLCLine(isChecked);
		}
			break;
		case R.id.sugarchart_cb_kf: {
			chart.setDrawKFLine(isChecked);
		}
			break;
		case R.id.sugarchart_cb_zh: {
			chart.setDrawZHLine(isChecked);
		}
			break;
		case R.id.sugarchart_cb_wuq: {
			chart.setDrawWUQLine(isChecked);
		}
			break;
		case R.id.sugarchart_cb_wuh: {
			chart.setDrawWUHLine(isChecked);
		}
			break;
		case R.id.sugarchart_cb_wanq: {
			chart.setDrawWANQLine(isChecked);
		}
			break;
		case R.id.sugarchart_cb_wanh: {
			chart.setDrawWANHLine(isChecked);
		}
			break;
		case R.id.sugarchart_cb_sq: {
			chart.setDrawSQLine(isChecked);
		}
			break;

		default:
			break;
		}
	}

	private int _currentId = R.id.sugar_today;
	
	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		if (_currentId == checkedId) {
			return;
		}
		switch (checkedId) {
		case R.id.sugar_today: {
			_currentId = R.id.sugar_today;
			((RadioButton)_rg.getChildAt(0)).setChecked(true);
			DateRouting = 0;
			flipper.showPrevious();
			startDate = today;
			MyUtils.showLog("切换查看今天数据   开始日期  "+startDate+" 结束日期  "+endDate);
			loadingData(startDate, endDate, true);
			_loadingToday = true;
		}
			break;
		case R.id.sugar_week: {
			if (DateRouting == 0) {
				flipper.showNext();
			}
			_currentId = R.id.sugar_week;
			((RadioButton)_rg.getChildAt(1)).setChecked(true);
			DateRouting = 7;
			startDate = TimeUtil.getInstance().dateAdd(0 - DateRouting,
					endDate, MODULE, Calendar.DAY_OF_MONTH);
			MyUtils.showLog("切换查看周数据   开始日期  "+startDate+" 结束日期   "+endDate);
			loadingData(startDate, endDate, true);
			_loadingToday = false;
		}
			break;
		case R.id.sugar_month: {
			if (DateRouting == 0) {
				flipper.showNext();
			}
			_currentId = R.id.sugar_month;
			((RadioButton)_rg.getChildAt(2)).setChecked(true);
			DateRouting = 30;
			startDate = TimeUtil.getInstance().dateAdd(0 - DateRouting,
					endDate, MODULE, Calendar.DAY_OF_MONTH);
			MyUtils.showLog("切换查看月份数据  开始日期 "+startDate+" 结束日期  "+endDate);
			loadingData(startDate, endDate, true);
			_loadingToday = false;
		}
			break;

		default:
			break;
		}
	}

}
