package com.ddoctor.user.activity.sugar;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.app.Dialog;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.enums.RecordLayoutType;
import com.ddoctor.enums.RefreshAction;
import com.ddoctor.enums.RetError;
import com.ddoctor.interfaces.OnClickCallBackListener;
import com.ddoctor.user.activity.BaseActivity;
import com.ddoctor.user.adapter.DiscomfirtListAdapter;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.data.MyProfile;
import com.ddoctor.user.task.AddDiscomfirtRecordTask;
import com.ddoctor.user.task.GetDiscomfirtListTask;
import com.ddoctor.user.task.TaskPostCallBack;
import com.ddoctor.user.view.DDPullToRefreshView;
import com.ddoctor.user.view.DDPullToRefreshView.OnHeaderRefreshListener;
import com.ddoctor.user.wapi.bean.TroubleBean;
import com.ddoctor.user.wapi.bean.TroubleitemBean;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.DialogUtil;
import com.ddoctor.utils.MyUtils;
import com.ddoctor.utils.TimeUtil;
import com.ddoctor.utils.ToastUtil;
import com.umeng.analytics.MobclickAgent;
import com.zhuge.analysis.stat.ZhugeSDK;

public class DiscomfirtActivity extends BaseActivity implements OnClickCallBackListener, OnHeaderRefreshListener, OnScrollListener {

	private ListView _listView;
	DDPullToRefreshView _refreshViewContainer;
	private View _getMoreView;
	private TextView _tv_norecord;

	private int _pageNum = 1;
	private RefreshAction _refreshAction = RefreshAction.PULLTOREFRESH;
	
	private List<TroubleBean> _dataList = new ArrayList<TroubleBean>();
	private List<TroubleBean> _resultList = new ArrayList<TroubleBean>();

	private DiscomfirtListAdapter _adapter;

	private TroubleBean _troubleBean;

	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("DiscomfirtActivity");
		MobclickAgent.onResume(DiscomfirtActivity.this);
		ZhugeSDK.getInstance().init(getApplicationContext());
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("DiscomfirtActivity");
		MobclickAgent.onPause(DiscomfirtActivity.this);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_discomfort);
		findViewById();
		getDiscomfirtRecord(true , _pageNum);
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.btn_left: {
			finishThisActivity();
		}
			break;
		case R.id.btn_right: {
			DialogUtil.showDiscomfirtMultiDialog(DiscomfirtActivity.this,
					DataModule.loadDict(MyProfile.DICT_TROUBLEITEMS,
							TroubleitemBean.class), null, this);
		}
			break;
		default:
			break;
		}
	}

	/**
	 * 添加自定义事件
	 * @param eventId
	 * @param eventName
	 */
	private void addEvent(String eventId , String eventName){
		MobclickAgent.onEvent(this, eventId);
		ZhugeSDK.getInstance().onEvent(this, eventName);
	}
	
	private void addDiscomfirtRecord() {
		addEvent("100012", getString(R.string.event_bs_100012));
		AddDiscomfirtRecordTask task = new AddDiscomfirtRecordTask(_troubleBean);
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {

			@Override
			public void taskFinish(DDResult result) {
				if (result.getError() == RetError.NONE) {
					ToastUtil.showToast("记录保存成功");
					_resultList.add(_troubleBean);
					if (_dataList.size() > 0) {
						_dataList.clear();
					}
					Collections.sort(_resultList);
					_dataList.addAll(formatList(_resultList));
					_adapter.notifyDataSetChanged();
				} else {
					ToastUtil.showToast(result.getErrorMessage());
				}
			}
		});
		task.executeParallel("");
	}

	private Dialog _loadingDialog = null;

	private void getDiscomfirtRecord(boolean showDialog , int page) {
		if (_loadingDialog == null) {
			_loadingDialog = DialogUtil.createLoadingDialog(DiscomfirtActivity.this,
					getString(R.string.basic_loading));
		}
		if (showDialog) {
			_loadingDialog.show();
		}
		GetDiscomfirtListTask task = new GetDiscomfirtListTask(page);
		final int page1 = page;
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {

			@Override
			public void taskFinish(DDResult result) {
				if (result.getError() == RetError.NONE) {
					List<TroubleBean> tmpList = result.getBundle()
							.getParcelableArrayList("list");
					if (page1 > 1) // 加载更多
					{
						_resultList.addAll(tmpList);
						Collections.sort(_resultList);
						_dataList.clear();
						_dataList.addAll(formatList(_resultList));
						_adapter.notifyDataSetChanged();
					} else {
						// 加载第一页
						_dataList.clear();
						_resultList.clear();
						_resultList.addAll(tmpList);
						Collections.sort(_resultList);
						_dataList.addAll(formatList(_resultList));
						_adapter.notifyDataSetChanged();

						_refreshViewContainer.onHeaderRefreshComplete();
						_refreshViewContainer.setVisibility(View.VISIBLE);
						_refreshAction = RefreshAction.NONE;

						if (_loadingDialog != null)
							_loadingDialog.dismiss();

					}

					// 是否显示"加载更多"
					if (tmpList.size() > 0) {
						setGetMoreContent("滑动加载更多", true, false);
						_bGetMoreEnable = true;
					} else {
						if (page1 == 1) {
							_tv_norecord.setText(result.getErrorMessage());
							_tv_norecord.setVisibility(View.VISIBLE);
							_tv_norecord.setTag(0);
						}
						// 没数据了，加载完成
						setGetMoreContent("已全部加载", false, false);
						_bGetMoreEnable = false;
					}

					// 确保加载成功后，再修改这个变量
					_pageNum = page1;
				} else {// 加载失败
					if (page1 > 1) {
						setGetMoreContent("滑动加载更多", true, false);
					} else {
						_refreshViewContainer.onHeaderRefreshComplete();
						_refreshAction = RefreshAction.NONE;

						if (_loadingDialog != null)
							_loadingDialog.dismiss();
					}

					ToastUtil.showToast(result.getErrorMessage());
				}

				_refreshAction = RefreshAction.NONE;
			}
		});

		task.executeParallel("");

	}

	protected List<TroubleBean> formatList(List<TroubleBean> list) {
		List<TroubleBean> resultList = new ArrayList<TroubleBean>();
		for (int i = 0; i < list.size(); i++) {
			TroubleBean troubleBean = new TroubleBean();
			list.get(i).setDate(
					TimeUtil.getInstance().formatDate2(list.get(i).getTime()));
			if (i == 0
					|| (i >= 1 && !list.get(i).getDate()
							.equals(list.get(i - 1).getDate()))) {
				troubleBean.setDate(list.get(i).getDate());
				troubleBean.setLayoutType(RecordLayoutType.TYPE_CATEGORY);
				resultList.add(troubleBean);
			}
			resultList.add(list.get(i));
		}
		return resultList;
	}


	protected void findViewById() {
		Button mleftBtn = getLeftButtonText(getString(R.string.basic_back));
		Button mrightBtn = getRightButtonText("录入");		
		setTitle("不适");
		mleftBtn.setOnClickListener(this);
		mrightBtn.setOnClickListener(this);
		
		_refreshViewContainer = (DDPullToRefreshView) findViewById(R.id.refreshViewContainer);
		_refreshViewContainer.setOnHeaderRefreshListener(this);
		_refreshViewContainer.setVisibility(View.INVISIBLE);

		_listView = (ListView) findViewById(R.id.listView);
		_listView.setOnScrollListener(this);
		_tv_norecord = (TextView) findViewById(R.id.tv_norecord);
		_tv_norecord.setVisibility(View.INVISIBLE);
		_listView.setEmptyView(_tv_norecord);

		initList();
		
	}

	private void initList() {
		// 获取更多
		_getMoreView = createGetMoreView();
		setGetMoreContent("已全部加载", false, false);
		_listView.addFooterView(_getMoreView);

		// 数据
		_adapter = new DiscomfirtListAdapter(this);
		_listView.setAdapter(_adapter);
		_adapter.setData(_dataList);
	}

	// 加载更多相关函数 >>>>>>
	boolean _bGetMoreEnable = false;

	private View createGetMoreView() {
		if (_getMoreView != null)
			return _getMoreView;

		View v = (View) getLayoutInflater().inflate(R.layout.refresh_footer,
				null);

		return v;
	}

	private void setGetMoreContent(String message, boolean showImage,
			boolean animation) {
		TextView tv = (TextView) _getMoreView
				.findViewById(R.id.pull_to_load_text);
		tv.setText(message);

		ImageView imgView = (ImageView) _getMoreView
				.findViewById(R.id.pull_to_load_image);
		AnimationDrawable ad = (AnimationDrawable) imgView.getBackground();
		if (showImage) {
			if (animation) {
				ad.start();
			} else {
				ad.stop();
				ad.selectDrawable(0);
			}

			imgView.setVisibility(View.VISIBLE);
		} else {
			ad.stop();
			ad.selectDrawable(0);

			imgView.setVisibility(View.GONE);
		}
	}

	@Override
	public void onScroll(AbsListView arg0, int firstVisibleItem, int arg2,
			int arg3) {

		// 工具栏的显示与隐藏
		if (_refreshAction == RefreshAction.NONE) {
			if (_bGetMoreEnable) {// 有加载更多
				int lastPos = _listView.getLastVisiblePosition();
				int total = _listView.getHeaderViewsCount() + _dataList.size()
						+ _listView.getFooterViewsCount();
				if (lastPos == total - 1) {
					// 加载更多显示出来了，开始加载
					_refreshAction = RefreshAction.LOADMORE;
					setGetMoreContent("正在加载...", true, true);
					getDiscomfirtRecord(false, _pageNum + 1);
				}
			}
		}

	}

	@Override
	public void onScrollStateChanged(AbsListView arg0, int arg1) {

	}

	@Override
	public void onHeaderRefresh(DDPullToRefreshView view) {
		if (_refreshAction == RefreshAction.NONE) {
			_refreshAction = RefreshAction.PULLTOREFRESH;
			getDiscomfirtRecord(false, 1);
		} else {
			// 正在加载，什么也不做
			view.onHeaderRefreshComplete();
		}
	}
	
	@Override
	public void onClickCallBack(Bundle data) {
		MyUtils.showLog("", "不适项选择结果  " + data);
		_troubleBean = new TroubleBean();
		_troubleBean.setId(0);
		_troubleBean.setItem(data.getString("data"));
		_troubleBean.setTime(TimeUtil.getInstance().getStandardDate(
				getString(R.string.time_format_19)));
		_troubleBean.setRemark("");
		addDiscomfirtRecord();
	}

}