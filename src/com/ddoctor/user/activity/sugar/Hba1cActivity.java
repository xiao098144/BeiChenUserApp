package com.ddoctor.user.activity.sugar;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.app.Dialog;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.enums.RecordLayoutType;
import com.ddoctor.enums.RefreshAction;
import com.ddoctor.enums.RetError;
import com.ddoctor.user.activity.BaseActivity;
import com.ddoctor.user.adapter.Hba1cListAdapter;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.task.AddHba1cRecordTask;
import com.ddoctor.user.task.GetHba1cListTask;
import com.ddoctor.user.task.TaskPostCallBack;
import com.ddoctor.user.view.DDPullToRefreshView;
import com.ddoctor.user.view.DDPullToRefreshView.OnHeaderRefreshListener;
import com.ddoctor.user.wapi.bean.PatientBean;
import com.ddoctor.user.wapi.bean.ProteinBean;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.DialogUtil;
import com.ddoctor.utils.StringUtils;
import com.ddoctor.utils.TimeUtil;
import com.ddoctor.utils.ToastUtil;
import com.umeng.analytics.MobclickAgent;
import com.zhuge.analysis.stat.ZhugeSDK;

public class Hba1cActivity extends BaseActivity implements
		OnHeaderRefreshListener, OnScrollListener {

	private Button mleftBtn;
	private Button mbtn_confirm;

	private EditText met_value;

	private ListView _listView;
	DDPullToRefreshView _refreshViewContainer;
	private View _getMoreView;
	private TextView _tv_norecord;

	private int _pageNum = 1;
	private RefreshAction _refreshAction = RefreshAction.PULLTOREFRESH;

	private List<ProteinBean> _dataList = new ArrayList<ProteinBean>(); // 传递到adapter
	private List<ProteinBean> _resultList = new ArrayList<ProteinBean>(); // 服务器反馈总数据集

	private Hba1cListAdapter _adapter;

	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("Hba1cActivity");
		MobclickAgent.onResume(Hba1cActivity.this);
		ZhugeSDK.getInstance().init(getApplicationContext());
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("Hba1cActivity");
		MobclickAgent.onPause(Hba1cActivity.this);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_hba1c);
		findViewById();
		getHba1cRecord(true, _pageNum);
	}

	/**
	 * 添加自定义事件
	 * @param eventId
	 * @param eventName
	 */
	private void addEvent(String eventId , String eventName){
		MobclickAgent.onEvent(this, eventId);
		ZhugeSDK.getInstance().onEvent(this, eventName);
	}
	
	private void addHba1cRecord() {
		addEvent("100006", getString(R.string.event_hba1c_100006));
		AddHba1cRecordTask task = new AddHba1cRecordTask(proteinBean);
		task.setTaskCallBack(new TaskPostCallBack<RetError>() {

			@Override
			public void taskFinish(RetError result) {
				if (result == RetError.NONE) {
					met_value.setText("");
					ToastUtil.showToast("记录保存成功");
					PatientBean patientBean = DataModule.getInstance().getLoginedUserInfo();
					patientBean.setProtein(proteinBean.getValue());
					DataModule.getInstance().saveLoginedUserInfo(patientBean);
					_resultList.add(proteinBean);
					Collections.sort(_resultList);
					if (_dataList.size() > 0) {
						_dataList.clear();
					}
					_dataList.addAll(formatList(_resultList));
					_adapter.notifyDataSetChanged();
				} else {
					ToastUtil.showToast(result.getErrorMessage());
				}
			}
		});
		task.executeParallel("");

	}

	private Dialog _loadingDialog;

	private void getHba1cRecord(boolean showDialog, int page) {
		if (_loadingDialog == null) {
			_loadingDialog = DialogUtil.createLoadingDialog(Hba1cActivity.this,
					getString(R.string.basic_loading));
		}
		if (showDialog) {
			_loadingDialog.show();
		}
		GetHba1cListTask task = new GetHba1cListTask(page);
		final int page1 = page;
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {

			@Override
			public void taskFinish(DDResult result) {
				if (result.getError() == RetError.NONE) {
					List<ProteinBean> tmpList = result.getBundle()
							.getParcelableArrayList("list");
					if (page1 > 1) // 加载更多
					{
						_resultList.addAll(tmpList);
						Collections.sort(_resultList);
						_dataList.clear();
						_dataList.addAll(formatList(_resultList));
						_adapter.notifyDataSetChanged();
					} else {
						// 加载第一页
						_dataList.clear();
						_resultList.clear();
						_resultList.addAll(tmpList);
						Collections.sort(_resultList);
						_dataList.addAll(_resultList);
						_adapter.notifyDataSetChanged();

						_refreshViewContainer.onHeaderRefreshComplete();
						_refreshViewContainer.setVisibility(View.VISIBLE);
						_refreshAction = RefreshAction.NONE;

						if (_loadingDialog != null)
							_loadingDialog.dismiss();

					}

					// 是否显示"加载更多"
					if (tmpList.size() > 0) {
						setGetMoreContent("滑动加载更多", true, false);
						_bGetMoreEnable = true;
					} else {
						if (page1 == 1) {
							_tv_norecord.setText(result.getErrorMessage());
							_tv_norecord.setVisibility(View.VISIBLE);
							_tv_norecord.setTag(0);
						}
						// 没数据了，加载完成
						setGetMoreContent("已全部加载", false, false);
						_bGetMoreEnable = false;
					}

					// 确保加载成功后，再修改这个变量
					_pageNum = page1;
				} else {// 加载失败
					if (page1 > 1) {
						setGetMoreContent("滑动加载更多", true, false);
					} else {
						_refreshViewContainer.onHeaderRefreshComplete();
						_refreshAction = RefreshAction.NONE;

						if (_loadingDialog != null)
							_loadingDialog.dismiss();
					}

					ToastUtil.showToast(result.getErrorMessage());
				}

				_refreshAction = RefreshAction.NONE;
			}
		});
		task.executeParallel("");
	}

	protected List<ProteinBean> formatList(List<ProteinBean> list) {
		List<ProteinBean> resultList = new ArrayList<ProteinBean>();
		for (int i = 0; i < list.size(); i++) {
			ProteinBean proteinBean = new ProteinBean();
			list.get(i).setDate(
					TimeUtil.getInstance().formatDate2(list.get(i).getTime()));
			if (i == 0
					|| (i >= 1 && !list.get(i).getDate()
							.equals(list.get(i - 1).getDate()))) {
				proteinBean.setDate(list.get(i).getDate());
				proteinBean.setLayoutType(RecordLayoutType.TYPE_CATEGORY);
				resultList.add(proteinBean);
			}
			resultList.add(list.get(i));
		}
		return resultList;
	}

	private ProteinBean proteinBean;

	private boolean checkdata() {
		String valueStr = met_value.getText().toString().trim();
		if (TextUtils.isEmpty(valueStr)) {
			ToastUtil.showToast("请填写记录值");
			return false;
		}
		if (!StringUtils.pureNum(valueStr)) {
			ToastUtil.showToast("请输入正常的记录值");
			return false;
		}
		   
		float value = Float.valueOf(valueStr);
		if (value <= 0 || value >100) {
			ToastUtil.showToast("请输入正常范围的糖化血红蛋白值");
			return false;
		}
		proteinBean = new ProteinBean();
		proteinBean.setId(0);
		proteinBean.setRemark("");
		proteinBean.setTime(TimeUtil.getInstance().getStandardDate(
				getString(R.string.time_format_19)));
		proteinBean.setValue(value);
		return true;
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.btn_left: {
			finishThisActivity();
		}
			break;
		case R.id.center_btn_confirm: {
			if (checkdata()) {
				addHba1cRecord();
			}
		}
			break;
		default:
			break;
		}
	}

	protected void findViewById() {
		mleftBtn = getLeftButtonText(getString(R.string.basic_back));
		setTitle(getString(R.string.hba1c_title));

		mbtn_confirm = (Button) findViewById(R.id.center_btn_confirm);

		met_value = (EditText) findViewById(R.id.hba1c_et);
		met_value.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});
		
		
		mleftBtn.setOnClickListener(this);
		mbtn_confirm.setOnClickListener(this);

		_refreshViewContainer = (DDPullToRefreshView) findViewById(R.id.refreshViewContainer);
		_refreshViewContainer.setOnHeaderRefreshListener(this);
		_refreshViewContainer.setVisibility(View.INVISIBLE);

		_listView = (ListView) findViewById(R.id.listView);
		_listView.setOnScrollListener(this);
		_tv_norecord = (TextView) findViewById(R.id.tv_norecord);
		_tv_norecord.setVisibility(View.INVISIBLE);
		_listView.setEmptyView(_tv_norecord);
		initList();

	}

	private void initList() {
		// 获取更多
		_getMoreView = createGetMoreView();
		setGetMoreContent("已全部加载", false, false);
		_listView.addFooterView(_getMoreView);

		// 数据
		_adapter = new Hba1cListAdapter(this);
		_listView.setAdapter(_adapter);
		_adapter.setData(_dataList);
	}

	// 加载更多相关函数 >>>>>>
	boolean _bGetMoreEnable = false;

	private View createGetMoreView() {
		if (_getMoreView != null)
			return _getMoreView;

		View v = (View) getLayoutInflater().inflate(R.layout.refresh_footer,
				null);

		return v;
	}

	private void setGetMoreContent(String message, boolean showImage,
			boolean animation) {
		TextView tv = (TextView) _getMoreView
				.findViewById(R.id.pull_to_load_text);
		tv.setText(message);

		ImageView imgView = (ImageView) _getMoreView
				.findViewById(R.id.pull_to_load_image);
		AnimationDrawable ad = (AnimationDrawable) imgView.getBackground();
		if (showImage) {
			if (animation) {
				ad.start();
			} else {
				ad.stop();
				ad.selectDrawable(0);
			}

			imgView.setVisibility(View.VISIBLE);
		} else {
			ad.stop();
			ad.selectDrawable(0);

			imgView.setVisibility(View.GONE);
		}
	}

	@Override
	public void onScroll(AbsListView arg0, int firstVisibleItem, int arg2,
			int arg3) {

		// 工具栏的显示与隐藏
		if (_refreshAction == RefreshAction.NONE) {
			if (_bGetMoreEnable) {// 有加载更多
				int lastPos = _listView.getLastVisiblePosition();
				int total = _listView.getHeaderViewsCount() + _dataList.size()
						+ _listView.getFooterViewsCount();
				if (lastPos == total - 1) {
					// 加载更多显示出来了，开始加载
					_refreshAction = RefreshAction.LOADMORE;
					setGetMoreContent("正在加载...", true, true);
					getHba1cRecord(false, _pageNum + 1);
				}
			}
		}

	}

	@Override
	public void onScrollStateChanged(AbsListView arg0, int arg1) {

	}

	@Override
	public void onHeaderRefresh(DDPullToRefreshView view) {
		if (_refreshAction == RefreshAction.NONE) {
			_refreshAction = RefreshAction.PULLTOREFRESH;
			getHba1cRecord(false, 1);
		} else {
			// 正在加载，什么也不做
			view.onHeaderRefreshComplete();
		}
	}

}
