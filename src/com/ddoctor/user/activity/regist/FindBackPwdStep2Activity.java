package com.ddoctor.user.activity.regist;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.enums.RetError;
import com.ddoctor.user.activity.BaseActivity;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.task.TaskPostCallBack;
import com.ddoctor.user.task.UpdatePwdTask;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.DialogUtil;
import com.ddoctor.utils.ToastUtil;
import com.umeng.analytics.MobclickAgent;
import com.zhuge.analysis.stat.ZhugeSDK;

public class FindBackPwdStep2Activity extends BaseActivity {

	private EditText _edittext_pwd;
	private EditText _edittext_pwd2;

	private Dialog _dialog = null;

	private String _mobile;
	private String _password;
	private String _verify;

	private void getIntentInfo() {
		Bundle data = getIntent().getBundleExtra("data");
		_mobile = data.getString("mobile");
		_verify = data.getString("verify");
	}

	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("FindBackPwdStep2Activity");
		MobclickAgent.onResume(FindBackPwdStep2Activity.this);
		ZhugeSDK.getInstance().init(getApplicationContext());
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("FindBackPwdStep2Activity");
		MobclickAgent.onPause(FindBackPwdStep2Activity.this);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_setnewpwd);
		getIntentInfo();
		findViewById();
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.btn_left: {
			finishThisActivity();
		}
			break;
		case R.id.regist_next: {
			if (on_btn_next()) {
				on_btn_updatepwd();
			}
		}
			break;
		default:
			break;
		}
	}

	// 下一步按钮
	private boolean on_btn_next() {
		String pwd1 = _edittext_pwd.getText().toString();
		if (pwd1.length() < 1) {
			ToastUtil.showToast("请输入您的新密码!");
			return false;
		}

		// 判断验证码是否正确
		String pwd2 = _edittext_pwd2.getText().toString();
		if (pwd2.length() < 1) {
			ToastUtil.showToast("请再次输入您的新密码");
			return false;
		}

		if (!pwd1.equals(pwd2)) {
			ToastUtil.showToast("两次输入的密码不正确，请重新输入");
			return false;
		}
		_password = pwd2;
		return true;

	}

	// 点击发送验证码按钮
	private void on_btn_updatepwd() {

		_dialog = DialogUtil.createLoadingDialog(this, "正在发送...");
		_dialog.show();

		UpdatePwdTask task = new UpdatePwdTask(_mobile, _password, _verify);
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {

			@Override
			public void taskFinish(DDResult result) {
				if (result.getError() == RetError.NONE) {
					ToastUtil.showToast("密码修改成功");
					Activity firstActivity = (Activity) DataModule
							.getInstance().registerInfoMap
							.get("newpwd_activity");
					if (firstActivity != null)
						firstActivity.finish();
					finishThisActivity();
				} else {
					ToastUtil.showToast(result.getErrorMessage());
				}
			}
		});
		task.executeParallel("");

	}

	public void findViewById() {
		Button leftBtn = getLeftButton();
		leftBtn.setText(getString(R.string.basic_back));
		leftBtn.setOnClickListener(this);
		findViewById(R.id.titlebar).setBackgroundColor(Color.TRANSPARENT);

		Button regist_next = (Button) findViewById(R.id.regist_next);
		regist_next.setOnClickListener(this);

		_edittext_pwd = (EditText) findViewById(R.id.edittext_password);
		_edittext_pwd2 = (EditText) findViewById(R.id.edittext_password2);

		TextView tv_mobile = (TextView) findViewById(R.id.newpwd_tv_mobile);
		tv_mobile.setText(_mobile);

	}

}
