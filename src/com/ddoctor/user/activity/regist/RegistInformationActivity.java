package com.ddoctor.user.activity.regist;

import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.beichen.user.R;
import com.ddoctor.enums.RetError;
import com.ddoctor.user.activity.BaseActivity;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.task.RegisterTask;
import com.ddoctor.user.task.TaskPostCallBack;
import com.ddoctor.user.view.RulerWheel;
import com.ddoctor.user.view.RulerWheel.OnValueChangeListener;
import com.ddoctor.user.wapi.bean.PatientBean;
import com.ddoctor.utils.DialogUtil;
import com.ddoctor.utils.ToastUtil;
import com.umeng.analytics.MobclickAgent;
import com.zhuge.analysis.stat.ZhugeSDK;

public class RegistInformationActivity extends BaseActivity {
	private Button regist_next;
	private TextView regist_weight, regist_height, regist_age;
	private RulerWheel ruler_weight, ruler_height, ruler_age;
	/**
	 * 体重最大值   
	 */
	private int WEIGHT_MAX = 200;
	/**
	 * 体重初始值
	 */
	private int WEIGHT_VALUE = 60;
	/**
	 * 身高最大值
	 */
	private int HEIGHT_MAX = 250;
	/**
	 * 身高初始值
	 */
	private int HEIGHT_VALUE = 160;

	/**
	 * 年龄最大值
	 */
	private int AGE_MAX = 2010;
	/**
	 * 年龄初始值
	 */
	private int AGE_VALUE = 1980;
	
	
	
	private Dialog _loadingDialog = null;

	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("RegistInformationActivity");
		MobclickAgent.onResume(RegistInformationActivity.this);
		ZhugeSDK.getInstance().init(getApplicationContext());
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("RegistInformationActivity");
		MobclickAgent.onPause(RegistInformationActivity.this);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_regist_information);

		initTitle();
		initSex();
		initRuler();
		regist_next = (Button) findViewById(R.id.regist_next);
		regist_next.setOnClickListener(this);
	}

	/**
	 * 尺子初始化
	 */
	private void initRuler() {

		regist_weight = (TextView) findViewById(R.id.regist_weight);
		regist_height = (TextView) findViewById(R.id.regist_height);
		regist_age = (TextView) findViewById(R.id.regist_age);
		regist_weight.setText(WEIGHT_VALUE + "");
		regist_height.setText(HEIGHT_VALUE + "");
		regist_age.setText(AGE_VALUE + "");
		ruler_weight = (RulerWheel) findViewById(R.id.ruler_weihgt);
		ruler_height = (RulerWheel) findViewById(R.id.ruler_height);
		ruler_age = (RulerWheel) findViewById(R.id.ruler_age);
		ruler_weight.setChange(WEIGHT_VALUE, WEIGHT_MAX, 10, 10 ,Color.WHITE);
		ruler_height.setChange(HEIGHT_VALUE, HEIGHT_MAX, 10, 10,Color.WHITE);
		ruler_age.setChange(AGE_VALUE, AGE_MAX, 10, 20,Color.WHITE);
		ruler_weight.setValueChangeListener(new OnValueChangeListener() {

			@Override
			public void onValueChange(float value) {
				regist_weight.setText((int) value + "");

			}
		});
		ruler_height.setValueChangeListener(new OnValueChangeListener() {

			@Override
			public void onValueChange(float value) {
				regist_height.setText((int) value + "");

			}
		});
		ruler_age.setValueChangeListener(new OnValueChangeListener() {

			@Override
			public void onValueChange(float value) {
				regist_age.setText((int) value + "");

			}
		});
	}

	/**
	 * 男女初始化
	 */
	private void initSex() {
		ToggleButton rigist_sex = (ToggleButton) findViewById(R.id.rigist_sex);
		rigist_sex.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean choice) {
				if (choice) {
//					ToastUtil.showToast("男");
				} else {
//					ToastUtil.showToast("女");
				}
			}
		});

	}

	/**
	 * 标题初始化
	 */
	public void initTitle() {
		this.getLeftButton().setText("返回");
		this.getLeftButton().setOnClickListener(this);
		
		View v = getTitleBar();
		if( v != null )
			v.setBackgroundColor(Color.TRANSPARENT);
	

	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.btn_left: {
			finishThisActivity();
		}
			break;
		case R.id.regist_next: {
			on_btn_next();
		}
			break;
		default:
			break;
		}
	}
	
	// 点击下一步
	private void on_btn_next()
	{
		_loadingDialog = DialogUtil.createLoadingDialog(this, "正在发送...");
		_loadingDialog.show();
		
		String mobile = (String)DataModule.getInstance().registerInfoMap.get("mobile");
		String password = (String)DataModule.getInstance().registerInfoMap.get("password");
		String vcode = (String)DataModule.getInstance().registerInfoMap.get("vcode");
		
		// sex
		ToggleButton register_sex = (ToggleButton) findViewById(R.id.rigist_sex);
		int sex = 1;
		if( register_sex.isChecked() )
			sex = 0;
		
		float weight = ruler_weight.getValue();  
		int height = (int)ruler_height.getValue();
		int age = (int)ruler_age.getValue();
		
//		PatientBean 
		
		RegisterTask task = new RegisterTask(mobile, password, vcode, sex, weight, height, age);
		task.setTaskCallBack(new TaskPostCallBack<RetError>() {
			@Override
			public void taskFinish(RetError result) {
				
				// 发送成功
				if( result == RetError.NONE )
				{
					_loadingDialog.dismiss();
					
					// 注册成功，直接登录了，需要保存下用户信息
					PatientBean patientBean = (PatientBean)result.getObject();
					DataModule.getInstance().saveLoginedUserInfo(patientBean);
				
					// 跳转页面
					skip(RegistPhotoActivity.class, true);
				}
				else
				{
					// 登录失败
					_loadingDialog.dismiss();
					ToastUtil.showToast(result.getErrorMessage());
				}
			}
		});
		task.executeParallel("");

		
	}

}
