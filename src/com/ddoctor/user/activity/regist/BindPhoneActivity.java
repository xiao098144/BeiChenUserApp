package com.ddoctor.user.activity.regist;

import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.beichen.user.R;
import com.ddoctor.enums.RetError;
import com.ddoctor.user.activity.BaseActivity;
import com.ddoctor.user.activity.MainTabActivity;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.task.BindingMobileTask;
import com.ddoctor.user.task.SendVCodeTask;
import com.ddoctor.user.task.TaskPostCallBack;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.DialogUtil;
import com.ddoctor.utils.ToastUtil;
import com.umeng.analytics.MobclickAgent;
import com.zhuge.analysis.stat.ZhugeSDK;

public class BindPhoneActivity extends BaseActivity {

	private EditText _edittext_mobile;
	private EditText _edittext_vcode;
	private Button _btn_sendvcode;

	private Dialog _dialog = null;

	private String _vcode = "";

	private final static int COUNTDOWN_TIME = 60;// 30
	private int _count = COUNTDOWN_TIME;

	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("BindPhoneActivity");
		MobclickAgent.onResume(BindPhoneActivity.this);
		ZhugeSDK.getInstance().init(getApplicationContext());
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("BindPhoneActivity");
		MobclickAgent.onPause(BindPhoneActivity.this);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_bindphone);
		findViewById();
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.btn_left: {
			finishThisActivity();
		}
			break;
		case R.id.regist_next: {
			on_btn_next();
		}
			break;
		case R.id.button_sendvcode: {
			on_btn_sendvcode();
		}
			break;
		default:
			break;
		}
	}

	// 下一步按钮
	private void on_btn_next() {
		String mobile = _edittext_mobile.getText().toString();
		if (mobile.length() < 1) {
			ToastUtil.showToast("请输入您的手机号!");
			return;
		}

		// 判断验证码是否正确
		String vcode = _edittext_vcode.getText().toString();
		if (vcode.length() < 1) {
			ToastUtil.showToast("请输入验证码！");
			return;
		}

		if (!vcode.equals(_vcode)) {
			ToastUtil.showToast("请输入正确的验证码!");
			return;
		}
		bindPhone(mobile, vcode);
	}

	private void bindPhone(String mobile , String verify){
		BindingMobileTask task = new BindingMobileTask(mobile, verify);
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {

			@Override
			public void taskFinish(DDResult result) {
				if (result.getError() ==RetError.NONE) {
					ToastUtil.showToast("手机号绑定成功");
					try {
						Activity firstActivity = (Activity) DataModule
								.getInstance().registerInfoMap.get("bindphone");
						if (firstActivity != null)
							firstActivity.finish();
					} catch (Exception e) {
						ToastUtil.showToast("绑定手机成功  销毁登录页面 报错了 "+e.getMessage());
					}
					skip(MainTabActivity.class, true);
				} else {
					ToastUtil.showToast(result.getErrorMessage());
				}
			}
		});
		task.executeParallel("");
	}
	
	// 点击发送验证码按钮
	private void on_btn_sendvcode() {

		// 倒计时中，什么也不处理
		if (_timer != null)
			return;

		String mobile = _edittext_mobile.getText().toString();
		if (mobile.length() < 1) {
			ToastUtil.showToast("请输入手机号!");
			return;
		}

		_dialog = DialogUtil.createLoadingDialog(this, "正在发送...");
		_dialog.show();

		SendVCodeTask task = new SendVCodeTask(mobile ,1);
		task.setTaskCallBack(new TaskPostCallBack<RetError>() {
			@Override
			public void taskFinish(RetError result) {
				// 发送成功
				if (result == RetError.NONE) {
					_vcode = (String) result.getObject();
					_dialog.dismiss();
					startTimer();
					ToastUtil.showToast("验证码已发送到您的手机！");
				} else {
					// 登录失败
					_dialog.dismiss();
					ToastUtil.showToast(result.getErrorMessage());
				}
			}
		});
		task.executeParallel("");
	}

	private Timer _timer = null;

	private void startTimer() {
		if (_timer == null)
			_timer = new Timer();

		if (_timerTask == null) {
			_timerTask = new TimerTask() {
				@Override
				public void run() {
					Message message = new Message();
					message.what = 1;
					_handler.sendMessage(message);
				}
			};

		}

		_count = COUNTDOWN_TIME;
		updateSendButtonText(false);

		_timer.schedule(_timerTask, 1000, 1000);
	}

	private void stopTimer() {
		if (_timerTask != null) {
			_timerTask.cancel();
			_timerTask = null;
		}

		if (_timer != null) {
			_timer.cancel();
			_timer.purge();
			_timer = null;
		}
	}

	TimerTask _timerTask = null;

	Handler _handler = new Handler() {
		public void handleMessage(Message msg) {
			if (msg.what == 1) {
				_count--;
				if (_count < 0) {
					stopTimer();
					updateSendButtonText(true);
				} else {
					updateSendButtonText(false);
				}
			}
			super.handleMessage(msg);
		};
	};

	private void updateSendButtonText(boolean showNormal) {
		if (showNormal) {
			_btn_sendvcode.setText(getResources().getString(
					R.string.regist_click_code));
		} else {
			String s = String.format(Locale.CHINESE, "%d秒后可重发", _count);
			_btn_sendvcode.setText(s);
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		stopTimer();
	}

	public void findViewById() {
		Button leftBtn = getLeftButton();
		leftBtn.setText(getString(R.string.basic_back));
		leftBtn.setOnClickListener(this);
		findViewById(R.id.titlebar).setBackgroundColor(Color.TRANSPARENT);

		Button regist_next = (Button) findViewById(R.id.regist_next);
		regist_next.setOnClickListener(this);
		regist_next.setText(getString(R.string.basic_finish));
		_btn_sendvcode = (Button) findViewById(R.id.button_sendvcode);
		_btn_sendvcode.setOnClickListener(this);

		_edittext_mobile = (EditText) findViewById(R.id.edittext_mobile);
		_edittext_vcode = (EditText) findViewById(R.id.edittext_vcode);

	}

}
