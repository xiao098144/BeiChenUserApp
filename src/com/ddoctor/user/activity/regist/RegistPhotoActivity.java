package com.ddoctor.user.activity.regist;

import java.io.File;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.enums.RetError;
import com.ddoctor.user.activity.BaseActivity;
import com.ddoctor.user.activity.MainTabActivity;
import com.ddoctor.user.activity.tsl.TSLUserInfoStep1Activity;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.task.FileUploadTask;
import com.ddoctor.user.task.TaskPostCallBack;
import com.ddoctor.user.wapi.bean.PatientBean;
import com.ddoctor.user.wapi.bean.UploadBean;
import com.ddoctor.user.wapi.constant.Upload;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.DialogUtil;
import com.ddoctor.utils.FileOperationUtil;
import com.ddoctor.utils.ImageLoaderUtil;
import com.ddoctor.utils.MyUtils;
import com.ddoctor.utils.ToastUtil;
import com.umeng.analytics.MobclickAgent;
import com.zhuge.analysis.stat.ZhugeSDK;

public class RegistPhotoActivity extends BaseActivity {
	Button regist_next;

	ImageView _photoButton;

	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("RegistPhotoActivity");
		MobclickAgent.onResume(RegistPhotoActivity.this);
		ZhugeSDK.getInstance().init(getApplicationContext());
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("RegistPhotoActivity");
		MobclickAgent.onPause(RegistPhotoActivity.this);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_regist_photo);
		initTitle();
		regist_next = (Button) findViewById(R.id.regist_next);
		regist_next.setOnClickListener(this);

		_photoButton = (ImageView) findViewById(R.id.button_photo);
		_photoButton.setOnClickListener(this);

		// 注册成功，跳转到修改头像页面，关掉前面的注册页面
		Activity firstActivity = (Activity) DataModule.getInstance().registerInfoMap
				.get("first_activity");
		if (firstActivity != null)
			firstActivity.finish();

		LinearLayout ll_tsl = (LinearLayout)findViewById(R.id.tslLayout);
		PatientBean pb = DataModule.getInstance().getLoginedUserInfo();
		
		if( pb != null && pb.getMobileArea() == 22 )
		{
			TextView tv = (TextView)ll_tsl.findViewById(R.id.tv_tsl1);
			if( tv != null )
			{
				tv.getPaint().setFakeBoldText(true);
			}
			
			Button btn = (Button)ll_tsl.findViewById(R.id.btn_tsl_go);
			if( btn != null )
				btn.setOnClickListener(this);
			
			ll_tsl.setVisibility(View.VISIBLE);
		}
		else
			ll_tsl.setVisibility(View.GONE);
	}

	/**
	 * 标题初始化
	 */
	public void initTitle() {
		this.getLeftButton().setText("返回");
		getLeftButton().setOnClickListener(this);
		
		View v = getTitleBar();
		if( v != null )
			v.setBackgroundColor(Color.TRANSPARENT);
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.btn_left: {
			finishThisActivity();
		}
			break;
		case R.id.regist_next: {
			on_btn_next();
		}
			break;
		case R.id.button_photo: {
			String[] items = new String[]{"本地图片","拍照"};
			DialogUtil.createListDialog(RegistPhotoActivity.this, items, new DialogUtil.ListDialogCallback(){

				@Override
				public void onItemClick(int which) {
					if(which == 0 )
					{
						Intent intent = new Intent();
						intent.setType("image/*");
						intent.setAction(Intent.ACTION_GET_CONTENT);
						startActivityForResult(intent, 0);	
					}
					else
					{
						Intent intent = new Intent(
								MediaStore.ACTION_IMAGE_CAPTURE);
						String filename = DataModule
								.getTakePhotoTempFilename("head");
						File f = new File(filename);
						if (f.exists())
							f.delete();
						Uri uri = Uri.fromFile(f);
						intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
						startActivityForResult(intent, 1);
					}
				}
			}).show();
		}
			break;
		case R.id.btn_tsl_go:
		{
			Activity loginActivity = (Activity)DataModule.getInstance().registerInfoMap.get("login_activity");
			if( loginActivity != null )
				loginActivity.finish();
			
			Intent intent = new Intent(this, MainTabActivity.class);
			startActivity(intent);

			intent = new Intent(this, TSLUserInfoStep1Activity.class);
			startActivity(intent);
		}
			break;
		default:
			break;
		}
	}

	private void on_btn_next() {
		// 注册完成，进入主页面
		Activity loginActivity = (Activity) DataModule.getInstance().registerInfoMap
				.get("login_activity");
		if (loginActivity != null)
			loginActivity.finish();

		skip(MainTabActivity.class, true);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		MyUtils.showLog("requestCode=" + requestCode + " resultCode="
				+ resultCode);

		if (resultCode != RESULT_CANCELED) {
			if (requestCode == 0) {// 选择图片
				if (data != null)
					startPhotoCrop(data.getData());
			} else if (requestCode == 1) {// 拍照
				String filename = DataModule
						.getTakePhotoTempFilename("head");
				File f = new File(filename);
				if (f.exists()) {
					startPhotoCrop(Uri.fromFile(f));
				}else {
					ToastUtil.showToast("获取图片失败!");
				}
				
			} else if (requestCode == 2) {
				// 剪裁
				if (data != null) {
					Bundle b = data.getExtras();
					if (b != null) {
						Bitmap bmp = MyUtils.loadBitmapFromFile(DataModule.getTakePhotoTempFilename("head-crop"));
						if (bmp != null) {
							FileOperationUtil.deleteFile(DataModule.getTakePhotoTempFilename("head"));
							onSetPhoto(bmp);
						}
					}
				}
			}
		}

		super.onActivityResult(requestCode, resultCode, data);
	}

	public void startPhotoCrop(Uri uri) {

		Intent intent = new Intent("com.android.camera.action.CROP");
		intent.setDataAndType(uri, "image/*");
		// 设置裁剪
		intent.putExtra("crop", "true");
		// aspectX aspectY 是宽高的比例
		intent.putExtra("aspectX", 1);
		intent.putExtra("aspectY", 1);
		// outputX outputY 是裁剪图片宽高
		intent.putExtra("outputX", 300);
		intent.putExtra("outputY", 300);
		intent.putExtra("output", Uri.fromFile(new File(DataModule.getTakePhotoTempFilename("head-crop"))));
		intent.putExtra("return-data", true);
		startActivityForResult(intent, 2);
	}

	private void onSetPhoto(Bitmap bmp) {
		// MyUtils.showLog("onSetPhoto:.....");
		// Drawable drawable = new BitmapDrawable(bmp);
		// _photoButton.setBackgroundDrawable(drawable);

		// TODO: 上传头像...
		onUploadPhoto(bmp);
	}

	// 上传头像
	private Dialog _loadingDialog = null;
	private Bitmap _bitmap = null;

	private void onUploadPhoto(Bitmap bmp) {
//		if (_bitmap != null) {
//			_bitmap.recycle();
//			_bitmap = null;
//		}
		_bitmap = bmp;

		_loadingDialog = DialogUtil.createLoadingDialog(this, "提交中...");
		_loadingDialog.show();

		UploadBean uploadBean = new UploadBean();
		uploadBean.setFileType("jpg");

		uploadBean.setType(Upload.HEAD);

		byte[] data = MyUtils.Bitmap2Bytes(bmp);
		String s_data = android.util.Base64
				.encodeToString(data, Base64.DEFAULT);
		uploadBean.setFile(s_data);

		FileUploadTask task = new FileUploadTask(uploadBean);
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {
			@Override
			public void taskFinish(DDResult result) {

				if (_loadingDialog != null) {
					_loadingDialog.dismiss();
					_loadingDialog = null;
				}

				if (result.getError() == RetError.NONE) {
					on_task_finished(result);
				} else {
					on_task_failed(result);
				}

			}
		});
		task.executeParallel("");
	}

	private void on_task_finished(DDResult result) {
		FileOperationUtil.deleteFile(DataModule.getTakePhotoTempFilename("head-crop"));
		MyUtils.showLog("on_task_finished");
		Bundle b = result.getBundle();
		String fileUrl = b.getString("fileUrl");

		MyUtils.showLog(fileUrl);

		// Bitmap bmp1 = MyUtils.getRoundedCornerBitmap(bmp, 150);

		ImageLoaderUtil.displayRoundedCorner(fileUrl, _photoButton, 150,
				R.drawable.default_protrait);

	}

	private void on_task_failed(DDResult result) {
		ToastUtil.showToast(result.getErrorMessage());
	}

}
