package com.ddoctor.user.activity.knowledge;

import java.util.ArrayList;
import java.util.List;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;

import com.beichen.user.R;
import com.ddoctor.enums.RetError;
import com.ddoctor.user.activity.BaseActivity;
import com.ddoctor.user.adapter.KnowledgeCatagoryAdapter;
import com.ddoctor.user.task.GetKnowledegCategoryTask;
import com.ddoctor.user.task.TaskPostCallBack;
import com.ddoctor.user.wapi.bean.CatagoryBean;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.DialogUtil;
import com.ddoctor.utils.ToastUtil;
import com.umeng.analytics.MobclickAgent;

public class KnowledgeLibActivity extends BaseActivity implements
		OnItemClickListener {

	private GridView _catagory_grid;
	private List<CatagoryBean> _dataList = new ArrayList<CatagoryBean>();
	private KnowledgeCatagoryAdapter _adapter;
	protected int _emptyView;

	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("KnowledgeLibActivity");
		MobclickAgent.onResume(KnowledgeLibActivity.this);
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("KnowledgeLibActivity");
		MobclickAgent.onPause(KnowledgeLibActivity.this);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_knowledge);
		findViewById();
		getKnowledgecatagory();
	}

	public void findViewById() {
		Button leftBtn = getLeftButtonText(getString(R.string.basic_back));
		setTitle(getString(R.string.knowledgelib_title));

		leftBtn.setOnClickListener(this);

		_catagory_grid = (GridView) findViewById(R.id.knowledge_catagory_grid);
		_adapter = new KnowledgeCatagoryAdapter(this);
		_catagory_grid.setAdapter(_adapter);
		_adapter.setData(_dataList);
		_catagory_grid.setOnItemClickListener(this);
	}

	private Dialog _loadingDialog =null;
	private void getKnowledgecatagory() {
		_loadingDialog = DialogUtil.createLoadingDialog(KnowledgeLibActivity.this);
		_loadingDialog.show();
		GetKnowledegCategoryTask task = new GetKnowledegCategoryTask();
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {

			@Override
			public void taskFinish(DDResult result) {
				_loadingDialog.dismiss();
				if (result.getError() == RetError.NONE) {
					ArrayList<CatagoryBean> list = result.getBundle()
							.getParcelableArrayList("list");
					_dataList.addAll(list);
					_emptyView = 3 - _dataList.size() % 3;
					if (_emptyView > 0 && _emptyView %3 !=0) {
						for (int i = 0; i < _emptyView; i++) {
							CatagoryBean catagoryBean = new CatagoryBean();
							catagoryBean.setId(0);
							catagoryBean
									.setImgUrl(_dataList.get(0).getImgUrl());
							catagoryBean.setName("");
							_dataList.add(catagoryBean);
						}
					}

					_adapter.notifyDataSetChanged();
				} else {
					ToastUtil.showToast(result.getErrorMessage());
				}
			}
		});
		task.executeParallel("");
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.btn_left: {
			finishThisActivity();
		}
			break;
		default:
			break;
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		int dataIdx = position;
		if (dataIdx >= _dataList.size() - _emptyView) {
			return;
		}

		Intent intent = new Intent();
		intent.setClass(KnowledgeLibActivity.this, KnowledegListActivity.class);
		intent.putExtra("catagory", _dataList.get(dataIdx).getId());
		startActivity(intent);
	}

}
