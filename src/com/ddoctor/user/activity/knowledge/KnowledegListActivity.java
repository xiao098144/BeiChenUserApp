package com.ddoctor.user.activity.knowledge;

import java.util.ArrayList;
import java.util.List;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.enums.RefreshAction;
import com.ddoctor.enums.RetError;
import com.ddoctor.user.activity.BaseActivity;
import com.ddoctor.user.adapter.KnowledgeListAdapter;
import com.ddoctor.user.config.AppBuildConfig;
import com.ddoctor.user.task.GetKnowledgeListTask;
import com.ddoctor.user.task.SearchKnowledgeTask;
import com.ddoctor.user.task.TaskPostCallBack;
import com.ddoctor.user.view.DDPullToRefreshView;
import com.ddoctor.user.view.DDPullToRefreshView.OnHeaderRefreshListener;
import com.ddoctor.user.wapi.bean.ContentBean;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.DialogUtil;
import com.ddoctor.utils.StringUtils;
import com.ddoctor.utils.ToastUtil;
import com.umeng.analytics.MobclickAgent;
import com.zhuge.analysis.stat.ZhugeSDK;

public class KnowledegListActivity extends BaseActivity implements
		OnHeaderRefreshListener, OnItemClickListener, OnScrollListener,
		TextWatcher {

	private EditText met_search;
	private ImageButton mbtn_search;
	private DDPullToRefreshView _refreshViewContainer;
	private ListView mlv;
	private TextView _tv_norecord;
	private View _getMoreView;

	private int _pageNum = 1;

	private int _searchNum = 1;

	private RefreshAction _refreshAction = RefreshAction.PULLTOREFRESH;
	private List<ContentBean> _dataList = new ArrayList<ContentBean>();
	
	private List<ContentBean> _sourceList = new ArrayList<ContentBean>();

	private boolean isSearch = false;

	private String keyword;

	private KnowledgeListAdapter madapter;

	private int _catagoryId = 0;
	
	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("KnowledgeListActivity");
		MobclickAgent.onResume(KnowledegListActivity.this);
		ZhugeSDK.getInstance().init(getApplicationContext());
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("KnowledgeListActivity");
		MobclickAgent.onPause(KnowledegListActivity.this);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_knowledeglist);
		_catagoryId = getIntent().getIntExtra("catagory", 1);
		findViewById();
		loadingData(true, _pageNum);
	}

	public void findViewById() {
		Button leftBtn = getLeftButtonText(getResources().getString(
				R.string.basic_back));
		setTitle(getString(R.string.knowledgelib_title));
  
		met_search = (EditText) findViewById(R.id.knowledgelib_search_et);
		met_search.setHint(StringUtils.fromatETHint(
				getString(R.string.et_hint_knowledgelib), 13));
		mbtn_search = (ImageButton) findViewById(R.id.knowledgelib_search_btn);
		
		_refreshViewContainer = (DDPullToRefreshView) findViewById(R.id.knowledeg_refresh);
		_refreshViewContainer.setOnHeaderRefreshListener(this);
		_refreshViewContainer.setVisibility(View.INVISIBLE);
		mlv = (ListView) findViewById(R.id.listView);
//		mlv.setSelector(getResources().getDrawable(R.drawable.list_selector));
		mlv.setSelector(R.drawable.list_selector);
		mlv.setOnItemClickListener(this);
		mlv.setOnScrollListener(this);
		_tv_norecord = (TextView) findViewById(R.id.tv_norecord);
		_tv_norecord.setVisibility(View.GONE);
		mlv.setEmptyView(_tv_norecord);
		
		_getMoreView = createGetMoreView();
		setGetMoreContent("已全部加载", false, false);
		mlv.addFooterView(_getMoreView);

		madapter = new KnowledgeListAdapter(KnowledegListActivity.this);
		madapter.setData(_dataList);
		mlv.setAdapter(madapter);

		mbtn_search.setOnClickListener(this);
		leftBtn.setOnClickListener(this);
		met_search.setOnKeyListener(new OnKeyListener() {

			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (keyCode == KeyEvent.KEYCODE_ENTER) {
					keyword = met_search.getText().toString().trim();
					if (TextUtils.isEmpty(keyword)) {
						isSearch = false;
						// ToastUtil.showToast("搜索关键字不能为空");
					} else {
						searchKnowledge(true ,keyword, _searchNum);
					}
					return true;
				}
				return false;
			}
		});
		met_search.addTextChangedListener(this);
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.btn_left: {
			finishThisActivity();
		}
			break;
		case R.id.knowledgelib_search_btn: {
			keyword = met_search.getText().toString().trim();
			if (TextUtils.isEmpty(keyword)) {
				isSearch = false;
				// ToastUtil.showToast("搜索关键字不能为空");
			} else {
				searchKnowledge(true ,keyword, _searchNum);
			}
		}
			break;

		default:
			break;
		}
	}

	// 加载更多相关函数 >>>>>>
	boolean _bGetMoreEnable = false;
	boolean _bsearchGetMoreEnable = false;

	private View createGetMoreView() {
		if (_getMoreView != null)
			return _getMoreView;

		View v = getLayoutInflater().inflate(R.layout.refresh_footer, null);

		return v;
	}

	private void setGetMoreContent(String message, boolean showImage,
			boolean animation) {
		TextView tv = (TextView) _getMoreView
				.findViewById(R.id.pull_to_load_text);
		tv.setText(message);

		ImageView imgView = (ImageView) _getMoreView
				.findViewById(R.id.pull_to_load_image);
		AnimationDrawable ad = (AnimationDrawable) imgView.getBackground();
		if (showImage) {
			if (animation) {
				ad.start();
			} else {
				ad.stop();
				ad.selectDrawable(0);
			}

			imgView.setVisibility(View.VISIBLE);
		} else {
			ad.stop();
			ad.selectDrawable(0);

			imgView.setVisibility(View.GONE);
		}
	}

	private Dialog _loadingDialog = null;

	private void loadingData(boolean showLoading, int page) {
		if (showLoading) {
			_loadingDialog = DialogUtil.createLoadingDialog(
					KnowledegListActivity.this,
					getString(R.string.basic_loading));
			_loadingDialog.show();
		}
		isSearch = false;
		GetKnowledgeListTask task = new GetKnowledgeListTask(page, _catagoryId);

		final int page1 = page;
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {

			@Override
			public void taskFinish(DDResult result) {
				if (result.getError() == RetError.NONE) {
					List<ContentBean> tmpList = result.getBundle().getParcelableArrayList("list");
					if (page1 > 1) // 加载更多
					{
						_dataList.addAll(tmpList);
						_sourceList.addAll(tmpList);
						madapter.notifyDataSetChanged();
					} else {
						// 加载第一页
						_dataList.clear();
						_sourceList.clear();
						_dataList.addAll(tmpList);
						_sourceList.addAll(tmpList);
						madapter.notifyDataSetChanged();

						_refreshViewContainer.onHeaderRefreshComplete();
						_refreshViewContainer.setVisibility(View.VISIBLE);
						_refreshAction = RefreshAction.NONE;

						if (_loadingDialog != null)
							_loadingDialog.dismiss();

					}

					// 是否显示"加载更多"
					if (tmpList.size() > 0) {
						setGetMoreContent("滑动加载更多", true, false);
						_bGetMoreEnable = true;
					} else {
						if (page1 == 1) {
							_tv_norecord.setText(result.getErrorMessage());
							_tv_norecord.setVisibility(View.VISIBLE);
						}
						// 没数据了，加载完成
						setGetMoreContent("已全部加载", false, false);
						_bGetMoreEnable = false;
					}

					// 确保加载成功后，再修改这个变量
					_pageNum = page1;
				} else {// 加载失败
					if (page1 > 1) {
						setGetMoreContent("滑动加载更多", true, false);
					} else {
						_refreshViewContainer.onHeaderRefreshComplete();
						_refreshAction = RefreshAction.NONE;

						if (_loadingDialog != null)
							_loadingDialog.dismiss();
					}

					ToastUtil.showToast(result.getErrorMessage());
				}

				_refreshAction = RefreshAction.NONE;
			}
		});

		task.executeParallel("");
	}

	private void searchKnowledge(boolean showDialog, String keyword, int page) {
		isSearch = true;
		if (showDialog) {
			if (_loadingDialog == null) {
				_loadingDialog = DialogUtil.createLoadingDialog(
						KnowledegListActivity.this,
						getString(R.string.basic_loading));
			}
			_loadingDialog.show();
		}
		final int page1 = page;
		SearchKnowledgeTask task = new SearchKnowledgeTask(page, keyword);
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {

			@Override
			public void taskFinish(DDResult result) {
				if (result.getError() == RetError.NONE) {
					List<ContentBean> tmpList = result.getBundle().getParcelableArrayList("list");
					if (page1 > 1) // 加载更多
					{
						_dataList.addAll(tmpList);
						madapter.notifyDataSetChanged();
					} else {
						// 加载第一页
						_dataList.clear();
						_dataList.addAll(tmpList);
						madapter.notifyDataSetChanged();

						_refreshViewContainer.onHeaderRefreshComplete();
						_refreshViewContainer.setVisibility(View.VISIBLE);
						_refreshAction = RefreshAction.NONE;

						if (_loadingDialog != null)
							_loadingDialog.dismiss();

					}

					// 是否显示"加载更多"
					if (tmpList.size() > 0) {
						setGetMoreContent("滑动加载更多", true, false);
						_bGetMoreEnable = true;
					} else {
						if (page1 == 1) {
							_tv_norecord.setText(result.getErrorMessage());
							_tv_norecord.setVisibility(View.VISIBLE);
						}
						// 没数据了，加载完成
						setGetMoreContent("已全部加载", false, false);
						_bGetMoreEnable = false;
					}

					// 确保加载成功后，再修改这个变量
						_searchNum= page1;
				} else {// 加载失败
					if (page1 > 1) {
						setGetMoreContent("滑动加载更多", true, false);
					} else {
						_refreshViewContainer.onHeaderRefreshComplete();
						_refreshAction = RefreshAction.NONE;

						if (_loadingDialog != null)
							_loadingDialog.dismiss();
					}

					ToastUtil.showToast(result.getErrorMessage());
				}

				_refreshAction = RefreshAction.NONE;
			}
		});
		task.executeParallel("");
	}

	@Override
	public void onHeaderRefresh(DDPullToRefreshView view) {
			if (_refreshAction == RefreshAction.NONE) {
				_refreshAction = RefreshAction.PULLTOREFRESH;
				if (isSearch) {
					searchKnowledge(false, keyword, 1);
				} else {
					loadingData(false, 1);
				}
			} else {
				// 正在加载，什么也不做
				view.onHeaderRefreshComplete();
			}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		int dataIdx = position - mlv.getHeaderViewsCount();
			if (dataIdx >=_dataList.size()) {
				return;
			}
		
		Intent intent = new Intent(KnowledegListActivity.this,
				KnowledgeDetailActivity.class);
		Bundle data = new Bundle();
		data.putString("url", _dataList.get(dataIdx).getUrl());
		data.putInt("id", _dataList.get(dataIdx).getContentId());
		intent.putExtra(AppBuildConfig.BUNDLEKEY, data);
		startActivity(intent);
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
			if (_refreshAction == RefreshAction.NONE) {
				if (_bGetMoreEnable) {// 有加载更多
					int lastPos = mlv.getLastVisiblePosition();
					int total = mlv.getHeaderViewsCount() + _dataList.size()
							+ mlv.getFooterViewsCount();
					if (lastPos == total - 1) {
						// 加载更多显示出来了，开始加载
						_refreshAction = RefreshAction.LOADMORE;
						setGetMoreContent("正在加载...", true, true);
						if (isSearch) {
							searchKnowledge(false, keyword, _searchNum+1);
						} else {
							loadingData(false, _pageNum + 1);
						}
					}
				}
			}
	}

	@Override
	public void afterTextChanged(Editable s) {

	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		if (s == null || TextUtils.isEmpty(s.toString().trim())) {
			_searchNum = 1;
			if (_sourceList.size()>0) {
				_dataList.clear();
				_dataList.addAll(_sourceList);
				madapter.notifyDataSetChanged();
			}else {
				loadingData(false, 1);
			}
			isSearch = false;
		}
	}

}
