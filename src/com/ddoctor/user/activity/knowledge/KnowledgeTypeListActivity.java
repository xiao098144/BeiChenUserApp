package com.ddoctor.user.activity.knowledge;

import com.ddoctor.user.activity.BaseFragmentActivity;

/**
 * 知识库二级分类
 * @author 萧
 * @Date 2015-5-29上午10:15:07
 * @TODO TODO
 */
public class KnowledgeTypeListActivity extends BaseFragmentActivity{

//	private String type;
//	private String title;
//
//	private FrameLayout top;
//	private TabPageIndicator indicator;
//	private ViewPager viewPager;
//	
//	private TypeIndicatorPagerAdapter adapter;
//	private String[] tabTitle;
//	private List<Fragment> _fragmentList = new ArrayList<Fragment>();
//	
//	private void getIntentInfo(){
//		Intent intent = getIntent();
//		Bundle data = intent.getBundleExtra(AppBuildConfig.BUNDLEKEY);
//		type = data.getString("type");
//		title = data.getString("title");
//		ToastUtil.showToast(type);
//	}
//	
//	private void initdata(){
//		tabTitle = new String[8];
//		for (int i = 0; i < tabTitle.length; i++) {
//			tabTitle[i] = "类别-"+(i+1);
//			KnowledgeTypeItemFragment fragment = new KnowledgeTypeItemFragment();
//			Bundle data = new Bundle();
//			data.putString("title", "标题-"+(i+1));
//			data.putInt("type", i);
//			ArrayList<KnowlegeBean> list = new ArrayList<KnowlegeBean>();
//			for (int j = 0; j < 15; j++) {
//				KnowlegeBean knowlegeBean = new KnowlegeBean();
//				knowlegeBean.setId(j);
//				knowlegeBean.setPraise(new Random().nextInt()*100);
//				knowlegeBean.setRecommend(new Random().nextInt()*200);
//				knowlegeBean.setType(j);
//				knowlegeBean.setTitle("分类-"+(i+1)+" 标题标题- "+(j+1));
//				list.add(knowlegeBean);
//			}
//			data.putParcelableArrayList("data", list);
//			fragment.setArguments(data);
//			_fragmentList.add(fragment);
//		}
//		adapter = new TypeIndicatorPagerAdapter(tabTitle, _fragmentList, getSupportFragmentManager());
//		viewPager.setAdapter(adapter);
//		indicator.setViewPager(viewPager);
//	}
//	
//	@Override
//	protected void onResume() {
//		super.onResume();
//		MobclickAgent.onPageStart("KnowledgeTypeListActivity");
//		MobclickAgent.onResume(KnowledgeTypeListActivity.this);
//		ZhugeSDK.getInstance().init(getApplicationContext());
//	}
//
//	@Override
//	protected void onPause() {
//		super.onPause();
//		MobclickAgent.onPageEnd("KnowledgeTypeListActivity");
//		MobclickAgent.onPause(KnowledgeTypeListActivity.this);
//	}
//	
//	@Override
//	public void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.act_typeindicatorlist);
//		getIntentInfo();
//		findViewById();
//		initdata();
//	}
//	
//	protected void findViewById() {
//		Button leftBtn = getLeftButtonText(getResources().getString(R.string.basic_back));
//		Button rigthBtn = getRightButtonText(getResources().getString(R.string.basic_change));
//		setTitle(title);
//		
//		top = (FrameLayout) findViewById(R.id.typeindicator_top);
//		top.setBackgroundColor(getResources().getColor(R.color.default_titlebar));
//		indicator = (TabPageIndicator) findViewById(R.id.type_indicator);
//		indicator.setSelectedColor(getResources().getColor(R.color.default_titlebar));
//		viewPager = (ViewPager) findViewById(R.id.pager);
//		
//		leftBtn.setOnClickListener(this);
//		rigthBtn.setOnClickListener(this);
//	}
//
//	
//	@Override
//	public void onClick(View v) {
//		super.onClick(v);
//		switch (v.getId()) {
//		case R.id.btn_left:{
//			finishThisActivity();
//		}
//			break;
//		case R.id.btn_right:{
//			
//		}
//		break;
//		default:
//			break;
//		}
//	}
	
}
