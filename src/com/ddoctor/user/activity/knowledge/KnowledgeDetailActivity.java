package com.ddoctor.user.activity.knowledge;

import java.io.File;
import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Looper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebView.HitTestResult;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.beichen.user.R;
import com.beichen.user.wxapi.WXEntryActivity;
import com.ddoctor.application.MyApplication;
import com.ddoctor.enums.RetError;
import com.ddoctor.user.activity.BaseActivity;
import com.ddoctor.user.config.AppBuildConfig;
import com.ddoctor.user.task.AddCollectionTask;
import com.ddoctor.user.task.GetKnowledgeDetailTask;
import com.ddoctor.user.task.TaskPostCallBack;
import com.ddoctor.user.wapi.bean.ContentBean;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.DownloadUtil;
import com.ddoctor.utils.FileOperationUtil;
import com.ddoctor.utils.FilePathUtil;
import com.ddoctor.utils.MyUtils;
import com.ddoctor.utils.ToastUtil;
import com.tencent.connect.share.QQShare;
import com.tencent.connect.share.QzoneShare;
import com.tencent.mm.sdk.modelmsg.SendMessageToWX;
import com.tencent.mm.sdk.modelmsg.WXMediaMessage;
import com.tencent.mm.sdk.modelmsg.WXWebpageObject;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.Tencent;
import com.tencent.tauth.UiError;
import com.umeng.analytics.MobclickAgent;
import com.zhuge.analysis.stat.ZhugeSDK;

public class KnowledgeDetailActivity extends BaseActivity implements
		DownloadUtil.OnDownLoadFinishedListener {

	private Button leftBtn;
	private LinearLayout right_layout;
	private ProgressBar progressBar;
	private WebView wv;
	private TextView mtv_error;

//	private ImageButton btn_collect;
//	private ImageButton btn_share;
//	private KnowlegeBean _contentBean = new KnowlegeBean();
	private ContentBean _contentBean = new ContentBean();

	private WebSettings settings;
	protected boolean isLongClick = false;
	private String shareImgUrl;
	private boolean isWXInstalled = false;

	private Tencent mtencent;

	private boolean fromCollection = false;

	private Dialog shareDialog;

	private int knowledgeId;
	private String url;

	private void getIntentInfo() {
		Intent intent = getIntent();
		Bundle data = intent.getBundleExtra(AppBuildConfig.BUNDLEKEY);
		url = data.getString("url");
		knowledgeId = data.getInt("id");
		fromCollection = data.getBoolean("collect", false);
	}

	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("KnowledgeDetailActivity");
		MobclickAgent.onResume(KnowledgeDetailActivity.this);
		ZhugeSDK.getInstance().init(getApplicationContext());
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("KnowledgeDetailActivity");
		MobclickAgent.onPause(KnowledgeDetailActivity.this);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_knowledgedetail);
		getIntentInfo();
		findViewById();
		getKnowledgeById();
		initWebView();
	}

	@SuppressLint("SetJavaScriptEnabled")
	private void initWebView() {
		wv.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
		settings = wv.getSettings();
		settings.setAppCachePath(FilePathUtil.getCachePath());
		settings.setAppCacheEnabled(true);
		settings.setCacheMode(WebSettings.LOAD_DEFAULT);

		settings.setDatabaseEnabled(true);
		settings.setDefaultTextEncodingName("UTF-8");
		/** 使用缓存 则 必须打开 */
		settings.setDomStorageEnabled(true);
		settings.setLoadWithOverviewMode(true);
		settings.setLoadsImagesAutomatically(true);

		settings.setNeedInitialFocus(true);

		settings.setBlockNetworkImage(false);

		settings.setJavaScriptEnabled(true);
		settings.setJavaScriptCanOpenWindowsAutomatically(false);

		wv.setWebChromeClient(new WebChromeClient() {

			@Override
			public void onProgressChanged(WebView view, int newProgress) {
				if (newProgress == 100) {
					progressBar.setVisibility(View.INVISIBLE);
				} else {
					if (View.VISIBLE != progressBar.getVisibility()) {
						progressBar.setVisibility(View.VISIBLE);
					}
					progressBar.setProgress(newProgress);
				}

				super.onProgressChanged(view, newProgress);
			}
		});
		wv.setWebViewClient(new WebViewClient() {

			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				view.loadUrl(url);
				return true;
			}

			@Override
			public void onPageStarted(WebView view, String url, Bitmap favicon) {
				// TODO Auto-generated method stub
				super.onPageStarted(view, url, favicon);
			}

			@Override
			public void onPageFinished(WebView view, String url) {
				// TODO Auto-generated method stub
				super.onPageFinished(view, url);
				mtv_error.setVisibility(View.GONE);
			}

			@Override
			public void onReceivedError(WebView view, int errorCode,
					String description, String failingUrl) {
				MyUtils.showLog("", errorCode + " " + description + " "
						+ failingUrl);
				mtv_error.setVisibility(View.VISIBLE);
				super.onReceivedError(view, errorCode, description, failingUrl);
			}

		});
		wv.setOnLongClickListener(new OnLongClickListener() {

			@Override
			public boolean onLongClick(View v) {
				HitTestResult hitTestResult = wv.getHitTestResult();

				if (null != hitTestResult
						&& (HitTestResult.IMAGE_TYPE == hitTestResult.getType() || HitTestResult.SRC_IMAGE_ANCHOR_TYPE == hitTestResult
								.getType())) {
					isLongClick = true;
					String a = "tp=webp&";
					// downloadDialog(hitTestResult.getExtra().replace(a, ""));
					MyUtils.showLog(""
							+ hitTestResult.getExtra().replace(a, "") + " "
							+ _contentBean.getImage());
					downloadDialog(_contentBean.getImage());
				}
				return false;
			}
		});
		wv.loadUrl(url);
	}

	private void getKnowledgeById() {

		GetKnowledgeDetailTask task = new GetKnowledgeDetailTask(knowledgeId);

		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {

			@Override
			public void taskFinish(DDResult result) {
				if (result.getError() == RetError.NONE) {
					_contentBean.setData((ContentBean) result.getBundle()
							.getParcelable("data"));
					wv.loadUrl(_contentBean.getUrl());
					new DownLoadThread(_contentBean.getImage(),
							FileOperationUtil.getFileName(_contentBean
									.getImage()), FilePathUtil.getPicPath())
							.start();
				} else {
					
				}
			}
		});
		task.executeParallel("");

	}

	private void addCollection() {
		AddCollectionTask task = new AddCollectionTask(knowledgeId);
		task.setTaskCallBack(new TaskPostCallBack<RetError>() {

			@Override
			public void taskFinish(RetError result) {
				if (result == RetError.NONE) {
					ToastUtil.showToast("添加收藏成功，可以到\"我的收藏\"查看收藏记录");
				} else {
					ToastUtil.showToast(result.getErrorMessage());
				}
			}
		});
		task.executeParallel("");
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.btn_left: {
			finishThisActivity();
		}
			break;
		case R.id.collect: {
			addCollection();
		}
			break;
		case R.id.share: {
			showShareOptionDialog();
		}
			break;
		case R.id.knowledgedetail_tv_error: {
			wv.reload();
		}
			break;
		default:
			break;
		}
	}

	public void downloadDialog(final String fileUrl) {
		final Dialog dialog = new Dialog(KnowledgeDetailActivity.this,
				R.style.NoTitleDialog);
		View view = LayoutInflater.from(KnowledgeDetailActivity.this).inflate(
				R.layout.layout_floating, null);
		view.setBackgroundResource(R.drawable.dark_round_shape);
		TextView tv = (TextView) view.findViewById(R.id.floating_tv_content);
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		tv.setLayoutParams(params);
		tv.setText("点击保存图片");
		tv.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				new DownLoadThread(fileUrl, FileOperationUtil
						.getFileName(fileUrl), FilePathUtil.getPicPath())
						.start();
				dialog.dismiss();
			}
		});
		Window window = dialog.getWindow();
		window.setGravity(Gravity.CENTER);
		window.setLayout(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		dialog.setCancelable(true);
		dialog.setCanceledOnTouchOutside(true);
		dialog.setContentView(view);
		dialog.show();
	}

	class DownLoadThread extends Thread {

		private String url;
		private String saveName;
		private String savePath;

		public DownLoadThread(String url, String saveName, String savePath) {
			this.url = url;
			this.saveName = saveName;
			this.savePath = savePath;
		}

		@Override
		public void run() {
			super.run();
			Looper.prepare();
			DownloadUtil.getInstance().startDown1(url, saveName, savePath);
			Looper.loop();
		}

	}

	private ImageButton generateImageButton(int resid) {
		ImageButton imgBtn = new ImageButton(KnowledgeDetailActivity.this);
		imgBtn.setBackgroundResource(resid);
		return imgBtn;
	}

	protected void findViewById() {
		// if (DataModule.getInstance().mwxAPI == null) {
		// DataModule.getInstance().mwxAPI = WXAPIFactory.createWXAPI(
		// getApplicationContext(), AppBuildConfig.WXAPP_ID, true);
		// }
		// isWXInstalled = DataModule.getInstance().mwxAPI.isWXAppInstalled();
		if (MyApplication.mwxAPI == null) {
			MyApplication.mwxAPI = WXAPIFactory.createWXAPI(
					getApplicationContext(), AppBuildConfig.WXAPP_ID, true);
		}
		isWXInstalled = MyApplication.mwxAPI.isWXAppInstalled();

		if (mtencent == null) {
			mtencent = Tencent.createInstance(AppBuildConfig.TencentKey,
					getApplicationContext());
		}

		leftBtn = getLeftButtonText(getString(R.string.basic_back));
		right_layout = (LinearLayout) findViewById(R.id.titlebar_right_layout);
//		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(-2,
//				-2, 1);
//		params.setMargins(3, 3, 3, 3);
//		if (!fromCollection) {
//			btn_collect = generateImageButton(R.drawable.knowledge_collect_normal);
//			btn_collect.setId(R.id.collect);
//			btn_collect.setOnClickListener(this);
//			right_layout.addView(btn_collect, params);
//		}
//		btn_share = generateImageButton(R.drawable.knowledge_share);
//		btn_share.setId(R.id.share);
//		right_layout.addView(btn_share, params);
//		right_layout.setVisibility(View.VISIBLE);
		setTitle(getResources().getString(R.string.knowledgelib_title));

		progressBar = (ProgressBar) findViewById(R.id.knowledgedetail_progress);
		wv = (WebView) findViewById(R.id.knowledgedetail_wv);
		mtv_error = (TextView) findViewById(R.id.knowledgedetail_tv_error);

		leftBtn.setOnClickListener(this);
//		btn_share.setOnClickListener(this);
		mtv_error.setOnClickListener(this);
		DownloadUtil.getInstance().setOnDownLoadFinished(this);
	}

	@Override
	public String getFilePath() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onComplete(String filePath) {
		if (isLongClick) {
			String msg = String.format(
					getResources().getString(R.string.knowledge_picSave),
					FilePathUtil.getPicPath());
			if (filePath.equalsIgnoreCase("error")) {
				msg = "地址异常，下载失败";
			}
			ToastUtil.showToast(msg);
		} else {
			shareImgUrl = filePath;
		}
	}

	private void showShareOptionDialog() {
		if (shareDialog == null) {
			shareDialog = new Dialog(KnowledgeDetailActivity.this,
					R.style.NoTitleDialog);
		}
		View view = getLayoutInflater().inflate(R.layout.layout_share, null);
		TextView tv_qq = (TextView) view.findViewById(R.id.share_tv_tencent);
		TextView tv_qzone = (TextView) view.findViewById(R.id.share_tv_qzone);
		TextView tv_wx = (TextView) view.findViewById(R.id.share_tv_wx);
		TextView tv_wx_friend = (TextView) view
				.findViewById(R.id.share_tv_wxfriend);
		Button btn_cancel = (Button) view.findViewById(R.id.btn_cancel);
		btn_cancel.setText(getString(R.string.basic_cancel));
		ShareClickListener shareClickListener = new ShareClickListener();
		tv_qq.setOnClickListener(shareClickListener);
		tv_qzone.setOnClickListener(shareClickListener);
		tv_wx.setOnClickListener(shareClickListener);
		tv_wx_friend.setOnClickListener(shareClickListener);
		btn_cancel.setOnClickListener(shareClickListener);

		Window window = shareDialog.getWindow();
		window.setGravity(Gravity.BOTTOM);
		android.view.WindowManager.LayoutParams lp = new android.view.WindowManager.LayoutParams();
		lp.width = android.view.WindowManager.LayoutParams.WRAP_CONTENT;
		lp.height = android.view.WindowManager.LayoutParams.WRAP_CONTENT;
		window.setAttributes(lp);
		shareDialog.setCancelable(true);
		shareDialog.setCanceledOnTouchOutside(true);
		shareDialog.setContentView(view);
		shareDialog.show();

	}

	private void dismissShareDialog() {
		if (shareDialog != null && shareDialog.isShowing()) {
			shareDialog.dismiss();
			shareDialog = null;
		}
	}

	class ShareClickListener implements OnClickListener {
		String sharePath = null;

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.share_tv_tencent: {
				dismissShareDialog();
				sharePath = "QQ";
				share2Tencent();
			}
				break;
			case R.id.share_tv_qzone: {
				dismissShareDialog();
				sharePath = "Qzone";
				share2Qzone();
			}
				break;
			case R.id.share_tv_wx: {
				dismissShareDialog();
				sharePath = "WX";
				share2WX(true);
			}
				break;
			case R.id.share_tv_wxfriend: {
				dismissShareDialog();
				sharePath = "WXFriend";
				share2WX(false);
			}
				break;
			case R.id.btn_cancel: {
				dismissShareDialog();
			}
				break;
			default:
				break;
			}
		}
	}

	/**
	 * Timeline 朋友圈 Session 好友
	 * 
	 * @param share2Friend
	 *            true 分享给好友 false 分享到朋友圈
	 */
	private void share2WX(final boolean share2Friend) {
		if (isWXInstalled) {
			WXEntryActivity.currentAction = WXEntryActivity.WX_SHARE;
			WXWebpageObject webpage = new WXWebpageObject();
			webpage.webpageUrl = url;
			WXMediaMessage msg = new WXMediaMessage(webpage);
			msg.title = _contentBean.getTitle();
			try {
				File file = new File(shareImgUrl);
				if (null != file && file.exists() && file.isFile()) {
					Bitmap bmp = BitmapFactory.decodeFile(file
							.getAbsolutePath());
					Bitmap thumbBmp = Bitmap.createScaledBitmap(bmp, 150, 150,
							true);
					bmp.recycle();
					msg.setThumbImage(thumbBmp);
				}
			} catch (Exception e) {
				MyUtils.showLog("图片异常，无法分享图片 " + e.getMessage());
			}
			SendMessageToWX.Req req = new SendMessageToWX.Req();
			req.transaction = buildTransaction("webpage");
			req.message = msg;
			req.scene = share2Friend ? SendMessageToWX.Req.WXSceneSession
					: SendMessageToWX.Req.WXSceneTimeline;
			// DataModule.getInstance().mwxAPI.sendReq(req);
			MyApplication.mwxAPI.sendReq(req);
		} else {
			ToastUtil.showToast("请先安装微信");
		}
	}

	private String buildTransaction(final String type) {
		return (type == null) ? String.valueOf(System.currentTimeMillis())
				: type + System.currentTimeMillis();
	}

	private void share2Qzone() {
		final Bundle params = new Bundle();
		params.putInt(QzoneShare.SHARE_TO_QZONE_KEY_TYPE,
				QzoneShare.SHARE_TO_QZONE_TYPE_IMAGE_TEXT);
		params.putString(QzoneShare.SHARE_TO_QQ_TITLE, _contentBean.getTitle());
		params.putString(QzoneShare.SHARE_TO_QQ_TARGET_URL, url);
		ArrayList<String> imgList = new ArrayList<String>();
		imgList.add(_contentBean.getImage());
		params.putStringArrayList(QzoneShare.SHARE_TO_QQ_IMAGE_URL, imgList);
		params.putString(QzoneShare.SHARE_TO_QQ_APP_NAME, getResources()
				.getString(R.string.app_name));
		mtencent.shareToQzone(KnowledgeDetailActivity.this, params,
				new ShareUiListener());
	}

	private void share2Tencent() {
		final Bundle params = new Bundle();
		params.putInt(QQShare.SHARE_TO_QQ_KEY_TYPE,
				QQShare.SHARE_TO_QQ_TYPE_DEFAULT);
		params.putString(QQShare.SHARE_TO_QQ_TITLE, _contentBean.getTitle());
		params.putString(QQShare.SHARE_TO_QQ_TARGET_URL, url);
		params.putString(QQShare.SHARE_TO_QQ_IMAGE_URL, _contentBean.getImage());
		params.putString(QQShare.SHARE_TO_QQ_APP_NAME, getResources()
				.getString(R.string.app_name));
		mtencent.shareToQQ(KnowledgeDetailActivity.this, params,
				new ShareUiListener());
	}

	class ShareUiListener implements IUiListener {
		@Override
		public void onCancel() {
			ToastUtil.showToast(getString(R.string.share_cancel));
			dismissShareDialog();
		}

		@Override
		public void onComplete(Object arg0) {
			dismissShareDialog();
			ToastUtil.showToast(getString(R.string.share_success));
		}

		@Override
		public void onError(UiError arg0) {
			dismissShareDialog();
			ToastUtil.showToast(getString(R.string.share_fail));
		}
	}

}
