package com.ddoctor.user.activity;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.beichen.user.R;
import com.ddoctor.utils.FileUtils;
import com.umeng.analytics.MobclickAgent;
import com.zhuge.analysis.stat.ZhugeSDK;

public class WebViewActivity extends BaseActivity{
	private WebView _webView;

	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("WebViewActivity");
		MobclickAgent.onResume(WebViewActivity.this);
		ZhugeSDK.getInstance().init(getApplicationContext());
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("WebViewActivity");
		MobclickAgent.onPause(WebViewActivity.this);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_webview);
		
		initUI();
		
		String source = getIntent().getStringExtra("source");
		String content = getIntent().getStringExtra("content");
		String title = getIntent().getStringExtra("title");
		if( source == null )
			source = "";
		if( content == null ) content = "";
		if( title == null ) title = "";
		this.setTitle(title);
		
		if( source.equalsIgnoreCase("local") )
		{
			String html = FileUtils
					.getFromAssets(getResources(), content);

			_webView.loadDataWithBaseURL("file:///android_asset/", html,
					"text/html", "utf-8", "");
		}
		else //if( source.equalsIgnoreCase("web") )
		{
			_webView.loadUrl(content);
		}
	}

	private void initUI() {
		this.setTitle("");
		
		getLeftButton().setText("返回");
		getLeftButton().setOnClickListener(this);
		getLeftButton().setVisibility(View.VISIBLE);
		
		
		_webView = (WebView) findViewById(R.id.webView);
		
		initWebView();
	}
	
	
	@SuppressLint("SetJavaScriptEnabled")
	private void initWebView() {

		_webView.getSettings().setJavaScriptEnabled(true);
		_webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
		_webView.setWebChromeClient(new WebChromeClient());

		_webView.setWebViewClient(new WebViewClient() {
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				view.loadUrl(url);
				return true;
			}

			@Override
			public void onPageFinished(WebView view, String url) {
				_webView.setVisibility(View.VISIBLE);
			}
		});
	}
	
	
	
	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.btn_left:
			this.finishThisActivity();
			break;
		default:
			break;
		}

	}

	
}
