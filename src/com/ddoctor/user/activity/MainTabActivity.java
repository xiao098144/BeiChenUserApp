package com.ddoctor.user.activity;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.Window;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.beichen.user.R;
import com.ddoctor.application.MyApplication;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.fragment.AskDoctorFragment;
import com.ddoctor.user.fragment.BloodSugarFragment;
import com.ddoctor.user.fragment.KnowledgeLibFragment;
import com.ddoctor.user.fragment.MineFragment;
import com.ddoctor.user.fragment.ShopFragment;
import com.ddoctor.user.service.Connect2RongCloud;
import com.ddoctor.utils.MyUtils;
import com.rongcloud.RCProvider;
import com.umeng.analytics.MobclickAgent;
import com.zhuge.analysis.stat.ZhugeSDK;


public class MainTabActivity extends FragmentActivity implements
		RadioGroup.OnCheckedChangeListener {

	private List<Fragment> _fragmentList = new ArrayList<Fragment>();

	private int _currentTabIndex = -1;

	private static MainTabActivity _mainTabActivityInstance = null;

	private static long _firstExitTime = 0;

	public static MainTabActivity sharedInstance() {
		return _mainTabActivityInstance;
	}

	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onResume(MainTabActivity.this);
		ZhugeSDK.getInstance().init(getApplicationContext());
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPause(MainTabActivity.this);
	}
	
	public void onCreate(Bundle savedInstanceState) {
		_mainTabActivityInstance = this;
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);

		setContentView(R.layout.main_layout);

		// DataModule.removeAllTempImage(this);
		
		MyUtils.showLog("MainTabActivity onCreate");

		// 检测融云token 是否存在  不存在则向服务器 请求获取 token值
		if (TextUtils.isEmpty(DataModule.getInstance().getRYToken())) {
			MyUtils.showLog("无token  请求token  ");
			RCProvider.getInstance().getRCToken();
		}else {
			MyUtils.showLog("token 已存在  启动服务连接融云  ");
			Intent intent = new Intent();
			intent.setClass(MainTabActivity.this,Connect2RongCloud.class);
			startService(intent);
		}
		
		buildUI();
		// ClientUpgrade cu = new ClientUpgrade(this);
		// cu.startCheckVersion();
	}
	
	
	@Override
    public void onSaveInstanceState(Bundle savedInstanceState){
		// 不要调用父类的方法，父类会恢复之前的fragment，导致重复加载
//        super.onSaveInstanceState(savedInstanceState);
        
        MyUtils.showLog("MainTabActivity::onSaveInstanceState");
    }	
	

	private void buildUI() {
		
		
		_fragmentList.add(new BloodSugarFragment());
		_fragmentList.add(new AskDoctorFragment());
		
//		_fragmentList.add(new ChatGroupFragment());  // 屏蔽糖友圈
//		_fragmentList.add(new KnowledgeListFragment());
		_fragmentList.add(new KnowledgeLibFragment());
		
		_fragmentList.add(new ShopFragment());
		_fragmentList.add(new MineFragment());
//		_fragmentList.add(new TestFragment());

		RadioGroup rg = (RadioGroup) this.findViewById(R.id.tabs_rg);

		rg.setOnCheckedChangeListener(this);

		showTab(0);
	}

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		for (int i = 0; i < group.getChildCount(); i++) {
			if (group.getChildAt(i).getId() == checkedId) {
				showTab(i);
				break;
			}
		}
	}

	public void showTab(int tabIndex) {
		RadioGroup group = (RadioGroup) this.findViewById(R.id.tabs_rg);

		if (tabIndex < 0 || tabIndex >= group.getChildCount())
			return;

		if (_currentTabIndex == tabIndex)
			return;

		if (_currentTabIndex >= 0) {
			_fragmentList.get(_currentTabIndex).onPause();
		}

		FragmentTransaction ft = this.getSupportFragmentManager()
				.beginTransaction();

		for (int i = 0; i < group.getChildCount(); i++) {
			Fragment fg = _fragmentList.get(i);
			RadioButton tabItem = (RadioButton) group.getChildAt(i);

			if (i == tabIndex) {

				// if( i > _currentTabIndex )
				// {
				// ft.setCustomAnimations(R.anim.slide_left_in,
				// R.anim.slide_left_out);
				// }
				// else
				// {
				// ft.setCustomAnimations(R.anim.slide_right_in,
				// R.anim.slide_right_out);
				// }

				if (fg.isAdded()) {
					fg.onResume();
				} else {
					ft.add(R.id.realtabcontent, fg);
				}
				

				ft.show(fg);

				tabItem.setTextColor(Color.rgb(255, 34, 34));
			} else {
				ft.hide(fg);
				tabItem.setTextColor(Color.rgb(138, 138, 138));
			}
		}
		ft.commit();

		_currentTabIndex = tabIndex;

		RadioButton rb = (RadioButton) group.getChildAt(tabIndex);
		if (!rb.isChecked())
			rb.setChecked(true);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			long secondTime = System.currentTimeMillis();
			if (secondTime - _firstExitTime <= 2000) {
				MyApplication.exit(true);
				try {
					MobclickAgent.onKillProcess(this);
				} catch (Exception e) {
					// TODO: handle exception
				}
				System.exit(0);
				
				// this.finish();
			} else {
				Toast toast = Toast.makeText(this, "再按一次退出程序！",
						Toast.LENGTH_SHORT);
				// toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();

				_firstExitTime = secondTime;
			}
			return true;
		}

		return false;
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		ZhugeSDK.getInstance().flush(getApplicationContext());
	}
	
}
