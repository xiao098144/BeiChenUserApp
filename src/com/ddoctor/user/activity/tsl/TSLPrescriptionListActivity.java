package com.ddoctor.user.activity.tsl;

/**
 * 商城
 * 康
 */
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.component.PhotoAlbum.Bimp;
import com.ddoctor.component.PhotoAlbum.DDPhotoAlbumActivity;
import com.ddoctor.enums.RefreshAction;
import com.ddoctor.enums.RetError;
import com.ddoctor.user.activity.BaseActivity;
import com.ddoctor.user.adapter.BaseAdapter;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.task.FileUploadTask;
import com.ddoctor.user.task.TSLAddPrescriptionTask;
import com.ddoctor.user.task.TSLGetPrescriptionListTask;
import com.ddoctor.user.task.TaskPostCallBack;
import com.ddoctor.user.view.DDPullToRefreshView;
import com.ddoctor.user.view.DDPullToRefreshView.OnHeaderRefreshListener;
import com.ddoctor.user.wapi.bean.ProductBean;
import com.ddoctor.user.wapi.bean.TslPrescriptionBean;
import com.ddoctor.user.wapi.bean.UploadBean;
import com.ddoctor.user.wapi.constant.TslStatusType;
import com.ddoctor.user.wapi.constant.Upload;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.DialogUtil;
import com.ddoctor.utils.DialogUtil.ConfirmDialog;
import com.ddoctor.utils.ImageLoaderUtil;
import com.ddoctor.utils.MyUtils;
import com.ddoctor.utils.ToastUtil;
import com.umeng.analytics.MobclickAgent;
import com.zhuge.analysis.stat.ZhugeSDK;

public class TSLPrescriptionListActivity extends BaseActivity implements
		OnHeaderRefreshListener, OnScrollListener, OnItemClickListener {
	RelativeLayout rl;

	private ListView _listView;
	DDPullToRefreshView _refreshViewContainer;
	private View _getMoreView;

	private List<TslPrescriptionBean> _dataList = new ArrayList<TslPrescriptionBean>();
	/**
	 * 商品列表适配器
	 */
	private ListAdapter _adapter;

	private int _pageNum = 1;
	private RefreshAction _refreshAction = RefreshAction.PULLTOREFRESH;
	
	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("TSLPrescriptionListActivity");
		MobclickAgent.onResume(TSLPrescriptionListActivity.this);
		ZhugeSDK.getInstance().init(getApplicationContext());
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("TSLPrescriptionListActivity");
		MobclickAgent.onPause(TSLPrescriptionListActivity.this);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tsl_prescription_list);
		
		initUI();
		
		loadingData(true, _pageNum);
	}

	private void initUI() {
		this.setTitle("我的处方");
		
		rl = (RelativeLayout) findViewById(R.id.titleBar);
		if (rl != null)
			rl.setBackgroundColor(getResources().getColor(R.color.default_titlebar));
		
		getLeftButton().setText("返回");
		getLeftButton().setOnClickListener(this);
		getLeftButton().setVisibility(View.VISIBLE);
		
		getRightButton().setText("增加处方");
		getRightButton().setOnClickListener(this);
		getRightButton().setVisibility(View.VISIBLE);
		
		
		_refreshViewContainer = (DDPullToRefreshView) findViewById(R.id.refreshViewContainer);
		_refreshViewContainer.setOnHeaderRefreshListener(this);
		_refreshViewContainer.setVisibility(View.INVISIBLE);
		
		_listView = (ListView) findViewById(R.id.listView);
		_listView.setOnItemClickListener(this);
		_listView.setOnScrollListener(this);
		
		
		initList();
	}
	
	
	private String loadingStr;

	private void initList() {
		// 获取更多
		_getMoreView = 	createGetMoreView();
		setGetMoreContent("已全部加载", false, false);
		_listView.addFooterView(_getMoreView);

		// 数据
		_adapter = new ListAdapter(this);
		_listView.setAdapter(_adapter);
	}
	
	// 加载更多相关函数 >>>>>>
	boolean _bGetMoreEnable = false;
	private View createGetMoreView()
	{
		if( _getMoreView != null )
			return _getMoreView;
		
		View v = (View) getLayoutInflater().inflate(R.layout.refresh_footer, null);
		
		return v;
	}
	
	private void setGetMoreContent(String message, boolean showImage, boolean animation)
	{
		TextView tv = (TextView)_getMoreView.findViewById(R.id.pull_to_load_text);
		tv.setText(message);
		
		ImageView imgView = (ImageView)_getMoreView.findViewById(R.id.pull_to_load_image);
		AnimationDrawable ad = (AnimationDrawable) imgView.getBackground();
		if( showImage )
		{
			if( animation )
			{
				ad.start();
			}
			else
			{
				ad.stop();
				ad.selectDrawable(0);
			}
				
			imgView.setVisibility(View.VISIBLE);
		}
		else
		{
			ad.stop();
			ad.selectDrawable(0);
	
			imgView.setVisibility(View.GONE);
		}
	}
	
	// <<<<< 获取更多相关函数
	

	private Dialog _loadingDialog = null;

	private void loadingData(boolean showLoading, int page)
	{
		if( showLoading )
		{
			_loadingDialog = DialogUtil.createLoadingDialog(this, "加载中...");
			_loadingDialog.show();
		}
		
		TSLGetPrescriptionListTask task = new TSLGetPrescriptionListTask(page);
		
		final int page1 = page;
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {
			
			@Override
			public void taskFinish(DDResult result) {
				if (result.getError() == RetError.NONE  )
				{
					List<TslPrescriptionBean> tmpList = (List<TslPrescriptionBean>)result.getObject();
					if( page1 > 1 ) // 加载更多
					{
						_dataList.addAll(tmpList);
						
						_adapter.notifyDataSetChanged();
					}
					else
					{
						// 加载第一页
						_dataList.clear();
						_dataList.addAll(tmpList);
						
						// for test
//						if( _dataList.size() > 0 )
//							_dataList.get(0).setTslPrescriptionState(TslStatusType.PRESCRIPTION_AUDIT_FAIL);
//						if( _dataList.size() > 1 )
//							_dataList.get(1).setTslPrescriptionState(TslStatusType.PRESCRIPTION_AUDIT_SUCCESS);
						
						_adapter.notifyDataSetChanged();
						
						_refreshViewContainer.onHeaderRefreshComplete();
						_refreshViewContainer.setVisibility(View.VISIBLE);
						_refreshAction = RefreshAction.NONE;
						
						if( _loadingDialog != null )
							_loadingDialog.dismiss();
						
					}

					// 是否显示"加载更多"
					
					setGetMoreContent("已全部加载", false, false);
					_bGetMoreEnable = false;
					
//					if( tmpList.size() > 0 )
//					{
//						setGetMoreContent("滑动加载更多", true, false);
//						_bGetMoreEnable = true;
//					}
//					else
//					{
//						// 没数据了，加载完成
//						setGetMoreContent("已全部加载", false, false);
//						_bGetMoreEnable = false;
//					}
					
					// 确保加载成功后，再修改这个变量
					_pageNum = page1;
				}
				else 
				{// 加载失败
					if( page1 > 1 )
					{
						setGetMoreContent("滑动加载更多", true, false);
					}
					else
					{
						_refreshViewContainer.onHeaderRefreshComplete();
						_refreshAction = RefreshAction.NONE;
						
						if( _loadingDialog != null )
							_loadingDialog.dismiss();
					}

					ToastUtil.showToast(result.getErrorMessage());
				}
				
				_refreshAction = RefreshAction.NONE;
			}
		});
		
		task.executeParallel("");
	}
	
	

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.btn_left:
			this.finishThisActivity();
			break;
		case R.id.btn_right:
			on_btn_add();
			break;
		default:
			break;
		}

	}

	@Override
	public void onScroll(AbsListView arg0, int firstVisibleItem, int arg2,
			int arg3) {
		
		// 工具栏的显示与隐藏		
		if( _refreshAction == RefreshAction.NONE )
		{
			if( _bGetMoreEnable )
			{// 有加载更多
				int lastPos = _listView.getLastVisiblePosition();
				int total = _listView.getHeaderViewsCount() + _dataList.size() + _listView.getFooterViewsCount();
				if( lastPos == total - 1 )
				{
					// 加载更多显示出来了，开始加载
					_refreshAction = RefreshAction.LOADMORE;
					setGetMoreContent("正在加载...", true, true);
					loadingData(false, _pageNum + 1);
				}
			}
		}
		

	}

	@Override
	public void onScrollStateChanged(AbsListView arg0, int arg1) {

	}


	@Override
	public void onHeaderRefresh(DDPullToRefreshView view) {
		if( _refreshAction == RefreshAction.NONE )
		{
			_refreshAction = RefreshAction.PULLTOREFRESH;
			loadingData(false, 1);
		}
		else
		{
			// 正在加载，什么也不做
			view.onHeaderRefreshComplete();
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

		// 进去商品详情页
		int dataIdx = arg2 - _listView.getHeaderViewsCount();
		
		MyUtils.showLog("" + dataIdx);
		
		if( dataIdx >= _dataList.size() )
			return;
		
		TslPrescriptionBean db = _dataList.get(dataIdx);
		
		if( db.getTslPrescriptionState() == TslStatusType.PRESCRIPTION_AUDIT_ING )
		{
			ToastUtil.showToast("正在审核中，请耐心等候...");
			return;
		}
		else if( db.getTslPrescriptionState() == TslStatusType.PRESCRIPTION_AUDIT_FAIL )
		{
			String msg1 = db.getTslPrescriptionMsg();
			String msg = String.format(Locale.CHINESE, "您的处方审核未通过，原因如下：\n\n%s\n\n请修改后重新提交!", msg1);
			// 提交成功
			DialogUtil.confirmDialog(this, msg, "确定",null, new ConfirmDialog(){

				@Override
				public void onOKClick(Bundle data) {
				}

				@Override
				public void onCancleClick() {
				}
				
			}).show();
			
			return;
		}
		
		Intent intent = new Intent(this, TSLPrescriptionDetailActivity.class);
		Bundle b = new Bundle();
		b.putSerializable("data", db);
		intent.putExtras(b);
		startActivity(intent);
	}
	
	public class ListAdapter extends BaseAdapter<ProductBean> {

		public ListAdapter(Context context) {
			super(context);
			
		}

		@Override
		public int getCount() {
			return _dataList.size();
		}
		

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {

			ValueHolder valueHolder = null;
			if (null == convertView) {
				convertView = inflater.inflate(R.layout.layout_tsl_prescription_list_item,parent, false);
				valueHolder = new ValueHolder();
				valueHolder.imgView = (ImageView)convertView.findViewById(R.id.imageView);
				valueHolder.imgView.setScaleType(ScaleType.CENTER_CROP);
				valueHolder.tv_name = (TextView) convertView.findViewById(R.id.tv_title);
				valueHolder.tv_time = (TextView) convertView.findViewById(R.id.tv_time);
				valueHolder.btn_status = (Button) convertView.findViewById(R.id.btn_status);
				
				float w = valueHolder.btn_status.getPaint().measureText("审核成功");
				RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams)valueHolder.btn_status.getLayoutParams();
				w += MyUtils.dp2px(20, _context);
				rlp.width = (int)w;
				
				convertView.setTag(valueHolder);
				
			} else {
				valueHolder = (ValueHolder) convertView.getTag();
			}
			
			TslPrescriptionBean dataBean = _dataList.get(position);
			
			valueHolder.dataIndex = position;
			
			valueHolder.imgView.setImageDrawable(null);
			
			if( dataBean.getName() == null )
				valueHolder.tv_name.setText("处方单已上传");
			else
				valueHolder.tv_name.setText(dataBean.getName());
			
			if( dataBean.getCreateTime() == null )
				valueHolder.tv_time.setText("");
			else
				valueHolder.tv_time.setText(dataBean.getCreateTime());
			
			int paddingLeft = MyUtils.dp2px(10, _context);
			int paddingRight = paddingLeft;
			
			if( dataBean.getTslPrescriptionState() == TslStatusType.PRESCRIPTION_AUDIT_ING )
			{
				valueHolder.btn_status.setText("审核中");
				valueHolder.btn_status.setBackgroundResource(R.drawable.bg_btn_blue_90);
//				valueHolder.btn_status.setPadding(paddingLeft, 0, paddingRight, 0);
			}
			else if(dataBean.getTslPrescriptionState() == TslStatusType.PRESCRIPTION_AUDIT_FAIL )
			{
				valueHolder.btn_status.setText("审核失败");
				valueHolder.btn_status.setBackgroundResource(R.drawable.bg_btn_red_90);
//				valueHolder.btn_status.setPadding(paddingLeft, 0, paddingRight, 0);
			}
			else if(dataBean.getTslPrescriptionState() == TslStatusType.PRESCRIPTION_AUDIT_SUCCESS )
			{
				valueHolder.btn_status.setText("审核通过");
				valueHolder.btn_status.setBackgroundResource(R.drawable.bg_btn_green_90);
//				valueHolder.btn_status.setPadding(paddingLeft, 0, paddingRight, 0);
			}
			
			ImageLoaderUtil.display(dataBean.getPrescriptionImage(), valueHolder.imgView, 0);
			
			return convertView;
		}
		
		private class ValueHolder {
			private ImageView imgView;
			private TextView tv_name;
			private TextView tv_time;			
			private Button btn_status;
			
			private int dataIndex;
		}

	}
	
	private void on_btn_add()
	{
		this.show_source_dialog();
	}
	
	
	private void show_photoalbum(){
		Intent intent = new Intent(this, DDPhotoAlbumActivity.class);
//		startActivity(intent);
		startActivityForResult(intent, 1000);
	}
	
	private void show_source_dialog()
	{
		String[] items = new String[] { "本地图片", "拍照" };
		
		DialogUtil.createListDialog(this, items, new DialogUtil.ListDialogCallback(){

			@Override
			public void onItemClick(int which) {
				if( which == 0 )
				{
					show_photoalbum(); // 支持多选
					
					// 单选
//					Intent intent = new Intent();
//					intent.setType("image/*");
//					intent.setAction(Intent.ACTION_GET_CONTENT);
//					startActivityForResult(intent, 0);
				}
				else
				{
					Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//					intent.putExtra(MediaStore.EXTRA_OUTPUT,  Uri.fromFile(new File(getTempPhotoFilename())));
					
					String filename = DataModule.getTakePhotoTempFilename("photo");
					File f = new File(filename);
					if( f.exists())
						f.delete();
					Uri uri = Uri.fromFile(f);
					intent.putExtra(MediaStore.EXTRA_OUTPUT,  uri);

					startActivityForResult(intent, 1);
				}
			}
			
		}).setTitle("选择处方单照片").show();
		
	}
	
	@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		
		super.onActivityResult(requestCode, resultCode, data);
		
		if( resultCode != RESULT_CANCELED )
		{
			if( requestCode == 0 )
			{// 选择图片
				if( data != null )
				{
					startPhotoCrop(data.getData());
				}
			}
			else if( requestCode == 1 )
			{// 拍照
				if( data != null )
				{
					// 
				}
				else
				{
					String filename = DataModule.getTakePhotoTempFilename("photo");
					File f = new File(filename);
					if( f.exists() )
					{
						startPhotoCrop(Uri.fromFile(f));
					}
					else
						ToastUtil.showToast("获取图片失败!");
					
				}
			}
			else if( requestCode == 2 )
			{
				// 剪裁
				if( data != null )
				{
					Bundle b = data.getExtras();
					if( b != null )
					{
						Bitmap bmp = b.getParcelable("data");
						if( bmp != null ){
							onUploadPhoto(bmp);
						}
					}
				}
			}
			else if( requestCode == 1000 )
			{
				// 多选
				ArrayList<String> imgList = data.getStringArrayListExtra("list");
				if( imgList != null && imgList.size() > 0 )
				{
					// 上传多个图片
					doUploadImageList(imgList);
				}
			}
		}
	}
	
	public void startPhotoCrop(Uri uri) {
		
		MyUtils.showLog("startPhotoCrop");

		boolean useCrop = false;
		
		if( !useCrop )
		{
		  	String filename = MyUtils.getImageContentFilePathFromUri(this,  uri);
		  	if( filename == null )
		  		filename = uri.getPath();
		  	
			MyUtils.showLog(filename);
			
			Bitmap bmp = MyUtils.loadBitmapFromFile(filename);
			if( bmp == null )
			{
				ToastUtil.showToast("错误!");
				return;
			}
			
			Bitmap new_bmp = MyUtils.resizeImage(bmp, 800);
//			bmp.recycle();
//			bmp = null;
			
			onUploadPhoto(new_bmp);
		}
		else
		{
			// 不剪裁
	        Intent intent = new Intent("com.android.camera.action.CROP");
	        intent.setDataAndType(uri, "image/*");
	        // 设置裁剪
	        intent.putExtra("crop", "true");
	        // aspectX aspectY 是宽高的比例
	        intent.putExtra("aspectX", 1);
	        intent.putExtra("aspectY", 1);
	        // outputX outputY 是裁剪图片宽高
	        intent.putExtra("outputX", 320);
	        intent.putExtra("outputY", 320);
	        intent.putExtra("return-data", true);
	        startActivityForResult(intent, 2);
		}
	}
	
	
	private List<String> _uploadImageList = null;
	private int _uploadIndex = -1;
	private int _uploadType = 0;
	
	public void doUploadImageList(List<String> imgList)
	{
		_uploadImageList = imgList;
		_uploadIndex = 0;
		_uploadType = 1;
		
		_loadingDialog = DialogUtil.createLoadingDialog(this, "提交中...", true);
		_loadingDialog.show();
		
		
		doUploadImages();
	}
	
	private void doUploadImagesDone()
	{
		_loadingDialog.dismiss();
		_loadingDialog = null;
		
		ToastUtil.showToast("上传图片完成！");
	}
	
	private void uploadNext()
	{
		if( _uploadIndex < _uploadImageList.size() - 1 )
		{
			_uploadIndex++;
			doUploadImages();
		}
		else
		{
			doUploadImagesDone();
		}
	}
	
	private void uploadFailed(int index){
		String s = String.format(Locale.CHINESE, "第%d张图片上传失败!", index + 1);
		ToastUtil.showToast(s);
	}
	
	private void doUploadImages()
	{
		MyUtils.showLog("upload index " +  _uploadIndex + " ...");
		
		UploadBean uploadBean = new UploadBean();
		uploadBean.setFileType("jpg");
		uploadBean.setType(Upload.PRESCRIPTION_IMAGE);
		
		Bitmap bmp = null;
		try{
			bmp = Bimp.revitionImageSize(_uploadImageList.get(_uploadIndex));
		}catch(Exception e){
			e.printStackTrace();
			uploadFailed(_uploadIndex);
			uploadNext();
			return;
		}
		
		byte[] data = MyUtils.Bitmap2Bytes(bmp);
		String s_data = android.util.Base64.encodeToString(data, Base64.DEFAULT);
		uploadBean.setFile(s_data);
		
		FileUploadTask task = new FileUploadTask(uploadBean);
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {
			@Override
			public void taskFinish(DDResult result) {

				if( result.getError() == RetError.NONE )
				{
					on_upload_task_finished(result);
				}
				else
				{
					on_upload_task_failed(result);
				}

			}
		});
		task.executeParallel("");
	}
	
	
	private Bitmap _bitmap = null;
	private void onUploadPhoto(Bitmap bmp)
	{
		MyUtils.showLog("onSetPhoto:.....");
		// TODO: 上传头像...
		
//		if( _bitmap != null )
//		{
//			_bitmap.recycle();
//			_bitmap = null;
//		}
		_bitmap = bmp;
		
		_loadingDialog = DialogUtil.createLoadingDialog(this, "提交中...", true);
		_loadingDialog.show();
		
		UploadBean uploadBean = new UploadBean();
		uploadBean.setFileType("jpg");
		uploadBean.setType(Upload.PRESCRIPTION_IMAGE);
		
		byte[] data = MyUtils.Bitmap2Bytes(bmp);
		String s_data = android.util.Base64.encodeToString(data, Base64.DEFAULT);
		uploadBean.setFile(s_data);
		
		FileUploadTask task = new FileUploadTask(uploadBean);
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {
			@Override
			public void taskFinish(DDResult result) {

				if( result.getError() == RetError.NONE )
				{
					on_upload_task_finished(result);
				}
				else
				{
					on_upload_task_failed(result);
				}

			}
		});
		task.executeParallel("");
	}
	
	private void on_upload_task_finished(DDResult result)
	{
		MyUtils.showLog("on_task_finished");
		Bundle b = result.getBundle();
		String fileUrl = b.getString("fileUrl");
		
		MyUtils.showLog(fileUrl);
		
		// 继续调用处方上传接口
		doSubmitPrescription(fileUrl);
	}
	
	private void on_upload_task_failed(DDResult result)
	{
		if( _uploadType == 1 )
		{
			uploadFailed(_uploadIndex);
			uploadNext();
			return;
		}
		
		if( _loadingDialog != null )
		{
			_loadingDialog.dismiss();
			_loadingDialog = null;
		}
		
		ToastUtil.showToast(result.getErrorMessage());
	}
	
	
	
	private void doSubmitPrescription(String imageUrl)
	{
		TslPrescriptionBean bean = new TslPrescriptionBean();
		bean.setTysPatientId(DataModule.getInstance().getLoginedUserId());
		bean.setPrescriptionImage(imageUrl);
		
		TSLAddPrescriptionTask task = new TSLAddPrescriptionTask(bean);
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {
			@Override
			public void taskFinish(DDResult result) {

				if( _uploadType == 0 )
				{
				if( _loadingDialog != null )
				{
					_loadingDialog.dismiss();
					_loadingDialog = null;
				}
				}

				if( result.getError() == RetError.NONE )
				{
					on_submit_task_finished(result);
				}
				else
				{
					on_submit_task_failed(result);
				}
			}
		});
		task.executeParallel("");
	}
	
	
	private void on_submit_task_finished(DDResult result)
	{
		// 提交成功
		TslPrescriptionBean bean = (TslPrescriptionBean)result.getObject();
		
		// 加入到列表中
		_dataList.add(0, bean);
		_adapter.notifyDataSetChanged();
		
		if( _uploadType == 1 )
		{
			uploadNext();
			return;
		}
		
		// 提示等待审核...
		// 提交成功
		DialogUtil.confirmDialog(this, "您的处方已经提交成功，请等待审核！", "确定", null, new ConfirmDialog(){

			@Override
			public void onOKClick(Bundle data) {
				
			}

			@Override
			public void onCancleClick() {
			}
			
		}).show();
	}
	
	private void on_submit_task_failed(DDResult result)
	{
		if( _uploadType == 1 )
		{
			uploadFailed(_uploadIndex);
			uploadNext();
			return;
		}
		ToastUtil.showToast(result.getErrorMessage());
	}
}
