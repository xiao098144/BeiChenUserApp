package com.ddoctor.user.activity.tsl;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.component.BloodSugarPopupWindow;
import com.ddoctor.enums.RetError;
import com.ddoctor.user.activity.BaseActivity;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.data.MyProfile;
import com.ddoctor.user.task.TSLGetOrderAllProductListTask;
import com.ddoctor.user.task.TaskPostCallBack;
import com.ddoctor.user.wapi.bean.TslAddressinfoBean;
import com.ddoctor.user.wapi.bean.TslPatientBean;
import com.ddoctor.user.wapi.bean.TslProductBean;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.DialogUtil;
import com.ddoctor.utils.MyUtils;
import com.ddoctor.utils.ToastUtil;
import com.umeng.analytics.MobclickAgent;
import com.zhuge.analysis.stat.ZhugeSDK;

public class TSLOrderBuyActivity extends BaseActivity {

	private ListView _listView = null;
	private DataListAdapter _adapter = null;
	
	private ArrayList<TslProductBean> _dataList  = new ArrayList<TslProductBean>();
	private ArrayList<TslAddressinfoBean> _addressList  = new ArrayList<TslAddressinfoBean>();
	
	private Button _btnStamp;
	private TextView _addressTextView;
	
	private TextView _addressOptionTextView;
	
	private TslAddressinfoBean _addressBean = null;
	
	private TextView _bloodsugar1TextView = null;
	private TextView _bloodsugar2TextView = null;
	
	private float _bloodsugar1 = 0;
	private float _bloodsugar2 = 0;
	
	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("TSLOrderBuyActivity");
		MobclickAgent.onResume(this);
		ZhugeSDK.getInstance().init(getApplicationContext());
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("TSLOrderBuyActivity");
		MobclickAgent.onPause(this);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tsl_order_buy);
		
		
		initUI();
		
		this.updateBloodSugarUI();
		
		doLoadData(true);
	}
	
//	
//	private void makeTestData()
//	{
//		for(int i = 0; i < 10; i++ )
//		{
//			TslProductBean pb = new TslProductBean();
//			pb.setName("阿司匹林" + i);
//			pb.setYongliang("6片/天");
//			pb.setFactory("天鹤药业");
//			pb.setPrice(12.0f);
//			
//			_dataList.add(pb);
//		}
//	}
//	
//	private void makeAddressTestData()
//	{
//		for(int i = 0; i < 10; i++ )
//		{
//			TslAddressinfoBean pb = new TslAddressinfoBean();
//			pb.setName("张山");
//			pb.setDetail("天津市北辰区 北京路小武基小区2栋1203天津市北辰区 北京路小武基小区2栋1203天津市北辰区 北京路小武基小区2栋1203");
//			pb.setPhone("1380013800" + i);
//			
//			_addressList.add(pb);
//		}
//	}
	

	protected void initUI() {
		this.setTitle("购药");
		Button leftButton = this.getLeftButton();
		leftButton.setText("返回");
		leftButton.setOnClickListener(this);
		leftButton.setVisibility(View.VISIBLE);
		
		Button rightButton = this.getRightButton();
		rightButton.setText("下一步");
		rightButton.setVisibility(View.VISIBLE);
		rightButton.setOnClickListener(this);
		
		
		_listView = (ListView)findViewById(R.id.listView);
		_listView.setVisibility(View.INVISIBLE);
		
		LinearLayout ll = (LinearLayout)LayoutInflater.from(this).inflate(R.layout.layout_tsl_buy_header, null);
		_btnStamp = (Button)ll.findViewById(R.id.btn_stamp);
		_btnStamp.setOnClickListener(this);
		_btnStamp.setSelected(true);
		
		RelativeLayout rl = (RelativeLayout)ll.findViewById(R.id.rel_address_title);
		rl.setOnClickListener(this);
		rl = (RelativeLayout)ll.findViewById(R.id.rel_address);
		rl.setOnClickListener(this);

		
		_addressTextView = (TextView)ll.findViewById(R.id.tv_address);
		_addressOptionTextView = (TextView)ll.findViewById(R.id.tv_address_option);
		
		
		
		_bloodsugar1TextView = (TextView)ll.findViewById(R.id.btn_tab1);
		_bloodsugar2TextView = (TextView)ll.findViewById(R.id.btn_tab2);
		_bloodsugar1TextView.setOnClickListener(this);
		_bloodsugar2TextView.setOnClickListener(this);
		
		int w = MyUtils.getScreenWidth(this)/2;
		LinearLayout.LayoutParams llp = (LinearLayout.LayoutParams)_bloodsugar1TextView.getLayoutParams();
		llp.width = w;
		
		llp = (LinearLayout.LayoutParams)_bloodsugar2TextView.getLayoutParams();
		llp.width = w;
		
		_listView.addHeaderView(ll);
		
		_adapter = new DataListAdapter(this);
		
		_listView.setAdapter(_adapter);
	}
	
	
	private Dialog _loadingDialog = null;

	private void doLoadData(boolean showLoading)
	{
		if( showLoading )
		{
			_loadingDialog = DialogUtil.createLoadingDialog(this, "加载中...");
			_loadingDialog.show();
		}
		
		TSLGetOrderAllProductListTask task = new TSLGetOrderAllProductListTask();
		
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {
			
			@Override
			public void taskFinish(DDResult result) {
				if (result.getError() == RetError.NONE  )
				{
					List<TslProductBean> tmpDataList = (List<TslProductBean>)result.getObject();
					List<TslAddressinfoBean> tmpAddressList = (List<TslAddressinfoBean>)result.getObject1();

					// 加载第一页
					_dataList.clear();
					_dataList.addAll(tmpDataList);
					_addressList.clear();
					_addressList.addAll(tmpAddressList);
					
//					// for test
//					if( _dataList.size() == 0 )
//						makeTestData();
////					if( _addressList.size() == 0 )
//						makeAddressTestData();
					
					if( _addressList.size() > 0 )
					{
						_addressBean = _addressList.get(0);
						TSLOrderBuyActivity.this.updateAddressInfo(_addressBean);
					}
						
					_adapter.notifyDataSetChanged();
					
					_listView.setVisibility(View.VISIBLE);
						
					if( _loadingDialog != null )
						_loadingDialog.dismiss();
				}
				else 
				{// 加载失败
						if( _loadingDialog != null )
							_loadingDialog.dismiss();

						ToastUtil.showToast(result.getErrorMessage());
				}
			}
		});
		
		task.executeParallel("");
	}
	
	
	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.btn_left:
			finishThisActivity();
			break;
		case R.id.btn_right:
			on_btn_next();
			break;
			
		case R.id.btn_stamp:
			_btnStamp.setSelected(!_btnStamp.isSelected());
			break;
		case R.id.btn_tab1:
			on_btn_bloodsugar(1);
			break;
		case R.id.btn_tab2:
			on_btn_bloodsugar(2);
			break;
		case R.id.rel_address:
		case R.id.rel_address_title:
			on_btn_address();
			break;
		default:
			break;
		}
	}
	
	
	private void on_btn_next()
	{
		// 检查数据完整性...
		if( _addressBean == null )
		{
			ToastUtil.showToast("请输入收货地址！");
			return;
		}
		
		if( _bloodsugar1 == 0 )
		{
			ToastUtil.showToast("请输入餐前血糖值!");
			return;
		}
		
		if( _bloodsugar2 == 0 )
		{
			ToastUtil.showToast("请输入餐后血糖值!");
			return;
		}
		

		// 药品数量是否为0
		// 1_中药 2_西药
		int cnt = 0;
		int productCnt = 0;
		int zyCnt = 0;
		int xyCnt = 0;
		for(int i = 0; i < _dataList.size(); i++ )
		{
			TslProductBean bean = _dataList.get(i);
			cnt += bean.getBuyNum();
			if( bean.getBuyNum() > 0 )
			{
				productCnt++;
			
				if( bean.getType().equalsIgnoreCase("1") )
					zyCnt++;
				else
	//			if( bean.getType().equalsIgnoreCase("1") )
					xyCnt++;
			}
		}
		
		if( cnt == 0 )
		{
			ToastUtil.showToast("请选择要购买的药品用药天数！");
			return;
		}
		
		if( zyCnt > 2 )
		{
			ToastUtil.showToast("中药最多购买2种！");
			return;
		}
		
		if( xyCnt > 4 )
		{
			ToastUtil.showToast("西药最多购买4种！");
			return;
		}
		
		int stamp = 0;
		if( _btnStamp.isSelected() )
			stamp = 1;
		
		
		DataModule.getInstance().activityMap.put("TSLOrderBuyActivity", this);
		
		Intent intent = new Intent(this, TSLOrderBuyConfirmActivity.class);
		Bundle b = new Bundle();
		b.putSerializable("address", _addressBean);
		b.putSerializable("productList", _dataList);
		b.putInt("stamp", stamp);
		b.putFloat("bloodsugar1", _bloodsugar1);
		b.putFloat("bloodsugar2", _bloodsugar2);
		intent.putExtras(b);
		
		startActivity(intent);
	}
	
	private void on_btn_address()
	{
		Intent intent = new Intent(this, TSLOrderAddressActivity.class);
		Bundle b = new Bundle();
		b.putSerializable("data", this._addressBean);
		b.putSerializable("addressList", _addressList);
		intent.putExtras(b);
		startActivityForResult(intent, 1);
		
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		super.onActivityResult(requestCode, resultCode, data);
		
		if( requestCode == 1 )
		{
			if( resultCode == 100 && data != null && data.getExtras() != null )
			{
				TslAddressinfoBean bean = (TslAddressinfoBean)data.getExtras().getSerializable("data");
				updateAddressInfo(bean);
			}
		}
	}
	
	private void updateAddressInfo(TslAddressinfoBean bean)
	{
		_addressBean = bean;
		
		
		if( _addressBean != null )
		{
			_addressOptionTextView.setText("修改");
			
			TslPatientBean pb = MyProfile.getTslUserInfo();
			
			String address;
			if( pb != null )
				address = String.format(Locale.CHINESE, "%s %s", pb.getTslPhone1(), _addressBean.getDetail());
			else
				address = String.format(Locale.CHINESE, "%s", _addressBean.getDetail());
				
			_addressTextView.setText(address);
			
		}
		else
		{
			_addressOptionTextView.setText("请选择");
			_addressTextView.setText("");
		}
	}
	
	
	private final static int MAX_BUY_DAYS = 30;
	
	
	public void updateBuyNumTips(TextView tv, int n, int dataIndex)
	{
		if( n >= MAX_BUY_DAYS )
			tv.setText("已达到购买限制");
		else
			tv.setText("选择购买的天数");
		
		TslProductBean b = _dataList.get(dataIndex);
		b.setBuyNum(n);
		b.setMaxDay(n);
	}
	
	
	public class DataListAdapter extends BaseAdapter {

		private Context _ctx = null;
		public DataListAdapter(Context ctx) {
			super();
			_ctx = ctx;
		}

		@Override
		public int getCount() {
			return _dataList.size();
		}
		
		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			
			MyUtils.showLog("index=" + position);
			
			ViewHolder vh = null;
			if (null == convertView) {
				convertView = LayoutInflater.from(_ctx).inflate(R.layout.layout_tsl_buy_item,
						parent, false);
				
				vh = new ViewHolder();
				vh.tvProductTitle = (TextView)convertView.findViewById(R.id.tv_title);
				vh.tvProductNum = (TextView)convertView.findViewById(R.id.tv_num);
				vh.tvProductCompany = (TextView)convertView.findViewById(R.id.tv_company);
				vh.tvProductPrice = (TextView)convertView.findViewById(R.id.tv_price);
				
				vh.btnDec = (ImageView)convertView.findViewById(R.id.btn_dec);
				vh.btnInc = (ImageView)convertView.findViewById(R.id.btn_inc);
				vh.tvNum = (TextView)convertView.findViewById(R.id.tv_buy_num);
				vh.tvTips = (TextView)convertView.findViewById(R.id.tv_buy_tips);
				
				// 0,7,14,15,16....30
				final ViewHolder holder = vh;
				vh.btnDec.setOnClickListener(new OnClickListener(){

					@Override
					public void onClick(View v) {
						int dataIndex = (Integer)holder.btnDec.getTag();
						int n = Integer.parseInt(holder.tvNum.getText().toString());
						if( n >= 15 )
							n--;
						else if( n == 14 )
							n = 7;
						else if( n == 7 )
							n = 0;
						holder.tvNum.setText(String.format(Locale.CHINESE, "%d", n));
						updateBuyNumTips(holder.tvTips, n, dataIndex);
					}
					
				});
				
				vh.btnInc.setOnClickListener(new OnClickListener(){

					@Override
					public void onClick(View v) {
						int dataIndex = (Integer)holder.btnInc.getTag();
						int n = Integer.parseInt(holder.tvNum.getText().toString());
						if( n >= 14 )
							n++;
						else if( n == 7 )
							n = 14;
						else if( n == 0 )
							n = 7;
						
						if( n >= MAX_BUY_DAYS )
							n = MAX_BUY_DAYS;
						
						holder.tvNum.setText(String.format(Locale.CHINESE, "%d", n));
						updateBuyNumTips(holder.tvTips, n, dataIndex);
					}
					
				});
				
				convertView.setTag(vh);
			}
			else
				vh = (ViewHolder)convertView.getTag();
			
			
			vh.dataIndex = position;
			vh.btnDec.setTag(position);
			vh.btnInc.setTag(position);
			
			TslProductBean productBean = _dataList.get(position);
			vh.tvProductTitle.setText(productBean.getName());
//			vh.tvProductNum.setText(productBean.getYongliang());
			vh.tvProductCompany.setText(productBean.getFactory());
			vh.tvProductPrice.setText(String.format(Locale.CHINESE, "售价：￥%.2f", productBean.getPrice()));
			
			
			int num = productBean.getBuyNum();
			
			vh.tvNum.setText("" + num);
			return convertView;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}
		
		class ViewHolder
		{
			int dataIndex;
			
			TextView tvProductTitle;
			TextView tvProductNum;
			TextView tvProductCompany;
			TextView tvProductPrice;
			
			ImageView btnDec;
			ImageView btnInc;
			TextView tvNum;
			TextView tvTips;
		}
		
	}
	
	private void updateBloodSugarUI()
	{
		if( _bloodsugar1 > 0 )
		{
			String s = String.format(Locale.CHINESE, "餐前血糖: %.1f", _bloodsugar1);
			_bloodsugar1TextView.setText(s);
		}
		else
		{
			_bloodsugar1TextView.setText("+餐前血糖");
		}
		
		if( _bloodsugar2 > 0 )
		{
			String s = String.format(Locale.CHINESE, "餐后血糖: %.1f", _bloodsugar2);
			_bloodsugar2TextView.setText(s);
		}
		else
		{
			_bloodsugar2TextView.setText("+餐后血糖");
		}
		
	}

	private void on_btn_bloodsugar(final int tabIndex)
	{
		RelativeLayout rootLayout = (RelativeLayout) findViewById(R.id.rootLayout);
		final BloodSugarPopupWindow pw = showBloodSugarDialog(rootLayout);
		pw.setOnBtnOkClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				float value = pw.getValue();
				if( tabIndex == 1 )
					_bloodsugar1 = value;
				else
					_bloodsugar2 = value;
				updateBloodSugarUI();
				
			}
			
		});
		
		if( tabIndex == 1 )
			pw.setValue(_bloodsugar1);
		else
			pw.setValue(_bloodsugar2);
	}
	
	
	public BloodSugarPopupWindow showBloodSugarDialog(View parent) {

		BloodSugarPopupWindow popupWindow = new BloodSugarPopupWindow(this);
		popupWindow.show(parent);
		return popupWindow;
	}
	
}
