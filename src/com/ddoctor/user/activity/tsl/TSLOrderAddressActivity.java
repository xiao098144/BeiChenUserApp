package com.ddoctor.user.activity.tsl;

import java.util.ArrayList;
import java.util.Locale;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.user.activity.BaseActivity;
import com.ddoctor.user.wapi.bean.TslAddressinfoBean;
import com.ddoctor.utils.MyUtils;
import com.ddoctor.utils.ToastUtil;
import com.umeng.analytics.MobclickAgent;
import com.zhuge.analysis.stat.ZhugeSDK;

public class TSLOrderAddressActivity extends BaseActivity implements OnItemClickListener {

	private ListView _listView = null;
	private DataListAdapter _adapter = null;
	
	private ArrayList<TslAddressinfoBean> _dataList  = null;//new ArrayList<TslAddressinfoBean>();
	private EditText _editText = null;
	
	private TslAddressinfoBean _addressBean = null;
	
	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("TSLOrderAddressActivity");
		MobclickAgent.onResume(this);
		ZhugeSDK.getInstance().init(getApplicationContext());
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("TSLOrderAddressActivity");
		MobclickAgent.onPause(this);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tsl_order_address);

		Bundle b = getIntent().getExtras();
		if( b != null )
		{
			_addressBean = (TslAddressinfoBean)b.getSerializable("data");
			_dataList = (ArrayList<TslAddressinfoBean>)b.getSerializable("addressList");
		}
		
		this.setResult(0, null);
		
		initUI();
	}
	

	protected void initUI() {
		this.setTitle("收货地址");
		Button leftButton = this.getLeftButton();
		leftButton.setText("返回");
		leftButton.setOnClickListener(this);
		leftButton.setVisibility(View.VISIBLE);
		
		Button rightButton = this.getRightButton();
		rightButton.setText("保存");
		rightButton.setOnClickListener(this);
		rightButton.setVisibility(View.VISIBLE);

		
		_listView = (ListView)findViewById(R.id.listView);
		_listView.setBackgroundColor(Color.argb(255, 245, 245, 245));
		
		
		LinearLayout ll = new LinearLayout(this);
		ll.setBackgroundColor(Color.WHITE);
		
		EditText et = new EditText(this);
		et.setHint("这里输入收货地址");
		et.setBackgroundDrawable(null);
		et.setGravity(Gravity.TOP);
		
		_editText = et;
		
		if(_addressBean != null )
			_editText.setText(_addressBean.getDetail());
		
		LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
				MyUtils.dp2px(100, this));
		ll.addView(et, llp);
		
		_listView.addHeaderView(ll);
		
		_adapter = new DataListAdapter(this);
		
		_listView.setAdapter(_adapter);
		_listView.setOnItemClickListener(this);
	}
	
	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.btn_left:
			finishThisActivity();
			break;
		case R.id.btn_right:
			on_btn_done();
			break;
		default:
			break;
		}
	}
	
	private void on_btn_done()
	{
		String address = _editText.getText().toString().trim();
		if( address.length() < 1  )
		{
			ToastUtil.showToast("请输入收货地址!");
			return;
		}

		TslAddressinfoBean bean = new TslAddressinfoBean();
		bean.setDetail(address);
		
		Intent intent = new Intent();
		Bundle b = new Bundle();
		b.putSerializable("data", bean);
		intent.putExtras(b);
		
		this.setResult(100, intent);
		finishThisActivity();
	}
	
	public class DataListAdapter extends BaseAdapter {

		private Context _ctx = null;
		public DataListAdapter(Context ctx) {
			super();
			_ctx = ctx;
		}

		@Override
		public int getCount() {
			if( _dataList == null )
				return 0;
			
			return _dataList.size();
		}
		
		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {

			if (null == convertView) {
				
				LinearLayout ll = new LinearLayout(_ctx);
				
				TextView tv = new TextView(_ctx);
				tv.setId(100);
				tv.setTextSize(16);
				
				LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(-1,-2);
				llp.leftMargin = MyUtils.dp2px(10, _ctx);
				llp.rightMargin = MyUtils.dp2px(10, _ctx);
//				if( position == 0 ){
//					llp.topMargin = MyUtils.dp2px(10, _ctx);
//					llp.bottomMargin = MyUtils.dp2px(10, _ctx);
//				}
//				else if( position == _dataList.size() - 1 )
//				{
//					llp.topMargin = MyUtils.dp2px(10, _ctx);
//					llp.bottomMargin = MyUtils.dp2px(10, _ctx);
//				}
//				else
				{
					llp.topMargin = MyUtils.dp2px(15, _ctx);
					llp.bottomMargin = MyUtils.dp2px(15, _ctx);
				}
				ll.addView(tv, llp);
				
				convertView = ll;
				
			}
			
			TslAddressinfoBean bean = _dataList.get(position);
			
			TextView tv = (TextView)convertView.findViewById(100);
			
			String address = String.format(Locale.CHINESE, "%s", bean.getDetail());
			tv.setText(address);
			
			return convertView;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}
		
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		
		int idx = arg2 - _listView.getHeaderViewsCount();
		TslAddressinfoBean bean = _dataList.get(idx);
		_editText.setText(bean.getDetail());
		
	}
	
}
