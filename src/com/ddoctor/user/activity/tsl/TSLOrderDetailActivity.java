package com.ddoctor.user.activity.tsl;

import java.util.ArrayList;
import java.util.List;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.enums.RetError;
import com.ddoctor.user.activity.BaseActivity;
import com.ddoctor.user.task.TSLGetOrderDetailTask;
import com.ddoctor.user.task.TaskPostCallBack;
import com.ddoctor.user.wapi.bean.TslOrderBean;
import com.ddoctor.user.wapi.bean.TslProductBean;
import com.ddoctor.user.wapi.constant.TslStatusType;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.DialogUtil;
import com.ddoctor.utils.MyUtils;
import com.ddoctor.utils.ToastUtil;
import com.umeng.analytics.MobclickAgent;
import com.zhuge.analysis.stat.ZhugeSDK;

public class TSLOrderDetailActivity extends BaseActivity {

	private ListView _listView = null;
	private DataListAdapter _adapter = null;
	
	private ArrayList<DataBean> _dataList  = new ArrayList<DataBean>();
	private ArrayList<TslOrderBean> _orderDataList  = new ArrayList<TslOrderBean>();
	
	private TextView _tv_orderId;
	private TextView _tv_orderStatus;
	private TextView _tv_name;
	private TextView _tv_phone;
	private TextView _tv_address;
	
	private TslOrderBean _orderBean = null;
	
	
	private final int ROW_TYPE_DIVIDER 	= 0;
	private final int ROW_TYPE_TIME 	= 1;
	private final int ROW_TYPE_ITEM 	= 2;
	private final int ROW_TYPE_SUMMARY 	= 3;
	private final int ROW_TYPE_COUNT 	= 4;
	
	
	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("TSLOrderDetailActivity");
		MobclickAgent.onResume(this);
		ZhugeSDK.getInstance().init(getApplicationContext());
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("TSLOrderDetailActivity");
		MobclickAgent.onPause(this);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tsl_order_detail);
		

		Bundle b = getIntent().getExtras();
		if( b == null || b.getSerializable("orderBean") == null )
		{
			ToastUtil.showToast("参数错误!");
			return;
		}
		
		_orderBean = (TslOrderBean)b.getSerializable("orderBean");
		
		initUI();
		
		
		doLoadData();
	}
	

	protected void initUI() {
		this.setTitle("订单详情");
		Button leftButton = this.getLeftButton();
		leftButton.setText("返回");
		leftButton.setOnClickListener(this);
		leftButton.setVisibility(View.VISIBLE);
		
		_listView = (ListView)findViewById(R.id.listView);
		_listView.setVisibility(View.INVISIBLE);
		
		LinearLayout ll = (LinearLayout)LayoutInflater.from(this).inflate(R.layout.layout_tsl_order_detail_header, null);
		
		float titleWidth = 0;
		TextView tvOrderId = (TextView)ll.findViewById(R.id.tv_orderid_title);
		float w = tvOrderId.getPaint().measureText(tvOrderId.getText().toString());
		if( w > titleWidth )
			titleWidth = w;
		
		TextView tvOrderStatus = (TextView)ll.findViewById(R.id.tv_order_status_title);
		w = tvOrderStatus.getPaint().measureText(tvOrderStatus.getText().toString());
		if( w > titleWidth )
			titleWidth = w;
		
		TextView tvUser = (TextView)ll.findViewById(R.id.tv_user_title);
		w = tvUser.getPaint().measureText(tvUser.getText().toString());
		if( w > titleWidth )
			titleWidth = w;
		
		TextView tvMobile = (TextView)ll.findViewById(R.id.tv_mobile_title);
		w = tvMobile.getPaint().measureText(tvMobile.getText().toString());
		if( w > titleWidth )
			titleWidth = w;
		
		TextView tvAddress = (TextView)ll.findViewById(R.id.tv_address_title);
		w = tvAddress.getPaint().measureText(tvAddress.getText().toString());
		if( w > titleWidth )
			titleWidth = w;
		
		tvOrderId.getLayoutParams().width = (int)titleWidth;
		tvOrderStatus.getLayoutParams().width = (int)titleWidth;
		tvUser.getLayoutParams().width = (int)titleWidth;
		tvMobile.getLayoutParams().width = (int)titleWidth;
		tvAddress.getLayoutParams().width = (int)titleWidth;

		
		_tv_orderId = (TextView)ll.findViewById(R.id.tv_orderid);
		_tv_orderStatus = (TextView)ll.findViewById(R.id.tv_order_status);
		_tv_name = (TextView)ll.findViewById(R.id.tv_user);
		_tv_phone = (TextView)ll.findViewById(R.id.tv_mobile);
		_tv_address = (TextView)ll.findViewById(R.id.tv_address);

		
		
		_tv_orderId.setText(_orderBean.getTslOrderNo());
		int status = _orderBean.getState();
		String statusName = TslStatusType.getOrderStatusName(status);
		_tv_orderStatus.setText(statusName);
		
		_tv_name.setText(_orderBean.getName());
		_tv_phone.setText(_orderBean.getPhone());
		_tv_address.setText(_orderBean.getAddress());
		
		_listView.addHeaderView(ll);
		
		_adapter = new DataListAdapter(this);
		
		_listView.setAdapter(_adapter);
	}
	
	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.btn_left:
			finishThisActivity();
			break;
		case R.id.btn_right:
			break;
		default:
			break;
		}
	}
	
	
	public class DataListAdapter extends BaseAdapter {

		private Context _ctx = null;
		public DataListAdapter(Context ctx) {
			super();
			_ctx = ctx;
		}

		@Override
		public int getCount() {
			return _dataList.size();
		}
		
		@Override
		public int getItemViewType(int position) {
			DataBean db = _dataList.get(position);
			return db.rowType;
		}
	
	
		@Override
		public int getViewTypeCount() {
			return ROW_TYPE_COUNT;
		}
		
		
		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			DataBean dataBean = _dataList.get(position);
			
			int type = getItemViewType(position);
			if( type == ROW_TYPE_ITEM )
			{
				TslOrderBean orderBean = _orderDataList.get(dataBean.orderIndex);

				ViewHolder vh = null;
				if (null == convertView) {
					convertView = LayoutInflater.from(_ctx).inflate(R.layout.layout_tsl_order_item,
							parent, false);
					vh = new ViewHolder();
					
					vh.tvTitle = (TextView)convertView.findViewById(R.id.tv_title);
					vh.tvBuyNum = (TextView)convertView.findViewById(R.id.tv_buy_num);
					
					
					convertView.setTag(vh);;
					
				} else {
					vh = (ViewHolder) convertView.getTag();
				}
				
				
				TslProductBean pb = orderBean.getProductList().get(dataBean.productIndex);
				
				vh.tvTitle.setText(pb.getName());
				
				vh.tvBuyNum.setText("" + pb.getMaxDay() + "天");
				
				
				return convertView;
			}
//			else if( type == ROW_TYPE_SUMMARY )
//			{
//				TslOrderBean orderBean = _orderDataList.get(dataBean.orderIndex);
//				if( convertView == null )
//				{
//					RelativeLayout.LayoutParams rlp;
//						
//					RelativeLayout rl = new RelativeLayout(_ctx);
//					rl.setBackgroundColor(0xFFF5F5F5);
//						
//					TextView tv;
//
//					tv = new TextView(_ctx);
//					tv.setId(100);
//					tv.setText("合计：");
//					tv.setTextColor(Color.argb(255, 102,102,102));
//					rlp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
//							RelativeLayout.LayoutParams.WRAP_CONTENT);
//					rlp.addRule(RelativeLayout.CENTER_VERTICAL);
//					rlp.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
//					rlp.topMargin = MyUtils.dp2px(10, getContext());
//					rlp.bottomMargin = MyUtils.dp2px(10, getContext());
//					rlp.leftMargin = MyUtils.dp2px(5, getContext());
//					rl.addView(tv, rlp);
//					
//					tv = new TextView(getContext());
//					tv.setId(200);
//					tv.getPaint().setFakeBoldText(true);
//					tv.setTextColor(Color.argb(255, 102,102,102));
//					rlp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
//							RelativeLayout.LayoutParams.WRAP_CONTENT);
//					rlp.addRule(RelativeLayout.CENTER_VERTICAL);
//					rlp.addRule(RelativeLayout.RIGHT_OF, 100);
//					rlp.topMargin = MyUtils.dp2px(10, getContext());
//					rlp.bottomMargin = MyUtils.dp2px(10, getContext());
//					rlp.leftMargin = MyUtils.dp2px(2, getContext());
//					rl.addView(tv, rlp);
//					
//					tv = new TextView(getContext());
//					tv.setId(300);
////					tv.getPaint().setFakeBoldText(true);
//					tv.setTextColor(Color.argb(255, 102,102,102));
//					
//					rlp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
//							RelativeLayout.LayoutParams.WRAP_CONTENT);
//					rlp.addRule(RelativeLayout.CENTER_VERTICAL);
//					rlp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
//					rlp.topMargin = MyUtils.dp2px(10, getContext());
//					rlp.bottomMargin = MyUtils.dp2px(10, getContext());
//					rlp.rightMargin = MyUtils.dp2px(10, getContext());
//					rl.addView(tv, rlp);
//					
//
//					convertView = rl;
//				}
////				MyUtils.showLog("" + orderBean.getTotalPrice() + "," + orderBean.getOrderStatus());
////				TextView tv = (TextView)convertView.findViewById(200);
////				tv.setText(String.format(Locale.CHINESE,"￥%.2f", orderBean.getTotalPayPrice()));
//				
////				tv = (TextView)convertView.findViewById(300);
////				tv.setText(DataModule.getOrderStatusName(orderBean.getOrderStatus()));  // 停止显示订单状态
////				tv.setText(DataModule.getPayStatusName(orderBean.getPayStatus()));  // 修改为显示支付状态   
//				
//				return convertView;
//				
//			}
			else if( type ==  ROW_TYPE_TIME )
			{
				TslOrderBean orderBean = _orderDataList.get(dataBean.orderIndex);
				if( convertView == null )
				{
					LinearLayout.LayoutParams llp;
					
					LinearLayout ll = new LinearLayout(getContext());
					ll.setBackgroundColor(0xFFF5F5F5);
					ll.setOrientation(LinearLayout.HORIZONTAL);
					
					TextView tv = new TextView(getContext());
					tv.setTag(100);
					tv.setTextColor(Color.argb(255, 102,102,102));
					llp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
							LinearLayout.LayoutParams.WRAP_CONTENT);
					llp.gravity = Gravity.CENTER_VERTICAL;
					llp.topMargin = MyUtils.dp2px(10, getContext());
					llp.bottomMargin = MyUtils.dp2px(10, getContext());
					llp.leftMargin = MyUtils.dp2px(5, getContext());
					ll.addView(tv, llp);

					convertView = ll;
				}
				
				TextView tv = (TextView)convertView.findViewWithTag(100);
				tv.setText(String.format("子订单号:%s", orderBean.getTslOrderNo()));
				
				return convertView;
			}
			else // DIVIDER
			{
				if( convertView == null )
				{
					
					LinearLayout ll_root = new LinearLayout(getContext());
					
					LinearLayout ll = new LinearLayout(getContext());
					ll.setBackgroundColor(Color.argb(255, 102,102,102));
					ll.setOrientation(LinearLayout.HORIZONTAL);
					
					LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
							MyUtils.dp2px(0.5f, getContext()));
					
					ll_root.addView(ll, llp);
					
					convertView = ll_root;
				}
				
				return convertView;
			}
		}
		
		
		private Context getContext(){
			return _ctx;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}
		
		class ViewHolder
		{
			TextView tvTitle;
			TextView tvNum;
			TextView tvCompany;
			TextView tvPrice;
			
			TextView tvBuyNum;
			TextView tvTips;
		}
		

	}
	
	
	private Dialog _loadingDialog = null;
	private void doLoadData()
	{
			_loadingDialog = DialogUtil.createLoadingDialog(this, "加载中...");
			_loadingDialog.show();
			
			TSLGetOrderDetailTask task = new TSLGetOrderDetailTask(_orderBean.getId());
			
			task.setTaskCallBack(new TaskPostCallBack<DDResult>() {
				
				@Override
				public void taskFinish(DDResult result) {
					if (result.getError() == RetError.NONE  )
					{
						on_submit_task_finished(result);
					}
					else 
					{// 加载失败
						on_submit_task_failed(result);
					}
				}
			});
			
			task.executeParallel("");
		}
	
	private void on_submit_task_finished(DDResult result)
	{
		List<TslOrderBean> orderList = (List<TslOrderBean>)result.getObject();
		_orderDataList.addAll(orderList);
		
		makeDataList(orderList);
		
		_adapter.notifyDataSetChanged();
		_listView.setVisibility(View.VISIBLE);
		
		_loadingDialog.dismiss();
		_loadingDialog = null;
		
	}
	
	private void makeDataList(List<TslOrderBean> orderList)
	{
		for(int i = 0; i < orderList.size(); i++ )
		{
			DataBean db;
			
//			if( _dataList.size() > 0 )
//			{
//				db = new DataBean();
//				db.rowType = ROW_TYPE_DIVIDER;
//				_dataList.add(db);
//			}
			
			TslOrderBean orderBean = orderList.get(i);
			db = new DataBean();
			db.orderIndex = i;
			db.rowType = ROW_TYPE_TIME;
			_dataList.add(db);
			
			// 下面的商品
			List<TslProductBean> productList = orderBean.getProductList();
			if( productList != null )
			{
				for(int j = 0; j < productList.size(); j++ )
				{
					db = new DataBean();
					db.orderIndex = i;
					db.productIndex = j;
					db.rowType = ROW_TYPE_ITEM;
					_dataList.add(db);
				}
			}
			
			// 统计项
//			db = new DataBean();
//			db.orderIndex = i;
//			db.rowType = ROW_TYPE_SUMMARY;
//			_dataList.add(db);
		}
	}
	
	
	private void on_submit_task_failed(DDResult result)
	{
		_loadingDialog.dismiss();
		_loadingDialog = null;
		
		ToastUtil.showToast(result.getErrorMessage());
	}
	

	@Override
	public void onDestroy() {
		super.onDestroy();
	
	}
	
	
	private class DataBean
	{
		public int rowType = ROW_TYPE_DIVIDER;
		public int orderIndex = -1;
		public int productIndex = -1;
	}
	
	
}
