package com.ddoctor.user.activity.tsl;

/**
 * 商城
 * 康
 */
import java.util.ArrayList;
import java.util.List;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.enums.RefreshAction;
import com.ddoctor.enums.RetError;
import com.ddoctor.user.activity.BaseActivity;
import com.ddoctor.user.adapter.BaseAdapter;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.task.TSLGetOrderListTask;
import com.ddoctor.user.task.TaskPostCallBack;
import com.ddoctor.user.view.DDPullToRefreshView;
import com.ddoctor.user.view.DDPullToRefreshView.OnHeaderRefreshListener;
import com.ddoctor.user.wapi.bean.ProductBean;
import com.ddoctor.user.wapi.bean.TslOrderBean;
import com.ddoctor.user.wapi.constant.TslStatusType;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.DialogUtil;
import com.ddoctor.utils.MyUtils;
import com.ddoctor.utils.ToastUtil;
import com.umeng.analytics.MobclickAgent;
import com.zhuge.analysis.stat.ZhugeSDK;

public class TSLOrderListActivity extends BaseActivity implements
		OnHeaderRefreshListener, OnScrollListener, OnItemClickListener {
	RelativeLayout rl;

	private ListView _listView;
	DDPullToRefreshView _refreshViewContainer;
	private View _getMoreView;

	private List<TslOrderBean> _dataList = new ArrayList<TslOrderBean>();
	/**
	 * 商品列表适配器
	 */
	private ListAdapter _adapter;

	private int _pageNum = 1;
	private RefreshAction _refreshAction = RefreshAction.PULLTOREFRESH;
	
	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("TSLOrderListActivity");
		MobclickAgent.onResume(TSLOrderListActivity.this);
		ZhugeSDK.getInstance().init(getApplicationContext());
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("TSLOrderListActivity");
		MobclickAgent.onPause(TSLOrderListActivity.this);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tsl_order_list);
		
		initUI();
		
		loadingData(true, _pageNum);
	}

	private void initUI() {
		this.setTitle("我的订单");
		
		rl = (RelativeLayout) findViewById(R.id.titleBar);
		if (rl != null)
			rl.setBackgroundColor(getResources().getColor(R.color.default_titlebar));
		
		getLeftButton().setText("返回");
		getLeftButton().setOnClickListener(this);
		getLeftButton().setVisibility(View.VISIBLE);
		
		getRightButton().setText("增加");
		getRightButton().setOnClickListener(this);
		getRightButton().setVisibility(View.VISIBLE);
		
		
		_refreshViewContainer = (DDPullToRefreshView) findViewById(R.id.refreshViewContainer);
		_refreshViewContainer.setOnHeaderRefreshListener(this);
		_refreshViewContainer.setVisibility(View.INVISIBLE);
		
		_listView = (ListView) findViewById(R.id.listView);
		_listView.setOnItemClickListener(this);
		_listView.setOnScrollListener(this);
		
		
		initList();
	}
	
	
	private String loadingStr;

	private void initList() {
		// 获取更多
		_getMoreView = 	createGetMoreView();
		setGetMoreContent("已全部加载", false, false);
		_listView.addFooterView(_getMoreView);

		// 数据
		_adapter = new ListAdapter(this);
		_listView.setAdapter(_adapter);
	}
	
	// 加载更多相关函数 >>>>>>
	boolean _bGetMoreEnable = false;
	private View createGetMoreView()
	{
		if( _getMoreView != null )
			return _getMoreView;
		
		View v = (View) getLayoutInflater().inflate(R.layout.refresh_footer, null);
		
		return v;
	}
	
	private void setGetMoreContent(String message, boolean showImage, boolean animation)
	{
		TextView tv = (TextView)_getMoreView.findViewById(R.id.pull_to_load_text);
		tv.setText(message);
		
		ImageView imgView = (ImageView)_getMoreView.findViewById(R.id.pull_to_load_image);
		AnimationDrawable ad = (AnimationDrawable) imgView.getBackground();
		if( showImage )
		{
			if( animation )
			{
				ad.start();
			}
			else
			{
				ad.stop();
				ad.selectDrawable(0);
			}
				
			imgView.setVisibility(View.VISIBLE);
		}
		else
		{
			ad.stop();
			ad.selectDrawable(0);
	
			imgView.setVisibility(View.GONE);
		}
	}
	
	// <<<<< 获取更多相关函数
	

	private Dialog _loadingDialog = null;

	private void loadingData(boolean showLoading, int page)
	{
		if( showLoading )
		{
			_loadingDialog = DialogUtil.createLoadingDialog(this, "加载中...");
			_loadingDialog.show();
		}
		
		TSLGetOrderListTask task = new TSLGetOrderListTask(page);
		
		final int page1 = page;
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {
			
			@Override
			public void taskFinish(DDResult result) {
				if (result.getError() == RetError.NONE  )
				{
					List<TslOrderBean> tmpList = (List<TslOrderBean>)result.getObject();
					if( page1 > 1 ) // 加载更多
					{
						_dataList.addAll(tmpList);
						
						_adapter.notifyDataSetChanged();
					}
					else
					{
						// 加载第一页
						_dataList.clear();
						_dataList.addAll(tmpList);
						_adapter.notifyDataSetChanged();
						
						_refreshViewContainer.onHeaderRefreshComplete();
						_refreshViewContainer.setVisibility(View.VISIBLE);
						_refreshAction = RefreshAction.NONE;
						
						if( _loadingDialog != null )
							_loadingDialog.dismiss();
						
					}

					// 是否显示"加载更多"
					
					setGetMoreContent("已全部加载", false, false);
					_bGetMoreEnable = false;
					
//					if( tmpList.size() > 0 )
//					{
//						setGetMoreContent("滑动加载更多", true, false);
//						_bGetMoreEnable = true;
//					}
//					else
//					{
//						// 没数据了，加载完成
//						setGetMoreContent("已全部加载", false, false);
//						_bGetMoreEnable = false;
//					}
					
					// 确保加载成功后，再修改这个变量
					_pageNum = page1;
				}
				else 
				{// 加载失败
					if( page1 > 1 )
					{
						setGetMoreContent("滑动加载更多", true, false);
					}
					else
					{
						_refreshViewContainer.onHeaderRefreshComplete();
						_refreshAction = RefreshAction.NONE;
						
						if( _loadingDialog != null )
							_loadingDialog.dismiss();
					}

					ToastUtil.showToast(result.getErrorMessage());
				}
				
				_refreshAction = RefreshAction.NONE;
			}
		});
		
		task.executeParallel("");
	}
	
	

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.btn_left:
			this.finishThisActivity();
			break;
		case R.id.btn_right:
			on_btn_add();
			break;
		default:
			break;
		}

	}

	@Override
	public void onScroll(AbsListView arg0, int firstVisibleItem, int arg2,
			int arg3) {
		
		// 工具栏的显示与隐藏		
		if( _refreshAction == RefreshAction.NONE )
		{
			if( _bGetMoreEnable )
			{// 有加载更多
				int lastPos = _listView.getLastVisiblePosition();
				int total = _listView.getHeaderViewsCount() + _dataList.size() + _listView.getFooterViewsCount();
				if( lastPos == total - 1 )
				{
					// 加载更多显示出来了，开始加载
					_refreshAction = RefreshAction.LOADMORE;
					setGetMoreContent("正在加载...", true, true);
					loadingData(false, _pageNum + 1);
				}
			}
		}
		

	}

	@Override
	public void onScrollStateChanged(AbsListView arg0, int arg1) {

	}


	@Override
	public void onHeaderRefresh(DDPullToRefreshView view) {
		if( _refreshAction == RefreshAction.NONE )
		{
			_refreshAction = RefreshAction.PULLTOREFRESH;
			loadingData(false, 1);
		}
		else
		{
			// 正在加载，什么也不做
			view.onHeaderRefreshComplete();
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

		// 进去商品详情页
		int dataIdx = arg2 - _listView.getHeaderViewsCount();
		
		MyUtils.showLog("" + dataIdx);
		
		if( dataIdx >= _dataList.size() )
			return;
		
		TslOrderBean db = _dataList.get(dataIdx);
		
		if( db.getProductList() ==  null || db.getProductList().size() < 1 )
		{
			ToastUtil.showToast("数据处理中，请稍后查看!");
			return;
		}
		
		Intent intent = new Intent(this, TSLOrderDetailActivity.class);
		Bundle b = new Bundle();
		b.putSerializable("orderBean", db);
		intent.putExtras(b);
		startActivity(intent);
	}
	
	public class ListAdapter extends BaseAdapter<ProductBean> {

		public ListAdapter(Context context) {
			super(context);

		}

		@Override
		public int getCount() {
			return _dataList.size();
		}
		

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {

			ValueHolder valueHolder = null;
			if (null == convertView) {
				convertView = inflater.inflate(R.layout.layout_tsl_order_list_item,parent, false);
				valueHolder = new ValueHolder();
				valueHolder.tv_name = (TextView) convertView.findViewById(R.id.tv_title);
				valueHolder.tv_time = (TextView) convertView.findViewById(R.id.tv_time);
				valueHolder.btn_status = (Button) convertView.findViewById(R.id.btn_status);
				
				float w = valueHolder.btn_status.getPaint().measureText("审核成功");
				RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams)valueHolder.btn_status.getLayoutParams();
				w += MyUtils.dp2px(20, _context);
				rlp.width = (int)w;
				
				convertView.setTag(valueHolder);
				
				
			} else {
				valueHolder = (ValueHolder) convertView.getTag();
			}
			
			TslOrderBean dataBean = _dataList.get(position);
			
			valueHolder.dataIndex = position;
			
			String s = "订单号：--";
			if( dataBean.getTslOrderNo() != null )
				s = String.format("订单号：%s", dataBean.getTslOrderNo());
			valueHolder.tv_name.setText(s);
			
			if( dataBean.getCreateTime() == null )
				valueHolder.tv_time.setText("");
			else
				valueHolder.tv_time.setText(dataBean.getRecordTime());
			
			int status = dataBean.getState();
			String statusName = TslStatusType.getOrderStatusName(status);
			valueHolder.btn_status.setText(statusName);
			

			int statusGroup = TslStatusType.getOrderStatusGroup(status);
			if( statusGroup == 2 )
				valueHolder.btn_status.setBackgroundResource(R.drawable.bg_btn_green_90);
			else if( statusGroup == 1 )
				valueHolder.btn_status.setBackgroundResource(R.drawable.bg_btn_red_90);
			else
				valueHolder.btn_status.setBackgroundResource(R.drawable.bg_btn_blue_90);
				
			
			return convertView;
		}
		
		private class ValueHolder {
			private TextView tv_name;
			private TextView tv_time;			
			private Button btn_status;
			
			private int dataIndex;
		}

	}
	
	private void on_btn_add()
	{
		DataModule.getInstance().activityMap.put("TSLOrderListActivity", this);
		
		Intent intent = new Intent(this, TSLOrderBuyActivity.class);
		startActivity(intent);
	}
	
	public void addOrderToList(TslOrderBean orderBean)
	{
		_dataList.add(0, orderBean);
		_adapter.notifyDataSetChanged();
	}
	
	
}
