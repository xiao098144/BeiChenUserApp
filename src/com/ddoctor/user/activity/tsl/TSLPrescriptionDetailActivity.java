package com.ddoctor.user.activity.tsl;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.user.activity.BaseActivity;
import com.ddoctor.user.wapi.bean.TslPrescriptionBean;
import com.ddoctor.user.wapi.bean.TslProductBean;
import com.ddoctor.utils.ImageLoaderUtil;
import com.ddoctor.utils.MyUtils;
import com.ddoctor.utils.ToastUtil;
import com.umeng.analytics.MobclickAgent;
import com.zhuge.analysis.stat.ZhugeSDK;

public class TSLPrescriptionDetailActivity extends BaseActivity {
	
	private TextView _tab1;
	private TextView _tab2;
	private TextView _tab3;
	
	private TextView _descTextView;
	private ImageView _imageView;
	
	private TextView _tv_hospital;
	private TextView _tv_time;
	private TextView _tv_doctor;

	private LinearLayout _itemListLayout;
	
	private TslPrescriptionBean _prescriptionBean = null;
	
	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("TSLPrescriptionDetailActivity");
		MobclickAgent.onResume(TSLPrescriptionDetailActivity.this);
		ZhugeSDK.getInstance().init(getApplicationContext());
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("TSLPrescriptionDetailActivity");
		MobclickAgent.onPause(TSLPrescriptionDetailActivity.this);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tsl_prescription_detail);
		
		Bundle b = getIntent().getExtras();
		if( b ==  null )
		{
			ToastUtil.showToast("参数错误!");
			return;
		}
		
		_prescriptionBean = (TslPrescriptionBean)b.getSerializable("data");
		
		initUI();
		
		loadItemList();
	}
	

	protected void initUI() {
		this.setTitle("处方详情");
		Button leftButton = this.getLeftButtonText("返回");
		leftButton.setOnClickListener(this);
		
		
		_tab1 = (TextView)findViewById(R.id.btn_tab1);
		_tab1.setOnClickListener(this);
		
		_tab2 = (TextView)findViewById(R.id.btn_tab2);
		_tab2.setOnClickListener(this);

		_tab3 = (TextView)findViewById(R.id.btn_tab3);
		_tab3.setOnClickListener(this);
		
		_descTextView = (TextView)findViewById(R.id.tv_desc);
		_imageView = (ImageView)findViewById(R.id.imageView);
		
		LinearLayout.LayoutParams rlp =  (LinearLayout.LayoutParams)_imageView.getLayoutParams();
		rlp.width  = MyUtils.getScreenWidth(this);
		rlp.height = rlp.width * 255 / 414;

		_imageView.setScaleType(ScaleType.CENTER_CROP);
		
		
		this._tv_hospital = (TextView)findViewById(R.id.tv_hospital);
		this._tv_time = (TextView)findViewById(R.id.tv_time);
		this._tv_doctor = (TextView)findViewById(R.id.tv_doctor);
		
		_tv_hospital.setText(_prescriptionBean.getHospital());
		_tv_time.setText(_prescriptionBean.getPrescriptionTime());
		_tv_doctor.setText(_prescriptionBean.getDoctor());
		
		_descTextView.setText(_prescriptionBean.getPrescriptionInfo());
		if( _prescriptionBean.getPrescriptionImage() != null )
		{
			ImageLoaderUtil.display(_prescriptionBean.getPrescriptionImage(), _imageView, 0);
		}
		
		
		_itemListLayout = (LinearLayout)findViewById(R.id.layout_item_list);
		
		
		Button btn = (Button)findViewById(R.id.btn_buy);
		btn.setOnClickListener(this);
		
	}
	
	private void loadItemList()
	{
		LinearLayout.LayoutParams llp;
		
//		for(int i = 0; i < 5; i++ )
//		{
//			LinearLayout ll_line = (LinearLayout)this.getLayoutInflater().inflate(R.layout.layout_tsl_prescription_item_line,null);
//			
//			llp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//			llp.topMargin = MyUtils.dp2px(10, this);
//			
//			_itemListLayout.addView(ll_line, llp);
//		}
		
		if( _prescriptionBean.getProductList() != null )
		{
			int totalCount = _prescriptionBean.getProductList().size();
			for(int i = 0; i < totalCount; i++ )
			{
				LinearLayout ll_line = (LinearLayout)this.getLayoutInflater().inflate(R.layout.layout_tsl_prescription_item_line,null);

				TslProductBean pb = _prescriptionBean.getProductList().get(i);

				RelativeLayout rl1 = (RelativeLayout)ll_line.findViewById(R.id.rl_item1);
				TextView tvTitle = (TextView)rl1.findViewById(R.id.tv_item_title);
				TextView tvNum = (TextView)rl1.findViewById(R.id.tv_item_num);
				tvTitle.setText(pb.getName());
//				tvNum.setText(pb.getYongliang());
				tvNum.setVisibility(View.GONE);

				llp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
				llp.topMargin = MyUtils.dp2px(10, this);
				
				_itemListLayout.addView(ll_line, llp);
				
				
			}
		}
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.btn_left:
			finishThisActivity();
			break;
		case R.id.btn_right:
			break;
		case R.id.btn_tab1:
			onTabSelected(0);
			break;
		case R.id.btn_tab2:
			onTabSelected(1);
			break;
		case R.id.btn_tab3:
			onTabSelected(2);
			break;
			
		case R.id.btn_buy:
			on_btn_buy();
		default:
			break;
		}
	}
	
	private void on_btn_buy()
	{
		Intent intent = new Intent(this, TSLOrderBuyActivity.class);
		startActivity(intent);
	}
	
	private void onTabSelected(int tabIndex)
	{
		if( tabIndex == 0 )
		{
			_tab1.setBackgroundColor(Color.WHITE);
			_tab2.setBackgroundColor(Color.TRANSPARENT);
			_tab3.setBackgroundColor(Color.TRANSPARENT);
			
			_descTextView.setVisibility(View.VISIBLE);
			_imageView.setVisibility(View.GONE);
			_descTextView.setText(_prescriptionBean.getPrescriptionInfo());
		}
		else if( tabIndex == 1 )
		{
			_tab2.setBackgroundColor(Color.WHITE);
			
			_tab1.setBackgroundColor(Color.TRANSPARENT);
			_tab3.setBackgroundColor(Color.TRANSPARENT);
			
			_descTextView.setVisibility(View.VISIBLE);
			_imageView.setVisibility(View.GONE);
			_descTextView.setText(_prescriptionBean.getSymptom());
		}
		else if( tabIndex == 2 )
		{
			_tab3.setBackgroundColor(Color.WHITE);
			
			_tab1.setBackgroundColor(Color.TRANSPARENT);
			_tab2.setBackgroundColor(Color.TRANSPARENT);

			_descTextView.setVisibility(View.GONE);
			_imageView.setVisibility(View.VISIBLE);
		}
	}
}
