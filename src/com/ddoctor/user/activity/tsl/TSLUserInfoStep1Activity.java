package com.ddoctor.user.activity.tsl;

/**
 * 购物车
 * 康
 */

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.enums.RetError;
import com.ddoctor.user.activity.BaseActivity;
import com.ddoctor.user.activity.WebViewActivity;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.data.MyProfile;
import com.ddoctor.user.task.FileUploadTask;
import com.ddoctor.user.task.TaskPostCallBack;
import com.ddoctor.user.wapi.WAPI;
import com.ddoctor.user.wapi.bean.PatientBean;
import com.ddoctor.user.wapi.bean.TslPatientBean;
import com.ddoctor.user.wapi.bean.UploadBean;
import com.ddoctor.user.wapi.constant.Upload;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.DialogUtil;
import com.ddoctor.utils.ImageLoaderUtil;
import com.ddoctor.utils.MyUtils;
import com.ddoctor.utils.ToastUtil;
import com.umeng.analytics.MobclickAgent;
import com.zhuge.analysis.stat.ZhugeSDK;

public class TSLUserInfoStep1Activity extends BaseActivity {

	private int _photoIndex = -1;
	private Bitmap _bitmap = null;
	
	private ImageView _userImgView1;
	private ImageView _userImgView2;
	private ImageView _userImgView3;
	
	private EditText _nameEditText;
	private EditText _csinoEditText;
	private EditText _idcardEditText;
	private EditText _mobileEditText;
	private EditText _addressEditText;
	
	private String _sbImageUrl = "";
	private String _sfzImageUrl = "";
	private String _sfzfImageUrl = "";
	
	
	private Button _btnCheckBox;
	
	private final int _radius = 10;

	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("TSLUserInfoStep1Activity");
		MobclickAgent.onResume(TSLUserInfoStep1Activity.this);
		ZhugeSDK.getInstance().init(getApplicationContext());
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("TSLUserInfoStep1Activity");
		MobclickAgent.onPause(TSLUserInfoStep1Activity.this);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tsl_userinfo_step1);
		
		initUI();
		updateUserInfo();
		initUserImageList();
		
		restoreOriginUserInfo();
	}
	
	private void updateUserInfo()
	{
		ImageView imgView = (ImageView)findViewById(R.id.headImageView);

		PatientBean patientBean = DataModule.getInstance().getLoginedUserInfo();
		if (patientBean != null) {
			// 头像
			ImageLoaderUtil.displayRoundedCorner(patientBean.getImage(),
					imgView, 150, R.drawable.chat_default_protrait);

			TextView textView = (TextView)findViewById(R.id.nameTextView);
			textView.setText(patientBean.getName());
		}
	}

	protected void initUI() {
		this.setTitle("我的医保资料");
		Button leftButton = this.getLeftButtonText("返回");
		Button rightButton = this.getRightButtonText("下一步");
		RelativeLayout rl = (RelativeLayout) findViewById(R.id.titleBar);
		if (rl != null) {
			rl.setBackgroundColor(getResources().getColor(
					R.color.default_titlebar));
		}
		leftButton.setOnClickListener(this);
		rightButton.setOnClickListener(this);
		
		
		
		
		_btnCheckBox = (Button)findViewById(R.id.btnCheckBox);
		_btnCheckBox.setSelected(false);
		_btnCheckBox.setOnClickListener(this);
		
		TextView tv = (TextView)findViewById(R.id.tv_readme);
		tv.setOnClickListener(this);
		tv.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG);
		tv.getPaint().setAntiAlias(true);
//		
		
		
		_nameEditText = (EditText)findViewById(R.id.et_name);
		_csinoEditText = (EditText)findViewById(R.id.et_csino);
		_idcardEditText = (EditText)findViewById(R.id.et_idcard);
		_mobileEditText = (EditText)findViewById(R.id.et_mobile);
		_addressEditText = (EditText)findViewById(R.id.et_address);
		
		// 默认糖医生的用户名作为备案电话
		PatientBean pb = DataModule.getInstance().getLoginedUserInfo();
		if( pb != null )
			_mobileEditText.setText(pb.getMobile());
		
		// for test
//		_nameEditText.setText("糖医生");
//		_csinoEditText.setText("1334234234234");
//		_idcardEditText.setText("11212131212121212");
//		_mobileEditText.setText("13800138000");
//		_addressEditText.setText("天津市武清区xxxxxxxx");
//		
//		_sbImageUrl = "http://test.ddoctor.cn:8080/upload/patient/shenfenzheng/1435286258042.jpg";
//		_sfzImageUrl = "http://test.ddoctor.cn:8080/upload/patient/shenfenzheng/1435286258042.jpg";
//		_sfzfImageUrl = "http://test.ddoctor.cn:8080/upload/patient/shenfenzheng/1435286258042.jpg";
	}
	

	private void restoreOriginUserInfo(){
		TslPatientBean patientBean = MyProfile.getTslUserInfo();
		if( patientBean != null )
		{
			_nameEditText.setText(patientBean.getTslName());
			_csinoEditText.setText(patientBean.getTslShebaonum());
			_idcardEditText.setText(patientBean.getTslShenfenzhengnum());
			_mobileEditText.setText(patientBean.getTslPhone1());
			_addressEditText.setText(patientBean.getTslAddressdetail());
			
			_sbImageUrl = patientBean.getTslPhotoshebaoPath();
			_sfzImageUrl = patientBean.getTslPhotoshenfenzhengPath();
			_sfzfImageUrl = patientBean.getTslPhotoshenfenzhengfPath();
			
			MyUtils.showLog("_sbImageUrl" + _sbImageUrl);
			MyUtils.showLog("_sfzImageUrl" + _sfzImageUrl);
			MyUtils.showLog("_sfzfImageUrl" + _sfzfImageUrl);
			
			if( MyUtils.isValidURLString(_sbImageUrl) )
			{
				_userImgView1.setImageDrawable(null);
				ImageLoaderUtil.displayRoundedCorner(_sbImageUrl, _userImgView1,_radius, 0);
			}
			
			if( MyUtils.isValidURLString(_sfzImageUrl) )
			{
				_userImgView2.setImageDrawable(null);
				ImageLoaderUtil.displayRoundedCorner(_sfzImageUrl, _userImgView2,_radius, 0);
			}
			
			if( MyUtils.isValidURLString(_sfzfImageUrl) )
			{
				_userImgView3.setImageDrawable(null);
				ImageLoaderUtil.displayRoundedCorner(_sfzfImageUrl, _userImgView3,_radius, 0);
			}
		}

	}	
	private void initUserImageList()
	{
		int screenWidth = MyUtils.getScreenWidth(this);
		int space = MyUtils.dp2px(10, this);
		
		int itemWidth = (screenWidth - space * 4) / 3;
		// 324x274
		int itemHeight = itemWidth * 274 / 324;
		
		LinearLayout ll_root = (LinearLayout)findViewById(R.id.ll_imagelist);
		LinearLayout.LayoutParams llp;
		RelativeLayout.LayoutParams rlp;
		
		RelativeLayout fl_img1 = (RelativeLayout)ll_root.findViewById(R.id.rl_img1);
		llp = (LinearLayout.LayoutParams)fl_img1.getLayoutParams();
		llp.width = itemWidth;
		llp.height = itemHeight;
		llp.leftMargin = space;
		fl_img1.setOnClickListener(this);
		ImageView iv_img1 = (ImageView)ll_root.findViewById(R.id.iv_img1);
		iv_img1.setImageDrawable(MyUtils.getRoundedCornerDrawable(this, R.drawable.tsl_user_img1, _radius));
		iv_img1.setScaleType(ScaleType.CENTER_CROP);
		_userImgView1 = iv_img1;
		

		RelativeLayout fl_img2 = (RelativeLayout)ll_root.findViewById(R.id.rl_img2);
		llp = (LinearLayout.LayoutParams)fl_img2.getLayoutParams();
		llp.width = itemWidth;
		llp.height = itemHeight;
		llp.leftMargin = space;
		fl_img2.setOnClickListener(this);
		ImageView iv_img2 = (ImageView)ll_root.findViewById(R.id.iv_img2);
		iv_img2.setScaleType(ScaleType.CENTER_CROP);
		iv_img2.setImageDrawable(MyUtils.getRoundedCornerDrawable(this, R.drawable.tsl_user_img2, _radius));
		_userImgView2 = iv_img2;
		
		RelativeLayout fl_img3 = (RelativeLayout)ll_root.findViewById(R.id.rl_img3);
		llp = (LinearLayout.LayoutParams)fl_img3.getLayoutParams();
		llp.width = itemWidth;
		llp.height = itemHeight;
		llp.leftMargin = space;
		fl_img3.setOnClickListener(this);
		ImageView iv_img3 = (ImageView)ll_root.findViewById(R.id.iv_img3);
		iv_img3.setScaleType(ScaleType.CENTER_CROP);
		iv_img3.setImageDrawable(MyUtils.getRoundedCornerDrawable(this, R.drawable.tsl_user_img1, _radius));
		_userImgView3 = iv_img3;
		
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.btn_left:
			finishThisActivity();
			break;
		case R.id.btn_right:
			on_btn_next();
			break;
			
		case R.id.rl_img1:
			on_btn_take_photo(0);
			break;
		case R.id.rl_img2:
			on_btn_take_photo(1);
			break;
		case R.id.rl_img3:
			on_btn_take_photo(2);
			break;
		case R.id.btnCheckBox:
		{
			on_btn_readme_checkbox();
			break;
		}
		
		case R.id.tv_readme:
		{
			on_btn_readme();
			break;
		}
		default:
			break;
		}
	}
	
	private void on_btn_take_photo(int photoIndex)
	{
		MyUtils.showMemory(this);
		_photoIndex = photoIndex;
		show_source_dialog();
	}
	
	
	private void show_source_dialog()
	{
		String[] items = new String[] { "本地图片", "拍照" };
		
		
		DialogUtil.createListDialog(this, items, new DialogUtil.ListDialogCallback(){

			@Override
			public void onItemClick(int which) {
				if( which == 0 )
				{
					Intent intent = new Intent();
					intent.setType("image/*");
					intent.setAction(Intent.ACTION_GET_CONTENT);
					startActivityForResult(intent, 0);
				}
				else
				{
					Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//					intent.putExtra(MediaStore.EXTRA_OUTPUT,  Uri.fromFile(new File(getTempPhotoFilename())));
					
//					ContentValues values = new ContentValues(3);
//					values.put(MediaStore.Images.Media.DISPLAY_NAME, "patient-photo");
//					values.put(MediaStore.Images.Media.DESCRIPTION, "patient photo");
//					values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
//					_imageFilePath = TSLUserInfoStep1Activity.this.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
//					intent.putExtra(MediaStore.EXTRA_OUTPUT, _imageFilePath);
					
					
					
					String filename = DataModule.getTakePhotoTempFilename("photo");
					File f = new File(filename);
					if( f.exists())
						f.delete();
					Uri uri = Uri.fromFile(f);
					intent.putExtra(MediaStore.EXTRA_OUTPUT,  uri);
					
					startActivityForResult(intent, 1);
				}
			}
		}).show();
	}
	
	@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		
		super.onActivityResult(requestCode, resultCode, data);
		
		MyUtils.showLog("requestCode=" + requestCode + " resultCode=" + resultCode);
		
		if( resultCode != RESULT_CANCELED )
		{
			if( requestCode == 0 )
			{// 选择图片
				if( data != null )
				{
					startPhotoCrop(data.getData());
				}
			}
			else if( requestCode == 1 )
			{// 拍照
				if( data != null )
				{
					// 
				}
				else
				{
					String filename = DataModule.getTakePhotoTempFilename("photo");
					File f = new File(filename);
					if( f.exists() )
					{
						startPhotoCrop(Uri.fromFile(f));
					}
					else
						ToastUtil.showToast("获取图片失败!");
					
				}
			}
			else if( requestCode == 2 )
			{
				// 剪裁
				if( data != null )
				{
					Bundle b = data.getExtras();
					if( b != null )
					{
						Bitmap bmp = b.getParcelable("data");
						if( bmp != null ){
							onUploadPhoto(bmp);
						}
					}
				}
			}
		}
		
	}
	
	public void startPhotoCrop(Uri uri) {
		
		if( uri == null )
			return;
		
		boolean useCrop = false;
		
		if( !useCrop )
		{
			MyUtils.showLog(uri.toString());
		  	String filename = MyUtils.getImageContentFilePathFromUri(this,  uri);
		  	if( filename == null )
		  		filename = uri.getPath();
		  	
			MyUtils.showLog(filename);
			
			Bitmap bmp = MyUtils.loadBitmapFromFile(filename);
			if( bmp == null )
			{
				ToastUtil.showToast("错误!");
				return;
			}
			
			Bitmap new_bmp = MyUtils.resizeImage(bmp, 800);
			
			onUploadPhoto(new_bmp);
//			bmp.recycle();
//			bmp = null;
		}
		else
		{
			// 不剪裁
	        Intent intent = new Intent("com.android.camera.action.CROP");
	        intent.setDataAndType(uri, "image/*");
	        // 设置裁剪
	        intent.putExtra("crop", "true");
	        // aspectX aspectY 是宽高的比例
	        intent.putExtra("aspectX", 1);
	        intent.putExtra("aspectY", 1);
	        // outputX outputY 是裁剪图片宽高
	        intent.putExtra("outputX", 320);
	        intent.putExtra("outputY", 320);
	        intent.putExtra("return-data", true);
	        startActivityForResult(intent, 2);
		}
	}
	
	
	private Dialog _loadingDialog = null;
	
	private void onUploadPhoto(Bitmap bmp)
	{
		MyUtils.showLog("onSetPhoto:.....");
		// TODO: 上传头像...
		
//		if( _bitmap != null )
//		{
//			_bitmap.recycle();
//			_bitmap = null;
//		}
		_bitmap = bmp;
		
		_loadingDialog = DialogUtil.createLoadingDialog(this, "提交中...");
		_loadingDialog.show();
		
		UploadBean uploadBean = new UploadBean();
		uploadBean.setFileType("jpg");
		
		if( _photoIndex == 0 )
			uploadBean.setType(Upload.SHEBAO_IMAGE);
		else if( _photoIndex == 1 )
			uploadBean.setType(Upload.SHENFENZHENG_IMAGE);
		else if( _photoIndex == 2 )
			uploadBean.setType(Upload.SHENFENZHENGF_IMAGE);
		
		
		byte[] data = MyUtils.Bitmap2Bytes(bmp);
		String s_data = android.util.Base64.encodeToString(data, Base64.DEFAULT);
		uploadBean.setFile(s_data);
		
		FileUploadTask task = new FileUploadTask(uploadBean);
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {
			@Override
			public void taskFinish(DDResult result) {

				if( _loadingDialog != null )
				{
					_loadingDialog.dismiss();
					_loadingDialog = null;
				}
				
				if( result.getError() == RetError.NONE )
				{
					on_task_finished(result);
				}
				else
				{
					on_task_failed(result);
				}

			}
		});
		task.executeParallel("");
	}
	
	private void on_task_finished(DDResult result)
	{
		MyUtils.showLog("on_task_finished");
		Bundle b = result.getBundle();
		String fileUrl = b.getString("fileUrl");
		
		MyUtils.showLog(fileUrl);
		
		Bitmap bmp = MyUtils.resizeImage(_bitmap, 200);
		Bitmap bmp1 = MyUtils.getRoundedCornerBitmap(bmp, _radius);
		if( _photoIndex == 0 )
		{
			_userImgView1.setScaleType(ScaleType.FIT_XY);
			_userImgView1.setImageBitmap(bmp1);
//			_userImgView1.setBackgroundDrawable(new BitmapDrawable(bmp1));
			_sbImageUrl = fileUrl;
		}
		else if( _photoIndex == 1 )
		{
			_userImgView1.setScaleType(ScaleType.FIT_XY);
			_userImgView2.setImageBitmap(bmp1);
//			_userImgView2.setBackgroundDrawable(new BitmapDrawable(bmp1));
			_sfzImageUrl = fileUrl;
		}
		else if( _photoIndex == 2 )
		{
			_userImgView1.setScaleType(ScaleType.FIT_XY);
			_userImgView3.setImageBitmap(bmp1);
//			_userImgView3.setBackgroundDrawable(new BitmapDrawable(bmp1));
			_sfzfImageUrl = fileUrl;
		}
		
		
	}
	
	private void on_task_failed(DDResult result)
	{
		ToastUtil.showToast(result.getErrorMessage());
	}
	
	private void on_btn_next()
	{
		String s_name = _nameEditText.getText().toString().trim();
		
		if( s_name.length() < 1 )
		{
			ToastUtil.showToast("请输入姓名！");
			return;
		}
		
		String s_csino = _csinoEditText.getText().toString().trim();
		if( s_csino.length() < 6 || s_csino.length() > 18 )
		{
			ToastUtil.showToast("请输入社保号!");
			return;
		}
		
		String s_idcard = _idcardEditText.getText().toString().trim();
		if( s_idcard.length() < 15 || s_idcard.length() > 18 )
		{
			ToastUtil.showToast("请输入正确的身份证号！");
			return;
		}
		
		
		String s_mobile = _mobileEditText.getText().toString().trim();
		if( s_mobile.length() < 1 || !MyUtils.isMobileNO(s_mobile) )
		{
			ToastUtil.showToast("请输入正确的备选手机号码！");
			return;
		}
		
		
		String s_address = _addressEditText.getText().toString().trim();
		if( s_address.length() < 1 )
		{
			ToastUtil.showToast("请输入家庭地址！");
			return;
		}
		
		// 三张图片
		if( _sbImageUrl.length() < 1 )
		{
			ToastUtil.showToast("请上传医保卡照片！");
			return;
		}
		
		if( _sfzImageUrl.length() < 1 )
		{
			ToastUtil.showToast("请上传身份证正面照片！");
			return;
		}
		
		if( _sfzfImageUrl.length() < 1 )
		{
			ToastUtil.showToast("请上传身份证背面照片！");
			return;
		}
		
		
		if( !_btnCheckBox.isSelected() )
		{
			ToastUtil.showToast("请查看《糖尿病门诊特殊病患者用药服务告知书》！");
			return;
		}
		
		// 进入下一页
		DataModule.getInstance().activityMap.put("TSLUserInfoStep1Activity", this);
		
		Map<String,String> dataMap = new HashMap<String, String>();
		dataMap.put("name", s_name);
		dataMap.put("csino", s_csino);
		dataMap.put("idcard", s_idcard);
		dataMap.put("mobile", s_mobile);
		dataMap.put("address",  s_address);
		dataMap.put("img_sbk", _sbImageUrl);
		dataMap.put("img_sfz", _sfzImageUrl);
		dataMap.put("img_sfzf", _sfzfImageUrl);
		
		Intent intent = new Intent(this, TSLUserInfoStep2Activity.class);

		Bundle b = new Bundle();
		
		Set<String> keySet = dataMap.keySet();  
		Iterator<String> iter = keySet.iterator();                      
		while(iter.hasNext())  
		{  
		    String key = iter.next();  
		    b.putString(key, dataMap.get(key));  
		}
		
		intent.putExtras(b);
		
		startActivity(intent);
	}
	
	private void on_btn_readme_checkbox(){
		_btnCheckBox.setSelected(!_btnCheckBox.isSelected());
	}
	
	private void on_btn_readme(){
		Intent intent = new Intent(this, WebViewActivity.class);
		intent.putExtra("title", "请阅读");
		intent.putExtra("content", WAPI.TSL_URL_MENTE);
		startActivity(intent);
		
//		_btnCheckBox.setSelected(true);
		
		
	}

}
