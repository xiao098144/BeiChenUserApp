package com.ddoctor.user.activity.tsl;

/**
 * 购物车
 * 康
 */

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.enums.RetError;
import com.ddoctor.interfaces.OnClickCallBackListener;
import com.ddoctor.user.activity.BaseActivity;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.data.MyProfile;
import com.ddoctor.user.task.FileUploadTask;
import com.ddoctor.user.task.TSLCompletePatientInfoTask;
import com.ddoctor.user.task.TaskPostCallBack;
import com.ddoctor.user.wapi.bean.TslPatientBean;
import com.ddoctor.user.wapi.bean.UploadBean;
import com.ddoctor.user.wapi.constant.Upload;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.DialogUtil;
import com.ddoctor.utils.DialogUtil.ConfirmDialog;
import com.ddoctor.utils.ImageLoaderUtil;
import com.ddoctor.utils.MyUtils;
import com.ddoctor.utils.ToastUtil;
import com.umeng.analytics.MobclickAgent;
import com.zhuge.analysis.stat.ZhugeSDK;

public class TSLUserInfoStep2Activity extends BaseActivity {

	private Bitmap _bitmap = null;

	private final int _radius = 15;

	private EditText _historyEditText;
	private EditText _uncomfortableEditText;

	private Button _historyButton;
	private Button _uncomfortableButton;
	private Button _takePhotoButton;

	private ImageView _menteImageView;
	private ImageView _prescriptionImageView;

	private TextView _tv_starttime;
	private TextView _tv_type;

	private String _menteFileUrl = null;
	private String _prescriptionFileUrl = null;
	
	private int _menteType = -1;
	private String _menteTypeString = "";
	private String _startTime = "";
	
	private TextView _tab1;
	private TextView _tab2;
	private int _currentTabIndex = 0;
	
	private static final String TAB_INDEX_KEY = "TSLUserInfoStep2Activity-tabindex";

	Map<String, String> _dataMap = new HashMap<String, String>();

	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("TSLUserInfoStep2Activity");
		MobclickAgent.onResume(TSLUserInfoStep2Activity.this);
		ZhugeSDK.getInstance().init(getApplicationContext());
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("TSLUserInfoStep2Activity");
		MobclickAgent.onPause(TSLUserInfoStep2Activity.this);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tsl_userinfo_step2);

		// 获取传递过来的参数
		Bundle b = getIntent().getExtras();
		if (b == null) {
			ToastUtil.showToast("参数错误!");
			return;
		}

		Set<String> keySet = b.keySet(); // 得到bundle中所有的key
		Iterator<String> iter = keySet.iterator();
		while (iter.hasNext()) {
			String key = iter.next();
			_dataMap.put(key, b.getString(key));
		}

		initUI();
		
		restoreOriginUserInfo();
		
		int idx = MyProfile.getIntValue(TAB_INDEX_KEY, 0);
		if( idx != 0 )
			this.onTabSelected(1);
	}

	protected void initUI() {
		this.setTitle("我的医保资料");
		Button leftButton = this.getLeftButtonText("返回");
		Button rightButton = this.getRightButtonText("完成");
		leftButton.setOnClickListener(this);
		rightButton.setOnClickListener(this);

		RelativeLayout rl = (RelativeLayout) findViewById(R.id.rl_type);
		rl.setOnClickListener(this);

		_historyEditText = (EditText) findViewById(R.id.et_history);

		rl = (RelativeLayout) findViewById(R.id.rl_starttime);
		rl.setOnClickListener(this);
		_tv_starttime = (TextView) findViewById(R.id.tv_starttime);
		_tv_type = (TextView)findViewById(R.id.tv_type);
		this._uncomfortableEditText = (EditText) findViewById(R.id.et_uncomfortable);

		_historyButton = (Button) findViewById(R.id.btn_history);
		_historyButton.setSelected(true);
		_historyButton.setOnClickListener(this);
		_historyButton.setVisibility(View.INVISIBLE); // 不需要这个，隐藏掉

		_uncomfortableButton = (Button) findViewById(R.id.btn_uncomfortable);
		_uncomfortableButton.setSelected(true);
		_uncomfortableButton.setOnClickListener(this);
		_uncomfortableButton.setVisibility(View.INVISIBLE); // 不需要这个，隐藏掉

		_takePhotoButton = (Button) findViewById(R.id.btn_take_photo);
		_takePhotoButton.setOnClickListener(this);
		
		
		_tab1 = (TextView)findViewById(R.id.btn_tab1);
		_tab1.setOnClickListener(this);
		
		_tab2 = (TextView)findViewById(R.id.btn_tab2);
		_tab2.setOnClickListener(this);
		

		rl = (RelativeLayout) findViewById(R.id.rl_photo);
		rl.setOnClickListener(this);
		_menteImageView = (ImageView) findViewById(R.id.iv_mente);
		_prescriptionImageView = (ImageView) findViewById(R.id.iv_prescription);
		
		RelativeLayout.LayoutParams rlp =  (RelativeLayout.LayoutParams)_menteImageView.getLayoutParams();
		rlp.width  = MyUtils.getScreenWidth(this);
		rlp.height = rlp.width * 255 / 414;

		rlp =  (RelativeLayout.LayoutParams)_prescriptionImageView.getLayoutParams();
		rlp.width  = MyUtils.getScreenWidth(this);
		rlp.height = rlp.width * 255 / 414;
		
		_menteImageView.setScaleType(ScaleType.CENTER_CROP);
		_prescriptionImageView.setScaleType(ScaleType.CENTER_CROP);
	}
	
	private void restoreOriginUserInfo(){
		
		TslPatientBean patientBean = MyProfile.getTslUserInfo();
		if( patientBean != null )
		{
			_tv_starttime.setText(patientBean.getTime());
			_startTime = patientBean.getTime();
			_uncomfortableEditText.setText(patientBean.getAllergy());
			_historyEditText.setText(patientBean.getYongYaoShi());
			
			_menteFileUrl = patientBean.getTslPhotomentePath();
			_prescriptionFileUrl = patientBean.getPrescriptionImage();
			
			if( MyUtils.isValidURLString(_menteFileUrl) )
			{
				_takePhotoButton.setVisibility(View.GONE);
				ImageLoaderUtil.display(_menteFileUrl, _menteImageView, 0);
			}
			
			if( MyUtils.isValidURLString(_prescriptionFileUrl) )
			{
				ImageLoaderUtil.display(_prescriptionFileUrl, _prescriptionImageView, 0);
			}

		}
		
		
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.btn_left:
			finishThisActivity();
			break;
		case R.id.btn_right:
			on_btn_done();
			break;

		case R.id.btn_history:
			_historyButton.setSelected(!_historyButton.isSelected());
			if (_historyButton.isSelected()) {
				_historyEditText.setEnabled(true);
			} else {
				_historyEditText.setEnabled(false);
				_historyEditText.setText("");
			}
			break;
		case R.id.btn_uncomfortable:
			_uncomfortableButton
					.setSelected(!_uncomfortableButton.isSelected());
			if (_uncomfortableButton.isSelected()) {
				_uncomfortableEditText.setEnabled(true);
			} else {
				_uncomfortableEditText.setEnabled(false);
				_uncomfortableEditText.setText("");
			}
			break;
		case R.id.rl_type:
			show_meiti_type_list();
			break;
		case R.id.rl_starttime:
		{
			int year = 0;
			int month = 0;
			int day = 0;
			if( _startTime != null && _startTime.length() > 0 )
			{
				String[] ss = _startTime.split("-");
				if( ss.length >= 3 )
				{
					try{
						year = Integer.parseInt(ss[0]);
						month = Integer.parseInt(ss[1]);
						day = Integer.parseInt(ss[2]);
					}
					catch(Exception e)
					{
						
					}
				}
			}
			DialogUtil.showDatePicker(TSLUserInfoStep2Activity.this, year, month, day,
					new OnClickCallBackListener() {

						@Override
						public void onClickCallBack(Bundle data) {
							_tv_starttime.setText(data.getString("name"));
							_startTime = data.getString("name");
						}
					}, 0);
		}
			break;
		case R.id.rl_photo:
		case R.id.btn_take_photo:
			on_btn_take_photo();
			break;
		case R.id.btn_tab1:
			onTabSelected(0);
			break;
		case R.id.btn_tab2:
			onTabSelected(1);
			break;
		default:
			break;
		}
	}
	
	
	private void onTabSelected(int tabIndex)
	{
		if( tabIndex == 0 )
		{
			_tab1.setBackgroundColor(Color.WHITE);
			_tab2.setBackgroundColor(Color.TRANSPARENT);
			
			if( _menteFileUrl == null || _menteFileUrl.length() < 1 )
			{
				this._takePhotoButton.setVisibility(View.VISIBLE);
				
				_menteImageView.setVisibility(View.GONE);
			}
			else
			{
				_menteImageView.setVisibility(View.VISIBLE);
				this._takePhotoButton.setVisibility(View.INVISIBLE);
			}
			_prescriptionImageView.setVisibility(View.GONE);
		}
		else if( tabIndex == 1 )
		{
			_tab2.setBackgroundColor(Color.WHITE);
			_tab1.setBackgroundColor(Color.TRANSPARENT);
			
			if( _prescriptionFileUrl == null || _prescriptionFileUrl.length() < 1 )
			{
				this._takePhotoButton.setVisibility(View.VISIBLE);
				
				_prescriptionImageView.setVisibility(View.GONE);
			}
			else
			{
				_prescriptionImageView.setVisibility(View.VISIBLE);
				this._takePhotoButton.setVisibility(View.INVISIBLE);
			}
			_menteImageView.setVisibility(View.GONE);
		}
		
		_currentTabIndex = tabIndex;
		
		MyProfile.setIntValue(TAB_INDEX_KEY,tabIndex);
	}
	

	private void on_btn_take_photo() {
		show_source_dialog();
	}

	private void show_source_dialog() {
		String[] items = new String[] { "本地图片", "拍照" };

		DialogUtil.createListDialog(this, items, new DialogUtil.ListDialogCallback(){

			@Override
			public void onItemClick(int which) {
						if (which == 0) {
							Intent intent = new Intent();
							intent.setType("image/*");
							intent.setAction(Intent.ACTION_GET_CONTENT);
							startActivityForResult(intent, 0);
						} else {
							Intent intent = new Intent(
									MediaStore.ACTION_IMAGE_CAPTURE);
							// intent.putExtra(MediaStore.EXTRA_OUTPUT,
							// Uri.fromFile(new File(getTempPhotoFilename())));

//							ContentValues values = new ContentValues(3);
//							values.put(MediaStore.Images.Media.DISPLAY_NAME,
//									"patient-photo");
//							values.put(MediaStore.Images.Media.DESCRIPTION,
//									"patient photo");
//							values.put(MediaStore.Images.Media.MIME_TYPE,
//									"image/jpeg");
//							_imageFilePath = TSLUserInfoStep2Activity.this
//									.getContentResolver()
//									.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
//											values);
//							intent.putExtra(MediaStore.EXTRA_OUTPUT,
//									_imageFilePath);
							
							String filename = DataModule.getTakePhotoTempFilename("photo");
							File f = new File(filename);
							if( f.exists())
								f.delete();
							Uri uri = Uri.fromFile(f);
							intent.putExtra(MediaStore.EXTRA_OUTPUT,  uri);
							
							startActivityForResult(intent, 1);
						}
					}
				}).show();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		super.onActivityResult(requestCode, resultCode, data);

		MyUtils.showLog("requestCode=" + requestCode + " resultCode="
				+ resultCode);

		if (resultCode != RESULT_CANCELED) {
			if (requestCode == 0) {// 选择图片
				if (data != null) {
					startPhotoCrop(data.getData());
				}
			} else if (requestCode == 1) {// 拍照
				if( data != null )
				{
					// 
				}
				else
				{
					String filename = DataModule.getTakePhotoTempFilename("photo");
					File f = new File(filename);
					if( f.exists() )
					{
						startPhotoCrop(Uri.fromFile(f));
					}
					else
						ToastUtil.showToast("获取图片失败!");
					
				}
			} else if (requestCode == 2) {
				// 剪裁
				if (data != null) {
					Bundle b = data.getExtras();
					if (b != null) {
						Bitmap bmp = b.getParcelable("data");
						if (bmp != null) {
							onUploadPhoto(bmp);
						}
					}
				}
			}
		}

	}

	public void startPhotoCrop(Uri uri) {

		
		if( uri == null )
			return;

		boolean useCrop = false;

		if (!useCrop) {
			String filename = MyUtils.getImageContentFilePathFromUri(this, uri);
			if (filename == null)
				filename = uri.getPath();

			MyUtils.showLog(filename);

			Bitmap bmp = MyUtils.loadBitmapFromFile(filename);
			if (bmp == null) {
				ToastUtil.showToast("错误!");
				return;
			}

			Bitmap new_bmp = MyUtils.resizeImage(bmp, 800);
//			bmp.recycle();
//			bmp = null;

			onUploadPhoto(new_bmp);
		} else {
			// 不剪裁
			Intent intent = new Intent("com.android.camera.action.CROP");
			intent.setDataAndType(uri, "image/*");
			// 设置裁剪
			intent.putExtra("crop", "true");
			// aspectX aspectY 是宽高的比例
			intent.putExtra("aspectX", 1);
			intent.putExtra("aspectY", 1);
			// outputX outputY 是裁剪图片宽高
			intent.putExtra("outputX", 320);
			intent.putExtra("outputY", 320);
			intent.putExtra("return-data", true);
			startActivityForResult(intent, 2);
		}
	}

	private Dialog _loadingDialog = null;

	private void onUploadPhoto(Bitmap bmp) {
		MyUtils.showLog("onSetPhoto:.....");
		// TODO: 上传头像...

//		if (_bitmap != null) {
//			_bitmap.recycle();
//			_bitmap = null;
//		}
		_bitmap = bmp;

		MyUtils.showLog("onSetPhoto:.....1");
		_loadingDialog = DialogUtil.createLoadingDialog(this, "提交中...");
		_loadingDialog.show();

		UploadBean uploadBean = new UploadBean();
		uploadBean.setFileType("jpg");
		if( _currentTabIndex == 0 )
			uploadBean.setType(Upload.MENTE_IMAGE);
		else
			uploadBean.setType(Upload.PRESCRIPTION_IMAGE);

		byte[] data = MyUtils.Bitmap2Bytes(bmp);
		String s_data = android.util.Base64
				.encodeToString(data, Base64.DEFAULT);
		uploadBean.setFile(s_data);

		FileUploadTask task = new FileUploadTask(uploadBean);
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {
			@Override
			public void taskFinish(DDResult result) {

				if (_loadingDialog != null) {
					_loadingDialog.dismiss();
					_loadingDialog = null;
				}

				if (result.getError() == RetError.NONE) {
					on_upload_task_finished(result);
				} else {
					on_upload_task_failed(result);
				}

			}
		});
		task.executeParallel("");
	}

	private void on_upload_task_finished(DDResult result) {
		MyUtils.showLog("on_task_finished");
		Bundle b = result.getBundle();
		String fileUrl = b.getString("fileUrl");
		MyUtils.showLog(fileUrl);
		
		RelativeLayout rl_photo = (RelativeLayout) findViewById(R.id.rl_photo);
		if( _currentTabIndex == 0 )
		{
			_menteFileUrl = fileUrl;
			_menteImageView.setImageBitmap(_bitmap);
			_menteImageView.setVisibility(View.VISIBLE);
			_prescriptionImageView.setVisibility(View.GONE);
		}
		else
		{
			_prescriptionFileUrl = fileUrl;
			_prescriptionImageView.setImageBitmap(_bitmap);
			_prescriptionImageView.setVisibility(View.VISIBLE);
			_menteImageView.setVisibility(View.GONE);
		}



		_takePhotoButton.setVisibility(View.GONE);

		// Bitmap bmp = MyUtils.resizeImage(_bitmap, 200);
		// Bitmap bmp1 = MyUtils.getRoundedCornerBitmap(bmp, _radius);
		// _userImgView3.setBackgroundDrawable(new BitmapDrawable(bmp1));
		// _sfzfImageUrl = fileUrl;
	}

	private void on_upload_task_failed(DDResult result) {
		ToastUtil.showToast(result.getErrorMessage());
	}
	
	
	private void show_meiti_type_list() {
		final String[] items = new String[] { "1型糖尿病", "2型糖尿病", "妊娠期糖尿病", "其他特殊类型"};

		DialogUtil.createListDialog(this, items, new DialogUtil.ListDialogCallback(){

			@Override
			public void onItemClick(int which) {
						
						_menteType = which;
						_tv_type.setText(items[which]);
						_menteTypeString = items[which];
						
					}
				}).show();
	}
	

	private void on_btn_done() {

		// 不需要这个了
//		if( _menteType == -1 )
//		{
//			ToastUtil.showToast("请选择糖尿病门特类型!");
//			return;
//		}
		
		if( _startTime == null || _startTime.length() < 1 )
		{
			ToastUtil.showToast("请选择确诊时间!");
			return;
		}
		
		
		if( _menteFileUrl == null || _menteFileUrl.length() <= 0 )
		{
			ToastUtil.showToast("请上传门特鉴定表!");
			this.onTabSelected(0);
			return;
		}
		
		if( _prescriptionFileUrl == null || _prescriptionFileUrl.length() <= 0 )
		{
			ToastUtil.showToast("请上传处方!");
			this.onTabSelected(1);
			return;
		}
		
		
		String s_history = _historyEditText.getText().toString();
		String s_uncomfortable = _uncomfortableEditText.getText().toString();

		/*
		dataMap.put("name", s_name);
		dataMap.put("csino", s_csino);
		dataMap.put("idcard", s_idcard);
		dataMap.put("mobile", s_mobile);
		dataMap.put("address",  s_address);
		dataMap.put("img_sbk", _sbImageUrl);
		dataMap.put("img_sfz", _sfzImageUrl);
		dataMap.put("img_sfzf", _sfzfImageUrl);
		 * 
		 */
		TslPatientBean tpb = new TslPatientBean();
		tpb.setTysPatientId(DataModule.getInstance().getLoginedUserId());
		tpb.setTslName(_dataMap.get("name"));
		tpb.setTslNickName(DataModule.getInstance().getLoginedUserInfo().getName());
		tpb.setTslSex(String.format("%d", DataModule.getInstance().getLoginedUserInfo().getSex()));
		tpb.setTslShenfenzhengnum(_dataMap.get("idcard"));
		tpb.setTslShebaonum(_dataMap.get("csino"));
		tpb.setTslPhone1(_dataMap.get("mobile"));
		tpb.setTslAddressdetail(_dataMap.get("address"));
		
		tpb.setTslPhotoshebaoPath(_dataMap.get("img_sbk"));
		tpb.setTslPhotoshenfenzhengPath(_dataMap.get("img_sfz"));
		tpb.setTslPhotoshenfenzhengfPath(_dataMap.get("img_sfzf"));
		tpb.setTslPhotomentePath(_menteFileUrl);
		tpb.setPrescriptionImage(_prescriptionFileUrl);
		
		tpb.setTime(_startTime);
		tpb.setBingZhong("糖尿病");//_menteTypeString);
		tpb.setYongYaoShi(s_history);
		tpb.setAllergy(s_uncomfortable);
		
//		DataModule.getInstance().s
		
		// 
//		MyProfile.setTslUserInfo(tpb);

		_loadingDialog = DialogUtil.createLoadingDialog(this, "提交中...");
		_loadingDialog.show();


		TSLCompletePatientInfoTask task = new TSLCompletePatientInfoTask(tpb);
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {
			@Override
			public void taskFinish(DDResult result) {

				if (_loadingDialog != null) {
					_loadingDialog.dismiss();
					_loadingDialog = null;
				}

				if (result.getError() == RetError.NONE) {
					on_task_finished(result);
				} else {
					on_task_failed(result);
				}

			}
		});
		task.executeParallel("");
	}

	
	private void on_task_finished(DDResult result) {
		
		MyProfile.tsl_setAuthStatus(MyProfile.TSL_AUTH_STATUS_WAIT_AUTH); // 未审核状态
		
		// 提交成功
		DialogUtil.confirmDialog(this, "您的信息已经成功提交，请等待审核！", "确定", null, new ConfirmDialog(){

			@Override
			public void onOKClick(Bundle data) {
				// 关掉步骤1
				Activity act = (Activity)DataModule.getInstance().activityMap.get("TSLUserInfoStep1Activity");
				if( act != null )
					act.finish();
				
				TSLUserInfoStep2Activity.this.finishThisActivity();
				
			}

			@Override
			public void onCancleClick() {
			}
			
		}).show();
	}

	private void on_task_failed(DDResult result) 
	{
		ToastUtil.showToast(result.getErrorMessage());
	}
	

}
