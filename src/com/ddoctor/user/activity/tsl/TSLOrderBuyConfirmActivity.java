package com.ddoctor.user.activity.tsl;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.enums.RetError;
import com.ddoctor.user.activity.BaseActivity;
import com.ddoctor.user.activity.WebViewActivity;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.data.MyProfile;
import com.ddoctor.user.task.TSLAddOrderTask;
import com.ddoctor.user.task.TSLSendVCodeTask;
import com.ddoctor.user.task.TaskPostCallBack;
import com.ddoctor.user.wapi.WAPI;
import com.ddoctor.user.wapi.bean.TslAddressinfoBean;
import com.ddoctor.user.wapi.bean.TslOrderBean;
import com.ddoctor.user.wapi.bean.TslPatientBean;
import com.ddoctor.user.wapi.bean.TslProductBean;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.DialogUtil;
import com.ddoctor.utils.ToastUtil;
import com.umeng.analytics.MobclickAgent;
import com.zhuge.analysis.stat.ZhugeSDK;

public class TSLOrderBuyConfirmActivity extends BaseActivity {

	private ListView _listView = null;
	private DataListAdapter _adapter = null;
	
	private ArrayList<TslProductBean> _dataList  = new ArrayList<TslProductBean>();
	
	private TextView _addressTextView;
	
	private Button _btnSendVCode;
	private EditText _vcodeEditText;
	
	private TextView _tv_name;
	private TextView _tv_phone;
	private TextView _tv_address;
	
	private float _bloodsugar1 = 0;
	private float _bloodsugar2 = 0;
	
	private TextView _addressOptionTextView;
	
	private int _stamp = 0;
	private TslAddressinfoBean _addressBean = null;
	
	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("TSLOrderBuyConfirmActivity");
		MobclickAgent.onResume(this);
		ZhugeSDK.getInstance().init(getApplicationContext());
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("TSLOrderBuyConfirmActivity");
		MobclickAgent.onPause(this);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tsl_order_buy_confirm);
		

//		b.putSerializable("address", _addressBean);
//		b.putSerializable("productList", _dataList);
		Bundle b = getIntent().getExtras();
		if( b == null || b.getSerializable("address") == null || b.getSerializable("productList") == null )
		{
			ToastUtil.showToast("参数错误!");
			return;
		}
		
		_stamp = b.getInt("stamp");
		_bloodsugar1 = b.getFloat("bloodsugar1");
		_bloodsugar2 = b.getFloat("bloodsugar2");
		
		_addressBean = (TslAddressinfoBean)b.getSerializable("address");
		ArrayList<TslProductBean> tmpList = (ArrayList<TslProductBean>)b.getSerializable("productList");
		for(int i = 0; i < tmpList.size(); i++ )
		{
			TslProductBean bean = tmpList.get(i);
			if( bean.getBuyNum() > 0 )
				_dataList.add(bean);
			
		}
				
		initUI();
	}
	

	protected void initUI() {
		this.setTitle("订单确认");
		Button leftButton = this.getLeftButton();
		leftButton.setText("返回");
		leftButton.setOnClickListener(this);
		leftButton.setVisibility(View.VISIBLE);
		
		Button btnSubmit = (Button)findViewById(R.id.btn_submit);
		btnSubmit.setOnClickListener(this);
		
		_btnSendVCode= (Button)findViewById(R.id.btn_sendvcode);
		_btnSendVCode.setOnClickListener(this);
		
		_vcodeEditText = (EditText)findViewById(R.id.et_vcode);
		
				
		_listView = (ListView)findViewById(R.id.listView);
		
		LinearLayout ll = (LinearLayout)LayoutInflater.from(this).inflate(R.layout.layout_tsl_buy_confirm_header, null);
		
		float titleWidth = 0;
		
		TextView tv1 = (TextView)ll.findViewById(R.id.tv_user_title);
		titleWidth = tv1.getPaint().measureText(tv1.getText().toString());
		
		TextView tv2 = (TextView)ll.findViewById(R.id.tv_mobile_title);
		float w = tv2.getPaint().measureText(tv2.getText().toString());
		if( w > titleWidth )
			titleWidth = w;
		
		TextView tv3 = (TextView)ll.findViewById(R.id.tv_address_title);
		w = tv3.getPaint().measureText(tv3.getText().toString());
		if( w > titleWidth )
			titleWidth = w;
		
		tv1.getLayoutParams().width = (int)titleWidth;
		tv2.getLayoutParams().width = (int)titleWidth;
		tv3.getLayoutParams().width = (int)titleWidth;

		
		_tv_name = (TextView)ll.findViewById(R.id.tv_user);
		_tv_phone = (TextView)ll.findViewById(R.id.tv_mobile);
		_tv_address = (TextView)ll.findViewById(R.id.tv_address);
		
		
		TslPatientBean pb = MyProfile.getTslUserInfo();
		if( pb != null )
		{
			_tv_name.setText(pb.getTslName());
			_tv_phone.setText(pb.getTslPhone1());
		}
		else
		{
			_tv_name.setText("--");
			_tv_phone.setText("--");
		}
		
		_tv_address.setText(_addressBean.getDetail());
		
		
		
		
		_listView.addHeaderView(ll);
		
		_adapter = new DataListAdapter(this);
		
		_listView.setAdapter(_adapter);
	}
	
	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.btn_left:
			finishThisActivity();
			break;
		case R.id.btn_right:
			break;
		case R.id.btn_sendvcode:
			on_btn_sendvcode();
			break;
			
		case R.id.rel_address:
		case R.id.rel_address_title:
			on_btn_address();
			break;
		case R.id.btn_submit:
			on_btn_submit();
		default:
			break;
		}
	}
	
	private void on_btn_address()
	{
		
	}
	
	private final static int MAX_BUY_DAYS = 30;
	
	
	public void updateBuyNumTips(TextView tv, int n)
	{
		if( n >= MAX_BUY_DAYS )
			tv.setText("已达到购买限制");
//		else if( n == 0 )
//			tv.setText("此药品不购买");
		else
			tv.setText("选择购买的天数");
	}
	
	
	public class DataListAdapter extends BaseAdapter {

		private Context _ctx = null;
		public DataListAdapter(Context ctx) {
			super();
			_ctx = ctx;
		}

		@Override
		public int getCount() {
			return _dataList.size();
		}
		
		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			ViewHolder vh = null;
			if (null == convertView) {
				convertView = LayoutInflater.from(_ctx).inflate(R.layout.layout_tsl_buy_item_confirm,
						parent, false);
				
				vh = new ViewHolder();
				vh.tvTitle = (TextView)convertView.findViewById(R.id.tv_title);
				vh.tvNum = (TextView)convertView.findViewById(R.id.tv_num);
				vh.tvCompany = (TextView)convertView.findViewById(R.id.tv_company);
				vh.tvPrice = (TextView)convertView.findViewById(R.id.tv_price);
				
				vh.tvBuyNum = (TextView)convertView.findViewById(R.id.tv_buy_num);
				vh.tvTips = (TextView)convertView.findViewById(R.id.tv_buy_tips);
				
				
				convertView.setTag(vh);;
				
			}
			else
				vh = (ViewHolder)convertView.getTag();
			
			TslProductBean pb = _dataList.get(position);
			
			vh.tvTitle.setText(pb.getName());
			vh.tvCompany.setText(pb.getFactory());
			String s = String.format("售价：￥%.2f", pb.getPrice());
			vh.tvPrice.setText(s);
			
			vh.tvBuyNum.setText("" + pb.getBuyNum());
			
			return convertView;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}
		
		class ViewHolder
		{
			TextView tvTitle;
			TextView tvNum;
			TextView tvCompany;
			TextView tvPrice;
			
			TextView tvBuyNum;
			TextView tvTips;
		}
		
	}
	
	
	private void on_btn_submit()
	{
//		if( !_readmeButton.isSelected() )
//		{
//			ToastUtil.showToast("请仔细阅读购买须知");
//			return;
//		}
		
		String vcode = _vcodeEditText.getText().toString().trim();
		if( vcode.length() < 1 )
		{
			ToastUtil.showToast("请输入验证码!");
			return;
		}
		
		if( !_vcode.equals(vcode) )
		{
			ToastUtil.showToast("验证码不正确!");
			return;
		}
		
		TslPatientBean patientBean = MyProfile.getTslUserInfo();
		if( patientBean == null )
		{
			ToastUtil.showToast("请先提交社保信息!");
			return;
		}
		
		TslOrderBean orderBean = new TslOrderBean();
		orderBean.setAddress(_addressBean.getDetail());
		orderBean.setPhone(patientBean.getTslPhone1());
		orderBean.setName(patientBean.getTslName());
		if( _stamp == 0 )
			orderBean.setShouyizhang("0");
		else
			orderBean.setShouyizhang("1");
		
		orderBean.setBloodsugar1(_bloodsugar1);
		orderBean.setBloodsugar2(_bloodsugar2);
		
		orderBean.setProductList(_dataList);
		
		doSubmitOrder(orderBean);
	}
	
	private Dialog _loadingDialog = null;
	private void doSubmitOrder(TslOrderBean orderBean)
	{
			_loadingDialog = DialogUtil.createLoadingDialog(this, "正在提交...");
			_loadingDialog.show();
			
			TSLAddOrderTask task = new TSLAddOrderTask(orderBean);
			
			task.setTaskCallBack(new TaskPostCallBack<DDResult>() {
				
				@Override
				public void taskFinish(DDResult result) {
					if (result.getError() == RetError.NONE  )
					{
						on_submit_task_finished(result);
					}
					else 
					{// 加载失败
						on_submit_task_failed(result);
					}
				}
			});
			
			task.executeParallel("");
		}
	
	private void on_submit_task_finished(DDResult result)
	{
		_loadingDialog.dismiss();
		_loadingDialog = null;
		
		TslOrderBean orderBean = (TslOrderBean)result.getObject();
		if( orderBean != null )
		{
			TSLOrderListActivity listActivity = (TSLOrderListActivity)DataModule.getInstance().activityMap.get("TSLOrderListActivity");
			if( listActivity != null )
			{
				listActivity.addOrderToList(orderBean);
			}
		}
		
		// 关掉之前的
		Activity act = (Activity)DataModule.getInstance().activityMap.get("TSLOrderBuyActivity");
		if( act != null )
			act.finish();
		

		Intent intent = new Intent(this, WebViewActivity.class);
		intent.putExtra("title", "提交成功");
		intent.putExtra("content", WAPI.TSL_URL_ORDER_SUCCESS);
		startActivity(intent);
		
		this.finish();
		
/*		
		// 提交成功
		 DialogUtil.confirmDialog(this, "提交成功，请等待审核!", "确定",null, new ConfirmDialog(){

			@Override
			public void onOKClick(Bundle data) {
				
				TSLOrderBuyConfirmActivity.this.finishThisActivity();
			}

			@Override
			public void onCancleClick() {
			}
			
		}).setCancelable(false).show();
		
	*/	
		
	}
	
	private void on_submit_task_failed(DDResult result)
	{
		_loadingDialog.dismiss();
		_loadingDialog = null;
		
		ToastUtil.showToast(result.getErrorMessage());
	}
	
	private void on_btn_readme()
	{
//		Intent intent = new Intent(this, WebViewActivity.class);
//		intent.putExtra("title", "购药须知");
//		intent.putExtra("content", "http://www.baidu.com");
//		startActivity(intent);
//		_readmeButton.setSelected(true);
	}
	
	private Dialog _dialog = null;
	private String _vcode = "";
	
	private final int COUNTDOWN_TIME = 60;
	private int _count = COUNTDOWN_TIME;
	private void on_btn_sendvcode()
	{
		// 倒计时中，什么也不处理
		if (_timer != null)
			return;
		
		_dialog = DialogUtil.createLoadingDialog(this, "正在发送...");
		_dialog.show();
	
		TSLSendVCodeTask task = new TSLSendVCodeTask();
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {
			@Override
			public void taskFinish(DDResult result) {
				// 发送成功
				if (result.getError() == RetError.NONE) 
				{
					_vcode = (String) result.getObject();
					_dialog.dismiss();
					startTimer();
					String s = String.format("验证码已发送到您的手机！(%s)", _vcode);
					ToastUtil.showToast(s);
//					ToastUtil.showToast("验证码已发送到您的手机！");
				} else {
					// 登录失败
					_dialog.dismiss();
					ToastUtil.showToast(result.getErrorMessage());
				}
			}
		});
		task.executeParallel("");
	}

	private Timer _timer = null;
	
	private void startTimer() {
		if (_timer == null)
			_timer = new Timer();
	
		if (_timerTask == null) {
			_timerTask = new TimerTask() {
				@Override
				public void run() {
					Message message = new Message();
					message.what = 1;
					_handler.sendMessage(message);
				}
			};
	
		}
	
		_count = COUNTDOWN_TIME;
		updateSendButtonText(false);
	
		_timer.schedule(_timerTask, 1000, 1000);
	}
	
	private void stopTimer() {
		if (_timerTask != null) {
			_timerTask.cancel();
			_timerTask = null;
		}
	
		if (_timer != null) {
			_timer.cancel();
			_timer.purge();
			_timer = null;
		}
	}

	TimerTask _timerTask = null;
	

	Handler _handler = new Handler() {
		public void handleMessage(Message msg) {
			if (msg.what == 1) {
				_count--;
				if (_count < 0) {
					stopTimer();
					updateSendButtonText(true);
				} else {
					updateSendButtonText(false);
				}
			}
			super.handleMessage(msg);
		};
	};

	private void updateSendButtonText(boolean showNormal) {
		if (showNormal) {
			_btnSendVCode.setText("发送验证码");
		} else {
			String s = String.format(Locale.CHINESE, "%d秒后可重发", _count);
			_btnSendVCode.setText(s);
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	
		stopTimer();
	}
	
}
