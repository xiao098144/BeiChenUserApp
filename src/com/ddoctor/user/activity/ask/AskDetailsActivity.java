package com.ddoctor.user.activity.ask;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.app.Dialog;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.View;
import android.view.ViewStub;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.beichen.user.R;
import com.ddoctor.enums.RefreshAction;
import com.ddoctor.enums.RetError;
import com.ddoctor.user.activity.BaseActivity;
import com.ddoctor.user.activity.ask.adapter.AskDetailsAdapter;
import com.ddoctor.user.config.AppBuildConfig;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.task.FileUploadTask;
import com.ddoctor.user.task.PatientReplyListTask;
import com.ddoctor.user.task.QuestionReplyTask;
import com.ddoctor.user.task.TaskPostCallBack;
import com.ddoctor.user.view.DDPullToRefreshView;
import com.ddoctor.user.view.DDPullToRefreshView.OnHeaderRefreshListener;
import com.ddoctor.user.wapi.bean.QuesionBean;
import com.ddoctor.user.wapi.bean.ReplyBean;
import com.ddoctor.user.wapi.bean.UploadBean;
import com.ddoctor.user.wapi.constant.Upload;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.DialogUtil;
import com.ddoctor.utils.FileOperationUtil;
import com.ddoctor.utils.MyUtils;
import com.ddoctor.utils.ToastUtil;
import com.ddoctor.utils.VoicePlayUtil;
import com.ddoctor.utils.soundrecorder.SoundRecorder;
import com.umeng.analytics.MobclickAgent;
import com.zhuge.analysis.stat.ZhugeSDK;

public class AskDetailsActivity extends BaseActivity implements
		VoicePlayUtil.OnStateChangedListener, OnHeaderRefreshListener,
		OnScrollListener, OnItemClickListener {

	private ViewStub vs;
	private EditText _et_content;

	private ListView _listView;
	DDPullToRefreshView _refreshViewContainer;
	private View _getMoreView;
	private TextView _tv_norecord;

	private AskDetailsAdapter _adapter;
	private List<QuesionBean> _dataList = new ArrayList<QuesionBean>();

	private int _pageNum = 1;
	private RefreshAction _refreshAction = RefreshAction.PULLTOREFRESH;

	private int _questionId;
	private int _patientId;
	private QuesionBean qb;
	private Bitmap _bitmap = null;
	private boolean _beSelf = false; // 是否查看个人提问详情

	protected AnimationDrawable anim;

	protected ImageView current_img;
	protected ImageView last_img;

	private int playerState = VoicePlayUtil.IDLE_STATE;

	protected int anim_res;
	private int back_res;

	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("AskDetailsActivity");
		MobclickAgent.onResume(AskDetailsActivity.this);
		ZhugeSDK.getInstance().init(getApplicationContext());
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("AskDetailsActivity");
		MobclickAgent.onPause(AskDetailsActivity.this);
	}

	@Override
	protected void onStop() {
		super.onStop();
		stopPlay();
	}

	
	private void getIntentInfo() {
		Bundle bundle = getIntent().getBundleExtra(AppBuildConfig.BUNDLEKEY);
		_beSelf = 1 == getIntent().getIntExtra("ismy", 0) ? true : false;
		qb = bundle.getParcelable("question");
		qb.setImage(getIntent().getStringExtra("image"));
		_questionId = qb.getId();
		Integer patientid = qb.getPatient().getId();
		_patientId = patientid==null?0:patientid;
		qb.setIsDoctor(0);
		qb.setReplyContent(qb.getContent());
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ask_details);
		getIntentInfo();
		findViewById();
		loadingData(true, 1);
	}

	protected void findViewById() {
		this.setTitle("问医");
		Button leftButton = this.getLeftButtonText(getString(R.string.basic_back));
		leftButton.setOnClickListener(this);
		RelativeLayout rl = (RelativeLayout) findViewById(R.id.titleBar);
		if (rl != null) {
			rl.setBackgroundColor(getResources().getColor(
					R.color.default_titlebar));
		}
		vs = (ViewStub) findViewById(R.id.sendLayout);
		if (_beSelf) {
			View view = vs.inflate();
			_et_content = (EditText) view.findViewById(R.id.content);
			ImageButton ibtn_add_more = (ImageButton) view
					.findViewById(R.id.ibtn_add_more);
			ImageButton ibtn_send = (ImageButton) view
					.findViewById(R.id.ibtn_send);
			ImageView ibtn_add_sound = (ImageView) view
					.findViewById(R.id.ibtn_add_sound);
			ibtn_add_more.setOnClickListener(this);
			ibtn_send.setOnClickListener(this);
			ibtn_add_sound.setOnClickListener(this);
		}
		_refreshViewContainer = (DDPullToRefreshView) findViewById(R.id.refreshViewContainer);
		_refreshViewContainer.setOnHeaderRefreshListener(this);
		_refreshViewContainer.setVisibility(View.INVISIBLE);

		_listView = (ListView) findViewById(R.id.listView);
		_listView.setOnScrollListener(this);
		_listView.setOnItemClickListener(this);
		_tv_norecord = (TextView) findViewById(R.id.tv_norecord);
		_listView.setEmptyView(_tv_norecord);
		initList();
		VoicePlayUtil.getInstance().setOnStateChangedListener(this);
	}

	private void initList() {
		// 获取更多
		_getMoreView = createGetMoreView();
		setGetMoreContent("已全部加载", false, false);
		_listView.addFooterView(_getMoreView);

		// 数据
		_adapter = new AskDetailsAdapter(this, _beSelf);
		_listView.setAdapter(_adapter);
		_adapter.setData(_dataList);
	}

	// 加载更多相关函数 >>>>>>
	boolean _bGetMoreEnable = false;

	private View createGetMoreView() {
		if (_getMoreView != null)
			return _getMoreView;

		View v = (View) getLayoutInflater().inflate(R.layout.refresh_footer,
				null);

		return v;
	}

	private void setGetMoreContent(String message, boolean showImage,
			boolean animation) {
		TextView tv = (TextView) _getMoreView
				.findViewById(R.id.pull_to_load_text);
		tv.setText(message);

		ImageView imgView = (ImageView) _getMoreView
				.findViewById(R.id.pull_to_load_image);
		AnimationDrawable ad = (AnimationDrawable) imgView.getBackground();
		if (showImage) {
			if (animation) {
				ad.start();
			} else {
				ad.stop();
				ad.selectDrawable(0);
			}

			imgView.setVisibility(View.VISIBLE);
		} else {
			ad.stop();
			ad.selectDrawable(0);

			imgView.setVisibility(View.GONE);
		}
	}

	// <<<<< 获取更多相关函数

	private Dialog _loadingDialog = null;

	private void loadingData(boolean isShowdialog, int page) {

		if (isShowdialog) {
			_loadingDialog = DialogUtil.createLoadingDialog(this, "加载中...");
			_loadingDialog.show();
		}

		final int page1 = page;
		PatientReplyListTask task = new PatientReplyListTask(_questionId,
				page1, _patientId);
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {
			@Override
			public void taskFinish(DDResult result) {
				if (result.getError() == RetError.NONE) {
					List<QuesionBean> tmpList = result.getBundle()
							.getParcelableArrayList("list");
					if (page1 > 1) // 加载更多
					{
						_dataList.addAll(tmpList);
						_adapter.notifyDataSetChanged();
					} else {
						// 加载第一页
						_dataList.clear();
						_dataList.add(qb);
						_dataList.addAll(tmpList);
						_adapter.notifyDataSetChanged();

						_refreshViewContainer.onHeaderRefreshComplete();
						_refreshViewContainer.setVisibility(View.VISIBLE);
						_refreshAction = RefreshAction.NONE;

						if (_loadingDialog != null)
							_loadingDialog.dismiss();

					}

					// 是否显示"加载更多"
					if (tmpList.size() > 0) {
						setGetMoreContent("滑动加载更多", true, false);
						_bGetMoreEnable = true;
					} else {
						if (page1 == 1) {
							_tv_norecord.setText(result.getErrorMessage());
							_tv_norecord.setTag(0);
						}
						// 没数据了，加载完成
						setGetMoreContent("已全部加载", false, false);
						_bGetMoreEnable = false;
					}

					// 确保加载成功后，再修改这个变量
					_pageNum = page1;
				} else {// 加载失败
					if (page1 > 1) {
						setGetMoreContent("滑动加载更多", true, false);
					} else {
						_refreshViewContainer.onHeaderRefreshComplete();
						_refreshAction = RefreshAction.NONE;

						if (_loadingDialog != null)
							_loadingDialog.dismiss();
					}

					ToastUtil.showToast(result.getErrorMessage());
				}

				_refreshAction = RefreshAction.NONE;
			}
		});

		task.executeParallel("");
	}

	private void sendReply(ReplyBean rb) {

		int doctorId = 0;
		if (qb.getDoctor() != null) {
			doctorId = qb.getDoctor().getId();
		}
		QuestionReplyTask task = new QuestionReplyTask(doctorId, rb);
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {
			@Override
			public void taskFinish(DDResult result) {

				if (result.getError() == RetError.NONE) {
					_et_content.setText("");
					loadingData(true, 1);
				} else {
					ToastUtil.showToast(result.getErrorMessage());
				}
			}
		});
		task.executeParallel("");
	}

	// private QuesionBean generateQuesionBeanFromReply(){
	//
	// }

	private long mLastClickTime;
	private int mLastButton;
	
	@Override
	public void onClick(View v) {
		super.onClick(v);
		if (!v.isEnabled())
			return;

		if (mLastButton == v.getId()) {
			// 两次点击的时间间隔至少为1.5s
			if (System.currentTimeMillis() - mLastClickTime < 300) {
				return;
			}
		}
		mLastClickTime = System.currentTimeMillis();

		switch (v.getId()) {
		case R.id.btn_left: {
			finishThisActivity();

		}
			break;
		case R.id.ibtn_add_more: {
			String[] items = new String[] { "本地图片", "拍照" };
			DialogUtil.createListDialog(AskDetailsActivity.this, items,
					new DialogUtil.ListDialogCallback() {

						@Override
						public void onItemClick(int which) {
							if (which == 0) {
								Intent intent = new Intent();
								intent.setType("image/*");
								intent.setAction(Intent.ACTION_GET_CONTENT);
								startActivityForResult(intent, 0);
							} else {
								Intent intent = new Intent(
										MediaStore.ACTION_IMAGE_CAPTURE);
								String filename = DataModule
										.getTakePhotoTempFilename("pic");
								File f = new File(filename);
								if (f.exists())
									f.delete();
								Uri uri = Uri.fromFile(f);
								intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
								startActivityForResult(intent, 1);
							}
						}
					}).show();
		}
			break;
		case R.id.ibtn_add_sound: {
			Intent intent = new Intent(this, SoundRecorder.class);
			startActivityForResult(intent, 2);
		}
			break;
		case R.id.ibtn_send: {
			String message = _et_content.getText().toString().trim();
			if (message.length() < 1) {
				ToastUtil.showToast("请输入回复内容");
			} else {
				ReplyBean rb = new ReplyBean();
				rb.setContent(message);
				rb.setId(0);
				rb.setQuestionId(qb.getId());
				sendReply(rb);
			}
		}
			break;
		default:
			break;
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		MyUtils.showLog("requestCode=" + requestCode + " resultCode="
				+ resultCode);

		if (resultCode != RESULT_CANCELED) {
			if (requestCode == 0) {// 选择图片
				if (data != null) {
					MyUtils.showLog("photoUri", data.getData() + "");
					String img_path = MyUtils.getPathFromUri(
							AskDetailsActivity.this, data.getData());
					Bitmap bitmap = MyUtils.loadBitmapFromFile(img_path);
					onUploadPhoto(MyUtils.resizeImage(bitmap,500));
				}
			} else if (requestCode == 1) {// 拍照

				String filename = DataModule.getTakePhotoTempFilename("pic");
				File f = new File(filename);
				if (f.exists()) {
					MyUtils.showLog("uploadpic: " + filename);
					Bitmap bitmap = MyUtils.loadBitmapFromFile(f
							.getAbsolutePath());
					onUploadPhoto(MyUtils.resizeImage(bitmap,500));
				} else
					ToastUtil.showToast("获取图片失败!");

			} else if (requestCode == 2) {
				String serverPath = data.getStringExtra("serverPath");
				if ((!("").equals(serverPath))) {
					ReplyBean rb = new ReplyBean();
					rb.setAudio(serverPath);
					rb.setId(0);
					rb.setQuestionId(qb.getId());
					sendReply(rb);
				}

			}
		}

		super.onActivityResult(requestCode, resultCode, data);
	}

	private void onUploadPhoto(Bitmap bmp) {
		if (_bitmap != null) {
			_bitmap.recycle();
			_bitmap = null;
		}
		_bitmap = bmp;

		_loadingDialog = DialogUtil.createLoadingDialog(
				AskDetailsActivity.this, "提交中...");
		_loadingDialog.show();

		UploadBean uploadBean = new UploadBean();
		uploadBean.setFileType("jpg");
		uploadBean.setType(Upload.QUESTION_IMAGE);

		byte[] data = MyUtils.Bitmap2Bytes(bmp);
		String s_data = android.util.Base64
				.encodeToString(data, Base64.DEFAULT);
		uploadBean.setFile(s_data);

		FileUploadTask task = new FileUploadTask(uploadBean);
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {
			@Override
			public void taskFinish(DDResult result) {

				if (_loadingDialog != null) {
					_loadingDialog.dismiss();
					_loadingDialog = null;
				}

				if (result.getError() == RetError.NONE) {
					FileOperationUtil.deleteFile(DataModule.getTakePhotoTempFilename("pic"));
					// on_task_finished(result);
					Bundle b = result.getBundle();
					String fileUrl = b.getString("fileUrl");
					ReplyBean rb = new ReplyBean();
					rb.setImage(fileUrl);
					rb.setId(0);
					rb.setQuestionId(qb.getId());
					sendReply(rb);
				} else {
					ToastUtil.showToast(result.getErrorMessage());
				}

			}
		});
		task.executeParallel("");
	}

	@Override
	public void onScroll(AbsListView arg0, int firstVisibleItem, int arg2,
			int arg3) {

		// 工具栏的显示与隐藏
		if (_refreshAction == RefreshAction.NONE) {
			if (_bGetMoreEnable) {// 有加载更多
				int lastPos = _listView.getLastVisiblePosition();
				int total = _listView.getHeaderViewsCount() + _dataList.size()
						+ _listView.getFooterViewsCount();
				if (lastPos == total - 1) {
					// 加载更多显示出来了，开始加载
					_refreshAction = RefreshAction.LOADMORE;
					setGetMoreContent("正在加载...", true, true);
					loadingData(false, _pageNum + 1);
				}
			}
		}

	}

	@Override
	public void onScrollStateChanged(AbsListView arg0, int arg1) {

	}

	@Override
	public void onHeaderRefresh(DDPullToRefreshView view) {
		if (_refreshAction == RefreshAction.NONE) {
			_refreshAction = RefreshAction.PULLTOREFRESH;
			loadingData(false, 1);
		} else {
			// 正在加载，什么也不做
			view.onHeaderRefreshComplete();
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		ImageView doctor_img = (ImageView) view
				.findViewById(R.id.quesdetail_doctor_reply_sound);
		ImageView patient_img = (ImageView) view
				.findViewById(R.id.quesdetail_patient_reply_sound);
		if (doctor_img != null
				&& doctor_img.getVisibility() == View.VISIBLE) {
			current_img = doctor_img;
			anim_res = R.anim.voice_play_right;
			current_img.setTag(true);
		} else if (patient_img != null
				&& patient_img.getVisibility() == View.VISIBLE) {
			current_img = patient_img;
			anim_res = R.anim.voice_play_left;
			current_img.setTag(false);
		}
		if (current_img != null) {
			if (current_img.getTag(R.id.tag_path) == null) {
				ToastUtil.showToast(" 正在下载语音文件 请稍后再试 ");
			} else if (("error").equals(current_img
					.getTag(R.id.tag_path))) {
				ToastUtil.showToast("服务器文件地址异常，下载语音文件失败");
			} else {
				if (playerState == VoicePlayUtil.PLAYING_STATE) {
					VoicePlayUtil.getInstance().stopPlay();
					if (last_img != current_img) {
						current_img.setBackgroundResource(anim_res);
						anim = (AnimationDrawable) current_img
								.getBackground();
						anim.start();
						VoicePlayUtil.getInstance().startPlay(
								current_img.getTag(R.id.tag_path)
										.toString());
					}
				} else {
					current_img.setBackgroundResource(anim_res);
					anim = (AnimationDrawable) current_img
							.getBackground();
					anim.start();
					VoicePlayUtil.getInstance().startPlay(
							current_img.getTag(R.id.tag_path)
									.toString());
				}
			}
		}
	}

	@Override
	public void onStateChanged(int state) {
		playerState = state;
		switch (state) {
		case VoicePlayUtil.PLAYING_STATE:
			last_img = current_img;
			break;
		default:
			break;
		}
	}

	@Override
	public void onError(int error) {
		Resources res = getResources();

		String message = null;
		switch (error) {
		case VoicePlayUtil.STORAGE_ACCESS_ERROR:
			message = res.getString(R.string.error_sdcard_access);
			break;
		case VoicePlayUtil.INTERNAL_ERROR:
			message = res.getString(R.string.error_app_internal);
			break;
		}
		if (message != null) {
			stopPlay();
			Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
		}
	}

	private void stopPlay() {
		if (anim != null && anim.isRunning()) {
			anim.stop();
			anim = null;
		}
		if (last_img != null) {
			if (last_img.getTag().equals(true)) {
				back_res = R.drawable.voice_playing__right;
			} else {
				back_res = R.drawable.voice_playing_left;
			}
			last_img.setBackgroundResource(back_res);
		}
		VoicePlayUtil.getInstance().stopPlay();
	}

	@Override
	public void onComplete() {
		stopPlay();
	}
	
}
