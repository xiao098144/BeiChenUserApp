package com.ddoctor.user.activity.ask;

import java.util.ArrayList;
import java.util.List;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.enums.RecordLayoutType;
import com.ddoctor.enums.RefreshAction;
import com.ddoctor.enums.RetError;
import com.ddoctor.user.activity.BaseActivity;
import com.ddoctor.user.activity.ask.adapter.AskDoctorAdapter;
import com.ddoctor.user.config.AppBuildConfig;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.task.GetMyQuestionListTask;
import com.ddoctor.user.task.TaskPostCallBack;
import com.ddoctor.user.view.DDPullToRefreshView;
import com.ddoctor.user.view.DDPullToRefreshView.OnHeaderRefreshListener;
import com.ddoctor.user.wapi.bean.QuesionBean;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.DialogUtil;
import com.ddoctor.utils.MyUtils;
import com.ddoctor.utils.TimeUtil;
import com.ddoctor.utils.ToastUtil;
import com.umeng.analytics.MobclickAgent;
import com.zhuge.analysis.stat.ZhugeSDK;

public class MyAskDoctorActivity extends BaseActivity implements
		OnHeaderRefreshListener, OnScrollListener, OnItemClickListener {
	private ListView _listView;
	DDPullToRefreshView _refreshViewContainer;
	private View _getMoreView;
	private TextView _tv_norecord;

	private int _pageNum = 1;
	private RefreshAction _refreshAction = RefreshAction.PULLTOREFRESH;

	private AskDoctorAdapter _adapter;
	private List<QuesionBean> _dataList = new ArrayList<QuesionBean>();
	private List<QuesionBean> _resulList = new ArrayList<QuesionBean>();

	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("MyAskDoctorActivity");
		MobclickAgent.onResume(MyAskDoctorActivity.this);
		ZhugeSDK.getInstance().init(getApplicationContext());
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("MyAskDoctorActivity");
		MobclickAgent.onPause(MyAskDoctorActivity.this);
	}
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_my_ask_doctor);
		findViewById();
		loadingData(true, _pageNum);
	}

	protected void findViewById() {
		this.setTitle("我的提问");
		Button leftButton = this.getLeftButtonText("返回");
		Button rightButton = this.getRightButtonText("提问");
		leftButton.setOnClickListener(this);
		rightButton.setOnClickListener(this);

		_refreshViewContainer = (DDPullToRefreshView) findViewById(R.id.refreshViewContainer);
		_refreshViewContainer.setOnHeaderRefreshListener(this);
		_refreshViewContainer.setVisibility(View.INVISIBLE);

		_listView = (ListView) findViewById(R.id.listView);
		_listView.setOnItemClickListener(this);
		_listView.setOnScrollListener(this);
		_tv_norecord = (TextView) findViewById(R.id.tv_norecord);
		_tv_norecord.setText("您还没有提问哦，快去提问吧");
		_listView.setEmptyView(_tv_norecord);

		initList();

	}

	private void initList() {
		// 获取更多
		_getMoreView = createGetMoreView();
		setGetMoreContent("已全部加载", false, false);
		_listView.addFooterView(_getMoreView);

		// 数据
		_adapter = new AskDoctorAdapter(MyAskDoctorActivity.this);
		_listView.setAdapter(_adapter);
		_adapter.setData(_dataList);
	}

	// 加载更多相关函数 >>>>>>
	boolean _bGetMoreEnable = false;
	boolean _searchbGetMoreEnable = false;

	private View createGetMoreView() {
		if (_getMoreView != null)
			return _getMoreView;

		View v = (View) MyAskDoctorActivity.this.getLayoutInflater().inflate(
				R.layout.refresh_footer, null);

		return v;
	}

	private void setGetMoreContent(String message, boolean showImage,
			boolean animation) {
		TextView tv = (TextView) _getMoreView
				.findViewById(R.id.pull_to_load_text);
		tv.setText(message);

		ImageView imgView = (ImageView) _getMoreView
				.findViewById(R.id.pull_to_load_image);
		AnimationDrawable ad = (AnimationDrawable) imgView.getBackground();
		if (showImage) {
			if (animation) {
				ad.start();
			} else {
				ad.stop();
				ad.selectDrawable(0);
			}

			imgView.setVisibility(View.VISIBLE);
		} else {
			ad.stop();
			ad.selectDrawable(0);

			imgView.setVisibility(View.GONE);
		}
	}

	protected List<QuesionBean> formatList(List<QuesionBean> list) {
		List<QuesionBean> resultList = new ArrayList<QuesionBean>();
		for (int i = 0; i < list.size(); i++) {
			QuesionBean quesionBean = new QuesionBean();
			list.get(i).setDate(
					TimeUtil.getInstance().formatDate2(list.get(i).getTime()));
			if (i == 0
					|| (i >= 1 && !list.get(i).getDate()
							.equals(list.get(i - 1).getDate()))) {
				quesionBean.setDate(list.get(i).getDate());
				quesionBean.setLayoutType(RecordLayoutType.TYPE_CATEGORY);
				resultList.add(quesionBean);
			}
			resultList.add(list.get(i));
		}
		return resultList;
	}

	// <<<<< 获取更多相关函数

	private Dialog _loadingDialog = null;

	private void loadingData(boolean showLoading, int page) {
		if (showLoading) {
			_loadingDialog = DialogUtil.createLoadingDialog(
					MyAskDoctorActivity.this, "加载中...");
			_loadingDialog.show();
		}

		GetMyQuestionListTask task = new GetMyQuestionListTask(page);
		final int page1 = page;
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {

			@Override
			public void taskFinish(DDResult result) {
				if (result.getError() == RetError.NONE) {
					List<QuesionBean> tmpList = result.getBundle()
							.getParcelableArrayList("list");
					if (page1 > 1) // 加载更多
					{
						_resulList.addAll(tmpList);
						_dataList.clear();
						_dataList.addAll(formatList(_resulList));
						_adapter.notifyDataSetChanged();
					} else {
						// 加载第一页
						_dataList.clear();
						_resulList.clear();
						_resulList.addAll(tmpList);
						_dataList.addAll(formatList(_resulList));
						_adapter.notifyDataSetChanged();

						_refreshViewContainer.onHeaderRefreshComplete();
						_refreshViewContainer.setVisibility(View.VISIBLE);
						_refreshAction = RefreshAction.NONE;

						if (_loadingDialog != null)
							_loadingDialog.dismiss();

					}

					// 是否显示"加载更多"
					if (tmpList.size() > 0) {
						setGetMoreContent("滑动加载更多", true, false);
						_bGetMoreEnable = true;
					} else {
						if (page1 == 1) {
							_tv_norecord.setText(result.getErrorMessage());
							_tv_norecord.setTag(0);
						}
						// 没数据了，加载完成
						setGetMoreContent("已全部加载", false, false);
						_bGetMoreEnable = false;
					}

					// 确保加载成功后，再修改这个变量
					_pageNum = page1;
				} else {// 加载失败
					if (page1 > 1) {
						setGetMoreContent("滑动加载更多", true, false);
					} else {
						_refreshViewContainer.onHeaderRefreshComplete();
						_refreshAction = RefreshAction.NONE;

						if (_loadingDialog != null)
							_loadingDialog.dismiss();
					}

					ToastUtil.showToast(result.getErrorMessage());
				}

				_refreshAction = RefreshAction.NONE;
			}
		});

		task.executeParallel("");
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.btn_left: {
			finishThisActivity();
		}
			break;
		case R.id.btn_right: {
			skipForResult(MyAskQuestionActivity.class, 200);
		}
			break;
		default:
			break;
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode != RESULT_OK) {
			return;
		}
		if (requestCode == 200) {
			loadingData(true, 1);
		}
	}
	
	@Override
	public void onScroll(AbsListView arg0, int firstVisibleItem, int arg2,
			int arg3) {

		// 工具栏的显示与隐藏
		if (_refreshAction == RefreshAction.NONE) {
			if (_bGetMoreEnable) {// 有加载更多
				int lastPos = _listView.getLastVisiblePosition();
				int total = _listView.getHeaderViewsCount() + _dataList.size()
						+ _listView.getFooterViewsCount();
				if (lastPos == total - 1) {
					// 加载更多显示出来了，开始加载
					_refreshAction = RefreshAction.LOADMORE;
					setGetMoreContent("正在加载...", true, true);
					loadingData(false, _pageNum + 1);
				}
			}
		}

	}

	@Override
	public void onScrollStateChanged(AbsListView arg0, int arg1) {

	}

	@Override
	public void onHeaderRefresh(DDPullToRefreshView view) {
		if (_refreshAction == RefreshAction.NONE) {
			_refreshAction = RefreshAction.PULLTOREFRESH;
			loadingData(false, 1);
		} else {
			// 正在加载，什么也不做
			view.onHeaderRefreshComplete();
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		int dataIdx = arg2 - _listView.getHeaderViewsCount();
		if (dataIdx >= _listView.getCount()) {
			return;
		}
		QuesionBean qb = (QuesionBean) _listView.getItemAtPosition(dataIdx);
		MyUtils.showLog("  ");
		qb.setPatient(DataModule.getInstance().getLoginedUserInfo());
		Bundle b = new Bundle();
		b.putParcelable("question", qb);
		Intent intent = new Intent(MyAskDoctorActivity.this,
				AskDetailsActivity.class);
		intent.putExtra("ismy", 1);
		intent.putExtra(AppBuildConfig.BUNDLEKEY, b);
		MyUtils.showLog(" 我的提问页  点击查看详情  onItemClick "+intent.getExtras().toString());
		startActivity(intent);
	}

}
