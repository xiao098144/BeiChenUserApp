package com.ddoctor.user.activity.ask;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.beichen.user.R;
import com.ddoctor.enums.RetError;
import com.ddoctor.user.activity.BaseActivity;
import com.ddoctor.user.task.QuestionTask;
import com.ddoctor.user.task.TaskPostCallBack;
import com.ddoctor.user.wapi.bean.QuesionBean;
import com.ddoctor.utils.DialogUtil;
import com.ddoctor.utils.ToastUtil;
import com.umeng.analytics.MobclickAgent;
import com.zhuge.analysis.stat.ZhugeSDK;

public class MyAskQuestionActivity extends BaseActivity {

	private EditText ask_question_content;
	private Dialog _dialog = null;

	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("MyAskQuestionActivity");
		MobclickAgent.onResume(MyAskQuestionActivity.this);
		ZhugeSDK.getInstance().init(getApplicationContext());
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("MyAskQuestionActivity");
		MobclickAgent.onPause(MyAskQuestionActivity.this);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ask_question);
		findViewById();

	}

	protected void findViewById() {
		this.setTitle("我的提问");
		Button leftButton = this.getLeftButtonText("返回");
		Button rightButton = this.getRightButtonText("完成");
		leftButton.setOnClickListener(this);
		rightButton.setOnClickListener(this);
		
		ask_question_content = (EditText) findViewById(R.id.ask_question_content);
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.btn_left: {
			finishThisActivity();

		}
			break;
		case R.id.btn_right: {

			setQuestion();
		}
			break;
		default:
			break;
		}
	}


	/**
	 * 添加自定义事件
	 * @param eventId
	 * @param eventName
	 */
	private void addEvent(String eventId , String eventName){
		MobclickAgent.onEvent(this, eventId);
		ZhugeSDK.getInstance().onEvent(this, eventName);
	}
	
	private void setQuestion() {

		addEvent("200001", getString(R.string.event_tw_200001));
		
		String question = ask_question_content.getText().toString().trim();
		if (question.length() < 1) {
			ToastUtil.showToast("请输入提问内容");
			return;
		}

		_dialog = DialogUtil.createLoadingDialog(this, "正在登录...");
		_dialog.show();
		QuesionBean qb = new QuesionBean();
		qb.setContent(question);
		qb.setImage("");
		qb.setAudio("");
		QuestionTask task = new QuestionTask("0", qb);
		task.setTaskCallBack(new TaskPostCallBack<RetError>() {
			@Override
			public void taskFinish(RetError result) {
				if (result == RetError.NONE) {
					_dialog.dismiss();
					ToastUtil.showToast("提问成功");
					ask_question_content.setText("");
					setResult(RESULT_OK);
					finishThisActivity();
					
				} else {

					_dialog.dismiss();
					ToastUtil.showToast(result.getErrorMessage());
				}
			}
		});
		task.executeParallel("");
	}

}
