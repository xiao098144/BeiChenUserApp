package com.ddoctor.user.activity.ask.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.user.adapter.BaseAdapter;
import com.ddoctor.user.wapi.bean.DistrictBean;

public class DistrictAdapter extends BaseAdapter<DistrictBean> {

	public DistrictAdapter(Context context) {
		super(context);

	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		ValueHolder valueHolder = null;
		if (null == convertView) {
			convertView = inflater.inflate(R.layout.layout_kind_doctor_item,
					parent, false);
			valueHolder = new ValueHolder();
			valueHolder.tv = (TextView) convertView.findViewById(R.id.tv);

			convertView.setTag(valueHolder);
		} else {
			valueHolder = (ValueHolder) convertView.getTag();
		}
		valueHolder.tv.setText(dataList.get(position).getName());
		return convertView;
	}

	private class ValueHolder {
		private TextView tv;
	}

}
