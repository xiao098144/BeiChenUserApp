package com.ddoctor.user.activity.ask.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.user.adapter.BaseAdapter;
import com.ddoctor.user.model.ConversationListBean;
import com.ddoctor.utils.TimeUtil;

public class ConversationListAdapter extends BaseAdapter<ConversationListBean> {

	public ConversationListAdapter(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if (null == convertView) {
			convertView = inflater.inflate(R.layout.layout_chatgroup_list_item, parent , false);
			holder = new ViewHolder();
			holder.img = (ImageView) convertView.findViewById(R.id.chatgroup_item_img);
			holder.img_tag = (ImageView) convertView.findViewById(R.id.chatgroup_item_img_tag);
			holder.tv_name = (TextView) convertView.findViewById(R.id.chatgroup_item_tv_name);
			holder.tv_content = (TextView) convertView.findViewById(R.id.chatgroup_item_tv_content);
			holder.tv_time = (TextView) convertView.findViewById(R.id.chatgroup_item_tv_time);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		holder.tv_name.setText(dataList.get(position).getConversationName());
		holder.tv_content.setText(dataList.get(position).getLastMsg());
		if (dataList.get(position).getLastMsgType() == 0) {
			holder.img_tag.setVisibility(View.VISIBLE);
		}else {
			holder.img_tag.setVisibility(View.INVISIBLE);
		}
		holder.tv_time.setText(TimeUtil.getInstance().formatReplyTime(dataList.get(position).getLastMsgTime(), null));
		return convertView;
	}

	private class ViewHolder {
		private ImageView img;
		private TextView tv_name;
		private TextView tv_content;
		private TextView tv_time;
		private ImageView img_tag;
	}

}
