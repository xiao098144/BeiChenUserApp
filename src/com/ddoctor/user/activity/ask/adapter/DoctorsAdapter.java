package com.ddoctor.user.activity.ask.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.user.adapter.BaseAdapter;
import com.ddoctor.user.wapi.bean.DoctorBean;
import com.ddoctor.utils.ImageLoaderUtil;
import com.ddoctor.utils.StringUtils;

public class DoctorsAdapter extends BaseAdapter<DoctorBean> {

	public DoctorsAdapter(Context context) {
		super(context);

	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		DoctorBean db = dataList.get(position);
		ValueHolder valueHolder = null;
		if (null == convertView) {
			convertView = inflater.inflate(R.layout.layout_doctors_item,
					parent, false);
			valueHolder = new ValueHolder();
			valueHolder.tv_name = (TextView) convertView
					.findViewById(R.id.tv_name);
			valueHolder.tv_level = (TextView) convertView
					.findViewById(R.id.tv_level);
			valueHolder.tv_describe = (TextView) convertView
					.findViewById(R.id.tv_describe);
			valueHolder.photo = (ImageView) convertView
					.findViewById(R.id.photo);
			valueHolder.tv_doctor_store = (TextView) convertView
					.findViewById(R.id.tv_doctor_store);
			valueHolder.tv_doctor_star = (TextView) convertView
					.findViewById(R.id.tv_doctor_star);

			convertView.setTag(valueHolder);
		} else {
			valueHolder = (ValueHolder) convertView.getTag();
		}
		valueHolder.tv_name.setText(db.getName());
		valueHolder.tv_level.setText(db.getLevel());
		valueHolder.tv_describe.setText(db.getDesc());
		if (db.getTotalscore() != null) {
			valueHolder.tv_doctor_store.setText(db.getTotalscore() + "");
		} else {
			valueHolder.tv_doctor_store.setText(0 + "");
		}
		if (db.getAverscore() != 0.0) {
			valueHolder.tv_doctor_star.setText((db.getAverscore() + "")
					.substring(0, 3));
		} else {
			valueHolder.tv_doctor_star.setText(5 + "");
		}

		ImageLoaderUtil.displayRoundedCorner(
				StringUtils.urlFormatRemote(dataList.get(position).getImage()),
				valueHolder.photo, 150, R.drawable.chat_default_protrait);
		// ImageLoaderUtil.display(StringUtils.urlFormatRemote(dataList.get(position).getImage()),
		// valueHolder.photo, R.drawable.chat_default_protrait);
		return convertView;
	}

	private class ValueHolder {
		private TextView tv_name, tv_level, tv_describe, tv_doctor_store,
				tv_doctor_star;
		private ImageView photo;
	}

}
