package com.ddoctor.user.activity.ask.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.enums.RecordLayoutType;
import com.ddoctor.user.activity.ask.DoctorDetailActivity;
import com.ddoctor.user.adapter.BaseAdapter;
import com.ddoctor.user.config.AppBuildConfig;
import com.ddoctor.user.wapi.bean.QuesionBean;
import com.ddoctor.utils.DownloadUtil;
import com.ddoctor.utils.FileOperationUtil;
import com.ddoctor.utils.FilePathUtil;
import com.ddoctor.utils.ImageLoaderUtil;
import com.ddoctor.utils.StringUtils;
import com.ddoctor.utils.TimeUtil;

public class AskDetailsAdapter extends BaseAdapter<QuesionBean> {
	
	private Context context;
	private boolean _beSelf;
	private FileDownLoadFinished fileDownLoadFinished;

	public AskDetailsAdapter(Context context, boolean _beSelf) {
		super(context);
		this.context = context;
		this._beSelf = _beSelf;
		fileDownLoadFinished = new FileDownLoadFinished();
		DownloadUtil.getInstance().setOnDownLoadFinished(
				fileDownLoadFinished);
	}

	@Override
	public int getViewTypeCount() {
		return 2;
	}

	@Override
	public int getItemViewType(int position) {
		if ("0".equals(String.valueOf(dataList.get(position).getIsDoctor()))) {

			return RecordLayoutType.TYPE_CATEGORY.ordinal();

		} else {

			return RecordLayoutType.TYPE_VALUE.ordinal();
		}
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		RecordLayoutType layoutType = RecordLayoutType.values()[getItemViewType(position)];
		final QuesionBean adb = dataList.get(position);
		switch (layoutType) {
		case TYPE_CATEGORY: {// 病人
			PatientHolder patientholder = null;
			if (null == convertView) {
				convertView = inflater.inflate(
						R.layout.layout_ask_patient_item, parent, false);
				patientholder = new PatientHolder();
				patientholder.tv_date = (TextView) convertView
						.findViewById(R.id.quesdetail_patient_tv_date);
				patientholder.tv_name = (TextView) convertView
						.findViewById(R.id.quesdetail_patient_tv_name);
				patientholder.tv_content = (TextView) convertView
						.findViewById(R.id.quesdetail_patient_tv_content);
				patientholder.patient_photo = (ImageView) convertView
						.findViewById(R.id.quesdetail_patient_photo);
				patientholder.ask_pic = (ImageView) convertView
						.findViewById(R.id.quesdetail_patient_ask_pic);
				patientholder.ask_sound = (ImageView) convertView
						.findViewById(R.id.quesdetail_patient_reply_sound);
				convertView.setTag(patientholder);
			} else {
				patientholder = (PatientHolder) convertView.getTag();
			}
			patientholder.tv_date.setText(TimeUtil.getInstance().formatReplyTime2(adb.getTime()));
			patientholder.tv_name.setText(adb.getPatient().getName());
			ImageLoaderUtil.displayRoundedCorner(
					StringUtils.urlFormatRemote(adb.getPatient().getImage()),
					patientholder.patient_photo, 150,
					R.drawable.chat_default_protrait);
			
			if (!TextUtils.isEmpty(adb.getReplyContent())) {
				patientholder.ask_pic.setVisibility(View.GONE);
				patientholder.ask_sound.setVisibility(View.GONE);
				patientholder.tv_content.setVisibility(View.VISIBLE);
				patientholder.tv_content.setText(adb.getReplyContent());
//				try {
//					final TextView tv = patientholder.tv_content;
//					tv.post(new Runnable() {
//
//						@Override
//						public void run() {
//							int lineCount = tv.getLineCount();
//							tv.setGravity(lineCount <= 1 ? Gravity.CENTER_VERTICAL
//									| Gravity.RIGHT
//									: Gravity.CENTER_VERTICAL | Gravity.LEFT);
//						}
//					});
//				} catch (Exception e) {
//					// TODO: handle exception
//				}
			} else {
				patientholder.tv_content.setVisibility(View.GONE);
			}

			if (!TextUtils.isEmpty(adb.getImage())) {
				patientholder.tv_content.setVisibility(View.GONE);
				patientholder.ask_sound.setVisibility(View.GONE);
				patientholder.ask_pic.setVisibility(View.VISIBLE);
				ImageLoaderUtil.display(
						StringUtils.urlFormatThumbnail(adb.getImage()),
						patientholder.ask_pic, R.drawable.rc_image_download_failure);
			} else {
				patientholder.ask_pic.setVisibility(View.GONE);
			}

			if (!TextUtils.isEmpty(adb.getAudio())) {

				patientholder.tv_content.setVisibility(View.GONE);
				patientholder.ask_pic.setVisibility(View.GONE);
				patientholder.ask_sound.setVisibility(View.VISIBLE);
				patientholder.ask_sound
						.setBackgroundResource(R.drawable.voice_playing_left);
				patientholder.ask_sound.setTag(R.id.tag_url, adb.getAudio());

				DownloadUtil.getInstance().startDown1(adb.getAudio(),
						FileOperationUtil.getFileName(adb.getAudio()),
						FilePathUtil.getVoicePath().getAbsolutePath());
				patientholder.ask_sound.setTag(R.id.tag_path,
						fileDownLoadFinished.getFilePath());
			} else {
				patientholder.ask_sound.setVisibility(View.GONE);
			}

		}
			break;
		case TYPE_VALUE: {// 医生
			DoctorHolder holder = null;
			if (null == convertView) {
				convertView = inflater.inflate(R.layout.layout_ask_doctor_item,
						parent, false);
				holder = new DoctorHolder();
				holder.tv_date = (TextView) convertView
						.findViewById(R.id.quesdetail_doctor_tv_date);
				holder.tv_name = (TextView) convertView
						.findViewById(R.id.quesdetail_doctor_tv_name);
				holder.tv_content = (TextView) convertView
						.findViewById(R.id.quesdetail_doctor_tv_content);
				holder.btn_adopt = (Button) convertView
						.findViewById(R.id.quesdetail_doctor_btn_adopt);
				holder.btn_adopt.setVisibility(View.GONE);
				holder.doctor_photo = (ImageView) convertView
						.findViewById(R.id.quesdetail_doctor_photo);
				holder.replay_pic = (ImageView) convertView
						.findViewById(R.id.quesdetail_doctor_reply_pic);
				holder.replay_sound = (ImageView) convertView
						.findViewById(R.id.quesdetail_doctor_reply_sound);
				convertView.setTag(holder);
			} else {
				holder = (DoctorHolder) convertView.getTag();
			}
			
			holder.tv_date.setText(TimeUtil.getInstance().formatReplyTime2(adb.getTime()));
			
			if (!TextUtils.isEmpty(adb.getDoctor().getName())) {
				holder.tv_name.setText(adb.getDoctor().getName());
			}
//			if (_beSelf) {
//				holder.btn_adopt.setVisibility(View.VISIBLE);
//			}
			ImageLoaderUtil.displayRoundedCorner(
					StringUtils.urlFormatRemote(adb.getDoctor().getImage()),
					holder.doctor_photo, 150, R.drawable.chat_default_protrait);
			holder.doctor_photo.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent intent = new Intent();
					Bundle data = new Bundle();
					data.putInt("doctorId", adb.getDoctor().getId());
					intent.putExtra(AppBuildConfig.BUNDLEKEY, data);
					intent.setClass(context, DoctorDetailActivity.class);
					context.startActivity(intent);
				}
			});

			if (!TextUtils.isEmpty(adb.getReplyContent())) {
				holder.replay_pic.setVisibility(View.GONE);
				holder.replay_sound.setVisibility(View.GONE);
				holder.tv_content.setVisibility(View.VISIBLE);
				holder.tv_content.setText(adb.getReplyContent());
				try {
					final TextView tv = holder.tv_content;
					tv.post(new Runnable() {

						@Override
						public void run() {
							int lineCount = tv.getLineCount();
							tv.setGravity(lineCount <= 1 ? Gravity.CENTER_VERTICAL
									| Gravity.RIGHT
									: Gravity.CENTER_VERTICAL | Gravity.LEFT);
						}
					});
				} catch (Exception e) {
					// TODO: handle exception
				}
			} else {
				holder.tv_content.setVisibility(View.GONE);
			}

			if (!TextUtils.isEmpty(adb.getImage())) {
				holder.tv_content.setVisibility(View.GONE);
				holder.replay_sound.setVisibility(View.GONE);
				holder.replay_pic.setVisibility(View.VISIBLE);
				ImageLoaderUtil
						.display(
								StringUtils.urlFormatThumbnail(adb.getImage()),
								holder.replay_pic,
								R.drawable.rc_image_download_failure);

			} else {
				holder.replay_pic.setVisibility(View.GONE);
			}

			if (!TextUtils.isEmpty(adb.getAudio())) {

				holder.tv_content.setVisibility(View.GONE);
				holder.replay_pic.setVisibility(View.GONE);
				holder.replay_sound.setVisibility(View.VISIBLE);
				holder.replay_sound
						.setBackgroundResource(R.drawable.voice_playing__right);
				holder.replay_sound.setTag(R.id.tag_url, adb.getAudio());

				DownloadUtil.getInstance().startDown1(adb.getAudio(),
						FileOperationUtil.getFileName(adb.getAudio()),
						FilePathUtil.getVoicePath().getAbsolutePath());
				holder.replay_sound.setTag(R.id.tag_path,
						fileDownLoadFinished.getFilePath());
			} else {
				holder.replay_sound.setVisibility(View.GONE);
			}
		}
			break;
		default:
			break;
		}

		return convertView;
	}

	@Override
	public boolean areAllItemsEnabled() {
		return false;
	}

	private class PatientHolder {
		private TextView tv_date, tv_content, tv_name;
		private ImageView patient_photo, ask_pic, ask_sound;
	}

	private class DoctorHolder {
		private TextView tv_date, tv_content, tv_name;
		private ImageView doctor_photo, replay_pic, replay_sound;
		private Button btn_adopt;
	}

	class FileDownLoadFinished implements
			DownloadUtil.OnDownLoadFinishedListener {
		private String filePath;

		@Override
		public void onComplete(String filePath) {
			this.filePath = filePath;
		}

		@Override
		public String getFilePath() {
			return filePath;
		}

	}
}
