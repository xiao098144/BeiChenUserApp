package com.ddoctor.user.activity.ask.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.enums.RecordLayoutType;
import com.ddoctor.user.adapter.BaseAdapter;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.wapi.bean.QuesionBean;
import com.ddoctor.utils.ImageLoaderUtil;
import com.ddoctor.utils.StringUtils;
import com.ddoctor.utils.TimeUtil;

public class AskDoctorAdapter extends BaseAdapter<QuesionBean> {

	public AskDoctorAdapter(Context context) {
		super(context);

	}

	@Override
	public int getViewTypeCount() {
		return 2;
	}

	@Override
	public int getItemViewType(int position) {
		if (RecordLayoutType.TYPE_CATEGORY == (dataList.get(position)
				.getLayoutType())) {
			return RecordLayoutType.TYPE_CATEGORY.ordinal();
		} else {
			return RecordLayoutType.TYPE_VALUE.ordinal();
		}
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		RecordLayoutType layoutType = RecordLayoutType.values()[getItemViewType(position)];
		switch (layoutType) {
		case TYPE_CATEGORY: {
			CategoryHolder category = null;
			if (null == convertView) {
				convertView = inflater.inflate(R.layout.layout_ask_date_item,
						parent, false);
				category = new CategoryHolder();
				category.tv_date = (TextView) convertView
						.findViewById(R.id.ask_date);
				convertView.setTag(category);
			} else {
				category = (CategoryHolder) convertView.getTag();
			}
			category.tv_date.setText(dataList.get(position).getDate());
		}
			break;
		case TYPE_VALUE: {
			ViewHolder holder = null;
			if (null == convertView) {
				convertView = inflater.inflate(
						R.layout.layout_ask_content_item, parent, false);
				holder = new ViewHolder();
				holder.patient_photo = (ImageView) convertView
						.findViewById(R.id.patient_photo);
				holder.tv_question = (TextView) convertView
						.findViewById(R.id.tv_question);
				holder.tv_time = (TextView) convertView
						.findViewById(R.id.tv_time);
				holder.tv_reply_num = (TextView) convertView.findViewById(R.id.tv_replay_num);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			if (dataList.get(position).getPatient() != null) {
				ImageLoaderUtil.displayRoundedCorner(
						StringUtils.urlFormatRemote(dataList.get(position)
								.getPatient().getImage()),
						holder.patient_photo, 150,
						R.drawable.chat_default_protrait);
			} else {
				ImageLoaderUtil.displayRoundedCorner(
						StringUtils.urlFormatRemote(DataModule.getInstance()
								.getLoginedUserInfo().getImage()),
						holder.patient_photo, 150,
						R.drawable.chat_default_protrait);
			}
			
			holder.tv_question
					.setText(dataList.get(position).getContent());
			holder.tv_time.setText(TimeUtil.getInstance().formatTime(
					dataList.get(position).getTime()));
			Integer reply = dataList.get(position).getReply();
			if (reply == null) {
				reply = 0;
			} 
				holder.tv_reply_num.setText(String.format("回复%d" , reply));
			
		}
			break;
		default:
			break;
		}

		return convertView;
	}

	@Override
	public boolean areAllItemsEnabled() {
		return false;
	}

	@Override
	public boolean isEnabled(int position) {
		if (RecordLayoutType.TYPE_CATEGORY == dataList.get(position)
				.getLayoutType()) {
			return false;
		} else {
			return true;
		}
	}

	private class CategoryHolder {
		private TextView tv_date;
	}

	private class ViewHolder {
		private ImageView patient_photo;
		private TextView tv_question, tv_time , tv_reply_num;
	}

}
