package com.ddoctor.user.activity.ask;

import io.rong.imkit.RongIM;
import io.rong.imlib.RongIMClient.ConversationType;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.enums.RetError;
import com.ddoctor.user.activity.BaseActivity;
import com.ddoctor.user.task.DoctorPatientTask;
import com.ddoctor.user.task.GetDoctorTask;
import com.ddoctor.user.task.TaskPostCallBack;
import com.ddoctor.user.wapi.bean.DoctorBean;
import com.ddoctor.utils.DialogUtil;
import com.ddoctor.utils.ImageLoaderUtil;
import com.ddoctor.utils.MyUtils;
import com.ddoctor.utils.StringUtils;
import com.ddoctor.utils.ToastUtil;
import com.rongcloud.RongCloudIdType;
import com.umeng.analytics.MobclickAgent;
import com.zhuge.analysis.stat.ZhugeSDK;

public class DoctorDetailActivity extends BaseActivity {
	TextView doctor_tv_name, doctor_tv_level, doctor_tv_hospital,
			doctor_experience, doctor_speciality;
	ImageView doctor_photo;
	Button doctor_ask_for, doctor_ask;
	Dialog _dialog;
	private int doctorId;
	private boolean hasRelation = false;
	
	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("DoctorDetailActivity");
		MobclickAgent.onResume(DoctorDetailActivity.this);
		ZhugeSDK.getInstance().init(getApplicationContext());
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("DoctorDetailActivity");
		MobclickAgent.onPause(DoctorDetailActivity.this);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_doctot_detail);
		Bundle data = getIntent().getBundleExtra("data");
		doctorId = data.getInt("doctorId");
//		hasRelation = data.getInt("type" , 1)==0?true:false;
		MyUtils.showLog("DoctorDetail "+data.toString()+" hasRelation "+hasRelation+" doctorId "+doctorId);
		findViewById();
		getData();
	}

	protected void findViewById() {
		Button leftButton = this.getLeftButtonText("返回");
		leftButton.setOnClickListener(this);
		RelativeLayout rl = (RelativeLayout) findViewById(R.id.titleBar);
		if (rl != null) {
			rl.setBackgroundColor(getResources().getColor(
					R.color.default_titlebar));
		}
			
		doctor_tv_name = (TextView) findViewById(R.id.doctor_tv_name);
		doctor_tv_level = (TextView) findViewById(R.id.doctor_tv_level);
		doctor_tv_hospital = (TextView) findViewById(R.id.doctor_tv_hospital);
		doctor_experience = (TextView) findViewById(R.id.doctor_experience);
		doctor_speciality = (TextView) findViewById(R.id.doctor_speciality);
		doctor_photo = (ImageView) findViewById(R.id.doctor_photo);
		doctor_ask_for = (Button) findViewById(R.id.doctor_ask_for);
		doctor_ask = (Button) findViewById(R.id.doctor_ask);
		
		doctor_ask_for.setOnClickListener(this);
		doctor_ask.setOnClickListener(this);
		
	}


	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.btn_left: {
			finishThisActivity();
		}
			break;
		case R.id.doctor_ask_for: {
			askForDoctor();
		}
			break;
		case R.id.doctor_ask: {
			RongIM.getInstance().startConversation(DoctorDetailActivity.this,
					ConversationType.PRIVATE,
					RongCloudIdType.RC_TYPE_DOC.getType() + doctorId,
					_doctorBean.getName());
		}
			break;
		default:
			break;
		}
	}

	private DoctorBean _doctorBean;
	
	private void getData() {

		_dialog = DialogUtil.createLoadingDialog(this, "正在登录...");
		_dialog.show();

		GetDoctorTask task = new GetDoctorTask(doctorId+"");
		task.setTaskCallBack(new TaskPostCallBack<RetError>() {
			@Override
			public void taskFinish(RetError result) {

				if (result == RetError.NONE) {
					_doctorBean = (DoctorBean) result.getObject();
					_dialog.dismiss();
					setData(_doctorBean);
				} else {

					_dialog.dismiss();
					ToastUtil.showToast(result.getErrorMessage());
				}
			}
		});
		task.executeParallel("");
	}

	protected void setData(DoctorBean doctor) {
		hasRelation = doctor.getRelation()==null?false:doctor.getRelation()==1?true:false;
		if (hasRelation) {
			doctor_ask.setVisibility(View.VISIBLE);
			doctor_ask_for.setVisibility(View.GONE);
		}else {
			doctor_ask.setVisibility(View.GONE);
			doctor_ask_for.setVisibility(View.VISIBLE);	
		}
		doctor_tv_name.setText(doctor.getName());
		doctor_tv_level.setText(doctor.getLevel());
		doctor_tv_hospital.setText(doctor.getHospital());
		doctor_experience.setText(doctor.getDesc());
		doctor_speciality.setText(doctor.getSkill());
		ImageLoaderUtil.displayRoundedCorner(
				StringUtils.urlFormatRemote(doctor.getImage()), doctor_photo,
				150, R.drawable.chat_default_protrait);
	}


	/**
	 * 添加自定义事件
	 * @param eventId
	 * @param eventName
	 */
	private void addEvent(String eventId , String eventName){
		MobclickAgent.onEvent(this, eventId);
		ZhugeSDK.getInstance().onEvent(this, eventName);
	}
	
	private void askForDoctor() {

		_dialog = DialogUtil.createLoadingDialog(this, "正在登录...");
		_dialog.show();

		addEvent("200002", getString(R.string.event_tjys_200002));
		
		DoctorPatientTask task = new DoctorPatientTask(doctorId+"");
		task.setTaskCallBack(new TaskPostCallBack<RetError>() {
			@Override
			public void taskFinish(RetError result) {

				if (result == RetError.NONE) {
					ToastUtil.showToast("申请成功，请等待医生同意");
					_dialog.dismiss();

				} else {

					_dialog.dismiss();
					ToastUtil.showToast(result.getErrorMessage());
				}
			}
		});
		task.executeParallel("");
	}

}
