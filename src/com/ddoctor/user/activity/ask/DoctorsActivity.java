package com.ddoctor.user.activity.ask;

import java.util.ArrayList;
import java.util.List;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.enums.RefreshAction;
import com.ddoctor.enums.RetError;
import com.ddoctor.user.activity.BaseActivity;
import com.ddoctor.user.activity.ask.adapter.DoctorsAdapter;
import com.ddoctor.user.config.AppBuildConfig;
import com.ddoctor.user.task.DoctorListTask;
import com.ddoctor.user.task.SearchDoctorListTask;
import com.ddoctor.user.task.TaskPostCallBack;
import com.ddoctor.user.view.DDPullToRefreshView;
import com.ddoctor.user.view.DDPullToRefreshView.OnHeaderRefreshListener;
import com.ddoctor.user.wapi.bean.DoctorBean;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.DialogUtil;
import com.ddoctor.utils.StringUtils;
import com.ddoctor.utils.ToastUtil;
import com.umeng.analytics.MobclickAgent;
import com.zhuge.analysis.stat.ZhugeSDK;

public class DoctorsActivity extends BaseActivity implements
		OnHeaderRefreshListener, OnScrollListener, OnItemClickListener,
		TextWatcher {

//	private Button btn_area, btn_hospital, btn_person, 
	private Button btn_left, btn_right;
	private ImageButton ibtn_search;
	private EditText et_search;

	private View cutline;// popwindow显示位置

	private ListView _listView;
	DDPullToRefreshView _refreshViewContainer;
	private View _getMoreView;
	private TextView _tv_norecord;

	private int _pageNum = 1;

	private int _searchNum = 1;
	private RefreshAction _refreshAction = RefreshAction.PULLTOREFRESH;

	DoctorsAdapter _adapter;

	// /**
	// * 地区医院职位列表
	// */
	// List<DistrictBean> distList = new ArrayList<DistrictBean>();
	// List<HospitalBean> hosList = new ArrayList<HospitalBean>();
	// List<LevelBean> levelList = new ArrayList<LevelBean>();

	/**
	 * 获取医生数据
	 */
	List<DoctorBean> _dataList = new ArrayList<DoctorBean>();
	List<DoctorBean> _sourceList = new ArrayList<DoctorBean>();

	private int _doctorType = 1;// 1是所有医生，0是我的医生
	private int _searchType = 0; // 搜索类型，0关键字 1所属城市 2医院 3 职位

	private boolean isSearch = false;  

	private String keyword;

	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("DoctorsActivity");
		MobclickAgent.onResume(DoctorsActivity.this);
		ZhugeSDK.getInstance().init(getApplicationContext());
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("DoctorsActivity");
		MobclickAgent.onPause(DoctorsActivity.this);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_doctors);
		findViewById();
//		distList = DataModule.loadDict(MyProfile.DICT_DISTRICTS, DistrictBean.class);
//		if (distList!=null && distList.size()>0) {
//			MyUtils.showLog("地区列表  "+distList.toString());
//		}
		
		getData(_pageNum, _doctorType, true);

	}

	protected void findViewById() {
		btn_left = this.getLeftButtonText("返回");
		btn_right = this.getRightButtonText("我的医生");
		btn_left.setOnClickListener(this);
		btn_right.setOnClickListener(this);
		RelativeLayout rl = (RelativeLayout) findViewById(R.id.titleBar);
		if (rl != null) {
			rl.setBackgroundColor(getResources().getColor(
					R.color.default_titlebar));
		}

		// btn_area = (Button) findViewById(R.id.btn_area);
		// btn_hospital = (Button) findViewById(R.id.btn_hospital);
		// btn_person = (Button) findViewById(R.id.btn_person);
		// cutline = (View) findViewById(R.id.cutline);

		ibtn_search = (ImageButton) findViewById(R.id.doctor_ibtn_search);
		et_search = (EditText) findViewById(R.id.doctor_et_search);
		et_search.setHint(StringUtils.fromatETHint(
				getString(R.string.et_hint_knowledgelib), 13));
		et_search.addTextChangedListener(this);

		// btn_area.setOnClickListener(this);
		// btn_hospital.setOnClickListener(this);
		// btn_person.setOnClickListener(this);

		ibtn_search.setOnClickListener(this);

		_refreshViewContainer = (DDPullToRefreshView) findViewById(R.id.refreshViewContainer);
		_refreshViewContainer.setOnHeaderRefreshListener(this);
		_refreshViewContainer.setVisibility(View.INVISIBLE);

		_listView = (ListView) findViewById(R.id.listView);
		_listView.setOnScrollListener(this);
		_listView.setOnItemClickListener(this);
		_tv_norecord = (TextView) findViewById(R.id.tv_norecord);
		_tv_norecord.setVisibility(View.INVISIBLE);
		_listView.setEmptyView(_tv_norecord);
		initList();

	}

	private void initList() {
		// 获取更多
		_getMoreView = createGetMoreView();
		setGetMoreContent("已全部加载", false, false);
		_listView.addFooterView(_getMoreView);

		// 数据
		_adapter = new DoctorsAdapter(this);
		_listView.setAdapter(_adapter);
		_adapter.setData(_dataList);
	}

	// 加载更多相关函数 >>>>>>
	boolean _bGetMoreEnable = false;

	private View createGetMoreView() {
		if (_getMoreView != null)
			return _getMoreView;

		View v = (View) getLayoutInflater().inflate(R.layout.refresh_footer,
				null);

		return v;
	}

	private void setGetMoreContent(String message, boolean showImage,
			boolean animation) {
		TextView tv = (TextView) _getMoreView
				.findViewById(R.id.pull_to_load_text);
		tv.setText(message);

		ImageView imgView = (ImageView) _getMoreView
				.findViewById(R.id.pull_to_load_image);
		AnimationDrawable ad = (AnimationDrawable) imgView.getBackground();
		if (showImage) {
			if (animation) {
				ad.start();
			} else {
				ad.stop();
				ad.selectDrawable(0);
			}

			imgView.setVisibility(View.VISIBLE);
		} else {
			ad.stop();
			ad.selectDrawable(0);

			imgView.setVisibility(View.GONE);
		}
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.btn_left: {

			finishThisActivity();
		}
			break;
		case R.id.btn_right: {
			if (_doctorType == 1) {
				btn_right.setText("全部医生");
				_doctorType = 0;
				getData(1, _doctorType, true);
				et_search.setText(null);
			} else {
				et_search.setText(null);
				btn_right.setText("我的医生");
				_doctorType = 1;
				getData(1, _doctorType, true);
			}
		}
			break;
		case R.id.doctor_ibtn_search: {
			keyword = et_search.getText().toString().trim();
			if (TextUtils.isEmpty(keyword)) {
				isSearch = false;
				ToastUtil.showToast("请输入搜索关键字");
			} else {
				_pageNum = 1;
				getSearchData(keyword, _pageNum, true);
			}
		}
		default:
			break;
		}
	}

	private Dialog _loadingDialog = null;

	private void getData(int page, int type, boolean isShowDialog) {

		if (isShowDialog) {
			_loadingDialog = DialogUtil.createLoadingDialog(this, "加载中...");
			_loadingDialog.show();
		}
		final int page1 = page;
		isSearch = false;
		DoctorListTask task = new DoctorListTask(page, type);
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {
			@Override
			public void taskFinish(DDResult result) {
				if (result.getError() == RetError.NONE) {
					List<DoctorBean> tmpList = result.getBundle()
							.getParcelableArrayList("list");
					if (page1 > 1) // 加载更多
					{
						_dataList.addAll(tmpList);
						_sourceList.addAll(tmpList);
						_adapter.notifyDataSetChanged();
					} else {
						// 加载第一页
						_dataList.clear();
						_sourceList.clear();
						_dataList.addAll(tmpList);
						_sourceList.addAll(tmpList);
						_adapter.notifyDataSetChanged();

						_refreshViewContainer.onHeaderRefreshComplete();
						_refreshViewContainer.setVisibility(View.VISIBLE);
						_refreshAction = RefreshAction.NONE;

						if (_loadingDialog != null)
							_loadingDialog.dismiss();

					}

					// 是否显示"加载更多"
					if (tmpList.size() > 0) {
						setGetMoreContent("滑动加载更多", true, false);
						_bGetMoreEnable = true;
					} else {
						if (page1 == 1) {
							_tv_norecord.setText(result.getErrorMessage());
							_tv_norecord.setVisibility(View.VISIBLE);
						}
						// 没数据了，加载完成
						setGetMoreContent("已全部加载", false, false);
						_bGetMoreEnable = false;
					}

					// 确保加载成功后，再修改这个变量
					_pageNum = page1;
				} else {// 加载失败
					if (page1 > 1) {
						setGetMoreContent("滑动加载更多", true, false);
					} else {
						_refreshViewContainer.onHeaderRefreshComplete();
						_refreshAction = RefreshAction.NONE;

						if (_loadingDialog != null)
							_loadingDialog.dismiss();
					}

					ToastUtil.showToast(result.getErrorMessage());
				}

				_refreshAction = RefreshAction.NONE;
			}
		});
		task.executeParallel("");
	}

	private void getSearchData(String keyword, int page, boolean isShowDialog) {
		if (isShowDialog) {
			if (_loadingDialog == null) {
				_loadingDialog = DialogUtil.createLoadingDialog(this, "加载中...");
			}
			_loadingDialog.show();
		}
		final int page1 = page;
		isSearch = true;
		SearchDoctorListTask task = new SearchDoctorListTask(page, keyword,
				_searchType);
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {
			@Override
			public void taskFinish(DDResult result) {
				if (result.getError() == RetError.NONE) {
					List<DoctorBean> tmpList = result.getBundle()
							.getParcelableArrayList("list");
					if (page1 > 1) // 加载更多
					{
						_dataList.addAll(tmpList);
						_adapter.notifyDataSetChanged();
					} else {
						// 加载第一页
						_dataList.clear();
						_dataList.addAll(tmpList);
						_adapter.notifyDataSetChanged();

						_refreshViewContainer.onHeaderRefreshComplete();
						_refreshViewContainer.setVisibility(View.VISIBLE);
						_refreshAction = RefreshAction.NONE;

						if (_loadingDialog != null)
							_loadingDialog.dismiss();

					}

					// 是否显示"加载更多"
					if (tmpList.size() > 0) {
						setGetMoreContent("滑动加载更多", true, false);
						_bGetMoreEnable = true;
					} else {
						if (page1 == 1) {
							ToastUtil.showToast("没有结果，请重新输入关键字进行搜索");
						}
						// 没数据了，加载完成
						setGetMoreContent("已全部加载", false, false);
						_bGetMoreEnable = false;
					}

					// 确保加载成功后，再修改这个变量
					_searchNum = page1;
				} else {// 加载失败
					if (page1 > 1) {
						setGetMoreContent("滑动加载更多", true, false);
					} else {
						_refreshViewContainer.onHeaderRefreshComplete();
						_refreshAction = RefreshAction.NONE;

						if (_loadingDialog != null)
							_loadingDialog.dismiss();
					}

					ToastUtil.showToast(result.getErrorMessage());
				}

				_refreshAction = RefreshAction.NONE;
			}
		});
		task.executeParallel("");
	}

	@Override
	public void onHeaderRefresh(DDPullToRefreshView view) {
		if (_refreshAction == RefreshAction.NONE) {
			_refreshAction = RefreshAction.PULLTOREFRESH;
			if (isSearch) {
				getSearchData(keyword, 1, false);
			} else {
				getData(1, _doctorType, false);
			}
		} else {
			// 正在加载，什么也不做
			view.onHeaderRefreshComplete();
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		int dataIdx = position - _listView.getHeaderViewsCount();
		if (dataIdx >= _dataList.size()) {
			return;
		}
		Intent intent = new Intent(DoctorsActivity.this,
				DoctorDetailActivity.class);
		Bundle data = new Bundle();
		DoctorBean db = _dataList.get(dataIdx);
		data.putInt("doctorId", db.getId());
		intent.putExtra(AppBuildConfig.BUNDLEKEY, data);
		startActivity(intent);
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		if (_refreshAction == RefreshAction.NONE) {
			if (_bGetMoreEnable) {// 有加载更多
				int lastPos = _listView.getLastVisiblePosition();
				int total = _listView.getHeaderViewsCount() + _dataList.size()
						+ _listView.getFooterViewsCount();
				if (lastPos == total - 1) {
					// 加载更多显示出来了，开始加载
					_refreshAction = RefreshAction.LOADMORE;
					setGetMoreContent("正在加载...", true, true);
					if (isSearch) {
						getSearchData(keyword, _searchNum + 1, false);
					} else {
						getData(_pageNum + 1, _doctorType, false);
					}
				}
			}
		}
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		// TODO Auto-generated method stub

	}

	@Override
	public void afterTextChanged(Editable s) {
		if (s == null || TextUtils.isEmpty(s.toString().trim())) {
			_searchNum = 1;
			if (_sourceList.size() > 0) {
				_dataList.clear();
				_dataList.addAll(_sourceList);
				_adapter.notifyDataSetChanged();
			} else {
				getData(1, _doctorType, false);
			}

		}
	}

}
