package com.ddoctor.user.activity.report;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.enums.RetError;
import com.ddoctor.user.activity.BaseActivity;
import com.ddoctor.user.activity.sugar.BloodSugarRecordListActivity;
import com.ddoctor.user.activity.sugar.SugarLineChartActivity;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.model.ReportStatusBean;
import com.ddoctor.user.model.ReportStatusBean.ReportStatusType;
import com.ddoctor.user.task.GetHealthEvaluationReportTask;
import com.ddoctor.user.task.TaskPostCallBack;
import com.ddoctor.user.view.RoundProgressBar;
import com.ddoctor.user.wapi.bean.EvaluationBean;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.DialogUtil;
import com.ddoctor.utils.MedicalDBUtil;
import com.ddoctor.utils.MyUtils;
import com.ddoctor.utils.StringUtils;
import com.ddoctor.utils.ToastUtil;
import com.umeng.analytics.MobclickAgent;
import com.zhuge.analysis.stat.ZhugeSDK;

/**
 * 
 * @author 萧
 * @Date 2015-6-28下午2:06:06
 * @TODO 只显示结果百分比 不显示异常提示 不提供异常点击
 */
public class HealthReportActivity extends BaseActivity {

	private LinearLayout _layout_line;
	private LinearLayout _layout_record;
	private RoundProgressBar mRoundProgressBar;
	private TextView _tv_last_result;

	private ImageView sugar_img_status;
	private ImageView diet_img_status;
	private ImageView sport_img_status;
	private ImageView medical_img_status;
	private TextView sugar_tv_status;
	private TextView diet_tv_status;
	private TextView sport_tv_status;
	private TextView medical_tv_status;
	// private TextView sugar_tv_error;
	// private TextView diet_tv_error;
	// private TextView sport_tv_error;
	// private TextView medical_tv_error;
	private TextView sugar_tv_progress;
	private TextView diet_tv_progress;
	private TextView sport_tv_progress;
	private TextView medical_tv_progress;

	private ReportStatusBean sugarStatus = new ReportStatusBean(
			ReportStatusType.AnalysisNormal);
	private ReportStatusBean dietStatus = new ReportStatusBean(
			ReportStatusType.AnalysisNormal);
	private ReportStatusBean sportStatus = new ReportStatusBean(
			ReportStatusType.AnalysisNormal);
	private ReportStatusBean medicalStatus = new ReportStatusBean(
			ReportStatusType.AnalysisNormal);

	private String format;
	private String sugar_normal;
	private String diet_normal;
	private String sport_normal;
	private String medical_normal;
	private String status_analysing;

	private int[] statusDrawable = new int[] {
			R.drawable.report_status_analysying,
			R.drawable.report_status_error, R.drawable.report_status_normal };

	private void initData() {
		status_analysing = getResources().getString(
				R.string.sugar_report_status_analysing);
		format = getResources().getString(R.string.sugar_report_progress);
		sugar_normal = getResources().getString(R.string.sugar_report_sugar);
		diet_normal = getResources().getString(R.string.sugar_report_diet);
		sport_normal = getResources().getString(R.string.sugar_report_sport);
		medical_normal = getResources()
				.getString(R.string.sugar_report_medical);
	}

	private enum AnalysisType {
		TYPE_SUGAR, TYPE_DIET, TYPE_SPORT, TYPE_MEDICAL
	}

	/**
	 * 
	 * @param analysisStaus
	 *            1 分析结束 结果异常 2分析结束 正常 默认0 正在分析
	 * @return
	 */
	private String getStatusById(int analysisStaus, AnalysisType type) {
		String result = status_analysing;
		switch (analysisStaus) {
		case 1:
		case 2: {
			if (type == AnalysisType.TYPE_SUGAR) {
				result = sugar_normal;
			} else if (type == AnalysisType.TYPE_DIET) {
				result = diet_normal;
			} else if (type == AnalysisType.TYPE_SPORT) {
				result = sport_normal;
			} else if (type == AnalysisType.TYPE_MEDICAL) {
				result = medical_normal;
			}
		}
			break;
		default:
			break;
		}
		return result;
	}

	private void showStatusUI(String lasttime) {
		sugar_img_status.setBackgroundResource(statusDrawable[sugarStatus
				.getAnalysisStatus().ordinal()]);
		sugar_tv_progress.setText(String.format(format,
				sugarStatus.getProgress()));
		sugar_tv_status.setText(getStatusById(sugarStatus.getAnalysisStatus()
				.ordinal(), AnalysisType.TYPE_SUGAR));
		setRoundProgress(sugarStatus.getProgress() / 4);

		// if (sugarStatus.getAnalysisStatus() == 1) {
		// sugar_tv_error.setVisibility(View.VISIBLE);
		// }

		diet_img_status.setBackgroundResource(statusDrawable[dietStatus
				.getAnalysisStatus().ordinal()]);
		diet_tv_progress
				.setText(String.format(format, dietStatus.getProgress()));
		diet_tv_status.setText(getStatusById(dietStatus.getAnalysisStatus()
				.ordinal(), AnalysisType.TYPE_DIET));
		setRoundProgress((dietStatus.getProgress() + sugarStatus.getProgress()) / 4);
		// setRoundProgress((sugarStatus.getProgress()+sugarStatus.getProgress())/4);
		// if (dietStatus.getAnalysisStatus() == 1) {
		// diet_tv_error.setVisibility(View.VISIBLE);
		// }

		sport_img_status.setBackgroundResource(statusDrawable[sportStatus
				.getAnalysisStatus().ordinal()]);
		sport_tv_progress.setText(String.format(format,
				sportStatus.getProgress()));
		sport_tv_status.setText(getStatusById(sportStatus.getAnalysisStatus()
				.ordinal(), AnalysisType.TYPE_SPORT));
		// if (sportStatus.getAnalysisStatus() == 1) {
		// sport_tv_error.setVisibility(View.VISIBLE);
		// }
		setRoundProgress((sportStatus.getProgress() + sugarStatus.getProgress() + dietStatus
				.getProgress()) / 4);
		// setRoundProgress((sugarStatus.getProgress()+sugarStatus.getProgress()+sugarStatus.getProgress())/4);

		medical_img_status.setBackgroundResource(statusDrawable[medicalStatus
				.getAnalysisStatus().ordinal()]);
		medical_tv_progress.setText(String.format(format,
				medicalStatus.getProgress()));
		medical_tv_status.setText(getStatusById(medicalStatus
				.getAnalysisStatus().ordinal(), AnalysisType.TYPE_MEDICAL));
		// if (medicalStatus.getAnalysisStatus() == 1) {
		// medical_tv_error.setVisibility(View.VISIBLE);
		// }
		setRoundProgress((sportStatus.getProgress() + sugarStatus.getProgress()
				+ dietStatus.getProgress() + medicalStatus.getProgress()) / 4);
		if (!TextUtils.isEmpty(lasttime)) {
			_tv_last_result.setText(String.format(
					getString(R.string.sugar_report_last), lasttime));
			_tv_last_result.setVisibility(View.VISIBLE);
		}
		// setRoundProgress((sugarStatus.getProgress()+sugarStatus.getProgress()+sugarStatus.getProgress()+sugarStatus.getProgress())/4);

	}

	private int _medicalcount = 0;

	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("HealthReportActivity");
		MobclickAgent.onResume(HealthReportActivity.this);
		ZhugeSDK.getInstance().init(getApplicationContext());
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("HealthReportActivity");
		MobclickAgent.onPause(HealthReportActivity.this);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_healthreport);
		_medicalcount = MedicalDBUtil.getIntance()
				.selectMedicalCountLatestWeek();
		initData();
		findViewById();
		EvaluationBean evaluationBean = DataModule.getInstance()
				.getHealthReportResult();
		if (evaluationBean != null) {
			initReportStatus(evaluationBean);
		}else {
			loadingData();
		}
		showStatusUI(evaluationBean!=null?evaluationBean.getEvaluationTime():null);

	}

	int progress;
	private int _currentProgress = 0;

	private void setRoundProgress(final int max) {
		MyUtils.showLog("setRoundProgress max = " + max);

		new Thread(new Runnable() {

			@Override
			public void run() {
				while (progress < max) {
					progress += 1;
					mRoundProgressBar.setProgress(progress);
					try {
						Thread.sleep(80);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				_currentProgress = max;
				progress = _currentProgress;
			}
		}).start();
	}

	public void findViewById() {
		this.setTitle(getResources().getString(R.string.sugar_report_title));
		Button rightBtn = getRightButtonText(getString(R.string.sugar_report_judge));
		Button leftBtn = getLeftButtonText(getString(R.string.basic_back));
		findViewById(R.id.titleBar).setBackgroundColor(Color.TRANSPARENT);
		mRoundProgressBar = (RoundProgressBar) findViewById(R.id.report_circle_progress);
		_tv_last_result = (TextView) findViewById(R.id.report_tv_last);
		_layout_line = (LinearLayout) findViewById(R.id.report_line_layout);
		_layout_record = (LinearLayout) findViewById(R.id.report_record_layout);

		sugar_img_status = (ImageView) findViewById(R.id.report_sugar_img_status);
		sugar_tv_status = (TextView) findViewById(R.id.report_sugar_tv_status);

		// sugar_tv_error = (TextView) findViewById(R.id.report_sugar_tv_error);

		sugar_tv_progress = (TextView) findViewById(R.id.report_sugar_tv_progress);

		diet_img_status = (ImageView) findViewById(R.id.report_diet_img_status);
		diet_tv_status = (TextView) findViewById(R.id.report_diet_tv_status);

		// diet_tv_error = (TextView) findViewById(R.id.report_diet_tv_error);

		diet_tv_progress = (TextView) findViewById(R.id.report_diet_tv_progress);

		sport_img_status = (ImageView) findViewById(R.id.report_sport_img_status);
		sport_tv_status = (TextView) findViewById(R.id.report_sport_tv_status);

		// sport_tv_error = (TextView) findViewById(R.id.report_sport_tv_error);

		sport_tv_progress = (TextView) findViewById(R.id.report_sport_tv_progress);

		medical_img_status = (ImageView) findViewById(R.id.report_medical_img_status);
		medical_tv_status = (TextView) findViewById(R.id.report_medical_tv_status);

		// medical_tv_error = (TextView)
		// findViewById(R.id.report_medical_tv_error);

		medical_tv_progress = (TextView) findViewById(R.id.report_medical_tv_progress);

		leftBtn.setOnClickListener(this);
		rightBtn.setOnClickListener(this);
		_layout_line.setOnClickListener(this);
		_layout_record.setOnClickListener(this);

	}

	private Dialog _loadingDialog;

	private void loadingData() {
		_loadingDialog = DialogUtil.createLoadingDialog(this);
		_loadingDialog.show();

		GetHealthEvaluationReportTask task = new GetHealthEvaluationReportTask(
				_medicalcount);
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {

			@Override
			public void taskFinish(DDResult result) {
				_loadingDialog.dismiss();
				if (result.getError() == RetError.NONE) {
					EvaluationBean evaluationBean = result.getBundle()
							.getParcelable("data");
					DataModule.getInstance().saveHealthReportResult(
							evaluationBean);
					initReportStatus(evaluationBean);
					showStatusUI(evaluationBean.getEvaluationTime());
				} else {
					ToastUtil.showToast(result.getErrorMessage());
				}
			}  
		});
		task.executeParallel("");

	}

	private void initReportStatus(EvaluationBean evaluationBean) {
		sugarStatus.setProgress(stringformat(evaluationBean.getSugar()));
		dietStatus.setProgress(stringformat(evaluationBean.getDiet()));
		medicalStatus.setProgress(stringformat(evaluationBean.getMedical()));
		sportStatus.setProgress(stringformat(evaluationBean.getSport()));
	}

	private int stringformat(String value) {
		if (StringUtils.pureNum(value)) {
			return Integer.valueOf(value);
		} else {
			int indexOf = value.lastIndexOf('%');
			return Integer.valueOf(value.substring(0, indexOf));
		}
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.btn_left: {
			finishThisActivity();
		}
			break;
		case R.id.btn_right: {
			loadingData();
			// generateAnalysisStatus();
		}
			break;
		case R.id.report_line_layout: {
			skip(SugarLineChartActivity.class, false);
		}
			break;
		case R.id.report_record_layout: {
			Intent intent = new Intent();
			intent.setClass(HealthReportActivity.this,
					BloodSugarRecordListActivity.class);
			startActivity(intent);
		}
			break;
		default:
			break;
		}
	}

}
