package com.ddoctor.user.activity.questionnaire;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

import com.beichen.user.R;

public class Survey1Fragment2 extends Fragment implements
		OnCheckedChangeListener, CompoundButton.OnCheckedChangeListener {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	private EditText met_histroy, met_oral, met_insulin, met_lowprotein;
	private RadioGroup mrg_ever_edu, mrg_regular_review, mrg_smbg,
			mrg_lowprotein;
	private LinearLayout mlayout_ever_edu;
	private CheckBox mcb_ever_edu1, mcb_ever_edu2, mcb_ever_edu3,
			mcb_ever_edu4, mcb_ever_edu5, mcb_ever_edu6, mcb_ever_edu7;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.layout_survey1fragement2, null);
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		findViewById(view);
	}

	private void findViewById(View view) {
		met_histroy = (EditText) view.findViewById(R.id.suvery1_et_history);
		met_oral = (EditText) view.findViewById(R.id.suvery1_et_oral);
		met_insulin = (EditText) view.findViewById(R.id.suvery1_et_insulin);
		met_lowprotein = (EditText) view
				.findViewById(R.id.suvery1_et_lowprotein);
		mrg_ever_edu = (RadioGroup) view.findViewById(R.id.rg_ever_edu);
		mrg_regular_review = (RadioGroup) view
				.findViewById(R.id.rg_regularly_review);
		mrg_smbg = (RadioGroup) view.findViewById(R.id.rg_smbg);
		mrg_lowprotein = (RadioGroup) view.findViewById(R.id.rg_lowprotein);
		mlayout_ever_edu = (LinearLayout) view
				.findViewById(R.id.layout_ever_edu);
		mcb_ever_edu1 = (CheckBox) view.findViewById(R.id.ever_edu_cb1);
		mcb_ever_edu2 = (CheckBox) view.findViewById(R.id.ever_edu_cb2);
		mcb_ever_edu3 = (CheckBox) view.findViewById(R.id.ever_edu_cb3);
		mcb_ever_edu4 = (CheckBox) view.findViewById(R.id.ever_edu_cb4);
		mcb_ever_edu5 = (CheckBox) view.findViewById(R.id.ever_edu_cb5);
		mcb_ever_edu6 = (CheckBox) view.findViewById(R.id.ever_edu_cb6);
		mcb_ever_edu7 = (CheckBox) view.findViewById(R.id.ever_edu_cb7);

		mrg_ever_edu.setOnCheckedChangeListener(this);
		mrg_regular_review.setOnCheckedChangeListener(this);
		mrg_smbg.setOnCheckedChangeListener(this);
		mrg_lowprotein.setOnCheckedChangeListener(this);

		mcb_ever_edu1.setOnCheckedChangeListener(this);
		mcb_ever_edu1.setOnCheckedChangeListener(this);
		mcb_ever_edu1.setOnCheckedChangeListener(this);
		mcb_ever_edu1.setOnCheckedChangeListener(this);
		mcb_ever_edu1.setOnCheckedChangeListener(this);
		mcb_ever_edu1.setOnCheckedChangeListener(this);
		mcb_ever_edu1.setOnCheckedChangeListener(this);

	}

	@Override
	public void onStart() {
		super.onStart();
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		switch (group.getId()) {
		case R.id.rg_ever_edu: {
			mlayout_ever_edu
					.setVisibility(checkedId == R.id.rb_ever_edu_true ? View.VISIBLE
							: View.GONE);
		}
			break;
		case R.id.rg_regularly_review: {
			if (checkedId == R.id.rb_regularly_review_true) {

			} else if (checkedId == R.id.rb_regularly_review_false) {

			}
		}
			break;
		case R.id.rg_smbg: {
			if (checkedId == R.id.rb_smbg_true) {

			} else if (checkedId == R.id.rb_smbg_false) {

			}
		}
			break;
		case R.id.rg_lowprotein: {
			if (checkedId == R.id.rb_lowprotein_true) {

			} else if (checkedId == R.id.rb_lowprotein_false) {

			}
		}
			break;
		default:
			break;
		}
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		switch (buttonView.getId()) {
		case R.id.ever_edu_cb1: {
			
		}
			break;
		case R.id.ever_edu_cb2: {

		}
			break;
		case R.id.ever_edu_cb3: {

		}
			break;
		case R.id.ever_edu_cb4: {

		}
			break;
		case R.id.ever_edu_cb5: {

		}
			break;
		case R.id.ever_edu_cb6: {

		}
			break;
		case R.id.ever_edu_cb7: {

		}
			break;

		default:
			break;
		}
	}
}
