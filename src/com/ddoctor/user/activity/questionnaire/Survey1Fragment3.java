package com.ddoctor.user.activity.questionnaire;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

import com.beichen.user.R;

public class Survey1Fragment3 extends Fragment implements
		OnCheckedChangeListener {

	private EditText met_fpg, met_2hpg, met_hba1c, met_cr, met_tc, met_tg,
			met_malb, met_nsd, met_medicalcost;
	private RadioGroup mrg_medicalcost, mrg_medicalheavel;
	private LinearLayout mlayout_medicalcost;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.layout_survey1fragement3, null);
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		findViewById(view);
	}

	private void findViewById(View view) {
		met_fpg = (EditText) view.findViewById(R.id.survey1_et_fpg);
		met_2hpg = (EditText) view.findViewById(R.id.survey1_et_2hpg);
		met_hba1c = (EditText) view.findViewById(R.id.survey1_et_hba1c);
		met_cr = (EditText) view.findViewById(R.id.survey1_et_cr);
		met_tc = (EditText) view.findViewById(R.id.survey1_et_tc);
		met_tg = (EditText) view.findViewById(R.id.survey1_et_tg);
		met_malb = (EditText) view.findViewById(R.id.survey1_et_malb);
		met_nsd = (EditText) view.findViewById(R.id.survey1_et_nsd);
		met_medicalcost = (EditText) view.findViewById(R.id.medicalcost_et);

		mrg_medicalcost = (RadioGroup) view.findViewById(R.id.medicalcost_rg);
		mrg_medicalheavel = (RadioGroup) view
				.findViewById(R.id.medicalcost_heavel_rg);

		mlayout_medicalcost = (LinearLayout) view
				.findViewById(R.id.survey1_layout_medicalcost);

		mrg_medicalcost.setOnCheckedChangeListener(this);
		mrg_medicalheavel.setOnCheckedChangeListener(this);

	}

	@Override
	public void onStart() {
		super.onStart();
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		switch (group.getId()) {
		case R.id.medicalcost_rg: {
			mlayout_medicalcost.setVisibility(checkedId == R.id.rb_cost_self?View.VISIBLE:View.GONE);
		}
			break;
		case R.id.medicalcost_heavel_rg: {
			
		}
			break;

		default:
			break;
		}
	}

}
