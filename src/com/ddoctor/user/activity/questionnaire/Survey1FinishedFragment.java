package com.ddoctor.user.activity.questionnaire;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.beichen.user.R;
import com.ddoctor.user.fragment.BaseFragment;
import com.umeng.analytics.MobclickAgent;

public class Survey1FinishedFragment extends BaseFragment{

	@Override
	public void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("Survey1FinishedFragment");
	}
	
	@Override
	public void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("Survey1FinishedFragment");
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.layout_survey1finished, null);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
	}
	
}
