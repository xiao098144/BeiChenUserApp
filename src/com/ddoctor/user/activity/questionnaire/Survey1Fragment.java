package com.ddoctor.user.activity.questionnaire;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.beichen.user.R;
import com.ddoctor.enums.RetError;
import com.ddoctor.interfaces.OnClickCallBackListener;
import com.ddoctor.user.activity.mine.MyInfoActivity;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.fragment.BaseFragment;
import com.ddoctor.user.task.TaskPostCallBack;
import com.ddoctor.user.task.UploadSurvey1Task;
import com.ddoctor.user.wapi.bean.IllnessBean;
import com.ddoctor.user.wapi.bean.PatientBean;
import com.ddoctor.user.wapi.bean.QuestionjbzlBean;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.DialogUtil;
import com.ddoctor.utils.MyUtils;
import com.ddoctor.utils.TimeUtil;
import com.ddoctor.utils.ToastUtil;
import com.umeng.analytics.MobclickAgent;
import com.zhuge.analysis.stat.ZhugeSDK;

public class Survey1Fragment extends BaseFragment implements
		OnCheckedChangeListener,OnClickCallBackListener {

	String name1, gender1, birthday1, marriage1, edu1, job1, cig1, drink1;
	private EditText table1_name, table1_mobile, table1_visite;
	private TextView table1_date;
	EditText et_name, tv_birthday;
	TextView tv_sex, tv_marriage, tv_edu, tv_job;
	private RadioGroup rg_cig, rg_drink;
	private LinearLayout cig_layout, drink_layout;
	private EditText met_cig, met_drink;
	private EditText et_height, et_weight, et_shousuo, et_shuzhang;
	private EditText et_bmi;

	private EditText met_histroy, met_oral, met_insulin, met_lowprotein;
	private RadioGroup mrg_ever_edu, mrg_regular_review, mrg_smbg,
			mrg_lowprotein;
	private LinearLayout mlayout_ever_edu;
	private CheckBox mcb_ever_edu1, mcb_ever_edu2, mcb_ever_edu3,
			mcb_ever_edu4, mcb_ever_edu5, mcb_ever_edu6, mcb_ever_edu7;

	private LinearLayout mlayout_reguralyreview, mlayout_selfjiandu,
			mlayout_lowproteinfood;
	private EditText met_reguraly, met_selfjiandu, met_lowproteinCount;

	private EditText met_fpg, met_2hpg, met_hba1c, met_cr, met_tc, met_tg,
			met_malb, met_nsd, met_medicalcost;
	private RadioGroup mrg_medicalcost, mrg_medicalheavel;
	private LinearLayout mlayout_medicalcost;

	int childCount = 3;
	private int _currentPosition = 0;

	private Button mbtn_last, mbtn_next;

	private ViewFlipper mViewFlipper;
   
	PatientBean patientBean;

	@Override
	public void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("Survey1Fragment");
	}
	
	@Override
	public void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("Survey1Fragment");
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.layout_survey1, null);
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		patientBean = DataModule.getInstance().getLoginedUserInfo();
		findviewById(view);
	}

	private void findviewById(View view) {
		table1_date = (TextView) view.findViewById(R.id.table1_date);
		table1_name = (EditText) view.findViewById(R.id.table1_name);
		table1_mobile = (EditText) view.findViewById(R.id.table1_phone);
		table1_visite = (EditText) view.findViewById(R.id.table1_visitbymobile);

		table1_date.setOnClickListener(this);
		table1_date.setText(TimeUtil.getInstance().getStandardDate(
				getString(R.string.time_format_10)));
		table1_name.setText(patientBean.getName());
		table1_mobile.setText(patientBean.getMobile());

		et_name = (EditText) view.findViewById(R.id.updateinfo_et_name1);
		et_name.setText(patientBean.getName());

		tv_birthday = (EditText) view
				.findViewById(R.id.updateinfo_tv_birthday1);
		tv_birthday.setText(String.valueOf(patientBean.getAgeByBirthday(patientBean.getBirthday())));
		tv_sex = (TextView) view.findViewById(R.id.updateinfo_tv_sex1);
		tv_sex.setText(patientBean.getSex() == null ? "男" : patientBean
				.getSex() == 0 ? "男" : "女");
		tv_sex.setTag(patientBean.getSex() == null ? 0
				: patientBean.getSex() == 0 ? 0 : 1);

		tv_marriage = (TextView) view
				.findViewById(R.id.updateinfo_tv_marriage1);
		tv_edu = (TextView) view.findViewById(R.id.updateinfo_tv_edu1);
		tv_job = (TextView) view.findViewById(R.id.updateinfo_tv_job1);
		rg_cig = (RadioGroup) view.findViewById(R.id.rg_cig);
		met_cig = (EditText) view.findViewById(R.id.et_cig1);
		met_drink = (EditText) view.findViewById(R.id.et_drink);
		cig_layout = (LinearLayout) view.findViewById(R.id.table1_cig_layout);
		drink_layout = (LinearLayout) view
				.findViewById(R.id.table1_drink_layout);
		rg_drink = (RadioGroup) view.findViewById(R.id.rg_drink);

		et_height = (EditText) view.findViewById(R.id.updateinfo_et_height);
		et_weight = (EditText) view.findViewById(R.id.updateinfo_et_weight);
		et_bmi = (EditText) view.findViewById(R.id.updateinfo_et_bmi);
		et_shousuo = (EditText) view.findViewById(R.id.updateinfo_et_shousuo);
		et_shuzhang = (EditText) view.findViewById(R.id.updateinfo_et_shuzhang);

		et_height.setText(String.valueOf(patientBean.getHeight()));
		et_weight.setText(String.format("%.0f", patientBean.getWeight()));
		et_bmi.setText(String.valueOf(DataModule.getBMI(
				patientBean.getHeight(), patientBean.getWeight())));

		rg_cig.setOnCheckedChangeListener(this);
		rg_drink.setOnCheckedChangeListener(this);

		tv_birthday.setOnClickListener(this);
		tv_sex.setOnClickListener(this);
		tv_marriage.setOnClickListener(this);
		tv_edu.setOnClickListener(this);
		tv_job.setOnClickListener(this);

		met_histroy = (EditText) view.findViewById(R.id.suvery1_et_history);
		met_oral = (EditText) view.findViewById(R.id.suvery1_et_oral);
		met_insulin = (EditText) view.findViewById(R.id.suvery1_et_insulin);
		met_lowprotein = (EditText) view
				.findViewById(R.id.suvery1_et_lowprotein);
		mrg_ever_edu = (RadioGroup) view.findViewById(R.id.rg_ever_edu);
		mrg_regular_review = (RadioGroup) view
				.findViewById(R.id.rg_regularly_review);
		mrg_smbg = (RadioGroup) view.findViewById(R.id.rg_smbg);
		mrg_lowprotein = (RadioGroup) view.findViewById(R.id.rg_lowprotein);
		mlayout_reguralyreview = (LinearLayout) view
				.findViewById(R.id.table1_regularly_review_layout);
		met_reguraly = (EditText) view.findViewById(R.id.et_regularly_review);
		mlayout_selfjiandu = (LinearLayout) view
				.findViewById(R.id.table1_smbg_layout);
		met_selfjiandu = (EditText) view.findViewById(R.id.et_smbg_review);
		mlayout_lowproteinfood = (LinearLayout) view
				.findViewById(R.id.table1_lowprotein_layout);
		met_lowproteinCount = (EditText) view
				.findViewById(R.id.et_lowprotein_review);
		mlayout_ever_edu = (LinearLayout) view
				.findViewById(R.id.layout_ever_edu);
		mcb_ever_edu1 = (CheckBox) view.findViewById(R.id.ever_edu_cb1);
		mcb_ever_edu2 = (CheckBox) view.findViewById(R.id.ever_edu_cb2);
		mcb_ever_edu3 = (CheckBox) view.findViewById(R.id.ever_edu_cb3);
		mcb_ever_edu4 = (CheckBox) view.findViewById(R.id.ever_edu_cb4);
		mcb_ever_edu5 = (CheckBox) view.findViewById(R.id.ever_edu_cb5);
		mcb_ever_edu6 = (CheckBox) view.findViewById(R.id.ever_edu_cb6);
		mcb_ever_edu7 = (CheckBox) view.findViewById(R.id.ever_edu_cb7);

		mrg_ever_edu.setOnCheckedChangeListener(this);
		mrg_regular_review.setOnCheckedChangeListener(this);
		mrg_smbg.setOnCheckedChangeListener(this);
		mrg_lowprotein.setOnCheckedChangeListener(this);

		met_fpg = (EditText) view.findViewById(R.id.survey1_et_fpg);
		met_2hpg = (EditText) view.findViewById(R.id.survey1_et_2hpg);
		met_hba1c = (EditText) view.findViewById(R.id.survey1_et_hba1c);
		met_cr = (EditText) view.findViewById(R.id.survey1_et_cr);
		met_tc = (EditText) view.findViewById(R.id.survey1_et_tc);
		met_tg = (EditText) view.findViewById(R.id.survey1_et_tg);
		met_malb = (EditText) view.findViewById(R.id.survey1_et_malb);
		met_nsd = (EditText) view.findViewById(R.id.survey1_et_nsd);
		met_medicalcost = (EditText) view.findViewById(R.id.medicalcost_et);

		mrg_medicalcost = (RadioGroup) view.findViewById(R.id.medicalcost_rg);
		mrg_medicalheavel = (RadioGroup) view
				.findViewById(R.id.medicalcost_heavel_rg);

		mlayout_medicalcost = (LinearLayout) view
				.findViewById(R.id.survey1_layout_medicalcost);

		mrg_medicalcost.setOnCheckedChangeListener(this);
		mrg_medicalheavel.setOnCheckedChangeListener(this);

		mbtn_last = (Button) view.findViewById(R.id.btn_last);
		mbtn_next = (Button) view.findViewById(R.id.btn_next);
		mbtn_last.setOnClickListener(this);
		mbtn_next.setOnClickListener(this);

		mViewFlipper = (ViewFlipper) view.findViewById(R.id.viewflipper);

		for (int i = 0; i <= (TimeUtil.getInstance().getCurrentYear() - 1940); i++) {
			_yearList.add(String.valueOf(1940 + i));
		}
		for (int i = 0; i < 2; i++) {
			IllnessBean illnessBean = new IllnessBean();
			illnessBean.setType(String.valueOf(Survey1Type.SEX.ordinal()));
			illnessBean.setId(i % 2);
			illnessBean.setName(i == 0 ? "男" : "女");
			_genderList.add(illnessBean);
		}

	}

	private List<String> _yearList = new ArrayList<String>();
	private List<IllnessBean> _genderList = new ArrayList<IllnessBean>();

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		switch (group.getId()) {
		case R.id.rg_cig: {
			cig_layout
					.setVisibility(checkedId == R.id.rb_cig_true ? View.VISIBLE
							: View.GONE);
		}
			break;
		case R.id.rg_drink: {
			drink_layout
					.setVisibility(checkedId == R.id.rb_drink_true ? View.VISIBLE
							: View.GONE);
		}
			break;

		case R.id.rg_ever_edu: {
			mlayout_ever_edu
					.setVisibility(checkedId == R.id.rb_ever_edu_true ? View.VISIBLE
							: View.GONE);
		}
			break;
		case R.id.rg_regularly_review: {
			if (checkedId == R.id.rb_regularly_review_true) {
				mlayout_reguralyreview.setVisibility(View.VISIBLE);
			} else if (checkedId == R.id.rb_regularly_review_false) {
				mlayout_reguralyreview.setVisibility(View.GONE);
			}
		}
			break;
		case R.id.rg_smbg: {
			if (checkedId == R.id.rb_smbg_true) {
				mlayout_selfjiandu.setVisibility(View.VISIBLE);
			} else if (checkedId == R.id.rb_smbg_false) {
				mlayout_selfjiandu.setVisibility(View.GONE);
			}
		}
			break;
		case R.id.rg_lowprotein: {
			if (checkedId == R.id.rb_lowprotein_true) {
				mlayout_lowproteinfood.setVisibility(View.VISIBLE);
			} else if (checkedId == R.id.rb_lowprotein_false) {
				mlayout_lowproteinfood.setVisibility(View.GONE);
			}
		}
			break;
		case R.id.medicalcost_rg: {
			mlayout_medicalcost
					.setVisibility(checkedId == R.id.rb_cost_self ? View.VISIBLE
							: View.GONE);
		}
			break;
		case R.id.medicalcost_heavel_rg: {

			switch (checkedId) {
			case R.id.rb_cost_heavel1: {
				 survey1Bean.setJjfd("完全没有负担");
			}
				break;
			case R.id.rb_cost_heavel2: {
				survey1Bean.setJjfd("有一些负担");
			}
				break;
			case R.id.rb_cost_heavel3: {
				survey1Bean.setJjfd("相当沉重的负担");
			}
				break;

			default:
				break;
			}
		}
			break;

		default:
			break;
		}
	}

//	private Survey1Bean survey1Bean = new Survey1Bean();
	private QuestionjbzlBean survey1Bean = new QuestionjbzlBean();
	
	private enum Survey1Type {
		DATE, YEAR, SEX, MARRIAGE, JOB, EDU
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.table1_date: {
			DialogUtil.showDatePicker(getActivity(), 1970, 0,0,this,
					Survey1Type.YEAR.ordinal());
		}
			break;
		case R.id.updateinfo_tv_birthday1: {
			DialogUtil.showYearPickerDialog(getActivity(), this, _yearList,
					Survey1Type.DATE.ordinal(), 40);
		}
			break;
		case R.id.updateinfo_tv_sex1: {
			DialogUtil.showSingleChoiceDialog(getActivity(), _genderList, this,
					Survey1Type.SEX.ordinal());
		}
			break;
		case R.id.updateinfo_tv_marriage1: {
			DialogUtil.showSingleChoiceDialog(getActivity(), DialogUtil
					.generateData(res.getStringArray(R.array.marriage_status)),
					this, Survey1Type.MARRIAGE.ordinal());
		}
			break;
		case R.id.updateinfo_tv_job1: {
			DialogUtil.showSingleChoiceDialog(getActivity(), DialogUtil
					.generateData(res.getStringArray(R.array.job_type)), this,
					Survey1Type.JOB.ordinal());
		}
			break;
		case R.id.updateinfo_tv_edu1: {
			DialogUtil.showSingleChoiceDialog(getActivity(), DialogUtil
					.generateData(res.getStringArray(R.array.edu_level)), this,
					Survey1Type.EDU.ordinal());
		}
			break;
		case R.id.btn_last: {
			if (_currentPosition > 0) {
				_currentPosition--;
				mViewFlipper.showPrevious();
				if (mViewFlipper.getDisplayedChild() < 1) {
					if (_currentPosition == 0) {
						mbtn_last.setVisibility(View.GONE);
					}
					if (("完成").equals(mbtn_next.getText().toString())) {
						mbtn_next.setText("下一页");
					}
				}
			}
		}
			break;
		case R.id.btn_next: {
			if (_currentPosition == childCount - 1) {
				mbtn_next.setText("完成");
				if (checkData3()) {
					MyUtils.showLog("","填完最后一页 "+survey1Bean.toString());
					uploadResult(survey1Bean);
				}
			}
			if (_currentPosition == 1) {
				if (checkData2()) {
					MyUtils.showLog("","填完第二页 "+survey1Bean.toString());
					if (_currentPosition < childCount - 1) {
						_currentPosition++;
						mViewFlipper.showNext();
						if (mViewFlipper.getDisplayedChild() > 0) {
							if (_currentPosition > 0) {
								mbtn_last.setVisibility(View.VISIBLE);
							}
						}
					}
				}
			}
			if (_currentPosition == 0) {
				if (checkData1()) {
					MyUtils.showLog("","填完第一页 "+survey1Bean.toString());
					if (_currentPosition < childCount - 1) {
						_currentPosition++;
						mViewFlipper.showNext();
						if (mViewFlipper.getDisplayedChild() > 0) {
							if (_currentPosition > 0) {
								mbtn_last.setVisibility(View.VISIBLE);
							}
						}
					}
				}
			}
			
			if (_currentPosition == childCount - 1) {
				mbtn_next.setText("完成");
			}
		}
			break;
		}
	}
	

	/**
	 * 添加自定义事件
	 * @param eventId
	 * @param eventName
	 */
	private void addEvent(String eventId , String eventName){
		MobclickAgent.onEvent(getActivity(), eventId);
		ZhugeSDK.getInstance().onEvent(getActivity(), eventName);
	}

	private void uploadResult(QuestionjbzlBean survey1Bean){
		addEvent("500002", getActivity().getResources().getString(R.string.event_dcwj_500002));
		UploadSurvey1Task task = new UploadSurvey1Task(survey1Bean);
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {
			
			@Override
			public void taskFinish(DDResult result) {
				if (result.getError() == RetError.NONE) {
					ToastUtil.showToast("提交成功");
					DataModule.getInstance().setSurvey1State(true);
					onClickCallBackListener.onClickCallBack(null);
				} else {
					ToastUtil.showToast(result.getErrorMessage());
				}
			}
		});
		task.executeParallel("");
	}
	
	private boolean checkData1() {
		String date = table1_date.getText().toString();
		String name = table1_name.getText().toString().trim();
		if (TextUtils.isEmpty(name)) {
			ToastUtil.showToast("姓名不能为空");
			return false;
		}
		String mobile = table1_mobile.getText().toString().trim();
		if (TextUtils.isEmpty(mobile)) {
			ToastUtil.showToast("电话不能为空");
			return false;
		}
		String visit = table1_visite.getText().toString().trim();
		if (TextUtils.isEmpty(visit)) {
			ToastUtil.showToast("随访时间不能为空");
			return false;
		}
		String age = tv_birthday.getText().toString().trim();
		if (TextUtils.isEmpty(age)) {
			ToastUtil.showToast("年龄不能为空");
			return false;
		}
		String sex = tv_sex.getText().toString();
		if (TextUtils.isEmpty(sex)) {
			ToastUtil.showToast("性别不能为空");
			return false;
		}
		String marriage = tv_marriage.getText().toString().trim();
		if (TextUtils.isEmpty(marriage)) {
			ToastUtil.showToast("婚姻状态不能为空");
			return false;
		}
		String edu = tv_edu.getText().toString().trim();
		if (TextUtils.isEmpty(edu)) {
			ToastUtil.showToast("文化程度不能为空");
			return false;
		}
		String job = tv_job.getText().toString().trim();
		if (TextUtils.isEmpty(job)) {
			ToastUtil.showToast("职业不能为空");
			return false;
		}
		if (rg_cig.getCheckedRadioButtonId() == R.id.rb_cig_false) {
			survey1Bean.setSmokeCount("0");
		} else {
			String cigcount = met_cig.getText().toString().trim();
			if (TextUtils.isEmpty(cigcount)) {
				ToastUtil.showToast("请填写每天吸烟的数量");
				return false;
			}
			survey1Bean.setSmokeCount(cigcount);
		}
		if (rg_drink.getCheckedRadioButtonId() == R.id.rb_drink_false) {
			survey1Bean.setDrinkCount("0");
		} else {
			String drinkcount = met_drink.getText().toString().trim();
			if (TextUtils.isEmpty(drinkcount)) {
				ToastUtil.showToast("请填写每周喝酒的数量");
				return false;
			}
			survey1Bean.setDrinkCount(drinkcount);
		}
		String height = et_height.getText().toString().trim();
		if (TextUtils.isEmpty(height)) {
			ToastUtil.showToast("身高不能为空");
			return false;
		}
		String weight = et_weight.getText().toString().trim();
		if (TextUtils.isEmpty(weight)) {
			ToastUtil.showToast("体重不能为空");
			return false;
		}
		String bmi = et_bmi.getText().toString().trim();
		if (TextUtils.isEmpty(bmi)) {
			ToastUtil.showToast("BMI不能为空");
			return false;
		}
		String shousuo = et_shousuo.getText().toString().trim();
		if (TextUtils.isEmpty(shousuo)) {
			ToastUtil.showToast("收缩压不能为空");
			return false;
		}
		String shuzhang = et_shuzhang.getText().toString().trim();
		if (TextUtils.isEmpty(shuzhang)) {
			ToastUtil.showToast("舒张压不能为空");
			return false;
		}
		survey1Bean.setDate(date);
		survey1Bean.setName(name);
		survey1Bean.setMobile(mobile);
		survey1Bean.setVisitTime(visit);
		survey1Bean.setAge(age);
		survey1Bean.setSex(sex);
		survey1Bean.setMarriage(marriage);
		survey1Bean.setEducation(edu);
		survey1Bean.setOccupation(job);
		survey1Bean.setHeight(height);
		survey1Bean.setWeight(weight);
		survey1Bean.setBmi(bmi);
		survey1Bean.setShshya(shousuo);
		survey1Bean.setShzhya(shuzhang);
		return true;
	}

	private boolean checkData2() {
		String history = met_histroy.getText().toString();
		if (TextUtils.isEmpty(history)) {
			ToastUtil.showToast("请填写糖尿病肾病病史，没有填0");
			return false;
		}
		survey1Bean.setTnbsbs(history);
		String oral = met_oral.getText().toString();
		if (TextUtils.isEmpty(oral)) {
			ToastUtil.showToast("请填写口服药使用时间，没有填0");
			return false;
		}
		survey1Bean.setKfysysj(oral);
		String insuString = met_insulin.getText().toString();
		if (TextUtils.isEmpty(insuString)) {
			ToastUtil.showToast("请填写胰岛素使用时间，没有填0");
			return false;
		}
		survey1Bean.setYdssysj(insuString);
		String lowproTime = met_lowprotein.getText().toString();
		if (TextUtils.isEmpty(lowproTime)) {
			ToastUtil.showToast("请填写低蛋白饮食时间，没有填0");
			return false;
		}
		survey1Bean.setDdbyssj(lowproTime);
		if (mrg_ever_edu.getCheckedRadioButtonId() == R.id.rb_ever_edu_false) {
			survey1Bean.setTnblesson("");
		} else {
			StringBuffer eduContent = new StringBuffer();
			if (mcb_ever_edu1.isChecked()) {
				eduContent.append("1");
				eduContent.append("@@");
			}
			if (mcb_ever_edu2.isChecked()) {
				eduContent.append("2");
				eduContent.append("@@");
			}
			if (mcb_ever_edu3.isChecked()) {
				eduContent.append("3");
				eduContent.append("@@");
			}
			if (mcb_ever_edu4.isChecked()) {
				eduContent.append("4");
				eduContent.append("@@");
			}
			if (mcb_ever_edu5.isChecked()) {
				eduContent.append("5");
				eduContent.append("@@");
			}
			if (mcb_ever_edu6.isChecked()) {
				eduContent.append("6");
				eduContent.append("@@");
			}
			if (mcb_ever_edu7.isChecked()) {
				eduContent.append("7");
				eduContent.append("@@");
			}
			String edu = eduContent.toString();
			if (edu.endsWith("@@")) {
				edu = edu.substring(0, edu.length()-2);
			}
			survey1Bean.setTnblesson(edu);
		}
		if (mrg_regular_review.getCheckedRadioButtonId() == R.id.rb_regularly_review_false) {
			survey1Bean.setTnbglfc("0");
		} else {
			String reguString = met_reguraly.getText().toString().trim();
			if (TextUtils.isEmpty(reguString)) {
				ToastUtil.showToast("请填写平均每年复查次数");
				return false;
			}
			survey1Bean.setTnbglfc(reguString);
		}
		if (mrg_smbg.getCheckedRadioButtonId() == R.id.rb_smbg_false) {
			survey1Bean.setTnbglzwjc("0");
		} else {
			String self = met_selfjiandu.getText().toString().trim();
			if (TextUtils.isEmpty(self)) {
				ToastUtil.showToast("请填写平均每月检测次数");
				return false;
			}
			survey1Bean.setTnbglzwjc(self);
		}
		if (mrg_lowprotein.getCheckedRadioButtonId() == R.id.rb_lowprotein_false) {
			survey1Bean.setTnbglddbys("0");
		} else {
			String reguString = met_lowproteinCount.getText().toString().trim();
			if (TextUtils.isEmpty(reguString)) {
				ToastUtil.showToast("请填写坚持低蛋白饮食时间");
				return false;
			}
			survey1Bean.setTnbglddbys(reguString);
		}

		return true;
	}

	private boolean checkData3() {
		String fpg = met_fpg.getText().toString().trim();
		if (TextUtils.isEmpty(fpg)) {
			ToastUtil.showToast("请填写空腹血糖值");
			return false;
		}
		survey1Bean.setSysfpg(fpg);
		String _2hpg = met_2hpg.getText().toString().trim();
		if (TextUtils.isEmpty(_2hpg)) {
			ToastUtil.showToast("请填写餐后2h血糖值");
			return false;
		}
		survey1Bean.setSys2hpg(_2hpg);
		String hba1c = met_hba1c.getText().toString().trim();
		if (TextUtils.isEmpty(hba1c)) {
			ToastUtil.showToast("请填写糖化血红蛋白值");
			return false;
		}
		survey1Bean.setSyshba1c(hba1c);
		String cr = met_cr.getText().toString().trim();
		if (TextUtils.isEmpty(cr)) {
			ToastUtil.showToast("请填写血肌酐值");
			return false;
		}
		survey1Bean.setSyscr(cr);
		String tc = met_tc.getText().toString().trim();
		if (TextUtils.isEmpty(tc)) {
			ToastUtil.showToast("请填写总胆固醇值");
			return false;
		}
		survey1Bean.setSystc(tc);
		String tg = met_tg.getText().toString().trim();
		if (TextUtils.isEmpty(tg)) {
			ToastUtil.showToast("请填写甘油三脂值");
			return false;
		}
		survey1Bean.setSystg(tg);
		String malb = met_malb.getText().toString().trim();
		if (TextUtils.isEmpty(malb)) {
			ToastUtil.showToast("请填写尿微量蛋白值");
			return false;
		}
		survey1Bean.setSysmalb(malb);
		String nsd = met_nsd.getText().toString().trim();
		if (TextUtils.isEmpty(nsd)) {
			ToastUtil.showToast("请填写尿素氮值");
			return false;
		}
		survey1Bean.setSysnsd(nsd);

		if (mrg_medicalcost.getCheckedRadioButtonId() == R.id.rb_cost_insuarance) {
			survey1Bean.setYlhf("医保");
		} else {
			String cost = met_medicalcost.getText().toString().trim();
			if (TextUtils.isEmpty(cost)) {
				ToastUtil.showToast("请填写自费所占比例");
				return false;
			}
			survey1Bean.setYlhf(cost);
		}

		return true;
	}

	OnClickCallBackListener onClickCallBackListener;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		if (activity instanceof MyInfoActivity) {
			((MyInfoActivity) activity)
					.setOnConnectFragmentActivityListener(this);
		}
		try {
			onClickCallBackListener = (OnClickCallBackListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ "must implement OnArticleSelectedListener");
		}
		res = activity.getResources();
	}

	private Resources res;

	@Override
	public void onClickCallBack(Bundle data) {
		String name = data.getString("name");

		Survey1Type type = Survey1Type.values()[data.getInt("type")];
		switch (type) {
		case YEAR: {
			table1_date.setText(name);
		}
			break;
		case DATE: {
			tv_birthday.setText(name);
		}
			break;
		case SEX: {
			tv_sex.setText(name);
		}
			break;
		case MARRIAGE: {
			tv_marriage.setText(name);
		}
			break;
		case JOB: {
			tv_job.setText(name);
		}
			break;
		case EDU: {
			tv_edu.setText(name);
		}
			break;
		default:
			break;
		}
	}

	@Override
	public void notifyFragment(Bundle data) {
		super.notifyFragment(data);
	}

}
