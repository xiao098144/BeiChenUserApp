package com.ddoctor.user.activity.questionnaire;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

import com.beichen.user.R;


public class Survey1Fragment1 extends Fragment implements OnCheckedChangeListener {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	String name1, gender1, birthday1, marriage1, edu1, job1, cig1, drink1;
	EditText et_name;
	TextView tv_birthday, tv_sex, tv_marriage, tv_edu, tv_job;
	private RadioGroup rg_cig, rg_drink;
	private LinearLayout cig_layout, drink_layout;

	

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.e("", "Survey1Fragment1 onCreateView");
		return inflater.inflate(R.layout.layout_survey1fragement1, null);
	}
	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		Log.e("", "Survey1Fragment1 onViewCreated");
		findviewById(view);
	}
	
	private void findviewById(View view){
		et_name = (EditText) view.findViewById(R.id.updateinfo_et_name1);
		tv_birthday = (TextView) view
				.findViewById(R.id.updateinfo_tv_birthday1);
		tv_sex = (TextView) view.findViewById(R.id.updateinfo_tv_sex1);
		tv_marriage = (TextView) view
				.findViewById(R.id.updateinfo_tv_marriage1);
		tv_edu = (TextView) view.findViewById(R.id.updateinfo_tv_edu1);
		tv_job = (TextView) view.findViewById(R.id.updateinfo_tv_job1);
		rg_cig = (RadioGroup) view.findViewById(R.id.rg_cig);
		cig_layout = (LinearLayout) view.findViewById(R.id.table1_cig_layout);
		drink_layout = (LinearLayout) view
				.findViewById(R.id.table1_drink_layout);
		rg_drink = (RadioGroup) view.findViewById(R.id.rg_drink);

		rg_cig.setOnCheckedChangeListener(this);
		rg_drink.setOnCheckedChangeListener(this);
	}
	
	class MyClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.updateinfo_tv_birthday1: {
//				DialogUtil.showDatePicker(context, 1970, tv_birthday);
			}
				break;
			case R.id.updateinfo_tv_sex1: {
//				DialogUtil.showSingleDialog(tv_sex, "性别", context, DialogUtil
//						.generateData(res.getStringArray(R.array.gender)),
//						false);
			}
				break;
			case R.id.updateinfo_tv_marriage1: {
//				DialogUtil.showSingleDialog(tv_sex, "婚姻状况", context, DialogUtil
//						.generateData(res.getStringArray(R.array.marriage)),
//						false);
			}
				break;
			case R.id.updateinfo_tv_job1: {
//				DialogUtil.showSingleDialog(tv_sex, "职业", context, DialogUtil
//						.generateData(res.getStringArray(R.array.job)), false);
			}
				break;
			case R.id.updateinfo_tv_edu1: {
//				DialogUtil.showSingleDialog(tv_sex, "文化程度", context, DialogUtil
//						.generateData(res.getStringArray(R.array.edu)), false);
			}
				break;
			}

		}

	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		MyClickListener myClickListener = new MyClickListener();
		tv_birthday.setOnClickListener(myClickListener);
		tv_sex.setOnClickListener(myClickListener);
		tv_marriage.setOnClickListener(myClickListener);
		tv_edu.setOnClickListener(myClickListener);
		tv_job.setOnClickListener(myClickListener);
	}

	private Context context;
	private Resources res;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		context = getActivity();
		res = context.getResources();
	}

	@Override
	public void onStart() {
		super.onStart();
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		switch (checkedId) {
		case R.id.rb_cig_false: {
			if (View.VISIBLE == cig_layout.getVisibility()) {
				cig_layout.setVisibility(View.INVISIBLE);
			}
			
		}
			break;
		case R.id.rb_cig_true: {
			if (View.VISIBLE != cig_layout.getVisibility()) {
				cig_layout.setVisibility(View.VISIBLE);
			}
		}
			break;
		case R.id.rb_drink_false: {
			if (View.VISIBLE == drink_layout.getVisibility()) {
				drink_layout.setVisibility(View.INVISIBLE);
			}
		}
			break;
		case R.id.rb_drink_true: {
			if (View.VISIBLE != drink_layout.getVisibility()) {
				drink_layout.setVisibility(View.VISIBLE);
			}
		}
			break;

		default:
			break;
		}
	}

}
