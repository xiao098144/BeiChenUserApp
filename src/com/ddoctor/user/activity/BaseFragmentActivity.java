package com.ddoctor.user.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.application.MyApplication;
import com.ddoctor.utils.ImageLoaderUtil;
import com.nostra13.universalimageloader.core.ImageLoader;

public class BaseFragmentActivity extends FragmentActivity implements
		OnClickListener {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		if (!ImageLoader.getInstance().isInited()) {
			ImageLoaderUtil.initImageLoader(getApplicationContext());
		}
		MyApplication.addActivity(this);
	}

	protected void setTitle(String title) {
		TextView titleTextView = (TextView) this
				.findViewById(R.id.title_center_txt);
		if (titleTextView != null)
			titleTextView.setText(title);
	}

	protected Button getLeftButton() {
		Button button = (Button) findViewById(R.id.btn_left);
		return button;
	}

	protected Button getLeftButtonText(String text) {
		Button button = (Button) findViewById(R.id.btn_left);
		button.setVisibility(View.VISIBLE);
		button.setText(text);
		return button;
	}

	protected Button getRightButton() {
		Button button = (Button) findViewById(R.id.btn_right);
		return button;
	}

	protected Button getRightButtonText(String text) {
		Button button = (Button) findViewById(R.id.btn_right);
		button.setVisibility(View.VISIBLE);
		button.setText(text);
		return button;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			finishThisActivity();
		}
		return super.onKeyDown(keyCode, event);
	}

	public static void leftOutRightIn(Context context) {
		((Activity) context).overridePendingTransition(R.anim.in_from_right,
				R.anim.out_to_left);
	}

	public static void rightOut(Context context) {
		((Activity) context).overridePendingTransition(R.anim.right_in,
				R.anim.right_out);

	}

	protected void finishThisActivity() {
		finish();
		rightOut(this);
	}

	@Override
	public void onClick(View v) {

	}

}
