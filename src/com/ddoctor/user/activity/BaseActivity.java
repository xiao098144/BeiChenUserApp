package com.ddoctor.user.activity;

import java.io.Serializable;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.application.MyApplication;
import com.ddoctor.utils.ImageLoaderUtil;
import com.nostra13.universalimageloader.core.ImageLoader;


public class BaseActivity extends Activity implements OnClickListener{
	protected BaseActivity CTX = BaseActivity.this;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		if (!ImageLoader.getInstance().isInited()) {
			ImageLoaderUtil.initImageLoader(getApplicationContext());
		}
		MyApplication.addActivity(this);
	}

	protected void setTitle(String title) {
		TextView titleTextView = (TextView) this
				.findViewById(R.id.title_center_txt);
		if (titleTextView != null)
			titleTextView.setText(title);
	}

	protected TextView getTopTitle(){
		TextView titleTextView = (TextView) this
				.findViewById(R.id.title_center_txt);
		return titleTextView;
	}
	
	protected View getTitleBar(){
		return findViewById(R.id.titlebar);
	}
	
	protected Button getLeftButton() {
		Button button = (Button) findViewById(R.id.btn_left);
		button.setVisibility(View.VISIBLE);
		return button;
	}

	protected Button getLeftButtonText(String text) {
		Button button = (Button) findViewById(R.id.btn_left);
		button.setVisibility(View.VISIBLE);
		button.setText(text);
		return button;
	}

	protected Button getRightButton() {
		Button button = (Button) findViewById(R.id.btn_right);
		button.setVisibility(View.VISIBLE);
		return button;
	}

	protected Button getRightButtonText(String text) {
		Button button = (Button) findViewById(R.id.btn_right);
		button.setVisibility(View.VISIBLE);
		button.setText(text);
		return button;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			finishThisActivity();
		}
		return super.onKeyDown(keyCode, event);
	}

	public static void leftOutRightIn(Context context) {
		((Activity) context).overridePendingTransition(R.anim.in_from_right,
				R.anim.out_to_left);
	}

	public static void rightOut(Context context) {
		((Activity) context).overridePendingTransition(R.anim.right_in,
				R.anim.right_out);

	}

	protected void finishThisActivity() {
		finish();
		rightOut(this);
	}

	@Override
	public void onClick(View v) {
		
		
	}
	/**
	 * 封装Intent跳转
	 * 
	 * @param clazz
	 *            要跳向的界面的class
	 * @param isCloseSelf
	 *            是否关闭本界面
	 */
	protected void skip(Class<?> clazz, boolean isCloseSelf) {
		Intent intent = new Intent(CTX, clazz);
		startActivity(intent);

		if (isCloseSelf) {
			CTX.finish();
		}
//		overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
	}

	/**
	 * 封装Intent跳转
	 * 
	 * @param clazz
	 *            要跳向的界面的class
	 * @param isCloseSelf
	 *            是否关闭本界面
	 */
	protected void skipForResult(Class<?> clazz, int requestCode) {
		Intent intent = new Intent(CTX, clazz);
		startActivityForResult(intent, requestCode);
		overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
	}

	/**
	 * 封装Intent跳转
	 * 
	 * @param key
	 * @param value
	 * @param clazz
	 * @param isCloseSelf
	 */
	protected void skip(String key, String value, Class<?> clazz,
			boolean isCloseSelf) {
		Intent intent = new Intent();
		intent.setClass(CTX, clazz);
		if (!TextUtils.isEmpty(key)) {
			intent.putExtra(key, value);
		}
		startActivity(intent);
		if (isCloseSelf) {
			CTX.finish();
		}
		//overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
	}
	@Override  
	public Resources getResources() {  
	    Resources res = super.getResources();    
	    Configuration config=new Configuration();    
	    config.setToDefaults();    
	    res.updateConfiguration(config,res.getDisplayMetrics() );  
	    return res;  
	} 
	/**
	 * 封装Intent 跳转 并传递Bundle 数据
	 * 
	 * @param key
	 * @param data
	 * @param clazz
	 * @param isCloseSelf
	 */
	protected void skip(String key, Bundle data, Class<?> clazz,
			boolean isCloseSelf) {
		Intent intent = new Intent();
		intent.setClass(CTX, clazz);
		if (!TextUtils.isEmpty(key)) {
			intent.putExtra(key, data);
		}
		startActivity(intent);
		if (isCloseSelf) {
			CTX.finish();
		}
		//overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
	}

	/**
	 * 封装Intent跳转
	 * 
	 * @param key
	 * @param value
	 * @param clazz
	 * @param isCloseSelf
	 */
	protected void skipForResult(String key, String value, Class<?> clazz,
			int requestCode) {
		Intent intent = new Intent();
		intent.setClass(CTX, clazz);
		if (!TextUtils.isEmpty(key)) {
			intent.putExtra(key, value);
		}
		startActivityForResult(intent, requestCode);
		//overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
	}

	/**
	 * 封装Intent跳转
	 * 
	 * @param key
	 * @param obj
	 * @param clazz
	 * @param isCloseSelf
	 */
	protected void skip(String key, Serializable obj, Class<?> clazz,
			boolean isCloseSelf) {
		Intent intent = new Intent();
		intent.setClass(CTX, clazz);
		if (!TextUtils.isEmpty(key)) {
			intent.putExtra(key, obj);
		}
		startActivity(intent);
		if (isCloseSelf) {
			CTX.finish();
		}
		//overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
	}

	/**
	 * 封装Intent跳转
	 * 
	 * @param key
	 * @param obj
	 * @param clazz
	 * @param isCloseSelf
	 */
	protected void skipForResult(String key, Serializable obj, Class<?> clazz,
			int requestCode) {
		Intent intent = new Intent();
		intent.setClass(CTX, clazz);
		if (!TextUtils.isEmpty(key)) {
			intent.putExtra(key, obj);
		}
		startActivityForResult(intent, requestCode);
		//overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
	}

	/**
	 * 默认 key 值  “data”
	 * @param key
	 * @param obj
	 * @param clazz
	 * @param requestCode
	 */
	protected void skipForResult(String key, Parcelable obj, Class<?> clazz,
			int requestCode) {
		Intent intent = new Intent();
		intent.setClass(CTX, clazz);
		if (TextUtils.isEmpty(key)) {
			key = "data";
		}
		intent.putExtra(key, obj);
		startActivityForResult(intent, requestCode);
	}
	
	/**
	 * 封装Intent跳转,keys必须和objs是一一对应的关系
	 * 
	 * @param @param keys
	 * @param @param objs
	 * @param @param clazz
	 * @param @param isCloseSelf
	 * @return void
	 * @throws
	 */
	protected void skip(String[] keys, Serializable[] objs, Class<?> clazz,
			boolean isCloseSelf) {
		Intent intent = new Intent();
		intent.setClass(CTX, clazz);
		for (int i = 0; i < keys.length; i++) {
			String key = keys[i];
			Serializable obj = objs[i];
			if (!TextUtils.isEmpty(key) && obj != null) {
				intent.putExtra(key, obj);
			}
		}
		startActivity(intent);
		if (isCloseSelf) {
			CTX.finish();
		}
		//overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
	}

	/**
	 * 封装Intent跳转,keys必须和objs是一一对应的关系
	 * 
	 * @param @param keys
	 * @param @param objs
	 * @param @param clazz
	 * @param @param isCloseSelf
	 * @return void
	 * @throws
	 */
	protected void skipForResult(String[] keys, Serializable[] objs,
			Class<?> clazz, int requestCode) {
		Intent intent = new Intent();
		intent.setClass(CTX, clazz);
		for (int i = 0; i < keys.length; i++) {
			String key = keys[i];
			Serializable obj = objs[i];
			if (!TextUtils.isEmpty(key) && obj != null) {
				intent.putExtra(key, obj);
			}
		}
		startActivityForResult(intent, requestCode);
		//overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
	}

	/**
	 * 封装Intent跳转
	 * 
	 * @param key
	 * @param obj
	 * @param clazz
	 * @param isCloseSelf
	 */
	protected void skip(String key, ArrayList<Parcelable> obj, Class<?> clazz,
			boolean isCloseSelf) {
		Intent intent = new Intent();
		intent.setClass(CTX, clazz);
		if (!TextUtils.isEmpty(key)) {
			intent.putParcelableArrayListExtra(key, obj);
		}
		startActivity(intent);
		if (isCloseSelf) {
			CTX.finish();
		}
		//overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
	}

	/**
	 * 封装Intent跳转
	 * 
	 * @param key
	 * @param obj
	 * @param clazz
	 * @param isCloseSelf
	 */
	protected void skip(String key, int obj, Class<?> clazz, boolean isCloseSelf) {
		Intent intent = new Intent();
		intent.setClass(CTX, clazz);
		if (!TextUtils.isEmpty(key)) {
			intent.putExtra(key, obj);
		}
		startActivity(intent);
		if (isCloseSelf) {
			CTX.finish();
		}
		//overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
	}

}
