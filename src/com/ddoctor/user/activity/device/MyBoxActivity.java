package com.ddoctor.user.activity.device;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.enums.RetError;
import com.ddoctor.user.activity.BaseActivity;
import com.ddoctor.user.task.DoBindMioBoxTask;
import com.ddoctor.user.task.GetMiOBoxStateTask;
import com.ddoctor.user.task.TaskPostCallBack;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.DialogUtil;
import com.ddoctor.utils.MyUtils;
import com.ddoctor.utils.StringUtils;
import com.ddoctor.utils.ToastUtil;

public class MyBoxActivity extends BaseActivity {

	private Button _btn_undo;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_miobox);
		findViewById();
		getMiOBoxState();
	}

	private void findViewById() {
		setTitle(getString(R.string.mine_device));
		Button leftButton = getLeftButton();
		leftButton.setText(getString(R.string.basic_back));
		leftButton.setOnClickListener(this);
		_et_device = (EditText) findViewById(R.id.miobox_et);
		_et_device.setHint(StringUtils.fromatETHint(getString(R.string.et_hint_device), 13));
		_layout = (RelativeLayout) findViewById(R.id.miobox_layout);
		_btn_do = (Button) findViewById(R.id.miobox_btn_bind);
		_btn_do.setOnClickListener(this);
		_btn_undo = (Button) findViewById(R.id.miobox_btn_undo);
		_btn_undo.setOnClickListener(this);
		_tv_result = (TextView) findViewById(R.id.miobox_tv_result);
	}

	private Dialog _loadingDialog = null;
	protected EditText _et_device;

	private int _type = 1;
	private String _boxSn;
	protected RelativeLayout _layout;
	protected Button _btn_do;
	protected TextView _tv_result;
	
	private void getMiOBoxState() {
		_loadingDialog = DialogUtil.createLoadingDialog(MyBoxActivity.this);
		_loadingDialog.show();

		GetMiOBoxStateTask task = new GetMiOBoxStateTask();
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {

			@Override
			public void taskFinish(DDResult result) {
				if (_loadingDialog != null && _loadingDialog.isShowing())
					_loadingDialog.dismiss();
				if (result.getError() == RetError.NONE) {
					_boxSn = result.getBundle().getString("boxSn");
					if (TextUtils.isEmpty(_boxSn)) {
						_tv_result.setVisibility(View.GONE);
						
						_et_device.setText("");
						_btn_undo.setText("扫描二维码");
						_type = 1;
						_layout.setVisibility(View.VISIBLE);
						_btn_undo.setVisibility(View.VISIBLE);
						
						
					} else {
						_layout.setVisibility(View.GONE);
						
						_tv_result.setText(String.format(getString(R.string.miobox_result), _boxSn));
						_tv_result.setVisibility(View.VISIBLE);
		
						_btn_undo.setText("解除绑定");
						_type = 2;
						_btn_undo.setVisibility(View.VISIBLE);
						
					}
					
				} else {
					ToastUtil.showToast(result.getErrorMessage());
				}
			}
		});
		task.executeParallel("");
	}
	//请求码：前往相机扫描
	 public static final int REQUEST_CODE_CAMERA = 4;
	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.btn_left:{
			finishThisActivity();
		}
		break;
		case R.id.miobox_btn_bind:{
			String value = _et_device.getText().toString().trim();
			if (TextUtils.isEmpty(value)) {
				ToastUtil.showToast("请输入设备号或者通过二维码扫描");
			}else {
				_et_device.setText("");
				Intent intent = new Intent();
				intent.putExtra("device", value);
				intent.setClass(MyBoxActivity.this, MiOBoxGlumeterListActivity.class);
				startActivityForResult(intent, 200);
			}
		}
			break;
		case R.id.miobox_btn_undo:{
			if (_type == 2) {
				cancelMioBoxBind();
			} else if(_type == 1){
				Intent intent = new Intent();
				intent.setClass(MyBoxActivity.this, QRScanActivity.class);
				startActivityForResult(intent, REQUEST_CODE_CAMERA);
			}
		}
		break;

		default:
			break;
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) 
	{
		super.onActivityResult(requestCode, resultCode, data);

	    MyUtils.showLog("","相机扫描完成哦！！！resultCode == CpCameraScanFormActivity.RETURN_ID？"
				+(resultCode == QRScanActivity.RETURN_ID));
		
		if(requestCode == REQUEST_CODE_CAMERA)
		{
		
			if(resultCode == QRScanActivity.RETURN_ID)
			{
				//注意：相机扫描时，默认是可以连续扫描多条，并把结果记录在一个集合时，
				//当退出扫描时再1次性把这批扫出来的产品加入到明细列表中，也就是说相
				//机扫描时扫描的结果并不是实时加入到明细列表中的
				String macCode = (String)data.getSerializableExtra("__camera_data__");
				Intent intent = new Intent();
				intent.putExtra("device", macCode);
				intent.setClass(MyBoxActivity.this, MiOBoxGlumeterListActivity.class);
				startActivityForResult(intent, 200);
				
			}
		}else if(requestCode == 200){
			if (resultCode == RESULT_OK) {
				getMiOBoxState();
			}
		}
	}
	
	private void cancelMioBoxBind(){
		_loadingDialog = DialogUtil.createLoadingDialog(MyBoxActivity.this);
		_loadingDialog.show();
		
		DoBindMioBoxTask task = new DoBindMioBoxTask(_boxSn);
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {

			@Override
			public void taskFinish(DDResult result) {
				if (_loadingDialog!=null) {
					_loadingDialog.dismiss();
				}
				if (result.getError() == RetError.NONE) {
					_tv_result.setVisibility(View.GONE);
					_layout.setVisibility(View.VISIBLE);
					_btn_undo.setText("扫描二维码");
					_type = 1;
					_btn_undo.setVisibility(View.VISIBLE);
					
				} else {
					ToastUtil.showToast(result.getErrorMessage());
				}
			}
		});
		task.executeParallel("");
	}
	
	
}
