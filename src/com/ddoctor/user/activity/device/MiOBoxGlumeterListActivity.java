package com.ddoctor.user.activity.device;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.enums.RetError;
import com.ddoctor.user.activity.BaseActivity;
import com.ddoctor.user.adapter.MiOBoxSingleAdapter;
import com.ddoctor.user.adapter.MiOBoxSingleAdapter.SingleViewHolder;
import com.ddoctor.user.task.DoBindMioBoxTask;
import com.ddoctor.user.task.GetMioBoxGluListTask;
import com.ddoctor.user.task.TaskPostCallBack;
import com.ddoctor.user.wapi.bean.MioGlucoseMeterBean;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.DialogUtil;
import com.ddoctor.utils.MyUtils;
import com.ddoctor.utils.ToastUtil;

public class MiOBoxGlumeterListActivity extends BaseActivity {

	private TextView _tv_device;
	private Button _btn_bind;
	private ListView _listView;
	private MiOBoxSingleAdapter adapter;
	

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_mioglumeterlist);
		findViewById();
		getMioBoxGluList();
	}

	private Dialog _loadingDialog;
	private List<MioGlucoseMeterBean> _dataList = new ArrayList<MioGlucoseMeterBean>();
	private String _boxSn;
	private void getMioBoxGluList(){
		_loadingDialog = DialogUtil.createLoadingDialog(MiOBoxGlumeterListActivity.this);
		_loadingDialog.show();
		GetMioBoxGluListTask task = new GetMioBoxGluListTask();
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {
			
			@Override
			public void taskFinish(DDResult result) {
				MyUtils.showLog("taskFinish  "+(_loadingDialog != null&&_loadingDialog.isShowing()));
				if (_loadingDialog != null) {
					_loadingDialog.dismiss();
				}
				if (result.getError() == RetError.NONE) {
					ArrayList<MioGlucoseMeterBean> list = result.getBundle().getParcelableArrayList("list");
					if (list != null && list.size()>0) {
						_dataList.addAll(list);
						adapter.notifyDataSetChanged();
					}
					
				} else {
					ToastUtil.showToast(result.getErrorMessage());
				}
			}
		});
		task.executeParallel("");
	}
	
	private void findViewById() {
		setTitle("密友盒子");
		Button leftButton = getLeftButton();
		leftButton.setText(getString(R.string.basic_back));
		leftButton.setOnClickListener(this);
		_tv_device = (TextView) findViewById(R.id.miobox_glu_tv_device);
		_btn_bind = (Button) findViewById(R.id.miobox_glu_btn_bind);
		_btn_bind.setOnClickListener(this);
		_listView = (ListView) findViewById(R.id.miobox_glu_list);
		adapter = new MiOBoxSingleAdapter(MiOBoxGlumeterListActivity.this);
		adapter.setData(_dataList, "");
		_listView.setAdapter(adapter);
		_listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int position,
					long arg3) {
				int dataIndex = position-_listView.getHeaderViewsCount();
				if (dataIndex>=_dataList.size()) {
					return;
				}
				SingleViewHolder holder = (SingleViewHolder) view.getTag();
				if (!holder.rb_check.isChecked()) {
					adapter.setSelected(dataIndex);
				}
			}
		});
		_boxSn = getIntent().getStringExtra("device");
		_tv_device.setText(String.format("设备号：%s", _boxSn));
		
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.btn_left: {
			finishThisActivity();
		}
			break;
		case R.id.miobox_glu_btn_bind: {
			Map<String, String> data = adapter.getSelectedData();
			MyUtils.showLog("选择的数据   "+data==null?"data is null ":data.toString());
			if (data==null || data.isEmpty()) {
				ToastUtil.showToast("请选择与盒子搭配使用的血糖仪");
			}else {
				String gluId = data.get("typeId");
				doMioBoxBind(gluId);
			}
		}
			break;

		default:
			break;
		}
	}
	
	private void doMioBoxBind(String gluId){
		_loadingDialog = DialogUtil.createLoadingDialog(MiOBoxGlumeterListActivity.this);
		_loadingDialog.show();
		
		DoBindMioBoxTask task = new DoBindMioBoxTask(_boxSn , gluId);
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {

			@Override
			public void taskFinish(DDResult result) {
				if (_loadingDialog!=null) {
					_loadingDialog.dismiss();
				}
				if (result.getError() == RetError.NONE) {
					ToastUtil.showToast("绑定成功");
					setResult(RESULT_OK);
					finishThisActivity();
				} else {
					ToastUtil.showToast(result.getErrorMessage());
				}
			}
		});
		task.executeParallel("");
	}
	
	

}
