package com.ddoctor.user.activity.medicine;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.enums.RetError;
import com.ddoctor.interfaces.OnClickCallBackListener;
import com.ddoctor.user.activity.BaseActivity;
import com.ddoctor.user.task.SaveRemindTask;
import com.ddoctor.user.task.TaskPostCallBack;
import com.ddoctor.user.wapi.bean.MedicalRecordBean;
import com.ddoctor.user.wapi.bean.MedicalRemindBean;
import com.ddoctor.utils.DialogUtil;
import com.ddoctor.utils.MyUtils;
import com.ddoctor.utils.StringUtils;
import com.ddoctor.utils.ToastUtil;
import com.umeng.analytics.MobclickAgent;
import com.zhuge.analysis.stat.ZhugeSDK;

public class MedicineSettingActivity extends BaseActivity implements
		OnClickCallBackListener {

	private ImageButton img_add;
	private TextView tv_set_routing, tv_routing;
	private EditText et_remark;
	
	private LinearLayout layout_choose;
	private RelativeLayout layout_time;
	private TextView tv_time;
	
	private String time;

	private String routing;

	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("MedicineSettingActivity");
		MobclickAgent.onResume(MedicineSettingActivity.this);
		ZhugeSDK.getInstance().init(getApplicationContext());
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("MedicineSettingActivity");
		MobclickAgent.onPause(MedicineSettingActivity.this);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_medicine_setting);
		week = getResources().getStringArray(R.array.week);
		findViewById();
	}

	MedicalRemindBean medicalRemindBean ;
	Dialog dialog;
	private boolean checkData() {

		if (medicalRemindBean == null) {
			medicalRemindBean = new MedicalRemindBean();
		}
		if (choose_list==null || choose_list.size()==0) {
			ToastUtil.showToast("请添加药品");
			return false;
		}
		medicalRemindBean.setDataList(choose_list);
		medicalRemindBean.setDate(choose_list.get(0).getDate());
		if (TextUtils.isEmpty(time)) {
			ToastUtil.showToast("请选择服用药品的时间");
			return false;
		}
		medicalRemindBean.setTime(time);
		if (TextUtils.isEmpty(routing)) {
			ToastUtil.showToast("请设置提醒周期");
			return false;
		}
		medicalRemindBean.setRouting(routing);
		String remark = et_remark.getText().toString().trim();
		medicalRemindBean.setRemark(remark);
		return true;
	}

	private void saveMedicalRemind(){
		
		SaveRemindTask task = new SaveRemindTask(medicalRemindBean ,1);
		task.setTaskCallBack(new TaskPostCallBack<RetError>() {
			
			@Override
			public void taskFinish(RetError result) {
//				dialog.dismiss();
				if (result == RetError.NONE) {
					ToastUtil.showToast("保存成功");
					setResult(RESULT_OK);
					finishThisActivity();
				} else {
					ToastUtil.showToast(result.getErrorMessage());
				}
			}
		});
		task.executeParallel("");
	}
	
	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.btn_left:
			finishThisActivity();
			break;
		case R.id.btn_right: {
			if (checkData()) {
				MyUtils.showLog("", "准备提交数据  "+medicalRemindBean.toString());
				saveMedicalRemind();
			}
		}
			break;
		case R.id.medicine_layout_time: {
			DialogUtil.creatTimeDialog(MedicineSettingActivity.this, 0, getResources().getString(R.string.basic_cancel), getResources().getString(R.string.basic_confirm), this);
		}
			break;
		case R.id.medicine_remind_img_add: {
			skipForResult("type", 2, MedicalListActivity.class, 300);
		}
			break;
		case R.id.medicine_remind_tv_routing:
		case R.id.medicine_remind_tv_set_routing: {
			DialogUtil.creatWeekDialog(MedicineSettingActivity.this,
					R.color.color_medical_title,
					getResources().getString(R.string.basic_cancel),
					getResources().getString(R.string.basic_confirm), this);
		}
			break;
		default:
			break;
		}
	}

	String[] week;

	@Override
	public void onClickCallBack(Bundle data) {
		int type = data.getInt("type" , -1);
		if (type == 0) {
			StringBuffer sb = new StringBuffer();
			sb.append(StringUtils.formatnum(
					Integer.valueOf(data.getString("hour")), "00"));
			sb.append(":");
			sb.append(StringUtils.formatnum(
					Integer.valueOf(data.getString("minute")), "00"));
			time = sb.toString();
			tv_time.setText(sb);
			tv_time.setGravity(Gravity.CENTER_VERTICAL|Gravity.RIGHT);
		} else {
			routing = data.getString("data");
			if (!(TextUtils.isEmpty(routing)||routing.length()!=7||routing.equals("000000"))) {
				StringBuffer sb = new StringBuffer();
				for (int i = 0; i < routing.length(); i++) {
					if ('1'==routing.charAt(i)) {
						sb.append(week[i]);
						sb.append("、");
					}
				}
				String string = sb.toString();
				if (string.endsWith("、")) {
					string = string.substring(0, string.length()-1);
				}
				tv_routing.setText(string);
				tv_set_routing.setVisibility(View.GONE);
				tv_routing.setVisibility(View.VISIBLE);
			}
		}
	}

	ArrayList<MedicalRecordBean> choose_list = new ArrayList<MedicalRecordBean>();

	private int isContains(MedicalRecordBean medicalRecordBean) {
		if (medicalRecordBean == null) {
			return -1;
		}
		for (int i = 0; i < choose_list.size(); i++) {
			if (medicalRecordBean.getMedicalId().equals(choose_list.get(i).getMedicalId())) {
				return i;
			}
		}
		return -1;
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			Intent intent) {
		super.onActivityResult(requestCode, resultCode, intent);
//		if (resultCode != RESULT_OK) {
//			return;
//		}
//		if (requestCode == 300) {
//			Bundle data = intent.getBundleExtra("data");
//			choose_list = data.getParcelableArrayList("data");
//		}
		if (resultCode != RESULT_OK) {
			return;
		}
		if (requestCode == 300) {
			Bundle data = intent.getBundleExtra("data");
			MedicalRecordBean medicalRecordBean =data.getParcelable("data");
			if (medicalRecordBean != null) {
				int index = isContains(medicalRecordBean);
				if (index>-1) {
					choose_list.get(index).setCount(medicalRecordBean.getCount());
				} else {
					choose_list.add(medicalRecordBean);
				}
				showChooseResult();
			}
		}
	}
	
	/**
	 * 显示选择药品记录item
	 */
	private void showChooseResult() {
		layout_choose.removeAllViews();
		for (int i = 0; i < choose_list.size(); i++) {
			addMedicalItem(choose_list.get(i), i);
		}
	}
	
	private void addMedicalItem(MedicalRecordBean medicalRecordBean, int position) {
		if (medicalRecordBean != null) {
			layout_choose.addView(generateDataView(medicalRecordBean.getName(),
					medicalRecordBean.getUnit(), medicalRecordBean.getCount() ,medicalRecordBean.getId(), position));
			if (View.VISIBLE != layout_choose.getVisibility()) {
				layout_choose.setVisibility(View.VISIBLE);
			}
		}
	}
	
	/**
	 * 生成药品item 布局
	 * 
	 * @param name
	 * @param unit
	 * @param position
	 * @return
	 */
	@SuppressLint("InflateParams")
	private View generateDataView(final String name, final String unit,String count,
			final int id, final int position) {
		final RelativeLayout layout = (RelativeLayout) getLayoutInflater().inflate(
				R.layout.layout_medicine_add, null);
		TextView tv_name = (TextView) layout
				.findViewById(R.id.medicine_item_tv_name);
		TextView tv_num = (TextView) layout
				.findViewById(R.id.medicine_item_tv_num);
		ImageView img_del = (ImageView) layout
				.findViewById(R.id.medicine_delete);
		img_del.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				layout_choose.removeView(layout);
				choose_list.remove(position);
			}
		});
		tv_name.setText(name+" "+unit);
		tv_num.setText(count);
		return layout;
	}
	
	protected void findViewById() {
		this.setTitle("添加用药提醒");
		Button leftButton = this.getLeftButtonText(getResources().getString(
				R.string.basic_back));
		Button rightButton = this.getRightButtonText(getResources().getString(
				R.string.basic_save));
		RelativeLayout rl = (RelativeLayout) findViewById(R.id.titleBar);
		if (rl != null) {
			rl.setBackgroundColor(getResources().getColor(R.color.blue));
		}
		leftButton.setOnClickListener(this);
		rightButton.setOnClickListener(this);
		
		img_add = (ImageButton) findViewById(R.id.medicine_remind_img_add);
		tv_set_routing = (TextView) findViewById(R.id.medicine_remind_tv_set_routing);
		tv_routing = (TextView) findViewById(R.id.medicine_remind_tv_routing);
		et_remark = (EditText) findViewById(R.id.medicine_remind_et_remark);
		
		layout_time = (RelativeLayout) findViewById(R.id.medicine_layout_time);
		tv_time = (TextView) findViewById(R.id.medicine_tv_time);
		layout_choose = (LinearLayout) findViewById(R.id.linear_chose);
		
		img_add.setOnClickListener(this);
		tv_set_routing.setOnClickListener(this);
		tv_routing.setOnClickListener(this);
		layout_time.setOnClickListener(this);
	}

}
