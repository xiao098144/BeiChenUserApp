package com.ddoctor.user.activity.medicine;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.Window;
import android.widget.LinearLayout;

import com.ddoctor.user.wapi.bean.MedicalRecordBean;
import com.ddoctor.user.wapi.bean.MedicalRemindBean;
import com.ddoctor.utils.DialogUtil;
import com.ddoctor.utils.DialogUtil.ConfirmDialog;
import com.ddoctor.utils.MedicalDBUtil;
import com.ddoctor.utils.TimeUtil;
import com.ddoctor.utils.ToastUtil;
import com.umeng.analytics.MobclickAgent;
import com.zhuge.analysis.stat.ZhugeSDK;

public class RemindDialogActivity extends Activity {

	public static RemindDialogActivity _dialogInstance = null;

	private final static int REMINDLATER = 10; // 稍候提醒时间

	private ArrayList<MedicalRemindBean> _dataList;
	private Dialog _dialog = null;

	private Vibrator _vibrator;

	public static RemindDialogActivity getInstance() {
		return _dialogInstance;
	}

	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("RemindDialogActivity");
		MobclickAgent.onResume(RemindDialogActivity.this);
		ZhugeSDK.getInstance().init(getApplicationContext());
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("RemindDialogActivity");
		MobclickAgent.onPause(RemindDialogActivity.this);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);

		_dialogInstance = this;
		_vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

		LinearLayout ll = new LinearLayout(this);
		ll.setBackgroundColor(Color.TRANSPARENT);
		setContentView(ll);

		Bundle b = getIntent().getExtras();

		_dataList = b.getParcelableArrayList("data");

		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < _dataList.size(); i++) {
			MedicalRemindBean medicalRemindBean = _dataList.get(i);
			List<MedicalRecordBean> list2 = medicalRemindBean.getDataList();
			for (int j = 0; j < list2.size(); j++) {
				sb.append((j + 1) + "、");
				sb.append(list2.get(j).getName() + " ");
				sb.append(list2.get(j).getUnit() + " ");
				sb.append(list2.get(j).getCount() + "\n");
			}
		}

		_vibrator.vibrate(new long[] { 200, 600, 200, 600 }, 2);

		// 提示添加成功
		_dialog = DialogUtil
				.confirmDialog(this, sb.toString(), "关闭", "稍后提醒",
						new ConfirmDialog() {

							@Override
							public void onOKClick(Bundle data) {
								for (int i = 0; i < _dataList.size(); i++) {
									MedicalDBUtil.getIntance()
											.shutMedicalAlarmById(
													_dataList.get(i).getId(),
													_dataList.get(i)
															.getParentId());
								}

								_dialogInstance = null;
								_vibrator.cancel();
								RemindDialogActivity.this.finish();
							}

							@Override
							public void onCancleClick() {
								for (int i = 0; i < _dataList.size(); i++) {
									MedicalRemindBean medicalRemindBean = _dataList
											.get(i);
									String time = medicalRemindBean.getTime();
									String pattern = "HH:mm";
									if (time.contains("-")) {
										pattern = "HH-mm";
									}
									String minuteAdd = TimeUtil.getInstance()
											.minuteAdd(REMINDLATER, time,
													pattern);
									medicalRemindBean.setTime(minuteAdd);
									if (0 == _dataList.get(i).getParentId()) {
										medicalRemindBean
												.setParentId(medicalRemindBean
														.getId());
										MedicalDBUtil.getIntance()
												.insertMedicalRemind(
														medicalRemindBean);
									} else {
										MedicalDBUtil.getIntance()
												.updateMedicalRemindTimeById(
														minuteAdd,
														medicalRemindBean
																.getId());
									}
								}
								ToastUtil.showToast("十分钟后继续提醒");

								_dialogInstance = null;
								_vibrator.cancel();
								RemindDialogActivity.this.finish();
							}

						}).setTitle("用药提醒").show();

	}

	@Override
	protected void onDestroy() {
		if (_dialog != null)
			_dialog.dismiss();

		_vibrator.cancel();

		super.onDestroy();
	}
}
