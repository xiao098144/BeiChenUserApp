package com.ddoctor.user.activity.medicine;

import java.util.ArrayList;
import java.util.List;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.enums.RecordLayoutType;
import com.ddoctor.enums.RetError;
import com.ddoctor.interfaces.OnClickCallBackListener;
import com.ddoctor.user.activity.BaseActivity;
import com.ddoctor.user.activity.medicine.adapter.MedGridAdapter;
import com.ddoctor.user.activity.medicine.adapter.MedicineRecordAdapter;
import com.ddoctor.user.activity.medicine.adapter.MyViewPagerAdapter;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.task.GetMedicalRecordTask;
import com.ddoctor.user.task.TaskPostCallBack;
import com.ddoctor.user.view.PageControl;
import com.ddoctor.user.wapi.bean.MedicalRecordBean;
import com.ddoctor.user.wapi.bean.MedicalRemindBean;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.DialogUtil;
import com.ddoctor.utils.MedicalDBUtil;
import com.ddoctor.utils.MyUtils;
import com.ddoctor.utils.ToastUtil;
import com.umeng.analytics.MobclickAgent;
import com.zhuge.analysis.stat.ZhugeSDK;

public class MedicineActivity extends BaseActivity implements OnClickCallBackListener {

	private LinearLayout _layout_remind, _layout_record;
	private ViewPager viewpager;
	private LinearLayout layout;
	private Button btn_add;
	private ListView medicineRecordLv;
	private TextView _tv_norecord;

	private MedicineRecordAdapter adapter;
	/**
	 * 设置一页有多少数据
	 */
	public static final int PAGE_SIZE = 4;
	/**
	 * 动态加载页适配器
	 */
	private MyViewPagerAdapter viewpageradapter;

	/**
	 * 圆点导航控件
	 */
	private PageControl pageControl;
	/** 无提醒时显示默认图 */
	private TextView _tv_noremind;

	private List<GridView> listGrid;
	private Dialog loadingDialog;

	private int pageNum = 1;

	private List<MedicalRecordBean> dataList = new ArrayList<MedicalRecordBean>();

	private List<MedicalRecordBean> formatData(List<MedicalRecordBean> list) {
		List<MedicalRecordBean> resultList = new ArrayList<MedicalRecordBean>();
		for (int i = 0; i < list.size(); i++) {
			MedicalRecordBean medicalRecordBean = new MedicalRecordBean();
			if (i == 0
					|| (i >= 1 && !list.get(i).getDate()
							.equals(list.get(i - 1).getDate()))) {
				medicalRecordBean.setDate(list.get(i).getDate());
				medicalRecordBean.setLayoutType(RecordLayoutType.TYPE_CATEGORY);
				resultList.add(medicalRecordBean);
			}
			resultList.add(list.get(i));
		}
		return resultList;
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("MedicineActivity");
		MobclickAgent.onPause(MedicineActivity.this);
		ZhugeSDK.getInstance().init(getApplicationContext());
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (savedInstanceState != null)
			MyUtils.showLog("MedicineActivity onCreate() "
					+ savedInstanceState.toString());
		else
			MyUtils.showLog("MedicineActivity onCreate() ");
		setContentView(R.layout.activity_medicine);
		findViewById();
		initTopGrid();
		loadingData(true);
		getMedicalRemind();
	}

	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
		super.onSaveInstanceState(savedInstanceState);

		MyUtils.showLog("MedicineActivity::onSaveInstanceState");
	}

	@Override
	public void onStop() {
		MyUtils.showLog("MedicineActivity onStop()");
		super.onStop();
	}

	@Override
	public void onDestroy() {
		MyUtils.showLog("MedicineActivity onDestroy()");
		super.onDestroy();
	}

	List<MedicalRemindBean> remindList;

	private void getMedicalRemind() {
		remindList = MedicalDBUtil.getIntance().selectAllMedicalRemind();
		
		if (null != remindList && remindList.size() > 0) {
			showGrid();
		} else {
			_tv_noremind.setText("还没有提醒记录，快去添加吧");
			_tv_noremind.setVisibility(View.VISIBLE);
		}
	}

	private void loadingData(boolean showDialog) {
		if (showDialog) {
			loadingDialog = DialogUtil
					.createLoadingDialog(this, "正在加载中，请稍候...");
			loadingDialog.show();
		}
		GetMedicalRecordTask task = new GetMedicalRecordTask(pageNum);
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {

			@Override
			public void taskFinish(DDResult result) {
				if (result.getError() == RetError.NONE) {
					ArrayList<MedicalRecordBean> list = result.getBundle()
							.getParcelableArrayList("list");
					if (dataList != null && dataList.size() > 0) {
						dataList.clear();
					}
					dataList.addAll(formatData(list));
					adapter.notifyDataSetChanged();
					if (list.size() == 0) {
						_tv_norecord.setText(result.getErrorMessage());
						_tv_norecord.setTag(0);
						_tv_norecord.setVisibility(View.VISIBLE);
					}
				} else {
					loadingDialog.dismiss();
					ToastUtil.showToast(result.getErrorMessage());
				}
				loadingDialog.dismiss();
			}
		});

		task.executeParallel("");
	}

	private void initTopGrid() {
		listGrid = new ArrayList<GridView>();
		if (pageControl == null) {
			pageControl = new PageControl(this, layout);
		}
		if (viewpageradapter == null) {
			viewpageradapter = new MyViewPagerAdapter(this);
		}
		viewpageradapter.setArray(listGrid);
		viewpager.setAdapter(viewpageradapter);
		viewpager.setOnPageChangeListener(new MyListener());
	}

	private int currentPageCount ;
	
	private void showGrid() {
		MyUtils.showLog("数据集合  "+remindList.size()+" 最后一条数据  "+remindList.get(remindList.size()-1).toString());
		// 计算有多少页
		int PageCount = remindList.size() / PAGE_SIZE;
		if (remindList.size() % PAGE_SIZE != 0) {
			PageCount += 1;
		}
		MyUtils.showLog("",
				"更新界面 " + PageCount + " 实际数据条数  " + remindList.size()+" 已有数据集合 "+listGrid.size()+" "+listGrid.toString());
		for (int i = currentPageCount; i < PageCount; i++) {
			GridView gridView = new GridView(this);
			final MedGridAdapter adapter = new MedGridAdapter(this,i);
			adapter.setOnClickCallBackListener(this);
			gridView.setAdapter(adapter);
			gridView.setNumColumns(2);

			gridView.setVerticalSpacing(10);
			gridView.setHorizontalSpacing(10);
			gridView.setSelector(new ColorDrawable(Color.TRANSPARENT));
//			gridView.setOnItemClickListener(adapter);
			gridView.setOnItemLongClickListener(adapter);
			listGrid.add(gridView);
		} 
		
		for (int i = 0; i < PageCount; i++) {
			MedGridAdapter adapter = (MedGridAdapter) listGrid.get(i).getAdapter();
			adapter.setData(generateData(i));
		}
		
		MyUtils.showLog("", "处理完毕  grid item 数目 " + listGrid.size()+" "+listGrid.toString());
		  
		pageControl.setPageSize(PageCount);
		currentPageCount = PageCount;
		viewpageradapter.setArray(listGrid);
		viewpageradapter.notifyDataSetChanged();
		if (View.VISIBLE != layout.getVisibility()) {
			layout.setVisibility(View.VISIBLE);
			viewpager.setVisibility(View.VISIBLE);
			_tv_noremind.setVisibility(View.INVISIBLE);
		}
	}
	
	public List<MedicalRemindBean> generateData(int page){
		List<MedicalRemindBean>mList = new ArrayList<MedicalRemindBean>();
		// 根据当前页计算每页放那几个数据
		int k = page * PAGE_SIZE;// 当前页的起始位置
		int iEnd = k + PAGE_SIZE;// 所有数据的结束位置
		while ((k < remindList.size()) && (k < iEnd)) {
			mList.add(remindList.get(k));
			k++;
		}
		return mList;
	}

	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("MedicineActivity");
		MobclickAgent.onResume(MedicineActivity.this);
		if (DataModule.getInstance().isUpdateMedical()) {
			loadingData(false);
			DataModule.getInstance().setUpdateMedical(false);
		}
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.btn_left: {
			finishThisActivity();
		}
			break;
		case R.id.btn_right: {

		}
			break;
		case R.id.medicine_btn_add: {
			skip(MedicalListActivity.class, false);
		}
			break;

		case R.id.medicine_img_set: {
			skipForResult(MedicineSettingActivity.class, 300);
		}
			break;
		case R.id.medicine_img_record: {
			skip(MedicineRecordActivity.class, false);
		}
			break;
		default:
			break;
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode != RESULT_OK) {
			return;
		}
		if (requestCode == 300) { // 更新用药提醒
			getMedicalRemind();
		}
	}

	class MyListener implements OnPageChangeListener {

		@Override
		public void onPageScrollStateChanged(int arg0) {

		}

		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {

		}

		@Override
		public void onPageSelected(int arg0) {

			pageControl.selectPage(arg0);
		}

	}

	protected void findViewById() {

		this.setTitle(getResources()
				.getString(R.string.title_activity_medicine));
		Button leftButton = this.getLeftButtonText(getResources().getString(
				R.string.basic_back));
//		Button rightButton = this.getRightButtonText(getResources().getString(
//				R.string.sc_edit));
		RelativeLayout rl = (RelativeLayout) findViewById(R.id.titleBar);
		if (rl != null) {
			rl.setBackgroundColor(getResources().getColor(
					R.color.color_medical_title));
		}
		leftButton.setOnClickListener(this);
//		rightButton.setOnClickListener(this);

		viewpager = (ViewPager) findViewById(R.id.medicine_viewpager);
		layout = (LinearLayout) findViewById(R.id.medicine_viewGroup);
		_tv_noremind = (TextView) findViewById(R.id.medicine_tv_noremind);
		_layout_remind = (LinearLayout) findViewById(R.id.medicine_img_set);
		_layout_record = (LinearLayout) findViewById(R.id.medicine_img_record);
		btn_add = (Button) findViewById(R.id.medicine_btn_add);
		medicineRecordLv = (ListView) findViewById(R.id.medicine_record_lv);
		_tv_norecord = (TextView) findViewById(R.id.tv_norecord);
		_tv_norecord.setVisibility(View.INVISIBLE);
		medicineRecordLv.setEmptyView(_tv_norecord);
		adapter = new MedicineRecordAdapter(MedicineActivity.this);
		adapter.setData(dataList);
		medicineRecordLv.setAdapter(adapter);

		btn_add.setOnClickListener(this);
		_layout_record.setOnClickListener(this);
		_layout_remind.setOnClickListener(this);

	}

	@Override
	public void onClickCallBack(Bundle data) {
		loadingData(false);
	}

}
