package com.ddoctor.user.activity.medicine;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.enums.RetError;
import com.ddoctor.interfaces.OnClickCallBackListener;
import com.ddoctor.user.activity.BaseActivity;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.task.AddMedicalRecordTask;
import com.ddoctor.user.task.TaskPostCallBack;
import com.ddoctor.user.wapi.bean.MedicalBean;
import com.ddoctor.user.wapi.bean.MedicalRecordBean;
import com.ddoctor.utils.DialogUtil;
import com.ddoctor.utils.DialogUtil.OnTimeSelectedListener;
import com.ddoctor.utils.MedicalDBUtil;
import com.ddoctor.utils.MyUtils;
import com.ddoctor.utils.StringUtils;
import com.ddoctor.utils.TimeUtil;
import com.ddoctor.utils.ToastUtil;
import com.flowlayout.FlowLayout;
import com.flowlayout.FlowLayout.LayoutParams;
import com.umeng.analytics.MobclickAgent;
import com.zhuge.analysis.stat.ZhugeSDK;

public class MedicineTimeActivity extends BaseActivity implements
		OnTimeSelectedListener, OnClickCallBackListener {

	private Button leftBtn;
	private Button rightBtn;
	private FlowLayout linear_name;
	private LinearLayout layout_choose;
	private LinearLayout layout_click_choose;
	private RelativeLayout layout_time;
	private TextView tv_time;
//	private TextView tv_date;

	private LayoutInflater inflater;

	private String date;
	private String time;

	private ArrayList<MedicalRecordBean> choose_list = new ArrayList<MedicalRecordBean>();
	private List<MedicalBean> history;

	private int type;
	private void getIntentInfo() {
		Intent intent = getIntent();
		Bundle data = intent.getBundleExtra("data");
		MedicalRecordBean medicalRecordBean =data.getParcelable("data");
		type = data.getInt("type" , 0);
		choose_list.add(medicalRecordBean);
	}

	/**
	 * 显示常用药历史记录
	 */
	private void showMedicalHistory() {
		history = MedicalDBUtil.getIntance().selectMedicalHistory();
		if (null != history && history.size() > 0) {
			for (int i = 0; i < history.size(); i++) {
				MedicalBean medicalBean = history.get(i);
				linear_name.addView(generateHistoryItem(medicalBean.getName(),
						medicalBean.getUnit(), i));
			}
		} else {
			linear_name.setVisibility(View.GONE);
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("MedicineTimeActivity");
		MobclickAgent.onResume(MedicineTimeActivity.this);
		ZhugeSDK.getInstance().init(getApplicationContext());
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("MedicineTimeActivity");
		MobclickAgent.onPause(MedicineTimeActivity.this);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_medicine_time);
		inflater = LayoutInflater.from(this);
		getIntentInfo();
		findViewById();
		showMedicalHistory();
		showChooseResult();
		date = TimeUtil.getInstance().getStandardDate(getResources().getString(R.string.time_format_10));
	}

	/**
	 * 显示选择药品记录item
	 */
	private void showChooseResult() {
		layout_choose.removeAllViews();
		for (int i = 0; i < choose_list.size(); i++) {
			addMedicalItem(choose_list.get(i), i);
		}
	}

	private void addMedicalItem(MedicalRecordBean medicalRecordBean, int position) {
		if (medicalRecordBean != null) {
			layout_choose.addView(generateDataView(medicalRecordBean.getName(),
					medicalRecordBean.getUnit(), medicalRecordBean.getCount() ,medicalRecordBean.getId(), position));
			if (View.VISIBLE != layout_choose.getVisibility()) {
				layout_choose.setVisibility(View.VISIBLE);
			}
		}
	}

	private View generateHistoryItem(String name, String unit, final int position) {
		TextView tv = new TextView(this);
		tv.setGravity(Gravity.CENTER);
		tv.setSingleLine(true);
		LayoutParams params = new FlowLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		params.setMargins(8, 8, 8, 8);
		tv.setPadding(0, 5, 0, 5);
		tv.setBackgroundColor(Color.WHITE);
		tv.setLayoutParams(params);
		tv.setText(name + " " + unit);
		tv.setTextColor(getResources().getColor(R.color.color_text_gray_dark));
		tv.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				showInputDialog(history.get(position));
			}
		});
		return tv;
	}

	/**
	 * 生成药品item 布局
	 * 
	 * @param name
	 * @param unit
	 * @param position
	 * @return
	 */
	@SuppressLint("InflateParams")
	private View generateDataView(final String name, final String unit,String count,
			final int id, final int position) {
		final RelativeLayout layout = (RelativeLayout) inflater.inflate(
				R.layout.layout_medicine_add, null);
		TextView tv_name = (TextView) layout
				.findViewById(R.id.medicine_item_tv_name);
		TextView tv_num = (TextView) layout
				.findViewById(R.id.medicine_item_tv_num);
		ImageView img_del = (ImageView) layout
				.findViewById(R.id.medicine_delete);
		img_del.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				layout_choose.removeView(layout);
				choose_list.remove(position);
			}
		});
		tv_name.setText(name+" "+unit);
		tv_num.setText(count);
		return layout;
	}

	private boolean checkData() {
		if (null == choose_list || choose_list.size() == 0) {
			ToastUtil.showToast("请选择服用的药品");
			return false;
		}

		if (TextUtils.isEmpty(date) || TextUtils.isEmpty(time)) {
			ToastUtil.showToast("请选择服用药品的时间");
			return false;
		}

		for (int i = 0; i < choose_list.size(); i++) {
			choose_list.get(i).setDate(date);
			choose_list.get(i).setTime(time);
		}

		return true;
	}

	Dialog loadingDialog;

	private void uploadData() {
		loadingDialog = DialogUtil.createLoadingDialog(
				MedicineTimeActivity.this,
				getResources().getString(R.string.basic_loading));
		loadingDialog.show();

		AddMedicalRecordTask task = new AddMedicalRecordTask(choose_list);
		task.setTaskCallBack(new TaskPostCallBack<RetError>() {

			@Override
			public void taskFinish(RetError result) {
				if (result == RetError.NONE) {
					loadingDialog.dismiss();
					DataModule.getInstance().setUpdateMedical(true);
					finishThisActivity();
				} else {
					loadingDialog.dismiss();
					ToastUtil.showToast(result.getErrorMessage());
				}
			}
		});
		task.executeParallel("");
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.btn_left:
			finishThisActivity();
			break;
		case R.id.btn_right: {
			if (checkData()) {
				MyUtils.showLog("","type = "+type);
				if (type==2) {
					Intent intent = new Intent();
					Bundle data = new Bundle();
					data.putParcelableArrayList("data", choose_list);
					intent.putExtra("data", data);
					setResult(RESULT_OK, intent);
					finish();
				}else {
					uploadData();
				}
			}
		}
			break;
		case R.id.medicine_layout_click_to_choose: {
			Intent intent = new Intent();
			intent.putExtra("type", 1);
			intent.setClass(MedicineTimeActivity.this,
					MedicalListActivity.class);
			startActivityForResult(intent, 200);
		}
			break;
		case R.id.medicine_layout_time: {
//			DialogUtil.dateTimePicker(0, MedicineTimeActivity.this, this);
			DialogUtil.creatTimeDialog(MedicineTimeActivity.this, 0, getResources().getString(R.string.basic_cancel), getResources().getString(R.string.basic_confirm), this);
		}
			break;
		default:
			break;
		}
	}

	/**
	 * 判断该药品是否已选中，是 返回对应list index 否 返回-1 
	 * @param medicalRecordBean
	 * @return
	 */
	private int isContains(MedicalRecordBean medicalRecordBean) {
		if (medicalRecordBean == null) {
			return -1;
		}
		for (int i = 0; i < choose_list.size(); i++) {
			if (medicalRecordBean.getMedicalId().equals(choose_list.get(i).getMedicalId())) {
				return i;
			}
		}
		return -1;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
		super.onActivityResult(requestCode, resultCode, intent);
		if (resultCode != RESULT_OK) {
			return;
		}
		if (requestCode == 200) {
			Bundle data = intent.getBundleExtra("data");
			MedicalRecordBean medicalRecordBean =data.getParcelable("data");
			if (medicalRecordBean != null) {
				int index = isContains(medicalRecordBean);
				if (index>-1) {
					choose_list.get(index).setCount(medicalRecordBean.getCount());
				} else {
					choose_list.add(medicalRecordBean);
				}
				showChooseResult();
			}
		}
	}

	@Override
	public void ontimeSelected(int year, int month, int day, int hour,
			int minute) {
		StringBuffer dateStr = new StringBuffer();
		dateStr.append(year + "-" + StringUtils.formatnum(month, "00") + "-"
				+ StringUtils.formatnum(day, "00"));
		date = String.valueOf(dateStr);
//		tv_date.setText(date);
		StringBuffer timeStr = new StringBuffer();
		timeStr.append(StringUtils.formatnum(hour, "00") + "-"
				+ StringUtils.formatnum(minute, "00"));
		time = String.valueOf(timeStr);
		tv_time.setText(time);
	}

	protected void findViewById() {

		this.setTitle("设置时间");
		leftBtn = this.getLeftButtonText(getResources().getString(
				R.string.basic_back));
		rightBtn = this.getRightButtonText(getResources().getString(
				R.string.basic_save));
		RelativeLayout rl = (RelativeLayout) findViewById(R.id.titleBar);
		if (rl != null) {
			rl.setBackgroundColor(getResources().getColor(R.color.blue));
		}
		
		linear_name = (FlowLayout) findViewById(R.id.linear_name);
		layout_choose = (LinearLayout) findViewById(R.id.linear_chose);
		layout_click_choose = (LinearLayout) findViewById(R.id.medicine_layout_click_to_choose);
		layout_time = (RelativeLayout) findViewById(R.id.medicine_layout_time);
//		tv_date = (TextView) findViewById(R.id.medicine_tv_date);
		tv_time = (TextView) findViewById(R.id.medicine_tv_time);
		
		leftBtn.setOnClickListener(this);
		rightBtn.setOnClickListener(this);
		layout_click_choose.setOnClickListener(this);
		layout_time.setOnClickListener(this);
	}

	float count = 1;
	private void showInputDialog(final MedicalBean medicalBean) {
		final Dialog dialog = new Dialog(MedicineTimeActivity.this, R.style.NoTitleDialog);
		View view = LayoutInflater.from(MedicineTimeActivity.this).inflate(
				R.layout.layout_medical_dose, null);
		TextView tv_title = (TextView) view
				.findViewById(R.id.medical_dose_dialog_tv_title);
		ImageButton left = (ImageButton) view
				.findViewById(R.id.medical_dose_left);
		ImageButton right = (ImageButton) view
				.findViewById(R.id.medical_dose_right);
		final EditText et_input = (EditText) view
				.findViewById(R.id.medical_dose_et_input);
		Button btn_confirm = (Button) view
				.findViewById(R.id.bottom_btn_confirm);
		Button btn_cancel = (Button) view.findViewById(R.id.bottom_btn_cancel);
		et_input.setText(count+"");
		tv_title.setText("请输入剂量");

		et_input.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if (start > 1)
                {
                      float num = Float.parseFloat(s.toString());
                      if(num < 1)
                          s = String.valueOf(1);
                      return;
                } 
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				if (s != null && !s.equals(""))
                {
                         float markVal = count;
                         try
                         {
                             markVal = Float.parseFloat(s.toString());
                         }
                         catch (NumberFormatException e)
                         {
                             markVal = count;
                         }
                         if (markVal < 1)
                         {
                        	 ToastUtil.showToast("服用剂量不能小于1");
                             et_input.setText("1");
                         }
                         return;
                 }
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}
		});
		
		OnClickListener onClickListener = new OnClickListener() {

			@Override
			public void onClick(View v) {
				switch (v.getId()) {
				case R.id.bottom_btn_cancel: {
					dialog.dismiss();
				}
					break;
				case R.id.bottom_btn_confirm: {
					String trim = et_input.getText().toString().trim();
					if (TextUtils.isEmpty(trim)) {
						ToastUtil.showToast("请输入目标数值");
					} else if (!StringUtils.pureNum(trim)) {
						ToastUtil.showToast("数据异常，请重新输入");
					} else {
						
						MedicalRecordBean medicalRecordBean = new MedicalRecordBean();
						medicalRecordBean.copyFromMedicalBean(medicalBean);
						if (trim.endsWith(".0")) {
							trim = trim.substring(0,trim.length()-2);
						}
						medicalRecordBean.setCount(trim+""+medicalBean.getUnit().substring(medicalBean.getUnit().length()-1));
						int index = isContains(medicalRecordBean);
						if (index>-1) {
							choose_list.get(index).setCount(medicalRecordBean.getCount());
						} else {
							choose_list.add(medicalRecordBean);
						}
						showChooseResult();

						dialog.dismiss();
					}
				}
					break;
				case R.id.medical_dose_left: {
					if (count==1) {
						ToastUtil.showToast("剂量不能小于1");
					}else {
						count--;
						et_input.setText(String.valueOf(count));
					}
				}
					break;
				case R.id.medical_dose_right: {
					count++;
					et_input.setText(String.valueOf(count));
				}
					break;

				default:
					break;
				}
			}
		};
		
		left.setOnClickListener(onClickListener);
		right.setOnClickListener(onClickListener);
		btn_cancel.setOnClickListener(onClickListener);
		btn_confirm.setOnClickListener(onClickListener);

		Window window = dialog.getWindow();
		window.setGravity(Gravity.CENTER);
		dialog.setCancelable(true);
		dialog.setCanceledOnTouchOutside(true);
		dialog.setContentView(view, new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.MATCH_PARENT,
				LinearLayout.LayoutParams.MATCH_PARENT));
		dialog.show();
	}

	@Override
	public void onClickCallBack(Bundle data) {
		StringBuffer sb = new StringBuffer();
		sb.append(StringUtils.formatnum(
				Integer.valueOf(data.getString("hour")), "00"));
		sb.append(":");
		sb.append(StringUtils.formatnum(
				Integer.valueOf(data.getString("minute")), "00"));
		time = sb.toString();
		tv_time.setText(sb);
	}

}
