package com.ddoctor.user.activity.medicine.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.enums.RecordLayoutType;
import com.ddoctor.user.adapter.BaseAdapter;
import com.ddoctor.user.wapi.bean.MedicalRecordBean;
import com.ddoctor.utils.TimeUtil;

public class MedicineRecordAdapter extends BaseAdapter<MedicalRecordBean>{

	public MedicineRecordAdapter(Context context) {
		super(context);
		
	}
	@Override
	public int getViewTypeCount() {
		return 2;
	}

	@Override
	public int getItemViewType(int position) {
		if (dataList==null || dataList.size()==0) {
			return RecordLayoutType.TYPE_CATEGORY.ordinal();
		}
		if (RecordLayoutType.TYPE_CATEGORY == dataList.get(position).getLayoutType()) {
			return RecordLayoutType.TYPE_CATEGORY.ordinal();
		} else {
			return RecordLayoutType.TYPE_VALUE.ordinal();
		}
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		RecordLayoutType layoutType = RecordLayoutType.values()[getItemViewType(position)];
		switch (layoutType) {
		case TYPE_CATEGORY: {
			CategoryHolder category = null;
			if (null == convertView) {
				convertView = inflater.inflate(
						R.layout.layout_recordlist_date_item, parent, false);
				category = new CategoryHolder();
				category.tv_date = (TextView) convertView
						.findViewById(R.id.recordlist_date_item_tv_date);
				convertView.setTag(category);
			} else {
				category = (CategoryHolder) convertView.getTag();
			}
			category.tv_date.setText(dataList.get(position).getDate());
		}
			break;
		case TYPE_VALUE: {
			ValueHolder valueHolder = null;
			if (null == convertView) {
				convertView = inflater.inflate(R.layout.layout_recordlist_item,
						parent, false);  
				valueHolder = new ValueHolder();
				valueHolder.tv_left = (TextView) convertView
						.findViewById(R.id.record_item_tv_left);
				valueHolder.tv_middle = (TextView) convertView
						.findViewById(R.id.record_item_tv_middle);
				valueHolder.tv_right = (TextView) convertView
						.findViewById(R.id.record_item_tv_right);
				
				RelativeLayout.LayoutParams params =  (LayoutParams) valueHolder.tv_middle.getLayoutParams();
				params.addRule(RelativeLayout.RIGHT_OF, valueHolder.tv_left.getId());
				params.addRule(RelativeLayout.LEFT_OF, valueHolder.tv_right.getId());
				
				convertView.setTag(valueHolder);
			} else {
				valueHolder = (ValueHolder) convertView.getTag();
			}
			
			String time = dataList.get(position).getTime();
			
			time = TimeUtil.getInstance().formatTime(time);
			
			if (!TextUtils.isEmpty(time)&&time.contains("-")) {
				time = time.replace("-", ":");
			}
			valueHolder.tv_left.setText(time);
			valueHolder.tv_middle.setText(dataList.get(position).getName());
			valueHolder.tv_right.setText(dataList.get(position).getCount());
		}
			break;
		default:
			break;
		}

		return convertView;
	}

	@Override
	public boolean areAllItemsEnabled() {
		return false;
	}

	@Override
	public boolean isEnabled(int position) {
		if (RecordLayoutType.TYPE_CATEGORY == RecordLayoutType.values()[getItemViewType(position)]) {
			return false;
		} else {
			return true;
		}
	}

	private class CategoryHolder {
		private TextView tv_date;
	}

	private class ValueHolder {
		private TextView tv_left;
		private TextView tv_middle;
		private TextView tv_right;
	}

}
