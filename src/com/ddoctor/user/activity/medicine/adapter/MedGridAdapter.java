package com.ddoctor.user.activity.medicine.adapter;

import java.util.ArrayList;
import java.util.List;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.enums.RetError;
import com.ddoctor.interfaces.OnClickCallBackListener;
import com.ddoctor.user.activity.medicine.MedicineActivity;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.task.AddMedicalRecordTask;
import com.ddoctor.user.task.TaskPostCallBack;
import com.ddoctor.user.wapi.bean.MedicalRecordBean;
import com.ddoctor.user.wapi.bean.MedicalRemindBean;
import com.ddoctor.utils.DialogUtil;
import com.ddoctor.utils.MedicalDBUtil;
import com.ddoctor.utils.MyUtils;
import com.ddoctor.utils.ToastUtil;

/**
 * 用于GridView装载数据的适配器
 * 
 * @author康
 * 
 */
public class MedGridAdapter extends BaseAdapter implements
		OnItemLongClickListener , OnItemClickListener {
	private List<MedicalRemindBean> mList = new ArrayList<MedicalRemindBean>();
	private Context mContext;// 上下文
	private int page;

	private OnClickCallBackListener onClickCallBackListener;

	public void setOnClickCallBackListener(
			OnClickCallBackListener onClickCallBackListener) {
		this.onClickCallBackListener = onClickCallBackListener;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		MyUtils.showLog("MedGridAdapter " + page);
		this.page = page;
	}

	public MedGridAdapter(Context mContext, int page) {
		super();
		this.mContext = mContext;
	}

	public MedGridAdapter(Context context, List<MedicalRemindBean> list,
			int page) {
		mContext = context;
		this.page = page;
		// 根据当前页计算每页放那几个数据
		int i = page * MedicineActivity.PAGE_SIZE;// 当前页的起始位置
		int iEnd = i + MedicineActivity.PAGE_SIZE;// 所有数据的结束位置
		while ((i < list.size()) && (i < iEnd)) {
			mList.add(list.get(i));
			i++;
		}
	}

	public void setData(List<MedicalRemindBean> mList) {
		this.mList = mList;
		notifyDataSetChanged();
	}

	public int getCount() {

		return mList == null ? 0 : mList.size();
	}

	public Object getItem(int position) {

		return mList == null ? null : mList.get(position);
	}

	public long getItemId(int position) {

		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder holder;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = LayoutInflater.from(mContext).inflate(
					R.layout.layout_med_grid_item, parent, false);
			holder.tv_name = (TextView) convertView.findViewById(R.id.med_name);
			holder.tv_num = (TextView) convertView.findViewById(R.id.med_num);
			holder.tv_describe = (TextView) convertView
					.findViewById(R.id.med_describe);
			holder.tv_date = (TextView) convertView.findViewById(R.id.med_date);
//			holder.layout_data = convertView.findViewById(R.id.data);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		MedicalRemindBean medicalRemindBean = mList.get(position);
		List<MedicalRecordBean> dataList = medicalRemindBean.getDataList();
		String time = medicalRemindBean.getTime();
		if (time.contains("-")) {
			time = time.replace("-", ":");
		}
		holder.tv_date.setText(time);

		StringBuffer sb = new StringBuffer();
		sb.append(dataList.get(0).getName());
		String unit = dataList.get(0).getUnit();
		char charAt = unit.charAt(unit.length() - 1);
		String count = dataList.get(0).getCount();
		sb.append(" " + count.substring(0, count.length() - 1) + "" + charAt);
		if (dataList.size() >= 2) {
			sb.append("\n" + dataList.get(1).getName());
			unit = dataList.get(1).getUnit();
			charAt = unit.charAt(unit.length() - 1);
			count = dataList.get(1).getCount();
			sb.append(" " + count.substring(0, count.length() - 1) + ""
					+ charAt);
		}

		holder.tv_name.setText(sb.toString());

		// holder.tv_num.setText(count.substring(0,count.length()-1)+""+charAt);

		return convertView;
	}

	class ViewHolder {
		TextView tv_name, tv_num, tv_describe, tv_date;
		LinearLayout layout_data , layout_operation;
		TextView tv_delete , tv_done;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		showDialog(view, mList.get(position));
	}
	
	@Override
	public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		showDialog(arg1, mList.get(arg2));
		// ToastUtil.showToast(" OnItemLOngClick arg2 "+arg2);
		// Bundle data = new Bundle();
		// data.putParcelable("data", mList.get(arg2));
		MyUtils.showLog(mList.get(arg2).toString() + " "
				+ mList.get(arg2).getDataList().toString());
		// MedicalDBUtil.getIntance().deleteMedicalRemindRecordById(id);
		return false;
	}

	Dialog loadingDialog;

	private void uploadData(MedicalRemindBean medicalRemindBean) {
		loadingDialog = DialogUtil.createLoadingDialog(mContext);
		loadingDialog.show();
		List<MedicalRecordBean> choose_list  = medicalRemindBean.getDataList();
		for (int i = 0; i < choose_list.size(); i++) {
			if (TextUtils.isEmpty(choose_list.get(i).getTime())) {
				choose_list.get(i).setTime(medicalRemindBean.getTime());
			}
		}
		
		AddMedicalRecordTask task = new AddMedicalRecordTask(choose_list);
		task.setTaskCallBack(new TaskPostCallBack<RetError>() {

			@Override
			public void taskFinish(RetError result) {
				if (result == RetError.NONE) {
					loadingDialog.dismiss();
					DataModule.getInstance().setUpdateMedical(true);
				} else {
					loadingDialog.dismiss();
					ToastUtil.showToast(result.getErrorMessage());
				}
			}
		});
		task.executeParallel("");
	}

	public void showDialog(View parent,
			final MedicalRemindBean medicalRemindBean) {
		View view = LayoutInflater.from(mContext).inflate(
				R.layout.layout_medicine_operation, null);
		final PopupWindow popupWindow = new PopupWindow(view,
				LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		// popupWindow.setBackgroundDrawable(new PaintDrawable());
		popupWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		// 设置点击窗口外边窗口消失
		popupWindow.setOutsideTouchable(true);
		// 设置此参数获得焦点，否则无法点击
		popupWindow.setFocusable(true);
		popupWindow.showAsDropDown(parent, -20, 0);
		popupWindow.update();
		view.setOnKeyListener(new OnKeyListener() {

			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (keyCode == KeyEvent.KEYCODE_BACK) {
					popupWindow.dismiss();
					return true;
				}
				return false;
			}
		});
		final Bundle data = new Bundle();
		data.putInt("type", 1);
		TextView tv_done = (TextView) view
				.findViewById(R.id.medicine_operation_tv_done);
		TextView tv_delete = (TextView) view
				.findViewById(R.id.medicine_operation_tv_delete);
		tv_done.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				uploadData(medicalRemindBean);
				MedicalDBUtil.getIntance().deleteMedicalRemindRecordById(
						medicalRemindBean.getId());
				mList.remove(medicalRemindBean);
				notifyDataSetChanged();
				onClickCallBackListener.onClickCallBack(data);
				popupWindow.dismiss();
			}
		});

		tv_delete.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				MedicalDBUtil.getIntance().deleteMedicalRemindRecordById(
						medicalRemindBean.getId());
				mList.remove(medicalRemindBean);
				notifyDataSetChanged();
				onClickCallBackListener.onClickCallBack(data);
				popupWindow.dismiss();
			}
		});

	}

}
