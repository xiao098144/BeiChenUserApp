package com.ddoctor.user.activity.medicine;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.beichen.user.R;
import com.ddoctor.user.activity.BaseFragmentActivity;
import com.ddoctor.user.adapter.TypeIndicatorPagerAdapter;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.data.MyProfile;
import com.ddoctor.user.fragment.MedicalListFragment;
import com.ddoctor.user.wapi.bean.MedicalBean;
import com.ddoctor.user.wapi.constant.MedicalType;
import com.ddoctor.utils.ToastUtil;
import com.pageindicator.TabPageIndicator;
import com.umeng.analytics.MobclickAgent;
import com.zhuge.analysis.stat.ZhugeSDK;

public class MedicalListActivity extends BaseFragmentActivity {

	private Button leftBtn;
	private FrameLayout top;
	private TabPageIndicator indicator;
	private ViewPager viewPager;

	private TypeIndicatorPagerAdapter adapter;

	private String[] tabTitle;
	private List<Fragment> _fragmentList = new ArrayList<Fragment>();
	private MedicalListFragment fragment1 = null;
	private MedicalListFragment fragment2 = null;
	private MedicalListFragment fragment3 = null;
//	private MedicalListFragment fragment4 = null;

	private ArrayList<MedicalBean> list1 = new ArrayList<MedicalBean>();
	private ArrayList<MedicalBean> list2 = new ArrayList<MedicalBean>();
	private ArrayList<MedicalBean> list3 = new ArrayList<MedicalBean>();
//	private ArrayList<MedicalBean> list4 = new ArrayList<MedicalBean>();

	/** 点击操作类型   判断来源页面  0 用药首页  1  添加用药记录  2 添加用药提醒 */
	private int type = 0;  	

	private void getIntentInfo(){
		Intent intent = getIntent();
		type = intent.getIntExtra("type", 0);
	}
	
	private void initData() {
		tabTitle = getResources().getStringArray(R.array.medical_type);
		fragment1 = new MedicalListFragment();
		fragment2 = new MedicalListFragment();
		fragment3 = new MedicalListFragment();
//		fragment4 = new MedicalListFragment();
		_fragmentList.add(fragment1);
		_fragmentList.add(fragment2);
		_fragmentList.add(fragment3);
//		_fragmentList.add(fragment4);
	}
	
	@Override
	protected void onResume(){
		super.onResume();
		MobclickAgent.onResume(MedicalListActivity.this);
		ZhugeSDK.getInstance().init(getApplicationContext());
	}
	
	@Override
	protected void onPause(){
		super.onPause();
		MobclickAgent.onPause(MedicalListActivity.this);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_typeindicatorlist);
		getIntentInfo();
		initData();
		findViewById();
		loadingData();
	}


	private void loadingData() {
		List<MedicalBean> medicalList = DataModule.loadDict(
				MyProfile.DICT_MEDICALS, MedicalBean.class);
		if (null == medicalList || medicalList.size() == 0) {
			ToastUtil.showToast("药品列表数据异常");
		} else {
			for (int i = 0; i < medicalList.size(); i++) {
				MedicalBean medicalBean = medicalList.get(i);
				
				Integer type = medicalBean.getType();
				switch (type) {
				case MedicalType.JIANGTANGYAO: {
					list1.add(medicalBean);
				}
					break;
				case MedicalType.JIANGYAYAO: {
					list2.add(medicalBean);
				}
					break;
				case MedicalType.TIAOZHIYAO: {
					list3.add(medicalBean);
				}
					break;
				// case MedicalType.YIDAOSU: {
				// list4.add(medicalBean);
				// }
				// break;

				default:
					break;
				}
			}
			fragment1.setArguments(generateData(1, tabTitle[0], list1));
			fragment2.setArguments(generateData(2, tabTitle[1], list2));
			fragment3.setArguments(generateData(3, tabTitle[2], list3));
//			fragment4.setArguments(generateData(4, tabTitle[3], list4));
		}
		adapter = new TypeIndicatorPagerAdapter(tabTitle, _fragmentList, getSupportFragmentManager());
		viewPager.setAdapter(adapter);
		indicator.setViewPager(viewPager);
	}

	private Bundle generateData(int medicalType , String name , ArrayList<MedicalBean> list){
		Bundle data  = new Bundle();
		data.putInt("type", type);
		data.putInt("medicalType", medicalType);
		data.putString("title", name);
		data.putParcelableArrayList("data", list);
		return data;
	}

	protected void findViewById() {
		RelativeLayout rl = (RelativeLayout) findViewById(R.id.titleBar);
		if (rl != null) {
			rl.setBackgroundColor(getResources().getColor(
					R.color.color_medical_title));
		}
		top = (FrameLayout) findViewById(R.id.typeindicator_top);
		top.setBackgroundColor(getResources().getColor(
				R.color.color_medical_title));
		leftBtn = getLeftButtonText(getString(R.string.basic_back));
		setTitle(getString(R.string.medicine_add_medical));
		
		indicator = (TabPageIndicator) findViewById(R.id.type_indicator);
		indicator.setSelectedColor(getResources().getColor(
				R.color.color_medical_title));
		viewPager = (ViewPager) findViewById(R.id.pager);
		leftBtn.setOnClickListener(this);
	}


	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.btn_left: {
			finishThisActivity();
		}
			break;
		default:
			break;
		}
	}

//	@Override
//	protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
//		super.onActivityResult(requestCode, resultCode, intent);
//		if (resultCode!=RESULT_OK) {
//			return;
//		}
//		if (requestCode==300) {
//			Bundle data = new Bundle();
//			data.putAll(intent.getBundleExtra("data"));
//			intent.putExtra("data", data);
//			setResult(RESULT_OK, intent);
//			finish();
//		}
//	}
	
}
