package com.ddoctor.user.activity.mine;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.enums.RetError;
import com.ddoctor.user.activity.BaseActivity;
import com.ddoctor.user.task.FeedBackTask;
import com.ddoctor.user.task.TaskPostCallBack;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.StringUtils;
import com.ddoctor.utils.ToastUtil;
import com.umeng.analytics.MobclickAgent;
import com.zhuge.analysis.stat.ZhugeSDK;

/**
 * 用户反馈
 * @author 萧
 * @Date 2015-6-25上午10:25:14
 * @TODO TODO
 */
public class UserFeedbackActivity extends BaseActivity implements TextWatcher {
	private EditText met_content;
	private TextView mtv_now;

	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("UserFeedbackActivity");
		MobclickAgent.onResume(UserFeedbackActivity.this);
		ZhugeSDK.getInstance().init(getApplicationContext());
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("UserFeedbackActivity");
		MobclickAgent.onPause(UserFeedbackActivity.this);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_userfeedback);
		findViewById();
	}

	protected void findViewById() {
		Button leftBtn = getLeftButtonText(getString(R.string.basic_back));
		Button rigthBtn = getRightButtonText(getString(R.string.basic_upload));
		setTitle(getString(R.string.mine_setting_feedback));
		
		met_content = (EditText) findViewById(R.id.feedback_et_describe);
		mtv_now = (TextView) findViewById(R.id.feedback_tv_now);

		leftBtn.setOnClickListener(this);
		rigthBtn.setOnClickListener(this);
		met_content.addTextChangedListener(this);
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.btn_left: {
			finishThisActivity();
		}
			break;
		case R.id.btn_right: {
			String trim = met_content.getText().toString().trim();
			if (TextUtils.isEmpty(trim)) {
				ToastUtil.showToast("反馈内容不能为空");
			}else {
				feedBack(trim);
			}
		}
			break;
		default:
			break;
		}
	}

	private void feedBack(String content){
		FeedBackTask task = new FeedBackTask(content);
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {
			
			@Override
			public void taskFinish(DDResult result) {
				if (result.getError() == RetError.NONE) {
					met_content.setText("");
					ToastUtil.showToast("反馈成功。");
					finishThisActivity();
				} else {
					ToastUtil.showToast(result.getErrorMessage());
				}
			}
		});
		task.executeParallel("");
	}
	
	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
		int length = 0;
		if (s!=null) {
			length = s.length();
		}
		mtv_now.setText(StringUtils.formatnum(length, "00"));
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		// TODO Auto-generated method stub

	}

	@Override
	public void afterTextChanged(Editable s) {
		int length = 0;
		if (s!=null) {
			length = s.length();
		}
		mtv_now.setText(StringUtils.formatnum(length, "00"));
	}
}