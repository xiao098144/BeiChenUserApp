package com.ddoctor.user.activity.mine;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.user.activity.BaseActivity;
import com.ddoctor.user.adapter.MyFriendsListAdapter;
import com.ddoctor.user.adapter.MyFriendsListAdapter.FriendListViewHolder;
import com.ddoctor.user.config.AppBuildConfig;
import com.ddoctor.user.model.MyFriendBean;
import com.ddoctor.user.swiplistview.RefreshListView;
import com.ddoctor.user.view.ClearEditText;
import com.ddoctor.utils.StringUtils;
import com.ddoctor.utils.ToastUtil;
import com.sortlist.DePinyinHelper;
import com.sortlist.SideBar;
import com.sortlist.SideBar.OnTouchingLetterChangedListener;
import com.umeng.analytics.MobclickAgent;
import com.zhuge.analysis.stat.ZhugeSDK;

public class MyFriendsActivity extends BaseActivity implements
		OnTouchingLetterChangedListener, OnItemClickListener {

	private Button leftBtn;
	private Button rightBtn;
	private ClearEditText et_search;
	private ImageButton img_search;
	private SideBar sideBar;
	private TextView tv_show;
	private RefreshListView refreshLv;

	private MyFriendsListAdapter adapter;
	private List<MyFriendBean> dataList;
	private List<MyFriendBean> chosed;

	private DePinyinHelper pinyinHelper;
	
	private void initData() {
		pinyinHelper = DePinyinHelper.getInstance(MyFriendsActivity.this);
		dataList = new ArrayList<MyFriendBean>();
//		chosed = new ArrayList<MyFriendBean>();
		for (int i = 0; i < 15; i++) {
			MyFriendBean friendBean = new MyFriendBean();
			friendBean.setId(i+"");
			friendBean.setPicUrl(""+i);
			friendBean.setContent("东方红赶紧回家大概说地近");
			if (i%3==0) {
				friendBean.setDocName("发给贵"+i);
			}else if (i%3==1) {
				friendBean.setDocName("解决"+i);
			}else {
				friendBean.setDocName("广告人"+i);
			}
			friendBean.setSortLetter(pinyinHelper.getFirstPinyins(friendBean.getDocName()).toUpperCase(Locale.getDefault()));
			friendBean.setFullPinyin(pinyinHelper.getPinyins(friendBean.getDocName(), ""));
			dataList.add(friendBean);
		}
		Collections.sort(dataList);
	}
	boolean fromGroup = false;
	private void getIntentInfo(){
		Intent intent = getIntent();
		if (intent!=null) {
			Bundle bundle = intent.getBundleExtra(AppBuildConfig.BUNDLEKEY);
			if (bundle != null) {
				fromGroup = bundle.getBoolean("fromGroup", false);
				if (fromGroup) {
					chosed = bundle.getParcelableArrayList("chosed");
				}
			}
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("MyFriendsActivity");
		MobclickAgent.onResume(MyFriendsActivity.this);
		ZhugeSDK.getInstance().init(getApplicationContext());
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("MyFriendsActivity");
		MobclickAgent.onPause(MyFriendsActivity.this);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_myfriendslsit);
		getIntentInfo();
		findViewById();
		initData();
		adapter.setData(dataList , chosed);
	}

	protected void findViewById() {
		leftBtn = getLeftButtonText(getResources().getString(
				R.string.basic_back));
		rightBtn = getRightButton();
		rightBtn.setVisibility(View.VISIBLE);
		int righttextRes = fromGroup?R.string.basic_finish:R.string.mine_friends_add;
		rightBtn = getRightButtonText(getResources().getString(righttextRes));
		int titleRes = fromGroup?R.string.group_contact_title:R.string.mine_friends;
		setTitle(getResources().getString(titleRes));
		
		et_search = (ClearEditText) findViewById(R.id.myfriendslist_search_et);
		et_search.setHint(StringUtils.fromatETHint(getResources().getString(R.string.et_hint_myfriend),13));
		img_search = (ImageButton) findViewById(R.id.myfriendslist_search_btn);
		refreshLv = (RefreshListView) findViewById(R.id.myfriendslist_lv);
		sideBar = (SideBar) findViewById(R.id.myfriendslist_sidebar);
		tv_show = (TextView) findViewById(R.id.myfriendslist_tv);
		adapter = new MyFriendsListAdapter(this , fromGroup);
		refreshLv.setAdapter(adapter);
		sideBar.setTextView(tv_show);
		sideBar.setOnTouchingLetterChangedListener(this);
		
		refreshLv.setOnItemClickListener(this);
		leftBtn.setOnClickListener(this);
		rightBtn.setOnClickListener(this);
		img_search.setOnClickListener(this);
		et_search.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				filterData(s.toString());
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {

			}
		});
		
	}


	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.btn_left: {
			finishThisActivity();
		}
			break;
		case R.id.btn_right: {
			if (fromGroup) {
				ArrayList<MyFriendBean> array = adapter.getSelectedTextList();
				Intent data = new Intent();
				data.putExtra(AppBuildConfig.BUNDLEKEY, array);
				setResult(RESULT_OK, data);
			}else {
				ToastUtil.showToast("加好友");
			}
			finishThisActivity();
		}
			break;
		case R.id.myfriendslist_search_btn: {

		}
			break;
		default:
			break;
		}
	}

	private void filterData(String filterStr){
		List<MyFriendBean> filterDateList = new ArrayList<MyFriendBean>();
		
		if(TextUtils.isEmpty(filterStr)){
			filterDateList = dataList;
		}else{
			filterDateList.clear();
			for (MyFriendBean myFriendBean : dataList) {
				String name = myFriendBean.getDocName();
				if(name.indexOf(filterStr.toString()) != -1 || pinyinHelper.getFirstPinyins(name).startsWith(filterStr.toString())){
					filterDateList.add(myFriendBean);
				}
			}
		}
		Collections.sort(filterDateList);
		adapter.setData(filterDateList);
	}
	
	@Override
	public void onTouchingLetterChanged(String s) {
		int position = adapter.getPositionForSection(s.charAt(0));
		if (position != -1) {
			refreshLv.setSelection(position);
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		FriendListViewHolder holder = (FriendListViewHolder) view.getTag();

		holder.cb.toggle();

		adapter.getIsSelected().put(position,
				holder.cb.isChecked());
		if (holder.cb.isChecked()) {
			adapter.getSelectedTextSparseArray().put(position,
					dataList.get(position));
		} else {
			adapter.getSelectedTextSparseArray().delete(position);
		}
	}

}
