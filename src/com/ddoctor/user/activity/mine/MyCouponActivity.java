package com.ddoctor.user.activity.mine;

import android.os.Bundle;
import android.view.View;

import com.beichen.user.R;
import com.ddoctor.user.activity.BaseActivity;
import com.ddoctor.utils.ToastUtil;
import com.umeng.analytics.MobclickAgent;
import com.zhuge.analysis.stat.ZhugeSDK;

public class MyCouponActivity extends BaseActivity{

	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("MyCouponActivity");
		MobclickAgent.onResume(MyCouponActivity.this);
		ZhugeSDK.getInstance().init(getApplicationContext());
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("MyCouponActivity");
		MobclickAgent.onPause(MyCouponActivity.this);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_my_coupon_list);
		
		setTitle("我的优惠劵");
		getLeftButton().setText("返回");
		getLeftButton().setVisibility(View.VISIBLE);
		getLeftButton().setOnClickListener(this);
		
		ToastUtil.showToast("您暂时没有优惠劵！");
	}
	
	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.btn_left:{
			finishThisActivity();
		}
			break;
		case R.id.btn_right:{

		}
		break;
		default:
			break;
		}
	}
}
