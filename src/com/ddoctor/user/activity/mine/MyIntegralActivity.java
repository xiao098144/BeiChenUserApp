package com.ddoctor.user.activity.mine;

import java.util.ArrayList;
import java.util.List;

import android.app.Dialog;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.enums.RefreshAction;
import com.ddoctor.enums.RetError;
import com.ddoctor.user.activity.BaseActivity;
import com.ddoctor.user.adapter.IntegralListAdapter;
import com.ddoctor.user.task.GetIntegralBalanceTask;
import com.ddoctor.user.task.GetIntegralRecordListTask;
import com.ddoctor.user.task.TaskPostCallBack;
import com.ddoctor.user.view.DDPullToRefreshView;
import com.ddoctor.user.view.DDPullToRefreshView.OnHeaderRefreshListener;
import com.ddoctor.user.view.JumpTextView;
import com.ddoctor.user.wapi.bean.PointBean;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.DialogUtil;
import com.ddoctor.utils.ToastUtil;
import com.umeng.analytics.MobclickAgent;
import com.zhuge.analysis.stat.ZhugeSDK;

public class MyIntegralActivity extends BaseActivity implements OnHeaderRefreshListener, OnScrollListener{

	private JumpTextView mtv_show;
	private ListView _listView;
	DDPullToRefreshView _refreshViewContainer;
	private View _getMoreView;
	private TextView _tv_norecord;
	
	private int _pageNum = 1;
	private RefreshAction _refreshAction = RefreshAction.PULLTOREFRESH;
	
	private IntegralListAdapter _adapter;
	
	private int mPage = 1;

	private List<PointBean> _dataList = new ArrayList<PointBean>(); // 传递到adapter

	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("MyIntegralActivity");
		MobclickAgent.onResume(MyIntegralActivity.this);
		ZhugeSDK.getInstance().init(getApplicationContext());
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("MyIntegralActivity");
		MobclickAgent.onPause(MyIntegralActivity.this);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_myintegral);
		findViewById();
		getIntegralBalance();
		getIntegralList(true , mPage);
	}
	
	private void getIntegralBalance() {

		GetIntegralBalanceTask task = new GetIntegralBalanceTask();
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {

			@Override
			public void taskFinish(DDResult result) {
				if (result.getError() == RetError.NONE) {
					mtv_show.setText(String.valueOf(result.getBundle().getInt("point", 0)));
				} else {
					ToastUtil.showToast(result.getErrorMessage());
				}
			}
		});
		task.executeParallel("");

	}

	private Dialog _loadingDialog = null;

	private void getIntegralList(boolean showDialog, int page) {
		if (_loadingDialog == null) {
			_loadingDialog = DialogUtil.createLoadingDialog(MyIntegralActivity.this,
					getString(R.string.basic_loading));
		}
		if (showDialog) {
			_loadingDialog.show();
		}
		final int page1 = page;
		GetIntegralRecordListTask task = new GetIntegralRecordListTask(page);
		
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {

			@Override
			public void taskFinish(DDResult result) {
				if (result.getError() == RetError.NONE  )
				{
					List<PointBean> tmpList = result.getBundle().getParcelableArrayList("list");
					if( page1 > 1 ) // 加载更多
					{
						_dataList.addAll(tmpList);
						
						_adapter.notifyDataSetChanged();
					}
					else
					{
						// 加载第一页
						_dataList.clear();
						_dataList.addAll(tmpList);
						_adapter.notifyDataSetChanged();
						
						_refreshViewContainer.onHeaderRefreshComplete();
						_refreshViewContainer.setVisibility(View.VISIBLE);
						_refreshAction = RefreshAction.NONE;
						
						if( _loadingDialog != null )
							_loadingDialog.dismiss();
						
					}

					// 是否显示"加载更多"
					if( tmpList.size() > 0 )
					{
						setGetMoreContent("滑动加载更多", true, false);
						_bGetMoreEnable = true;
					}
					else
					{
						if (page1 == 1) {
							_tv_norecord.setText(result.getErrorMessage());
							_tv_norecord.setTag(0);
						}
						// 没数据了，加载完成
						setGetMoreContent("已全部加载", false, false);
						_bGetMoreEnable = false;
					}
					
					// 确保加载成功后，再修改这个变量
					_pageNum = page1;
				}
				else 
				{// 加载失败
					if( page1 > 1 )
					{
						setGetMoreContent("滑动加载更多", true, false);
					}
					else
					{
						_refreshViewContainer.onHeaderRefreshComplete();
						_refreshAction = RefreshAction.NONE;
						
						if( _loadingDialog != null )
							_loadingDialog.dismiss();
					}

					ToastUtil.showToast(result.getErrorMessage());
				}
				
				_refreshAction = RefreshAction.NONE;
			}
		});
		task.executeParallel("");
	}

	protected void findViewById() {
		Button leftBtn = getLeftButtonText(getResources().getString(R.string.basic_back));
		setTitle(getString(R.string.mine_integral));
		leftBtn.setOnClickListener(this);
		
		mtv_show = (JumpTextView) findViewById(R.id.myintegral_tv_show);
		_refreshViewContainer = (DDPullToRefreshView) findViewById(R.id.refreshViewContainer);
		_refreshViewContainer.setOnHeaderRefreshListener(this);
		_refreshViewContainer.setVisibility(View.INVISIBLE);
		
		_listView = (ListView) findViewById(R.id.listView);
		_listView.setOnScrollListener(this);
		_tv_norecord = (TextView) findViewById(R.id.tv_norecord);
		_listView.setEmptyView(_tv_norecord);
		initList();
	}
	
	private void initList() {
		// 获取更多
		_getMoreView = 	createGetMoreView();
		setGetMoreContent("已全部加载", false, false);
		_listView.addFooterView(_getMoreView);

		// 数据
		_adapter = new IntegralListAdapter(this);
		_listView.setAdapter(_adapter);
		_adapter.setData(_dataList);
	}
	
	// 加载更多相关函数 >>>>>>
	boolean _bGetMoreEnable = false;
	private View createGetMoreView()
	{
		if( _getMoreView != null )
			return _getMoreView;
		
		View v = (View) getLayoutInflater().inflate(R.layout.refresh_footer, null);
		
		return v;
	}
	
	private void setGetMoreContent(String message, boolean showImage, boolean animation)
	{
		TextView tv = (TextView)_getMoreView.findViewById(R.id.pull_to_load_text);
		tv.setText(message);
		
		ImageView imgView = (ImageView)_getMoreView.findViewById(R.id.pull_to_load_image);
		AnimationDrawable ad = (AnimationDrawable) imgView.getBackground();
		if( showImage )
		{
			if( animation )
			{
				ad.start();
			}
			else
			{
				ad.stop();
				ad.selectDrawable(0);
			}
				
			imgView.setVisibility(View.VISIBLE);
		}
		else
		{
			ad.stop();
			ad.selectDrawable(0);
	
			imgView.setVisibility(View.GONE);
		}
	}
	
	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		// 工具栏的显示与隐藏		
				if( _refreshAction == RefreshAction.NONE )
				{
					if( _bGetMoreEnable )
					{// 有加载更多
						int lastPos = _listView.getLastVisiblePosition();
						int total = _listView.getHeaderViewsCount() + _dataList.size() + _listView.getFooterViewsCount();
						if( lastPos == total - 1 )
						{
							// 加载更多显示出来了，开始加载
							_refreshAction = RefreshAction.LOADMORE;
							setGetMoreContent("正在加载...", true, true);
							getIntegralList(false, _pageNum + 1);
						}
					}
				}
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onHeaderRefresh(DDPullToRefreshView view) {
		if( _refreshAction == RefreshAction.NONE )
		{
			_refreshAction = RefreshAction.PULLTOREFRESH;
			getIntegralList(false, 1);
		}
		else
		{
			// 正在加载，什么也不做
			view.onHeaderRefreshComplete();
		}
	}
	
	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.btn_left:{
			finishThisActivity();
		}
			break;
		
		default:
			break;
		}
	}
	
}
