package com.ddoctor.user.activity.mine;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.beichen.user.R;
import com.ddoctor.enums.RetError;
import com.ddoctor.user.activity.BaseActivity;
import com.ddoctor.user.component.ClientUpgrade;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.task.AppUpdateTask;
import com.ddoctor.user.task.TaskPostCallBack;
import com.ddoctor.user.wapi.bean.ClientUpdateBean;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.DialogUtil;
import com.ddoctor.utils.DialogUtil.ConfirmDialog;
import com.ddoctor.utils.FileOperationUtil;
import com.ddoctor.utils.FileUtils;
import com.ddoctor.utils.MyUtils;
import com.ddoctor.utils.ToastUtil;
import com.umeng.analytics.MobclickAgent;
import com.zhuge.analysis.stat.ZhugeSDK;

public class MySettingsActivity extends BaseActivity implements
		OnCheckedChangeListener {

	private ToggleButton gps;
	private ToggleButton push;
	private TextView tv_feedback;
	private TextView tv_update;
	private TextView tv_clearCache;
	private TextView tv_aboutus;

	private long mcacheSize = 0;

	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("MySettingsActivity");
		MobclickAgent.onResume(MySettingsActivity.this);
		ZhugeSDK.getInstance().init(getApplicationContext());
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("MySettingsActivity");
		MobclickAgent.onPause(MySettingsActivity.this);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_settings);
		findViewById();
	}

	protected void findViewById() {
		Button leftBtn = getLeftButtonText(getResources().getString(
				R.string.basic_back));
		setTitle(getResources().getString(R.string.mine_settings));
		gps = (ToggleButton) findViewById(R.id.settings_gps);
		push = (ToggleButton) findViewById(R.id.settings_push);
		tv_feedback = (TextView) findViewById(R.id.settings_tv_feedback);
		tv_clearCache = (TextView) findViewById(R.id.settings_tv_cache);
		tv_update = (TextView) findViewById(R.id.settings_tv_version);
		tv_aboutus = (TextView) findViewById(R.id.settings_tv_aboutus);

		tv_update.setText(String.format("v%s", MyUtils.getVersionName(this)));
		mcacheSize = FileOperationUtil.getCacheLongSize();
		String cacheSize = FileUtils.formatFileSize(mcacheSize);
		if (mcacheSize > 0) {
			tv_clearCache.setText(cacheSize);
		}
		leftBtn.setOnClickListener(this);
		tv_feedback.setOnClickListener(this);
		tv_clearCache.setOnClickListener(this);
		tv_update.setOnClickListener(this);
		tv_aboutus.setOnClickListener(this);
		gps.setOnCheckedChangeListener(this);
		push.setOnCheckedChangeListener(this);
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.btn_left: {
			finishThisActivity();
		}
			break;
		case R.id.settings_tv_feedback: {
			skip(UserFeedbackActivity.class, false);
		}
			break;
		case R.id.settings_tv_cache: {
			if (mcacheSize > 0) {
				FileOperationUtil.cleanAppCache(this);
				tv_clearCache.setText("");
			} else {
				ToastUtil.showToast("没有缓存，无需清理");
			}

		}
			break;
		case R.id.settings_tv_version: {
			getAppUpdate();
		}
			break;
		case R.id.settings_tv_aboutus: {
			skip(AboutUsActivity.class, false);
		}
			break;
		default:
			break;
		}
	}

	private Dialog _loadingDialog;

	private void getAppUpdate() {
		_loadingDialog = DialogUtil
				.createLoadingDialog(MySettingsActivity.this);
		_loadingDialog.show();
		AppUpdateTask task = new AppUpdateTask();
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {

			@Override
			public void taskFinish(DDResult result) {
				_loadingDialog.dismiss();
				if (result.getError() == RetError.NONE) {
					ClientUpdateBean updateBean = (ClientUpdateBean) result
							.getBundle().getSerializable("update");
					_clientUpdateBean = updateBean;
					doClientUpdate(updateBean);
				} else {
					ToastUtil.showToast(result.getErrorMessage());
				}
			}
		});
		task.executeParallel("");
	}

	private ClientUpdateBean _clientUpdateBean;

	private void doClientUpdate(ClientUpdateBean updateBean) {
		String okTitle = "立即更新";
		String cancelTitle = "下次再说";
		if (updateBean.getIsMust() != 0) {
			cancelTitle = "";
		}

		final String downloadUrl = updateBean.getUpdateUrl();
		MyUtils.showLog("", " 下载地址   " + downloadUrl);
		final ClientUpdateBean bean = updateBean;
		DialogUtil
				.confirmDialog(this, updateBean.getRemark(), okTitle,
						cancelTitle, new ConfirmDialog() {

							@Override
							public void onOKClick(Bundle data) {
								// TODO: 下载apk包...
								ClientUpgrade cu = new ClientUpgrade(
										MySettingsActivity.this);
								cu.downloadApk(
										downloadUrl,
										new ClientUpgrade.ClientUpgradeCallback() {

											@Override
											public void onSuccess() {
												// TODO Auto-generated method
												// stub
												MySettingsActivity.this
														.finish();
											}

											@Override
											public void onFailed() {
												if (bean.getIsMust() != 0) {
													DialogUtil
															.confirmDialog(
																	MySettingsActivity.this,
																	"下载失败!",
																	"重试",
																	"退出",
																	new ConfirmDialog() {

																		@Override
																		public void onOKClick(
																				Bundle data) {
																			doClientUpdate(_clientUpdateBean);
																		}

																		@Override
																		public void onCancleClick() {

																		}
																	}).show();
												} else {
													DialogUtil
															.confirmDialog(
																	MySettingsActivity.this,
																	"下载失败!",
																	"重试",
																	"取消",
																	new ConfirmDialog() {

																		@Override
																		public void onOKClick(
																				Bundle data) {
																			doClientUpdate(_clientUpdateBean);
																		}

																		@Override
																		public void onCancleClick() {

																		}
																	}).show();

												}

											}

											@Override
											public void onCancel() {
												// 用户取消下载了
												if (bean.getIsMust() != 0) // 必须升级了，取消了，就退出
												{

												} else {

												}

											}
										});
							}

							@Override
							public void onCancleClick() {
								// on_task_finish(false);
							}
						}).setCancelable(false).setTitle("有新版本了").show();
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		switch (buttonView.getId()) {
		case R.id.settings_gps: {
			if (isChecked) {
				DataModule.getInstance().setGPSState(true);
			} else {
				DataModule.getInstance().setGPSState(false);
			}
		}
			break;

		default: {
			if (isChecked) {
				ToastUtil.showToast("开启消息推送");
			} else {
				ToastUtil.showToast("关闭消息推送");
			}
		}
			break;
		}
	}
}
