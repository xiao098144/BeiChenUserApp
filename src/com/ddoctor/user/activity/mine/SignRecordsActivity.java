package com.ddoctor.user.activity.mine;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.user.activity.BaseActivity;
import com.ddoctor.user.view.RecordCalendar;
import com.ddoctor.user.view.RecordCalendar.OnCalendarClickListener;
import com.ddoctor.user.view.RecordCalendar.OnCalendarDateChangedListener;
import com.ddoctor.utils.TimeUtil;
import com.ddoctor.utils.ToastUtil;
import com.umeng.analytics.MobclickAgent;
import com.zhuge.analysis.stat.ZhugeSDK;

/**
 * 签到记录
 * 
 * @author 萧
 * @Date 2015-5-23下午7:45:52
 * @TODO TODO
 */
public class SignRecordsActivity extends BaseActivity implements OnCalendarClickListener, OnCalendarDateChangedListener {

	private TextView tv_right;
	private RecordCalendar recordCalendar;
	private TextView tv_month;
	private ImageView img_last;
	private ImageView img_next;

	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("SignRecordsActivity");
		MobclickAgent.onResume(SignRecordsActivity.this);
		ZhugeSDK.getInstance().init(getApplicationContext());
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("SignRecordsActivity");
		MobclickAgent.onPause(SignRecordsActivity.this);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_signrecords);

		findViewById();
	}

	protected void findViewById() {
		Button leftBtn = (Button) findViewById(R.id.btn_left);
		img_last = (ImageView) findViewById(R.id.signrecord_calendar_last);
		img_next = (ImageView) findViewById(R.id.signrecord_calendar_next);
		tv_month = (TextView) findViewById(R.id.signrecord_calendar_month);
		tv_month.setText(TimeUtil.getInstance().getCurrentMonth()+"月");
		tv_right = (TextView) findViewById(R.id.tv_right);
		recordCalendar = (RecordCalendar) findViewById(R.id.signrecord_calendar);
		recordCalendar.setOnCalendarClickListener(this);
		recordCalendar.setOnCalendarDateChangedListener(this);
		leftBtn.setOnClickListener(this);
		img_last.setOnClickListener(this);
		img_next.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.btn_left: {
			finishThisActivity();
		}
			break;
		case R.id.signrecord_calendar_last: {
			recordCalendar.lastMonth();
		}
			break;
		case R.id.signrecord_calendar_next: {
			recordCalendar.nextMonth();
		}
			break;

		default:
			break;
		}
	}

	@Override
	public void onCalendarClick(int row, int col, String dateFormat) {
		String[] split = dateFormat.split("-");
		int month = Integer.valueOf(split[1]);
		int date = Integer.valueOf(split[2]);

		if (recordCalendar .getCalendarMonth() - month == 1// 跨年跳转
				|| recordCalendar.getCalendarMonth() - month == -11) {
			recordCalendar.lastMonth();

		} else if (month - recordCalendar.getCalendarMonth() == 1 // 跨年跳转
				|| month - recordCalendar.getCalendarMonth() == -11) {
			recordCalendar.nextMonth();
		} else {
			ToastUtil.showToast("点击选择日期  " + dateFormat);
			if (TimeUtil.getInstance().afterToday(dateFormat,getResources().getString(R.string.time_format_10))) {
				ToastUtil.showToast("不能签到未来的日期");
			}else {
				recordCalendar.removeAllBgColor();
				ToastUtil.showToast("点击选择日期  " + dateFormat);
				recordCalendar.addMark(dateFormat);
			}
		}
	}

	
	
	@Override
	public void onCalendarDateChanged(int year, int month) {
		tv_month.setText(month+"月");
	}

}
