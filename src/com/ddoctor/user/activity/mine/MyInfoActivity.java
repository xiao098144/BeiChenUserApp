package com.ddoctor.user.activity.mine;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.enums.RetError;
import com.ddoctor.interfaces.OnClickCallBackListener;
import com.ddoctor.interfaces.OnConnectFragmentActivityListener;
import com.ddoctor.user.activity.BaseFragmentActivity;
import com.ddoctor.user.activity.questionnaire.Survey1FinishedFragment;
import com.ddoctor.user.activity.questionnaire.Survey1Fragment;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.fragment.MyBasicInfoFragment;
import com.ddoctor.user.fragment.MyDetailInfoFragment;
import com.ddoctor.user.task.TaskPostCallBack;
import com.ddoctor.user.task.UpdatePatientInfoTask;
import com.ddoctor.user.wapi.bean.PatientBean;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.ToastUtil;
import com.umeng.analytics.MobclickAgent;
import com.zhuge.analysis.stat.ZhugeSDK;

public class MyInfoActivity extends BaseFragmentActivity implements
		OnCheckedChangeListener, OnClickCallBackListener {

	private Button leftBtn;
	private Button rightBtn;
	private RelativeLayout mlayout_basic;
	private CheckBox mcb_basic;
	private TextView mtv_basic;
	private RelativeLayout mlayout_detail;
	private CheckBox mcb_detail;
	private TextView mtv_detail;
	private RelativeLayout mlayout_ques;
	private CheckBox mcb_ques;
	private TextView mtv_ques;

	private int mcurrentIndex = -1; // 当前项
	private List<Fragment> mfragmentList = new ArrayList<Fragment>();
	private MyBasicInfoFragment mBasicInfoFragment = new MyBasicInfoFragment();
	private MyDetailInfoFragment mDetailInfoFragment = new MyDetailInfoFragment();
	private Survey1Fragment mSurvey1Fragment = new Survey1Fragment();
	private Survey1FinishedFragment mSurvey1FinishedFragment = new Survey1FinishedFragment();

	private OnConnectFragmentActivityListener onConnectFragmentActivityListener;

	public void setOnConnectFragmentActivityListener(
			OnConnectFragmentActivityListener onConnectFragmentActivityListener) {
		this.onConnectFragmentActivityListener = onConnectFragmentActivityListener;
	}

	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("MyInfoActivity");
		MobclickAgent.onResume(MyInfoActivity.this);
		ZhugeSDK.getInstance().init(getApplicationContext());
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("MyInfoActivity");
		MobclickAgent.onPause(MyInfoActivity.this);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_myinfo);
		findViewById();
		patientBean = DataModule.getInstance().getLoginedUserInfo();
		showTab(0);
	}

	PatientBean patientBean;

	public PatientBean getPatient() {
		return patientBean;
	}

	private boolean _isBasicInfoComplete = true;
	private boolean _isDetailInfoComplete = true;
	private boolean _isFinishedSurvey = false;

	protected void findViewById() {
		mfragmentList.add(mBasicInfoFragment);
		mfragmentList.add(mDetailInfoFragment);
		mfragmentList.add(mSurvey1Fragment);
		mfragmentList.add(mSurvey1FinishedFragment);

		leftBtn = getLeftButtonText(getResources().getString(
				R.string.basic_back));
		rightBtn = getRightButtonText(getResources().getString(
				R.string.basic_save));
		setTitle(getResources().getString(R.string.mine_info));
		mlayout_basic = (RelativeLayout) findViewById(R.id.myinfo_frame_basic);
		mcb_basic = (CheckBox) findViewById(R.id.myinfo_btn_basic);
		mtv_basic = (TextView) findViewById(R.id.myinfo_tv_basic);

		mlayout_detail = (RelativeLayout) findViewById(R.id.myinfo_frame_detail);
		mcb_detail = (CheckBox) findViewById(R.id.myinfo_btn_detail);
		mtv_detail = (TextView) findViewById(R.id.myinfo_tv_detail);

		mlayout_ques = (RelativeLayout) findViewById(R.id.myinfo_frame_ques);
		mcb_ques = (CheckBox) findViewById(R.id.myinfo_btn_ques);
		mtv_ques = (TextView) findViewById(R.id.myinfo_tv_ques);

		PatientBean userInfo = DataModule.getInstance().getLoginedUserInfo();
		_isBasicInfoComplete = userInfo==null?false:userInfo.isBasicInfoComplete();
		mcb_basic.setChecked(_isBasicInfoComplete);
		_isDetailInfoComplete = userInfo==null?false:userInfo.isDetailInfoComplete();
		mcb_detail.setChecked(_isDetailInfoComplete);
		_isFinishedSurvey = DataModule.getInstance().getSurvey1State();
		mcb_ques.setChecked(_isFinishedSurvey);
		mcb_basic.setOnCheckedChangeListener(this);
		mcb_detail.setOnCheckedChangeListener(this);
		mcb_ques.setOnCheckedChangeListener(this);

		leftBtn.setOnClickListener(this);
		rightBtn.setOnClickListener(this);

		mlayout_basic.setOnClickListener(this);
		mtv_basic.setOnClickListener(this);
		mlayout_detail.setOnClickListener(this);
		mtv_detail.setOnClickListener(this);
		mlayout_ques.setOnClickListener(this);
		mtv_ques.setOnClickListener(this);
	}

	private void showTab(int tabIndex) {
		if (tabIndex < 0 || tabIndex >= mfragmentList.size())
			return;

		if (mcurrentIndex == tabIndex)
			return;

		if (mcurrentIndex >= 0) {
			mfragmentList.get(mcurrentIndex).onPause();
		}
		if (tabIndex == 2) {
			if (DataModule.getInstance().getSurvey1State()) {
				tabIndex = 3;
			}
		}

		FragmentTransaction ft = this.getSupportFragmentManager()
				.beginTransaction();

		for (int i = 0; i < mfragmentList.size(); i++) {
			Fragment fg = mfragmentList.get(i);
			if (i == tabIndex) {
				if (fg.isAdded()) {
					fg.onResume();
				} else {
					ft.add(R.id.info_layout, fg);
				}
				ft.show(fg);
			} else {
				ft.hide(fg);
			}
		}
		ft.commit();

		mcurrentIndex = tabIndex;

	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.btn_left: {
			finishThisActivity();
		}
			break;
		case R.id.btn_right: {
			Bundle data = new Bundle();
			data.putInt("type", mcurrentIndex);
			onConnectFragmentActivityListener.notifyFragment(data);
		}
			break;
		case R.id.myinfo_tv_basic:
		case R.id.myinfo_frame_basic:
//		case R.id.myinfo_btn_basic: {
			showTab(0);
//		}
			break;
		case R.id.myinfo_frame_detail:
		case R.id.myinfo_tv_detail:
//		case R.id.myinfo_btn_detail: {
			showTab(1);
//		}
			break;
		case R.id.myinfo_frame_ques:
		case R.id.myinfo_tv_ques:
//		case R.id.myinfo_btn_ques: {
			showTab(2);
//		}
			break;

		default:
			break;
		}
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		switch (buttonView.getId()) {
		case R.id.myinfo_btn_basic: {
			mcb_basic.setChecked(_isBasicInfoComplete);
		}
			break;
		case R.id.myinfo_btn_detail: {
			mcb_detail.setChecked(_isDetailInfoComplete);
		}
			break;
		case R.id.myinfo_btn_ques: {
			mcb_ques.setChecked(_isFinishedSurvey);
		}
			break;

		default:
			break;
		}
	}


	/**
	 * 添加自定义事件
	 * @param eventId
	 * @param eventName
	 */
	private void addEvent(String eventId , String eventName){
		MobclickAgent.onEvent(this, eventId);
		ZhugeSDK.getInstance().onEvent(this, eventName);
	}
	
	private void updatePatientInfo(final PatientBean patientBean) {
		addEvent("500001", getString(R.string.event_grxx_500001));
		UpdatePatientInfoTask task = new UpdatePatientInfoTask(patientBean);
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {

			@Override
			public void taskFinish(DDResult result) {
				if (result.getError() == RetError.NONE) {
					ToastUtil.showToast("修改成功");
					DataModule.getInstance().saveLoginedUserInfo(patientBean);
				} else {
					ToastUtil.showToast(result.getErrorMessage());
				}
			}
		});
		task.executeParallel("");
	}

	@Override
	public void onClickCallBack(Bundle data) {

		switch (mcurrentIndex) {
		case 0: {
			PatientBean patientBean = data.getParcelable("data");
			updatePatientInfo(patientBean);
			_isBasicInfoComplete = patientBean.isBasicInfoComplete();
			mcb_basic.setChecked(_isBasicInfoComplete);
		}
			break;
		case 1: {
			PatientBean patientBean = data.getParcelable("data");
			updatePatientInfo(patientBean);
			_isDetailInfoComplete = patientBean.isDetailInfoComplete();
			mcb_detail.setChecked(_isDetailInfoComplete);
		}
			break;
		case 2: {
			_isFinishedSurvey = DataModule.getInstance().getSurvey1State();
			mcb_ques.setChecked(_isFinishedSurvey);
			showTab(3);
		}
			break;
		default:
			break;
		}

	}

}
