package com.ddoctor.user.activity.mine;

import java.util.ArrayList;
import java.util.List;

import android.app.Dialog;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.enums.RecordLayoutType;
import com.ddoctor.enums.RefreshAction;
import com.ddoctor.enums.RetError;
import com.ddoctor.user.activity.BaseActivity;
import com.ddoctor.user.adapter.MyRecordsAdapter;
import com.ddoctor.user.task.DeleteRecordTask;
import com.ddoctor.user.task.GetMyRecordListTask;
import com.ddoctor.user.task.TaskPostCallBack;
import com.ddoctor.user.view.DDPullToRefreshView;
import com.ddoctor.user.view.DDPullToRefreshView.OnHeaderRefreshListener;
import com.ddoctor.user.wapi.bean.RecordBean;
import com.ddoctor.user.wapi.constant.Record;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.DialogUtil;
import com.ddoctor.utils.MyUtils;
import com.ddoctor.utils.TimeUtil;
import com.ddoctor.utils.ToastUtil;
import com.umeng.analytics.MobclickAgent;
import com.zhuge.analysis.stat.ZhugeSDK;

public class MyRecordsActivity extends BaseActivity implements
		OnHeaderRefreshListener, OnScrollListener {

	private ListView _listView;
	DDPullToRefreshView _refreshViewContainer;
	private View _getMoreView;
	private TextView _tv_norecord;

	private int _pageNum = 1;
	private RefreshAction _refreshAction = RefreshAction.PULLTOREFRESH;

	private MyRecordsAdapter _adapter;

	private List<RecordBean> _dataList = new ArrayList<RecordBean>();
	private List<RecordBean> _resultList = new ArrayList<RecordBean>();

	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("MyRecordsActivity");
		MobclickAgent.onResume(MyRecordsActivity.this);
		ZhugeSDK.getInstance().init(getApplicationContext());
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("MyRecordsActivity");
		MobclickAgent.onPause(MyRecordsActivity.this);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_myrecords);
		findViewById();

		loadingData(true, _pageNum);
	}

	protected void findViewById() {
		Button mleftBtn = getLeftButtonText(getResources().getString(
				R.string.basic_back));
		setTitle(getResources().getString(R.string.mine_records));
		mleftBtn.setOnClickListener(this);

		_refreshViewContainer = (DDPullToRefreshView) findViewById(R.id.refreshViewContainer);
		_refreshViewContainer.setOnHeaderRefreshListener(this);
		_refreshViewContainer.setVisibility(View.INVISIBLE);

		_listView = (ListView) findViewById(R.id.listView);
		_listView.setOnScrollListener(this);

		_tv_norecord = (TextView) findViewById(R.id.tv_norecord);
		_tv_norecord.setVisibility(View.INVISIBLE);
		_listView.setEmptyView(_tv_norecord);

		initList();

	}

	private void initList() {
		// 获取更多
		_getMoreView = createGetMoreView();
		setGetMoreContent("已全部加载", false, false);
		_listView.addFooterView(_getMoreView);

		// 数据
		_adapter = new MyRecordsAdapter(this);
		_listView.setAdapter(_adapter);
		_adapter.setData(_dataList);
	}

	// 加载更多相关函数 >>>>>>
	boolean _bGetMoreEnable = false;

	private View createGetMoreView() {
		if (_getMoreView != null)
			return _getMoreView;

		View v = (View) getLayoutInflater().inflate(R.layout.refresh_footer,
				null);

		return v;
	}

	private void setGetMoreContent(String message, boolean showImage,
			boolean animation) {
		TextView tv = (TextView) _getMoreView
				.findViewById(R.id.pull_to_load_text);
		tv.setText(message);

		ImageView imgView = (ImageView) _getMoreView
				.findViewById(R.id.pull_to_load_image);
		AnimationDrawable ad = (AnimationDrawable) imgView.getBackground();
		if (showImage) {
			if (animation) {
				ad.start();
			} else {
				ad.stop();
				ad.selectDrawable(0);
			}

			imgView.setVisibility(View.VISIBLE);
		} else {
			ad.stop();
			ad.selectDrawable(0);

			imgView.setVisibility(View.GONE);
		}
	}

	private Dialog _loadingDialog = null;

	private void loadingData(boolean showLoading, int page) {
		if (showLoading) {
			_loadingDialog = DialogUtil.createLoadingDialog(this, "加载中...");
			_loadingDialog.show();
		}

		GetMyRecordListTask task = new GetMyRecordListTask(page);

		final int page1 = page;
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {

			@Override
			public void taskFinish(DDResult result) {
				if (result.getError() == RetError.NONE) {
					List<RecordBean> tmpList = result.getBundle()
							.getParcelableArrayList("list");
					MyUtils.showLog(" MyRecordActivity  "+tmpList.toString());
					if (page1 > 1) // 加载更多
					{
						_resultList.addAll(tmpList);

						_dataList.addAll(formatList(tmpList));

						_adapter.notifyDataSetChanged();
					} else {
						// 加载第一页
						_resultList.clear();
						_resultList.addAll(tmpList);
						_dataList.clear();
						_dataList.addAll(formatList(tmpList));
						_adapter.notifyDataSetChanged();

						_refreshViewContainer.onHeaderRefreshComplete();
						_refreshViewContainer.setVisibility(View.VISIBLE);
						_refreshAction = RefreshAction.NONE;

						if (_loadingDialog != null)
							_loadingDialog.dismiss();

					}
					// 是否显示"加载更多"
					if (tmpList.size() > 0) {
						setGetMoreContent("滑动加载更多", true, false);
						_bGetMoreEnable = true;
					} else {
						if (page1 == 1) {
							_tv_norecord.setVisibility(View.VISIBLE);
							_tv_norecord.setText(result.getErrorMessage());
							_tv_norecord.setTag(0);
						}
						// 没数据了，加载完成
						setGetMoreContent("已全部加载", false, false);
						_bGetMoreEnable = false;
					}

					// 确保加载成功后，再修改这个变量
					_pageNum = page1;
				} else {// 加载失败
					if (page1 > 1) {
						setGetMoreContent("滑动加载更多", true, false);
					} else {
						_refreshViewContainer.onHeaderRefreshComplete();
						_refreshAction = RefreshAction.NONE;

						if (_loadingDialog != null)
							_loadingDialog.dismiss();
					}

					ToastUtil.showToast(result.getErrorMessage());
				}

				_refreshAction = RefreshAction.NONE;
			}
		});

		task.executeParallel("");
	}

	private boolean isDeleteLastPosition;

	protected List<RecordBean> formatList(List<RecordBean> list) {
		List<RecordBean> resultList = new ArrayList<RecordBean>();
		for (int i = 0; i < list.size(); i++) {
			RecordBean proteinBean = new RecordBean();
			list.get(i).setDate(
					TimeUtil.getInstance().formatDate2(list.get(i).getTime()));
			if (i == 0
					|| (i >= 1 && !list.get(i).getDate()
							.equals(list.get(i - 1).getDate()))) {
				proteinBean.setDate(list.get(i).getDate());
				proteinBean.setLayoutType(RecordLayoutType.TYPE_CATEGORY);
				resultList.add(proteinBean);
			}
			resultList.add(list.get(i));
		}
		return resultList;
	}

	private void deleteRecord(final int position) {
		if (_loadingDialog == null) {
			_loadingDialog = DialogUtil.createLoadingDialog(this,
					"正在加载中，请稍候...");
		}
		if (_loadingDialog != null) {
			_loadingDialog.show();
		}
		int recordId = 0;

		if (RecordLayoutType.TYPE_CATEGORY == _dataList.get(position - 1)
				.getLayoutType()) {
			RecordBean recordBean = null;
			try {
				recordBean = _dataList.get(position + 1);
			} catch (Exception e) {
				recordBean = null;
			} finally {
				recordId = _dataList.get(position).getId();
				if (null == recordBean
						|| RecordLayoutType.TYPE_CATEGORY == recordBean
								.getLayoutType()) {
					isDeleteLastPosition = true;
				}
			}
		} else {
			recordId = _dataList.get(position).getId();
		}

		DeleteRecordTask task = new DeleteRecordTask(recordId,
				Record.SUGAR_VALUE);
		task.setTaskCallBack(new TaskPostCallBack<RetError>() {

			@Override
			public void taskFinish(RetError result) {
				if (result == RetError.NONE) {
					_dataList.remove(position);
					if (isDeleteLastPosition) {
						_dataList.remove(position - 1);
						isDeleteLastPosition = false;
					}
					_loadingDialog.dismiss();
				} else {
					_loadingDialog.dismiss();
					ToastUtil.showToast(result.getErrorMessage());
				}
				_adapter.notifyDataSetChanged();
			}
		});
		task.executeParallel("");
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.btn_left: {
			finishThisActivity();
		}
			break;

		default:
			break;
		}
	}

	// @Override
	// public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long
	// arg3) {
	// int dataIdx = arg2 - _listView.getHeaderViewsCount();
	// if (dataIdx >= _dataList.size()) {
	// return;
	// }
	// // ToastUtil.showToast(_dataList.get(dataIdx).toString());
	// MyUtils.showLog("",
	// "点击  " + dataIdx + " " + _dataList.get(dataIdx).toString()
	// + " " + _resultList.get(dataIdx).toString());
	// }

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		// 工具栏的显示与隐藏
		if (_refreshAction == RefreshAction.NONE) {
			if (_bGetMoreEnable) {// 有加载更多
				int lastPos = _listView.getLastVisiblePosition();
				int total = _listView.getHeaderViewsCount() + _dataList.size()
						+ _listView.getFooterViewsCount();
				if (lastPos == total - 1) {
					// 加载更多显示出来了，开始加载
					_refreshAction = RefreshAction.LOADMORE;
					setGetMoreContent("正在加载...", true, true);
					loadingData(false, _pageNum + 1);
				}
			}
		}
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onHeaderRefresh(DDPullToRefreshView view) {
		if (_refreshAction == RefreshAction.NONE) {
			_refreshAction = RefreshAction.PULLTOREFRESH;
			loadingData(false, 1);
		} else {
			// 正在加载，什么也不做
			view.onHeaderRefreshComplete();
		}
	}
}
