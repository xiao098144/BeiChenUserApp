package com.ddoctor.user.activity.mine;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.enums.RetError;
import com.ddoctor.user.activity.BaseActivity;
import com.ddoctor.user.task.GetAppAboutInfoTask;
import com.ddoctor.user.task.TaskPostCallBack;
import com.ddoctor.user.wapi.bean.AboutBean;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.MyUtils;
import com.ddoctor.utils.ToastUtil;
import com.umeng.analytics.MobclickAgent;
import com.zhuge.analysis.stat.ZhugeSDK;

/**
 * 关于我们
 * 
 * @author 萧
 * @Date 2015-6-25上午10:58:11
 * @TODO TODO
 */
public class AboutUsActivity extends BaseActivity {

	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("AboutUsActivity");
		MobclickAgent.onResume(AboutUsActivity.this);
		ZhugeSDK.getInstance().init(getApplicationContext());
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("AboutUsActivity");
		MobclickAgent.onPause(AboutUsActivity.this);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_aboutus);
		getAppAboutInfo();
		findViewById();
	}

	public void findViewById() {
		Button leftBtn = getLeftButton();
		leftBtn.setText(getString(R.string.basic_back));
		setTitle(getString(R.string.mine_setting_aboutus));
		leftBtn.setOnClickListener(this);
		Button btn_service = (Button) findViewById(R.id.aboutus_btn_service);
		btn_service.setOnClickListener(this);
		
		TextView tv_version = (TextView) findViewById(R.id.aboutus_tv_version);
		tv_version.setText(String.format("糖医生V%s", MyUtils.getVersionName(this)));

	}

	private String _serviceNum;

	private void getAppAboutInfo() {
		GetAppAboutInfoTask task = new GetAppAboutInfoTask();
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {

			@Override
			public void taskFinish(DDResult result) {
				if (result.getError() == RetError.NONE) {
					AboutBean aboutBean = (AboutBean) result.getBundle()
							.getSerializable("about");
					_serviceNum = aboutBean.getService();
				} else {
					ToastUtil.showToast(result.getErrorMessage());
				}
			}
		});
		task.executeParallel("");
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.btn_left: {
			finishThisActivity();
		}
			break;
		case R.id.aboutus_btn_service: {
			if (TextUtils.isEmpty(_serviceNum)) {
				_serviceNum = "01059454123";
			}
			Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:"
					+ _serviceNum));
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
		}
			break;

		default:
			break;
		}
	}

}
