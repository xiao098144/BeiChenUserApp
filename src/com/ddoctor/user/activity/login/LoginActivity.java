package com.ddoctor.user.activity.login;

import org.json.JSONObject;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.beichen.user.R;
import com.beichen.user.wxapi.WXEntryActivity;
import com.ddoctor.application.MyApplication;
import com.ddoctor.enums.RetError;
import com.ddoctor.user.activity.BaseActivity;
import com.ddoctor.user.activity.MainTabActivity;
import com.ddoctor.user.activity.regist.BindPhoneActivity;
import com.ddoctor.user.activity.regist.FindBackPwdStep1Activity;
import com.ddoctor.user.activity.regist.RegistPhoneActivity;
import com.ddoctor.user.config.AppBuildConfig;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.service.PollingService;
import com.ddoctor.user.task.LoginTask;
import com.ddoctor.user.task.QuickLoginTask;
import com.ddoctor.user.task.TaskPostCallBack;
import com.ddoctor.user.wapi.bean.PatientBean;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.DialogUtil;
import com.ddoctor.utils.StringUtils;
import com.ddoctor.utils.ToastUtil;
import com.tencent.mm.sdk.modelmsg.SendAuth;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.Tencent;
import com.tencent.tauth.UiError;
import com.testin.agent.TestinAgent;
import com.umeng.analytics.MobclickAgent;
import com.zhuge.analysis.stat.ZhugeSDK;

public class LoginActivity extends BaseActivity {
	Button login_btn;

	private Dialog _dialog = null;

	private EditText _editUsername = null;
	private EditText _editPassword = null;

	private TextView _tv_newpwd;

	private int quickLoginType;

	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("LoginActivity");
		MobclickAgent.onResume(LoginActivity.this);
		ZhugeSDK.getInstance().init(getApplicationContext());
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("LoginActivity");
		MobclickAgent.onPause(LoginActivity.this);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		login_btn = (Button) findViewById(R.id.login_btn);
		login_btn.setOnClickListener(this);
		Button login_register = (Button) findViewById(R.id.login_register);
		login_register.setOnClickListener(this);
		
//		ImageButton ibtn_qq = (ImageButton) findViewById(R.id.ibtn_qq);
//		ImageButton ibtn_weixin = (ImageButton) findViewById(R.id.ibtn_weixin);
//		ibtn_qq.setOnClickListener(this);
//		ibtn_weixin.setOnClickListener(this);
		
		_editUsername = (EditText) findViewById(R.id.edittext_username);
		_editPassword = (EditText) findViewById(R.id.edittext_password);
		_tv_newpwd = (TextView) findViewById(R.id.login_tv_newpwd);

		_tv_newpwd.setOnClickListener(this);

//		// for test
//		_editUsername.setText("13910388910");
//		_editPassword.setText("123456");
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.login_btn: {
			on_btn_login();

		}
			break;
		case R.id.login_register: {
			on_btn_register();

		}
			break;
		case R.id.login_tv_newpwd: {
			skip(FindBackPwdStep1Activity.class, false);
		}
			break;
		case R.id.ibtn_qq: {
			quickLoginType = 2;

			if (MyApplication.tencent == null) {
				MyApplication.tencent = Tencent.createInstance(
						AppBuildConfig.TencentKey, getApplicationContext());
			}
			if (!MyApplication.tencent.isSessionValid()) {
				MyApplication.tencent.login(CTX, "all", new BaseIuiListener());
			} else {
				quicklogin(DataModule.getInstance().getOpenId(),
						quickLoginType);
			}
		}
			break;
		case R.id.ibtn_weixin: {
			if (MyApplication.mwxAPI == null) {
				MyApplication.mwxAPI = WXAPIFactory.createWXAPI(this,
						AppBuildConfig.WXAPP_ID, true);
				MyApplication.mwxAPI.registerApp(AppBuildConfig.WXAPP_ID);
			}
			if (MyApplication.mwxAPI.isWXAppInstalled()) {
				quickLoginType = 1;
				WXEntryActivity.currentAction = WXEntryActivity.WX_QUICKLOGIN;
				DataModule.getInstance().registerInfoMap.put("bindphone", LoginActivity.this);
				SendAuth.Req req = new SendAuth.Req();
				req.scope = "snsapi_userinfo";
				req.state = "com.ddoctor.user";
				MyApplication.mwxAPI.sendReq(req);
				// }
			} else {
				ToastUtil.showToast(" 请先安装微信 ");
			}
		}
			break;
		default:
			break;
		}
	}

	private void startService() {
		Intent intent = new Intent();
		intent.setClass(getApplicationContext(), PollingService.class);
		startService(intent);
	}

	// 点击登录按钮
	private void on_btn_login() {
		// 检查输入的字段值，不完整提示并返回
		String mobile = _editUsername.getText().toString().trim();
		if (mobile.length() < 1) {
			ToastUtil.showToast("请输入手机号!");
			return;
		}
		if (!StringUtils.checkPhoneNumber(mobile)) {
			ToastUtil.showToast("请输入格式正确的手机号!");
			return;
		}

		String passwd = _editPassword.getText().toString().trim();
		if (passwd.length() < 1) {
			ToastUtil.showToast("请输入密码!");
			return;
		}
		// 字段值输入正确，开始登录
		// 先显示loading....
		_dialog = DialogUtil.createLoadingDialog(this, "正在登录...");
		_dialog.show();

		LoginTask task = new LoginTask(mobile, passwd);
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {
			@Override
			public void taskFinish(DDResult result) {
				// 登录成功，进入主界面
				if (result.getError() == RetError.NONE) {
					PatientBean resp = (PatientBean) result.getObject();
					_dialog.dismiss();
					TestinAgent.setUserInfo(resp.getId()+"");
					DataModule.getInstance().saveLoginedUserInfo(resp);
					startService();
					// 进入主界面
					skip(MainTabActivity.class, true);
				} else {
					// 登录失败
					_dialog.dismiss();
					ToastUtil.showToast(result.getErrorMessage());
				}
			}
		});
		task.executeParallel("");
	}

	// 点击注册按钮
	private void on_btn_register() {
		// 保存这个页面，后面如果注册成功，需要关掉这个页面
		DataModule.getInstance().registerInfoMap.clear();
		DataModule.getInstance().registerInfoMap.put("login_activity", this);

		skip(RegistPhoneActivity.class, false);
	}

	private class BaseIuiListener implements IUiListener {

		@Override
		public void onCancel() {
			ToastUtil.showToast(" 取消登录 ");
		}

		@Override
		public void onComplete(Object arg0) {
			doComplete((JSONObject) arg0);
		}

		@Override
		public void onError(UiError e) {
			ToastUtil.showToast(e.errorMessage);
		}

		public void doComplete(JSONObject jsonObject) {
			String expires_in = jsonObject.optString("expires_in");
			if ("".equals(expires_in)) {
				expires_in = "0";
			}
			long expires = System.currentTimeMillis()
					+ Long.parseLong(expires_in) * 1000;
			DataModule.getInstance().saveTencentQuickLoginInfo(jsonObject.optString("openid") ,expires , jsonObject.optString("access_token"));
			quicklogin(jsonObject.optString("openid"), quickLoginType);
		}

	}

	private void quicklogin(String quickId, int type) {

		_dialog = DialogUtil.createLoadingDialog(this, "正在登录...");
		_dialog.show();

		QuickLoginTask task = new QuickLoginTask(quickId, type);
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {
			@Override
			public void taskFinish(DDResult result) {
				_dialog.dismiss();
				if (result.getError() == RetError.NONE) {
					// 进入主界面
					if (!TextUtils.isEmpty(DataModule.getInstance()
							.getLoginedUserInfo().getMobile())) {
						ToastUtil.showToast("快捷登录成功");
						skip(MainTabActivity.class, true);
					} else {
						DataModule.getInstance().registerInfoMap.put("bindphone", LoginActivity.this);
						skip(BindPhoneActivity.class, false);
					}
				} else {
					// 登录失败
					ToastUtil.showToast(result.getErrorMessage());
				}
			}
		});
		task.executeParallel("");
	}
}
