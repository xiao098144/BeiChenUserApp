package com.ddoctor.user.activity.sport;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.location.LocationClientOption.LocationMode;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BaiduMap.SnapshotReadyCallback;
import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.MapStatus;
import com.baidu.mapapi.map.MapStatusUpdate;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.Marker;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.map.OverlayOptions;
import com.baidu.mapapi.map.Polyline;
import com.baidu.mapapi.map.PolylineOptions;
import com.baidu.mapapi.model.LatLng;
import com.beichen.user.R;
import com.beichen.user.wxapi.WXEntryActivity;
import com.ddoctor.application.MyApplication;
import com.ddoctor.enums.RetError;
import com.ddoctor.user.activity.BaseActivity;
import com.ddoctor.user.config.AppBuildConfig;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.task.AddSportRecordTask;
import com.ddoctor.user.task.TaskPostCallBack;
import com.ddoctor.user.view.RoundProgressBar;
import com.ddoctor.user.wapi.bean.SportBean;
import com.ddoctor.utils.FilePathUtil;
import com.ddoctor.utils.MyUtils;
import com.ddoctor.utils.SportRemindDBUtil;
import com.ddoctor.utils.StringUtils;
import com.ddoctor.utils.TimeUtil;
import com.ddoctor.utils.ToastUtil;
import com.pedometer.PedometerSettings;
import com.pedometer.StepService;
import com.tencent.connect.share.QQShare;
import com.tencent.connect.share.QzoneShare;
import com.tencent.mm.sdk.modelmsg.SendMessageToWX;
import com.tencent.mm.sdk.modelmsg.WXImageObject;
import com.tencent.mm.sdk.modelmsg.WXMediaMessage;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.Tencent;
import com.tencent.tauth.UiError;
import com.umeng.analytics.MobclickAgent;
import com.zhuge.analysis.stat.ZhugeSDK;

public class SportActivity extends BaseActivity {

	private LinearLayout linear_step;

	private RoundProgressBar mRoundProgressBar;

	private ImageButton sport_walk, sport_run, sport_prepare, sport_stop;
	private LinearLayout _layout_setting, _layout_record;
	private TextView tv_calory, tv_distance, tv_time;

	private ImageButton map_all_screen, sport_share;

	private MapView mMapView = null;
	private BaiduMap mBaiduMap;
	/** 定位点的集合 */
	public static ArrayList<LatLng> points = new ArrayList<LatLng>();
	private Polyline mMarkerPolyLine;
	private Marker mMarkerA;
	/** 标记图片 */
	private BitmapDescriptor bdA;
	private LocationClient mLocationClient;
	public BDLocationListener myListener = new MyLocationListener();
	private double latitude, longitude;
	private PolylineOptions ooPolyline;

	private boolean bloodMap = false;
	private int index = 0;

	private int mStepValue;
	private int mLastStepValue;
	private int mPaceValue;
	private int mLastPaceValue;
	private float mDistanceValue;
	private float mLastDistanceValue;
	private int mCaloriesValue;
	private int mLastCaloriesValue;

	/**
	 * True, when service is running.
	 */
	private boolean mIsRunning;

	private int mlastSecond = 0;
	private int mlastMinute = 0;
	private long duration = 0;
	private long mlastDuration = 0;
	private long mtotalDuration = 0;

	private final static int SETLOCATION = 1;
	private final static int DRAWLINEINMAP = 2;
	private static final int STEPS_MSG = 3;
	private static final int PACE_MSG = 4;
	private static final int DISTANCE_MSG = 5;
	private static final int CALORIES_MSG = 6;
	private static final int MOVE_DURATION_MSG = 7;

	private final static long MINUTE = 1000 * 60;
	private final static long SECOND = 1000;

	private final static int defaultStepGoals = 5000;

	private SportBean sportBean;

	private int sportType;

	@SuppressLint("HandlerLeak")
	public Handler mHandler = new Handler() {

		public void handleMessage(Message msg) {

			switch (msg.what) {

			case SETLOCATION:// 地图
				// 设定中心点坐标
				LatLng cenpt = new LatLng(latitude, longitude);
				mMapStatus = new MapStatus.Builder().target(cenpt).build();
				// 定义MapStatusUpdate对象，以便描述地图状态将要发生的变化
				MapStatusUpdate mMapStatusUpdate = MapStatusUpdateFactory
						.newMapStatus(mMapStatus);
				mBaiduMap.setMapStatus(mMapStatusUpdate);
				if (index == 1) {
					ooA = new MarkerOptions().position(
							new LatLng(latitude, longitude)).icon(bdA);
					mMarkerA = (Marker) (mBaiduMap.addOverlay(ooA));
				} else {
					if (mMarkerA == null) {
						ooA = new MarkerOptions().position(new LatLng(latitude, longitude)).icon(bdA);
						mMarkerA = (Marker) (mBaiduMap.addOverlay(ooA));
					}
					mMarkerA.setPosition(new LatLng(latitude, longitude));
				}
				break;
			case DRAWLINEINMAP:
				drawMapLine();
				break;
			case MOVE_DURATION_MSG: {
				tv_time.setText("" + msg.arg1);
			}
				break;
			case STEPS_MSG:
				mStepValue = (int) msg.arg1;
				if (mStepValue < defaultStepGoals) {
					mRoundProgressBar.setCurrentStepNum(mStepValue);
				} else {
					ToastUtil.showToast("恭喜您的目标已完成，共计完成" + mStepValue + "步");
					unbindStepService();
					stopStepService();
					stopGPS();
				}
				break;
			case PACE_MSG:
				mPaceValue = msg.arg1;
				if (mPaceValue <= 0) {

				} else {

				}
				break;
			case DISTANCE_MSG:
				mDistanceValue = ((float) msg.arg1) / 1000f;
				if (mDistanceValue <= 0) {
					tv_distance.setText("0");
				} else {
					tv_distance.setText(StringUtils.formatStr("%.0f",
							((mDistanceValue + 0.000001f) * 1000)));
				}
				break;
			case CALORIES_MSG:
				mCaloriesValue = msg.arg1;
				if (mCaloriesValue <= 0) {
					tv_calory.setText("0");
				} else {
					tv_calory.setText("" + mCaloriesValue);
				}
				break;

			default:
				break;
			}

		}
	};

	// TODO: unite all into 1 type of message
	private StepService.ICallback mCallback = new StepService.ICallback() {
		public void stepsChanged(int value) {
			mHandler.sendMessage(mHandler.obtainMessage(STEPS_MSG, value, 0));
		}

		public void paceChanged(int value) {
			mHandler.sendMessage(mHandler.obtainMessage(PACE_MSG, value, 0));
		}

		public void distanceChanged(float value) {
			mHandler.sendMessage(mHandler.obtainMessage(DISTANCE_MSG,
					(int) (value * 1000), 0));
		}

		public void caloriesChanged(float value) {
			mHandler.sendMessage(mHandler.obtainMessage(CALORIES_MSG,
					(int) (value), 0));
		}

		@Override
		public void timeChanged(long currentduration) {
			duration = currentduration;
			mtotalDuration = mlastDuration + duration;
			int minute = (int) (mtotalDuration / MINUTE);
			mHandler.obtainMessage(MOVE_DURATION_MSG, minute, 0).sendToTarget();
		}

	};

	int currentStepNum = 0;
	private OverlayOptions ooA;
	private Boolean bl = true;
	private MapStatus mMapStatus;

	private PedometerSettings mPedometerSettings;

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("SportActivity");
		MobclickAgent.onPause(SportActivity.this);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sport);

		String content = SportRemindDBUtil.getInstance().getWalkContent();
		MyUtils.showLog("SportActivity onCreate " + content);
		int goal = defaultStepGoals;
		if (!TextUtils.isEmpty(content)) {
			goal = Integer.valueOf(content.substring(0, content.length() - 1));
		}
		findViewById();
		initMap();
		initStep(goal);

		if (DataModule.getInstance().isGPSEnable()) {
			if (!mLocationClient.isStarted()) {
				startGPS();
			}
		} else {
			// ToastUtil.showToast("");
		}

		mPedometerSettings = new PedometerSettings(
				PreferenceManager.getDefaultSharedPreferences(this));
		mIsRunning = mPedometerSettings.isServiceRunning();
		mPedometerSettings.clearServiceRunning();

	};

	private StepService mService;

	private ServiceConnection mConnection = new ServiceConnection() {
		public void onServiceConnected(ComponentName className, IBinder service) {
			mService = ((StepService.StepBinder) service).getService();
			mService.registerCallback(mCallback);
			mService.reloadSettings();
		}

		public void onServiceDisconnected(ComponentName className) {
			mService = null;
		}
	};

	private String TAG = "SPORT";

	private void startStepService() {
		if (!mIsRunning) {
			MyUtils.showLog(TAG, "[SERVICE] Start");
			mIsRunning = true;
			startService(new Intent(SportActivity.this, StepService.class));
		}
	}

	private void bindStepService() {
		MyUtils.showLog(TAG, "[SERVICE] Bind");
		Intent intent = new Intent(SportActivity.this, StepService.class);
		intent.putExtra("duration", mlastDuration);
		bindService(intent, mConnection, Context.BIND_AUTO_CREATE
				+ Context.BIND_DEBUG_UNBIND);
	}

	private void unbindStepService() {
		MyUtils.showLog(TAG, "[SERVICE] Unbind");
		mlastDuration = mtotalDuration;
		mLastCaloriesValue = mCaloriesValue;
		mLastDistanceValue = mDistanceValue;
		mLastPaceValue = mPaceValue;
		mLastStepValue = mStepValue;
		unbindService(mConnection);
	}

	private void stopStepService() {
		MyUtils.showLog(TAG, "[SERVICE] Stop");
		if (mService != null) {
			MyUtils.showLog(TAG, "[SERVICE] stopService");
			stopService(new Intent(SportActivity.this, StepService.class));
		}
		mIsRunning = false;
	}

	private void checkData() {
		sportBean = new SportBean();
		sportBean.setDate(TimeUtil.getInstance().getStandardDate(
				getString(R.string.time_format_19)));
		sportBean.setTime(TimeUtil.getInstance().long2DateStr(
				mtotalDuration - mlastDuration, "mm:ss"));
		sportBean.setType(sportType);
		sportBean.setDistance((int) (mDistanceValue * 1000));
		sportBean.setKcal(mCaloriesValue);
		sportBean.setStep(mStepValue);
	}

	private void addSportRecord() {
		AddSportRecordTask task = new AddSportRecordTask(sportBean);
		task.setTaskCallBack(new TaskPostCallBack<RetError>() {

			@Override
			public void taskFinish(RetError result) {
				if (result == RetError.NONE) {
					ToastUtil.showToast("记录保存成功");
					resetValues();
				} else {
					ToastUtil.showToast(result.getErrorMessage());
				}
			}
		});
		task.executeParallel("");
	}

	/**
	 * 重置数据
	 */
	private void resetValues() {
		resetStep();
		resetMap();
	}

	/**
	 * 重置运动记录 清零
	 */
	private void resetStep() {
		mRoundProgressBar.setCurrentStepNum(0);

		mlastDuration = 0;
		mlastMinute = 0;
		mlastSecond = 0;
		mtotalDuration = 0;
		mlastDuration = 0;

		mCaloriesValue = 0;
		mStepValue = 0;
		mPaceValue = 0;
		mDistanceValue = 0;

		mLastCaloriesValue = 0;
		mLastDistanceValue = 0;
		mLastPaceValue = 0;
		mLastStepValue = 0;

		mService.resetValues();

	}

	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("SportActivity");
		MobclickAgent.onResume(SportActivity.this);
		ZhugeSDK.getInstance().init(getApplicationContext());
		if (DataModule.getInstance().isStepGoalUpdated()) {
			String content = SportRemindDBUtil.getInstance().getWalkContent();
			MyUtils.showLog("SportActivity onCreate " + content);
			int goal = defaultStepGoals;
			if (!TextUtils.isEmpty(content)) {
				goal = Integer.valueOf(content.substring(0,
						content.length() - 1));
			}
			initStep(goal);
			DataModule.getInstance().setStepGoalUpdated(false);
		}
	}

	/**
	 * 清空地图轨迹
	 */
	private void resetMap() {
		points.clear();
		index = 0;
		clearLine();
		bloodMap = true;
	}

	public void onClick(View v) {
		switch (v.getId()) {

		case R.id.btn_left: {
			finishThisActivity();
		}
			break;
		case R.id.map_all_screen: {
			if (bl) {
				linear_step.setVisibility(View.GONE);
				bl = false;
			} else {
				linear_step.setVisibility(View.VISIBLE);
				bl = true;
			}
		}
			break;
		case R.id.sport_share: {
			filePath = new File(FilePathUtil.getImageCachePath(),
					System.currentTimeMillis() + ".png");
			// if (takeScreenShot()) {
			if (takeBaiduMapSnapShot()) {
				showShareOptionDialog();
			} else {
				ToastUtil.showToast("截图失败，请重试");
			}

		}
			break;
		case R.id.sport_walk: {
			sportType = 1;
			sport_walk.setVisibility(View.INVISIBLE);
			sport_run.setVisibility(View.VISIBLE);
		}
			break;
		case R.id.sport_run: {
			sportType = 2;
			sport_walk.setVisibility(View.VISIBLE);
			sport_run.setVisibility(View.INVISIBLE);
		}
			break;
		case R.id.sport_prepare: {
			if (mLocationClient != null && !mLocationClient.isStarted()) {
				startGPS();
			}
			if (isfirst) {
				startMap();
			}
			if (!mIsRunning && mPedometerSettings.isNewStart()) {
				startStepService();
				bindStepService();
			} else if (mIsRunning) {
				bindStepService();
			}
			sport_prepare.setVisibility(View.INVISIBLE);
			sport_stop.setVisibility(View.VISIBLE);
		}
			break;
		case R.id.sport_stop: {
			checkData();
			addSportRecord();
			unbindStepService();
			stopStepService();
			if (mLocationClient != null && mLocationClient.isStarted()) {
				stopGPS();
			}
			sport_prepare.setVisibility(View.VISIBLE);
			sport_stop.setVisibility(View.INVISIBLE);
		}
			break;
		case R.id.sport_setting: {
			if (mIsRunning) {
				ToastUtil.showToast("计步器正在运行中，请先停止计步器");
			} else {
				skip(SportSettingActivity.class, false);
			}
		}
			break;
		case R.id.sport_record: {
			if (mIsRunning) {
				ToastUtil.showToast("计步器正在运行中，请先停止计步器");
			} else {
				skip(SportRecordActivity.class, false);
			}
		}
			break;
		default:
			break;
		}

	}

	private void initStep(int goal) {
		MyUtils.showLog("initStep goal " + goal);
		mRoundProgressBar.setGoal(goal);
		mRoundProgressBar.setCurrentStepNum(0);
	}

	private boolean isfirst = true;

	private File filePath;

	/** 开始地图定位 ，并画出运动轨迹 */
	private void startMap() {
		points.clear();
		index = 0;
		clearLine();
		bloodMap = true;
		isfirst = false;
	}
	
	boolean falg = true;
	private boolean takeBaiduMapSnapShot() {
		mBaiduMap.snapshot(new SnapshotReadyCallback() {

			@Override
			public void onSnapshotReady(Bitmap arg0) {
				if (filePath==null) {
					filePath = new File(FilePathUtil.getImageCachePath(), System.currentTimeMillis()+".png");
				}
				FileOutputStream fos = null;
				try {
					fos = new FileOutputStream(filePath.getAbsolutePath());
					if (null != fos) {
						arg0.compress(Bitmap.CompressFormat.PNG, 90, fos);
						fos.flush();
						fos.close();
					}
				} catch (FileNotFoundException e) {
					e.printStackTrace();
					falg = false;
				} catch (IOException e) {
					e.printStackTrace();
					falg = false;
				}
			}
		});
		return falg;
	}

	private boolean takeScreenShot() {

		// View是你需要截图的View
		View view = getWindow().getDecorView();
		view.setDrawingCacheEnabled(true);
		view.buildDrawingCache();
		Bitmap b1 = view.getDrawingCache();

		// 获取状态栏高度
		Rect frame = new Rect();
		getWindow().getDecorView().getWindowVisibleDisplayFrame(frame);
		int statusBarHeight = frame.top;

		// 获取屏幕长和高
		int width = MyUtils.getScreenWidth(SportActivity.this);
		int height = MyUtils.getScreenHeight(SportActivity.this);
		// 去掉标题栏
		Bitmap b = Bitmap.createBitmap(b1, 0, statusBarHeight, width, height
				- statusBarHeight);
		view.destroyDrawingCache();
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(filePath.getAbsolutePath());
			if (null != fos) {
				b.compress(Bitmap.CompressFormat.PNG, 90, fos);
				fos.flush();
				fos.close();
			}
		} catch (FileNotFoundException e) {

		} catch (IOException e) {

		}
		return filePath.exists();
	}

	/** 初始化地图与gps */
	private void initMap() {
		bdA = BitmapDescriptorFactory.fromResource(R.drawable.gps_point1);
		mMapView.removeViewAt(1);
		mMapView.showZoomControls(false);
		mMapView.showScaleControl(false);
		mBaiduMap = mMapView.getMap();
		// 普通地图
		mBaiduMap.setMapType(BaiduMap.MAP_TYPE_NORMAL);
		// 开启定位图层
		mBaiduMap.setMyLocationEnabled(true);

		mLocationClient = MyApplication.mLocationClient;
		mLocationClient.registerLocationListener(myListener);

		ooPolyline = new PolylineOptions();

		LocationClientOption option = new LocationClientOption();
		option.setLocationMode(LocationMode.Hight_Accuracy);// 设置定位模式
		option.setCoorType("bd09ll");// 返回的定位结果是百度经纬度，默认值gcj02
		option.setScanSpan(5000);// 设置发起定位请求的间隔时间为5000ms
		option.setIsNeedAddress(true);// 返回的定位结果包含地址信息
		option.setNeedDeviceDirect(true);// 返回的定位结果包含手机机头的方向
		mLocationClient.setLocOption(option);
		mMapStatus = new MapStatus.Builder().zoom(18).build();
		// 定义MapStatusUpdate对象，以便描述地图状态将要发生的变化
		MapStatusUpdate mMapStatusUpdate = MapStatusUpdateFactory
				.newMapStatus(mMapStatus);
		mBaiduMap.setMapStatus(mMapStatusUpdate);
	}

	/** 实现实位回调监听 得到dps坐标 */
	public class MyLocationListener implements BDLocationListener {

		@Override
		public void onReceiveLocation(BDLocation location) {

			index++;
			Message message = new Message();
			if (bloodMap) {
				latitude = location.getLatitude();
				longitude = location.getLongitude();
				LatLng p1 = new LatLng(latitude, longitude);
				points.add(p1);
				if (points.size() > 1) {
					message.what = DRAWLINEINMAP;
					mHandler.sendMessage(message);
				}

			} else {
				latitude = location.getLatitude();
				longitude = location.getLongitude();
				message.what = SETLOCATION;
				mHandler.sendMessage(message);
			}
		}
	}

	/*** 划线 */
	private void drawMapLine() {
		ooPolyline.width(10).color(0xAAFF0000).points(points);
		mMarkerPolyLine = (Polyline) mBaiduMap.addOverlay(ooPolyline);
		mMarkerPolyLine.setPoints(points);
		// mMarkerA.setPosition(points.get(points.size() - 1));
		addCustomElementsDemo(points.get(points.size() - 1));
	}

	/** 清除地图标记 */
	private void clearLine() {
		mMapView.getMap().clear();
	}

	private void startGPS() {
		mLocationClient.start();
	}

	private void stopGPS() {
		mLocationClient.stop();
	}

	/** 标记 */
	public void addCustomElementsDemo(LatLng mlatLng) {

		ooA = new MarkerOptions().position(mlatLng).icon(bdA);
		mMarkerA = (Marker) (mBaiduMap.addOverlay(ooA));

		// mMarkerA.setPosition(mlatLng); 需要先实例化Marker
	}

	protected void findViewById() {
		if (MyApplication.mwxAPI == null) {
			MyApplication.mwxAPI = WXAPIFactory.createWXAPI(
					getApplicationContext(), AppBuildConfig.WXAPP_ID, true);
		}
		isWXInstalled = MyApplication.mwxAPI.isWXAppInstalled();

		if (mtencent == null) {
			mtencent = Tencent.createInstance(AppBuildConfig.TencentKey,
					getApplicationContext());
		}

		this.setTitle(TimeUtil.getInstance().getStandardDate("MM/dd"));
		Button leftButton = getLeftButtonText(getResources().getString(
				R.string.basic_back));
		leftButton.setOnClickListener(this);
		RelativeLayout rl = (RelativeLayout) findViewById(R.id.titleBar);
		if (rl != null) {
			rl.setBackgroundColor(getResources().getColor(R.color.green_light));
		}
		mMapView = (MapView) findViewById(R.id.bmapView);
		sport_walk = (ImageButton) findViewById(R.id.sport_walk);
		sport_run = (ImageButton) findViewById(R.id.sport_run);
		sport_prepare = (ImageButton) findViewById(R.id.sport_prepare);
		sport_stop = (ImageButton) findViewById(R.id.sport_stop);
		tv_calory = (TextView) findViewById(R.id.sport_tv_calory);
		tv_distance = (TextView) findViewById(R.id.sport_tv_distance);
		tv_time = (TextView) findViewById(R.id.sport_tv_time);
		_layout_setting = (LinearLayout) findViewById(R.id.sport_setting);
		_layout_record = (LinearLayout) findViewById(R.id.sport_record);
		linear_step = (LinearLayout) findViewById(R.id.linear_step);
		map_all_screen = (ImageButton) findViewById(R.id.map_all_screen);
		sport_share = (ImageButton) findViewById(R.id.sport_share);
		// sport_share.setVisibility(View.GONE);
		mRoundProgressBar = (RoundProgressBar) findViewById(R.id.roundProgressBar);

		map_all_screen.setOnClickListener(this);
		sport_share.setOnClickListener(this);
		sport_walk.setOnClickListener(this);
		sport_run.setOnClickListener(this);
		sport_prepare.setOnClickListener(this);
		sport_stop.setOnClickListener(this);
		_layout_record.setOnClickListener(this);
		_layout_setting.setOnClickListener(this);

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (mLocationClient != null && mLocationClient.isStarted()) {
			stopGPS();
		}
		if (mIsRunning) {
			unbindStepService();
			stopStepService();
		}
	}

	private boolean isWXInstalled = false;

	private Tencent mtencent;
	private Dialog shareDialog;

	private void showShareOptionDialog() {
		if (shareDialog == null) {
			shareDialog = new Dialog(SportActivity.this, R.style.NoTitleDialog);
		}
		View view = getLayoutInflater().inflate(R.layout.layout_share, null);
		TextView tv_qq = (TextView) view.findViewById(R.id.share_tv_tencent);
		TextView tv_qzone = (TextView) view.findViewById(R.id.share_tv_qzone);
		TextView tv_wx = (TextView) view.findViewById(R.id.share_tv_wx);
		TextView tv_wx_friend = (TextView) view
				.findViewById(R.id.share_tv_wxfriend);
		Button btn_cancel = (Button) view.findViewById(R.id.btn_cancel);
		btn_cancel.setText(getString(R.string.basic_cancel));
		ShareClickListener shareClickListener = new ShareClickListener();
		tv_qq.setOnClickListener(shareClickListener);
		tv_qzone.setOnClickListener(shareClickListener);
		tv_qzone.setVisibility(View.GONE);
		tv_wx.setOnClickListener(shareClickListener);
		tv_wx_friend.setOnClickListener(shareClickListener);
		btn_cancel.setOnClickListener(shareClickListener);

		Window window = shareDialog.getWindow();
		window.setGravity(Gravity.BOTTOM);
		android.view.WindowManager.LayoutParams lp = new android.view.WindowManager.LayoutParams();
		lp.width = android.view.WindowManager.LayoutParams.WRAP_CONTENT;
		lp.height = android.view.WindowManager.LayoutParams.WRAP_CONTENT;
		window.setAttributes(lp);
		shareDialog.setCancelable(true);
		shareDialog.setCanceledOnTouchOutside(true);
		shareDialog.setContentView(view);
		shareDialog.show();

	}

	private void dismissShareDialog() {
		if (shareDialog != null && shareDialog.isShowing()) {
			shareDialog.dismiss();
			shareDialog = null;
		}
	}

	class ShareClickListener implements OnClickListener {
		String sharePath = null;

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.share_tv_tencent: {
				dismissShareDialog();
				sharePath = "QQ";
				share2Tencent();
			}
				break;
			case R.id.share_tv_qzone: {
				dismissShareDialog();
				sharePath = "Qzone";
				share2Qzone();
			}
				break;
			case R.id.share_tv_wx: {
				dismissShareDialog();
				sharePath = "WX";
				share2WX(true);
			}
				break;
			case R.id.share_tv_wxfriend: {
				dismissShareDialog();
				sharePath = "WXFriend";
				share2WX(false);
			}
				break;
			case R.id.btn_cancel: {
				dismissShareDialog();
			}
				break;
			default:
				break;
			}
		}
	}

	/**
	 * Timeline 朋友圈 Session 好友
	 * 
	 * @param share2Friend
	 *            true 分享给好友 false 分享到朋友圈
	 */
	private void share2WX(final boolean share2Friend) {
		if (isWXInstalled) {
			WXEntryActivity.currentAction = WXEntryActivity.WX_SHARE;
			WXImageObject imageObject = new WXImageObject();
			imageObject.setImagePath(filePath.getAbsolutePath());
			WXMediaMessage msg = new WXMediaMessage(imageObject);
			msg.title = getString(R.string.app_name);
			try {
				File file = new File(filePath.getAbsolutePath());
				if (null != file && file.exists() && file.isFile()) {
					Bitmap bmp = BitmapFactory.decodeFile(file
							.getAbsolutePath());
					Bitmap thumbBmp = Bitmap.createScaledBitmap(bmp, 150, 150,
							true);
					bmp.recycle();
					msg.setThumbImage(thumbBmp);
				}
			} catch (Exception e) {
				MyUtils.showLog("图片异常，无法分享图片 " + e.getMessage());
			}
			SendMessageToWX.Req req = new SendMessageToWX.Req();
			req.transaction = buildTransaction("image");
			req.message = msg;
			req.scene = share2Friend ? SendMessageToWX.Req.WXSceneSession
					: SendMessageToWX.Req.WXSceneTimeline;
			MyUtils.showLog("准备发送分享请求  " + req.toString());
			// DataModule.getInstance().mwxAPI.sendReq(req);
			MyApplication.mwxAPI.sendReq(req);
		} else {
			ToastUtil.showToast("请先安装微信");
		}
	}

	private String buildTransaction(final String type) {
		return (type == null) ? String.valueOf(System.currentTimeMillis())
				: type + System.currentTimeMillis();
	}
	private void share2Qzone() {
		final Bundle params = new Bundle();
		params.putInt(QzoneShare.SHARE_TO_QZONE_KEY_TYPE,
				QzoneShare.SHARE_TO_QZONE_TYPE_IMAGE_TEXT);
		params.putString(QzoneShare.SHARE_TO_QQ_TITLE,
				getString(R.string.app_name));
		params.putString(QzoneShare.SHARE_TO_QQ_TARGET_URL , "http://www.ddoctor.cn");
//		params.putString(QzoneShare.SHARE_TO_QQ_SUMMARY,"糖医生运动轨迹分享");
		ArrayList<String> imgList = new ArrayList<String>();
		imgList.add(filePath.getAbsolutePath());
//		params.putStringArrayList(QzoneShare.SHARE_TO_QQ_IMAGE_LOCAL_URL,
//				imgList);
//		params.putString(QzoneShare.SHARE_TO_QQ_IMAGE_LOCAL_URL, filePath.getAbsolutePath());
		// params.putStringArrayList(QzoneShare.SHARE_TO_QQ_IMAGE_URL, imgList);
		params.putString(QzoneShare.SHARE_TO_QQ_APP_NAME, getResources()
				.getString(R.string.app_name));
		MyUtils.showLog("SportActivity 分享运动轨迹到QQzone  传递参数  "+params.toString());
		mtencent.shareToQzone(SportActivity.this, params, new ShareUiListener());
	}

	private void share2Tencent() {
		final Bundle params = new Bundle();
		params.putInt(QQShare.SHARE_TO_QQ_KEY_TYPE,
				QQShare.SHARE_TO_QQ_TYPE_IMAGE);
		params.putString(QQShare.SHARE_TO_QQ_TITLE,
				getString(R.string.app_name));
		params.putString(QQShare.SHARE_TO_QQ_IMAGE_LOCAL_URL,
				filePath.getAbsolutePath());
		params.putString(QQShare.SHARE_TO_QQ_APP_NAME, getResources()
				.getString(R.string.app_name));
		mtencent.shareToQQ(SportActivity.this, params, new ShareUiListener());
	}

	class ShareUiListener implements IUiListener {
		@Override
		public void onCancel() {
			ToastUtil.showToast(getString(R.string.share_cancel));
			dismissShareDialog();
		}

		@Override
		public void onComplete(Object arg0) {
			dismissShareDialog();
			ToastUtil.showToast(getString(R.string.share_success));
		}

		@Override
		public void onError(UiError arg0) {
			dismissShareDialog();
			ToastUtil.showToast(getString(R.string.share_fail));
		}
	}

}
