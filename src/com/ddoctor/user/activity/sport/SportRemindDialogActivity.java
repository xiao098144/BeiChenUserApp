package com.ddoctor.user.activity.sport;

import java.util.ArrayList;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.Window;
import android.widget.LinearLayout;

import com.ddoctor.user.wapi.bean.SportRemindBean;
import com.ddoctor.utils.DialogUtil;
import com.ddoctor.utils.DialogUtil.ConfirmDialog;
import com.ddoctor.utils.SportRemindDBUtil;
import com.ddoctor.utils.TimeUtil;
import com.ddoctor.utils.ToastUtil;
import com.umeng.analytics.MobclickAgent;
import com.zhuge.analysis.stat.ZhugeSDK;

public class SportRemindDialogActivity extends Activity {

	public static SportRemindDialogActivity _dialogInstance = null;

	private final static int REMINDLATER = 10; // 稍候提醒时间

	private ArrayList<SportRemindBean> _dataList;
	private Dialog _dialog = null;

	private Vibrator _vibrator;

	public static SportRemindDialogActivity getInstance() {
		return _dialogInstance;
	}

	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("SportRemindDialogActivity");
		MobclickAgent.onResume(SportRemindDialogActivity.this);
		ZhugeSDK.getInstance().init(getApplicationContext());
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("SportRemindDialogActivity");
		MobclickAgent.onPause(SportRemindDialogActivity.this);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);

		_dialogInstance = this;
		_vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

		LinearLayout ll = new LinearLayout(this);
		ll.setBackgroundColor(Color.TRANSPARENT);
		setContentView(ll);

		Bundle b = getIntent().getExtras();

		_dataList = b.getParcelableArrayList("data");

		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < _dataList.size(); i++) {
			SportRemindBean sportRemindBean = _dataList.get(i);
			sb.append((i + 1) + "、");
			sb.append(sportRemindBean.getContent() + " ");
			// sb.append(sportRemindBean.getType() == );
			sb.append("\n");
		}

		_vibrator.vibrate(new long[] { 200, 600, 200, 600 }, 2);

		// 提示添加成功
		_dialog = DialogUtil
				.confirmDialog(this, sb.toString(), "关闭", "稍后提醒",
						new ConfirmDialog() {

							@Override
							public void onOKClick(Bundle data) {
								for (int i = 0; i < _dataList.size(); i++) {
									SportRemindDBUtil.getInstance()
											.shutSportAlarmById(
													_dataList.get(i).getId(),
													_dataList.get(i)
															.getParentid());
								}

								_dialogInstance = null;
								_vibrator.cancel();
								SportRemindDialogActivity.this.finish();
							}

							@Override
							public void onCancleClick() {
								for (int i = 0; i < _dataList.size(); i++) {
									SportRemindBean sportRemindBean = _dataList
											.get(i);
									String time = sportRemindBean.getTime();
									String pattern = "HH:mm";
									if (time.contains("-")) {
										pattern = "HH-mm";
									}
									String minuteAdd = TimeUtil.getInstance()
											.minuteAdd(REMINDLATER, time,
													pattern);
									sportRemindBean.setTime(minuteAdd);
									if (0 == _dataList.get(i).getParentid()) {
										sportRemindBean
												.setParentid(sportRemindBean
														.getId());
										SportRemindDBUtil.getInstance()
												.insertSportRemind(
														sportRemindBean);
									} else {
										SportRemindDBUtil
												.getInstance()
												.updateSportRemindTimeById(
														minuteAdd,
														sportRemindBean.getId());
									}
								}
								ToastUtil.showToast("十分钟后继续提醒");

								_dialogInstance = null;
								_vibrator.cancel();
								SportRemindDialogActivity.this.finish();
							}

						}).setTitle("运动提醒").show();

	}

	@Override
	protected void onDestroy() {
		if (_dialog != null)
			_dialog.dismiss();

		_vibrator.cancel();

		super.onDestroy();
	}
}
