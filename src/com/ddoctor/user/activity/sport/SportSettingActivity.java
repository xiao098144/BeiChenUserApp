package com.ddoctor.user.activity.sport;

import android.app.Dialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.enums.RetError;
import com.ddoctor.interfaces.OnClickCallBackListener;
import com.ddoctor.user.activity.BaseActivity;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.task.SaveRemindTask;
import com.ddoctor.user.task.TaskPostCallBack;
import com.ddoctor.user.wapi.bean.SportRemindBean;
import com.ddoctor.utils.DialogUtil;
import com.ddoctor.utils.MyUtils;
import com.ddoctor.utils.SportRemindDBUtil;
import com.ddoctor.utils.StringUtils;
import com.ddoctor.utils.ToastUtil;
import com.umeng.analytics.MobclickAgent;
import com.zhuge.analysis.stat.ZhugeSDK;

public class SportSettingActivity extends BaseActivity implements
		OnClickCallBackListener {

	private RadioGroup mrg_sport;
	private TextView mtv_anim, mtv_set_anim, mtv_time, mtv_set_time,
			mtv_routing, mtv_set_routing;
	private FrameLayout mlayout_anim, mlayout_time, mlayout_routing;

	private EditText met_remark;

	private int sportType = 1; // 运动类型 默认1 代表步行 2 代表 跑步

	private SportRemindBean _walkBean;
	private SportRemindBean _runBean;

	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("SportSettingActivity");
		MobclickAgent.onResume(SportSettingActivity.this);
		ZhugeSDK.getInstance().init(getApplicationContext());
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("SportSettingActivity");
		MobclickAgent.onPause(SportSettingActivity.this);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sport_setting);
		week = getResources().getStringArray(R.array.week);
		_walkBean = SportRemindDBUtil.getInstance().selectSportRemindByType(1);
		_runBean = SportRemindDBUtil.getInstance().selectSportRemindByType(2);
		MyUtils.showLog(" SportSettingActivity  _walkBean "
				+ _walkBean.toString()+" \n _runBean "+_runBean.toString());
		findViewById();
		showDataUI(_walkBean);
	}

	private void showDataUI(SportRemindBean sportRemindBean) {

		String content = sportRemindBean.getContent();
		mtv_anim.setText(content);
		mtv_set_anim.setVisibility(TextUtils.isEmpty(content)?View.VISIBLE:View.GONE);
		mtv_anim.setVisibility(TextUtils.isEmpty(content)?View.GONE:View.VISIBLE);
		
		time = sportRemindBean.getTime();
		mtv_time.setText(time);
		mtv_set_time.setVisibility(TextUtils.isEmpty(time)?View.VISIBLE:View.GONE);
		mtv_time.setVisibility(TextUtils.isEmpty(time)?View.GONE:View.VISIBLE);
		
		routing = sportRemindBean.getRouting();
		if (!(TextUtils.isEmpty(routing) || routing.length() != 7 || routing
				.equals("000000"))) {
			mtv_routing.setText(getNameByrouting(routing));
			mtv_set_routing.setVisibility(View.GONE);
			mtv_routing.setVisibility(View.VISIBLE);
		} else {
			mtv_set_routing.setVisibility(View.VISIBLE);
			mtv_routing.setVisibility(View.GONE);
		}
		met_remark.setText(sportRemindBean.getRemark());
	}

	private String getNameByrouting(String routing) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < routing.length(); i++) {
			if ('1' == routing.charAt(i)) {
				sb.append(week[i]);
				sb.append("、");
			}
		}
		String string = sb.toString();
		if (string.endsWith("、")) {
			string = string.substring(0, string.length() - 1);
		}
		return string;
	}

	private boolean checkData() {
		MyUtils.showLog(mtv_anim.getVisibility()+" View.ViFIBLE "+View.VISIBLE);
		if (View.VISIBLE != mtv_anim.getVisibility()) {
			ToastUtil.showToast("请选择运动目标");
			return false;
		}
		MyUtils.showLog(mtv_time.getVisibility()+" View.VISIBLE "+View.VISIBLE+" "+time);
		if (View.VISIBLE != mtv_time.getVisibility() || TextUtils.isEmpty(time)) {
			ToastUtil.showToast("请选择开始运动的时间");
			return false;
		}
		MyUtils.showLog(mtv_routing.getVisibility()+" View.VISIBLE "+View.VISIBLE+" "+routing);
		if (View.VISIBLE != mtv_routing.getVisibility()
				|| TextUtils.isEmpty(routing)) {
			ToastUtil.showToast("请选择重复周期");
			return false;
		}
		sportRemindBean = new SportRemindBean();
		if (sportType == 1) {
			sportRemindBean.setId(_walkBean.getId());
		} else {
			sportRemindBean.setId(_runBean.getId());
		}
		sportRemindBean.setType(sportType);
		sportRemindBean.setContent(mtv_anim.getText().toString().trim());
		sportRemindBean.setTime(time);
		sportRemindBean.setRouting(routing);
		sportRemindBean.setParentid(0);
		sportRemindBean.setRemark(met_remark.getText().toString().trim());
		return true;
	}

	private SportRemindBean sportRemindBean;

	private void saveSportRemind() {

		SaveRemindTask task = new SaveRemindTask(sportRemindBean, 2);
		task.setTaskCallBack(new TaskPostCallBack<RetError>() {

			@Override
			public void taskFinish(RetError result) {
				if (result == RetError.NONE) {
					ToastUtil.showToast("保存成功");
					if (sportType == 1) {
						DataModule.getInstance().setStepGoalUpdated(true);
					}
					finishThisActivity();
				} else {
					ToastUtil.showToast(result.getErrorMessage());
				}
			}
		});
		task.executeParallel("");
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.btn_left: {
			finishThisActivity();

		}
			break;
		case R.id.btn_right: {
			if (checkData()) {
				saveSportRemind();
			}
		}
			break;
		case R.id.sport_layout_anim: {
			clickType = TYPE_ANIM;
			String unit = null;
			if (sportType == 1) {
				unit = "步";
			} else {
				unit = "米";
			}
			showInputDialog("提示", unit, "请输入目标数值", this);
		}
			break;
		case R.id.sport_layout_time: {
			clickType = TYPE_TIME;
			DialogUtil.creatTimeDialog(this, R.color.green_light, "取消", "确定",
					this);
		}
			break;
		case R.id.sport_layout_routing: {
			clickType = TYPE_ROUTING;
			DialogUtil.creatWeekDialog(this, R.color.green_light, "取消", "确定",
					this);
		}
			break;
		default:
			break;
		}
	}

	private String routing;
	private String time;
	private String[] week;
	private int clickType;
	private final static int TYPE_ANIM = 1;
	private final static int TYPE_TIME = 2;
	private final static int TYPE_ROUTING = 3;

	@Override
	public void onClickCallBack(Bundle data) {
		switch (clickType) {
		case TYPE_ANIM: {
			StringBuffer sb = new StringBuffer();
			sb.append(data.getInt("num"));
			if (sportType == 1) {
				sb.append("步");
			} else {
				sb.append("m");
			}
			mtv_anim.setText(sb);
			mtv_set_anim.setVisibility(View.INVISIBLE);
			mtv_anim.setVisibility(View.VISIBLE);
		}
			break;
		case TYPE_TIME: {
			StringBuffer sb = new StringBuffer();
			sb.append(StringUtils.formatnum(
					Integer.valueOf(data.getString("hour")), "00"));
			sb.append(":");
			sb.append(StringUtils.formatnum(
					Integer.valueOf(data.getString("minute")), "00"));
			time = sb.toString();
			mtv_time.setText(time);
			mtv_set_time.setVisibility(View.GONE);
			mtv_time.setVisibility(View.VISIBLE);
		}
			break;
		case TYPE_ROUTING: {
			routing = data.getString("data");
			MyUtils.showLog("", "选择结果  " + routing);
			if (!(TextUtils.isEmpty(routing) || routing.length() != 7 || routing
					.equals("000000"))) {
				mtv_routing.setText(getNameByrouting(routing));
				mtv_set_routing.setVisibility(View.GONE);
				mtv_routing.setVisibility(View.VISIBLE);
			} else {
				mtv_set_routing.setVisibility(View.VISIBLE);
				mtv_routing.setVisibility(View.GONE);
			}
		}
			break;
		default:
			break;
		}
	}

	//
	// @Override
	// public void onDialogShowing() {
	// // mlayout_routing.setPressed(true);
	// // mtv_set_routing.setPressed(true);
	// }
	//
	// @Override
	// public void onDialogDismiss() {
	// // mlayout_routing.setPressed(false);
	// // mtv_set_routing.setPressed(false);
	// }

	protected void findViewById() {
		Button leftButton = this
				.getLeftButtonText(getString(R.string.basic_back));
		Button rightButton = this
				.getRightButtonText(getString(R.string.basic_save));

		RelativeLayout rl = (RelativeLayout) findViewById(R.id.titleBar);
		if (rl != null) {
			rl.setBackgroundColor(getResources().getColor(R.color.green_light));
		}
		leftButton.setOnClickListener(this);
		rightButton.setOnClickListener(this);
		mrg_sport = (RadioGroup) findViewById(R.id.sport_menu);

		mtv_anim = (TextView) findViewById(R.id.sport_remind_tv_anim);
		mtv_set_anim = (TextView) findViewById(R.id.sport_remind_tv_add_anim);
		mlayout_anim = (FrameLayout) findViewById(R.id.sport_layout_anim);

		mtv_time = (TextView) findViewById(R.id.sport_remind_tv_time);
		mtv_set_time = (TextView) findViewById(R.id.sport_remind_tv_set_time);
		mlayout_time = (FrameLayout) findViewById(R.id.sport_layout_time);

		mtv_routing = (TextView) findViewById(R.id.sport_remind_tv_routing);
		mtv_set_routing = (TextView) findViewById(R.id.sport_remind_tv_set_routing);
		mlayout_routing = (FrameLayout) findViewById(R.id.sport_layout_routing);

		met_remark = (EditText) findViewById(R.id.sport_remind_et_remark);

		mrg_sport.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				sportType = checkedId == R.id.menu_walk ? 1 : 2;
				if (_walkBean == null) {
					_walkBean = SportRemindDBUtil.getInstance()
							.selectSportRemindByType(1);
					MyUtils.showLog("onCheckChanged _walkBean "
							+ _walkBean.toString());
				}
				if (_runBean == null) {
					_runBean = SportRemindDBUtil.getInstance()
							.selectSportRemindByType(2);
					MyUtils.showLog("onCheckChanged _runBean "
							+ _runBean.toString());
				}
				if (sportType == 1) {
					showDataUI(_walkBean);
				} else {
					showDataUI(_runBean);
				}
			}
		});
		mlayout_anim.setOnClickListener(this);
		mlayout_time.setOnClickListener(this);
		mlayout_routing.setOnClickListener(this);
	}

	private void showInputDialog(String title, String unit, String hint,
			final OnClickCallBackListener onClickCallBackListener) {
		final Dialog dialog = new Dialog(SportSettingActivity.this,
				R.style.NoTitleDialog);
		View view = LayoutInflater.from(SportSettingActivity.this).inflate(
				R.layout.layout_sport_remind_anim, null);
		TextView tv_title = (TextView) view.findViewById(R.id.dialog_tv_title);
		TextView tv_unit = (TextView) view.findViewById(R.id.dialog_tv_unit);
		final EditText et_input = (EditText) view
				.findViewById(R.id.dialog_et_input);
		Button btn_confirm = (Button) view
				.findViewById(R.id.bottom_btn_confirm);
		Button btn_cancel = (Button) view.findViewById(R.id.bottom_btn_cancel);

		tv_title.setText(title);
		tv_unit.setText(unit);
		et_input.setHint(hint);
		btn_cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		btn_confirm.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				String trim = et_input.getText().toString().trim();
				if (TextUtils.isEmpty(trim)) {
					ToastUtil.showToast("请输入目标数值");
				} else if (!StringUtils.pureNum(trim)) {
					ToastUtil.showToast("数据异常，请重新输入");
				} else if ("0".equals(trim)) {
					ToastUtil.showToast("目标数值不能等于0，请重新输入");
				} else {
					Bundle data = new Bundle();
					data.putInt("num", Integer.valueOf(trim));
					onClickCallBackListener.onClickCallBack(data);
					dialog.dismiss();
				}
			}
		});

		Window window = dialog.getWindow();
		window.setGravity(Gravity.CENTER);
		dialog.setCancelable(true);
		dialog.setCanceledOnTouchOutside(true);
		dialog.setContentView(view, new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.MATCH_PARENT,
				LinearLayout.LayoutParams.MATCH_PARENT));
		dialog.show();
	}

}
