package com.ddoctor.user.activity.sport;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.app.Dialog;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.enums.RecordLayoutType;
import com.ddoctor.enums.RefreshAction;
import com.ddoctor.enums.RetError;
import com.ddoctor.interfaces.OnClickCallBackListener;
import com.ddoctor.user.activity.BaseActivity;
import com.ddoctor.user.activity.diet.DietRecordListActivity;
import com.ddoctor.user.activity.medicine.MedicineRecordActivity;
import com.ddoctor.user.activity.sugar.BloodSugarRecordListActivity;
import com.ddoctor.user.adapter.SportRecordAdapter;
import com.ddoctor.user.task.GetRecordDateAndCountTask;
import com.ddoctor.user.task.GetSportRecordListTask;
import com.ddoctor.user.task.TaskPostCallBack;
import com.ddoctor.user.view.DDPullToRefreshView;
import com.ddoctor.user.view.DDPullToRefreshView.OnHeaderRefreshListener;
import com.ddoctor.user.wapi.bean.SportBean;
import com.ddoctor.user.wapi.constant.Record;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.DialogUtil;
import com.ddoctor.utils.MyUtils;
import com.ddoctor.utils.TimeUtil;
import com.ddoctor.utils.ToastUtil;
import com.pedometer.PedometerSettings;
import com.umeng.analytics.MobclickAgent;
import com.zhuge.analysis.stat.ZhugeSDK;

public class SportRecordActivity extends BaseActivity implements OnClickCallBackListener, OnHeaderRefreshListener, OnScrollListener {

	private TextView tv_title;
	
	private ListView _listView;
	DDPullToRefreshView _refreshViewContainer;
	private View _getMoreView;
	private TextView _tv_norecord;

	private int _pageNum = 1;
	private RefreshAction _refreshAction = RefreshAction.PULLTOREFRESH;
	
	private SportRecordAdapter _adapter;

	private int count;

	private List<SportBean> _dataList = new ArrayList<SportBean>();
	private List<SportBean> _resultList = new ArrayList<SportBean>();

	String title1 = "您一共使用了 ";
	String title2 = " 天，跑了 ";
	String title3;
	private int title_color;

	private List<SportBean> formatData(List<SportBean> list) {
		List<SportBean> resultList = new ArrayList<SportBean>();
		for (int i = 0; i < list.size(); i++) {
			SportBean sportBean = new SportBean();
			list.get(i).setRecorddate(TimeUtil.getInstance().formatDate2(list.get(i).getDate()));
			if (i == 0
					|| (i >= 1 && !list.get(i).getRecorddate()
							.equals(list.get(i - 1).getRecorddate()))) {
				sportBean.setRecorddate(list.get(i).getRecorddate());
				sportBean.setLayoutType(RecordLayoutType.TYPE_CATEGORY);
				resultList.add(sportBean);
			}
			resultList.add(list.get(i));
		}
		return resultList;
	}

	private int fromClass = 0; // 列表切换用 1 血糖列表 2 饮食 3 运动 4 用药

	private RelativeLayout _rl;

	private void getIntentInfo() {
		fromClass = getIntent().getIntExtra("from", 0);
	}

	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("SportRecordActivity");
		MobclickAgent.onResume(SportRecordActivity.this);
		ZhugeSDK.getInstance().init(getApplicationContext());
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("SportRecordActivity");
		MobclickAgent.onPause(SportRecordActivity.this);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_bs_recordlist);
		getIntentInfo();
		findViewById();
		loadingData(true , _pageNum);
		loadingTitle();
	}

	/**
	 * 获取记录总数
	 */
	private void loadingTitle() {

		GetRecordDateAndCountTask task = new GetRecordDateAndCountTask(
				Record.SPORT_RECORD);
		task.setTaskCallBack(new TaskPostCallBack<RetError>() {

			@Override
			public void taskFinish(RetError result) {
				MyUtils.showLog("", "页面接收到 获取总数接口 反馈的时间  "
						+ TimeUtil.getInstance().getStandardDate("HH:mm:ss.S"));
				if (RetError.NONE == result) {
					Bundle data = result.getBundle();
					int date = data.getInt("date");
					count = (int) data.getFloat("count");
					updateTitle(date, count);
				} else {
					tv_title.setVisibility(View.GONE);
				}
			}
		});
		task.executeParallel("");

	}

	private void updateTitle(int date, int distance) {
		SpannableStringBuilder str = new SpannableStringBuilder(title1);
		String dateStr = date + "";
		str.append(dateStr);
		str.append(title2);
		String countStr;
		if (distance < 1000) {
			title3 = " 米约等于 ";
			countStr = distance + "";
		} else {
			title3 = " 千米约等于 ";
			countStr = (float) distance / 1000 + "";
		}
		str.append(countStr);
		str.append(title3);
		String steps = countStepsBydistance(distance) + "";
		str.append(steps);
		str.append(" 步");
		str.setSpan(new ForegroundColorSpan(title_color), title1.length(),
				title1.length() + dateStr.length(),
				Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		str.setSpan(
				new ForegroundColorSpan(title_color),
				title1.length() + dateStr.length() + title2.length(),
				title1.length() + dateStr.length() + title2.length()
						+ countStr.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		str.setSpan(
				new ForegroundColorSpan(title_color),
				title1.length() + dateStr.length() + title2.length()
						+ countStr.length() + title3.length(),
				title1.length() + dateStr.length() + title2.length()
						+ countStr.length() + steps.length() + title3.length(),
				Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		tv_title.setText(str);
		tv_title.setVisibility(View.VISIBLE);
	}

	private int countStepsBydistance(int distance) {
		PedometerSettings settings = new PedometerSettings(
				PreferenceManager.getDefaultSharedPreferences(this));
		float stepLength = settings.getStepLength();
		return (int) (distance / stepLength * 100);
	}

	private Dialog _loadingDialog;
	private void loadingData(boolean showDialog , int page) {
		if (showDialog) {
			_loadingDialog = DialogUtil
					.createLoadingDialog(this, "正在加载中，请稍候...");
			_loadingDialog.show();
		}
		GetSportRecordListTask task = new GetSportRecordListTask(page);
		final int page1 = page;
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {

			@Override
			public void taskFinish(DDResult result) {
				if (result.getError() == RetError.NONE) {
					List<SportBean> tmpList = result.getBundle()
							.getParcelableArrayList("list");
					if (page1 > 1) // 加载更多
					{
						_resultList.addAll(tmpList);
						Collections.sort(_resultList);
						_dataList.clear();
						_dataList.addAll(formatData(_resultList));
						_adapter.notifyDataSetChanged();
					} else {
						// 加载第一页
						_dataList.clear();
						_resultList.clear();
						_resultList.addAll(tmpList);
						Collections.sort(_resultList);
						_dataList.addAll(formatData(_resultList));
						_adapter.notifyDataSetChanged();

						_refreshViewContainer.onHeaderRefreshComplete();
						_refreshViewContainer.setVisibility(View.VISIBLE);
						_refreshAction = RefreshAction.NONE;

						if (_loadingDialog != null)
							_loadingDialog.dismiss();

					}

					// 是否显示"加载更多"
					if (tmpList.size() > 0) {
						setGetMoreContent("滑动加载更多", true, false);
						_bGetMoreEnable = true;
					} else {
						if (page1 == 1) {
							_tv_norecord.setText(result.getErrorMessage());
							_tv_norecord.setTag(0);
						}
						// 没数据了，加载完成
						setGetMoreContent("已全部加载", false, false);
						_bGetMoreEnable = false;
					}

					// 确保加载成功后，再修改这个变量
					_pageNum = page1;
				} else {// 加载失败
					if (page1 > 1) {
						setGetMoreContent("滑动加载更多", true, false);
					} else {
						_refreshViewContainer.onHeaderRefreshComplete();
						_refreshAction = RefreshAction.NONE;

						if (_loadingDialog != null)
							_loadingDialog.dismiss();
					}

					ToastUtil.showToast(result.getErrorMessage());
				}

				_refreshAction = RefreshAction.NONE;
			}
		});

		task.executeParallel("");
	}

	protected void findViewById() {
		title_color = getResources().getColor(R.color.green_light);
		this.setTitle("运动记录");
		Button leftBtn = getLeftButtonText(getResources().getString(
				R.string.basic_back));
		Button rightBtn = getRightButtonText(getResources().getString(
				R.string.basic_change));
		_rl = (RelativeLayout) findViewById(R.id.titleBar);
		if (_rl != null) {
			_rl.setBackgroundColor(title_color);
		}
		tv_title = (TextView) findViewById(R.id.recordlist_title);
		leftBtn.setOnClickListener(this);
		rightBtn.setOnClickListener(this);

		_refreshViewContainer = (DDPullToRefreshView) findViewById(R.id.refreshViewContainer);
		_refreshViewContainer.setOnHeaderRefreshListener(this);
		_refreshViewContainer.setVisibility(View.INVISIBLE);

		_listView = (ListView) findViewById(R.id.listView);
		_listView.setOnScrollListener(this);
		_tv_norecord = (TextView) findViewById(R.id.tv_norecord);
		_listView.setEmptyView(_tv_norecord);
		initList();
		
	}

	private void initList() {
		// 获取更多
		_getMoreView = createGetMoreView();
		setGetMoreContent("已全部加载", false, false);
		_listView.addFooterView(_getMoreView);

		// 数据
		_adapter = new SportRecordAdapter(this);
		_listView.setAdapter(_adapter);
		_adapter.setData(_dataList);
	}

	// 加载更多相关函数 >>>>>>
	boolean _bGetMoreEnable = false;

	private View createGetMoreView() {
		if (_getMoreView != null)
			return _getMoreView;

		View v = (View) getLayoutInflater().inflate(R.layout.refresh_footer,
				null);

		return v;
	}

	private void setGetMoreContent(String message, boolean showImage,
			boolean animation) {
		TextView tv = (TextView) _getMoreView
				.findViewById(R.id.pull_to_load_text);
		tv.setText(message);

		ImageView imgView = (ImageView) _getMoreView
				.findViewById(R.id.pull_to_load_image);
		AnimationDrawable ad = (AnimationDrawable) imgView.getBackground();
		if (showImage) {
			if (animation) {
				ad.start();
			} else {
				ad.stop();
				ad.selectDrawable(0);
			}

			imgView.setVisibility(View.VISIBLE);
		} else {
			ad.stop();
			ad.selectDrawable(0);

			imgView.setVisibility(View.GONE);
		}
	}

	@Override
	public void onScroll(AbsListView arg0, int firstVisibleItem, int arg2,
			int arg3) {

		// 工具栏的显示与隐藏
		if (_refreshAction == RefreshAction.NONE) {
			if (_bGetMoreEnable) {// 有加载更多
				int lastPos = _listView.getLastVisiblePosition();
				int total = _listView.getHeaderViewsCount() + _dataList.size()
						+ _listView.getFooterViewsCount();
				if (lastPos == total - 1) {
					// 加载更多显示出来了，开始加载
					_refreshAction = RefreshAction.LOADMORE;
					setGetMoreContent("正在加载...", true, true);
					loadingData(false, _pageNum + 1);
				}
			}
		}

	}

	@Override
	public void onScrollStateChanged(AbsListView arg0, int arg1) {

	}

	@Override
	public void onHeaderRefresh(DDPullToRefreshView view) {
		if (_refreshAction == RefreshAction.NONE) {
			_refreshAction = RefreshAction.PULLTOREFRESH;
			loadingData(false, 1);
		} else {
			// 正在加载，什么也不做
			view.onHeaderRefreshComplete();
		}
	}
	
	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.btn_left: {
			finishThisActivity();
		}
			break;
		case R.id.btn_right: {
			DialogUtil.changeRecordList(SportRecordActivity.this, _rl,
					this);
		}
			break;
		default:
			break;
		}
	}

	@Override
	public void onClickCallBack(Bundle data) {
		int type = data.getInt("type");
		switch (type) {
		case 2: {
			if (fromClass == 2) {
				finishThisActivity();
			} else {
				skip("from", 3, DietRecordListActivity.class, false);
			}
		}
			break;
		case 1: {
			if (fromClass == 1) {
				finishThisActivity();
			} else {
				skip("from", 3, BloodSugarRecordListActivity.class, false);
			}
		}
			break;
		case 4: {
			if (fromClass == 4) {
				finishThisActivity();
			} else {
				skip("from", 3, MedicineRecordActivity.class, false);
			}

		}
			break;
		default:
			break;
		}
	}

}