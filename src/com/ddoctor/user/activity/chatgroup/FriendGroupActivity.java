package com.ddoctor.user.activity.chatgroup;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;

import com.beichen.user.R;
import com.ddoctor.user.activity.BaseActivity;
import com.ddoctor.user.adapter.FriendGroupListAdapter;
import com.ddoctor.user.model.FriendGroupListBean;
import com.ddoctor.user.swiplistview.RefreshListView;
import com.ddoctor.utils.ToastUtil;

public class FriendGroupActivity extends BaseActivity implements OnItemClickListener{

	private Button leftBtn;
	private RefreshListView refreshLv;
	
	private List<FriendGroupListBean>dataList;
	private FriendGroupListAdapter adapter;
	
	private void initdata(){
		dataList = new ArrayList<FriendGroupListBean>();
		for (int i = 0; i < 7; i++) {
			FriendGroupListBean fglBean = new FriendGroupListBean();
			fglBean.setDescribe("圈子描述圈子描述圈子描述圈子描述圈子描述圈子描述圈子描述圈子描述圈子描述圈子描述圈子描述  --- "+i);
			fglBean.setGroup_name("圈子名称圈子名称 --- "+i);
			fglBean.setLastmsg_name("用户名用户名用户名用户名 --- "+i);
			fglBean.setLastmsg_content("最后消息内容最后消息内容最后消息内容 --- "+i);
			fglBean.setMsg_time("2015-"+(i+5)+"-"+(i+23)+" 12:33:21");
			if (i%3==0) {
				fglBean.setPicUrl("");
			}else {
				fglBean.setPicUrl("dfhkjl;'");
			}
			dataList.add(fglBean);
		}
		Collections.sort(dataList);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_myrecords);
		findViewById();
		initdata();
//		adapter.setData(dataList);
	}
	
	protected void findViewById() {
		leftBtn = getLeftButtonText(getResources().getString(R.string.basic_back));
		setTitle(getResources().getString(R.string.group_friend));
//		refreshLv = (RefreshListView) findViewById(R.id.myreocrds_lv);
//		refreshLv.setDivider(null);
//		refreshLv.setDividerHeight(12);
//		adapter = new FriendGroupListAdapter(this);
//		refreshLv.setAdapter(adapter);
//		leftBtn.setOnClickListener(this);
//		refreshLv.setOnItemClickListener(this);
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.btn_left:{
			finishThisActivity();
		}
			break;

		default:
			break;
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		ToastUtil.showToast("点击查看 "+position);
	}
	
}
