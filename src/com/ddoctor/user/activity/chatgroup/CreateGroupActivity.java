package com.ddoctor.user.activity.chatgroup;

import java.util.ArrayList;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.user.activity.BaseActivity;
import com.ddoctor.user.activity.mine.MyFriendsActivity;
import com.ddoctor.user.config.AppBuildConfig;
import com.ddoctor.user.model.MyFriendBean;
import com.ddoctor.utils.MyUtils;
import com.ddoctor.utils.StringUtils;
import com.ddoctor.utils.ToastUtil;

public class CreateGroupActivity extends BaseActivity implements TextWatcher {

	private Button leftBtn;
	private Button rigthBtn;
	private EditText et_name;
	private EditText et_describe;
	private ImageButton img_btn_add;
	private TextView tv_now;
	private LinearLayout friend_layout;
	private TextView tv_tips;

	private LayoutInflater inflater;

	private ArrayList<MyFriendBean> chosed = new ArrayList<MyFriendBean>();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_creategroup);
		inflater = LayoutInflater.from(CreateGroupActivity.this);
		findViewById();
	}

	protected void findViewById() {
		leftBtn = getLeftButtonText(getString(R.string.basic_back));
		rigthBtn = getRightButtonText(getString(R.string.creategroup_create));
		setTitle(getString(R.string.creategroup_title));
		et_name = (EditText) findViewById(R.id.creategroup_et_name);
		et_name.setHint(StringUtils
				.fromatETHint(
						getResources().getString(
								R.string.et_hint_creategroup_name), 14));
		et_describe = (EditText) findViewById(R.id.creategroup_et_describe);
		img_btn_add = (ImageButton) findViewById(R.id.creategroup_img_add);
		tv_now = (TextView) findViewById(R.id.creategroup_tv_now);
		tv_tips = (TextView) findViewById(R.id.creategroup_tv_add_friend);
		friend_layout = (LinearLayout) findViewById(R.id.creategroup_friend_protrait_layout);
		tv_now.setText("0");
		tv_tips.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG); // 下划线
		tv_tips.getPaint().setAntiAlias(true);// 抗锯齿

		leftBtn.setOnClickListener(this);
		rigthBtn.setOnClickListener(this);
		img_btn_add.setOnClickListener(this);
		tv_tips.setOnClickListener(this);
		et_describe.addTextChangedListener(this);
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.btn_left: {
			finishThisActivity();
		}
			break;
		case R.id.btn_right: {
			ToastUtil.showToast("");
		}
			break;
		case R.id.creategroup_img_add: {
			ToastUtil.showToast("");
		}
			break;
		case R.id.creategroup_tv_add_friend: {
			Bundle data = new Bundle();
			data.putBoolean("fromGroup", true);
			data.putParcelableArrayList("chosed", chosed);
			Intent intent = new Intent();
			intent.putExtra(AppBuildConfig.BUNDLEKEY, data);
			intent.setClass(CreateGroupActivity.this, MyFriendsActivity.class);
			startActivityForResult(intent, 200);
		}
			break;

		default:
			break;
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		MyUtils.showLog("", "数据反馈  requestCode = " + requestCode
				+ " resultCode = " + resultCode);
		if (resultCode != RESULT_OK) {
			return;
		}
		if (requestCode == 200) {
			chosed = data.getParcelableArrayListExtra(AppBuildConfig.BUNDLEKEY);
			if (chosed != null && chosed.size() > 0) {
				tv_tips.setVisibility(View.GONE);
				if (friend_layout.getChildCount() > 1) {
					friend_layout.removeAllViews();
				}
				int total = chosed.size() / 4;
				MyUtils.showLog("", "接收到的数据  " + chosed.toString());
				LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
						LinearLayout.LayoutParams.MATCH_PARENT, 0, 1);
				friend_layout.setLayoutParams(params);
				for (int i = 0; i < total; i++) {
					LinearLayout layout = getLinearlayout();
					friend_layout.addView(layout, i, params);
					for (int j = 0; j < 4; j++) {
						int index = 0;
						if (i == 0) {
							index = j;
						} else {
							index = (i * 4) + (j + 1);
						}
						layout.addView(
								generateFriendItem(chosed.get(index)
										.getPicUrl(), chosed.get(index)
										.getDocName()), j);
						index = 0;
					}
				}
				int left = chosed.size() % 4;
				LinearLayout layout = getLinearlayout();
				for (int i = 0; i < left; i++) {
					layout.addView(
							generateFriendItem(chosed.get(total * 4 + i)
									.getPicUrl(), chosed.get(total * 4 + i)
									.getDocName()), i);
				}
				layout.addView(generateFriendItem(null, null), left);
				friend_layout.addView(layout, total);
				layout.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						Bundle data = new Bundle();
						data.putBoolean("fromGroup", true);
						data.putParcelableArrayList("chosed", chosed);
						Intent intent = new Intent();
						intent.putExtra(AppBuildConfig.BUNDLEKEY, data);
						intent.setClass(CreateGroupActivity.this,
								MyFriendsActivity.class);
						startActivityForResult(intent, 200);
					}
				});
				friend_layout.setVisibility(View.VISIBLE);
			} else {
				tv_tips.setVisibility(View.VISIBLE);
				friend_layout.setVisibility(View.GONE);
			}
		}
	}

	private LinearLayout getLinearlayout() {
		LinearLayout layout = new LinearLayout(this);
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.MATCH_PARENT,
				LinearLayout.LayoutParams.WRAP_CONTENT);
		layout.setGravity(Gravity.CENTER_VERTICAL);
		layout.setLayoutParams(params);
		return layout;
	}

	/**
	 * 头像 姓名 全为空时 标识添加-继续添加按钮
	 * 
	 * @param picUrl
	 * @param name
	 * @return
	 */
	private View generateFriendItem(String picUrl, String name) {
		View view = inflater.inflate(R.layout.layout_pictext, null);
		ImageView img = (ImageView) view
				.findViewById(R.id.pictext_img_protrait);
		if (picUrl == null) {
			img.setBackgroundResource(R.drawable.rc_ic_setting_friends_add);
		} else {
			img.setBackgroundResource(R.drawable.chat_default_protrait);
		}
		TextView tv_name = (TextView) view.findViewById(R.id.pictext_tv_name);
		if (null == name) {
			name = getResources().getString(R.string.continue_add);
		}
		tv_name.setText(name);

		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
				MyUtils.getScreenWidth(CreateGroupActivity.this) / 4,
				LayoutParams.WRAP_CONTENT);
		params.setMargins(0, 15, 0, 15);
		view.setLayoutParams(params);
		return view;
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
		tv_now.setText(s.length() + "");
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		// TODO Auto-generated method stub

	}

	@Override
	public void afterTextChanged(Editable s) {
		if (s != null) {
			tv_now.setText(s.length() + "");
		}
	}

}
