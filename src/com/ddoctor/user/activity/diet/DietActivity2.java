package com.ddoctor.user.activity.diet;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.PaintDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.animation.AnimationUtils;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.enums.RetError;
import com.ddoctor.user.activity.BaseFragmentActivity;
import com.ddoctor.user.config.AppBuildConfig;
import com.ddoctor.user.task.GetDietRecordListTask;
import com.ddoctor.user.task.TaskPostCallBack;
import com.ddoctor.user.view.RecordCalendar;
import com.ddoctor.user.view.RecordCalendar.OnCalendarClickListener;
import com.ddoctor.user.view.RecordCalendar.OnCalendarDateChangedListener;
import com.ddoctor.user.wapi.bean.DietBean;
import com.ddoctor.user.wapi.bean.FoodBean;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.FileUtils;
import com.ddoctor.utils.ImageLoaderUtil;
import com.ddoctor.utils.MyUtils;
import com.ddoctor.utils.StringUtils;
import com.ddoctor.utils.TimeUtil;
import com.ddoctor.utils.ToastUtil;
import com.umeng.analytics.MobclickAgent;
import com.zhuge.analysis.stat.ZhugeSDK;

public class DietActivity2 extends BaseFragmentActivity implements
		OnCheckedChangeListener, OnClickListener {

	private Button leftBtn;
	private Button rightBtn;
	private RadioGroup rg_time;

	// private LinearLayout _layout_menu; // 暂时屏蔽推荐食谱
	private RelativeLayout _layout_doc; // 我的营养师 点击 弹出提示 不跳转页面
	private LinearLayout _layout_record;

	private WebView _webView;
	private ImageView _progressBarImageView;
	private TextView _chartTextView;

	/** 早午晚餐类型 */
	private TextView tv_meal_type;
	/** 记录时间 */
	private TextView tv_record_time;
	private RelativeLayout layout_meal;
	/** 仅显示第一张图片 */
	private ImageView mealImg;
	/** 具体描述 */
	private TextView tv_mealDescribe;
	private TextView tv_hot;
	private TextView tv_add;

	private TextView extra_tv_add;
	/** 额外加餐类型 */
	private TextView extra_tv_meal_type;
	/** 记录时间 */
	private TextView extra_tv_record_time;
	private RelativeLayout extra_layout_meal;
	/** 仅显示第一张图片 */
	private ImageView extra_mealImg;
	/** 具体描述 */
	private TextView extra_tv_mealDescribe;
	private TextView extra_tv_hot;

	private int _currentTabIndex = -1;

	DietBean moringBean = new DietBean();
	DietBean noonBean = new DietBean();
	DietBean nightBean = new DietBean();
	DietBean extraMoringBean = new DietBean();
	DietBean extraNoonBean = new DietBean();
	DietBean extraNightBean = new DietBean();
	
	private void initData() {
		MODULE = getResources().getString(R.string.time_format_10);
		today = TimeUtil.getInstance().getStandardDate(MODULE);
		endDate = TimeUtil.getInstance().dateAddFrom(1, today, MODULE,
				Calendar.DATE);
		startDate = TimeUtil.getInstance().dateAddFrom(DATARouting, endDate,
				MODULE, Calendar.DATE);

		moringBean.setType(1);
		extraMoringBean.setType(4);
		noonBean.setType(2);
		extraNoonBean.setType(5);
		nightBean.setType(3);
		extraNightBean.setType(6);
	}

	private String today, startDate, endDate;
	private String MODULE;
	private static final int DATARouting = -7; // 数据加载周期 即每次加载几天的数据

	protected String loading_error = "加载失败，点击重试";

	boolean falg = false;

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.putInt("tabIndex", showTabIndex);
		outState.putBoolean("falg", true);
		MyUtils.showLog(" DietActivity onSaveIntsanced 页面被销毁  保存数据   ");
		// super.onSaveInstanceState(outState);
	}

	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onResume(DietActivity2.this);
		ZhugeSDK.getInstance().init(getApplicationContext());
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPause(DietActivity2.this);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (null != savedInstanceState) {
			MyUtils.showLog("DietActivity2 onCreate 页面销毁 重建  "
					+ savedInstanceState.toString());
			showTabIndex = savedInstanceState.getInt("tabIndex");
			falg = savedInstanceState.getBoolean("falg", false);
		} else {
			showTabIndex = getDefaultMealType();
		}
		setContentView(R.layout.act_diet2);
		MyUtils.showLog("DietActivity2 OnCreate ");
		initData();
		findViewById();
		initWebView();
		startGetChartData();

	}

	// 加载图表的数据
	private void startGetChartData() {
		_webView.setVisibility(View.INVISIBLE);
		_chartTextView.setVisibility(View.INVISIBLE);
		showChartLoading(true);

		GetDietRecordListTask task = new GetDietRecordListTask(startDate,
				endDate, 1, 0);
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {
			@Override
			public void taskFinish(DDResult result) {
				if (result.getError() == RetError.NONE) {
					ArrayList<DietBean> dataList = result.getBundle()
							.getParcelableArrayList("list");
					if (dataList != null && dataList.size() > 0) {
						loadChart(dataList);
					} else {
						loadChartFailed(result.getErrorMessage());
					}
				} else {
					loadChartFailed(loading_error);
				}
			}
		});
		task.executeParallel("");
	}

	List<DietBean> todayList = new ArrayList<DietBean>();
	/**
	 * 处理日历选择结果 msg arg1 月份 arg2 日期
	 */
	private Handler handler = new Handler(new Handler.Callback() {

		@Override
		public boolean handleMessage(Message msg) {
			Bundle data = msg.getData();
			int year = data.getInt("year");
			int month = data.getInt("month");
			int date = data.getInt("date");

			endDate = TimeUtil.getInstance().dateAddFrom(
					1,
					year + "-" + StringUtils.formatnum(month, "00") + "-"
							+ StringUtils.formatnum(date, "00"), MODULE,
					Calendar.DATE);
			startDate = TimeUtil.getInstance().dateAddFrom(DATARouting,
					endDate, MODULE, Calendar.DATE);
			startGetChartData();

			return false;
		}
	});

	private void loadChart(ArrayList<DietBean> dataList) {
		StringBuffer categories = new StringBuffer(), hot = new StringBuffer();
		/**
		 * 分解得到单纯日期便于后期分组处理、以及得到今日数据
		 */
		List<String> dateList = new ArrayList<String>();
		if (todayList == null) {
			todayList = new ArrayList<DietBean>();
		}
		if (todayList.size() > 0) {
			todayList.clear();
		}
		for (int i = 0; i < dataList.size(); i++) {
			String date = TimeUtil.getInstance().getFormatDate(
					dataList.get(i).getTime(), "MM/dd");
			if (!dateList.contains(date)) {
				dateList.add(date);
			}
			dataList.get(i).setDate(date);
			if (TimeUtil.getInstance().isSameDay(today,
					dataList.get(i).getTime())) {
				todayList.add(dataList.get(i));
			}
		}
		_currentTabIndex = -1;
		showTodayDetail();
		for (int i = dateList.size() - 1; i >= 0; i--) {
			List<DietBean> list = new ArrayList<DietBean>();
			for (int j = dataList.size() - 1; j >= 0; j--) {
				if (dateList.get(i).equals(dataList.get(j).getDate())) {
					list.add(dataList.get(j));
				}
			}
			Collections.sort(list);
			hot.append(list.get(0).getHeat());
			categories.append("'");
			categories.append(dateList.get(i));
			categories.append("'");
			if (i > 0) {
				hot.append(",");
				categories.append(",");
			}
		}
		loadChart(categories.toString(), hot.toString());
	}

	private void showTodayDetail() {
		MyUtils.showLog("",
				"今日数据 " + todayList.size() + " " + todayList.toString());
		if (todayList.size() > 0) {
			for (int i = 0; i < todayList.size(); i++) {
				Integer type = todayList.get(i).getType();
				if (type == null) {
					type = 1;
				}
				switch (type) {
				case 1: {
					moringBean.setData(todayList.get(i));
				}
					break;
				case 2: {
					noonBean.setData(todayList.get(i));
				}
					break;
				case 3: {
					nightBean.setData(todayList.get(i));
				}
					break;
				case 4: {
					extraMoringBean.setData(todayList.get(i));
				}
					break;
				case 5: {
					extraNoonBean.setData(todayList.get(i));
				}
					break;
				case 6: {
					extraNightBean.setData(todayList.get(i));
				}
					break;

				default:
					break;
				}
			}
		}

		showTab(showTabIndex);
	}

	private void loadChartFailed(String msg) {
		showTodayDetail();
		_chartTextView.setText(msg);
		_chartTextView.setVisibility(View.VISIBLE);
		showChartLoading(false);
	}

	private void showChartLoading(boolean show) {
		AnimationDrawable ad = (AnimationDrawable) _progressBarImageView
				.getBackground();
		if (show) {
			ad.start();
			_progressBarImageView.setVisibility(View.VISIBLE);
		} else {
			_progressBarImageView.setVisibility(View.INVISIBLE);
			ad.stop();
		}

	}

	// 加载WebView里的chart
	private void loadChart(String categories, String hot) {
		final int screenWidth = MyUtils.getScreenWidth(DietActivity2.this);
		String html = FileUtils.getFromAssets(getResources(), "diet.html");

		html = html.replace("{$AndroidContainerWidth}", "" + screenWidth);
		html = html.replace("{$AndroidContainerHeight}",
				"" + _webView.getHeight());  

		// String strCategories =
		// "'05/11','05/12','05/13','05/14','05/15','05/16','05/18'";
		html = html.replace("{$CategoriesData}", categories);

		// String sugar = "8.5, 6.7, 9.4, 8.4, 11.3, 10.2, 8.7";
//		html = html.replace("{$sugar}", sugar);
		// String hot = "3.5, 9.7, 5.4, 2.5, 5.3, 7.2, 11.7";
		html = html.replace("{$hot}", hot);

		_webView.loadDataWithBaseURL("file:///android_asset/", html,
				"text/html", "utf-8", "");
	}

	@SuppressLint("SetJavaScriptEnabled")
	private void initWebView() {

		_webView.getSettings().setJavaScriptEnabled(true);
		_webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
		_webView.setWebChromeClient(new WebChromeClient());

		_webView.setWebViewClient(new WebViewClient() {
			public boolean shouldOverrideUrlLoading(WebView view, String url) { // 重写此方法表明点击网页里面的链接还是在当前的webview里跳转，不跳到浏览器那边
				view.loadUrl(url);
				return true;
			}

			@Override
			public void onPageFinished(WebView view, String url) {
				_webView.setVisibility(View.VISIBLE);
				showChartLoading(false);
			}
		});

		/*
		 * ViewTreeObserver vto; final View rootView = view; vto =
		 * _webView.getViewTreeObserver(); vto.addOnGlobalLayoutListener(new
		 * OnGlobalLayoutListener(){
		 * 
		 * @Override public void onGlobalLayout() { // TODO Auto-generated
		 * method stub MyUtils.showLog("webView:" + _webView.getHeight());
		 * 
		 * loadChart(); } });
		 */
	}

	/**
	 * 根据当前小时 显示 早午晚三餐对应类型 0 早 1 午 2晚
	 * 
	 * @return
	 */
	private int getDefaultMealType() {
		int hour = TimeUtil.getInstance().getCurrentHour();
		if (hour > 5 && hour <= 10) {
			return 0;
		} else if (hour >= 11 && hour <= 15) {
			return 1;
		} else {
			return 2;
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_left: {
			finishThisActivity();
		}
			break;
		case R.id.btn_right: {
			showCalendar(rightBtn, handler);
		}
			break;
		case R.id.diet_menu: {
			Intent intent = new Intent();
			intent.setClass(DietActivity2.this, MyRecipeActivity.class);
			startActivity(intent);
		}
			break;
		case R.id.diet_doc_layout: {
			ToastUtil.showToast("营养师正在休息，请稍候再试");
		}
			break;
		case R.id.diet_record_layout: {
			Intent intent = new Intent();
			intent.setClass(DietActivity2.this, DietRecordListActivity.class);
			startActivity(intent);
		}
			break;
		case R.id.textView: {
			startGetChartData();
		}
			break;
		case R.id.diet_detail_tv_add: {
			skip(false);
		}
			break;
		case R.id.diet_extra_tv_add: {
			skip(true);
		}
			break;
		case R.id.diet_detail_layout: {
			skip(false);
		}
			break;
		case R.id.diet_extra_layout: {
			skip(true);
		}
			break;
		default:
			break;
		}
	}

	private int showTabIndex;

	private void skip(boolean isExtra) {
		Intent intent = new Intent();
		intent.setClass(DietActivity2.this, AddDietRecordActivity2.class);
		Bundle data = new Bundle();
		Integer dietId = 0;
		if (isExtra) {
			switch (_currentTabIndex) {
			case 0: {
				dietId = extraMoringBean.getId();
			}
				break;
			case 1: {
				dietId = extraNoonBean.getId();
			}
				break;
			case 2: {
				dietId = extraNightBean.getId();
			}
				break;
			default:
				break;
			}
			data.putInt("type", (_currentTabIndex + 1) + 3);
			data.putInt("id",dietId==null ?0:dietId);
		} else {
			switch (_currentTabIndex) {
			case 0: {
				dietId = moringBean.getId();
			}
				break;
			case 1: {
				dietId = noonBean.getId();
			}
				break;
			case 2: {
				dietId = nightBean.getId();
			}
				break;
			default:
				break;
			}
			data.putInt("type", (_currentTabIndex + 1));
			data.putInt("id",dietId==null?0:dietId);
		}
		showTabIndex = _currentTabIndex;
		intent.putExtra(AppBuildConfig.BUNDLEKEY, data);
		DietActivity2.this.startActivityForResult(intent, 200);
	}

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		for (int i = 0; i < group.getChildCount(); i++) {
			if (group.getChildAt(i).getId() == checkedId) {
				showTab(i);
				break;
			}
		}
	}

	private void showTab(int tabIndex) {
		if (tabIndex < 0 || tabIndex >= rg_time.getChildCount())
			return;

		if (_currentTabIndex == tabIndex)
			return;
		switch (tabIndex) {
		case 0: {
			showDataUI(moringBean, extraMoringBean, 1);
		}
			break;
		case 1: {
			showDataUI(noonBean, extraNoonBean, 2);
		}
			break;
		case 2: {
			showDataUI(nightBean, extraNightBean, 3);
		}
			break;
		default:
			break;
		}

		_currentTabIndex = tabIndex;

		RadioButton rb = (RadioButton) rg_time.getChildAt(tabIndex);
		if (!rb.isChecked())
			rb.setChecked(true);
	}

	private void showDataUI(DietBean mealBean, DietBean extraMealBean,
			int mealType) {

		tv_meal_type.setText(getMealTypeById(mealType));
		if (null == mealBean || TextUtils.isEmpty(mealBean.getTime())) {
			layout_meal.setVisibility(View.GONE);
			tv_add.setVisibility(View.VISIBLE);
			tv_record_time.setText(null);
		} else {
			tv_add.setVisibility(View.GONE);
			layout_meal.setVisibility(View.VISIBLE);
			tv_record_time.setText(TimeUtil.getInstance().formatReplyTime2(
					mealBean.getTime()));
			
			tv_mealDescribe.setText(getDescribe(mealBean));
			
			Integer heat = mealBean.getHeat();
			if (heat == null) {
				heat = 0;
			}
			tv_hot.setText(String.format("%dkcal", heat));
			
			showImg(mealBean.getFile(), mealImg);
		}

		extra_tv_meal_type.setText(getMealTypeById(mealType + 3));
		if (extraMealBean == null || TextUtils.isEmpty(extraMealBean.getTime())) {
			extra_layout_meal.setVisibility(View.GONE);
			extra_tv_add.setVisibility(View.VISIBLE);
			extra_tv_record_time.setText(null);
		} else {
			extra_tv_add.setVisibility(View.GONE);
			extra_layout_meal.setVisibility(View.VISIBLE);
			extra_tv_record_time.setText(TimeUtil.getInstance()
					.formatReplyTime2(extraMealBean.getTime()));

			extra_tv_mealDescribe.setText(getDescribe(extraMealBean));
			
			Integer heat = extraMealBean.getHeat();
			if (heat == null) {
				heat = 0;
			}
			extra_tv_hot.setText(String.format("%dkcal", heat));
			MyUtils.showLog(" showData extra_img " + extra_mealImg.toString());
			showImg(extraMealBean.getFile(), extra_mealImg);
		}
	}

	private String getMealTypeById(int type) {

		String[] array = getResources().getStringArray(R.array.meal_type);
		if (type > array.length) {
			type -= 1;
		}
		if (type <= 0) {
			type += 1;
		}
		return array[type - 1];
	}

	private void showImg(String urlStr, ImageView img) {
		MyUtils.showLog("showImg urlStr " + urlStr + " " + img.toString());
		if (!TextUtils.isEmpty(urlStr)) {
			String[] url = urlStr.split("\\|");
			ImageLoaderUtil.display(StringUtils.urlFormatThumbnail(url[0]),
					img, R.drawable.food_pic);
		} else {
			img.setBackgroundResource(R.drawable.food_pic);
		}
	}

	private String getDescribe(DietBean dietBean) {
		if (dietBean == null) {
			return "";
		}
		StringBuffer sb = new StringBuffer();

		List<FoodBean> gushuList = dietBean.getGuShuList();
		List<FoodBean> shucaiList = dietBean.getShuCaiList();
		List<FoodBean> shuiguoList = dietBean.getShuiGuoList();
		List<FoodBean> dadouList = dietBean.getDaDouList();
		List<FoodBean> rouDanList = dietBean.getRouDanList();
		List<FoodBean> ruList = dietBean.getRuList();
		List<FoodBean> yingguoList = dietBean.getYingGuoList();
		List<FoodBean> youzhiList = dietBean.getYouZhiList();
		
		if (gushuList != null && gushuList.size() > 0) {
			for (int i = 0; i < gushuList.size(); i++) {
				sb.append(gushuList.get(i).getName());
				sb.append(gushuList.get(i).getCount()+gushuList.get(i).getUnit());
				sb.append("、");
			}
		}

		if (shucaiList != null && shucaiList.size() > 0) {
			for (int i = 0; i < shucaiList.size(); i++) {
				sb.append(shucaiList.get(i).getName());
				sb.append(shucaiList.get(i).getCount()+shucaiList.get(i).getUnit());
				sb.append("、");
			}
		}
		if (shuiguoList != null && shuiguoList.size() > 0) {
			for (int i = 0; i < shuiguoList.size(); i++) {
				sb.append(shuiguoList.get(i).getName());
				sb.append(shuiguoList.get(i).getCount()+shuiguoList.get(i).getUnit());
				sb.append("、");
			}
		}
		if (dadouList != null && dadouList.size() > 0) {
			for (int i = 0; i < dadouList.size(); i++) {
				sb.append(dadouList.get(i).getName());
				sb.append(dadouList.get(i).getCount()+dadouList.get(i).getUnit());
				sb.append("、");
			}
		}
		if (ruList != null && ruList.size() > 0) {
			for (int i = 0; i < ruList.size(); i++) {
				sb.append(ruList.get(i).getName());
				sb.append(ruList.get(i).getCount()+ruList.get(i).getUnit());
				sb.append("、");
			}
		}
		if (rouDanList != null && rouDanList.size() > 0) {
			for (int i = 0; i < rouDanList.size(); i++) {
				sb.append(rouDanList.get(i).getName());
				sb.append(rouDanList.get(i).getCount()+rouDanList.get(i).getUnit());
				sb.append("、");
			}
		}
		if (yingguoList != null && yingguoList.size() > 0) {
			for (int i = 0; i < yingguoList.size(); i++) {
				sb.append(yingguoList.get(i).getName());
				sb.append(yingguoList.get(i).getCount()+yingguoList.get(i).getUnit());
				sb.append("、");
			}
		}
		if (youzhiList != null && youzhiList.size() > 0) {
			for (int i = 0; i < youzhiList.size(); i++) {
				sb.append(youzhiList.get(i).getName());
				sb.append(youzhiList.get(i).getCount()+youzhiList.get(i).getUnit());
				sb.append("、");
			}
		}
		MyUtils.showLog("最终结果处理前   "+sb.toString());
		if (sb != null && sb.toString().length()>2&&sb.toString().endsWith("、")) {
			MyUtils.showLog("最终结果处理后   "+sb.toString().substring(0, sb.toString().length() - 1));
			return sb.toString().substring(0, sb.toString().length() - 1);
		}
		return sb.toString();
	}
	
	/**
	 * 显示日历 并返回点击选择的日期 交予 handler 做处理 arg1 月份 arg2 日期
	 * 
	 * @param parent
	 * @param handler
	 */
	public void showCalendar(View parent, final Handler handler) {
		View view = View.inflate(DietActivity2.this, R.layout.layout_calendar,
				null);
		view.startAnimation(AnimationUtils.loadAnimation(DietActivity2.this,
				R.anim.fade_in));
		view.setFocusable(true);
		view.setFocusableInTouchMode(true);
		TextView tv = (TextView) view.findViewById(R.id.calendar_tips);
		tv.setText("点击选择今天以前任意一天（默认今天），查看当天之前7天以内的饮食摄入糖分、热量曲线。");

		final PopupWindow popupWindow = new PopupWindow(view,
				LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		popupWindow.setBackgroundDrawable(new PaintDrawable());
		// 设置点击窗口外边窗口消失
		popupWindow.setOutsideTouchable(true);
		// 设置此参数获得焦点，否则无法点击
		popupWindow.setFocusable(true);
		popupWindow.showAsDropDown(parent, 0, 10);
		popupWindow.update();
		view.setOnKeyListener(new OnKeyListener() {

			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (keyCode == KeyEvent.KEYCODE_BACK) {
					popupWindow.dismiss();
					return true;
				}
				return false;
			}
		});
		final RecordCalendar calendar = (RecordCalendar) view
				.findViewById(R.id.popupwindow_calendar);

		// 监听所选中的日期
		calendar.setOnCalendarClickListener(new OnCalendarClickListener() {

			public void onCalendarClick(int row, int col, String dateFormat) {
				MyUtils.showLog("", "点击选择 " + dateFormat);
				String[] split = dateFormat.split("-");
				int year = Integer.valueOf(split[0]);
				int month = Integer.valueOf(split[1]);
				int date = Integer.valueOf(split[2]);

				if (calendar.getCalendarMonth() - month == 1// 跨年跳转
						|| calendar.getCalendarMonth() - month == -11) {
					calendar.lastMonth();

				} else if (month - calendar.getCalendarMonth() == 1 // 跨年跳转
						|| month - calendar.getCalendarMonth() == -11) {

					calendar.nextMonth();

				} else {
					if (TimeUtil.getInstance().afterToday(year, month, date, 0,
							0, null)) {
						ToastUtil.showToast("无法选择今天之后的日期");
					} else {
						calendar.removeAllBgColor();
						calendar.setCalendarDayBgColor(dateFormat,
								R.drawable.calendar_date_focused);
						Bundle data = new Bundle();
						data.putInt("year", year);
						data.putInt("month", month);
						data.putInt("date", date);
						Message msg = handler.obtainMessage();
						msg.setData(data);
						msg.sendToTarget();
						popupWindow.dismiss();
					}
				}
			}
		});

		// 监听当前月份
		calendar.setOnCalendarDateChangedListener(new OnCalendarDateChangedListener() {
			public void onCalendarDateChanged(int year, int month) {

			}
		});

	}

	public void findViewById() {
		rightBtn = getRightButtonText(getResources().getString(R.string.date));
		leftBtn = getLeftButtonText(getResources().getString(
				R.string.basic_back));
		setTitle(getResources().getString(R.string.diet_title));

		// tv_menu = (TextView) findViewById(R.id.diet_menu);

		_layout_doc = (RelativeLayout) findViewById(R.id.diet_doc_layout);
		_layout_record = (LinearLayout) findViewById(R.id.diet_record_layout);
		rg_time = (RadioGroup) findViewById(R.id.diet_rg);

		_webView = (WebView) findViewById(R.id.webView);
		_webView.setBackgroundColor(Color.TRANSPARENT);
		_webView.setVisibility(View.INVISIBLE);

		_progressBarImageView = (ImageView) findViewById(R.id.progressBarImageView);
		_chartTextView = (TextView) findViewById(R.id.textView);
		_chartTextView.setText("加载中...");
		_chartTextView.setVisibility(View.INVISIBLE);
		_chartTextView.setTextColor(Color.WHITE);

		rightBtn.setOnClickListener(this);
		leftBtn.setOnClickListener(this);
		rg_time.setOnCheckedChangeListener(this);

		_layout_doc.setOnClickListener(this);
		_layout_record.setOnClickListener(this);
		_chartTextView.setOnClickListener(this);

		tv_meal_type = (TextView) findViewById(R.id.diet_detail_tv_meal_type);
		tv_record_time = (TextView) findViewById(R.id.diet_detail_tv_record_time);
		tv_mealDescribe = (TextView) findViewById(R.id.diet_detail_tv_describe);
		tv_add = (TextView) findViewById(R.id.diet_detail_tv_add);
		tv_hot = (TextView) findViewById(R.id.diet_detail_tv_hot_content);
		mealImg = (ImageView) findViewById(R.id.diet_detail_img);
		layout_meal = (RelativeLayout) findViewById(R.id.diet_detail_layout);

		extra_tv_meal_type = (TextView) findViewById(R.id.diet_extra_tv_meal_type);
		extra_tv_record_time = (TextView) findViewById(R.id.diet_extra_tv_record_time);
		extra_tv_mealDescribe = (TextView) findViewById(R.id.diet_extra_tv_describe);
		extra_tv_add = (TextView) findViewById(R.id.diet_extra_tv_add);
		extra_tv_hot = (TextView) findViewById(R.id.diet_extra_tv_hot_content);
		extra_mealImg = (ImageView) findViewById(R.id.diet_extra_img);
		extra_layout_meal = (RelativeLayout) findViewById(R.id.diet_extra_layout);

		tv_add.setOnClickListener(this);
		extra_tv_add.setOnClickListener(this);
		layout_meal.setOnClickListener(this);
		extra_layout_meal.setOnClickListener(this);

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			Intent intent) {
		super.onActivityResult(requestCode, resultCode, intent);
		if (resultCode != RESULT_OK) {
			return;
		}
		if (!falg) {
			if (requestCode == 200) {
				MyUtils.showLog(" 返回到DietActivity  获取数据 "
						+ intent.getParcelableExtra("data").toString());
				startGetChartData();
				MyUtils.showLog("dietActivity 重新请求曲线图 ");
				DietBean dietBean = intent.getParcelableExtra("data");
				Integer type = dietBean.getType();

				switch (type) {
				case 1: {

				}
					break;
				case 4: {

				}
					break;
				case 2: {

				}
					break;
				case 5: {

				}
					break;
				case 3: {

				}
					break;
				case 6: {

				}
					break;
				default:
					break;
				}
			}
		}
	}

}
