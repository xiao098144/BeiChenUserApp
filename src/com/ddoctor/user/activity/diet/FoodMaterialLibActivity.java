package com.ddoctor.user.activity.diet;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;

import com.beichen.user.R;
import com.ddoctor.user.activity.BaseFragmentActivity;
import com.ddoctor.user.adapter.TypeIndicatorPagerAdapter;
import com.ddoctor.user.fragment.FoodMaterailItemFragment;
import com.ddoctor.user.model.FMLibItemBean;
import com.ddoctor.utils.ToastUtil;
import com.pageindicator.TabPageIndicator;
import com.umeng.analytics.MobclickAgent;
import com.zhuge.analysis.stat.ZhugeSDK;

public class FoodMaterialLibActivity extends BaseFragmentActivity {

	private Button leftBtn;
	private Button rigthBtn;
	private FrameLayout top;
	private TabPageIndicator indicator;
	private ViewPager viewPager;

	private TypeIndicatorPagerAdapter adapter;

	private String[] tabTitle;
	private List<Fragment> _fragmentList = new ArrayList<Fragment>();
	private FoodMaterailItemFragment fragment_rice = new FoodMaterailItemFragment();
	private FoodMaterailItemFragment fragment_greens = new FoodMaterailItemFragment();
	private FoodMaterailItemFragment fragment_meat = new FoodMaterailItemFragment();
	private FoodMaterailItemFragment fragment_fruit = new FoodMaterailItemFragment();
	private FoodMaterailItemFragment fragment_drink = new FoodMaterailItemFragment();

	private ArrayList<FMLibItemBean> riceList = new ArrayList<FMLibItemBean>();
	private ArrayList<FMLibItemBean> greensList = new ArrayList<FMLibItemBean>();
	private ArrayList<FMLibItemBean> meatList = new ArrayList<FMLibItemBean>();
	private ArrayList<FMLibItemBean> fruitList = new ArrayList<FMLibItemBean>();
	private ArrayList<FMLibItemBean> drinkList = new ArrayList<FMLibItemBean>();

	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onResume(FoodMaterialLibActivity.this);
		ZhugeSDK.getInstance().init(getApplicationContext());
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPause(FoodMaterialLibActivity.this);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_typeindicatorlist);
		initData();
		findViewById();
	}

	private void initData() {
		tabTitle = getResources().getStringArray(R.array.foodmaterial_type);

		_fragmentList.add(fragment_rice);
		_fragmentList.add(fragment_greens);
		_fragmentList.add(fragment_meat);
		_fragmentList.add(fragment_fruit);
		_fragmentList.add(fragment_drink);
		for (int i = 0; i < 50; i++) {
			Random rand = new Random();
			FMLibItemBean itemBean = new FMLibItemBean();
			itemBean.setSugarContent(20 + i);
			itemBean.setHotContent(120 + i);
			itemBean.setRating(rand.nextFloat() * 5);
			if (i % 5 == 0) {
				itemBean.setPicUrl("0");
				riceList.add(itemBean);
			} else if (i % 5 == 1) {
				itemBean.setPicUrl("1");
				greensList.add(itemBean);
			} else if (i % 5 == 2) {
				itemBean.setPicUrl("2");
				meatList.add(itemBean);
			} else if (i % 5 == 3) {
				itemBean.setPicUrl("3");
				fruitList.add(itemBean);
			} else {
				itemBean.setPicUrl("4");
				drinkList.add(itemBean);
			}
		}

		Bundle data = new Bundle();
		data.putString("title", tabTitle[0]);
		data.putInt("type", 0);
		data.putParcelableArrayList("data", riceList);
		fragment_rice.setArguments(data);
		data = new Bundle();
		data.putString("title", tabTitle[1]);
		data.putInt("type", 1);
		data.putParcelableArrayList("data", greensList);
		fragment_greens.setArguments(data);
		data = new Bundle();
		data.putString("title", tabTitle[2]);
		data.putInt("type", 2);
		data.putParcelableArrayList("data", meatList);
		fragment_meat.setArguments(data);
		data = new Bundle();
		data.putString("title", tabTitle[3]);
		data.putInt("type", 3);
		data.putParcelableArrayList("data", fruitList);
		fragment_fruit.setArguments(data);
		data = new Bundle();
		data.putString("title", tabTitle[4]);
		data.putInt("type", 4);
		data.putParcelableArrayList("data", drinkList);
		fragment_drink.setArguments(data);
	}

	protected void initTitle() {

		leftBtn = getLeftButton();
		rigthBtn = getRightButton();
		rigthBtn.setVisibility(View.VISIBLE);
		leftBtn.setVisibility(View.VISIBLE);
		leftBtn.setText(getResources().getString(R.string.basic_back));
		rigthBtn.setText(getResources().getString(R.string.basic_more));
		this.setTitle(getResources()
				.getString(R.string.diet_foodmaterial_title));
	}

	public void findViewById() {
		initTitle();
		top = (FrameLayout) findViewById(R.id.typeindicator_top);
		top.setBackgroundColor(getResources().getColor(R.color.color_diet_title));
		indicator = (TabPageIndicator) findViewById(R.id.type_indicator);
		indicator.setSelectedColor(getResources().getColor(R.color.color_diet_title));
		viewPager = (ViewPager) findViewById(R.id.pager);
		adapter = new TypeIndicatorPagerAdapter(tabTitle, _fragmentList,
				getSupportFragmentManager());

		viewPager.setAdapter(adapter);
		indicator.setViewPager(viewPager);
		setListener();
	}

	protected void setListener() {
		leftBtn.setOnClickListener(this);
		rigthBtn.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.btn_left: {
			finishThisActivity();
		}
			break;
		case R.id.btn_right: {
			ToastUtil.showToast("更多");
		}
			break;
		default:
			break;
		}
	}

}
