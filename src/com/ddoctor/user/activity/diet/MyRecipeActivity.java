package com.ddoctor.user.activity.diet;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.user.activity.BaseActivity;
import com.ddoctor.user.view.CircleImageView;
import com.ddoctor.utils.ToastUtil;
import com.umeng.analytics.MobclickAgent;
import com.zhuge.analysis.stat.ZhugeSDK;

public class MyRecipeActivity extends BaseActivity{

	private Button leftBtn;
	private Button rightBtn;
	private TextView tv_tips;
	private CircleImageView img_rice;
	private CircleImageView img_grains;
	private CircleImageView img_greens;
	private CircleImageView img_meat;
	private CircleImageView img_fruit;
	private CircleImageView img_drink;
	private TextView tv_rice;
	private TextView tv_grains;
	private TextView tv_greens;
	private TextView tv_meat;
	private TextView tv_fruit;
	private TextView tv_drink;
	private ImageButton img_change;
	private ImageView img_sc1;
	private TextView tv_sc_name1;
	private Button btn_sc1;
	private ImageView img_sc2;
	private TextView tv_sc_name2;
	private Button btn_sc2;
	private ImageView img_sc3;
	private TextView tv_sc_name3;
	private Button btn_sc3;

	private String notice ="您已摄入过多的糖分和热量，为了您的身体健康，我们为您配置适合您的食材。"; 
	private String clickArea;
	
	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("MyRecipeActivity");
		MobclickAgent.onResume(MyRecipeActivity.this);
		ZhugeSDK.getInstance().init(getApplicationContext());
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("MyRecipeActivity");
		MobclickAgent.onPause(MyRecipeActivity.this);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_recipe);
		clickArea = getResources().getString(R.string.clickArea);
		findViewById();
		showDataUI();
	}

	private void showDataUI(){
		changeNotice(notice);
	}
	
	public void findViewById() {
		rightBtn = getRightButtonText(getString(R.string.diet_food_lib));
		leftBtn = getLeftButtonText(getString(R.string.basic_back));
		setTitle(getResources().getString(R.string.diet_recipe_title));
		
		tv_tips = (TextView) findViewById(R.id.recipe_tv_tips);
		tv_tips.setMovementMethod(LinkMovementMethod.getInstance());
		img_rice = (CircleImageView) findViewById(R.id.recipe_img_rice);
		img_grains = (CircleImageView) findViewById(R.id.recipe_img_grains);
		img_greens = (CircleImageView) findViewById(R.id.recipe_img_greens);
		img_meat = (CircleImageView) findViewById(R.id.recipe_img_meat);
		img_fruit = (CircleImageView) findViewById(R.id.recipe_img_fruit);
		img_drink = (CircleImageView) findViewById(R.id.recipe_img_drink);
		tv_rice = (TextView) findViewById(R.id.recipe_tv_rice);
		tv_grains = (TextView) findViewById(R.id.recipe_tv_grains);
		tv_greens = (TextView) findViewById(R.id.recipe_tv_greens);
		tv_meat = (TextView) findViewById(R.id.recipe_tv_meat);
		tv_fruit = (TextView) findViewById(R.id.recipe_tv_fruit);
		tv_drink = (TextView) findViewById(R.id.recipe_tv_drink);
		img_change = (ImageButton) findViewById(R.id.recipe_img_btn_change);
		img_sc1 = (ImageView) findViewById(R.id.recipe_sc_recommend_img1);
		img_sc2 = (ImageView) findViewById(R.id.recipe_sc_recommend_img2);
		img_sc3 = (ImageView) findViewById(R.id.recipe_sc_recommend_img3);
		tv_sc_name1 = (TextView) findViewById(R.id.recipe_sc_recommend_tv_name1);
		tv_sc_name2 = (TextView) findViewById(R.id.recipe_sc_recommend_tv_name2);
		tv_sc_name3 = (TextView) findViewById(R.id.recipe_sc_recommend_tv_name3);
		btn_sc1 = (Button) findViewById(R.id.recipe_sc_recommend_btn1);
		btn_sc2 = (Button) findViewById(R.id.recipe_sc_recommend_btn2);
		btn_sc3 = (Button) findViewById(R.id.recipe_sc_recommend_btn3);

		leftBtn.setOnClickListener(this);
		rightBtn.setOnClickListener(this);
		img_change.setOnClickListener(this);
		btn_sc1.setOnClickListener(this);
		btn_sc2.setOnClickListener(this);
		btn_sc3.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.btn_left: {
			finishThisActivity();
		}
			break;
		case R.id.btn_right: {
			Intent intent = new Intent();
			intent.setClass(MyRecipeActivity.this, FoodMaterialLibActivity.class);
			startActivity(intent);
		}
			break;
		case R.id.recipe_img_btn_change: {
			ToastUtil.showToast("更换推荐食谱");
		}
			break;
		case R.id.recipe_sc_recommend_btn1: {
			ToastUtil.showToast("点击查看推荐商品1");
		}
			break;
		case R.id.recipe_sc_recommend_btn2: {
			ToastUtil.showToast("点击查看推荐商品2");
		}
			break;
		case R.id.recipe_sc_recommend_btn3: {
			ToastUtil.showToast("点击查看推荐商品3");
		}
			break;
		default:
			break;
		}
	}

	private ClickableSpan clickableSpan = new ClickableSpan() {

		@Override
		public void onClick(View widget) {
			ToastUtil.showToast("点击小贴士 查看更多");
		}
	};

	private void changeNotice(String notice) {
		SpannableStringBuilder str = new SpannableStringBuilder(notice);
		str.append(clickArea);
		str.setSpan(new UnderlineSpan(), str.length() - clickArea.length(),
				str.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		str.setSpan(clickableSpan, str.length() - clickArea.length(),
				str.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		str.setSpan(new ForegroundColorSpan(Color.RED), str.length()
				- clickArea.length(), str.length(),
				Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		tv_tips.setText(str);
	}
	
}
