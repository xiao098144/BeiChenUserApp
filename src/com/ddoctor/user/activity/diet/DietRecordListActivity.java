package com.ddoctor.user.activity.diet;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.enums.RecordLayoutType;
import com.ddoctor.enums.RefreshAction;
import com.ddoctor.enums.RetError;
import com.ddoctor.interfaces.OnClickCallBackListener;
import com.ddoctor.user.activity.BaseActivity;
import com.ddoctor.user.activity.medicine.MedicineRecordActivity;
import com.ddoctor.user.activity.sport.SportRecordActivity;
import com.ddoctor.user.activity.sugar.BloodSugarRecordListActivity;
import com.ddoctor.user.adapter.DietRecordAdapter;
import com.ddoctor.user.task.GetDietRecordListTask;
import com.ddoctor.user.task.GetRecordDateAndCountTask;
import com.ddoctor.user.task.TaskPostCallBack;
import com.ddoctor.user.view.DDPullToRefreshView;
import com.ddoctor.user.view.DDPullToRefreshView.OnHeaderRefreshListener;
import com.ddoctor.user.wapi.bean.DietBean;
import com.ddoctor.user.wapi.constant.Record;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.DialogUtil;
import com.ddoctor.utils.TimeUtil;
import com.ddoctor.utils.ToastUtil;
import com.umeng.analytics.MobclickAgent;
import com.zhuge.analysis.stat.ZhugeSDK;

/**
 * 
 * @author 萧
 * @Date 2015-5-13下午11:32:57
 * @TODO 饮食记录
 */
public class DietRecordListActivity extends BaseActivity implements OnClickCallBackListener, OnHeaderRefreshListener, OnScrollListener {

	private Button leftBtn;
	private Button rightBtn;
	private TextView tv_title;

	private DietRecordAdapter _adapter;

	private String today, startDate, endDate;
	private String MODULE;
	private static final int DATARouting = -7; // 数据加载周期 即每次加载几天的数据

	private ListView _listView;
	DDPullToRefreshView _refreshViewContainer;
	private View _getMoreView;
	private TextView _tv_norecord;
	 

	private int _pageNum = 1;
	private RefreshAction _refreshAction = RefreshAction.PULLTOREFRESH;
	
	String title1 = "您一共使用了 ";
	String title2 = " 天，记录了 ";
	String title3 = " 次饮食";
	
	private List<DietBean> _dataList = new ArrayList<DietBean>();
	
	private List<DietBean> _resultList = new ArrayList<DietBean>();

	private int title_color;

	private List<DietBean> formatData(List<DietBean> list) {
		List<DietBean> resultList = new ArrayList<DietBean>();
		for (int i = 0; i < list.size(); i++) {
			DietBean dietBean = new DietBean();
			list.get(i).setDate(
					TimeUtil.getInstance().formatDate2(list.get(i).getTime()));
			if (i == 0
					|| (i >= 1 && !list.get(i).getDate()
							.equals(list.get(i - 1).getDate()))) {
				dietBean.setDate(list.get(i).getDate());
				dietBean.setLayoutType(RecordLayoutType.TYPE_CATEGORY);
				resultList.add(dietBean);
			}
			resultList.add(list.get(i));
		}
		return resultList;
	}
	
	private int  fromClass = 0; // 列表切换用  1 血糖列表  2 饮食  3 运动  4 用药
	private void getIntentInfo(){
		fromClass = getIntent().getIntExtra("from", 0);
	}
	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("DietRecordListActivity");
		MobclickAgent.onResume(DietRecordListActivity.this);
		ZhugeSDK.getInstance().init(getApplicationContext());
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("DietRecordListActivity");
		MobclickAgent.onPause(DietRecordListActivity.this);
	}
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_bs_recordlist);
		getIntentInfo();
		findViewById();
		loadingData(true,_pageNum);
		loadingTitle();
	}

	Comparator<DietBean> comparator = new Comparator<DietBean>() {

		@Override
		public int compare(DietBean lhs, DietBean rhs) {
			int compare ;
			compare = lhs.getTime().compareTo(rhs.getTime());
			return 0-compare;
		}
	};
	
	private Dialog _loadingDialog = null;

	private void loadingData(boolean showDialog , int page){
		if (showDialog) {
			_loadingDialog = DialogUtil.createLoadingDialog(this, "正在加载中，请稍候...");
			_loadingDialog.show();
		}
		GetDietRecordListTask task = new GetDietRecordListTask(startDate, endDate, page, 1);
		final int page1 = page;
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {

			@Override
			public void taskFinish(DDResult result) {
				if (result.getError() == RetError.NONE) {
					List<DietBean> tmpList = result.getBundle()
							.getParcelableArrayList("list");
					if (page1 > 1) // 加载更多
					{
						_resultList.addAll(tmpList);
						Collections.sort(_resultList , comparator);
						_dataList.clear();
						_dataList.addAll(formatData(_resultList));
						_adapter.notifyDataSetChanged();
					} else {
						// 加载第一页
						_dataList.clear();
						_resultList.clear();
						_resultList.addAll(tmpList);
						Collections.sort(_resultList , comparator);
						_dataList.addAll(formatData(_resultList));
						_adapter.notifyDataSetChanged();

						_refreshViewContainer.onHeaderRefreshComplete();
						_refreshViewContainer.setVisibility(View.VISIBLE);
						_refreshAction = RefreshAction.NONE;

						if (_loadingDialog != null)
							_loadingDialog.dismiss();

					}

					// 是否显示"加载更多"
					if (tmpList.size() > 0) {
						setGetMoreContent("滑动加载更多", true, false);
						_bGetMoreEnable = true;
					} else {
						if (page1 == 1) {
							_tv_norecord.setText(result.getErrorMessage());
							_tv_norecord.setTag(0);
						}
						// 没数据了，加载完成
						setGetMoreContent("已全部加载", false, false);
						_bGetMoreEnable = false;
					}

					// 确保加载成功后，再修改这个变量
					_pageNum = page1;
				} else {// 加载失败
					if (page1 > 1) {
						setGetMoreContent("滑动加载更多", true, false);
					} else {
						_refreshViewContainer.onHeaderRefreshComplete();
						_refreshAction = RefreshAction.NONE;

						if (_loadingDialog != null)
							_loadingDialog.dismiss();
					}

					ToastUtil.showToast(result.getErrorMessage());
				}

				_refreshAction = RefreshAction.NONE;
			}
		});

		task.executeParallel("");
	}
	
	/**
	 * 获取记录总数
	 */
	private void loadingTitle() {

		GetRecordDateAndCountTask task = new GetRecordDateAndCountTask(
				Record.DIET_RECORD);
		task.setTaskCallBack(new TaskPostCallBack<RetError>() {

			@Override
			public void taskFinish(RetError result) {
				if (RetError.NONE == result) {
					Bundle data = result.getBundle();
					int date = data.getInt("date");
					int count = (int) data.getFloat("count");
					updateTitle(date, count);
				} else {
					tv_title.setVisibility(View.GONE);
				}
			}
		});
		task.executeParallel("");

	}
	
	private void updateTitle(int date, int count) {
		SpannableStringBuilder str = new SpannableStringBuilder(title1);
		String datestr = date+"";
		str.append(datestr);
		str.append(title2);
		String countStr = count + "";
		str.append(countStr);
		str.append(title3);
		str.setSpan(new ForegroundColorSpan(title_color), title1.length(),
				title1.length()+datestr.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		str.setSpan(new ForegroundColorSpan(title_color), title1.length() +datestr.length()
				+ title2.length(), title1.length() + title2.length()+datestr.length()
				+ countStr.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		tv_title.setText(str);
		tv_title.setVisibility(View.VISIBLE);
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.btn_left: {
			finishThisActivity();
		}
			break;
		case R.id.btn_right: {
			DialogUtil.changeRecordList(DietRecordListActivity.this,
					rightBtn, this);
		}
			break;
		default:
			break;
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode != RESULT_OK) {
			return;
		}
		if (requestCode == 200) {
			DietBean dietBean = data.getParcelableExtra("data");
			for (int i = 0; i < _resultList.size(); i++) {
				if (dietBean.getId().equals(_resultList.get(i).getId())) {
					_resultList.set(i, dietBean);
					break;
				}else {
					continue;
				}
			}
			Collections.sort(_resultList, comparator);
			_dataList.clear();
			_dataList.addAll(formatData(_resultList));
			_adapter.notifyDataSetChanged();
		}
	}

	protected void findViewById() {
		MODULE = getResources().getString(R.string.time_format_10);
		today = TimeUtil.getInstance().getStandardDate(MODULE);
		endDate = TimeUtil.getInstance().dateAddFrom(1, today, MODULE,Calendar.DATE);
		startDate = TimeUtil.getInstance().dateAddFrom(DATARouting, endDate,MODULE,Calendar.DATE);

		title_color = getResources().getColor(R.color.color_diet_title);
		RelativeLayout rl = (RelativeLayout) findViewById(R.id.titleBar);
		if (rl != null) {
			rl.setBackgroundColor(title_color);
		}
		leftBtn = getLeftButton();
		rightBtn = getRightButton();
		rightBtn.setText(getResources().getString(R.string.basic_change));
		leftBtn.setText(getResources().getString(R.string.basic_back));
		this.setTitle(getResources().getString(R.string.diet_record));
		
		tv_title = (TextView) findViewById(R.id.recordlist_title);
	
		leftBtn.setOnClickListener(this);
		rightBtn.setOnClickListener(this);
		
		_refreshViewContainer = (DDPullToRefreshView) findViewById(R.id.refreshViewContainer);
		_refreshViewContainer.setOnHeaderRefreshListener(this);
		_refreshViewContainer.setVisibility(View.INVISIBLE);

		_listView = (ListView) findViewById(R.id.listView);
		_listView.setOnScrollListener(this);
		_tv_norecord = (TextView) findViewById(R.id.tv_norecord);
		_listView.setEmptyView(_tv_norecord);

		initList();

	}

	private void initList() {
		// 获取更多
		_getMoreView = createGetMoreView();
		setGetMoreContent("已全部加载", false, false);
		_listView.addFooterView(_getMoreView);

		// 数据
		_adapter = new DietRecordAdapter(this);
		_listView.setAdapter(_adapter);
		_adapter.setData(_dataList);
	}

	// 加载更多相关函数 >>>>>>
	boolean _bGetMoreEnable = false;

	private View createGetMoreView() {
		if (_getMoreView != null)
			return _getMoreView;

		View v = (View) getLayoutInflater().inflate(R.layout.refresh_footer,
				null);

		return v;
	}

	private void setGetMoreContent(String message, boolean showImage,
			boolean animation) {
		TextView tv = (TextView) _getMoreView
				.findViewById(R.id.pull_to_load_text);
		tv.setText(message);

		ImageView imgView = (ImageView) _getMoreView
				.findViewById(R.id.pull_to_load_image);
		AnimationDrawable ad = (AnimationDrawable) imgView.getBackground();
		if (showImage) {
			if (animation) {
				ad.start();
			} else {
				ad.stop();
				ad.selectDrawable(0);
			}

			imgView.setVisibility(View.VISIBLE);
		} else {
			ad.stop();
			ad.selectDrawable(0);

			imgView.setVisibility(View.GONE);
		}
	}

	@Override
	public void onScroll(AbsListView arg0, int firstVisibleItem, int arg2,
			int arg3) {

		// 工具栏的显示与隐藏
		if (_refreshAction == RefreshAction.NONE) {
			if (_bGetMoreEnable) {// 有加载更多
				int lastPos = _listView.getLastVisiblePosition();
				int total = _listView.getHeaderViewsCount() + _dataList.size()
						+ _listView.getFooterViewsCount();
				if (lastPos == total - 1) {
					// 加载更多显示出来了，开始加载
					_refreshAction = RefreshAction.LOADMORE;
					setGetMoreContent("正在加载...", true, true);
					loadingData(false, _pageNum + 1);
				}
			}
		}

	}

	@Override
	public void onScrollStateChanged(AbsListView arg0, int arg1) {

	}

	@Override
	public void onHeaderRefresh(DDPullToRefreshView view) {
		if (_refreshAction == RefreshAction.NONE) {
			_refreshAction = RefreshAction.PULLTOREFRESH;
			loadingData(false, 1);
		} else {
			// 正在加载，什么也不做
			view.onHeaderRefreshComplete();
		}
	}
	

	@Override
	public void onClickCallBack(Bundle data) {
		int type = data.getInt("type");
		switch (type) {
		case 1: {
			if (fromClass == 1) {
				finishThisActivity();
			} else {
				skip("from", 2, BloodSugarRecordListActivity.class, false);
			}
		}
			break;
		case 3: {
			if (fromClass == 3) {
				finishThisActivity();
			} else {
				skip("from", 2, SportRecordActivity.class, false);
			}
		}
			break;
		case 4: {
			if (fromClass == 4) {
				finishThisActivity();
			} else {
				skip("from", 2, MedicineRecordActivity.class, false);
			}

		}
			break;
		default:
			break;
		}
	}

}
