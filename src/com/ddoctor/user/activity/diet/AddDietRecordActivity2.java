package com.ddoctor.user.activity.diet;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Base64;
import android.util.SparseArray;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.enums.RetError;
import com.ddoctor.interfaces.OnClickCallBackListener;
import com.ddoctor.user.activity.BaseActivity;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.data.MyProfile;
import com.ddoctor.user.model.EnergyBean;
import com.ddoctor.user.task.FileUploadTask;
import com.ddoctor.user.task.GetDietDetailTask;
import com.ddoctor.user.task.ManagerDietRecordTask;
import com.ddoctor.user.task.TaskPostCallBack;
import com.ddoctor.user.wapi.bean.DietBean;
import com.ddoctor.user.wapi.bean.FoodBean;
import com.ddoctor.user.wapi.bean.FoodNutritionBean;
import com.ddoctor.user.wapi.bean.UploadBean;
import com.ddoctor.user.wapi.constant.FoodType;
import com.ddoctor.user.wapi.constant.Upload;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.DialogUtil;
import com.ddoctor.utils.ImageLoaderUtil;
import com.ddoctor.utils.MyUtils;
import com.ddoctor.utils.StringUtils;
import com.ddoctor.utils.TimeUtil;
import com.ddoctor.utils.ToastUtil;
import com.umeng.analytics.MobclickAgent;

public class AddDietRecordActivity2 extends BaseActivity implements
		OnClickCallBackListener {

	private TextView _tv_bound, _tv_value;
	private ImageView _img_loc, _img_back;
	private int back_length, loc_length, bound_length;

	private int BoundValue = 2000, UpValue = 5000;
	private boolean _canMove = true; // 能量值达到最上限时不再计算左边距 不再移动 默认为true false即不能移动

	private Button _btn_add;
	private LinearLayout _record_layout;

	private EditText _et_remark;
	private ImageButton imgBtnAdd;

	private HorizontalScrollView _hsc;
	private ImageView[] _img = new ImageView[9];
	private ArrayList<String> _urlList = new ArrayList<String>();

	private ArrayList<FoodBean> _resultList = new ArrayList<FoodBean>();

	private Integer mealType = 1; // 记录类型 早午晚、三餐加餐 默认早餐

	private DietBean sourceBean;
	private int _dietId = 0;
	private boolean isUpdate = false;
	private String[] _typeArray;
	private Map<String, FoodNutritionBean[]> _nameArray;

	private void getIntentInfo() {
		Intent intent = getIntent();
		if (null != intent) {
			Bundle data = intent.getBundleExtra("data");
			if (null != data) {
				_dietId = data.getInt("id", 0);
				mealType = data.getInt("type", 1);
				MyUtils.showLog("", "接收到的数据  " + data.toString());
			}
		}
	}

	private boolean _isDataError = false; // 是否食物字典数据异常

	private void initData() {
		List<FoodNutritionBean> list = DataModule.loadDict(MyProfile.DICT_FOOD,
				FoodNutritionBean.class);
		if (list == null || list.size() == 0) {
			_isDataError = true;
		}
		if (!_isDataError) {
			String[] typeArray = getResources().getStringArray(
					R.array.foodmaterial_type);
			List<String> typeList = new ArrayList<String>();
			_nameArray = new HashMap<String, FoodNutritionBean[]>(
					typeArray.length);
			SparseArray<List<FoodNutritionBean>> nameList = new SparseArray<List<FoodNutritionBean>>();
			for (int i = 0; i < typeArray.length; i++) {
				List<FoodNutritionBean> list3 = new ArrayList<FoodNutritionBean>();
				nameList.append(i, list3);
			}

			for (int i = 0; i < list.size(); i++) {
				Integer foodType = list.get(i).getFoodType();
				if (foodType == null) {
					foodType = 1;
				}
				nameList.get(foodType - 1).add(list.get(i));
			}
			for (int i = 0; i < nameList.size(); i++) {
				List<FoodNutritionBean> list2 = nameList.get(i);
				if (list2 != null && list2.size() > 0) {
					FoodNutritionBean[] fb = new FoodNutritionBean[list2.size()];
					list2.toArray(fb);
					_nameArray.put(typeArray[i], fb);
					typeList.add(typeArray[i]);
				}
			}
			_typeArray = new String[typeList.size()];
			typeList.toArray(_typeArray);
		}
	}

	private String[] meal;

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.putInt("id", _dietId);
		outState.putInt("type", mealType);
		outState.putParcelable("data", sourceBean);
		outState.putStringArrayList("url", _urlList);
		outState.putParcelableArrayList("result", _resultList);
		outState.putFloat("energy", _energy);
		outState.putFloat("protein", _protein);
		outState.putFloat("fat", _fat);
		outState.putFloat("_currentEnergy", _currentEnergy);
		outState.putSparseParcelableArray("energyList", _energyList);
		outState.putInt("back_length", back_length);
		outState.putInt("loc_length", loc_length);
		outState.putInt("bound_length", bound_length);
		// outState.putInt("leftMargin",leftMargin);
		super.onSaveInstanceState(outState);
		MyUtils.showLog("AddDietRecordActivity onSaveInstanceState 页面被销毁  保存数据   "
				+ outState.toString());
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		MyUtils.showLog("AdddietRecordActivty  onRestoreInstanceState 销毁重建  "
				+ savedInstanceState.toString());
		super.onRestoreInstanceState(savedInstanceState);
	}

	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("AddDietRecordActivity");
		MobclickAgent.onResume(AddDietRecordActivity2.this);
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("AddDietRecordActivity");
		MobclickAgent.onPause(AddDietRecordActivity2.this);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (null != savedInstanceState) {
			MyUtils.showLog("AdddietRecordActivity  onCreate 页面被销毁后自动重建   "
					+ savedInstanceState.toString());
			_dietId = savedInstanceState.getInt("id", 0);
			mealType = savedInstanceState.getInt("type");
			sourceBean = savedInstanceState.getParcelable("data");
			_urlList = savedInstanceState.getStringArrayList("url");
			_currentImgIndex = _urlList.size();

			_resultList = savedInstanceState.getParcelableArrayList("result");

			_energy = savedInstanceState.getFloat("energy", 0f);
			_protein = savedInstanceState.getFloat("protein", 0f);
			_fat = savedInstanceState.getFloat("fat", 0f);
			_currentEnergy = savedInstanceState.getFloat("_currentEnergy", 0f);
			_energyList = savedInstanceState
					.getSparseParcelableArray("energyList");
			back_length = savedInstanceState.getInt("back_length", 0);
			loc_length = savedInstanceState.getInt("loc_length", 0);
			bound_length = savedInstanceState.getInt("bound_length", 0);
			// leftMargin = savedInstanceState.getInt("leftMargin",0);

		}
		setContentView(R.layout.act_add_dietrecord2);
		getIntentInfo();
		initData();
		findViewById();
		if (_dietId != 0) {
			getDietDetail();
			isUpdate = true;
			// showData();
		}
		if (_urlList.size() > 0) {
			for (int i = 0; i < _urlList.size(); i++) {
				_img[i].setVisibility(View.VISIBLE);
				ImageLoaderUtil.display(_urlList.get(i), _img[i],
						R.drawable.rc_image_download_failure);
			}
		}
		if (_energy > 0) {
			if (_energy >= UpValue) {
				_canMove = false;
			} else {
				_canMove = true;
			}
			MyUtils.showLog(" onCreate 页面重建   _energy  " + _energy
					+ " _canMove = " + _canMove);
			_tv_value.setText(String.format(Locale.CHINESE, "%dkcal",
					(int) Math.ceil(_energy)));
			calculateLength();
		}
		if (_resultList.size() > 0) {
			showResult();
		}

	}

	private DietBean dietBean;

	/**
	 * 提交前检测数据
	 * 
	 * @return
	 */
	private boolean checkData() {
		dietBean = new DietBean();
		List<FoodBean> daDouList = new ArrayList<FoodBean>();
		List<FoodBean> guShuList = new ArrayList<FoodBean>();
		List<FoodBean> shuCaiList = new ArrayList<FoodBean>();
		List<FoodBean> shuiGuoList = new ArrayList<FoodBean>();
		List<FoodBean> ruList = new ArrayList<FoodBean>();
		List<FoodBean> rouDanList = new ArrayList<FoodBean>();
		List<FoodBean> yingGuoList = new ArrayList<FoodBean>();
		List<FoodBean> youZhiList = new ArrayList<FoodBean>();

		if (!_isDataError) {
			if (_energy == 0) {
				ToastUtil.showToast("请添加食物");
				return false;
			}

			if (_resultList != null && _resultList.size() > 0) {
				for (int i = 0; i < _resultList.size(); i++) {
					FoodBean fb = _resultList.get(i);
					switch (fb.getType()) {
					case FoodType.CATEGORY_GUSHU: {
						guShuList.add(fb);
					}
						break;
					case FoodType.CATEGORY_SHUCAI: {
						shuCaiList.add(fb);
					}
						break;
					case FoodType.CATEGORY_SHUIGUO: {
						shuiGuoList.add(fb);
					}
						break;
					case FoodType.CATEGORY_DADOU: {
						daDouList.add(fb);
					}
						break;
					case FoodType.CATEGORY_RULEI: {
						ruList.add(fb);
					}
						break;
					case FoodType.CATEGORY_ROUDAN: {
						rouDanList.add(fb);
					}
						break;
					case FoodType.CATEGORY_YINGGUO: {
						yingGuoList.add(fb);
					}
						break;
					case FoodType.CATEGORY_YOUZHI: {
						youZhiList.add(fb);
					}
						break;

					default:
						break;
					}
				}
			} else {
				ToastUtil.showToast("");
			}
		}
		dietBean.setDaDouList(daDouList);
		dietBean.setGuShuList(guShuList);
		dietBean.setShuCaiList(shuCaiList);
		dietBean.setShuiGuoList(shuiGuoList);
		dietBean.setRuList(ruList);
		dietBean.setRouDanList(rouDanList);
		dietBean.setYingGuoList(yingGuoList);
		dietBean.setYouZhiList(youZhiList);
		dietBean.setHeat((int) Math.ceil(_energy));
		dietBean.setSugar(0);

		dietBean.setId(_dietId);

		dietBean.setName(meal[mealType - 1]);
		dietBean.setType(mealType);

		String fileUrl = "";
		// if (_urlList==null || _urlList.size() ==0) {
		// ToastUtil.showToast("请上传至少一场饮食图片");
		// return false;
		// }
		if (_urlList != null && _urlList.size() > 0) {
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < _urlList.size(); i++) {
				sb.append(_urlList.get(i));
				sb.append("|");
			}
			fileUrl = sb.toString();
		}
		if (fileUrl != null && fileUrl.endsWith("|")) {
			fileUrl = fileUrl.substring(0, fileUrl.length() - 1);
		}

		dietBean.setFile(fileUrl);
		if (_isDataError) {
			String remark = _et_remark.getText().toString().trim();

			if (TextUtils.isEmpty(remark) && TextUtils.isEmpty(fileUrl)) {
				ToastUtil.showToast("饮食记录不能为空，请填写您的饮食备注或者上传至少一张图片");
				return false;
			}
		}
		dietBean.setRemark(_et_remark.getText().toString().trim());

		dietBean.setTime(TimeUtil.getInstance().getStandardDate(
				getString(R.string.time_format_19)));

		return true;
	}

	private void getDietDetail() {
		_loadingDialog = DialogUtil.createLoadingDialog(
				AddDietRecordActivity2.this, "正在加载中，请稍候... ");
		_loadingDialog.show();

		GetDietDetailTask task = new GetDietDetailTask(_dietId);
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {

			@Override
			public void taskFinish(DDResult result) {
				_loadingDialog.dismiss();
				if (result.getError() == RetError.NONE) {
					sourceBean = result.getBundle().getParcelable("data");
					if (sourceBean != null) {
						showData();
					}
				} else {
					ToastUtil.showToast(result.getErrorMessage());
				}
			}
		});
		task.executeParallel("");
	}

	private void formatList(ArrayList<FoodBean> list, int type) {
		for (int i = 0; i < list.size(); i++) {
			list.get(i).setType(type);
			switch (type) {
			case 1: {
				list.get(i).setTypeName("谷薯类");
			}
				break;
			case 2: {
				list.get(i).setTypeName("蔬菜类");
			}
				break;
			case 3: {
				list.get(i).setTypeName("水果类");
			}
				break;
			case 4: {
				list.get(i).setTypeName("大豆类");
			}
				break;
			case 5: {
				list.get(i).setTypeName("乳类");
			}
				break;
			case 6: {
				list.get(i).setTypeName("肉蛋类");
			}
				break;
			case 7: {
				list.get(i).setTypeName("硬果类");
			}
				break;
			case 8: {
				list.get(i).setTypeName("油脂类");
			}
				break;

			default:
				break;
			}

		}
	}

	protected void showData() {
		String file = sourceBean.getFile(); // 显示图片
		if (!TextUtils.isEmpty(file)) {
			String[] imgUrl = file.split("\\|");
			if (imgUrl.length > 0) {
				_currentImgIndex = imgUrl.length;
				_hsc.setVisibility(View.VISIBLE);
				for (int i = 0; i < imgUrl.length; i++) {
					_urlList.add(imgUrl[i]);
					_img[i].setVisibility(View.VISIBLE);
					ImageLoaderUtil.display(
							StringUtils.urlFormatThumbnail(imgUrl[i]), _img[i],
							R.drawable.rc_image_download_failure);
				}
			}
		}
		// 依次取出食物列表 并添加到总的集合列表中
		ArrayList<FoodBean> daDouList = (ArrayList<FoodBean>) sourceBean
				.getDaDouList();
		formatList(daDouList, FoodType.CATEGORY_DADOU);
		_resultList.addAll(daDouList);
		ArrayList<FoodBean> guShuList = (ArrayList<FoodBean>) sourceBean
				.getGuShuList();
		formatList(guShuList, FoodType.CATEGORY_GUSHU);
		_resultList.addAll(guShuList);
		ArrayList<FoodBean> shuCaiList = (ArrayList<FoodBean>) sourceBean
				.getShuCaiList();
		formatList(shuCaiList, FoodType.CATEGORY_SHUCAI);
		_resultList.addAll(shuCaiList);
		ArrayList<FoodBean> shuiGuoList = (ArrayList<FoodBean>) sourceBean
				.getShuiGuoList();
		formatList(shuiGuoList, FoodType.CATEGORY_SHUIGUO);
		_resultList.addAll(shuiGuoList);
		ArrayList<FoodBean> ruList = (ArrayList<FoodBean>) sourceBean
				.getRuList();
		formatList(ruList, FoodType.CATEGORY_RULEI);
		_resultList.addAll(ruList);
		ArrayList<FoodBean> yingGuoList = (ArrayList<FoodBean>) sourceBean
				.getYingGuoList();
		formatList(yingGuoList, FoodType.CATEGORY_YINGGUO);
		_resultList.addAll(yingGuoList);
		ArrayList<FoodBean> rouDanList = (ArrayList<FoodBean>) sourceBean
				.getRouDanList();
		formatList(rouDanList, FoodType.CATEGORY_ROUDAN);
		_resultList.addAll(rouDanList);
		ArrayList<FoodBean> youZhiList = (ArrayList<FoodBean>) sourceBean
				.getYouZhiList();
		formatList(youZhiList, FoodType.CATEGORY_YOUZHI);
		_resultList.addAll(youZhiList);

		Collections.sort(_resultList); // 按类别排序 并显示结果
		showResult();

		_energy = sourceBean.getHeat() == null ? 0 : sourceBean.getHeat(); // 根据摄取食物能量值
																			// 显示在进度条中
		if (_energy > 0) {
			if (_energy >= UpValue) {
				// _canMove = false;
			} else {
				_canMove = true;
			}
			_tv_value.setText(String.format(Locale.CHINESE, "%dkcal",
					(int) (_energy)));
			calculateLength();
		}

		_et_remark.setText(sourceBean.getRemark());
	}

	Dialog _loadingDialog;

	private void managerDietRecord() {
		_loadingDialog = DialogUtil.createLoadingDialog(
				AddDietRecordActivity2.this, "正在加载中，请稍候... ");
		_loadingDialog.show();
		ManagerDietRecordTask task = new ManagerDietRecordTask(dietBean);
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {

			@Override
			public void taskFinish(DDResult result) {
				_loadingDialog.dismiss();
				if (result.getError() == RetError.NONE) {
					if (isUpdate) {
						ToastUtil.showToast("修改成功");
					} else {
						ToastUtil.showToast("保存成功");
					}
					Intent intent = getIntent().putExtra("data", dietBean);
					setResult(RESULT_OK, intent);
					finishThisActivity();
				} else {
					ToastUtil.showToast(result.getErrorMessage());
				}
			}
		});
		task.executeParallel("");
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.btn_left: {
			finishThisActivity();
		}
			break;
		case R.id.btn_right: {
			if (checkData()) {
				managerDietRecord();
			}
		}
			break;
		case R.id.diet_add_record_img_add: {
			if (_currentImgIndex < 8) {
				String[] items = new String[] { "本地图片", "拍照" };
				DialogUtil.createListDialog(AddDietRecordActivity2.this, items,
						new DialogUtil.ListDialogCallback() {

							@Override
							public void onItemClick(int which) {
								if (which == 0) {
									Intent intent = new Intent();
									intent.setType("image/*");
									intent.setAction(Intent.ACTION_GET_CONTENT);
									startActivityForResult(intent, 0);
								} else {
									Intent intent = new Intent(
											MediaStore.ACTION_IMAGE_CAPTURE);
									String filename = DataModule
											.getTakePhotoTempFilename("diet");
									File f = new File(filename);
									if (f.exists())
										f.delete();
									Uri uri = Uri.fromFile(f);
									intent.putExtra(MediaStore.EXTRA_OUTPUT,
											uri);
									startActivityForResult(intent, 1);
								}
							}
						}).show();
			} else {
				ToastUtil.showToast("很抱歉，最多添加九张图片");
			}
		}
			break;
		case R.id.diet_add_record_btn: {
			back_length = _img_back.getWidth();
			loc_length = _img_loc.getWidth();
			bound_length = _tv_bound.getWidth();

			if (!_isDataError) { // 数据正常时允许显示列表对话框
				DialogUtil
						.foodSelectDialog(AddDietRecordActivity2.this,
								_typeArray, _nameArray, getResources()
										.getColor(R.color.color_diet_title),
								this);
			} else {
				ToastUtil.showToast("数据异常，暂时无法添加");
			}
		}
			break;

		default:
			break;
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		MyUtils.showLog("requestCode=" + requestCode + " resultCode="
				+ resultCode);

		if (resultCode != Activity.RESULT_CANCELED) {
			if (requestCode == 0) {// 选择图片
				if (data != null) {
					MyUtils.showLog("  " + data.getData());

					String img_path = MyUtils.getPathFromUri(
							AddDietRecordActivity2.this, data.getData());
					
					try {
						Bitmap bitmap = MyUtils.loadBitmapFromFile(img_path);
						onUploadPhoto(MyUtils.resizeImage(bitmap, 500));
					} catch (Exception e) {
						ToastUtil.showToast("获取图片失败!");
					}
					
				}
			} else if (requestCode == 1) {// 拍照
				String filename = DataModule.getTakePhotoTempFilename("diet");
				File f = new File(filename);
				if (f.exists()) {
					
					try {
						Bitmap bitmap = MyUtils.loadBitmapFromFile(f
								.getAbsolutePath());
						onUploadPhoto(MyUtils.resizeImage(bitmap, 500));
					} catch (Exception e) {
						ToastUtil.showToast("获取图片失败!");
					}
					
				} else
					ToastUtil.showToast("获取图片失败!");
			}
		}

		super.onActivityResult(requestCode, resultCode, data);
	}

	// 上传头像
	private Bitmap _bitmap = null;

	private void onUploadPhoto(Bitmap bmp) {
		if (_bitmap != null) {
			_bitmap.recycle();
			_bitmap = null;
		}
		_bitmap = bmp;

		_loadingDialog = DialogUtil.createLoadingDialog(
				AddDietRecordActivity2.this, "提交中...");
		_loadingDialog.show();

		UploadBean uploadBean = new UploadBean();
		uploadBean.setFileType("jpg");

		uploadBean.setType(Upload.DIET_IMAGE);

		byte[] data = MyUtils.Bitmap2Bytes(bmp);
		String s_data = android.util.Base64
				.encodeToString(data, Base64.DEFAULT);
		uploadBean.setFile(s_data);

		FileUploadTask task = new FileUploadTask(uploadBean);
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {
			@Override
			public void taskFinish(DDResult result) {

				if (_loadingDialog != null) {
					_loadingDialog.dismiss();
					_loadingDialog = null;
				}

				if (result.getError() == RetError.NONE) {
					on_task_finished(result);
				} else {
					on_task_failed(result);
				}

			}
		});
		task.executeParallel("");
	}

	private void on_task_finished(DDResult result) {
		MyUtils.showLog("on_task_finished");
		Bundle b = result.getBundle();
		String fileUrl = b.getString("fileUrl");
		MyUtils.showLog(fileUrl);
		_urlList.add(_currentImgIndex, fileUrl);
		if (_hsc.getVisibility() != View.VISIBLE) {
			_hsc.setVisibility(View.VISIBLE);
		}
		_img[_currentImgIndex].setVisibility(View.VISIBLE);
		ImageLoaderUtil.display(StringUtils.urlFormatThumbnail(fileUrl),
				_img[_currentImgIndex], R.drawable.rc_image_download_failure);
		_currentImgIndex++;
		if (_currentImgIndex == 8) {
			imgBtnAdd.setVisibility(View.GONE);
		}
	}

	private int _currentImgIndex = 0;

	private void on_task_failed(DDResult result) {
		ToastUtil.showToast(result.getErrorMessage());
	}

	public void findViewById() {
		Button rightBtn = getRightButtonText(getResources().getString(
				R.string.basic_save));
		Button leftBtn = getLeftButtonText(getResources().getString(
				R.string.basic_back));
		findViewById(R.id.titleBar).setBackgroundColor(
				getResources().getColor(R.color.color_diet_title));
		leftBtn.setOnClickListener(this);
		rightBtn.setOnClickListener(this);

		if (meal == null) {
			meal = getResources().getStringArray(R.array.meal_type);
		}

		try {
			setTitle(meal[mealType - 1]);
		} catch (Exception e) {
			e.printStackTrace();
		}
		_tv_bound = (TextView) findViewById(R.id.tv_bound);
		_tv_bound.setText(String.format(Locale.CHINESE, "%dkcal", BoundValue));
		_tv_value = (TextView) findViewById(R.id.tv_value);
		_img_loc = (ImageView) findViewById(R.id.img_loc);
		_img_back = (ImageView) findViewById(R.id.img_back);

		_et_remark = (EditText) findViewById(R.id.et_remark);
		imgBtnAdd = (ImageButton) findViewById(R.id.diet_add_record_img_add);
		imgBtnAdd.setOnClickListener(this);
		_hsc = (HorizontalScrollView) findViewById(R.id.disease_lv);
		_img[0] = (ImageView) findViewById(R.id.disease_img1);
		_img[1] = (ImageView) findViewById(R.id.disease_img2);
		_img[2] = (ImageView) findViewById(R.id.disease_img3);
		_img[3] = (ImageView) findViewById(R.id.disease_img4);
		_img[4] = (ImageView) findViewById(R.id.disease_img5);
		_img[5] = (ImageView) findViewById(R.id.disease_img6);
		_img[6] = (ImageView) findViewById(R.id.disease_img7);
		_img[7] = (ImageView) findViewById(R.id.disease_img8);
		_img[8] = (ImageView) findViewById(R.id.disease_img9);

		_btn_add = (Button) findViewById(R.id.diet_add_record_btn);
		_record_layout = (LinearLayout) findViewById(R.id.diet_add_record_layout);
		_btn_add.setOnClickListener(this);

	}

	private void calculteEnergyByName(FoodNutritionBean[] fnbArray,
			FoodBean fb, EnergyBean energyBean) {
		if (fnbArray == null || fnbArray.length == 0) {
			return;
		}
		int index = -1;
		for (int i = 0; i < fnbArray.length; i++) {
			if (fnbArray[i].getFoodName().equals(fb.getName())) {
				index = i;
				break;
			}
		}
		if (index >= 0 && index < fnbArray.length) {
			FoodNutritionBean fnb = fnbArray[index];
			Integer weight = fnb.getFoodWeight();
			float uw = ((float) fb.getCount()) / weight;
			float energy = uw
					* (fnb.getFoodEnergy() == null ? 0 : fnb.getFoodEnergy());
			float protein = uw
					* (fnb.getFoodProtein() == null ? 0 : fnb.getFoodProtein());
			float fat = uw * (fnb.getFoodFat() == null ? 0 : fnb.getFoodFat());
			energyBean.setEnergy(energy);
			energyBean.setProtein(protein);
			energyBean.setFat(fat);
			energyBean.setFbid(fb.getId());
			MyUtils.showLog(" calculateEnergyByName fnb "+fnb.toString()+" fb "+fb.toString()+" energyBean "+energyBean.toString());
			_energyList.append(energyBean.getFbid(), energyBean);
		}

	}

	private void showResult() {
		_record_layout.removeAllViews();
		for (int i = 0; i < _resultList.size(); i++) {
			if (isUpdate) {
				FoodBean fb = _resultList.get(i);
				EnergyBean energyBean = new EnergyBean();
				FoodNutritionBean[] fnb = _nameArray.get(fb.getTypeName());
				calculteEnergyByName(fnb, fb, energyBean);
			}
			_record_layout.addView(generateDataLayout(_resultList.get(i), i));
		}

		if (_record_layout.getVisibility() != View.VISIBLE) {
			_record_layout.setVisibility(View.VISIBLE);
		}
		if ("点击添加".equals(_btn_add.getText())) {
			_btn_add.setBackgroundResource(R.drawable.diet_add);
			_btn_add.setTextColor(getResources().getColor(R.color.white));
			_btn_add.setText(getResources().getString(R.string.continue_add));
		}
	}

	/**
	 * 生成单条记录布局
	 * 
	 * @param fb
	 * @param position
	 * @return
	 */
	private View generateDataLayout(final FoodBean fb, final int position) {
		final LinearLayout chileLayout = (LinearLayout) getLayoutInflater()
				.inflate(R.layout.layout_record_item_nodelete, null);
		ImageView img_del = (ImageView) chileLayout
				.findViewById(R.id.record_item_img_del);
		img_del.setVisibility(View.VISIBLE);
		TextView tv_type = (TextView) chileLayout
				.findViewById(R.id.record_item_nodelete_tv_left);
		tv_type.setVisibility(View.VISIBLE);
		TextView tv_name = (TextView) chileLayout
				.findViewById(R.id.record_item_nodelete_tv_middle);
		tv_name.setVisibility(View.VISIBLE);
		TextView tv_unit = (TextView) chileLayout
				.findViewById(R.id.record_item_nodelete_tv_right);
		tv_unit.setVisibility(View.VISIBLE);
		tv_type.setText(getFoodTypeById(fb.getType()));
		tv_name.setText(fb.getName());
		tv_unit.setText(fb.getCount() + fb.getUnit());
		img_del.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				_record_layout.removeView(chileLayout);
				_resultList.remove(fb);

				MyUtils.showLog(" 删除记录 " + fb.toString() + " 剩余记录  "
						+ _resultList.toString());

				EnergyBean energyBean = _energyList.get(fb.getId());
				MyUtils.showLog(" 删除对应能量记录  " + energyBean.toString());
				calculateEnergy(fb.getId(), 0 - energyBean.getEnergy(),
						0 - energyBean.getProtein(), 0 - energyBean.getFat());

				if (_record_layout.getChildCount() == 0) {
					_record_layout.setVisibility(View.GONE);
					_btn_add.setBackgroundResource(R.drawable.diet_record_bg);
					_btn_add.setTextColor(getResources().getColor(
							R.color.color_text_gray_dark));
					_btn_add.setText(getResources().getString(
							R.string.click_to_add));
				}
			}
		});

		return chileLayout;
	}

	public String getFoodTypeById(Integer type) {
		if (type == null) {
			type = 1;
		}
		String[] array = getResources().getStringArray(
				R.array.foodmaterial_type);
		return array[type - 1];
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

	}

	/**
	 * 判断该食物是否已选中 是 返回 对应list索引 否 返回-1
	 * 
	 * @param fb
	 * @return
	 */
	private int isContains(FoodBean fb, List<FoodBean> list) {
		if (fb == null) {
			return -1;
		}
		int index = -1;
		if (list != null && list.size() > 0) {

			for (int i = 0; i < list.size(); i++) {
				if (fb.getId() != null
						&& fb.getId().equals(list.get(i).getId())) {
					index = i;
				} else if (fb.getName() != null
						&& fb.getName().equals(list.get(i).getName())) {
					index = i;
				}
			}
		}
		return index;
	}

	private float _energy;
	private float _protein;
	private float _fat;
	private float _currentEnergy;

	private SparseArray<EnergyBean> _energyList = new SparseArray<EnergyBean>();

	/**
	 * 计算能量值
	 * 
	 * @param fnb
	 * @param unit
	 */
	private void calculateEnergy(Integer fbid, float energy, float protein,
			float fat) {
		_currentEnergy = _energy;

		MyUtils.showLog(" 计算结果  能量  " + energy + "  蛋白值   " + protein
				+ "  脂肪   " + fat);
		_energy += energy;
		_protein += protein;
		_fat += fat;
		MyUtils.showLog("calculateEnergy  计算能量值完毕显示结果  能量值  = "
				+ (int) (_energy) + " 格式化字符串   "
				+ String.format("%dkcal", (int) (_energy)));
		MyUtils.showLog(" 设置完毕   _tv_value.getWidth = " + _tv_value.getWidth());
		_tv_value.setText(String.format("%dkcal", (int) (_energy)));
		MyUtils.showLog(" 显示完毕   _tv_value.getText() = "
				+ _tv_value.getText().toString());
		EnergyBean energyBean = new EnergyBean();
		energyBean.setFbid(fbid);
		energyBean.setEnergy(Math.abs(energy));
		energyBean.setProtein(Math.abs(protein));
		energyBean.setFat(Math.abs(fat));
		MyUtils.showLog(" 新纪录 calculateEnergy " + energyBean.toString());
		if (_energyList.get(fbid) != null) { // 如果已存在 替换原有记录
			EnergyBean energyBean2 = _energyList.get(fbid);
			energyBean2.setEnergy(energyBean2.getEnergy() + energy);
			energyBean2.setProtein(energyBean2.getProtein() + protein);
			energyBean2.setFat(energyBean2.getFat() + fat);
			MyUtils.showLog(" 替换已存在记录 calculateEnergy "
					+ energyBean2.toString());
			// if (energyBean2.getEnergy() == 0) {
			// _energyList.remove(fbid);
			// } else {
			_energyList.put(fbid, energyBean2);
			// }
		} else {
			_energyList.append(fbid, energyBean);
		}

		MyUtils.showLog(" 准备移动  _energy " + _energy + " ");
		if (_energy >= UpValue || _energy <= 0) {
			// _canMove = false;
		} else {
			_canMove = true;
		}

		// if (_canMove) {
		calculateLength();
		// }

	}

	private int leftMargin = 0; // 左边距

	private int _moveLength = 0; // 能量值换算成px 移动距离

	private int dest = 0;

	int length = 0;

	/**
	 * 根据能量值 计算左边距 并移动指针
	 */
	private void calculateLength() {
		if (back_length == 0 || loc_length == 0 || bound_length == 0) {
			back_length = _img_back.getWidth();
			loc_length = _img_loc.getWidth();
			bound_length = _tv_bound.getWidth();
		}

		int bblength = back_length - bound_length;
		if (_energy <= BoundValue) {
			_moveLength = (int) Math.ceil((_energy / BoundValue)
					* (bblength - loc_length / 2))
					- leftMargin;
			// if (value >=this._currentEnergy) {
			// if (_moveLength>loc_length/2) {
			// _moveLength-= loc_length / 2;
			// }
			// }
			MyUtils.showLog("<2000   _energy == " + _energy
					+ " _leftMargin == " + leftMargin + "  _moveLength "
					+ _moveLength);

		} else {
			dest = bblength
					+ (int) Math
							.ceil(((_energy - BoundValue) / (UpValue - BoundValue))
									* (bound_length - loc_length / 2));
			_moveLength = bblength
					+ (int) Math
							.ceil(((_energy - BoundValue) / (UpValue - BoundValue))
									* (bound_length - loc_length / 2))
					- leftMargin;

			MyUtils.showLog(" 大于2000  _energy == " + _energy
					+ " leftMargin == " + leftMargin + "  dest ==  " + dest
					+ " _moveLength == " + _moveLength
					+ " (dest-leftMargin) == " + (dest - leftMargin));

		}
		final RelativeLayout.LayoutParams imgparams = (android.widget.RelativeLayout.LayoutParams) _img_loc
				.getLayoutParams();
		final RelativeLayout.LayoutParams tvparams = (android.widget.RelativeLayout.LayoutParams) _tv_value
				.getLayoutParams();
		MyUtils.showLog(" 初始时左边距为   img_loc.leftMargin " + imgparams.leftMargin
				+ " tv.leftmargin " + tvparams.leftMargin);
		if (_moveLength + leftMargin >= (back_length - loc_length)) {
			_moveLength = back_length - loc_length - leftMargin;
			// _canMove = false;
		}
		if (_energy < UpValue || _currentEnergy < UpValue) {
			_canMove = true;
		}

		MyUtils.showLog("当前值   _energy = " + _energy + " back_length = "
				+ back_length + " loc_length " + loc_length + " bblength = "
				+ bblength + " bound_length = " + bound_length
				+ " 计算距离  leftMargin  " + leftMargin + " 此次相对上一次应该移动的距离  "
				+ _moveLength);

		MyUtils.showLog("准备移动   起始点  " + imgparams.leftMargin + " leftMargin "
				+ leftMargin + "移动距离   _moveLength == " + _moveLength
				+ " 终点   " + (_moveLength + imgparams.leftMargin)
				+ "  是否可以移动  _canMOve = " + _canMove);
		if (_canMove) {
			leftMargin += _moveLength;
			imgparams.setMargins(leftMargin, 0, 0, 0);
			tvparams.setMargins(leftMargin, 0, 0, 0);
			_img_loc.requestLayout();
			_tv_value.requestLayout();
 
			Paint paint = new Paint();  
			float measureText = paint.measureText("0000000000kcal");
			MyUtils.showLog("设置前 measureText "+measureText+" _tv_value.getWidth "+_tv_value.getWidth());
			if (_tv_value.getWidth() < measureText) {
				tvparams.width = (int) measureText;
				// _tv_value.requestLayout();
			}
			MyUtils.showLog("设置后 _tv_value.getWidth "+_tv_value.getWidth()+" "+_tv_value.getText().toString());

		}

		// if (_canMove) {
		// TranslateAnimation animation = new TranslateAnimation(
		// Animation.ABSOLUTE, imgparams.leftMargin,
		// Animation.ABSOLUTE, _moveLength+imgparams.leftMargin,
		// Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF,
		// 0);
		// animation.setDuration(1500);
		// animation.setFillAfter(true);
		// animation.setAnimationListener(new AnimationListener() {
		//
		// @Override
		// public void onAnimationEnd(Animation animation) {
		// leftMargin += _moveLength;
		// imgparams.leftMargin += _moveLength;
		// // tvparams.leftMargin +=_moveLength;
		// if (dest >= back_length - loc_length) {
		// _canMove = false;
		// } else {
		// _canMove = true;
		// }
		// }
		//
		// @Override
		// public void onAnimationRepeat(Animation animation) {
		// // TODO Auto-generated method stub
		//
		// }
		//
		// @Override
		// public void onAnimationStart(Animation animation) {
		//
		// }
		//
		// });
		// TranslateAnimation animation2 = new TranslateAnimation(
		// Animation.ABSOLUTE,tvparams.leftMargin,
		// Animation.ABSOLUTE, _moveLength+tvparams.leftMargin ,
		// Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF,
		// 0);
		// animation2.setDuration(1500);
		// animation2.setFillAfter(true);
		// animation2.setAnimationListener(new AnimationListener() {
		//
		// @Override
		// public void onAnimationStart(Animation animation) {
		//
		// }
		//
		// @Override
		// public void onAnimationRepeat(Animation animation) {
		// // TODO Auto-generated method stub
		//
		// }
		//
		// @Override
		// public void onAnimationEnd(Animation animation) {
		// tvparams.leftMargin += _moveLength;
		// if (dest >= back_length - loc_length) {
		// _canMove = false;
		// } else {
		// _canMove = true;
		// }
		// Paint paint = new Paint();
		// float measureText = paint.measureText("000000kcal");
		// MyUtils.showLog("000000kcal 所占宽度  " + measureText
		// + " 此时 _tv_value.getWidth = " + _tv_value.getWidth());
		// if (_tv_value.getWidth() < measureText) {
		// tvparams.width = (int) measureText;
		// // _tv_value.requestLayout();
		// }
		//
		// }
		// });
		// _img_loc.startAnimation(animation);
		// _tv_value.startAnimation(animation2);
		// }
	}

	@Override
	public void onClickCallBack(Bundle data) {
		// MyUtils.showLog("接收反馈  " + data.toString());
		int type = data.getInt("type", 0) + 1;
		FoodNutritionBean fnb = data.getParcelable("data");
		String unit = data.getString("unit");
		Integer unitValue = Integer
				.valueOf(unit.substring(0, unit.length() - 1));
		FoodBean fb = new FoodBean();
		fb.setId(fnb.getId());
		fb.setCount(unitValue);
		fb.setName(fnb.getFoodName());
		fb.setUnit("g");
		fb.setType(type);
		int index = isContains(fb, _resultList);
		boolean donothing = false; // 判断是否进行操作 默认false 即需要进行下一步能量值与动画操作
		// MyUtils.showLog(" onClickCallBack  unit = " + unit);
		if (index == -1) {
			_resultList.add(fb);
			donothing = false;
		} else {
			if (MyUtils.domainEquals(fb, _resultList.get(index))) { // 判断当前选的食物和已存在列表中对象是否完全一致
				donothing = true;
				MyUtils.showLog("选择的食物已存在 且 摄取的分量一致   不做操作");
			} else {
				donothing = false;
				Integer unitBefroe = _resultList.get(index).getCount();
				// MyUtils.showLog("onClickCallBack  unitBefore " + unitBefroe);
				unitValue = unitValue - unitBefroe;
				_resultList.set(index, fb);
			}
		}
		if (!donothing) {
			// MyUtils.showLog("onClickCallBack  最终结果  摄取量    " + unitValue);

			Integer weight = fnb.getFoodWeight();
			float uw = ((float) unitValue) / weight;
			// MyUtils.showLog("计算   unit 与 weight  比值   " + uw);
			float energy = uw
					* (fnb.getFoodEnergy() == null ? 0 : fnb.getFoodEnergy());
			float protein = uw
					* (fnb.getFoodProtein() == null ? 0 : fnb.getFoodProtein());
			float fat = uw * (fnb.getFoodFat() == null ? 0 : fnb.getFoodFat());

			calculateEnergy(fnb.getId(), energy, protein, fat);
			Collections.sort(_resultList);
			showResult();
		}
	}

}
