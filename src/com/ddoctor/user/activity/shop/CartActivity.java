package com.ddoctor.user.activity.shop;

/**
 * 购物车
 * 康
 */
import java.util.ArrayList;
import java.util.Locale;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.user.activity.BaseActivity;
import com.ddoctor.user.adapter.BaseAdapter;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.data.MyDBUtil;
import com.ddoctor.user.model.ShopBean;
import com.ddoctor.user.wapi.bean.ProductBean;
import com.ddoctor.utils.DialogUtil;
import com.ddoctor.utils.DialogUtil.ConfirmDialog;
import com.ddoctor.utils.ImageLoaderUtil;
import com.ddoctor.utils.ToastUtil;
import com.umeng.analytics.MobclickAgent;
import com.zhuge.analysis.stat.ZhugeSDK;

public class CartActivity extends BaseActivity {

//	List<ShopBean> _dataList = new ArrayList<ShopBean>();
	ArrayList<ProductBean> _dataList = new ArrayList<ProductBean>();
	CartAdapter _adapter = null;
	
	ListView _listView;
	private TextView _tv_norecord;

	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("CartActivity");
		MobclickAgent.onResume(CartActivity.this);
		ZhugeSDK.getInstance().init(getApplicationContext());
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("CartActivity");
		MobclickAgent.onPause(CartActivity.this);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_shop_cart);

		initUI();
		
		initData();
	}

	protected void initUI() {
		this.setTitle("我的购物车");
		Button leftButton = this.getLeftButtonText("返回");
		Button rightButton = this.getRightButtonText("清空");
		RelativeLayout rl = (RelativeLayout) findViewById(R.id.titleBar);
		if (rl != null) {
			rl.setBackgroundColor(getResources().getColor(
					R.color.default_titlebar));
		}
		leftButton.setOnClickListener(this);
		rightButton.setOnClickListener(this);
		
		_listView = (ListView) findViewById(R.id.listView);
		_tv_norecord = (TextView) findViewById(R.id.tv_norecord);
		_tv_norecord.setText("您还没有添加任何商品到购物车，快去商城看看吧");
		_tv_norecord.setVisibility(View.INVISIBLE);
		_listView.setEmptyView(_tv_norecord);
		
		Button btn = (Button)findViewById(R.id.btn_buy);
		btn.setOnClickListener(this);

	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.btn_left:
			finishThisActivity();
			break;
		case R.id.btn_right:
			doEmptyCartPrompt();
			break;
		case R.id.btn_buy:
			on_btn_submit();
			break;
		default:
			break;
		}
	}

	private void initData() {
		
		// 从数据库读取购物车数据列表
		String sql = String.format(Locale.CHINESE, "select * from shopcart where uid=%d order by createtime desc",
				DataModule.getInstance().getLoginedUserId());
		Cursor cursor = MyDBUtil.getInstance().querySQL(sql);
		if( cursor != null )
		{
			if( cursor.moveToFirst() )
			{
				do
				{
					int id = cursor.getInt(cursor.getColumnIndex("id"));
					String name = cursor.getString(cursor.getColumnIndex("name"));
					String image = cursor.getString(cursor.getColumnIndex("image"));
					int num = cursor.getInt(cursor.getColumnIndex("num"));
					String createtime = cursor.getString(cursor.getColumnIndex("createtime"));
					float discount = cursor.getFloat(cursor.getColumnIndex("discount"));
					
					ProductBean productBean = new ProductBean();
					productBean.setId(id);
					productBean.setImage(image);
					productBean.setName(name);
					productBean.setDiscount(discount);
					productBean.setCount(num);
					_dataList.add(productBean);
					
//					ShopBean sb = new ShopBean();
//					sb.setId(id);
//					sb.setName(name);
//					sb.setDate(createtime);
//					sb.setImage(image);
//					sb.setNum(num);
//					sb.setPrice(discount);
//					
//					_dataList.add(sb);
					
				}while( cursor.moveToNext() );
			}
			
			cursor.close();
		}
		
		
//		for (int j = 0; j < tmplist.size(); j++) {
//			ShopBean sb = new ShopBean();
//			if (j == 0
//					|| (j >= 1 && !tmplist.get(j).getDate()
//							.equals(tmplist.get(j - 1).getDate()))) {
//
//				sb.setDate(tmplist.get(j).getDate());
//				sb.setLayoutType(RecordLayoutType.TYPE_CATEGORY);
//				_dataList.add(sb);
//			}
//			if (sb != null
//					&& RecordLayoutType.TYPE_CATEGORY == sb.getLayoutType()) {
//				sb = new ShopBean();
//			}
//			sb = tmplist.get(j);
//			_dataList.add(sb);
//		}
		
		_adapter = new CartAdapter(this);
		_listView.setAdapter(_adapter);
		
		if (_dataList.size() == 0) {
			_tv_norecord.setText("您还没有添加任何商品到购物车，快去商城看看吧");
			_tv_norecord.setVisibility(View.VISIBLE);
			_tv_norecord.setTag(0);
		}

	}
	
	
	//=============================================================================
	

	public class CartAdapter extends BaseAdapter<ShopBean> {

		public CartAdapter(Context context) {
			super(context);
		}
	
		@Override
		public int getCount() {
			return _dataList.size();
		}
		
	
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			
			ValueHolder valueHolder = null;
			if (null == convertView) {
				convertView = inflater.inflate(R.layout.layout_cart_item,
						parent, false);
				valueHolder = new ValueHolder();
				valueHolder.img_url = (ImageView)convertView.findViewById(R.id.img_url);
				valueHolder.tv_name = (TextView) convertView
						.findViewById(R.id.tv_name);
				valueHolder.tv_price = (TextView) convertView
						.findViewById(R.id.tv_price);
				valueHolder.ibtn_subtract_num = (ImageButton) convertView
						.findViewById(R.id.ibtn_subtract_num);
				valueHolder.ibtn_add_num = (ImageButton) convertView
						.findViewById(R.id.ibtn_add_num);
				valueHolder.et_num = (EditText) convertView
						.findViewById(R.id.et_num);
				convertView.setTag(valueHolder);
				
				convertView.setOnLongClickListener(new OnLongClickListener(){
	
					@Override
					public boolean onLongClick(View v) {
						ValueHolder holder = (ValueHolder) v.getTag();
						
						doDeletePromptWithDataIndex(holder.dataIndex);
						
						return false;
					}
					
				});
			} else {
				valueHolder = (ValueHolder) convertView.getTag();
			}
			
			valueHolder.dataIndex = position;
			
			valueHolder.img_url.setImageDrawable(null);
			
//			final ShopBean shopBean = _dataList.get(position);
			final ProductBean pb = _dataList.get(position);
	
			ImageLoaderUtil.display(pb.getImage(), valueHolder.img_url, R.drawable.ic_launcher);
			
			valueHolder.et_num.setText("0");
			valueHolder.tv_name.setText(String.valueOf(pb.getName()));
			valueHolder.tv_price.setText(String.format("￥%.2f", pb.getDiscount()));
			
	
			final EditText et = valueHolder.et_num;
			et.setText(String.valueOf(pb.getCount()));
	
			valueHolder.ibtn_add_num.setOnClickListener(new OnClickListener() {
	
				@Override
				public void onClick(View arg0) {
					int num = Integer.parseInt(et.getText().toString());
					num++;
					int pid = pb.getId();
					if( MyDBUtil.getInstance().addCartProductNum(pid, 1) )
					{
						et.setText(num + "");
						pb.setCount(num);
					}
					else
					{
						ToastUtil.showToast("操作失败!");
					}
		
				}
			});
	
			valueHolder.ibtn_subtract_num.setOnClickListener(new OnClickListener() 
			{
				@Override
				public void onClick(View arg0) 
				{
					int num = Integer.parseInt(et.getText().toString());
					if( num > 0 )
					{
						num--;
						
						int pid = pb.getId();
						if( MyDBUtil.getInstance().addCartProductNum(pid, -1) )
						{
							et.setText(num + "");
							pb.setCount(num);
						}
						else
						{
							ToastUtil.showToast("操作失败!");
							return;
						}
					}
					
					
					if( num <= 0) 
					{
	//					ToastUtil.showToast("已经到最少了");
						// 提示添加成功
						DialogUtil.confirmDialog(CartActivity.this, "从购物车删除此商品？", "删除","保留",  new ConfirmDialog(){
	
							@Override
							public void onOKClick(Bundle data) {
								// 删除
								doProductRemoveFromCart(pb.getId());
							}
	
							@Override
							public void onCancleClick() {
								// 保留
								
							}
							
						}).show();
					} 
				}
			});
			
			return convertView;
		}
	
		@Override
		public boolean areAllItemsEnabled() {
			return false;
		}
	
		@Override
		public boolean isEnabled(int position) {
			// if ("category".equalsIgnoreCase(String.valueOf(dataList.get(position)
			// .getLayoutType()))) {
			return false;
			// } else {
			// return true;
			// }
		}
	
		private class ValueHolder {
			private TextView tv_name;
			private TextView tv_price;
			private ImageButton ibtn_subtract_num;
			private ImageButton ibtn_add_num;
			private EditText et_num;
			private ImageView img_url;
			
			private int dataIndex;
		}
	}


	private void doDeletePromptWithDataIndex(int dataIndex)
	{
		final int index = dataIndex;
		DialogUtil.confirmDialog(CartActivity.this, "从购物车删除此商品？",  "删除", "取消", new ConfirmDialog(){

			@Override
			public void onOKClick(Bundle data) {
				// 删除
//				ShopBean sb = _dataList.get(index);
				ProductBean pb = _dataList.get(index);
				doProductRemoveFromCart(pb.getId());
			}

			@Override
			public void onCancleClick() {
				// 取消
				
			}
			
		}).show();
		
	}
	
	private void doEmptyCartPrompt()
	{
		if( _dataList.size() == 0 )
		{
			ToastUtil.showToast("购物车是空的哦！");
			return;
		}
		
		DialogUtil.confirmDialog(CartActivity.this, "确实要清空购物车吗？",  "清空", "取消", new ConfirmDialog(){

			@Override
			public void onOKClick(Bundle data) {
				// 删除
				if( MyDBUtil.getInstance().emptyCart() )
				{
					_dataList.clear();
					_adapter.notifyDataSetChanged();
					ToastUtil.showToast("清空购物车成功!");
				}
				else
				{
					ToastUtil.showToast("清空购物车失败!");
				}
			}

			@Override
			public void onCancleClick() {
				// 取消
				
			}
			
		}).show();
		
	}
	
	
	private void doProductRemoveFromCart(int pid)
	{
		// 从数据库中删除
		if( MyDBUtil.getInstance().removeFromCart(pid) )
		{
			// 从_dataList中删除
			for(int i = 0; i < _dataList.size(); i++ )
			{
//				ShopBean sb = _dataList.get(i);
//				if( sb.getId() == pid  )
				ProductBean pb = _dataList.get(i);
				if( pid == pb.getId()  )
				{
					_dataList.remove(i);
					break;
				}
			}

			_adapter.notifyDataSetChanged();// 刷新UI

			ToastUtil.showToast("从购物车删除成功!");
		}
		else
			ToastUtil.showToast("删除失败！");
	}
	
	// 提交订单
	private void on_btn_submit()
	{
		
//		List<ShopBean> dataList = new ArrayList<ShopBean>();
		ArrayList<ProductBean> dataList = new ArrayList<ProductBean>();
		dataList.addAll(_dataList);
		int cnt = dataList.size();
		for(int i = cnt -1 ; i >= 0; i-- )
		{
			if( dataList.get(i).getCount()!=null && dataList.get(i).getCount() <= 0 )
				dataList.remove(i);
		}
		
		if( dataList.size() < 1 )
		{
			ToastUtil.showToast("购物车是空的哦！");
			return;
		}
		
		Intent intent = new Intent(CartActivity.this, OrderSubmitActivity.class);
//		intent.putExtra("dataList", (Serializable)dataList);
		intent.putParcelableArrayListExtra("dataList", dataList);
		startActivity(intent);
	}
}
