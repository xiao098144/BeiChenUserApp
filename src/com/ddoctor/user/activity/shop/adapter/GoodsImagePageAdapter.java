package com.ddoctor.user.activity.shop.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.beichen.user.R;
import com.ddoctor.utils.ImageLoaderUtil;

public class GoodsImagePageAdapter extends PagerAdapter {

	/**
	 * 装ImageView数组
	 */
	private List<String> _imgList = null;
	private List<ImageView> _imgViewList = null;

	/**
	 * 图片资源id
	 */
	private int[] imgIdArray;

	Context context;

	public GoodsImagePageAdapter(Context context, List<String> imgList) {
		super();
		this.context = context;

		_imgList = imgList;
		if( _imgList.size() > 0 )
		{
			_imgViewList = new ArrayList<ImageView>();
			for(int i = 0; i < _imgList.size(); i++ )
				_imgViewList.add(null);
		}
	}

	@Override
	public int getCount() {
		return _imgList.size();
	}

	@Override
	public boolean isViewFromObject(View arg0, Object arg1) {
		return arg0 == arg1;
	}

	@Override
	public void destroyItem(View container, int position, Object object) {
		ImageView v = _imgViewList.get(position);
		if( v != null )
		{
			if( v.getParent() != null )
				((ViewPager) container).removeView(v);
			
			_imgViewList.set(position, null);
		}
	}

	/**
	 * 载入图片进去，用当前的position 除以 图片数组长度取余数是关键
	 */
	@Override
	public Object instantiateItem(View container, int position) 
	{
		ImageView v = _imgViewList.get(position);
		if( v == null )
		{
			v = new ImageView(this.context);
			
			LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
					LinearLayout.LayoutParams.MATCH_PARENT,
					LinearLayout.LayoutParams.MATCH_PARENT);
			v.setLayoutParams(lp);
			
			_imgViewList.set(position, v);
		}
		
		if( v.getParent() == null )
			((ViewPager) container).addView(v);
		
		String url = _imgList.get(position);
		ImageLoaderUtil.display(url,v,R.drawable.ic_launcher);
		
		return v;
	}
}
