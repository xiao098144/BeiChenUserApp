package com.ddoctor.user.activity.shop.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.user.adapter.BaseAdapter;
import com.ddoctor.user.wapi.bean.ProductBean;
import com.ddoctor.utils.ImageLoaderUtil;
import com.ddoctor.utils.StringUtils;

public class ShopListAdapter extends BaseAdapter<ProductBean> {

	public ShopListAdapter(Context context) {
		super(context);

	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ProductBean sb = dataList.get(position);
		ViewHolder holder = null;
		if (null == convertView) {
			convertView = inflater.inflate(R.layout.layout_shop_list_item,
					parent, false);
			holder = new ViewHolder();
			holder.img = (ImageView) convertView.findViewById(R.id.goods_list_item_img);
			holder.img.setScaleType(ScaleType.FIT_CENTER);
			holder.tv_name = (TextView) convertView
					.findViewById(R.id.goods_list_item_tv_name);
			holder.tv_info = (TextView) convertView
					.findViewById(R.id.goods_list_item_tv_info);
			holder.tv_old_price = (TextView) convertView
					.findViewById(R.id.goods_list_item_tv_old_price);
			holder.tv_new_price = (TextView) convertView
					.findViewById(R.id.goods_list_item_tv_new_price);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		holder.img.setImageDrawable(null);
		
		ImageLoaderUtil.display(StringUtils.urlFormatRemote(sb.getImage()), holder.img, 0);
		holder.tv_name.setText(sb.getName());
		holder.tv_info.setText(sb.getInfo());
		
		holder.tv_old_price.setText(formatStr("市场价：", sb.getPrice()+""));
		holder.tv_old_price.getPaint().setFlags(
				Paint.STRIKE_THRU_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
		holder.tv_new_price.setText(formatStr("商城价：", sb.getDiscount()+""));
		return convertView;
	}

	private String formatStr(String prefix ,String source){
		StringBuffer sb = new StringBuffer();
		sb.append(prefix);
		sb.append(source);
		return sb.toString();
	}
	
	private class ViewHolder {
		private ImageView img;
		private TextView tv_name, tv_info, tv_old_price, tv_new_price;
	}

}
