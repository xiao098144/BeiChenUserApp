package com.ddoctor.user.activity.shop;

/**
 * 确认订单、准备提交订单
 */
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.enums.RetError;
import com.ddoctor.user.activity.BaseActivity;
import com.ddoctor.user.adapter.BaseAdapter;
import com.ddoctor.user.data.MyDBUtil;
import com.ddoctor.user.task.OrderSubmitTask;
import com.ddoctor.user.task.PayPrepareTask;
import com.ddoctor.user.task.TaskPostCallBack;
import com.ddoctor.user.wapi.bean.DeliverBean;
import com.ddoctor.user.wapi.bean.OrderBean;
import com.ddoctor.user.wapi.bean.ProductBean;
import com.ddoctor.user.wapi.constant.PayMethod;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.DialogUtil;
import com.ddoctor.utils.DialogUtil.ConfirmDialog;
import com.ddoctor.utils.ImageLoaderUtil;
import com.ddoctor.utils.ToastUtil;
import com.umeng.analytics.MobclickAgent;
import com.zhuge.analysis.stat.ZhugeSDK;

public class OrderSubmitActivity extends BaseActivity {

	private RelativeLayout rel_address;

	// private OrderBean _orderBean = new OrderBean();
	private DeliverBean _deliverBean = null;
	private List<ProductBean> _dataList = null;

	private ListView _listView;
	private TextView _totalPriceTextView;
	private LinearLayout _footerLayout;

	private TextView _tv_load_error;

	private RelativeLayout _layout_data;

	private Float _fee = 0f; // 运费

	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("OrderSubmitActivity");
		MobclickAgent.onResume(OrderSubmitActivity.this);
		ZhugeSDK.getInstance().init(getApplicationContext());
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("OrderSubmitActivity");
		MobclickAgent.onPause(OrderSubmitActivity.this);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_shop_order_submit);

		// 传入的参数，1个或n个商品的列表List<ShopBean>
		// _dataList =
		// (List<ShopBean>)this.getIntent().getSerializableExtra("dataList");
		_dataList = getIntent().getParcelableArrayListExtra("dataList");

		initUI();
		payPrepare(true);
	}

	private void payPrepare(boolean showDialog) {
		if (showDialog) {

			if (_loadingDialog == null) {
				_loadingDialog = DialogUtil.createLoadingDialog(this, "提交中...");
			}
			_loadingDialog.setCancelable(true);
			_loadingDialog.show();
		}

		PayPrepareTask task = new PayPrepareTask(_dataList);
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {

			@Override
			public void taskFinish(DDResult result) {
				_loadingDialog.dismiss();
				if (result.getError() == RetError.NONE) {
					OrderBean orderBean = result.getBundle().getParcelable(
							"order");
					showDataUI(orderBean);
					if (_tv_load_error.getVisibility() != View.GONE) {
						_tv_load_error.setVisibility(View.GONE);
					}
					if (_layout_data.getVisibility() != View.VISIBLE) {
						_layout_data.setVisibility(View.VISIBLE);
					}
				} else {
					ToastUtil.showToast(result.getErrorMessage());
					if (_tv_load_error.getVisibility() != View.VISIBLE) {
						_tv_load_error.setVisibility(View.VISIBLE);
					}
					if (_layout_data.getVisibility() != View.GONE) {
						_layout_data.setVisibility(View.GONE);
					}

				}
			}
		});
		task.executeParallel("");

	}

	protected void initUI() {
		this.setTitle("订单详情");
		Button leftButton = this.getLeftButtonText("返回");

		RelativeLayout rl = (RelativeLayout) findViewById(R.id.titleBar);
		if (rl != null) {
			rl.setBackgroundColor(getResources().getColor(
					R.color.default_titlebar));
		}
		leftButton.setOnClickListener(this);

		_tv_load_error = (TextView) findViewById(R.id.order_tv_loading_error);
		_tv_load_error.setOnClickListener(this);

		_layout_data = (RelativeLayout) findViewById(R.id.order_data_layout);

		_listView = (ListView) findViewById(R.id.listView);
		_totalPriceTextView = (TextView) findViewById(R.id.tv_total_price);

		Button btn = (Button) findViewById(R.id.btn_submit_order);
		btn.setOnClickListener(this);

		LinearLayout footerLayout = (LinearLayout) getLayoutInflater().inflate(
				R.layout.layout_shop_order_submit_footer, null);
		_listView.addFooterView(footerLayout);
		_footerLayout = footerLayout;

		// rl = (RelativeLayout)_footerLayout.findViewById(R.id.rl_pay_type);
		// rl.setOnClickListener(this);
		rel_address = (RelativeLayout) _footerLayout
				.findViewById(R.id.rel_address);
		rel_address.setOnClickListener(this);

		rl = (RelativeLayout) _footerLayout.findViewById(R.id.rl_score);
		rl.setOnClickListener(this);

	}

	private int _currentDeliverId = 0;

	private void showDataUI(OrderBean orderBean) {
		PayAdapter adpater = new PayAdapter(this);
		_listView.setAdapter(adpater);
		adpater.setData(_dataList);

		TextView tv = (TextView) _footerLayout
				.findViewById(R.id.tv_shop_good_price);
		tv.setText(String.format(Locale.CHINESE, "￥%.2f",
				orderBean.getTotalPrice()));
		TextView tv_express = (TextView) _footerLayout
				.findViewById(R.id.tv_shop_express_price);
		DeliverBean deliver = orderBean.getDeliver();
		if (deliver != null) {
			_fee = deliver.getFee();
			showAddress(deliver);
			_currentDeliverId = deliver.getId();
		}

		tv_express.setText(String.format(Locale.CHINESE, "￥%.2f", _fee));

		_totalPriceTextView.setText(String.format(Locale.CHINESE, "应付金额：￥%.2f",
				orderBean.getTotalPayPrice()));

	}

	private void showAddress(DeliverBean db) {
		TextView tv = (TextView) _footerLayout.findViewById(R.id.tv_pay_adress);

		tv.setText(db.getAddress());

		tv = (TextView) _footerLayout.findViewById(R.id.tv_pay_contact);
		tv.setText(db.getName() + " " + db.getMobile());

		_deliverBean = db;
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.order_tv_loading_error: {
			payPrepare(false);
		}
			break;
		case R.id.rel_address: {// 收货地址
			on_btn_address_select();
		}
			break;
		case R.id.btn_left: {
			finishThisActivity();
			break;
		}
		case R.id.btn_submit_order: {
			on_btn_submit();
			break;
		}
		// case R.id.rl_pay_type:{ // 付款方式
		//
		// }
		// break;
		case R.id.rl_score: {
			on_btn_score();
		}
			break;
		default:
			break;
		}
	}

	public class PayAdapter extends BaseAdapter<ProductBean> {

		public PayAdapter(Context context) {
			super(context);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			ValueHolder valueHolder = null;
			if (null == convertView) {
				convertView = inflater.inflate(R.layout.layout_pay_item,
						parent, false);
				valueHolder = new ValueHolder();
				valueHolder.imgView = (ImageView) convertView
						.findViewById(R.id.imgView);
				valueHolder.tv_name = (TextView) convertView
						.findViewById(R.id.tv_name);
				valueHolder.tv_price = (TextView) convertView
						.findViewById(R.id.tv_price);
				valueHolder.tv_num = (TextView) convertView
						.findViewById(R.id.tv_num);
				convertView.setTag(valueHolder);

			} else {
				valueHolder = (ValueHolder) convertView.getTag();
			}

			valueHolder.dataIndex = position;

			valueHolder.imgView.setImageDrawable(null);

			final ProductBean pb = dataList.get(position);
			ImageLoaderUtil.display(pb.getImage(), valueHolder.imgView,
					R.drawable.ic_launcher);

			valueHolder.tv_name.setText(String.valueOf(pb.getName()));
			valueHolder.tv_price.setText(String.format("￥%.2f",
					pb.getDiscount()));
			valueHolder.tv_num.setText(String.format("x%d", pb.getCount()));

			return convertView;
		}

		private class ValueHolder {
			private ImageView imgView;
			private TextView tv_name;
			private TextView tv_price;
			private TextView tv_num;

			private int dataIndex;
		}
	}

	/**
	 * 添加自定义事件
	 * 
	 * @param eventId
	 * @param eventName
	 */
	private void addEvent(String eventId, String eventName) {
		MobclickAgent.onEvent(this, eventId);
		ZhugeSDK.getInstance().onEvent(this, eventName);
	}

	private Dialog _loadingDialog = null;

	private void on_btn_submit() {
		// check...
		if (_deliverBean == null) {
			ToastUtil.showToast("请选择收货地址!");
			return;
		}

		OrderBean orderBean = new OrderBean();
		orderBean.setDeliver(_deliverBean);
		orderBean.setVoucherId(0);
		orderBean.setPayMethod(PayMethod.ALIPAY);

		List<ProductBean> productList = new ArrayList<ProductBean>();
		float total_price = 0;
		int total_count = 0;
		for (int i = 0; i < _dataList.size(); i++) {
			// ShopBean sb = _dataList.get(i);

			// ProductBean pb = new ProductBean();
			// pb.setId(sb.getId());
			// pb.setCount(sb.getNum());
			// productList.add(pb);

			// total_price += (sb.getPrice() * sb.getNum());
			// total_count += sb.getNum();
			ProductBean pb = _dataList.get(i);
			productList.add(pb);
			total_price += (pb.getDiscount() * pb.getCount());
			total_count += pb.getCount();
		}

		orderBean.setProducts(productList);

		orderBean.setTotalPrice(total_price);
		orderBean.setTotalPayPrice(total_price);
		orderBean.setTotalCount(total_count);
		orderBean.setTotalDiscount(total_price);

		_loadingDialog = DialogUtil.createLoadingDialog(this, "提交中...");
		_loadingDialog.show();

		addEvent("400001", getString(R.string.event_tjdd_400001));

		OrderSubmitTask task = new OrderSubmitTask(orderBean);
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {
			@Override
			public void taskFinish(DDResult result) {

				if (_loadingDialog != null) {
					_loadingDialog.dismiss();
					_loadingDialog = null;
				}

				if (result.getError() == RetError.NONE) {
					on_task_finished(result);
				} else {
					on_task_failed(result);
				}

			}
		});
		task.executeParallel("");

	}

	private void on_task_finished(DDResult result) {
		final int orderId = result.getBundle().getInt("orderId");

		// 提交订单成功，把购物车里的东西清空
		MyDBUtil.getInstance().emptyCart();

		// 提示一下
		DialogUtil.confirmDialog(this, "订单提交成功！", "立即支付", "",
				new ConfirmDialog() {

					@Override
					public void onOKClick(Bundle data) {
						// 跳转到订单付款页面
						Intent intent = new Intent(OrderSubmitActivity.this,
								OrderDetailActivity.class);
						intent.putExtra("orderId", orderId);
						startActivity(intent);

						finish();
					}

					@Override
					public void onCancleClick() {
						// TODO Auto-generated method stub

					}
				}).show();

	}

	private void on_task_failed(DDResult result) {
		ToastUtil.showToast(result.getErrorMessage());
	}

	private void on_btn_address_select() {
		Intent intent = new Intent(this, ShopAddressListActivity.class);
		intent.putExtra("deliverid", _currentDeliverId);
		startActivityForResult(intent, 100);
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 100 && resultCode == 1) {// 选择收货地址

			Bundle b = data.getExtras(); // data为B中回传的Intent
			boolean addressDeleted = b.getBoolean("del", false); // 当前使用的地址被删除
			DeliverBean db = (DeliverBean) b.getSerializable("data");
			if (db != null) {
				_currentDeliverId = db.getId();
				showAddress(db);
			}
			if (addressDeleted) {
				_deliverBean = null;
			}

		}
	}

	private void on_btn_score() {
		RelativeLayout rootLayout = (RelativeLayout) findViewById(R.id.rootLayout);
		doShowScorePanel(rootLayout);

	}

	public void doShowScorePanel(View parent) {

		if (2 > 1) {
			ToastUtil.showToast("暂时不支持积分抵扣！");
			return;
		}

		// TODO: 先要获取用户当前积分，然后显示给用户看

		View view = View.inflate(this, R.layout.layout_pay_score_popwin, null);

		view.startAnimation(AnimationUtils.loadAnimation(this,
				R.anim.slide_in_from_bottom));
		view.setFocusable(true);
		view.setFocusableInTouchMode(true);

		final PopupWindow popupWindow = new PopupWindow(view,
				WindowManager.LayoutParams.MATCH_PARENT,
				WindowManager.LayoutParams.WRAP_CONTENT);
		popupWindow.setBackgroundDrawable(new ColorDrawable(0x00000000));

		WindowManager.LayoutParams lp = getWindow().getAttributes();
		lp.alpha = 0.4f;
		getWindow().setAttributes(lp);

		// 设置点击窗口外边窗口消失
		popupWindow.setOutsideTouchable(true);

		popupWindow.setSoftInputMode(PopupWindow.INPUT_METHOD_NEEDED);
		popupWindow
				.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

		// 设置此参数获得焦点，否则无法点击
		popupWindow.setFocusable(true);
		popupWindow.showAtLocation(parent, Gravity.BOTTOM, 0, 0);
		popupWindow.update();
		popupWindow.setOnDismissListener(new OnDismissListener() {

			@Override
			public void onDismiss() {
				// TODO Auto-generated method stub
				WindowManager.LayoutParams lp = getWindow().getAttributes();
				lp.alpha = 1f;
				getWindow().setAttributes(lp);
			}

		});

		TextView cancelTextView = (TextView) view.findViewById(R.id.btn_cancel);
		cancelTextView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				popupWindow.dismiss();
			}

		});

		Button btnOk = (Button) view.findViewById(R.id.btn_score_ok);
		btnOk.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				EditText et = (EditText) popupWindow.getContentView()
						.findViewById(R.id.inputEditText);

				String s = et.getText().toString().trim();
				if (s.length() < 1) {
					ToastUtil.showToast("请输入要抵扣的积分!");
					return;
				}

				if (!TextUtils.isDigitsOnly(s)) {
					ToastUtil.showToast("请输入正确的格式!");
					return;
				}

				try {
					int score = Integer.parseInt(s);
					if (score > 1000) {
						ToastUtil.showToast("每个订单最多只能使用1000积分!");
						return;
					}

					popupWindow.dismiss();
					return;
				} catch (Exception e) {
					e.printStackTrace();
				}

				ToastUtil.showToast("错误!");

			}
		});

		view.setOnKeyListener(new OnKeyListener() {

			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				// TODO Auto-generated method stub
				if (keyCode == KeyEvent.KEYCODE_BACK) {
					popupWindow.dismiss();
					return true;
				}
				return false;
			}
		});

	}

}
