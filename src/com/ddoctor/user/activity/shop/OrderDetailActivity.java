package com.ddoctor.user.activity.shop;

/**
 * 支付界面
 * 康
 */
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alipay.sdk.app.PayTask;
import com.beichen.user.R;
import com.ddoctor.enums.RetError;
import com.ddoctor.user.activity.BaseActivity;
import com.ddoctor.user.adapter.BaseAdapter;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.pay.AliPayConstant;
import com.ddoctor.user.pay.AliPayUtil;
import com.ddoctor.user.pay.PayResult;
import com.ddoctor.user.task.GetOrderDetailTask;
import com.ddoctor.user.task.SynchPayStatusTask;
import com.ddoctor.user.task.TaskPostCallBack;
import com.ddoctor.user.wapi.bean.OrderBean;
import com.ddoctor.user.wapi.bean.ProductBean;
import com.ddoctor.user.wapi.constant.PayStatus;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.DialogUtil;
import com.ddoctor.utils.ImageLoaderUtil;
import com.ddoctor.utils.MyUtils;
import com.ddoctor.utils.ToastUtil;
import com.umeng.analytics.MobclickAgent;
import com.zhuge.analysis.stat.ZhugeSDK;

public class OrderDetailActivity extends BaseActivity implements
		OnClickListener {

	private OrderBean _orderBean = null;
	private List<ProductBean> _dataList = new ArrayList<ProductBean>();

	private ListView _listView;
	private RelativeLayout _buttonLayout;

	// 金额
	private TextView _totalPriceTextView;
	private TextView _orderTimeTextView;
	private TextView _expressNameTextView;
	private TextView _expressNumTextView;
	private TextView _addressTextView;
	private TextView _contactTextView;
	private TextView _orderStatusTextView;

	private Button _payButton;

	private int _orderId = 0; // 订单号

	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("OrderDetailActivity");
		MobclickAgent.onResume(OrderDetailActivity.this);
		ZhugeSDK.getInstance().init(getApplicationContext());
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("OrderDetailActivity");
		MobclickAgent.onPause(OrderDetailActivity.this);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_shop_order_pay);

		_orderId = this.getIntent().getIntExtra("orderId", 0);

		initUI();

		showContentUI(false);

		loadData();
	}

	private void showContentUI(boolean show) {
		if (show) {
			_buttonLayout.setVisibility(View.VISIBLE);
			_listView.setVisibility(View.VISIBLE);
		} else {
			_buttonLayout.setVisibility(View.INVISIBLE);
			_listView.setVisibility(View.INVISIBLE);
		}
	}

	protected void initUI() {
		this.setTitle("订单详情");
		Button leftButton = this.getLeftButtonText("返回");

		RelativeLayout rl = (RelativeLayout) findViewById(R.id.titleBar);
		if (rl != null) {
			rl.setBackgroundColor(getResources().getColor(
					R.color.default_titlebar));
		}
		leftButton.setOnClickListener(this);

		_listView = (ListView) findViewById(R.id.listView);

		_buttonLayout = (RelativeLayout) findViewById(R.id.rel_pay);

		LinearLayout footerLayout = (LinearLayout) getLayoutInflater().inflate(
				R.layout.layout_shop_order_pay_footer, null);
		_listView.addFooterView(footerLayout);

		_totalPriceTextView = (TextView) footerLayout
				.findViewById(R.id.tv_shop_good_price);
		_orderTimeTextView = (TextView) footerLayout
				.findViewById(R.id.tv_shop_order_time);
		_expressNameTextView = (TextView) footerLayout
				.findViewById(R.id.tv_express_name);
		_expressNumTextView = (TextView) footerLayout
				.findViewById(R.id.tv_express_num);

		_addressTextView = (TextView) footerLayout
				.findViewById(R.id.tv_pay_address);
		_contactTextView = (TextView) footerLayout
				.findViewById(R.id.tv_pay_contact);
		_orderStatusTextView = (TextView) footerLayout
				.findViewById(R.id.tv_order_state);

		_payButton = (Button) findViewById(R.id.btn_order_pay);
		_payButton.setOnClickListener(this);
	}

	private void updateUI() {
		MyUtils.showLog("updateUI "+_orderBean.toString());
		_totalPriceTextView.setText(String.format(Locale.CHINESE,"￥%.2f", _orderBean.getTotalDiscount()));

		_orderTimeTextView.setText(_orderBean.getTime());

		if (_orderBean.getExpress() != null) {
			_expressNameTextView.setText(_orderBean.getExpress().getName());
			_expressNumTextView.setText(_orderBean.getExpress().getExpressNO());
		}

		if (_orderBean.getDeliver() != null) {
			_addressTextView.setText(_orderBean.getDeliver().getAddress());
			_contactTextView.setText(_orderBean.getDeliver().getName() + " "
					+ _orderBean.getDeliver().getMobile());
		}

		// int status = _orderBean.getOrderStatus();
		int status = _orderBean.getPayStatus();
		// _orderStatusTextView.setText(DataModule.getOrderStatusName(status));
		_orderStatusTextView.setText(DataModule.getPayStatusName(status));

		// 根据订单状态，显示不同的按钮
//		if (status == OrderStatus.NOPAY) // 未支付，显示支付按钮
			if (status == PayStatus.NOPAY) // 根据支付状态  未支付，显示支付按钮  
		{
			_payButton.setText("立即支付");
			_payButton.setTag(1);
		} else {
			_payButton.setText("返回");
			_payButton.setTag(0);
		}

		List<ProductBean> dl = _orderBean.getProducts();
		if (_dataList.size()>0) {
			_dataList.clear();
		}
		if (dl != null && dl.size() > 0)
			_dataList.addAll(dl);

		PayAdapter adapter = new PayAdapter(this);
		_listView.setAdapter(adapter);
		adapter.notifyDataSetChanged();

	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.btn_left: {
			finishThisActivity();

		}
			break;
		case R.id.btn_order_pay: {
			int tag = (Integer) _payButton.getTag();
			if (tag == 1)
				on_btn_pay();
			else
				finishThisActivity();
		}
			break;
		default:
			break;
		}
	}

	public class PayAdapter extends BaseAdapter<ProductBean> {

		public PayAdapter(Context context) {
			super(context);
		}

		@Override
		public int getCount() {
			MyUtils.showLog("data count=" + _dataList.size());
			return _dataList.size();
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			ValueHolder valueHolder = null;
			if (null == convertView) {
				convertView = inflater.inflate(R.layout.layout_pay_item,
						parent, false);
				valueHolder = new ValueHolder();
				valueHolder.imgView = (ImageView) convertView
						.findViewById(R.id.imgView);
				valueHolder.tv_name = (TextView) convertView
						.findViewById(R.id.tv_name);
				valueHolder.tv_price = (TextView) convertView
						.findViewById(R.id.tv_price);
				valueHolder.tv_num = (TextView) convertView
						.findViewById(R.id.tv_num);
				convertView.setTag(valueHolder);

			} else {
				valueHolder = (ValueHolder) convertView.getTag();
			}

			valueHolder.dataIndex = position;

			valueHolder.imgView.setImageDrawable(null);

			ProductBean productBean = _dataList.get(position);

			ImageLoaderUtil.display(productBean.getImage(),
					valueHolder.imgView, R.drawable.ic_launcher);

			valueHolder.tv_name.setText(String.valueOf(productBean.getName()));
			valueHolder.tv_price.setText(String.format("￥%.2f",
					productBean.getDiscount()));
			valueHolder.tv_num.setText(String.format("x%d",
					productBean.getCount()));

			return convertView;
		}

		private class ValueHolder {
			private ImageView imgView;
			private TextView tv_name;
			private TextView tv_price;
			private TextView tv_num;

			private int dataIndex;
		}
	}

	private void on_btn_pay() {
		// 选择付款方式
		RelativeLayout rootLayout = (RelativeLayout) findViewById(R.id.rootLayout);
		doSelectPayType(rootLayout);

	}

	private Dialog _loadingDialog = null;

	private void loadData() {
		_loadingDialog = DialogUtil.createLoadingDialog(this, "加载中...");
		_loadingDialog.show();

		GetOrderDetailTask task = new GetOrderDetailTask(_orderId);
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {
			@Override
			public void taskFinish(DDResult result) {

				if (_loadingDialog != null) {
					_loadingDialog.dismiss();
					_loadingDialog = null;
				}

				if (result.getError() == RetError.NONE) {
					on_task_finished(result);
				} else {
					on_task_failed(result);
				}

			}
		});
		task.executeParallel("");

	}

	private void on_task_finished(DDResult result) {
		_orderBean = (OrderBean) result.getObject();

		updateUI();
		showContentUI(true);
	}

	private void on_task_failed(DDResult result) {
		ToastUtil.showToast(result.getErrorMessage());
	}

	public void doSelectPayType(View parent) {

		View view = View.inflate(this, R.layout.layout_pay_type_popwin, null);

		view.startAnimation(AnimationUtils.loadAnimation(this,
				R.anim.slide_in_from_bottom));
		view.setFocusable(true);
		view.setFocusableInTouchMode(true);

		final PopupWindow popupWindow = new PopupWindow(view,
				WindowManager.LayoutParams.MATCH_PARENT,
				WindowManager.LayoutParams.WRAP_CONTENT);
		popupWindow.setBackgroundDrawable(new ColorDrawable(0x00000000));

		WindowManager.LayoutParams lp = getWindow().getAttributes();
		lp.alpha = 0.4f;
		getWindow().setAttributes(lp);

		// 设置点击窗口外边窗口消失
		popupWindow.setOutsideTouchable(true);

		// 设置此参数获得焦点，否则无法点击
		popupWindow.setFocusable(true);
		popupWindow.showAtLocation(parent, Gravity.BOTTOM, 0, 0);
		popupWindow.update();
		popupWindow.setOnDismissListener(new OnDismissListener() {

			@Override
			public void onDismiss() {
				// TODO Auto-generated method stub
				WindowManager.LayoutParams lp = getWindow().getAttributes();
				lp.alpha = 1f;
				getWindow().setAttributes(lp);
			}
		});

		TextView cancelTextView = (TextView) view.findViewById(R.id.btn_cancel);
		cancelTextView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				popupWindow.dismiss();
			}

		});

		RelativeLayout rl;

		rl = (RelativeLayout) view.findViewById(R.id.rl_paytype_zfb);
		rl.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				aliPay();
				popupWindow.dismiss();
			}
		});

		rl = (RelativeLayout) view.findViewById(R.id.rl_paytype_wx);
		rl.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				popupWindow.dismiss();
			}
		});

		view.setOnKeyListener(new OnKeyListener() {

			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				// TODO Auto-generated method stub
				if (keyCode == KeyEvent.KEYCODE_BACK) {
					popupWindow.dismiss();
					return true;
				}
				return false;
			}
		});

	}

	private Handler mHandler = new Handler() {
		public void handleMessage(Message msg) {
			PayResult payResult = new PayResult((String) msg.obj);

			// 支付宝返回此次支付结果及加签，建议对支付宝签名信息拿签约时支付宝提供的公钥做验签
			String resultInfo = payResult.getResult();

			String resultStatus = payResult.getResultStatus();

			// 判断resultStatus 为“9000”则代表支付成功，具体状态码代表含义可参考接口文档
			if (TextUtils.equals(resultStatus, "9000")) {
				OrderBean orderBean = new OrderBean();
				orderBean.setId(_orderId);
				orderBean.setPayStatus(2);
				orderBean.setMessage(resultInfo);
				synchPayStatus(orderBean);
			} else {
				// 判断resultStatus 为非“9000”则代表可能支付失败
				// “8000”代表支付结果因为支付渠道原因或者系统原因还在等待支付结果确认，最终交易是否成功以服务端异步通知为准（小概率状态）
				if (TextUtils.equals(resultStatus, "8000")) {
					Toast.makeText(OrderDetailActivity.this, "支付结果确认中",
							Toast.LENGTH_SHORT).show();

				} else {
					// 其他值就可以判断为支付失败，包括用户主动取消支付，或者系统返回的错误
					Toast.makeText(OrderDetailActivity.this, "支付失败",
							Toast.LENGTH_SHORT).show();

				}
			}
		};
	};

	private void synchPayStatus(OrderBean orderBean) {
		addEvent("400002", getString(R.string.event_pay_400002));
		SynchPayStatusTask task = new SynchPayStatusTask(orderBean);
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {

			@Override
			public void taskFinish(DDResult result) {
				if (result.getError() == RetError.NONE) {
					Toast.makeText(OrderDetailActivity.this, "支付成功",
							Toast.LENGTH_SHORT).show();
					loadData();
				}
			}
		});
		task.executeParallel("");
	}


	/**
	 * 添加自定义事件
	 * @param eventId
	 * @param eventName
	 */
	private void addEvent(String eventId , String eventName){
		MobclickAgent.onEvent(this, eventId);
		ZhugeSDK.getInstance().onEvent(this, eventName);
	}
	
	/**
	 * 支付宝异步通知地址 zfburl 传递到支付宝的价格 totalPrice 还是 totalPayPrice 订单id
	 * _orderBean.getId 为null 所以写的_orderId
	 */
	private void aliPay() {
//		String zfbUrl = "http://api.ddoctor.cn/zfb";
		 String zfbUrl = DataModule.getInstance().getZfbUrl();
		 if (TextUtils.isEmpty(zfbUrl)) {
			zfbUrl = "http://api.ddoctor.cn/zfb";
		}
		String orderInfo = AliPayUtil.getOrderInfo(_orderBean.getFullname(),
				_orderBean.getFullname(), _orderBean.getTotalPrice(),
				String.valueOf(_orderId), zfbUrl);

		String sign = AliPayUtil.sign(orderInfo, AliPayConstant.RSA_PRIVATE);
		try {
			// 仅需对sign 做URL编码
			sign = URLEncoder.encode(sign, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		// 完整的符合支付宝参数规范的订单信息
		final String payInfo = orderInfo + "&sign=\"" + sign + "\"&"
				+ AliPayUtil.getSignType();

		Runnable payRunnable = new Runnable() {

			@Override
			public void run() {
				// 构造PayTask 对象
				PayTask alipay = new PayTask(OrderDetailActivity.this);
				// 调用支付接口，获取支付结果
				String result = alipay.pay(payInfo);

				Message msg = new Message();
				msg.obj = result;
				mHandler.sendMessage(msg);
			}
		};

		// 必须异步调用
		Thread payThread = new Thread(payRunnable);
		payThread.start();
	}

}
