package com.ddoctor.user.activity.shop;

import java.util.ArrayList;
import java.util.List;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.enums.RetError;
import com.ddoctor.user.activity.BaseActivity;
import com.ddoctor.user.adapter.BaseAdapter;
import com.ddoctor.user.model.AddressBean;
import com.ddoctor.user.task.DeleteRecordTask;
import com.ddoctor.user.task.GetShopAddressListTask;
import com.ddoctor.user.task.TaskPostCallBack;
import com.ddoctor.user.wapi.bean.DeliverBean;
import com.ddoctor.user.wapi.constant.Record;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.DialogUtil;
import com.ddoctor.utils.DialogUtil.ConfirmDialog;
import com.ddoctor.utils.MyUtils;
import com.ddoctor.utils.ToastUtil;
import com.umeng.analytics.MobclickAgent;
import com.zhuge.analysis.stat.ZhugeSDK;

public class ShopAddressListActivity extends BaseActivity implements
		OnItemClickListener,OnItemLongClickListener {
	
	private TextView _tv_norecord;
	private ListView _listView;
	private Button _bottomButton;
	private List<MyDeliverBean> _dataList = new ArrayList<MyDeliverBean>();

	private AddressListAdapter _adapter;
	
	private Dialog _loadingDialog = null;

	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("ShopAddressListActivity");
		MobclickAgent.onResume(ShopAddressListActivity.this);
		ZhugeSDK.getInstance().init(getApplicationContext());
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("ShopAddressListActivity");
		MobclickAgent.onPause(ShopAddressListActivity.this);
	}

	private int _currentId = 0; // 订单确认页面使用的地址
	private boolean _isCurrentAddressDel = false; // 是否删除了订单确认页当前正使用的地址
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_shop_addresslist);
		_currentId = getIntent().getIntExtra("deliverid", 0);
		initUI();
		showContentUI(false);
		
		_adapter = new AddressListAdapter(this);
		_listView.setAdapter(_adapter);
		
		setResult(0, null); // 默认返回值
		
		loadDataList();
	}
	
	private void loadDataList()
	{
		
		_loadingDialog = DialogUtil.createLoadingDialog(this, "加载中...");
		_loadingDialog.show();
		
		GetShopAddressListTask task = new GetShopAddressListTask();
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {
			
			@Override
			public void taskFinish(DDResult result) {
				if (result.getError() == RetError.NONE  )
				{
					List<DeliverBean> tmpList = (List<DeliverBean>)result.getBundle().getSerializable("list");
						// 加载第一页
					_dataList.clear();
					for(int i = 0; i < tmpList.size(); i++ )
					{
						MyDeliverBean mdb = new MyDeliverBean();
						mdb.deliverBean = tmpList.get(i);
						mdb.bSelected = false;
						_dataList.add(mdb);
					}
					_adapter.notifyDataSetChanged();
					
					showContentUI(true);
						
					if( _loadingDialog != null )
						_loadingDialog.dismiss();
					
					if( _dataList.size() == 0 ){
						_tv_norecord.setText(result.getErrorMessage());
						_tv_norecord.setVisibility(View.VISIBLE);
					}
				}
				else 
				{// 加载失败
					if( _loadingDialog != null )
						_loadingDialog.dismiss();

					ToastUtil.showToast(result.getErrorMessage());
				}
			}
		});
		
		task.executeParallel("");
	}
	
	private void showContentUI(boolean show)
	{
		RelativeLayout rl = (RelativeLayout)findViewById(R.id.buttonBarLayout);
		
		if( show )
		{
			rl.setVisibility(View.VISIBLE);
			_listView.setVisibility(View.VISIBLE);
		}
		else
		{
			rl.setVisibility(View.INVISIBLE);
			_listView.setVisibility(View.INVISIBLE);
		}
		
	}

	protected void initUI() {
		setTitle(getResources().getString(R.string.sc_addressmanager));
		RelativeLayout rl = (RelativeLayout) findViewById(R.id.titleBar);
		if (rl != null) {
			rl.setBackgroundColor(getResources().getColor(
					R.color.default_titlebar));
		}

		getLeftButton().setText("返回");
		getRightButton().setText("编辑");
		
		getRightButton().setTag(0); // 用tag记录编辑状态 0_选择状态 1_编辑状态
		
		getLeftButton().setOnClickListener(this);
		getRightButton().setOnClickListener(this);
		getLeftButton().setVisibility(View.VISIBLE);
		getRightButton().setVisibility(View.VISIBLE);
		
		
		
		_bottomButton = (Button) findViewById(R.id.btn_add);
		_bottomButton.setText(getResources().getString(R.string.sc_addnewaddress));
		_bottomButton.setOnClickListener(this);
		
		_listView = (ListView) findViewById(R.id.listView);
		
		_listView.setOnItemClickListener(this);
		_listView.setOnItemLongClickListener(this);
		_tv_norecord = (TextView) findViewById(R.id.tv_norecord);
		_tv_norecord.setVisibility(View.INVISIBLE);
		_listView.setEmptyView(_tv_norecord);
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		finishBack();
		return super.onKeyDown(keyCode, event);
	}
	
	private void finishBack(){
		if (_isCurrentAddressDel) {
			Intent intent = new Intent();
			Bundle b = new Bundle();
			b.putBoolean("del", _isCurrentAddressDel);
			DeliverBean deliverBean = new DeliverBean();
			deliverBean.setId(0);
			deliverBean.setName("");
			deliverBean.setMobile("");
			deliverBean.setAddress("");
			b.putSerializable("data", deliverBean);
			intent.putExtras(b);
			setResult(1, intent);
		}
		finishThisActivity();
	}
	
	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.btn_left: {
			finishBack();
		}
			break;
		case R.id.btn_right: {
			on_btn_edit();
		}
			break;
		case R.id.btn_add: {
			int n = (Integer)getRightButton().getTag();
			if( n == 1 )
				on_btn_del();
			else
				on_btn_add();
		}
			break;
		default:
			break;
		}
	}
	
	private void on_btn_edit()
	{
		int flag = (Integer)getRightButton().getTag();
		if( flag == 0 )
		{
			getRightButton().setTag(1);
			getRightButton().setText("完成");
			_bottomButton.setText("删除");

			for(int i = 0; i < _dataList.size(); i++ )
			{
				_dataList.get(i).bShowCheckbox = true;
				_dataList.get(i).bSelected = false;
			}
		}
		else
		{
			getRightButton().setTag(0);
			getRightButton().setText("编辑");
			_bottomButton.setText(getResources().getString(R.string.sc_addnewaddress));

			for(int i = 0; i < _dataList.size(); i++ )
			{
				_dataList.get(i).bShowCheckbox = false;
				_dataList.get(i).bSelected = false;
			}
		}
		
		_adapter.notifyDataSetChanged();
		
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {

		MyDeliverBean mdb = _dataList.get(position);

		int n = (Integer)getRightButton().getTag();
		if( n == 1 ) // 编辑状态
		{
			mdb.bSelected = !mdb.bSelected;
			_adapter.notifyDataSetChanged();
		}
		else
		{ // 选择状态
			
			Intent intent = new Intent();
			Bundle b = new Bundle();
			b.putSerializable("data", mdb.deliverBean);
			intent.putExtras(b);
			
			setResult(1, intent);
			finish();
		}
	}
	

	@Override
	public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		
		// 提示删除
		DialogUtil.confirmDialog(this, "确实要清空购物车吗？",  "清空", "取消", new ConfirmDialog(){

			@Override
			public void onOKClick(Bundle data) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onCancleClick() {
				// TODO Auto-generated method stub
				
			}
			
		}
		);
		
		return false;
	}	
	
	
	
	public class AddressListAdapter extends BaseAdapter<AddressBean>{

		public AddressListAdapter(Context context) {
			super(context);
			// TODO Auto-generated constructor stub
		}
		
		@Override
		public int getCount() {
			return ( _dataList == null ) ? 0:  _dataList.size();
		}
		
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder = null;
			if (null == convertView) {
				convertView = inflater.inflate(R.layout.layout_addresslist_item, parent , false);
				holder = new ViewHolder();
				holder.tv_name = (TextView) convertView.findViewById(R.id.addresslist_item_tv_name);
				holder.tv_phone = (TextView) convertView.findViewById(R.id.addresslist_item_tv_phone);
				holder.tv_address = (TextView) convertView.findViewById(R.id.addresslist_item_tv_address);
				holder.rl_checkBox = (RelativeLayout)convertView.findViewById(R.id.rl_checkbox);
				holder.iv_check_flag = (ImageView)convertView.findViewById(R.id.flagImageView);
				
				convertView.setTag(holder);
			} 
			else 
			{
				holder = (ViewHolder) convertView.getTag();
			}
			
			holder.dataIndex = position;
			
			MyDeliverBean mdb = _dataList.get(position);
			
			holder.rl_checkBox.setVisibility(mdb.bShowCheckbox ? View.VISIBLE : View.GONE);
			if( mdb.bShowCheckbox )
			{
				if( mdb.bSelected )
					holder.iv_check_flag.setBackgroundResource(R.drawable.flag_checked);
				else
					holder.iv_check_flag.setBackgroundResource(R.drawable.flag_uncheck);
			}
			
			holder.tv_name.setText(mdb.deliverBean.getName());
			holder.tv_phone.setText(mdb.deliverBean.getMobile());

			holder.tv_address.setText(mdb.deliverBean.getAddress());
			
			return convertView;
		}

		private class ViewHolder{
			private TextView tv_name;
			private TextView tv_phone;
			private TextView tv_address;
			
			private RelativeLayout rl_checkBox;
			private ImageView iv_check_flag;
			
			private int dataIndex;
		}
		
	}
	
	private void on_btn_del()
	{
		// 不用提示确认了，用户已经选择确认了
		StringBuffer idList = new StringBuffer();
		for(int i = 0; i < _dataList.size(); i++ )
		{
			if( !_dataList.get(i).bSelected )
				continue;

			if( idList.length() > 0 )
				idList.append(",");
			
			idList.append("" + _dataList.get(i).deliverBean.getId());
			
			MyUtils.showLog(i + " selected");
		}
		
		if( idList.length() == 0 )
		{
			ToastUtil.showToast("请选择要删除的记录！");
			return;
		}
		
		_loadingDialog = DialogUtil.createLoadingDialog(this, "加载中...");
		_loadingDialog.show();
		
		final String records = idList.toString();
		MyUtils.showLog("选择删除的地址有  "+records);
		//TODO: 调用删除接口
		DeleteRecordTask task = new DeleteRecordTask(records, Record.DELIVER);
		task.setTaskCallBack(new TaskPostCallBack<RetError>() {
			
			@Override
			public void taskFinish(RetError result) {
				_loadingDialog.dismiss();
				
				if (result == RetError.NONE  )
				{
					if (records.contains(String.valueOf(_currentId))) {
						_isCurrentAddressDel = true;
					}
					
					do_delete();
				}
				else
				{
					ToastUtil.showToast(result.getErrorMessage());
				}
			}
		});
		
		task.executeParallel("");
		
	}
	
	private void do_delete()
	{
		
		int cnt = _dataList.size();
		for(int i = cnt -1 ; i >= 0; i-- )
		{
			if( !_dataList.get(i).bSelected )
				continue;

			_dataList.remove(i);
		}
		
		_adapter.notifyDataSetChanged();
		
		ToastUtil.showToast("删除成功！");
	}
	
	private void on_btn_add()
	{
		// AddressManagerActivity会把新增的数据传递过来，这样这个界面的列表就不用去server刷新了，直接加数据刷新页面即可
		Intent intent = new Intent(this, AddressManagerActivity.class);
		startActivityForResult(intent, 0);
		
	}
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		if( resultCode == 1 )
		{
		    Bundle b = data.getExtras(); //data为B中回传的Intent
		    DeliverBean db = (DeliverBean)b.getSerializable("data");
		    if( db != null )
		    {
		    	// 刷新列表
		    	MyDeliverBean mdb = new MyDeliverBean();
		    	mdb.bSelected = false;
		    	mdb.deliverBean = db;
		    	_dataList.add(0, mdb);
		    	_adapter.notifyDataSetChanged();
		    }
		}
	}
	
	// 定义这个的目的就是加入一个bSelected记录选中状态（尽量不改Server提供的DeliverBean)
	private class MyDeliverBean// extends DeliverBean // 不用继承，后面可能需要传递deliverBean参数,MyDeliverBean转成DeliverBean可能麻烦点
	{
		boolean bSelected = false;
		boolean bShowCheckbox = false;
		
		DeliverBean deliverBean;
	}

}
