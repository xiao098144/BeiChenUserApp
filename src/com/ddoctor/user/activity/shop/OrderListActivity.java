package com.ddoctor.user.activity.shop;

/**
 * 商城
 * 康
 */
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.enums.RefreshAction;
import com.ddoctor.enums.RetError;
import com.ddoctor.user.activity.BaseActivity;
import com.ddoctor.user.adapter.BaseAdapter;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.task.GetShopOrderListTask;
import com.ddoctor.user.task.TaskPostCallBack;
import com.ddoctor.user.view.DDPullToRefreshView;
import com.ddoctor.user.view.DDPullToRefreshView.OnHeaderRefreshListener;
import com.ddoctor.user.wapi.bean.OrderBean;
import com.ddoctor.user.wapi.bean.ProductBean;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.DialogUtil;
import com.ddoctor.utils.ImageLoaderUtil;
import com.ddoctor.utils.MyUtils;
import com.ddoctor.utils.ToastUtil;
import com.umeng.analytics.MobclickAgent;
import com.zhuge.analysis.stat.ZhugeSDK;

public class OrderListActivity extends BaseActivity implements
		OnHeaderRefreshListener, OnScrollListener, OnItemClickListener {
	RelativeLayout rl;

	
	private final int ROW_TYPE_DIVIDER 	= 0;
	private final int ROW_TYPE_TIME 	= 1;
	private final int ROW_TYPE_ITEM 	= 2;
	private final int ROW_TYPE_SUMMARY 	= 3;
	private final int ROW_TYPE_COUNT 	= 4;
	

	private ListView _listView;
	DDPullToRefreshView _refreshViewContainer;
	private View _getMoreView;
	private TextView _tv_norecord;

	private List<OrderBean> _orderDataList = new ArrayList<OrderBean>();
	private List<DataBean> _dataList = new ArrayList<DataBean>();
	/**
	 * 商品列表适配器
	 */
	private OrderListAdapter _adapter;

	private int _pageNum = 1;
	private RefreshAction _refreshAction = RefreshAction.PULLTOREFRESH;
	
	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("OrderListActivity");
		MobclickAgent.onResume(OrderListActivity.this);
		ZhugeSDK.getInstance().init(getApplicationContext());
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("OrderListActivity");
		MobclickAgent.onPause(OrderListActivity.this);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_shop_order_list);
		
		initUI();
		
		loadingData(true, _pageNum);
	}

	private void initUI() {
		this.setTitle("我的订单");
		
		rl = (RelativeLayout) findViewById(R.id.titleBar);
		if (rl != null)
			rl.setBackgroundColor(getResources().getColor(R.color.default_titlebar));
		
		getLeftButton().setText("返回");
		getLeftButton().setOnClickListener(this);
		getLeftButton().setVisibility(View.VISIBLE);
		
		_refreshViewContainer = (DDPullToRefreshView) findViewById(R.id.refreshViewContainer);
		_refreshViewContainer.setOnHeaderRefreshListener(this);
		_refreshViewContainer.setVisibility(View.INVISIBLE);
		
		_listView = (ListView) findViewById(R.id.listView);
		_listView.setOnItemClickListener(this);
		_listView.setOnScrollListener(this);
		_tv_norecord = (TextView) findViewById(R.id.tv_norecord);
		_tv_norecord.setText("您还没有购买商品哦，快去商城看看吧");
		_tv_norecord.setVisibility(View.INVISIBLE);
		_listView.setEmptyView(_tv_norecord);
		
		initList();
	}
	
	
	private void makeDataList(List<OrderBean> orderList)
	{
		int startIndex = _orderDataList.size();
		
		for(int i = 0; i < orderList.size(); i++ )
		{
			DataBean db;
			
			if( _dataList.size() > 0 )
			{
				db = new DataBean();
				db.rowType = ROW_TYPE_DIVIDER;
				_dataList.add(db);
			}
			
			OrderBean orderBean = orderList.get(i);
			db = new DataBean();
			db.orderIndex = startIndex + i;
			db.rowType = ROW_TYPE_TIME;
			_dataList.add(db);
			
			// 下面的商品
			List<ProductBean> productList = orderBean.getProducts();
			if( productList != null )
			{
				for(int j = 0; j < productList.size(); j++ )
				{
					db = new DataBean();
					db.orderIndex = startIndex + i;
					db.productIndex = j;
					db.rowType = ROW_TYPE_ITEM;
					_dataList.add(db);
				}
			}
			
			// 统计项
			db = new DataBean();
			db.orderIndex = startIndex + i;
			db.rowType = ROW_TYPE_SUMMARY;
			_dataList.add(db);
		}
	}
	
//	private void showContentUI(boolean show)
//	{
//		if( show )
//		{
//			_refreshViewContainer.setVisibility(View.VISIBLE);
//		}
//		else
//		{
//			_refreshViewContainer.setVisibility(View.INVISIBLE);
//		}
//	}
	
	private String loadingStr;

	private void initList() {
		// 获取更多
		_getMoreView = 	createGetMoreView();
		setGetMoreContent("已全部加载", false, false);
		_listView.addFooterView(_getMoreView);

		// 数据
		_adapter = new OrderListAdapter(this);
		_listView.setAdapter(_adapter);
	}
	
	// 加载更多相关函数 >>>>>>
	boolean _bGetMoreEnable = false;
	private View createGetMoreView()
	{
		if( _getMoreView != null )
			return _getMoreView;
		
		View v = (View) getLayoutInflater().inflate(R.layout.refresh_footer, null);
		
		return v;
	}
	
	private void setGetMoreContent(String message, boolean showImage, boolean animation)
	{
		TextView tv = (TextView)_getMoreView.findViewById(R.id.pull_to_load_text);
		tv.setText(message);
		
		ImageView imgView = (ImageView)_getMoreView.findViewById(R.id.pull_to_load_image);
		AnimationDrawable ad = (AnimationDrawable) imgView.getBackground();
		if( showImage )
		{
			if( animation )
			{
				ad.start();
			}
			else
			{
				ad.stop();
				ad.selectDrawable(0);
			}
				
			imgView.setVisibility(View.VISIBLE);
		}
		else
		{
			ad.stop();
			ad.selectDrawable(0);
	
			imgView.setVisibility(View.GONE);
		}
	}
	
	// <<<<< 获取更多相关函数
	

	private Dialog _loadingDialog = null;

	private void loadingData(boolean showLoading, int page)
	{
		if( showLoading )
		{
			_loadingDialog = DialogUtil.createLoadingDialog(this, "加载中...");
			_loadingDialog.show();
		}
		
		GetShopOrderListTask task = new GetShopOrderListTask(page);
		
		final int page1 = page;
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {
			
			@Override
			public void taskFinish(DDResult result) {
				if (result.getError() == RetError.NONE  )
				{
					List<OrderBean> tmpList = (List<OrderBean>)result.getObject();
					if( page1 > 1 ) // 加载更多
					{
						makeDataList(tmpList); // 必须先调用这个
						_orderDataList.addAll(tmpList);
						
						_adapter.notifyDataSetChanged();
					}
					else
					{
						// 加载第一页
						_orderDataList.clear();
						_dataList.clear();
						makeDataList(tmpList); // 必须先调用这个
						_orderDataList.addAll(tmpList);
						_adapter.notifyDataSetChanged();
						
						_refreshViewContainer.onHeaderRefreshComplete();
						_refreshViewContainer.setVisibility(View.VISIBLE);
						_refreshAction = RefreshAction.NONE;
						
						if( _loadingDialog != null )
							_loadingDialog.dismiss();
						
					}

					// 是否显示"加载更多"
					if( tmpList.size() > 0 )
					{
						setGetMoreContent("滑动加载更多", true, false);
						_bGetMoreEnable = true;
					}
					else
					{	
						if (page1 == 1) {
							_tv_norecord.setText(result.getErrorMessage());
							_tv_norecord.setVisibility(View.VISIBLE);
							_tv_norecord.setTag(0);
						}
						// 没数据了，加载完成
						setGetMoreContent("已全部加载", false, false);
						_bGetMoreEnable = false;
					}
					
					// 确保加载成功后，再修改这个变量
					_pageNum = page1;
				}
				else 
				{// 加载失败
					if( page1 > 1 )
					{
						setGetMoreContent("滑动加载更多", true, false);
					}
					else
					{
						_refreshViewContainer.onHeaderRefreshComplete();
						_refreshAction = RefreshAction.NONE;
						
						if( _loadingDialog != null )
							_loadingDialog.dismiss();
					}

					ToastUtil.showToast(result.getErrorMessage());
				}
				
				_refreshAction = RefreshAction.NONE;
			}
		});
		
		task.executeParallel("");
	}
	
	

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.btn_left:
			finish();
			break;
		default:
			break;
		}

	}

	@Override
	public void onScroll(AbsListView arg0, int firstVisibleItem, int arg2,
			int arg3) {
		
		// 工具栏的显示与隐藏		
		if( _refreshAction == RefreshAction.NONE )
		{
			if( _bGetMoreEnable )
			{// 有加载更多
				int lastPos = _listView.getLastVisiblePosition();
				int total = _listView.getHeaderViewsCount() + _dataList.size() + _listView.getFooterViewsCount();
				if( lastPos == total - 1 )
				{
					// 加载更多显示出来了，开始加载
					_refreshAction = RefreshAction.LOADMORE;
					setGetMoreContent("正在加载...", true, true);
					loadingData(false, _pageNum + 1);
				}
			}
		}
		

	}

	@Override
	public void onScrollStateChanged(AbsListView arg0, int arg1) {

	}


	@Override
	public void onHeaderRefresh(DDPullToRefreshView view) {
		if( _refreshAction == RefreshAction.NONE )
		{
			_refreshAction = RefreshAction.PULLTOREFRESH;
			loadingData(false, 1);
		}
		else
		{
			// 正在加载，什么也不做
			view.onHeaderRefreshComplete();
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

		// 进去商品详情页
		int dataIdx = arg2 - _listView.getHeaderViewsCount();
		
		if( dataIdx >= _dataList.size() )
			return;
		
		DataBean db = _dataList.get(dataIdx);
		if( db.rowType == ROW_TYPE_DIVIDER )
			return;
		
		OrderBean orderBean = _orderDataList.get(db.orderIndex);
		
		Intent intent = new Intent(this, OrderDetailActivity.class);
		intent.putExtra("orderId", orderBean.getId());
		startActivity(intent);
	}
	
	public class OrderListAdapter extends BaseAdapter<ProductBean> {

		public OrderListAdapter(Context context) {
			super(context);

		}

		@Override
		public int getCount() {
			return _dataList.size();
		}
		


		@Override
		public int getItemViewType(int position) {
			DataBean db = _dataList.get(position);
			return db.rowType;
		}
	
	
		@Override
		public int getViewTypeCount() {
			return ROW_TYPE_COUNT;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			DataBean dataBean = _dataList.get(position);
			
			int type = getItemViewType(position);
			if( type == ROW_TYPE_ITEM )
			{
				OrderBean orderBean = _orderDataList.get(dataBean.orderIndex);

				ValueHolder valueHolder = null;
				if (null == convertView) {
					convertView = inflater.inflate(R.layout.layout_pay_item,
							parent, false);
					valueHolder = new ValueHolder();
					valueHolder.imgView = (ImageView)convertView.findViewById(R.id.imgView);
					valueHolder.tv_name = (TextView) convertView.findViewById(R.id.tv_name);
					valueHolder.tv_price = (TextView) convertView.findViewById(R.id.tv_price);
					valueHolder.tv_num = (TextView) convertView.findViewById(R.id.tv_num);
					convertView.setTag(valueHolder);
					
				} else {
					valueHolder = (ValueHolder) convertView.getTag();
				}
				
				valueHolder.dataIndex = position;
				
				valueHolder.imgView.setImageDrawable(null);
				
				ProductBean productBean = orderBean.getProducts().get(dataBean.productIndex);
		
				ImageLoaderUtil.display(productBean.getImage(), valueHolder.imgView, R.drawable.ic_launcher);
				
				valueHolder.tv_name.setText(String.valueOf(productBean.getName()));
				valueHolder.tv_price.setText(String.format("￥%.2f", productBean.getDiscount()));
//				valueHolder.tv_price.getPaint().setFlags(
//						Paint.STRIKE_THRU_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
				valueHolder.tv_num.setText(String.format("x%d", productBean.getCount()));
				
				return convertView;
			}
			else if( type == ROW_TYPE_SUMMARY )
			{
				OrderBean orderBean = _orderDataList.get(dataBean.orderIndex);
				if( convertView == null )
				{
					RelativeLayout.LayoutParams rlp;
						
					RelativeLayout rl = new RelativeLayout(getContext());
					rl.setBackgroundColor(0xFFF5F5F5);
						
					TextView tv;

					tv = new TextView(getContext());
					tv.setId(100);
					tv.setText("合计：");
					tv.setTextColor(Color.argb(255, 102,102,102));
					rlp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
							RelativeLayout.LayoutParams.WRAP_CONTENT);
					rlp.addRule(RelativeLayout.CENTER_VERTICAL);
					rlp.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
					rlp.topMargin = MyUtils.dp2px(10, getContext());
					rlp.bottomMargin = MyUtils.dp2px(10, getContext());
					rlp.leftMargin = MyUtils.dp2px(5, getContext());
					rl.addView(tv, rlp);
					
					tv = new TextView(getContext());
					tv.setId(200);
					tv.getPaint().setFakeBoldText(true);
					tv.setTextColor(Color.argb(255, 102,102,102));
					rlp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
							RelativeLayout.LayoutParams.WRAP_CONTENT);
					rlp.addRule(RelativeLayout.CENTER_VERTICAL);
					rlp.addRule(RelativeLayout.RIGHT_OF, 100);
					rlp.topMargin = MyUtils.dp2px(10, getContext());
					rlp.bottomMargin = MyUtils.dp2px(10, getContext());
					rlp.leftMargin = MyUtils.dp2px(2, getContext());
					rl.addView(tv, rlp);
					
					tv = new TextView(getContext());
					tv.setId(300);
//					tv.getPaint().setFakeBoldText(true);
					tv.setTextColor(Color.argb(255, 102,102,102));
					
					rlp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
							RelativeLayout.LayoutParams.WRAP_CONTENT);
					rlp.addRule(RelativeLayout.CENTER_VERTICAL);
					rlp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
					rlp.topMargin = MyUtils.dp2px(10, getContext());
					rlp.bottomMargin = MyUtils.dp2px(10, getContext());
					rlp.rightMargin = MyUtils.dp2px(10, getContext());
					rl.addView(tv, rlp);
					

					convertView = rl;
				}
				MyUtils.showLog("" + orderBean.getTotalPrice() + "," + orderBean.getOrderStatus());
				TextView tv = (TextView)convertView.findViewById(200);
				tv.setText(String.format(Locale.CHINESE,"￥%.2f", orderBean.getTotalPayPrice()));
				
				tv = (TextView)convertView.findViewById(300);
//				tv.setText(DataModule.getOrderStatusName(orderBean.getOrderStatus()));  // 停止显示订单状态
				tv.setText(DataModule.getPayStatusName(orderBean.getPayStatus()));  // 修改为显示支付状态   
				
				return convertView;
				
			}
			else if( type ==  ROW_TYPE_TIME )
			{
				OrderBean orderBean = _orderDataList.get(dataBean.orderIndex);
				if( convertView == null )
				{
					LinearLayout.LayoutParams llp;
					
					LinearLayout ll = new LinearLayout(getContext());
					ll.setBackgroundColor(0xFFF5F5F5);
					ll.setOrientation(LinearLayout.HORIZONTAL);
					
					TextView tv = new TextView(getContext());
					tv.setTag(100);
					tv.setTextColor(Color.argb(255, 102,102,102));
					llp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
							LinearLayout.LayoutParams.WRAP_CONTENT);
					llp.gravity = Gravity.CENTER_VERTICAL;
					llp.topMargin = MyUtils.dp2px(10, getContext());
					llp.bottomMargin = MyUtils.dp2px(10, getContext());
					llp.leftMargin = MyUtils.dp2px(5, getContext());
					ll.addView(tv, llp);

					convertView = ll;
				}
				
				TextView tv = (TextView)convertView.findViewWithTag(100);
				tv.setText(orderBean.getTime());
				
				return convertView;
			}
			else // DIVIDER
			{
				if( convertView == null )
				{
					
					LinearLayout ll_root = new LinearLayout(getContext());
					
					LinearLayout ll = new LinearLayout(getContext());
					ll.setBackgroundColor(Color.GRAY);
					ll.setOrientation(LinearLayout.HORIZONTAL);
					
					LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
							MyUtils.dp2px(10, getContext()));
					
					ll_root.addView(ll, llp);
					
					convertView = ll_root;
				}
				
				return convertView;
			}
		}
		
		private class ValueHolder {
			private ImageView imgView;
			private TextView tv_name;
			private TextView tv_price;			
			private TextView tv_num;
			
			private int dataIndex;
		}

	}
	
	private class DataBean
	{
		public int rowType = ROW_TYPE_DIVIDER;
		public int orderIndex = -1;
		public int productIndex = -1;
	}
	
}
