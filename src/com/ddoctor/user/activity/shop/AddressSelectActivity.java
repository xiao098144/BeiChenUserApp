package com.ddoctor.user.activity.shop;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.user.activity.BaseActivity;
import com.ddoctor.user.adapter.BaseAdapter;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.data.MyProfile;
import com.ddoctor.user.wapi.bean.DistrictBean;
import com.ddoctor.utils.MyUtils;
import com.umeng.analytics.MobclickAgent;
import com.zhuge.analysis.stat.ZhugeSDK;

public class AddressSelectActivity extends BaseActivity implements
		OnItemClickListener {

	private ListView _listView;
	private TextView _selectedTextView;

	private List<DistrictBean> _dataList = new ArrayList<DistrictBean>();
	private List<DistrictBean> _selectedList = new ArrayList<DistrictBean>();

	private DataAdapter _adapter;
	
	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("AddressSelectActivity");
		MobclickAgent.onResume(AddressSelectActivity.this);
		ZhugeSDK.getInstance().init(getApplicationContext());
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("AddressSelectActivity");
		MobclickAgent.onPause(AddressSelectActivity.this);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_shop_address_select);
		
		initUI();
		
		loadData();
		
		setResult(0, null);
		
		_adapter = new DataAdapter(this);
		_listView.setAdapter(_adapter);
	}
	
	private void loadData()
	{
		List<DistrictBean> list = DataModule.loadDict(MyProfile.DICT_DISTRICTS, DistrictBean.class);
		if( list == null )
			return;
		
		_dataList.addAll(list);
	}
	
	protected void initUI() {
		setTitle("请选择");
		RelativeLayout rl = (RelativeLayout) findViewById(R.id.titleBar);
		if (rl != null) {
			rl.setBackgroundColor(getResources().getColor(
					R.color.default_titlebar));
		}

		getLeftButton().setText("返回");
		getLeftButton().setOnClickListener(this);
		getLeftButton().setVisibility(View.VISIBLE);
		
		
		_selectedTextView = (TextView)findViewById(R.id.textView);
		
		_listView = (ListView) findViewById(R.id.listView);
		_listView.setSelector(new ColorDrawable(getResources().getColor(R.color.default_listview_selector)));
		_listView.setOnItemClickListener(this);
	}
	
	private void updateSelectedAddressInfo()
	{
		StringBuffer sb = new StringBuffer();
		for(int i = 0; i < _selectedList.size(); i++ )
		{
			DistrictBean db = _selectedList.get(i);
			
			sb.append(db.getName());
		}
		
		_selectedTextView.setText(sb.toString());
	}
	
	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.btn_left: {
			finish();
		}
			break;
		default:
			break;
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		int cnt = _selectedList.size();
		if( cnt == 0 )
		{
			DistrictBean db = _dataList.get(position);
			_selectedList.add(db);
			
			_dataList.clear();
			_dataList.addAll(db.getCitys());
			_adapter.notifyDataSetChanged();
		}
		else if( cnt == 1 )
		{
			DistrictBean db = _dataList.get(position);
			_selectedList.add(db);
			
			_dataList.clear();
			_dataList.addAll(db.getAreas());
			_adapter.notifyDataSetChanged();
		}
		else if( cnt == 2 )
		{
			DistrictBean db = _dataList.get(position);
			_selectedList.add(db);

			this.updateSelectedAddressInfo();

			Intent intent = new Intent();
			Bundle b = new Bundle();
			b.putSerializable("data", (Serializable)_selectedList);
			intent.putExtras(b);
			
			this.setResult(1, intent);
			
			finish();
			return;
			
		}

		this.updateSelectedAddressInfo();
	}
	
	
	public class DataAdapter extends BaseAdapter<DistrictBean>{

		public DataAdapter(Context context) {
			super(context);
			// TODO Auto-generated constructor stub
		}
		
		@Override
		public int getCount() {
			MyUtils.showLog("count=" + _dataList.size());
			return _dataList.size();
		}
		
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (null == convertView) {
				
				LinearLayout ll = new LinearLayout(getContext());
				ll.setOrientation(LinearLayout.HORIZONTAL);
				
				TextView tv = new TextView(getContext());
				tv.setTextColor(Color.BLACK);
				tv.setId(100);
				tv.setPadding(0,  MyUtils.dp2px(10, getContext()),  0,  MyUtils.dp2px(10, getContext()));
				LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
						LinearLayout.LayoutParams.WRAP_CONTENT);
				llp.leftMargin = MyUtils.dp2px(10, getContext());
				llp.gravity = Gravity.CENTER_VERTICAL;
				ll.addView(tv, llp);
				
				convertView = ll;
			}
			
			DistrictBean db = _dataList.get(position);
			
			TextView tv = (TextView)convertView.findViewById(100);
			tv.setText(db.getName());
			
			return convertView;
		}

	}
}
