package com.ddoctor.user.activity.shop;

import java.util.List;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.enums.RetError;
import com.ddoctor.user.activity.BaseActivity;
import com.ddoctor.user.task.AddShopAddressTask;
import com.ddoctor.user.task.TaskPostCallBack;
import com.ddoctor.user.wapi.bean.DeliverBean;
import com.ddoctor.user.wapi.bean.DistrictBean;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.DialogUtil;
import com.ddoctor.utils.StringUtils;
import com.ddoctor.utils.ToastUtil;
import com.umeng.analytics.MobclickAgent;
import com.zhuge.analysis.stat.ZhugeSDK;

public class AddressManagerActivity extends BaseActivity {

	private Button leftBtn;
	private TextView tv_province;
	private EditText _streetEditText;
	private EditText _nameEditText;
	private EditText _phoneEditText;
	private Button btn_save;
	
	private TextView _addressTextView;
	
	private List<DistrictBean> _addressList = null;
	
	private Dialog _loadingDialog = null;

	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("AddressManagerActivity");
		MobclickAgent.onResume(AddressManagerActivity.this);
		ZhugeSDK.getInstance().init(getApplicationContext());
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("AddressManagerActivity");
		MobclickAgent.onPause(AddressManagerActivity.this);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_shop_address);
		findViewById();
		
		setResult(0, null);
	}

	protected void findViewById() {
		leftBtn = getLeftButtonText(getResources().getString(
				R.string.basic_back));
		setTitle(getResources().getString(R.string.sc_addressmanager));
		RelativeLayout rl = (RelativeLayout) findViewById(R.id.rl_address_info);
		rl.setOnClickListener(this);
		
		_addressTextView = (TextView)findViewById(R.id.tv_address_info);
		
		_streetEditText = (EditText) findViewById(R.id.addressmanager_et_input_street);
		_nameEditText = (EditText) findViewById(R.id.addressmanager_et_input_name);
		_phoneEditText = (EditText) findViewById(R.id.addressmanager_et_input_phone);
		btn_save = (Button) findViewById(R.id.bottom_red_center_btn);
		btn_save.setText(getResources().getString(R.string.basic_save));
		
		leftBtn.setOnClickListener(this);
		btn_save.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.btn_left: {
			finishThisActivity();
		}
			break;
		case R.id.rl_address_info: {
			on_btn_address_select(); // 选择省市区
		}
			break;
		case R.id.bottom_red_center_btn: {
			on_btn_submit(); // 提交
		}
			break;
		default:
			break;
		}
	}
	
	private void on_btn_address_select()
	{
		Intent intent = new Intent(this, AddressSelectActivity.class);
		this.startActivityForResult(intent, 0);
	}
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		if( resultCode == 1 )
		{
		    Bundle b = data.getExtras(); //data为B中回传的Intent
		    List<DistrictBean> dataList = (List<DistrictBean>)b.getSerializable("data");
		    if( dataList != null )
		    {
		    	_addressList = dataList;
		    	String s = getFullAddressString(dataList);
				
				_addressTextView.setText(s);
		    }
		}
	}	
	
	
	private String getFullAddressString(List<DistrictBean> dataList)
	{
		StringBuffer sb = new StringBuffer();
		for(int i = 0; i < dataList.size(); i++ )
		{
			DistrictBean db = dataList.get(i);
			
			sb.append(db.getName());
		}
		
		return sb.toString();
	}
	
	private void on_btn_submit()
	{
		// check...
		if( _addressList == null || _addressList.size() < 3 )
		{
			ToastUtil.showToast("请输入省市区!");
			return;
		}
		
		String s_street = _streetEditText.getText().toString().trim();
		if( s_street.length() < 1 )
		{
			ToastUtil.showToast("请输入街道地址!");
			return;
		}
		
		String s_name = _nameEditText.getText().toString().trim();
		if( s_name.length() < 1 )
		{
			ToastUtil.showToast("请输入收货人名称!");
			return;
		}

		String s_phone = _phoneEditText.getText().toString().trim();
		if( s_phone.length() != 11 )
		{
			ToastUtil.showToast("请输入联系电话!");
			return;
		}
		if (!StringUtils.checkPhoneNumber(s_phone)) {
			ToastUtil.showToast("请输入正确的手机号!");
			return;
		}
		
		DeliverBean deliverBean = new DeliverBean();
		deliverBean.setId(0);
		deliverBean.setName(s_name);
		deliverBean.setMobile(s_phone);
		deliverBean.setStreet(s_street);
		deliverBean.setCode("");
		deliverBean.setAddress(getFullAddressString(_addressList));
		
		deliverBean.setProvince(_addressList.get(0).getId());
		deliverBean.setProvinceName(_addressList.get(0).getName());
		
		deliverBean.setCity(_addressList.get(1).getId());
		deliverBean.setCityName(_addressList.get(1).getName());

		deliverBean.setArea(_addressList.get(2).getId());
		deliverBean.setAreaName(_addressList.get(2).getName());

		
		final DeliverBean db = deliverBean;
		_loadingDialog = DialogUtil.createLoadingDialog(this, "提交中...");
		_loadingDialog.show();
		
		AddShopAddressTask task = new AddShopAddressTask(deliverBean);
		
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {
			
			@Override
			public void taskFinish(DDResult result) 
			{
				_loadingDialog.dismiss();
				
				if (result.getError() == RetError.NONE  )
				{
					ToastUtil.showToast("添加成功！");
					
					// 添加成功，把新增的数据传递给上层activity
					Intent intent = new Intent();
					Bundle b = new Bundle();
					db.setId((Integer) result.getObject());
					b.putSerializable("data", db);
					intent.putExtras(b);
					setResult(1, intent);
					finish();
				}
				else
				{
					ToastUtil.showToast(result.getErrorMessage());
				}
			}
		});
		
		task.executeParallel("");
	}

}
