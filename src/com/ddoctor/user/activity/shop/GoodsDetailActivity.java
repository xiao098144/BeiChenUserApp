package com.ddoctor.user.activity.shop;
/**
 * 商品详情
 * 康
 */
import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.user.activity.BaseActivity;
import com.ddoctor.user.activity.shop.adapter.GoodsImagePageAdapter;
import com.ddoctor.user.data.MyDBUtil;
import com.ddoctor.user.wapi.bean.ProductBean;
import com.ddoctor.utils.DialogUtil;
import com.ddoctor.utils.DialogUtil.ConfirmDialog;
import com.ddoctor.utils.ImageLoaderUtil;
import com.ddoctor.utils.MyUtils;
import com.ddoctor.utils.ToastUtil;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.umeng.analytics.MobclickAgent;
import com.zhuge.analysis.stat.ZhugeSDK;


public class GoodsDetailActivity extends BaseActivity {
	/**
	 * ViewPager
	 */
	private ViewPager _viewPager;

	private Button btn_addtocart, btn_buy;
	
	private ImageView[] _pageDotImageViewList = null;

	private static final int MSG_CHANGE_PHOTO = 1;

	/** 图片自动切换时间 */
	private static final int PHOTO_CHANGE_TIME = 5000;
	
	
	private RelativeLayout _buttonBarLayout;
	private ScrollView _scrollView;
	private LinearLayout _goodsDetailLayout;
	private ImageView _goodsDetailImageView = null;
	
	private ProductBean _productBean = null;
	
	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("GoodsDetailActivity");
		MobclickAgent.onResume(GoodsDetailActivity.this);
		ZhugeSDK.getInstance().init(getApplicationContext());
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("GoodsDetailActivity");
		MobclickAgent.onPause(GoodsDetailActivity.this);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_shop_goods_detail);
		
		
		initUI();
		
		_productBean = (ProductBean)getIntent().getParcelableExtra("data");
		if( _productBean != null )
		{
			showContentUI(true);
			
//			MyUtils.showLog(_productBean.toString());
			// 加载
			updateUI();
		}
		else
		{
			showContentUI(false);
			
			ToastUtil.showToast("商品参数错误!");
		}
		
	}
	
	
	private void updateUI()
	{
		initViewPager();
		
		TextView tv;
		String s;
		
		
		tv = (TextView)findViewById(R.id.tv_goods_discount);
		s = String.format("折后价：%.2f", _productBean.getDiscount()); 
		tv.setText(s);
		
		tv = (TextView)findViewById(R.id.tv_goods_name);
		tv.setText(_productBean.getName());
		
		tv = (TextView)findViewById(R.id.tv_goods_brand);
		tv.setText(_productBean.getBrand());

		tv = (TextView)findViewById(R.id.tv_goods_model);
		tv.setText(_productBean.getModel());
		
		tv = (TextView)findViewById(R.id.tv_goods_price);
		s = String.format("市场价：%.2f", _productBean.getPrice()); 
		tv.setText(s);
		
		tv = (TextView)findViewById(R.id.tv_goods_desc);
		tv.setText(_productBean.getMoreinfo());
		
		
		s = _productBean.getImage4();
		MyUtils.showLog(s);
		if( s != null && MyUtils.isValidURLString(s) )
		{
			LinearLayout imgLayout = (LinearLayout)_goodsDetailLayout.findViewById(R.id.goodsDetailImageLayout);
			LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 
					LinearLayout.LayoutParams.WRAP_CONTENT);
			llp.gravity = Gravity.CENTER_HORIZONTAL;
			ImageView imgView = new ImageView(this);
			imgView.setScaleType(ScaleType.FIT_CENTER);
			imgView.setBackgroundColor(Color.WHITE);
			imgLayout.addView(imgView, llp);
			_goodsDetailImageView = imgView;
			
//			ImageLoaderUtil.display(s, imgView, R.drawable.ic_launcher);
			ImageLoaderUtil.displayListener(s, imgView,  0, new ImageLoadingListener(){

				@Override
				public void onLoadingCancelled(String arg0, View arg1) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void onLoadingComplete(String arg0, View arg1,
						Bitmap arg2) {
					// TODO Auto-generated method stub
					

					int w = MyUtils.getScreenWidth(GoodsDetailActivity.this);
					int h = w * arg2.getHeight() / arg2.getWidth();
					
					MyUtils.showLog("onLoadingComplete..." + w + "x" + h);
					LinearLayout.LayoutParams llp = (LinearLayout.LayoutParams)_goodsDetailImageView.getLayoutParams();
					llp.width = w;
					llp.height = h;
				}

				@Override
				public void onLoadingFailed(String arg0, View arg1,
						FailReason arg2) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void onLoadingStarted(String arg0, View arg1) {
					// TODO Auto-generated method stub
					
				}
				
			});
		}
		else
			_goodsDetailLayout.setVisibility(View.GONE);
	}

	private Handler mHandler = new Handler() {
		@Override
		public void dispatchMessage(Message msg) {
			switch (msg.what) {
			case MSG_CHANGE_PHOTO:
				int index = _viewPager.getCurrentItem();
				_viewPager.setCurrentItem(index + 1);
				mHandler.sendEmptyMessageDelayed(MSG_CHANGE_PHOTO,
						PHOTO_CHANGE_TIME);
				break;
			}
			super.dispatchMessage(msg);
		}
	};


	protected void initUI() {
		

		Button leftButton = this.getLeftButtonText(getResources().getString(R.string.basic_back));
		Button rightButton = this.getRightButtonText("购物车");
		RelativeLayout rl = (RelativeLayout) findViewById(R.id.titleBar);
		if (rl != null) {
			rl.setBackgroundColor(getResources().getColor(
					R.color.default_titlebar));
		}
		leftButton.setOnClickListener(this);
		rightButton.setOnClickListener(this);
		
		
		btn_buy = (Button) findViewById(R.id.btn_buy);
		btn_addtocart = (Button) findViewById(R.id.btn_add_to_cart);

		btn_addtocart.setOnClickListener(this);
		btn_buy.setOnClickListener(this);
		
		_buttonBarLayout = (RelativeLayout)findViewById(R.id.buttonBarLayout);
		_scrollView = (ScrollView)findViewById(R.id.scrollView);
		_goodsDetailLayout = (LinearLayout)findViewById(R.id.goodsDetailLayout);
	}
	
	private void showContentUI(boolean show)
	{
		_buttonBarLayout.setVisibility(show ? View.VISIBLE : View.INVISIBLE);
		
		_scrollView.setVisibility(show ? View.VISIBLE : View.INVISIBLE);
	}


	@Override
	public void onClick(View v) {
		super.onClick(v);
		
		switch (v.getId()) 
		{
		case R.id.btn_left:
			finishThisActivity();
			break;
		case R.id.btn_buy:
			on_btn_buy();
			break;
		case R.id.btn_right:
			skip(CartActivity.class, false);
			break;
		case R.id.btn_add_to_cart:
			on_btn_addtocart();
			break;
		default:
			break;
		}
	}

	private void initViewPager() {
		ViewGroup group = (ViewGroup) findViewById(R.id.viewGroup);
		_viewPager = (ViewPager) findViewById(R.id.viewPager);
		
		int width = MyUtils.getScreenWidth(this);
		int height = width * 300 / 400; // 按设计比例计算高度
		
		RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams)_viewPager.getLayoutParams();
		rlp.height = height;
		_viewPager.setLayoutParams(rlp);
		
		List<String> imgList = new ArrayList<String>();
		if( _productBean.getImage() != null && MyUtils.isValidURLString(_productBean.getImage()) )
			imgList.add(_productBean.getImage());
		if( _productBean.getImage2() != null && MyUtils.isValidURLString(_productBean.getImage2()) )
			imgList.add(_productBean.getImage2());
		if( _productBean.getImage3() != null && MyUtils.isValidURLString(_productBean.getImage3()) )
			imgList.add(_productBean.getImage3());
//		if( _productBean.getImage4() != null && MyUtils.isValidURLString(_productBean.getImage4()) )
//			imgList.add(_productBean.getImage4());
		
		// 将点点加入到ViewGroup中
//		ImageView imgView = new ImageView[imgList.length];

		if (imgList.size() < 1)
		{
			_viewPager.setVisibility(View.GONE);
			group.setVisibility(View.GONE);
			return;
		}
		
		// 只有一张图，不显示页码
		if( imgList.size() == 1 )
			group.setVisibility(View.GONE);
		
		_pageDotImageViewList = new ImageView[imgList.size()];
		
		// 添加页码指示点
		for (int i = 0; i < imgList.size(); i++) 
		{
			ImageView imageView = new ImageView(this);
		
			LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
					LinearLayout.LayoutParams.WRAP_CONTENT,
					LinearLayout.LayoutParams.WRAP_CONTENT);
			lp.setMargins(4, 4, 4, 4);

			if (i == 0) {
				imageView.setBackgroundResource(R.drawable.page_dot_focus);
			} else {
				imageView.setBackgroundResource(R.drawable.page_dot);
			}

			group.addView(imageView, lp);
			_pageDotImageViewList[i] = imageView;
		}

/*		mImageViews = new ImageView[2][];
		// 将图片装载到数组中,其中一组类似缓冲，防止图片少时出现黑色图片，即显示不出来
		mImageViews[0] = new ImageView[imgIdArray.length];
		mImageViews[1] = new ImageView[imgIdArray.length];

		for (int i = 0; i < mImageViews.length; i++) {
			for (int j = 0; j < mImageViews[i].length; j++) {
				final ImageView imageView = new ImageView(this);
				LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
						LinearLayout.LayoutParams.WRAP_CONTENT,
						LinearLayout.LayoutParams.WRAP_CONTENT);
				imageView.setLayoutParams(lp);
				imageView.setImageResource(imgIdArray[j]);
				imageView.setContentDescription("R.id.item" + (j + 1));
				imageView.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						ToastUtil.showToast(imageView.getContentDescription()
								+ "");

					}
				});
				mImageViews[i][j] = imageView;

			}
		}
		*/

		// 设置Adapter
		_viewPager.setAdapter(new GoodsImagePageAdapter(this, imgList));
		// 设置监听，主要是设置点点的背景
		_viewPager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageScrollStateChanged(int arg0) {

			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {

			}

			@Override
			public void onPageSelected(int arg0) {
				setImageBackground(arg0);
			}
		});

//		viewPager.setOnTouchListener(new OnTouchListener() {
//
//			@Override
//			public boolean onTouch(View v, MotionEvent event) {
//				if (imgIdArray.length == 0 || imgIdArray.length == 1)
//					return true;
//				else
//					return false;
//			}
//		});
//
//		// 设置ViewPager的默认项, 设置为长度的50倍，这样子开始就能往左滑动
//		viewPager.setCurrentItem((imgIdArray.length) * 50);
//		if (imgIdArray.length > 1) {
//			mHandler.sendEmptyMessageDelayed(MSG_CHANGE_PHOTO,
//					PHOTO_CHANGE_TIME);
//		}

	}

	/**
	 * 设置选中的tip的背景
	 * 
	 * @param selectItemsIndex
	 */
	private void setImageBackground(int selectItemsIndex) {
		for (int i = 0; i < _pageDotImageViewList.length; i++) {
			if (i == selectItemsIndex) {
				this._pageDotImageViewList[i].setBackgroundResource(R.drawable.page_dot_focus);
			} else {
				_pageDotImageViewList[i].setBackgroundResource(R.drawable.page_dot);
			}
		}
	}
	
	private void on_btn_buy()
	{
		// 			skip(PayActivity.class, false);
		
//		List<ShopBean> dataList = new ArrayList<ShopBean>();
//		
//		ShopBean sb = new ShopBean();
//		sb.setId(_productBean.getId());
//		sb.setName(_productBean.getName());
//		sb.setPrice(_productBean.getDiscount());
//		sb.setImage(_productBean.getImage());
//		sb.setNum(1);
//		dataList.add(sb);
		
		ArrayList<ProductBean> dataList = new ArrayList<ProductBean>();
		_productBean.setCount(1);
		dataList.add(_productBean);
		
		Intent intent = new Intent(this, OrderSubmitActivity.class);
		intent.putParcelableArrayListExtra("dataList", dataList);
		
		startActivity(intent);

	}
	
	// 添加到购物车
	private void on_btn_addtocart()
	{
		// 保存到本地数据库中
		if( !MyDBUtil.getInstance().addToCart(_productBean, 1) )
		{
			ToastUtil.showToast("添加到购物车失败，请稍后重试！");
			return;
		}
		
		// 提示添加成功
		DialogUtil.confirmDialog(this, "添加到购物车成功!", "继续购物", "去结算", new ConfirmDialog(){

			@Override
			public void onOKClick(Bundle data) {
			}

			@Override
			public void onCancleClick() {
				// 去购物车
				skip(CartActivity.class, false);
			}
			
		}).show();
		
	}
}
