package com.ddoctor.user.activity;

import java.util.Map;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.location.LocationClientOption.LocationMode;
import com.beichen.user.R;
import com.ddoctor.application.MyApplication;
import com.ddoctor.enums.RetError;
import com.ddoctor.user.activity.login.LoginActivity;
import com.ddoctor.user.component.ClientUpgrade;
import com.ddoctor.user.config.AppBuildConfig;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.service.PollingService;
import com.ddoctor.user.task.AppInitializeTask;
import com.ddoctor.user.task.TaskPostCallBack;
import com.ddoctor.user.wapi.bean.ClientUpdateBean;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.DialogUtil;
import com.ddoctor.utils.DialogUtil.ConfirmDialog;
import com.ddoctor.utils.ImageLoaderUtil;
import com.ddoctor.utils.MyUtils;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.tencent.tauth.Tencent;
import com.testin.agent.TestinAgent;
import com.umeng.analytics.AnalyticsConfig;
import com.umeng.analytics.MobclickAgent;
import com.zhuge.analysis.stat.ZhugeSDK;

import fynn.app.PromptDialog;

public class SplashScreenActivity extends Activity {
	private DDResult _result = null;
	private ClientUpdateBean _clientUpdateBean = null;
	private ImageView _loadingImageView = null;

	private LocationClient mLocationClient;

	public class MyLocationListener implements BDLocationListener {

		@Override
		public void onReceiveLocation(BDLocation location) {
			if (null != location) {
				 MyUtils.showLog("启动时获取地理位置   "+location.getAddrStr()+" "+location.getLatitude()+" "+location.getLongitude());
				DataModule.getInstance().saveLocation(location.getAddrStr(),
						location.getLatitude(), location.getLongitude());
				if (mgpsStart) {
					mLocationClient.stop();
				}
			}
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("SplashActivity");
		MobclickAgent.onResume(SplashScreenActivity.this);
		ZhugeSDK.getInstance().init(getApplicationContext());
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("SplashActivity");
		MobclickAgent.onPause(SplashScreenActivity.this);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
		// WindowManager.LayoutParams.FLAG_FULLSCREEN);
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.activity_splashscreen);
		
	
		// 适配背景图
		adjustBgImageLayout();

		initAnalysis();
		initLocationClient();
		if (DataModule.getInstance().isGPSEnable()) {
			initLocationClient();
			mLocationClient.start();
			mgpsStart = true;
		}
		_loadingImageView = (ImageView) findViewById(R.id.loadingImageView);
		startLoading(true);

		// test();
		/** 微信初始化 并注册应用 */
		MyApplication.mwxAPI = WXAPIFactory.createWXAPI(
				getApplicationContext(), AppBuildConfig.WXAPP_ID, true);
		MyApplication.mwxAPI
				.registerApp(com.ddoctor.user.config.AppBuildConfig.WXAPP_ID);
		/** Tencent 初始化 */
		MyApplication.tencent = Tencent.createInstance(
				AppBuildConfig.TencentKey, getApplicationContext());

		MyApplication.tencent.setOpenId(DataModule.getInstance().getOpenId());

		long expires = DataModule.getInstance().getTencentExpires();

		MyApplication.tencent.setAccessToken(DataModule.getInstance()
				.getTencentAccessToken(),
				((expires - System.currentTimeMillis()) / 1000) + "");

		startService();
		startLoading();

	}

	private void adjustBgImageLayout() {
		ImageView bgImageView = (ImageView) findViewById(R.id.imageView);
		RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) bgImageView
				.getLayoutParams();
		// 1080x1920
		int imgw = 1080;
		int imgh = 1920;
		int sw = MyUtils.getScreenWidth(this);
		int sh = MyUtils.getScreenHeight(this);
		int w, h;
		if (((float) imgw) / ((float) imgh) >= ((float) sw) / ((float) sh)) {
			h = sh;
			w = imgw * h / imgh;
		} else {
			w = sw;
			h = imgh * w / imgw;
		}

		rlp.width = w;
		rlp.height = h;
		rlp.leftMargin = (sw - w) / 2;
		rlp.topMargin = 0;

	}

	/**
	 * 初始化统计分析 友盟 云测
	 */
	private void initAnalysis() {
		MobclickAgent.updateOnlineConfig(this);
		MobclickAgent.setCatchUncaughtExceptions(true);
		MobclickAgent.setSessionContinueMillis(2 * 60 * 1000);
		AnalyticsConfig.enableEncrypt(true);
		MobclickAgent.openActivityDurationTrack(false);
		TestinAgent.init(this, AppBuildConfig.TESTINAPPKEY);
	}

	private boolean mgpsStart = false;

	private void initLocationClient() {
		mLocationClient = MyApplication.mLocationClient;
		mLocationClient.registerLocationListener(new MyLocationListener());
		LocationClientOption option = new LocationClientOption();
		option.setLocationMode(LocationMode.Hight_Accuracy);// 设置定位模式
		option.setCoorType("bd09ll");// 返回的定位结果是百度经纬度，默认值gcj02
		option.setScanSpan(5000);// 设置发起定位请求的间隔时间为5000ms
		option.setIsNeedAddress(true);// 返回的定位结果包含地址信息
		option.setNeedDeviceDirect(true);// 返回的定位结果包含手机机头的方向
		mLocationClient.setLocOption(option);
	}

	private void startLoading(boolean show) {
		if (show) {
			AnimationDrawable ad = (AnimationDrawable) _loadingImageView
					.getBackground();
			ad.start();
			_loadingImageView.setVisibility(View.VISIBLE);
		} else {
			_loadingImageView.setVisibility(View.INVISIBLE);

			AnimationDrawable ad = (AnimationDrawable) _loadingImageView
					.getBackground();
			if (ad.isRunning())
				ad.stop();
		}
	}

	private void startService() {
		if (DataModule.getInstance().getLoginedUserId() != 0) {
			Intent intent = new Intent();
			intent.setClass(getApplicationContext(), PollingService.class);
			startService(intent);
		}
	}

	// private class Person
	// {
	// private String name;
	// private int id;
	// private float weight;
	//
	// public void setName(String name)
	// {
	// this.name = name;
	// }
	//
	// public String getName(){return this.name;}
	//
	// public int getId(){return this.id;}
	// public void setId(int id){ this.id = id;}
	//
	// public float getWeight() { return weight;}
	// public void setWeight(float weight){this.weight = weight;}
	// }

	// private void json_test()
	// {
	// Gson gson = new Gson();
	// // String s =
	// "{\"name\":\"1920-10-01 12:02:32\",\"id\":12,\"weight\":12.2}";
	// // Person p = gson.fromJson(s, Person.class);
	// Person p = new Person();
	// p.setName("1920-01-01 12:00:00");
	// p.setId(11);
	// p.setWeight(12.2f);
	//
	//
	// JSONObject obj = WAPI.beanToJSONObject(p);
	//
	// String s= obj.toString();
	// Person p1 = gson.fromJson(s, Person.class);
	//
	//
	// MyUtils.showLog(p1.getName());
	// }

	private void startLoading() {

		ImageLoaderUtil.initImageLoader(this);

		AppInitializeTask task = new AppInitializeTask();
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {
			@Override
			public void taskFinish(DDResult result) {
				_result = result;
				startLoading(false);
				on_task_finish(true);
			}
		});
		task.executeParallel("");
	}

	private void doClientUpdate(ClientUpdateBean updateBean) {
		String okTitle = "立即更新";
		String cancelTitle = "下次再说";
		if (updateBean.getIsMust() != 0) {
			cancelTitle = "";
		}

		// for test
		final String downloadUrl = updateBean.getUpdateUrl();
		final ClientUpdateBean bean = updateBean;
		DialogUtil
				.confirmDialog(this, updateBean.getRemark(), okTitle,
						cancelTitle, new ConfirmDialog() {

							@Override
							public void onOKClick(Bundle data) {
								// TODO: 下载apk包...
								ClientUpgrade cu = new ClientUpgrade(
										SplashScreenActivity.this);
								cu.downloadApk(
										downloadUrl,
										new ClientUpgrade.ClientUpgradeCallback() {

											@Override
											public void onSuccess() {
												// TODO Auto-generated method
												// stub
												SplashScreenActivity.this
														.finish();
											}

											@Override
											public void onFailed() {
												if (bean.getIsMust() != 0) {
													DialogUtil
															.confirmDialog(
																	SplashScreenActivity.this,
																	"下载失败!",
																	"重试",
																	"退出",
																	new ConfirmDialog() {

																		@Override
																		public void onOKClick(
																				Bundle data) {
																			doClientUpdate(_clientUpdateBean);
																		}

																		@Override
																		public void onCancleClick() {
																			SplashScreenActivity.this
																					.finish();
																		}
																	}).show();
												} else {
													DialogUtil
															.confirmDialog(
																	SplashScreenActivity.this,
																	"下载失败!",
																	"重试",
																	"取消",
																	new ConfirmDialog() {

																		@Override
																		public void onOKClick(
																				Bundle data) {
																			doClientUpdate(_clientUpdateBean);
																		}

																		@Override
																		public void onCancleClick() {
																			// 进入
																			on_task_finish(false);
																		}
																	}).show();

												}

											}

											@Override
											public void onCancel() {
												// 用户取消下载了
												if (bean.getIsMust() != 0) // 必须升级了，取消了，就退出
												{
													SplashScreenActivity.this
															.finish();
												} else {
													// 进入
													on_task_finish(false);
												}

											}
										});
							}

							@Override
							public void onCancleClick() {
								on_task_finish(false);
							}
						}).setCancelable(false).setTitle("有新版本了").show();
	}

	private void on_task_finish(boolean clientUpdate) {
		if (clientUpdate && _result.getObject() != null) {
			Map<String, Object> dataMap = (Map<String, Object>) _result
					.getObject();
			if (dataMap.containsKey("update")) {
				ClientUpdateBean updateBean = (ClientUpdateBean) dataMap
						.get("update");
				_clientUpdateBean = updateBean;
				doClientUpdate(updateBean);
				return;
			}
		}
		if (_result.getError() == RetError.NONE) {
			// 加载成功，进入主界面
			// 如果上次已经登录过，直接进入主界面，如果未登录，进入登录/注册界面，这里要判断一下
			if (DataModule.getInstance().isLogined()) {
				// 已经登录，进入主界面
				Intent intent = new Intent(SplashScreenActivity.this,
						MainTabActivity.class);
				startActivity(intent);
			} else {
				// 未登录，打开登录界面
				Intent intent = new Intent(SplashScreenActivity.this,
						LoginActivity.class);
				startActivity(intent);
			}
			SplashScreenActivity.this.finish();
			// }else if( result == RetError.NETWORK_ERROR ){
			// // TODO：如果是网络问题，用缓存进入
		} else if (_result.getError() == RetError.MISSING_USER_INFO) {
			// 获取用户信息出错，不直接登录了，直接进入登录界面

			DataModule.getInstance().saveLoginedUserInfo(null);

			Intent intent = new Intent(SplashScreenActivity.this,
					LoginActivity.class);
			startActivity(intent);
		} else {
			showErrorPrompt(_result);
		}

	}

	private void showErrorPrompt(DDResult result) {
		startLoading(false);
		PromptDialog.Builder dialog = DialogUtil.confirmDialog(this,
				result.getErrorMessage(), "确定", "", new ConfirmDialog() {

					@Override
					public void onOKClick(Bundle data) {
						SplashScreenActivity.this.finish();
					}

					@Override
					public void onCancleClick() {
						// TODO Auto-generated method stub

					}
				});
		if (!SplashScreenActivity.this.isFinishing()) {
			dialog.show();
		}

	}
}
