package com.ddoctor.user.model;

import android.os.Parcel;
import android.os.Parcelable;

public class MyFriendBean implements Comparable<MyFriendBean>, Cloneable,
		Parcelable {

	private String sortLetter;
	private String fullPinyin;
	private String picUrl;
	private String docName;
	private String content;
	private String id;

	public MyFriendBean() {
		super();
		// TODO Auto-generated constructor stub
	}

	public MyFriendBean(String picUrl, String docName, String content) {
		super();
		this.picUrl = picUrl;
		this.docName = docName;
		this.content = content;
	}

	public MyFriendBean(Parcel source) {
		this();
		this.sortLetter = source.readString();
		this.fullPinyin = source.readString();
		this.picUrl = source.readString();
		this.docName = source.readString();
		this.content = source.readString();
		this.id = source.readString();
	}

	public String getSortLetter() {
		return sortLetter;
	}

	public void setSortLetter(String sortLetter) {
		this.sortLetter = sortLetter;
	}

	public String getFullPinyin() {
		return fullPinyin;
	}

	public void setFullPinyin(String fullPinyin) {
		this.fullPinyin = fullPinyin;
	}

	public String getPicUrl() {
		return picUrl;
	}

	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl;
	}

	public String getDocName() {
		return docName;
	}

	public void setDocName(String docName) {
		this.docName = docName;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "MyFriendBean [sortLetter=" + sortLetter + ", id=" + id + "]";
	}

	@Override
	public int compareTo(MyFriendBean another) {
		int compare;
		if (getSortLetter().equals("#")) {
			compare = 1;
		}
		if (another.getSortLetter().equals("#")) {
			compare = -1;
		}
		compare = getSortLetter().compareTo(another.getSortLetter());
		if (compare == 0) {
			compare = getFullPinyin().compareTo(another.getFullPinyin());
		}
		return compare;
	}

	@Override
	public Object clone() {
		MyFriendBean myFriendBean = null;
		try {
			myFriendBean = (MyFriendBean) super.clone();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return myFriendBean;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(getSortLetter());
		dest.writeString(getFullPinyin());
		dest.writeString(getPicUrl());
		dest.writeString(getDocName());
		dest.writeString(getContent());
		dest.writeString(getId());

	}

	public static final Parcelable.Creator<MyFriendBean> CREATOR = new Parcelable.Creator<MyFriendBean>() {

		@Override
		public MyFriendBean createFromParcel(Parcel source) {
			return new MyFriendBean(source);
		}

		@Override
		public MyFriendBean[] newArray(int size) {

			return new MyFriendBean[size];
		}
	};

}
