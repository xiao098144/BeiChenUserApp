package com.ddoctor.user.model;

import android.os.Parcel;
import android.os.Parcelable;

public class FMLibItemBean implements Parcelable{

	private String picUrl;
	private float sugarContent;
	private float hotContent;
	private float rating;

	
	public FMLibItemBean(Parcel source){
		picUrl = source.readString();
		sugarContent = source.readFloat();
		hotContent = source.readFloat();
		rating = source.readFloat();
	}
	
	public FMLibItemBean() {
		super();
		// TODO Auto-generated constructor stub
	}

	public FMLibItemBean(String picUrl, float sugarContent, float hotContent,
			float rating) {
		super();
		this.picUrl = picUrl;
		this.sugarContent = sugarContent;
		this.hotContent = hotContent;
		this.rating = rating;
	}

	public String getPicUrl() {
		return picUrl;
	}

	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl;
	}

	public float getSugarContent() {
		return sugarContent;
	}

	public void setSugarContent(float sugarContent) {
		this.sugarContent = sugarContent;
	}

	public float getHotContent() {
		return hotContent;
	}

	public void setHotContent(float hotContent) {
		this.hotContent = hotContent;
	}

	public float getRating() {
		return rating;
	}

	public void setRating(float rating) {
		this.rating = rating;
	}

	@Override
	public String toString() {
		return "FMLibItemBean [picUrl=" + picUrl + ", sugarContent="
				+ sugarContent + ", hotContent=" + hotContent + ", rating="
				+ rating + "]";
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(picUrl);
		dest.writeFloat(sugarContent);
		dest.writeFloat(hotContent);
		dest.writeFloat(rating);
	}
	
	public static final Parcelable.Creator<FMLibItemBean> CREATOR = new Parcelable.Creator<FMLibItemBean>() {

		@Override
		public FMLibItemBean createFromParcel(Parcel source) {
			// TODO Auto-generated method stub
			return new FMLibItemBean(source);
		}

		@Override
		public FMLibItemBean[] newArray(int size) {
			return new FMLibItemBean[size];
		}
	};

}
