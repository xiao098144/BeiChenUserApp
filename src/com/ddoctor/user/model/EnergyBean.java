package com.ddoctor.user.model;

import android.os.Parcel;
import android.os.Parcelable;

public class EnergyBean implements Parcelable {

	private float energy;
	private float protein;
	private float fat;
	private Integer fbid;

	public EnergyBean() {
		super();
		// TODO Auto-generated constructor stub
	}

	public EnergyBean(float energy, float protein, float fat, Integer fbid) {
		super();
		this.energy = energy;
		this.protein = protein;
		this.fat = fat;
		this.fbid = fbid;
	}

	public float getEnergy() {
		return energy;
	}

	public void setEnergy(float energy) {
		this.energy = energy;
	}

	public float getProtein() {
		return protein;
	}

	public void setProtein(float protein) {
		this.protein = protein;
	}

	public float getFat() {
		return fat;
	}

	public void setFat(float fat) {
		this.fat = fat;
	}

	public Integer getFbid() {
		return fbid;
	}

	public void setFbid(Integer fbid) {
		this.fbid = fbid;
	}

	@Override
	public String toString() {
		return "EnergyBean [energy=" + energy + ", protein=" + protein
				+ ", fat=" + fat + ", fbid=" + fbid + "]";
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeFloat(this.energy);
		dest.writeFloat(this.protein);
		dest.writeFloat(this.fat);
		dest.writeValue(this.fbid);
	}

	protected EnergyBean(Parcel in) {
		this.energy = in.readFloat();
		this.protein = in.readFloat();
		this.fat = in.readFloat();
		this.fbid = (Integer) in.readValue(Integer.class.getClassLoader());
	}

	public static final Parcelable.Creator<EnergyBean> CREATOR = new Parcelable.Creator<EnergyBean>() {
		public EnergyBean createFromParcel(Parcel source) {
			return new EnergyBean(source);
		}

		public EnergyBean[] newArray(int size) {
			return new EnergyBean[size];
		}
	};
}
