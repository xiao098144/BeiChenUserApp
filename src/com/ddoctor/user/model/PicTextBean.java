package com.ddoctor.user.model;

/**
 * 图文
 * 
 * @author 萧
 * @Date 2015-5-5下午10:51:16
 * @TODO TODO
 */
public class PicTextBean {

	/** 图片 */
	private int resId;
	/** 文字颜色 */
	private int color;
	/** 文字内容 */
	private String text;

	public PicTextBean() {
		super();
	}

	public int getImgResId() {
		return resId;
	}

	public void setImgResId(int resId) {
		this.resId = resId;
	}

	public int getTextColor() {
		return color;
	}

	public void setTextColor(int color) {
		this.color = color;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return "PicTextBean [resId=" + resId + ", color=" + color + ", text="
				+ text + "]";
	}

}
