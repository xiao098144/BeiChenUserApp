package com.ddoctor.user.model;



public class FriendGroupListBean implements Comparable<FriendGroupListBean> {

	private String protrait;
	private String group_name;
	private String msg_time;
	private String describe;
	private String picUrl;
	private String lastmsg_name;
	private String lastmsg_content;
	
	
	public FriendGroupListBean() {
		super();
		// TODO Auto-generated constructor stub
	}


	public FriendGroupListBean(String protrait, String group_name,
			String msg_time, String describe, String picUrl,
			String lastmsg_name, String lastmsg_content) {
		super();
		this.protrait = protrait;
		this.group_name = group_name;
		this.msg_time = msg_time;
		this.describe = describe;
		this.picUrl = picUrl;
		this.lastmsg_name = lastmsg_name;
		this.lastmsg_content = lastmsg_content;
	}


	public String getProtrait() {
		return protrait;
	}


	public void setProtrait(String protrait) {
		this.protrait = protrait;
	}


	public String getGroup_name() {
		return group_name;
	}


	public void setGroup_name(String group_name) {
		this.group_name = group_name;
	}


	public String getMsg_time() {
		return msg_time;
	}


	public void setMsg_time(String msg_time) {
		this.msg_time = msg_time;
	}


	public String getDescribe() {
		return describe;
	}


	public void setDescribe(String describe) {
		this.describe = describe;
	}


	public String getPicUrl() {
		return picUrl;
	}


	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl;
	}


	public String getLastmsg_name() {
		return lastmsg_name;
	}


	public void setLastmsg_name(String lastmsg_name) {
		this.lastmsg_name = lastmsg_name;
	}


	public String getLastmsg_content() {
		return lastmsg_content;
	}


	public void setLastmsg_content(String lastmsg_content) {
		this.lastmsg_content = lastmsg_content;
	}


//	@Override
//	public String toString() {
//		return "FriendGroupListBean [protrait=" + protrait + ", group_name="
//				+ group_name + ", msg_time=" + msg_time + ", describe="
//				+ describe + ", picUrl=" + picUrl + ", lastmsg_name="
//				+ lastmsg_name + ", lastmsg_content=" + lastmsg_content + "]";
//	}




	@Override
	public int compareTo(FriendGroupListBean another) {
		int compare;
		compare = getMsg_time().compareTo(another.getMsg_time());
		if (compare == 0) {
			compare = getGroup_name().compareTo(another.getGroup_name());
		}
		return compare;
	}


	@Override
	public String toString() {
		return "FriendGroupListBean [msg_time=" + msg_time + ", picUrl="
				+ picUrl + "]";
	}

}
