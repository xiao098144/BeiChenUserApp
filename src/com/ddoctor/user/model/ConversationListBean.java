package com.ddoctor.user.model;


public class ConversationListBean implements Comparable<ConversationListBean> {

	private String picUrl;
	private String conversationName;
	private String lastMsg;
	private String lastMsgTime;
	private int lastMsgType;
	private boolean isStick; // 是否置顶
	private String setStickTime; // 设置置顶时间

	public ConversationListBean() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getPicUrl() {
		return picUrl;
	}

	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl;
	}

	public String getConversationName() {
		return conversationName;
	}

	public void setConversationName(String conversationName) {
		this.conversationName = conversationName;
	}

	public String getLastMsg() {
		return lastMsg;
	}

	public void setLastMsg(String lastMsg) {
		this.lastMsg = lastMsg;
	}

	public String getLastMsgTime() {
		return lastMsgTime;
	}

	public void setLastMsgTime(String lastMsgTime) {
		this.lastMsgTime = lastMsgTime;
	}

	public int getLastMsgType() {
		return lastMsgType;
	}

	public void setLastMsgType(int lastMsgType) {
		this.lastMsgType = lastMsgType;
	}

	public boolean isStick() {
		return isStick;
	}

	public void setStick(boolean isStick) {
		this.isStick = isStick;
	}

	public String getSetStickTime() {
		return setStickTime;
	}

	public void setSetStickTime(String setStickTime) {
		this.setStickTime = setStickTime;
	}

	
	
	@Override
	public String toString() {
		return "ConversationListBean [conversationName=" + conversationName
				+ ", lastMsgTime=" + lastMsgTime + ", isStick=" + isStick
				+ ", setStickTime=" + setStickTime + "]";
	}

	@Override
	public int compareTo(ConversationListBean another) {
		int compare = 0;
		try {
			if (isStick()) {
				if (!another.isStick()) {
					compare  =1;
				}else {
					compare = getSetStickTime().compareTo(another.getSetStickTime());
					if (compare == 0) {
						compare = getLastMsgTime().compareTo(another.getLastMsgTime());
						if (compare == 0) {
							compare = getConversationName().compareTo(
									another.getConversationName());
						}
					}
				}
			} else {
				if (another.isStick()) {
					compare = -1;
				}else {
					compare = getLastMsgTime().compareTo(another.getLastMsgTime());
					if (compare == 0) {
						compare = getConversationName().compareTo(
								another.getConversationName());
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0-compare;
	}

}
