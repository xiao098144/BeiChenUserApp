package com.ddoctor.user.model;

/**
 * 健康报告 分析状态
 * 
 * @author 萧
 * @Date 2015-5-11下午9:37:06
 * @TODO TODO
 */
public class ReportStatusBean {

	public enum ReportStatusType {
		Analysing, AnalysisError, AnalysisNormal
	}

	private ReportStatusType analysisStatus;
	/** 当前进度 */
	private int progress;

	public ReportStatusBean() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ReportStatusBean(ReportStatusType analysisStatus) {
		super();
		this.analysisStatus = analysisStatus;
	}

	public ReportStatusBean(ReportStatusType analysisStatus, int progress) {
		super();
		this.analysisStatus = analysisStatus;
		this.progress = progress;
	}

	public ReportStatusType getAnalysisStatus() {
		return analysisStatus;
	}
	
	public void setAnalysisStatus(ReportStatusType analysisStatus) {
		this.analysisStatus = analysisStatus;
	}
	
	public int getProgress() {
		return progress;
	}

	public void setProgress(int progress) {
		this.progress = progress;
	}

	@Override
	public String toString() {
		return "ReportStatusBean [analysisStatus=" + analysisStatus
				+ ", progress=" + progress + "]";
	}

}
