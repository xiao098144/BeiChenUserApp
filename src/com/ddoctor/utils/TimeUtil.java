package com.ddoctor.utils;

import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.annotation.SuppressLint;
import android.text.TextUtils;

/**
 * 时间类工具
 */
@SuppressLint("SimpleDateFormat")
public class TimeUtil {

	private static TimeUtil timeUtil;

	private TimeUtil() {

	}

	public static TimeUtil getInstance() {
		if (timeUtil == null) {
			timeUtil = new TimeUtil();
		}
		return timeUtil;
	}

	public int getCurrentYear() {
		return getTime(Calendar.YEAR);
	}

	public int getCurrentMonth() {
		return getTime(Calendar.MONTH)+1;
	}

	public int getCurrentDay() {
		return getTime(Calendar.DAY_OF_MONTH);
	}
	
	/**
	 * 获取周几  注意顺序：周日、一、二、三、四、五、六
	 * @return
	 */
	public int getCurrentDayOfWeek(){
		return getTime(Calendar.DAY_OF_WEEK);
	}

	public int getCurrentHour() {
		return getTime(Calendar.HOUR_OF_DAY);
	}

	public int getCurrentMinute() {
		return getTime(Calendar.MINUTE);
	}

	/**
	 * 依据类型获取时间
	 * 
	 * @param type
	 * @return
	 */
	private int getTime(int type) {
		Calendar calendar = Calendar.getInstance();
		return calendar.get(type);
	}

	
	
	/**
	 * 长整型转指定格式字符串
	 * @param time
	 * @param pattern 默认格式  yyyy-MM-dd
	 * @return
	 */
	public String long2DateStr(long time , String pattern){
		if (TextUtils.isEmpty(pattern)) {
			pattern = "yyyy-MM-dd";
		}
		Date date = new Date(time);
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		return sdf.format(date);
	}
	
	public String timeStrFormat(String strDate, String fromFormat,
			String toFormat) {
		try {
			SimpleDateFormat formatter = new SimpleDateFormat(fromFormat);
			ParsePosition pos = new ParsePosition(0);
			Date strtodate = formatter.parse(strDate, pos);
			SimpleDateFormat formatter2 = new SimpleDateFormat(toFormat);
			return formatter2.format(strtodate);
		} catch (Exception e) {
			return strDate;
		}
	}
	
	/**
	 * 比较指定日期 与今天的日期差
	 * @param date
	 * @return
	 */
	public int compareTime(String date){
		String today = getStandardDate("yyyy-MM-dd");
		long dateStr2Long = dateStr2Long(date, "yyyy-MM-dd");
		long todaylong = dateStr2Long(today, "yyyy-MM-dd");
		long time = todaylong-dateStr2Long;
		return (int) Math.abs((time/(1000*24*60*60)));
	}
	
	/**
	 * 圈子格式化回复时间
	 * 
	 * @param replyTime
	 * @param pattern
	 * @return
	 */
	public String formatReplyTime(String replyTime, String pattern) {
		if (TextUtils.isEmpty(pattern)) {
			pattern = "yyyy-MM-dd HH:mm:ss";
		}
		long dateStr2Long = dateStr2Long(replyTime, pattern);
		int timeDifference = (int) Math.abs((dateStr2Long - new Date()
				.getTime()) / (1000 * 60 * 60 * 24));
		if (timeDifference == 0) {
			return formatTime(replyTime);
		} else {
			Calendar calendar = Calendar.getInstance();
//			calendar.setTime(date);
			calendar.setTimeInMillis(dateStr2Long);
			if (timeDifference < 7) {
				int day = calendar.get(Calendar.DAY_OF_WEEK);
				String[] week = new String[] { "周日", "周一", "周二", "周三", "周四",
						"周五", "周六" };
				return week[day - 1];
			} else {
				StringBuffer sb = new StringBuffer();
				sb.append(calendar.get(Calendar.MONTH) + 1);
				sb.append("月");
				sb.append(calendar.get(Calendar.DAY_OF_MONTH));
				sb.append("日");
				return sb.toString();
			}
		}
	}

	public boolean afterToday(int year , int month ,int day , int hour ,int minute ,String pattern){
		StringBuffer sb = new StringBuffer();
		sb.append(year);
		sb.append("-");
		sb.append(StringUtils.formatnum(month, "00") + "-");
		sb.append(StringUtils.formatnum(day, "00") + " ");
		sb.append(StringUtils.formatnum(hour, "00") + ":");
		sb.append(StringUtils.formatnum(minute, "00"));
		return afterToday(sb.toString(), pattern);
	}
	
	public boolean afterToday(String checkDate, String pattern) {
		if (TextUtils.isEmpty(pattern)) {
			int length = checkDate.length();
			switch (length) {
			case 10:
				pattern = "yyyy-MM-dd";
				break;
			case 16:
				pattern = "yyyy-MM-dd HH:mm";
				break;
			case 19:
				pattern = "yyyy-MM-dd HH:mm:ss";
				break;
			default:
				pattern = "yyyy-MM-dd";
				break;
			}
		}
		String today = getStandardDate(pattern);
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		try {
			MyUtils.showLog("",
					"checkDate " + checkDate + " "
							+ sdf.parse(checkDate).getTime() + " today "
							+ sdf.parse(today).getTime());
			return sdf.parse(checkDate).after(sdf.parse(today));
		} catch (ParseException e) {
			return false;
		}
	}

	public boolean isSameDay(String day1 , String day2){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
			Date date1 = sdf.parse(day1);
			Date date2 = sdf.parse(day2);
			return date1.compareTo(date2)==0?true:false;
		} catch (ParseException e) {
			if (day1.length()>=10&&day2.length()>=10) {
				return day1.substring(0,10).compareTo(day1.substring(0, 10))==0?true:false;
			} else {
				return day1.compareTo(day2)==0?true:false;
			}
		}
	}
	
	/**
	 * 取出日期 月份 格式化成 MM/dd
	 * @param time
	 * @param module
	 * @return
	 */
	public String getFormatDate(String time , String module){
		String date = time.substring(8, 10);
		String month = time.substring(5,7);
		return month+"/"+date;
//		return getFormatDate(date, month, module);
	}
	
	/**
	 * 时间日期字符串格式化
	 * 
	 * @param date
	 *            纯数字
	 * @param month
	 *            纯数字 格式有误 直接返回当前时间
	 * @param module
	 *            格式有误 即为默认格式
	 * @return
	 */
	public String getFormatDate(String date, String month, String module) {
		if (TextUtils.isEmpty(module)) {
			module = "MM/dd";
		}
		SimpleDateFormat sdf = new SimpleDateFormat(module);
		Calendar ca = Calendar.getInstance();
		if (StringUtils.pureNum(date) && StringUtils.pureNum(month)) {
			ca.set(Calendar.MONTH, Integer.valueOf(month)-1);
			ca.set(Calendar.DAY_OF_MONTH, Integer.valueOf(date));
			return sdf.format(ca.getTime());
		} else {
			return sdf.format(ca.getTime());
		}

	}

	@SuppressLint("SimpleDateFormat")
	public String HourFormat(int hour, int minute) {

		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		Calendar ca = Calendar.getInstance();
		ca.set(Calendar.HOUR_OF_DAY, hour);
		ca.set(Calendar.MINUTE, minute);
		return sdf.format(ca.getTime());
	}

	/**
	 * 将 日期字符串 转换成 long毫秒值
	 * 
	 * @param dateStr
	 * @param pattern
	 * @return
	 */
	public long dateStr2Long(String dateStr, String pattern) {
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		Date date;
		try {
			date = sdf.parse(dateStr);
		} catch (Exception e) {
			date = new Date();
		}
		return date.getTime();
	}

	/**
	 * 判断安装时间 是否今天
	 * 
	 * @param date
	 * @return
	 */
	public boolean isToday(String date) {
		int compareTo = getStandardDate("yyyy-MM-dd").compareTo(date);
		return compareTo > 0;
	}

	public String hourAdd(String hour) {
		String[] split = hour.split(":");
		Calendar canlendar = Calendar.getInstance(); // java.util包
		canlendar.set(Calendar.HOUR_OF_DAY, Integer.valueOf(split[0]));
		canlendar.set(Calendar.MINUTE, Integer.valueOf(split[1]));
		canlendar.add(Calendar.HOUR_OF_DAY, 1); // 日期减 如果不够减会将月变动
		SimpleDateFormat sdfd = new SimpleDateFormat("HH:mm");
		return sdfd.format(canlendar.getTime());
	}

	public String dateAddFrom(int days, String fromDate, String module , int type) {
		return dateAdd(days, fromDate, module , type);
	}

	/**
	 * 将从指定日期加减n天数。 如传入整型-5 意为将当前日期减去5天的日期 如传入整型5 意为将当前日期加上5天后的日期 返回字串 默认格式
	 * yyyy-MM-dd 默认日期 当前日期
	 * 
	 * @param days
	 * @return
	 */
	@SuppressLint("SimpleDateFormat")
	public String dateAdd(int days) {
		return dateAdd(days, null, null , Calendar.DATE);
	}
	
	public String minuteAdd(int minutes , String fromTime){
		return dateAdd(minutes, fromTime, "HH:mm", Calendar.MINUTE);
	}
	
	public String minuteAdd(int minutes, String time, String pattern) {
		return dateAdd(minutes, time, pattern, Calendar.MINUTE);
	}

	/**
	 * 将当前日期加减n天。 如传入整型-5 意为将当前日期减去5天的日期 如传入整型5 意为将当前日期加上5天后的日期 返回字串 默认格式
	 * yyyy-MM-dd
	 * 
	 * @param days
	 * @param module 时间格式）可以没有
	 * @type 要操作的时间类型 如 Calendar.Day 
	 * @return
	 */
	@SuppressLint("SimpleDateFormat")
	public String dateAdd(int days, String fromDate, String module , int type) {
		if (type == 0) {
			type = Calendar.DAY_OF_MONTH;
		}
		if (TextUtils.isEmpty(module)) {
			module = "yyyy-MM-dd";
		}
		// 日期处理模块 (将日期加上某些天或减去天数)返回字符串
		Calendar canlendar = Calendar.getInstance(); // java.util包
		if (!TextUtils.isEmpty(fromDate)) {
			canlendar.setTime(strToDate(fromDate, module));
		}
		canlendar.add(type, days); // 日期减 如果不够减会将月变动
		SimpleDateFormat sdfd = new SimpleDateFormat(module);
		return sdfd.format(canlendar.getTime());
	}

//	/**
//	 * 分钟数增加
//	 * @param minutes
//	 * @param fromDate
//	 * @param module
//	 * @return
//	 */
//	public String minuteAdd(int minutes, String fromTime, String module) {
//		if (TextUtils.isEmpty(module)) {
//			module = "HH:mm";
//		}
//		// 日期处理模块 (将日期加上某些天或减去天数)返回字符串
//		Calendar canlendar = Calendar.getInstance(); // java.util包
//		if (!TextUtils.isEmpty(fromTime)) {
//			canlendar.setTime(strToDate(fromTime, module));
//		}
//		canlendar.add(Calendar.MINUTE, minutes); // 日期减 如果不够减会将月变动
//		SimpleDateFormat sdfd = new SimpleDateFormat(module);
//		return sdfd.format(canlendar.getTime());
//	}
	
	
	
	/**
	 * 将Date转换为指定格式的字符串
	 * 
	 * @param dateDate
	 * @return
	 */
	public String dateToStr(java.util.Date dateDate, String module) {
		if (TextUtils.isEmpty(module)) {
			module = "yyyy-MM-dd HH:mm:ss";
		}
		SimpleDateFormat formatter = new SimpleDateFormat(module);
		String dateString = formatter.format(dateDate);
		return dateString;
	}

	/**
	 * 将时间字符串转换成指定格式的 util.Date
	 * 
	 * @param dateStr
	 * @param module
	 *            默认格式yyyy-MM-dd
	 * @return
	 */
	public Date strToDate(String dateStr, String module) {
		if (TextUtils.isEmpty(module)) {
			module = "yyyy-MM-dd";
		}
		SimpleDateFormat formatter = new SimpleDateFormat(module);
		Date date = null;
		;
		try {
			date = formatter.parse(dateStr);
		} catch (ParseException e) {
			date = new Date();
		}
		return date;
	}

	/**
	 * 去除日期字符串年份
	 * 
	 * @param 日期格式
	 *            2014-01-01
	 * @return 01-01
	 */
	public String subDate(String str) {
		return str.substring(str.indexOf("-") + 1);
	}

	/**
	 * 取得日期 只取MM-dd 并拼接成MM/dd 样式
	 * 
	 * @param str
	 *            yyyy-MM-dd HH:mm:ss
	 * @return
	 */
	public String getDate(String str) {
		if (TextUtils.isEmpty(str)) {
			return "--";
		}
		StringBuilder date = new StringBuilder(str.substring(5, 7));
		date.append("/");
		date.append(str.substring(8, 10));
		return date.toString();
	}

	/**
	 * 返回标准时间日期 yyyy-MM-dd HH:mm:ss
	 * 
	 * @return
	 */
	public String getStandardDate(String module) {
		return new SimpleDateFormat(module).format(new Date());
	}

	/**
	 * 格式化 回复时间只取到分钟数
	 * 
	 * @param replyTime
	 * @return
	 */
	public String formatReplyTime2(String replyTime) {
		if (TextUtils.isEmpty(replyTime)) {
			return "";
		}
		if (replyTime.length() > 16) {
			return replyTime.substring(0, 16);
		}
		return replyTime;
	}

	/**
	 * 格式化时间 取得小时：分钟 HH:mm
	 * 
	 * @param time
	 * @return
	 */
	public String formatTime(String time) {
		if (TextUtils.isEmpty(time)) {
			return "";
		}
		if (timeMatcher(time, "HH:mm")) {
			return time;
		}

		if (time.length()>=16) {
			return time.substring(11, 16);
		}
		return time;
	}

	/**
	 * 时间格式化 只取年月日
	 * 
	 * @param date
	 * @return
	 */
	public String formatDate2(String date) {
		if (TextUtils.isEmpty(date)) {
			return "";
		}
		if (date.length() > 10) {
			return date.substring(0, 10);
		}
		return date;
	}

	private boolean timeMatcher(String time, String pattern) {
		Pattern p = Pattern.compile(pattern);
		Matcher matcher = p.matcher(time);
		return matcher.matches();
	}

	// /**
	// * 从HH:mm 中获取hour 如果time为空 返回当前小时
	// *
	// * @param time
	// * @return
	// */
	// public int getHoutFromStr(String time) {
	// if (TextUtils.isEmpty(time)) {
	// return getCurrentTime().hour;
	// }
	// String sub = time.substring(0, 2);
	// if (sub.matches("^[0-9]*$")) {
	// return Integer.valueOf(sub);
	// } else {
	// return Integer.valueOf(sub.charAt(0));
	// }
	// }

	/**
	 * 将yyyy-mm-dd 中 - 相应替换为年月日
	 * 
	 * @param date
	 * @return
	 */
	public String formatDate(String date) {
		if (TextUtils.isEmpty(date)) {
			return "";
		}
		String[] split = date.split("-");
		StringBuffer tmp = new StringBuffer();
		for (int i = 0; i < split.length; i++) {
			tmp.append(split[i]);
			switch (i) {
			case 0: {
				tmp.append("年");
			}
				break;
			case 1: {
				tmp.append("月");
			}
				break;
			case 2: {
				tmp.append("日");
			}
				break;

			default:
				break;
			}
		}
		return tmp.toString();
	}

}
