package com.ddoctor.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.text.TextUtils;

import com.ddoctor.application.MyApplication;
import com.ddoctor.user.wapi.bean.PatientBean;
import com.google.gson.Gson;

/**
 * * SharedPreferences 的公具类
 * 
 * @author teeker_bin
 * 
 */
public class SharedUtils {
	private static final String SP_NAME = "config";
	private static SharedPreferences sharedPreferences = MyApplication
			.getInstance().getSharedPreferences(SP_NAME, Context.MODE_PRIVATE);
	private static Editor editor = sharedPreferences.edit();

	public static void removeData(String key){
		editor.remove(key);
		editor.commit();
	}
	
	public static String getUUID(){
		return sharedPreferences.getString("uuid", "");
	}
	
	public static void setUUID(String uuid){
		setString("uuid", uuid);
	}
	
	public static String getMobile(){
		return sharedPreferences.getString("mobile", "");
	}
	
	public static void setMobile(String mobile){
		if("".equals(getMobile()))
		setString("mobile", mobile);
	}

	public static String getString(String key, String defaultValue) {
		return sharedPreferences.getString(key, defaultValue);
	}

	public static int getInt(String key, int defaultValue) {
		return sharedPreferences.getInt(key, defaultValue);
	}

	public static boolean getBoolean(String key, boolean defaultValue) {
		return sharedPreferences.getBoolean(key, defaultValue);
	}

	public static void setString(String key, String value) {
		editor.putString(key, value);
		editor.commit();

	}

	public static long getLong(String key, long defaultValue) {
		return sharedPreferences.getLong(key, defaultValue);

	}

	public static float getFloat(String key , float defaultValue){
		return sharedPreferences.getFloat(key, defaultValue);
	}
	
	public static void setLong(String key, long value) {
		editor.putLong(key, value);
		editor.commit();
	}

	public static void setInt(String key, int value) {
		editor.putInt(key, value);
		editor.commit();
	}

	public static void setBoolean(String key, boolean value) {
		editor.putBoolean(key, value);
		editor.commit();
	}

	public static void setFloat(String key , float value){
		editor.putFloat(key, value);
		editor.commit();
	}

	public static void clearData() {
		editor.clear();
		editor.commit();
	}
	
	/**
	 * 将obj 序列化后以指定的键值 存储在sp
	 * 
	 * @param obj
	 * @param key
	 * @throws IOException
	 */
	private static void serialize(Object obj, String key) throws IOException {
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		ObjectOutputStream objectOutputStream = new ObjectOutputStream(
				byteArrayOutputStream);
		objectOutputStream.writeObject(obj);
		String serStr = byteArrayOutputStream.toString("ISO-8859-1");
		serStr = java.net.URLEncoder.encode(serStr, "UTF-8");
		objectOutputStream.close();
		byteArrayOutputStream.close();
//		MyUtils.showLog("************serialize: key=" + key + ", str=" + serStr);
		
//		MyUtils.showLog("serialize 序列化大小  "+serStr.length()+" json 大小 "+new Gson().toJson(obj).length());
		editor.putString(key, serStr);
		editor.putString(key+"json", new Gson().toJson(obj));
		editor.commit();
	}

	/**
	 * 反序列化
	 * 
	 * @param key
	 * @return
	 * @throws StreamCorruptedException
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	private static Object deSerialize(String key) throws StreamCorruptedException,
			IOException, ClassNotFoundException {
		String string = getString(key, "");
//		MyUtils.showLog("************deSerialize: string=" + string);
		if (TextUtils.isEmpty(string)) {
			String string2 = getString(key+"json", "");
			if (TextUtils.isEmpty(string2)) {
				MyUtils.showLog("存储的json 为空");
			}else {
				MyUtils.showLog("取出存储的json "+string2);
			}
			MyUtils.showLog("deSerialize: string == null" );
			return null;
		} else {
			String redStr = java.net.URLDecoder.decode(string, "UTF-8");
			ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(
					redStr.getBytes("ISO-8859-1"));
			ObjectInputStream objectInputStream = new ObjectInputStream(
					byteArrayInputStream);
			Object object = objectInputStream.readObject();
			objectInputStream.close();
			byteArrayInputStream.close();
			return object;
		}
	}

	public static void setLoginUserInfo(String key, PatientBean patientBean) {
		try {
			serialize(patientBean, key);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static PatientBean getLoginUserInfo(String key){
		try {
			PatientBean pb =  (PatientBean)deSerialize(key);
			return pb;
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return null;
	}
	
	public static <T> void saveSerializeObject(String key, T t) {
		try {
			serialize(t, key);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static Object getSerializeObject(String key){
		try {
			return deSerialize(key);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return null;
	}

	
	
}
