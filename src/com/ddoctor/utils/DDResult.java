package com.ddoctor.utils;

import android.os.Bundle;

import com.ddoctor.enums.RetError;

public class DDResult {

	private RetError _error = RetError.NONE;
	private String _message = "";
	private Bundle _bundle = null;
	private Object _object = null;
	private Object _object1 = null;
	
	public static DDResult makeResult(RetError error, String errMsg)
	{
		return new DDResult(error, errMsg);
	}
	
	public static DDResult makeResult(RetError error)
	{
		return new DDResult(error);
	}
	

	public DDResult(){

	}

	public DDResult(RetError error){
		setError(error);
	}

	public DDResult(RetError error, String errMsg){
		setError(error);
		setErrorMessage(errMsg);
	}

	public RetError getError(){
		return _error;
	}

	public void setError(RetError error){
		_error = error;
	}

	public String getErrorMessage()
	{
		if( _message == null || _message.length() > 0 )
			return _message;
		
		return RetError.toText(_error);
	}
	public void setErrorMessage(String message) {
		_message = message;
	}


	public void setBundle(Bundle bundle) {
		_bundle = bundle;
	}
	public Bundle getBundle() {
		return _bundle;
	}
	
	public Object getObject(){
		return _object;
	}
	
	public void setObject(Object obj){
		_object = obj;
	}
	
	public Object getObject1(){
		return _object1;
	}
	
	public void setObject1(Object obj){
		_object1 = obj;
	}
	
}
