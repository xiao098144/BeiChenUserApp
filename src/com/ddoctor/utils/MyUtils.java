package com.ddoctor.utils;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.MemoryInfo;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.telephony.TelephonyManager;
import android.text.format.Formatter;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;

import com.ddoctor.application.MyApplication;

public class MyUtils {

	private static boolean isShowLog = false;

	public static final int NETWORK_NONE = 0;
	public static final int NETWORK_WIFI = 1;
	public static final int NETWORK_MOBILE = 2;

	public static int dip2px(Context context, float dipValue) {
		final float scale = context.getResources().getDisplayMetrics().density;
		return (int) (dipValue * scale + 0.5f);
	}

	public static int dp2px(float dp, Context context) {
		return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
				context.getResources().getDisplayMetrics());
	}

	public static int px2dip(Context context, float pxValue) {
		final float scale = context.getResources().getDisplayMetrics().density;
		return (int) (pxValue / scale + 0.5f);
	}

	public static void showLog(String log) {
		if (isShowLog) {
			// System.out.println(log);
			Log.e("ddoctor", log);
		}
	}

	public static void showLog(String tag, String log) {
		if (isShowLog) {
			Log.e(tag, log);
		}
	}

	public static String get_duration_string(int duration) {
		if (duration < 0)
			duration = 0;

		String s = "";

		if (duration < 60) // 秒

			s = String.format("%d秒", duration);
		else if (duration < 60 * 60) // 分钟
		{
			int minute = duration / 60;
			int sec = duration % 60;
			s = String.format("%d分%d秒", minute, sec);
		} else {
			int hour = duration / 3600;
			int minute = (duration % 3600) / 60;
			int sec = (duration % 3600) % 60;
			s = String.format("%d小时%d分%d秒", hour, minute, sec);
		}

		return s;
	}

	public static String get_filename_from_url(String url, boolean bHasExt) {
		String[] p1 = url.split("\\/", -1);
		if (p1.length <= 0)
			return "";

		String fname = p1[p1.length - 1];

		if (bHasExt)
			return fname;

		String[] p2 = fname.split("\\.", -1);
		if (p2.length <= 0)
			return "";

		return p2[0];
	}

	public static String get_filename_ext_from_url(String url) {
		String[] p1 = url.split("\\/", -1);
		if (p1.length <= 0)
			return "";

		String fname = p1[p1.length - 1];

		String[] p2 = fname.split("\\.", -1);
		if (p2.length <= 1)
			return "";

		return p2[1];
	}

	public static String getTimeMaskString() {
		SimpleDateFormat sDateFormat = new SimpleDateFormat("yyyyMMddhhmmssSS");
		String date = sDateFormat.format(new java.util.Date());

		return date;
	}

	public static long getTickCount() {
		Date dt = new Date();
		return dt.getTime();
	}

	public static boolean checkSdCardExists() {
		if (android.os.Environment.getExternalStorageState().equals(
				android.os.Environment.MEDIA_MOUNTED))
			return true;

		return false;
	}

	public static boolean isHttpUrl(String url) {
		if (url != null && url.length() > 10
				&& url.substring(0, 7).compareToIgnoreCase("http://") == 0)
			return true;

		return false;
	}

	public static boolean FileExist(String filename) {
		File file = new File(filename);
		if (file.exists())
			return true;

		return false;
	}

	public static boolean PrivateFileExist(Context context, String filename) {
		if (filename == null || filename.length() < 1)
			return false;

		try {
			FileInputStream fin = context.openFileInput(filename);
			fin.close();
			return true;
		} catch (FileNotFoundException e) {

		} catch (Exception e) {

		}

		return false;
	}

	public static BitmapDrawable loadPrivateBitmapFile(Context context,
			String filename) {
		if (context == null || filename == null)
			return null;
		System.out.println("file::::::::::::" + filename);
		BitmapDrawable bd = null;
		try {
			FileInputStream fin = context.openFileInput(filename);

			BitmapDrawable bd1 = new BitmapDrawable(fin);
			fin.close();

			bd = bd1;
		} catch (Exception e) {

		}

		return bd;
	}

	public static BitmapDrawable loadBitmapFile(String filename) {
		if (filename == null)
			return null;

		BitmapDrawable bd = null;
		try {
			FileInputStream fin = new FileInputStream(filename);

			BitmapDrawable bd1 = new BitmapDrawable(fin);
			fin.close();

			bd = bd1;
		} catch (Exception e) {

		}

		return bd;
	}

	public static boolean copyPrivateFileToSDCard(Context context,
			String src_fname, String dst_fname) {
		try {
			FileInputStream fin = context.openFileInput(src_fname);

			FileOutputStream fout = new FileOutputStream(dst_fname);

			byte[] buf = new byte[1024];
			int len;

			while ((len = fin.read(buf)) > 0) {
				fout.write(buf, 0, len);
			}

			fout.close();
			fin.close();

			return true;
		} catch (Exception e) {

		}
		return false;
	}

	public static boolean writeFile(String filename, String data) {
		try {

			FileOutputStream fout = new FileOutputStream(filename);

			fout.write(data.getBytes(), 0, data.getBytes().length);

			fout.close();

			return true;
		} catch (Exception e) {

		}

		return false;
	}



	public static String readFile(String filename) {
		try {
			FileInputStream stream = new FileInputStream(filename);
			ByteArrayOutputStream out = new ByteArrayOutputStream(1024);
			byte[] b = new byte[1024];
			int n;
			while ((n = stream.read(b)) != -1)
				out.write(b, 0, n);
			stream.close();
			out.close();
			
			return new String(out.toByteArray(), "utf-8");
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;

	}
	
	public static boolean makeSureDirExists(String dirname) {
		File file = new File(dirname);
		if (file.exists())
			return true;

		return file.mkdirs();
	}

	public static boolean deleteFile(String filename) {
		File file = new File(filename);
		if (file.exists())
			return file.delete();

		return true;
	}

	public static String getStorageRootPath() {
		return Environment.getExternalStorageDirectory().getPath();
	}

	public static String GetSizeString(long size) {
		DecimalFormat df = new DecimalFormat();
		String style = "0.0";// 定义要显示的数字的格式
		df.applyPattern(style);// 将格式应用于格式化器

		if (size > 1024 * 1024 * 1024) // G
		{
			// sprintf(ch, "%.02fG", (float)size / (float)(1024 * 1024 * 1024));
			double d = (double) size / (double) (1024 * 1024 * 1024);
			df.applyPattern("0.0G");
			return df.format(d);
		} else if (size > 1024 * 1024) // M
		{
			// sprintf(ch, "%.02fM", (float)size / (float)(1024 * 1024));
			double d = (double) size / (double) (1024 * 1024);
			df.applyPattern("0.0M");
			return df.format(d);
		} else if (size > 1024) {

			;// sprintf(ch, "%.02fK", (float)size / (float)1024);
			double d = (double) size / (double) 1024;
			df.applyPattern("0.0K");
			return df.format(d);
		} else {

			;// sprintf(ch, "%.0fB", (float)size);
			double d = size;
			df.applyPattern("0.0B");
			return df.format(d);
		}
	}

	public static int getNetworkType(Context context) {
		ConnectivityManager connectivityManager = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);// 获取系统的连接服务
		NetworkInfo activeNetInfo = connectivityManager.getActiveNetworkInfo();// 获取网络的连接情况

		if (activeNetInfo != null) {

			if (activeNetInfo.isAvailable()) {
				if (activeNetInfo.getType() == ConnectivityManager.TYPE_WIFI) {
					// 判断WIFI网
					return NETWORK_WIFI;
				} else if (activeNetInfo.getType() == ConnectivityManager.TYPE_MOBILE) {
					// 判断3G网
					return NETWORK_MOBILE;
				}

				return NETWORK_MOBILE;
			}
		}

		return NETWORK_NONE;
	}

	public static boolean isValidURLString(String urlString) {
		if (urlString == null)
			return false;

		String s = urlString.toLowerCase();

		if (s.startsWith("http://"))
			return true;

		return false;
	}

	public static boolean isSDCardMounted() {
		String state = Environment.getExternalStorageState();
		if (Environment.MEDIA_MOUNTED.equals(state))
			return true;

		return false;
	}

	public static String getLocalImageFileNameByUrl(String url, String prefix) {
		String s = String.format("%s%s", prefix, MD5Util.getMD5Encoding(url));

		return s;
	}

	public static String getLocaldeviceId(Context _context) {
		TelephonyManager tm = (TelephonyManager) _context
				.getSystemService(Context.TELEPHONY_SERVICE);
		String deviceId = tm.getDeviceId();
		if (deviceId == null || deviceId.trim().length() == 0) {
			deviceId = String.valueOf(System.currentTimeMillis());
		}
		return deviceId;
	}

	/**
	 * 获取应用的当前版本号
	 * 
	 * @return
	 * @throws Exception
	 */
	public static String getVersionName(Context context) {
		String version = "";
		try {

			// 获取packagemanager的实例
			PackageManager packageManager = context.getPackageManager();
			// getPackageName()是你当前类的包名，0代表是获取版本信息
			PackageInfo packInfo;
			packInfo = packageManager.getPackageInfo(context.getPackageName(),
					0);
			version = packInfo.versionName;

		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return version;
	}

	public static int getVersionCode(Context context) {
		int code = 0;
		try {

			// 获取packagemanager的实例
			PackageManager packageManager = context.getPackageManager();
			// getPackageName()是你当前类的包名，0代表是获取版本信息
			PackageInfo packInfo;
			packInfo = packageManager.getPackageInfo(context.getPackageName(),
					0);
			code = packInfo.versionCode;

		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return code;
	}

	public static String getImgSavePath() {
		String path = getRootDir() + "/CartoonImgSave/";
		File destDir = new File(path);
		if (!destDir.exists()) {// 创建文件�?
			destDir.mkdirs();
		}
		return path;

	}

	/**
	 * 获取根目�?
	 */
	public static String getRootDir() {
		return Environment.getExternalStorageDirectory().getAbsolutePath();
	}

	/**
	 * 使用当前时间戳拼接一个文件名
	 * 
	 * @param format
	 * @return
	 */
	public static String getFileName() {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss_SS");
		String fileName = format.format(new Timestamp(System
				.currentTimeMillis()));
		return fileName;
	}// - 通过 Intent.ACTION_MEDIA_SCANNER_SCAN_FILE 扫描某个文件

	public static void fileScan(Context context, String fName) {
		Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
		Uri uri = Uri.fromFile(new File(fName));
		intent.setData(uri);
		context.sendBroadcast(intent);
	}

	/**
	 * 将Bitmap文件保存为本地文件
	 * 
	 * @param bmp
	 * @param filename
	 */
	public static void createImgToFile(Bitmap bmp, String filename) {
		FileOutputStream b = null;
		try {
			b = new FileOutputStream(filename);
			bmp.compress(Bitmap.CompressFormat.JPEG, 100, b);// 把数据写入文件
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			try {
				b.flush();
				b.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static Bitmap getDiskBitmap(String pathString) {
		Bitmap bitmap = null;
		try {
			File file = new File(pathString);
			if (file.exists()) {
				bitmap = BitmapFactory.decodeFile(pathString);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}

		return bitmap;
	}

	public static Bitmap loadBitmapByFile(Context context, String filename) {
		if (context == null || filename == null)
			return null;
		Bitmap bd = null;
		try {
			FileInputStream fin = context.openFileInput(filename);
			bd = BitmapFactory.decodeStream(fin);
			fin.close();

		} catch (Exception e) {

		}

		return bd;
	}

	public static boolean isNetworkAvailable() {
		ConnectivityManager cm = (ConnectivityManager) MyApplication
				.getInstance().getSystemService(Context.CONNECTIVITY_SERVICE);
		if (cm.getActiveNetworkInfo() != null
				&& cm.getActiveNetworkInfo().isAvailable()
				&& cm.getActiveNetworkInfo().isConnected()) {
			return true;
		} else {
			return false;
		}
	}

	public static int getScreenWidth(Context context) {
		if (context == null) {
			context = MyApplication.getInstance().getApplicationContext();
		}
		DisplayMetrics dm = new DisplayMetrics();
		dm = context.getResources().getDisplayMetrics();
		int screenWidth = dm.widthPixels;
		return screenWidth;
	}

	public static int getScreenHeight(Context context) {
		DisplayMetrics dm = new DisplayMetrics();
		dm = context.getResources().getDisplayMetrics();
		int screenHeight = dm.heightPixels;
		return screenHeight;
	}

	public static String getIMEI(Context context) {
		try {
			TelephonyManager tm = (TelephonyManager) context
					.getSystemService(Context.TELEPHONY_SERVICE);
//			String imei = tm.getSimSerialNumber();
			String imei = tm.getDeviceId();
			// String imsi = tm.getSubscriberId();
			return imei;
		} catch (Exception e) {

		}

		return "";
	}
	
	public static String getIMSI(Context context) {
		try {
			TelephonyManager tm = (TelephonyManager) context
					.getSystemService(Context.TELEPHONY_SERVICE);
			// String imei = tm.getSimSerialNumber();
			String imsi = tm.getSubscriberId();
			return imsi;
		} catch (Exception e) {

		}
		return "";
	}
	
	
	/**
	 * ****** 获取当前网络类型 当用wifi上的时候 getType 是 WIFI getExtraInfo是空的 当用手机上的时候
	 * getType 是MOBILE
	 * <p/>
	 * 用移动CMNET方式 getExtraInfo 的值是cmnet 用移动CMWAP方式 getExtraInfo 的值是cmwap
	 * 但是不在代理的情况下访问普通的网站访问不了
	 * <p/>
	 * 用联通3gwap方式 getExtraInfo 的值是3gwap 用联通3gnet方式 getExtraInfo 的值是3gnet
	 * 用联通uniwap方式 getExtraInfo 的值是uniwap 用联通uninet方式 getExtraInfo 的值是uninet
	 * 
	 * @return
	 */
	public static String getNetType(Context context) {
		String netType = "WIFI";
		ConnectivityManager connectionManager = (ConnectivityManager) context
				.getSystemService("connectivity");
		// 获取网络的状态信息，有下面三种方式
		NetworkInfo networkInfo = connectionManager.getActiveNetworkInfo();
		if (networkInfo == null) {
			return netType;
		}
		int nType = networkInfo.getType();

		if (nType == ConnectivityManager.TYPE_MOBILE) {
			if (networkInfo.getExtraInfo().toLowerCase().equals("cmnet")) {
				netType = "CMNET";
			} else {
				netType = "CMWAP";
			}
		} else if (nType == ConnectivityManager.TYPE_WIFI) {
			netType = "WIFI";
		}

		return netType;
	}





	public static String getLineNumber(Context context) {
		try {
			TelephonyManager tm = (TelephonyManager) context
					.getSystemService(Context.TELEPHONY_SERVICE);
			String number = tm.getLine1Number();
			return number;
		} catch (Exception e) {

		}

		return "";
	}

	public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, float roundPx) {
		int w = bitmap.getWidth();
		int h = bitmap.getHeight();
		Bitmap output = Bitmap.createBitmap(w, h, Config.ARGB_8888);
		Canvas canvas = new Canvas(output);
		final int color = 0xff424242;
		final Paint paint = new Paint();
		final Rect rect = new Rect(0, 0, w, h);
		final RectF rectF = new RectF(rect);
		paint.setAntiAlias(true);
		canvas.drawARGB(0, 0, 0, 0);
		paint.setColor(color);
		canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
		paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
		canvas.drawBitmap(bitmap, rect, rect, paint);

		return output;
	}

	public static BitmapDrawable getRoundedCornerDrawable(Context context,
			int resId, int radius) {
		Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(),
				resId);
		bitmap = getRoundedCornerBitmap(bitmap, radius);

		return new BitmapDrawable(context.getResources(), bitmap);
	}

	public static byte[] Bitmap2Bytes(Bitmap bm) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
		return baos.toByteArray();
	}

	public static String getImageContentFilePathFromUri(Activity ctx, Uri uri) {
		if (!uri.getScheme().equals("content"))
			return null;

		String[] proj = { MediaStore.Images.Media.DATA };
		Cursor cursor = ctx.managedQuery(uri, proj, null, null, null);
		if (cursor != null) {
			int actual_image_column_index = cursor
					.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
			cursor.moveToFirst();

			String img_path = cursor.getString(actual_image_column_index);

			try {
				// 4.0以上的版本会自动关闭 (4.0--14;; 4.0.3--15)
				if (Integer.parseInt(Build.VERSION.SDK) < 14) {
					cursor.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			File file = new File(img_path);
			Uri fileUri = Uri.fromFile(file);

			return fileUri.getPath();
		}

		return null;

	}

	public static Bitmap loadBitmapFromFile(String filename) {
		try {
			File file = new File(filename);

			FileInputStream fs = new FileInputStream(file);
			BufferedInputStream bs = new BufferedInputStream(fs);

			BitmapFactory.Options opts = new BitmapFactory.Options();
			// opts.inTempStorage = new byte[100 * 1024];
			opts.inPreferredConfig = Bitmap.Config.RGB_565;
			// opts.inPurgeable = true;
			// opts.inSampleSize = 4;
			// opts.inInputShareable = true;

			return BitmapFactory.decodeStream(bs, null, opts);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static boolean saveBitmapToFile(Bitmap bm, String filename) {
		File f = new File(filename);
		if (f.exists()) {
			f.delete();
		}

		try {
			FileOutputStream out = new FileOutputStream(f);
			bm.compress(Bitmap.CompressFormat.JPEG, 90, out);
			out.flush();
			out.close();
			return true;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return false;

	}

	public static String getPathFromUri(Activity activity, Uri uri) {
		String[] proj = { MediaStore.Images.Media.DATA };
		Cursor actualimagecursor = activity.managedQuery(uri, proj, null, null,
				null);
		int actual_image_column_index = actualimagecursor
				.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		actualimagecursor.moveToFirst();
		return actualimagecursor.getString(actual_image_column_index);
	}

	// 使用Bitmap加Matrix来缩放
	public static Bitmap resizeImage(Bitmap bitmap, int w, int h) {
		Bitmap BitmapOrg = bitmap;
		int width = BitmapOrg.getWidth();
		int height = BitmapOrg.getHeight();
		int newWidth = w;
		int newHeight = h;

		float scaleWidth = ((float) newWidth) / width;
		float scaleHeight = ((float) newHeight) / height;

		Matrix matrix = new Matrix();
		matrix.postScale(scaleWidth, scaleHeight);
		// if you want to rotate the Bitmap
		// matrix.postRotate(45);
		return Bitmap
				.createBitmap(BitmapOrg, 0, 0, width, height, matrix, true);
	}

	public static Bitmap resizeImage(Bitmap bmp, int max_edge) {
		int w = bmp.getWidth();
		int h = bmp.getHeight();
		int w1 = w;
		int h1 = h;
		if (w >= h) {
			if (w1 > max_edge)
				w1 = max_edge;

			h1 = w1 * h / w;
		} else {
			if (h1 > max_edge)
				h1 = max_edge;

			w1 = w * h1 / h;
		}

		return resizeImage(bmp, w1, h1);
	}

	/**
	 * 大于512k进行压缩
	 * 
	 * @param image
	 * @return
	 */
	// public static Bitmap comp(Bitmap image) {
	//
	// ByteArrayOutputStream baos = new ByteArrayOutputStream();
	// if (image != null) {
	// image.compress(Bitmap.CompressFormat.JPEG, 100, baos);
	// }
	// int ratio = baos.toByteArray().length / 1024 / 256 + 1;
	// if (baos.toByteArray().length / 1024 > 256) {//
	// 判断如果图片大于1M,进行压缩避免在生成图片（BitmapFactory.decodeStream）时溢出
	// baos.reset();// 重置baos即清空baos
	// image.compress(Bitmap.CompressFormat.JPEG, 100 / ratio, baos);//
	// 这里压缩50%，把压缩后的数据存放到baos中
	// }
	//
	// ByteArrayInputStream isBm = new ByteArrayInputStream(baos.toByteArray());
	// BitmapFactory.Options newOpts = new BitmapFactory.Options();
	// // 开始读入图片，此时把options.inJustDecodeBounds 设回true了
	// newOpts.inJustDecodeBounds = true;
	// Bitmap bitmap = BitmapFactory.decodeStream(isBm, null, newOpts);
	// newOpts.inJustDecodeBounds = false;
	// int w = newOpts.outWidth;
	// int h = newOpts.outHeight;
	// float hh = 800f;// 这里设置高度为800f
	// float ww = 480f;// 这里设置宽度为480f
	// // 缩放比。由于是固定比例缩放，只用高或者宽其中一个数据进行计算即可
	// int be = 1;// be=1表示不缩放
	// if (w > h && w > ww) {// 如果宽度大的话根据宽度固定大小缩放
	// be = (int) (newOpts.outWidth / ww);
	// } else if (w < h && h > hh) {// 如果高度高的话根据宽度固定大小缩放
	// be = (int) (newOpts.outHeight / hh);
	// }
	// if (be <= 0)
	// be = 1;
	// newOpts.inSampleSize = be;// 设置缩放比例
	// // 重新读入图片，注意此时已经把options.inJustDecodeBounds 设回false了
	// isBm = new ByteArrayInputStream(baos.toByteArray());
	// bitmap = BitmapFactory.decodeStream(isBm, null, newOpts);
	// if (image != null && !image.isRecycled()) {
	// image.recycle();
	// image = null;
	// }
	// return compressImage(bitmap);// 压缩好比例大小后再进行质量压缩
	// }

	/**
	 * 质量压缩
	 * 
	 * @param image
	 * @return
	 */
	private static Bitmap compressImage(Bitmap image) {

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		if (image != null) {
			image.compress(Bitmap.CompressFormat.JPEG, 100, baos);// 质量压缩方法，这里100表示不压缩，把压缩后的数据存放到baos中
		}
		int options = 100;
		while (baos.toByteArray().length / 1024 > 150) { // 循环判断如果压缩后图片是否大于100kb,大于继续压缩
			baos.reset();// 重置baos即清空baos
			image.compress(Bitmap.CompressFormat.JPEG, options, baos);// 这里压缩options%，把压缩后的数据存放到baos中
			options -= 5;// 每次都减少10
		}
		ByteArrayInputStream isBm = new ByteArrayInputStream(baos.toByteArray());// 把压缩后的数据baos存放到ByteArrayInputStream中
		Bitmap bitmap = BitmapFactory.decodeStream(isBm, null, null);// 把ByteArrayInputStream数据生成图片

		if (image != null && !image.isRecycled()) {
			image.recycle();
			image = null;
		}

		ByteArrayOutputStream baoss = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baoss);

		return bitmap;
	}

	public static String getAvailMemory(Context ctx) {// 获取android当前可用内存大小

		ActivityManager am = (ActivityManager) ctx
				.getSystemService(Context.ACTIVITY_SERVICE);
		MemoryInfo mi = new MemoryInfo();
		am.getMemoryInfo(mi);
		// mi.availMem; 当前系统的可用内存

		return Formatter.formatFileSize(ctx, mi.availMem);// 将获取的内存大小规格化
	}

	public static void showMemory(Context ctx) {
		MyUtils.showLog("memory: " + MyUtils.getAvailMemory(ctx));
	}

	public static byte[] GetData(String path) {// 将图片文件转化为字节数组字符串，并对其进行Base64编码处理
		String imgFile = path;// 待处理的图片
		InputStream in = null;
		byte[] data = null;
		// 读取图片字节数组
		try {
			in = new FileInputStream(imgFile);
			data = new byte[in.available()];
			in.read(data);
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		// 对字节数组Base64编码

		return data;// 返回Base64编码过的字节数组字符串

	}

	public static boolean hasImageCaptureBug() {

		// list of known devices that have the bug
		ArrayList<String> devices = new ArrayList<String>();
		devices.add("android-devphone1/dream_devphone/dream");
		devices.add("generic/sdk/generic");
		devices.add("vodafone/vfpioneer/sapphire");
		devices.add("tmobile/kila/dream");
		devices.add("verizon/voles/sholes");
		devices.add("google_ion/google_ion/sapphire");

		return devices.contains(android.os.Build.BRAND + "/"
				+ android.os.Build.PRODUCT + "/" + android.os.Build.DEVICE);

	}

	public static boolean isMobileNO(String mobiles) {

		Pattern p = Pattern
				.compile("^((1[0-9][0-9])|(15[^4,\\D])|(18[0,5-9]))\\d{8}$");

		Matcher m = p.matcher(mobiles);

		// System.out.println(m.matches()+"---");
		//
		return m.matches();

	}

	/**
	 * 比较两个BEAN的值是否相等 
	 * 
	 * @param source
	 * @param target
	 * @return
	 */
	public static boolean domainEquals(Object source, Object target) {
		if (source == null || target == null) {
			return false;
		}
		boolean rv = true;
		rv = classOfSrc(source, target, rv);
		return rv;
	}

	/**
	 * 源目标为非MAP类型时
	 * 
	 * @param source
	 * @param target
	 * @param rv
	 * @return
	 */
	private static boolean classOfSrc(Object source, Object target, boolean rv) {
		Class<?> srcClass = source.getClass();
		Field[] fields = srcClass.getDeclaredFields();
		Class<?> tarClass = target.getClass();
		Field[] fields2 = tarClass.getDeclaredFields();
		if (fields.length != fields2.length) {
			return false;
		}
		for (Field field : fields) {
			String nameKey = field.getName();
			String srcValue = getClassValue(source, nameKey) == null ? ""
					: getClassValue(source, nameKey).toString();
			String tarValue = getClassValue(target, nameKey) == null ? ""
					: getClassValue(target, nameKey).toString();
			if (!srcValue.equals(tarValue)) {
				rv = false;
				break;
			}
		}
		return rv;
	}

	/**
	 * 根据字段名称取值
	 * 
	 * @param obj
	 * @param fieldName
	 * @return
	 */
	public static Object getClassValue(Object obj, String fieldName) {
		if (obj == null) {
			return null;
		}
		try {
			Class<?> beanClass = obj.getClass();
			Method[] ms = beanClass.getMethods();
			for (int i = 0; i < ms.length; i++) {
				// 非get方法不取
				if (!ms[i].getName().startsWith("get")) {
					continue;
				}
				Object objValue = null;
				try {
					objValue = ms[i].invoke(obj, new Object[] {});
				} catch (Exception e) {
					showLog("反射取值出错：" + e.toString());
					continue;
				}
				if (objValue == null) {
					continue;
				}
				if (ms[i].getName().toUpperCase()
						.equals(fieldName.toUpperCase())
						|| ms[i].getName().substring(3).toUpperCase()
								.equals(fieldName.toUpperCase())) {
					return objValue;
				} else if (fieldName.toUpperCase().equals("SID")
						&& (ms[i].getName().toUpperCase().equals("ID") || ms[i]
								.getName().substring(3).toUpperCase()
								.equals("ID"))) {
					return objValue;
				}
			}
		} catch (Exception e) {
			// logger.info("取方法出错！" + e.toString());
		}
		return null;
	}

}
