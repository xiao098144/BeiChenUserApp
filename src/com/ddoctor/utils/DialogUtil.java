package com.ddoctor.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.PaintDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.ddoctor.interfaces.OnClickCallBackListener;
import com.beichen.user.R;
import com.ddoctor.user.adapter.DiscomfirtMultiListAdapter;
import com.ddoctor.user.adapter.IllnessMultiListAdapter;
import com.ddoctor.user.adapter.MultiChoiceHolder;
import com.ddoctor.user.adapter.SingleAdapter;
import com.ddoctor.user.wapi.bean.FoodNutritionBean;
import com.ddoctor.user.wapi.bean.IllnessBean;
import com.ddoctor.user.wapi.bean.TroubleitemBean;
import com.ddoctor.user.wheelview.ArrayWheelAdapter;
import com.ddoctor.user.wheelview.OnWheelChangedListener;
import com.ddoctor.user.wheelview.WheelView;

import fynn.app.PromptDialog;

@SuppressLint("InflateParams")
public class DialogUtil {

	public interface OnTimeSelectedListener {
		void ontimeSelected(int year, int month, int day, int hour, int minute);
	}

	public static List<IllnessBean> generateData(String[] arrays) {
		List<IllnessBean> list = new ArrayList<IllnessBean>();
		for (int i = 0; i < arrays.length; i++) {
			IllnessBean illnessBean = new IllnessBean();
			illnessBean.setId(i);
			illnessBean.setName(arrays[i]);
			list.add(illnessBean);
		}
		return list;
	}

	public static void showDatePicker(final Context context, int defaultYear,
			int defaultMonth, int defaultDay,
			final OnClickCallBackListener onClickCallBackListener,
			final int type) {

		View view = LayoutInflater.from(context).inflate(
				R.layout.layout_datepicker, null);

		final PopupWindow popupWindow = new PopupWindow(view,
				LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		popupWindow.setFocusable(true);
		popupWindow.setBackgroundDrawable(new PaintDrawable());
		popupWindow.showAtLocation(view, Gravity.BOTTOM, 0, 0);

		final DatePicker datepicker = (DatePicker) view
				.findViewById(R.id.layout_birthday_datepicker);

		// String module = context.getString(R.string.time_format_10);
		// String today = TimeUtil.getInstance().getStandardDate(module);
		// datepicker.setMaxDate(TimeUtil.getInstance().dateStr2Long(today,
		// module));
		//
		Button btn_ok = (Button) view.findViewById(R.id.dialog_btn_ensure);
		btn_ok.setText(context.getString(R.string.basic_confirm));
		Button btn_cancel = (Button) view.findViewById(R.id.dialog_btn_cancel);
		btn_cancel.setText(context.getString(R.string.basic_cancel));

		final int currentYear = TimeUtil.getInstance().getCurrentYear();
		final int currentMonthOfYear = TimeUtil.getInstance().getCurrentMonth() - 1;
		final int currentDayOfMonth = TimeUtil.getInstance().getCurrentDay();
		int year;
		if (defaultYear == 0) {
			year = currentYear;
		} else {
			year = defaultYear;
		}

		int monthOfYear;
		if (defaultMonth == 0) {
			monthOfYear = currentMonthOfYear;
		} else {
			monthOfYear = defaultMonth - 1;
		}

		int dayOfMonth = currentDayOfMonth;
		if (defaultDay == 0) {
			dayOfMonth = currentDayOfMonth;
		} else {
			dayOfMonth = defaultDay;
		}

		datepicker.init(year, monthOfYear, dayOfMonth, null);

		btn_ok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				int year = datepicker.getYear();
				int month = datepicker.getMonth() + 1;
				int day = datepicker.getDayOfMonth();

				StringBuffer name = new StringBuffer();
				name.append(year);
				name.append("-");
				name.append(StringUtils.formatnum(month, "00"));
				name.append("-");
				name.append(StringUtils.formatnum(day, "00"));
				Bundle data = new Bundle();
				data.putInt("type", type);
				data.putString("name", name.toString());
				onClickCallBackListener.onClickCallBack(data);
				popupWindow.dismiss();
			}
		});

		btn_cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				popupWindow.dismiss();
			}
		});

	}

	public static void showIllnessMultiDialog(Context context,
			final List<IllnessBean> list, List<IllnessBean> chosed,
			final OnClickCallBackListener onClickCallBackListener,
			final int type) {

		MyUtils.showLog(" 多选框   数据集合  " + list.toString() + " 已选集合  "
				+ chosed.toString());

		final Dialog dialog = new Dialog(context, R.style.NoTitleDialog);
		View view = LayoutInflater.from(context).inflate(
				R.layout.layout_multichoice_dialog, null);
		TextView tv_title = (TextView) view
				.findViewById(R.id.multidialog_tv_title);
		ListView lv = (ListView) view.findViewById(R.id.multidialog_lv);
		Button btn_confirm = (Button) view
				.findViewById(R.id.bottom_btn_confirm);
		Button btn_cancel = (Button) view.findViewById(R.id.bottom_btn_cancel);

		tv_title.setText("请选择");

		final IllnessMultiListAdapter adapter = new IllnessMultiListAdapter(
				context);
		adapter.setData(list, chosed);
		lv.setAdapter(adapter);
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View view,
					int position, long arg3) {
				MultiChoiceHolder holder = (MultiChoiceHolder) view.getTag();
				holder.cb.toggle();
				adapter.getIsSelected().put(position, holder.cb.isChecked());
				if (holder.cb.isChecked()) {
					adapter.getSelectedItemSparseArray().put(position,
							list.get(position));
				} else {
					adapter.getSelectedItemSparseArray().delete(position);
				}
			}
		});
		btn_cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		btn_confirm.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Bundle data = new Bundle();
				List<IllnessBean> list2 = adapter.getSelectedItemList();
				StringBuffer nameStr = new StringBuffer();
				StringBuffer idsStr = new StringBuffer();
				if (list2 != null && list2.size() > 0) {
					for (int i = 0; i < list2.size(); i++) {
						IllnessBean illnessBean = list2.get(i);
						nameStr.append(illnessBean.getName());
						nameStr.append("、");
						idsStr.append(illnessBean.getId());
						idsStr.append("、");
					}
					String name = nameStr.toString();
					String ids = idsStr.toString();
					if (name.endsWith("、")) {
						name = name.substring(0, name.length() - 1);
					}
					if (ids.endsWith("、")) {
						ids = ids.substring(0, ids.length() - 1);
					}
					data.putString("name", name);
					data.putString("id", ids);
				} else {
					idsStr.append("43");
					data.putString("name", nameStr.toString());
					data.putString("id", idsStr.toString());
				}
				data.putInt("type", type);
				onClickCallBackListener.onClickCallBack(data);
				dialog.dismiss();
			}
		});
		showDialog(dialog, view, Gravity.BOTTOM, true);
	}

	public static void showYearPickerDialog(Context context,
			final OnClickCallBackListener onClickCallBackListener,
			final List<String> list, final int type, final int currentitem) {
		View view = LayoutInflater.from(context).inflate(
				R.layout.layout_wheelsingle_dialog, null);
		final PopupWindow popupWindow = new PopupWindow(view,
				LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		popupWindow.setFocusable(true);
		popupWindow.setBackgroundDrawable(new PaintDrawable());
		popupWindow.showAtLocation(view, Gravity.BOTTOM, 0, 0);
		final WheelView wheelView = (WheelView) view
				.findViewById(R.id.wheelview);
		String[] value = new String[list.size()];
		for (int i = 0; i < value.length; i++) {
			value[i] = list.get(i);
		}
		wheelView.setViewAdapter(new ArrayWheelAdapter<String>(context, value));
		wheelView.setCurrentItem(currentitem);
		wheelView.setVisibleItems(7);

		Button dialog_btn_cancel = (Button) view
				.findViewById(R.id.dialog_btn_cancel);
		dialog_btn_cancel.setVisibility(View.VISIBLE);
		dialog_btn_cancel.setText(context.getResources().getString(
				R.string.basic_cancel));
		dialog_btn_cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				popupWindow.dismiss();
			}
		});
		Button dialog_btn_ensure = (Button) view
				.findViewById(R.id.dialog_btn_ensure);
		dialog_btn_ensure.setVisibility(View.VISIBLE);
		dialog_btn_ensure.setText(context.getResources().getString(
				R.string.basic_confirm));
		dialog_btn_ensure.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				Bundle data = new Bundle();
				data.putInt("type", type);
				data.putString("name", list.get(wheelView.getCurrentItem()));
				onClickCallBackListener.onClickCallBack(data);
				popupWindow.dismiss();
			}
		});
	}

	public static void showSingleChoiceDialog(Context context,
			final List<IllnessBean> list,
			final OnClickCallBackListener onClickCallBackListener,
			final int type) {
		View view = LayoutInflater.from(context).inflate(
				R.layout.layout_singlechoice_dialog, null);
		final PopupWindow popupWindow = new PopupWindow(view,
				LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		popupWindow.setFocusable(true);
		popupWindow.setBackgroundDrawable(new PaintDrawable());
		popupWindow.showAtLocation(view, Gravity.BOTTOM, 0, 0);

		Button dialog_btn_cancel = (Button) view
				.findViewById(R.id.dialog_btn_cancel);
		dialog_btn_cancel.setVisibility(View.VISIBLE);
		dialog_btn_cancel.setText(context.getResources().getString(
				R.string.basic_cancel));
		dialog_btn_cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				popupWindow.dismiss();
			}
		});
		Button dialog_btn_ensure = (Button) view
				.findViewById(R.id.dialog_btn_ensure);
		dialog_btn_ensure.setVisibility(View.GONE);
		// dialog_btn_ensure.setText(context.getResources().getString(
		// R.string.basic_confirm));
		// dialog_btn_ensure.setOnClickListener(new OnClickListener() {
		//
		// @Override
		// public void onClick(View arg0) {
		//
		// }
		// });

		final ListView lv = (ListView) view.findViewById(R.id.single_lv);
		SingleAdapter adapter = new SingleAdapter(context);
		adapter.setData(list);
		lv.setAdapter(adapter);
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				final int dataIndex = arg2 - lv.getHeaderViewsCount();
				Bundle data = new Bundle();
				data.putInt("type", type);
				data.putInt("id", list.get(dataIndex).getId());
				data.putString("name", list.get(dataIndex).getName());
				onClickCallBackListener.onClickCallBack(data);
				popupWindow.dismiss();
			}
		});
	}

	public static void showDiscomfirtMultiDialog(Context context,
			final List<TroubleitemBean> list, List<TroubleitemBean> chosed,
			final OnClickCallBackListener onClickCallBackListener) {
		final Dialog dialog = new Dialog(context, R.style.NoTitleDialog);
		View view = LayoutInflater.from(context).inflate(
				R.layout.layout_multichoice_dialog, null);
		TextView tv_title = (TextView) view
				.findViewById(R.id.multidialog_tv_title);
		ListView lv = (ListView) view.findViewById(R.id.multidialog_lv);
		Button btn_confirm = (Button) view
				.findViewById(R.id.bottom_btn_confirm);
		Button btn_cancel = (Button) view.findViewById(R.id.bottom_btn_cancel);

		tv_title.setText("不适选项");

		final DiscomfirtMultiListAdapter adapter = new DiscomfirtMultiListAdapter(
				context);
		adapter.setData(list, chosed);
		lv.setAdapter(adapter);
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View view,
					int position, long arg3) {
				MultiChoiceHolder holder = (MultiChoiceHolder) view.getTag();
				holder.cb.toggle();
				adapter.getIsSelected().put(position, holder.cb.isChecked());
				if (holder.cb.isChecked()) {
					adapter.getSelectedItemSparseArray().put(position,
							list.get(position));
				} else {
					adapter.getSelectedItemSparseArray().delete(position);
				}
			}
		});
		btn_cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		btn_confirm.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				MyUtils.showLog("", "  " + adapter.getSelectedText() + " "
						+ adapter.getSelectedItemList().toString());
				Bundle data = new Bundle();
				data.putString("data", adapter.getSelectedText());
				data.putParcelableArrayList("list",
						adapter.getSelectedItemList());
				onClickCallBackListener.onClickCallBack(data);
				dialog.dismiss();
			}
		});
		showDialog(dialog, view, Gravity.CENTER, true);
	}

	public static void changeRecordList(Context context, View parent,
			final OnClickCallBackListener onClickCallBackListener) {
		if (onClickCallBackListener == null) {
			return;
		}
		View view = View.inflate(context, R.layout.layout_change_recordlist,
				null);
		view.startAnimation(AnimationUtils.loadAnimation(context,
				R.anim.fade_in));
		view.setFocusable(true);
		view.setFocusableInTouchMode(true);

		final PopupWindow popupWindow = new PopupWindow(view,
				MyUtils.getScreenWidth(context), LayoutParams.WRAP_CONTENT);//
		popupWindow.setBackgroundDrawable(new PaintDrawable());
		// 设置点击窗口外边窗口消失
		popupWindow.setOutsideTouchable(true);
		// 设置此参数获得焦点，否则无法点击
		popupWindow.setFocusable(true);
		popupWindow.showAsDropDown(parent, 0, 10);
		popupWindow.update();
		view.setOnKeyListener(new OnKeyListener() {

			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (keyCode == KeyEvent.KEYCODE_BACK) {
					popupWindow.dismiss();
					return true;
				}
				return false;
			}
		});

		TextView tv_report = (TextView) view.findViewById(R.id.record_report);
		TextView tv_diet = (TextView) view.findViewById(R.id.record_diet);
		TextView tv_sport = (TextView) view.findViewById(R.id.record_sport);
		TextView tv_medical = (TextView) view.findViewById(R.id.record_medical);
		final Bundle data = new Bundle();
		OnClickListener onClickListener = new OnClickListener() {

			@Override
			public void onClick(View v) {
				switch (v.getId()) {
				case R.id.record_report: {
					data.putInt("type", 1);
					onClickCallBackListener.onClickCallBack(data);
					popupWindow.dismiss();
				}
					break;
				case R.id.record_diet: {
					data.putInt("type", 2);
					onClickCallBackListener.onClickCallBack(data);
					popupWindow.dismiss();
				}
					break;
				case R.id.record_sport: {
					data.putInt("type", 3);
					onClickCallBackListener.onClickCallBack(data);
					popupWindow.dismiss();
				}
					break;
				case R.id.record_medical: {
					data.putInt("type", 4);
					onClickCallBackListener.onClickCallBack(data);
					popupWindow.dismiss();
				}
					break;

				default:
					break;
				}
			}
		};

		tv_report.setOnClickListener(onClickListener);
		tv_diet.setOnClickListener(onClickListener);
		tv_sport.setOnClickListener(onClickListener);
		tv_medical.setOnClickListener(onClickListener);

	}

	/**
	 * 日期时间选择
	 * 
	 * @param defaultYear
	 * @param tv
	 * @param context
	 */
	public static void dateTimePicker(int defaultYear, final Context context,
			final OnTimeSelectedListener onTimeSelectedListener) {
		if (onTimeSelectedListener == null) {
			ToastUtil.showToast("请先设置OnTimeSelectedListener");
			return;
		}
		final Dialog dialog = new Dialog(context, R.style.NoTitleDialog);
		View view = LayoutInflater.from(context).inflate(
				R.layout.layout_datetime, null);

		final DatePicker datePicker = (DatePicker) view
				.findViewById(R.id.layout_date);

		final TimePicker timePicker = (TimePicker) view
				.findViewById(R.id.layout_time);

		final int currentYear = TimeUtil.getInstance().getCurrentYear();
		final int currentMonthOfYear = TimeUtil.getInstance().getCurrentMonth();
		final int currentDayOfMonth = TimeUtil.getInstance().getCurrentDay();

		int year = defaultYear == 0 ? currentYear : defaultYear;
		int monthOfYear = currentMonthOfYear - 1;
		int dayOfMonth = currentDayOfMonth;

		datePicker.init(year, monthOfYear, dayOfMonth, null);

		timePicker.setIs24HourView(true);
		timePicker.setCurrentHour(TimeUtil.getInstance().getCurrentHour());
		timePicker.setCurrentMinute(TimeUtil.getInstance().getCurrentMinute());

		RelativeLayout top = (RelativeLayout) view
				.findViewById(R.id.relative_title);
		top.setBackgroundColor(context.getResources().getColor(
				R.color.default_titlebar));
		Button btn_cancel = (Button) view.findViewById(R.id.dialog_btn_cancel);
		Button btn_confirm = (Button) view.findViewById(R.id.dialog_btn_ensure);
		btn_cancel.setVisibility(View.VISIBLE);
		btn_confirm.setVisibility(View.VISIBLE);
		btn_cancel.setText(context.getResources().getString(
				R.string.basic_cancel));
		btn_confirm.setText(context.getResources().getString(
				R.string.basic_confirm));
		btn_cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		btn_confirm.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				int year = datePicker.getYear();
				int month = datePicker.getMonth() + 1;
				int date = datePicker.getDayOfMonth();
				int hour = timePicker.getCurrentHour();
				int minute = timePicker.getCurrentMinute();

				// if (TimeUtil.getInstance().afterToday(year, month, date,
				// hour,
				// minute, "yyyy-MM-dd HH:mm")) {
				// ToastUtil.showToast("无法选择超出今天的日期");
				// } else {
				onTimeSelectedListener.ontimeSelected(year, month, date, hour,
						minute);
				dialog.dismiss();
				// }

			}
		});

		showDialog(dialog, view, Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM,
				true);
	}

	/**
	 * 显示dialog
	 * 
	 * @param view
	 */
	private static void showDialog(Dialog dialog, View view, int gravity,
			boolean cancelable) {
		Window window = dialog.getWindow();
		window.setGravity(gravity); // 此处可以设置dialog显示的位置
		window.setLayout(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		dialog.setContentView(view);
		dialog.setCancelable(cancelable);
		dialog.setCanceledOnTouchOutside(cancelable);
		dialog.show();
	}

	public static Dialog createLoadingDialog(Context context, String msg) {
		return createLoadingDialog(context, msg, false);
	}

	public static Dialog createLoadingDialog(Context context, String msg,
			boolean showMessage) {

		LayoutInflater inflater = LayoutInflater.from(context);
		View v = inflater.inflate(R.layout.loading_dialog, null);// 得到加载view
		LinearLayout layout = (LinearLayout) v.findViewById(R.id.dialog_view);// 加载布局
		// main.xml中的ImageView
		ImageView spaceshipImage = (ImageView) v.findViewById(R.id.img);
		TextView tipTextView = (TextView) v.findViewById(R.id.tipTextView);// 提示文字
		if (msg == null || msg.length() < 1 || !showMessage)
			tipTextView.setVisibility(View.GONE);

		// 注：在小米note和小米1s上这个动画会自动播放，不用调用下面的代码，但是其它系统不行
		AnimationDrawable ad = (AnimationDrawable) spaceshipImage
				.getBackground();
		ad.start();
		final AnimationDrawable ad1 = ad;

		/*
		 * // 加载动画 Animation hyperspaceJumpAnimation =
		 * AnimationUtils.loadAnimation( context, R.anim.loading_animation); //
		 * 使用ImageView显示动画
		 * spaceshipImage.startAnimation(hyperspaceJumpAnimation);
		 */

		tipTextView.setText(msg);// 设置加载信息

		Dialog loadingDialog = new Dialog(context, R.style.loading_dialog);// 创建自定义样式dialog

		loadingDialog.setOnDismissListener(new OnDismissListener() {

			@Override
			public void onDismiss(DialogInterface dialog) {
				ad1.stop();
			}

		});

		loadingDialog.setCancelable(false);// 不可以用“返回键”取消
		loadingDialog.setContentView(layout, new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.MATCH_PARENT,
				LinearLayout.LayoutParams.MATCH_PARENT));// 设置布局
		return loadingDialog;
	}

	// public static Dialog createLoadingDialog(Context context, String msg) {
	//
	// LayoutInflater inflater = LayoutInflater.from(context);
	// View v = inflater.inflate(R.layout.loading_dialog, null);// 得到加载view
	// RelativeLayout layout = (RelativeLayout) v
	// .findViewById(R.id.dialog_view);// 加载布局
	// // main.xml中的ImageView
	// ImageView spaceshipImage = (ImageView) v.findViewById(R.id.img);
	// TextView tipTextView = (TextView) v.findViewById(R.id.tipTextView);//
	// 提示文字
	// // 加载动画
	// Animation animation = AnimationUtils.loadAnimation(context,
	// R.anim.loading_animation);
	// animation.setDuration(1000);
	// // 使用ImageView显示动画
	// spaceshipImage.startAnimation(animation);
	// tipTextView.setText(msg);// 设置加载信息
	//
	//
	// Dialog loadingDialog = new Dialog(context, R.style.loading_dialog);//
	// 创建自定义样式dialog
	// // loadingDialog.setCancelable(false);// 不可以用“返回键”取消
	// loadingDialog.setContentView(
	// layout,
	// new LinearLayout.LayoutParams(
	// MyUtils.getSecreenWidth(context) / 3, MyUtils
	// .getSecreenWidth(context) / 3));// 设置布局
	//
	// return loadingDialog;
	//
	// }

	public static Dialog createLoadingDialog(Context context) {
		return createLoadingDialog(context, "正在加载");
	}

	/**
	 * 确认对话框
	 * 
	 * @param context
	 * @param title
	 * @param content
	 */
	public static PromptDialog.Builder confirmDialog(Context context,
			String title, String content, String txtOk, String txtCancel,
			final ConfirmDialog callBack) {

		PromptDialog.Builder dialog = new PromptDialog.Builder(context);
		if ("".equals(title)) {
			dialog.setTitle("提示");
		} else {
			dialog.setTitle(title);
		}
		dialog.setViewStyle(PromptDialog.VIEW_STYLE_TITLEBAR);
		dialog.setMessage(content);
		dialog.setButton1(txtOk, new PromptDialog.OnClickListener() {

			@Override
			public void onClick(Dialog dialog, int which) {
				dialog.dismiss();
				callBack.onOKClick(null);

			}
		});

		if (txtCancel != null && !"".equals(txtCancel)) {
			dialog.setButton2(txtCancel, new PromptDialog.OnClickListener() {

				@Override
				public void onClick(Dialog dialog, int which) {
					dialog.dismiss();
					callBack.onCancleClick();

				}
			});
		}

		return dialog;
	}

	public static PromptDialog.Builder confirmDialog(Context context,
			String content, String txtOk, String txtCancle,
			final ConfirmDialog callBack) {
		return confirmDialog(context, "", content, txtOk, txtCancle, callBack);
	}

	public static PromptDialog.Builder createListDialog(Context context,
			List<String> dataList, final ListDialogCallback callBack) {
		PromptDialog.Builder dialog = new PromptDialog.Builder(context);
		dialog.setTitle("请选择");
		dialog.setViewStyle(PromptDialog.VIEW_STYLE_LIST);
		dialog.setListTextGravity(Gravity.LEFT);
		dialog.setDataList(dataList);
		dialog.setOnItemClickListener(new PromptDialog.OnClickListener() {
			@Override
			public void onClick(Dialog dialog, int which) {
				dialog.dismiss();
				if (callBack != null) {
					callBack.onItemClick(which);
				}
			}
		});

		dialog.setMessage("");
		dialog.setButton1("取消", new PromptDialog.OnClickListener() {

			@Override
			public void onClick(Dialog dialog, int which) {
				dialog.dismiss();
			}
		});

		return dialog;
	}

	public static PromptDialog.Builder createListDialog(Context context,
			String[] stringList, final ListDialogCallback callBack) {
		List<String> dataList = new ArrayList<String>();
		for (int i = 0; i < stringList.length; i++)
			dataList.add(stringList[i]);

		return createListDialog(context, dataList, callBack);
	}

	public interface ConfirmDialog {
		void onOKClick(Bundle data);

		void onCancleClick();
	}

	public interface ListDialogCallback {
		void onItemClick(int which);
	}

	public static void creatTimeDialog(Context context, int color, String left,
			String right, final OnClickCallBackListener onClickCallBackListener) {
		final String[] hours = new String[] { "0", "1", "2", "3", "4", "5",
				"6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16",
				"17", "18", "19", "20", "21", "22", "23" };
		final String[] mins = new String[] {"0", "1", "2", "3", "4", "5", "6", "7",
				"8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18",
				"19", "20", "21", "22", "23", "24", "25", "26", "27", "28",
				"29", "30", "31", "32", "33", "34", "35", "36", "37", "38",
				"39", "40", "41", "42", "43", "44", "45", "46", "47", "48",
				"49", "50", "51", "52", "53", "54", "55", "56", "57", "58",
				"59" };

		View view = LayoutInflater.from(context).inflate(
				R.layout.layout_dialog_set_time, null);
		final PopupWindow popupWindow = new PopupWindow(view,
				LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		popupWindow.setFocusable(true);
		popupWindow.setBackgroundDrawable(new PaintDrawable());
		popupWindow.showAtLocation(view, Gravity.BOTTOM, 0, 0);
		final WheelView wl_hour = (WheelView) view
				.findViewById(R.id.wheel_hour);
		final WheelView wl_min = (WheelView) view.findViewById(R.id.wheel_min);
		if (!left.equals("")) {
			Button dialog_btn_cancel = (Button) view
					.findViewById(R.id.dialog_btn_cancel);
			dialog_btn_cancel.setVisibility(View.VISIBLE);
			dialog_btn_cancel.setText(left);
			dialog_btn_cancel.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					popupWindow.dismiss();

				}
			});
		}
		if (!right.equals("")) {
			Button dialog_btn_ensure = (Button) view
					.findViewById(R.id.dialog_btn_ensure);
			dialog_btn_ensure.setVisibility(View.VISIBLE);
			dialog_btn_ensure.setText(right);
			dialog_btn_ensure.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					Bundle data = new Bundle();
					data.putInt("type", 0);
					data.putString("hour", hours[wl_hour.getCurrentItem()]);
					data.putString("minute", mins[wl_min.getCurrentItem()]);
					onClickCallBackListener.onClickCallBack(data);
					popupWindow.dismiss();
				}
			});
		}
		if (color != 0) {
			RelativeLayout rl = (RelativeLayout) view
					.findViewById(R.id.dialogBar);
			rl.setBackgroundColor(context.getResources().getColor(color));
		}
		// 添加change事件

		wl_hour.setViewAdapter(new ArrayWheelAdapter<String>(context, hours));
		// 设置可见条目数量

		wl_min.setViewAdapter(new ArrayWheelAdapter<String>(context, mins));
		wl_hour.setCurrentItem(TimeUtil.getInstance().getCurrentHour());
		wl_min.setCurrentItem(TimeUtil.getInstance().getCurrentMinute() - 1);
		wl_hour.setVisibleItems(7);
		wl_min.setVisibleItems(7);

		// wl_hour.addChangingListener(new OnWheelChangedListener() {
		//
		// @Override
		// public void onChanged(WheelView wheel, int oldValue, int newValue) {
		// // TODO Auto-generated method stub
		// hour = wl_hour.getCurrentItem();
		// }
		// });
		// // 添加change事件
		// wl_min.addChangingListener(new OnWheelChangedListener() {
		//
		// @Override
		// public void onChanged(WheelView wheel, int oldValue, int newValue) {
		// min = wl_min.getCurrentItem();
		//
		// }
		// });

	}

	/**
	 * 周期选择 通过回调接口反馈数据
	 * 
	 * @param context
	 */
	public static void creatWeekDialog(Context context, int color, String left,
			String right, final OnClickCallBackListener onClickCallBackListener) {
		View view = LayoutInflater.from(context).inflate(
				R.layout.layout_dialog_set_day, null);
		final PopupWindow popupWindow = new PopupWindow(view,
				LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		popupWindow.setFocusable(true);
		popupWindow.setBackgroundDrawable(new PaintDrawable());
		popupWindow.showAtLocation(view, Gravity.BOTTOM, 0, 0);
		// onClickCallBackListener.onDialogShowing();
		popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {

			@Override
			public void onDismiss() {
				// onClickCallBackListener.onDialogDismiss();
			}
		});

		if (color != 0) {
			RelativeLayout rl = (RelativeLayout) view
					.findViewById(R.id.dialogBar);
			rl.setBackgroundColor(context.getResources().getColor(color));
		}
		final CheckBox cb1 = (CheckBox) view.findViewById(R.id.check1);
		final CheckBox cb2 = (CheckBox) view.findViewById(R.id.check2);
		final CheckBox cb3 = (CheckBox) view.findViewById(R.id.check3);
		final CheckBox cb4 = (CheckBox) view.findViewById(R.id.check4);
		final CheckBox cb5 = (CheckBox) view.findViewById(R.id.check5);
		final CheckBox cb6 = (CheckBox) view.findViewById(R.id.check6);
		final CheckBox cb7 = (CheckBox) view.findViewById(R.id.check7);

		LinearLayout layout1 = (LinearLayout) view
				.findViewById(R.id.routing_layout1);
		LinearLayout layout2 = (LinearLayout) view
				.findViewById(R.id.routing_layout2);
		LinearLayout layout3 = (LinearLayout) view
				.findViewById(R.id.routing_layout3);
		LinearLayout layout4 = (LinearLayout) view
				.findViewById(R.id.routing_layout4);
		LinearLayout layout5 = (LinearLayout) view
				.findViewById(R.id.routing_layout5);
		LinearLayout layout6 = (LinearLayout) view
				.findViewById(R.id.routing_layout6);
		LinearLayout layout7 = (LinearLayout) view
				.findViewById(R.id.routing_layout7);
		OnClickListener onClickListener = new OnClickListener() {

			@Override
			public void onClick(View v) {
				switch (v.getId()) {
				case R.id.routing_layout1: {
					cb1.setChecked(!cb1.isChecked());
				}
					break;
				case R.id.routing_layout2: {
					cb2.setChecked(!cb2.isChecked());
				}
					break;
				case R.id.routing_layout3: {
					cb3.setChecked(!cb3.isChecked());
				}
					break;
				case R.id.routing_layout4: {
					cb4.setChecked(!cb4.isChecked());
				}
					break;
				case R.id.routing_layout5: {
					cb5.setChecked(!cb5.isChecked());
				}
					break;
				case R.id.routing_layout6: {
					cb6.setChecked(!cb6.isChecked());
				}
					break;
				case R.id.routing_layout7: {
					cb7.setChecked(!cb7.isChecked());
				}
					break;
				case R.id.dialog_btn_cancel: {
					popupWindow.dismiss();
				}
					break;
				case R.id.dialog_btn_ensure: {
					StringBuffer sb = new StringBuffer();
					sb.append(cb1.isChecked() ? 1 : 0);
					sb.append(cb2.isChecked() ? 1 : 0);
					sb.append(cb3.isChecked() ? 1 : 0);
					sb.append(cb4.isChecked() ? 1 : 0);
					sb.append(cb5.isChecked() ? 1 : 0);
					sb.append(cb6.isChecked() ? 1 : 0);
					sb.append(cb7.isChecked() ? 1 : 0);
					// if ("0000000".equals(sb.toString())) {
					//
					// }else {
					//
					// }
					Bundle data = new Bundle();
					data.putString("data", sb.toString());
					onClickCallBackListener.onClickCallBack(data);
					popupWindow.dismiss();
				}
					break;
				default:
					break;
				}
			}
		};

		layout1.setOnClickListener(onClickListener);
		layout2.setOnClickListener(onClickListener);
		layout3.setOnClickListener(onClickListener);
		layout4.setOnClickListener(onClickListener);
		layout5.setOnClickListener(onClickListener);
		layout6.setOnClickListener(onClickListener);
		layout7.setOnClickListener(onClickListener);

		if (!left.equals("")) {
			Button dialog_btn_cancel = (Button) view
					.findViewById(R.id.dialog_btn_cancel);
			dialog_btn_cancel.setVisibility(View.VISIBLE);
			dialog_btn_cancel.setText(left);
			dialog_btn_cancel.setOnClickListener(onClickListener);
		}
		if (!right.equals("")) {
			Button dialog_btn_ensure = (Button) view
					.findViewById(R.id.dialog_btn_ensure);
			dialog_btn_ensure.setVisibility(View.VISIBLE);
			dialog_btn_ensure.setText(right);
			dialog_btn_ensure.setOnClickListener(onClickListener);
		}

	}

	public static void foodSelectDialog(final Context context,
			final String[] typeArray,
			final Map<String, FoodNutritionBean[]> nameArray, int titleBgColor,
			final OnClickCallBackListener onClickCallBackListener) {
		final String unit[] = new String[8];
		for (int i = 0; i < 8; i++) {
			unit[i] = String.format("%dg", (i + 1) * 50);
		}

		View view = LayoutInflater.from(context).inflate(
				R.layout.layout_foodtype_dialog, null);
		final PopupWindow popupWindow = new PopupWindow(view,
				LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		popupWindow.setFocusable(true);
		popupWindow.setBackgroundDrawable(new PaintDrawable());
		popupWindow.showAtLocation(view, Gravity.BOTTOM, 0, 0);
		final WheelView wl_type = (WheelView) view
				.findViewById(R.id.wheelview_type);
		final WheelView wl_name = (WheelView) view
				.findViewById(R.id.wheelview_name);
		final WheelView wl_unit = (WheelView) view
				.findViewById(R.id.wheelview_unit);
		Button dialog_btn_cancel = (Button) view
				.findViewById(R.id.dialog_btn_cancel);
		dialog_btn_cancel.setVisibility(View.VISIBLE);
		dialog_btn_cancel.setText(context.getResources().getString(
				R.string.basic_cancel));
		dialog_btn_cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				popupWindow.dismiss();
			}
		});
		Button dialog_btn_ensure = (Button) view
				.findViewById(R.id.dialog_btn_ensure);
		dialog_btn_ensure.setVisibility(View.VISIBLE);
		dialog_btn_ensure.setText(context.getResources().getString(
				R.string.basic_confirm));
		dialog_btn_ensure.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				Bundle data = new Bundle();
				 MyUtils.showLog("类别  "+wl_type.getCurrentItem()+" 名称   "+nameArray.get(typeArray[wl_type.getCurrentItem()])[wl_name.getCurrentItem()]+"  分量   "+unit[wl_unit.getCurrentItem()]);
				data.putInt("type", wl_type.getCurrentItem());
				data.putString("unit", unit[wl_unit.getCurrentItem()]);
				data.putParcelable("data", nameArray.get(typeArray[wl_type
						.getCurrentItem()])[wl_name.getCurrentItem()]);
				onClickCallBackListener.onClickCallBack(data);
				popupWindow.dismiss();
			}
		});
		if (titleBgColor == 0) {
			titleBgColor = context.getResources().getColor(
					R.color.default_titlebar);
		}
		RelativeLayout rl = (RelativeLayout) view.findViewById(R.id.dialogBar);
		rl.setBackgroundColor(titleBgColor);

		// 添加change事件
		wl_type.setViewAdapter(new ArrayWheelAdapter<String>(context, typeArray));
		wl_type.setCurrentItem(0);
		wl_type.setVisibleItems(5);

		wl_name.setViewAdapter(new ArrayWheelAdapter<FoodNutritionBean>(
				context, nameArray.get(typeArray[0])));
		wl_name.setCurrentItem(0);
		wl_name.setVisibleItems(7);

		wl_unit.setViewAdapter(new ArrayWheelAdapter<String>(context, unit));
		wl_unit.setCurrentItem(0);
		wl_unit.setVisibleItems(7);

		wl_type.addChangingListener(new OnWheelChangedListener() {

			@Override
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
				int currentItem = wl_type.getCurrentItem();
				FoodNutritionBean[] fnbArray = nameArray.get(typeArray[currentItem]);
				if (fnbArray != null && fnbArray.length > 0) {
					wl_name.setViewAdapter(new ArrayWheelAdapter<FoodNutritionBean>(
							context, fnbArray));
					wl_name.setCurrentItem(0);
					wl_name.setVisibleItems(5);
				}
			}
		});

		wl_name.addChangingListener(new OnWheelChangedListener() {

			@Override
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
				wl_unit.setCurrentItem(0);
			}
		});

	}

}
