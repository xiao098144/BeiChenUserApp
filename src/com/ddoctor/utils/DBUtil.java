package com.ddoctor.utils;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class DBUtil {
//	private static DBHelper _instance = null;
	
	private SQLiteDatabase _database = null;
	private String _dbFilename = "my.db";
	
	public DBUtil()
	{
	}
	
	public void setDBName(String dbname)
	{
		_dbFilename = dbname;
	}
	
	
//	public static DBHelper getInstance()
//	{
//		if( _instance == null ){
//			_instance = new DBHelper();
//		}
//		
//		return _instance;
//	}
	
	public int db_open(Context context)
	{
		_database = context.openOrCreateDatabase(_dbFilename, Context.MODE_PRIVATE, null);
		if( _database == null )
			return 1;

		onDatabaseOpend();
		
		return 0;
	}
	
	public boolean isOpen()
	{
		if( _database == null )
			return false;
		
		return _database.isOpen();
	}
	
	public void db_close()
	{
		_database.close();
	}
	
	public void onDatabaseOpend()
	{
	}
	
	public boolean execSQL(String sql)
	{
		try
		{
			_database.execSQL(sql);
			return true;
		}
		catch(Exception e)
		{
			Log.e("error", sql);
			return false;
		}
	}
	
	public Cursor querySQL(String sql)
	{
		try{
			if (null == _database) {
				MyUtils.showLog("","???????? _database is null ");
			}
			return _database.rawQuery(sql, null);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}
}
