package com.ddoctor.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import android.content.Context;
import android.os.Environment;
import android.text.TextUtils;

/**
 * 文件操作
 */
public class FileOperationUtil {

	public static ArrayList<String> getExternalStorageDirectory() {
		String dir = new String();
		ArrayList<String> list = new ArrayList<String>();
		try {
			Runtime runtime = Runtime.getRuntime();
			Process proc = runtime.exec("mount");
			InputStream is = proc.getInputStream();
			InputStreamReader isr = new InputStreamReader(is);
			String line;
			BufferedReader br = new BufferedReader(isr);
			while ((line = br.readLine()) != null) {
				if (line.contains("secure")) {
					continue;
				}
				if (line.contains("asec")) {
					continue;
				}
				if (line.contains("fat")) {
					String columns[] = line.split(" ");
					if (columns != null && columns.length > 1) {
						dir = dir.concat(columns[1]);
						list.add(columns[1]);
					}
				} else if (line.contains("fuse")) {
					String columns[] = line.split(" ");
					if (columns != null && columns.length > 1) {
						list.add(columns[1]);
					}
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return list;
	}

	public static boolean isFileExist(String fileUrl) {
		if (TextUtils.isEmpty(fileUrl)) {
			return false;
		}
		File file = new File(fileUrl);
		return file.exists();
	}

	public static String saveCrashLogToFile(String msg, String name) {
		// 保存文件，设置文件名
		String mFileName = name + ".txt";
		String b = "\n******分割线******\n";
		try {
			String mDirectory = FilePathUtil.getMainRootPath();
			FileOutputStream mFileOutputStream = new FileOutputStream(
					mDirectory + mFileName, true);
			mFileOutputStream.write(msg.getBytes());
			mFileOutputStream.write(b.getBytes());
			mFileOutputStream.close();
			return mFileName;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 读取文件转为string
	 * 
	 * @param @param fa
	 * @param @return
	 * @return String
	 * @throws
	 */
	public static String getJsonInfo(File fa) {
		try {
			StringBuilder sb = new StringBuilder();
			BufferedReader br = new BufferedReader(new InputStreamReader(
					new FileInputStream(fa)));
			String temp = null;
			while ((temp = br.readLine()) != null) {
				sb.append(temp);
			}
			br.close();
			return sb.toString();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 获取文件名
	 * 
	 * @param @param filepaht
	 * @param @return
	 * @return String
	 * @throws
	 */
	public static String getFileName(String filepaht) {
		if (TextUtils.isEmpty(filepaht)) {
			return "tmp";
		}
		return filepaht.substring(filepaht.lastIndexOf("/") + 1,
				filepaht.length());
	}

	public static long getLastModify(String fileUrl) {
		File file = new File(fileUrl);
		if (!file.exists()) {
			return 0;
		} else {
			return file.lastModified();
		}

	}

	/**
	 * 复制文件到指定目录下
	 * 
	 * @param fromFile
	 * @param toFile
	 * @return
	 */
	public static boolean copy2SD(String fromFile, String toFile) {
		try {
			InputStream fosfrom = new FileInputStream(fromFile);
			OutputStream fosto = new FileOutputStream(toFile);
			byte bt[] = new byte[1024];
			int c;
			while ((c = fosfrom.read(bt)) > 0) {
				fosto.write(bt, 0, c);
			}
			fosfrom.close();
			fosto.close();
			return true;

		} catch (Exception ex) {
			return false;
		}
	}

	/**
	 * 根据文件路径与文件名删除文件
	 * 
	 * @param path
	 * @param fileName
	 */
	public static void deleteFile(String path, String fileName) {
		File file = new File(path, fileName);
		if (file.exists() && file.isFile()) {
			file.delete();
		}
	}

	/**
	 * 根据文件路径删除文件
	 * 
	 * @param path
	 */
	public static void deleteFile(String path) {
		File file = new File(path);
		if (file.exists() && file.isFile()) {
			file.delete();
		}
	}

	/**
	 * 将文件数组排序，目录放在上面，文件在下面
	 * 
	 * @param file
	 * @return
	 */
	public static File[] sort(File[] file) {
		ArrayList<File> list = new ArrayList<File>();
		// 放入所有目录
		for (File f : file) {
			if (f.isDirectory()) {
				list.add(f);
			}
		}
		// 放入所有文件
		for (File f : file) {
			if (f.isFile()) {
				list.add(f);
			}
		}
		return list.toArray(new File[file.length]);
	}

	/**
	 * 删除文件夹
	 * 
	 * @param @param filepath
	 * @return void
	 * @throws
	 */
	public static void deleteFolder(String filepath) {
		File file = new File(filepath);
		if (file.exists()) {
			if (file.isDirectory()) {
				File[] listFiles = file.listFiles();
				for (File f : listFiles) {
					deleteFolder(f.getAbsolutePath());
				}
				file.delete();
			} else {
				file.delete();
			}
		}
	}

	/** * 清除本应用内部缓存(/data/data/com.xxx.xxx/cache) * * @param context */
	public static void cleanInternalCache(Context context) {
		deleteFilesByDirectory(context.getCacheDir());
	}

	/** * 清除本应用所有数据库(/data/data/com.xxx.xxx/databases) * * @param context */
	public static void cleanDatabases(Context context) {
		deleteFilesByDirectory(new File("/data/data/"
				+ context.getPackageName() + "/databases"));
	}

	/**
	 * * 清除本应用SharedPreference(/data/data/com.xxx.xxx/shared_prefs) * @param
	 * context
	 */
	public static void cleanSharedPreference(Context context) {
		deleteFilesByDirectory(new File("/data/data/"
				+ context.getPackageName() + "/shared_prefs"));
	}

	/**
	 * 按名字清除本应用数据库 * context * @param dbName
	 * */
	public static void cleanDatabaseByName(Context context, String dbName) {
		context.deleteDatabase(dbName);
	}

	/** * 清除/data/data/com.xxx.xxx/files下的内容 * * @param context */
	public static void cleanFiles(Context context) {
		deleteFilesByDirectory(context.getFilesDir());
	}

	/**
	 * * 清除外部cache下的内容(/mnt/sdcard/android/data/com.xxx.xxx/cache) * * @param
	 * context
	 */
	public static void cleanExternalCache(Context context) {
		if (Environment.getExternalStorageState().equals(
				Environment.MEDIA_MOUNTED)) {
			deleteFilesByDirectory(context.getExternalCacheDir());
		}
	}

	/** * 清除自定义路径下的文件，使用需小心，请不要误删。而且只支持目录下的文件删除 * * @param filePath */
	public static void cleanCustomCache(String filePath) {
		File file = new File(filePath);
		if (null != file && file.exists()) {
			deleteFilesByDirectory(file);
		}

	}

	public static String getCacheSize() {
		return FileUtils.formatFileSize(getCacheLongSize());
	}

	/**
	 * 获取缓存空间大小 long
	 * 
	 * @return
	 */
	public static long getCacheLongSize() {
		String[] folderPath = new String[] { FilePathUtil.getCachePath(),
				FilePathUtil.getCrashLogPath(), FilePathUtil.getDownloadPath(),
				FilePathUtil.getHeadPicPath(),
				FilePathUtil.getImageCachePath().getAbsolutePath(),
				FilePathUtil.getPicPath(), FilePathUtil.getLogPath(),
				FilePathUtil.getVoicePath2() };
		long size = 0;
		for (int i = 0; i < folderPath.length; i++) {
			File file = new File(folderPath[i]);
			size += getFileSize(file);
		}
		return size;
	}

	/**
	 * 获取指定文件夹大小 long 值
	 * 
	 * @param f
	 * @return
	 */
	public static long getFileSize(File f) {
		long size = 0;
		File flist[] = f.listFiles();
		if (flist != null && flist.length > 0) {
			for (int i = 0; i < flist.length; i++) {
				if (flist[i].isDirectory()) {
					size = size + getFileSize(flist[i]);
				} else {
					size = size + flist[i].length();
				}
			}
		}
		return size;
	}

	/**
	 * 清楚App外部缓存
	 * 
	 * @param mineAct
	 * 
	 * @param folderPath
	 */
	public static void cleanAppCache(Context context) {
		// ToastUtil.showToast("开始清除缓存");
		String[] folderPath = new String[] { FilePathUtil.getCachePath(),
				FilePathUtil.getCrashLogPath(), FilePathUtil.getDownloadPath(),
				FilePathUtil.getHeadPicPath(),
				FilePathUtil.getImageCachePath().getAbsolutePath(),
				FilePathUtil.getPicPath(), FilePathUtil.getLogPath(),
				FilePathUtil.getVoicePath2() };
		for (String filePath : folderPath) {
			cleanCustomCache(filePath);
		}
		try {
			Thread.sleep(1500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			ToastUtil.showToast("缓存清理成功");
		}
	}

	/**
	 * 清除本应用所有的数据 * @param context * @param filepath
	 */
	public static void cleanApplicationData(Context context, String... filepath) {
		cleanInternalCache(context);
		cleanExternalCache(context);
		cleanDatabases(context);
		cleanSharedPreference(context);
		cleanFiles(context);
		for (String filePath : filepath) {
			cleanCustomCache(filePath);
		}
	}

	/**
	 * * 删除方法 这里只会删除某个文件夹下的文件，如果传入的directory是个文件，将不做处理 *
	 * 
	 * @param directory
	 * */
	private static void deleteFilesByDirectory(File directory) {
		if (null != directory && directory.exists() && directory.isDirectory()) {
			File[] listFiles = directory.listFiles();
			if (listFiles.length > 0) {
				for (File item : listFiles) {
					item.delete();
				}
			} else {
				directory.delete();
			}

		}
	}

	public static class FileSortUtil {
		/**
		 * 按大小
		 * 
		 * @param @param fliePath
		 * @return void
		 * @throws
		 */
		public static void orderByLength(List<File> files) {
			Collections.sort(files, new Comparator<File>() {
				public int compare(File f1, File f2) {
					long diff = f1.length() - f2.length();
					if (diff > 0)
						return 1;
					else if (diff == 0)
						return 0;
					else
						return -1;
				}

				public boolean equals(Object obj) {
					return true;
				}
			});
		}

		/**
		 * 按name
		 * 
		 * @param @param fliePath
		 * @return void
		 * @throws
		 */
		public static void orderByName(List<File> files) {
			// List<File> files = Arrays.asList(new File(fliePath).listFiles());
			Collections.sort(files, new Comparator<File>() {
				public int compare(File o1, File o2) {
					if (o1.isDirectory() && o2.isFile())
						return -1;
					if (o1.isFile() && o2.isDirectory())
						return 1;
					return o1.getName().compareTo(o2.getName());
				}
			});
		}

		/**
		 * 按日期
		 * 
		 * @param @param fliePath
		 * @return void
		 * @throws
		 */
		public static void orderByDate(List<File> files) {
			Collections.sort(files, new Comparator<File>() {
				public int compare(File f1, File f2) {
					long diff = f1.lastModified() - f2.lastModified();
					if (diff > 0)
						return 1;
					else if (diff == 0)
						return 0;
					else
						return -1;
				}

				public boolean equals(Object obj) {
					return true;
				}
			});
		}
	}
}
