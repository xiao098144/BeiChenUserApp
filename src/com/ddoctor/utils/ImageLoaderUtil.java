package com.ddoctor.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.widget.ImageView;

import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;
import com.nostra13.universalimageloader.core.imageaware.ImageAware;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

public class ImageLoaderUtil {
	private static final long discCacheLimitTime = 3600 * 24 * 15L;

	private static ImageLoader imageLoader = ImageLoader.getInstance();

	public static ImageLoader getImageLoader() {
		return imageLoader;
	}

	public ImageLoaderUtil() {
		// TODO Auto-generated constructor stub
	}

	public static void initImageLoader(Context context) {
		if( imageLoader.isInited() )
			return;
		
		ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(context);
		config.threadPriority(Thread.NORM_PRIORITY);
		config.denyCacheImageMultipleSizesInMemory();
		config.diskCacheFileNameGenerator(new Md5FileNameGenerator());
		config.diskCacheSize(100 * 1024 * 1024); // 50 MiB
		config.tasksProcessingOrder(QueueProcessingType.LIFO);
		config.diskCacheFileCount(300);
		config.diskCache(new UnlimitedDiscCache(FilePathUtil.getImageCachePath()));
//		config.writeDebugLogs(); // Remove for release app

		ImageLoader.getInstance().init(config.build());
	}
	
	public static void display(String uri, ImageAware imageAware,
			int default_pic) {
		DisplayImageOptions options = new DisplayImageOptions.Builder()
				// .showImageOnLoading(default_pic)
				.showImageForEmptyUri(default_pic).showImageOnFail(default_pic)
				.cacheInMemory(true).cacheOnDisk(true)
				.bitmapConfig(Bitmap.Config.RGB_565)
				.displayer(new SimpleBitmapDisplayer()).build();
		imageLoader.displayImage(uri, imageAware, options);
	}

	public static void display(String uri, ImageView img, int default_pic) {
		DisplayImageOptions options = new DisplayImageOptions.Builder()
				 .showImageOnLoading(default_pic)
				.showImageForEmptyUri(default_pic).showImageOnFail(default_pic)
				.cacheInMemory(true).cacheOnDisk(true)
				.bitmapConfig(Bitmap.Config.RGB_565)
				.displayer(new SimpleBitmapDisplayer()).build();
		imageLoader.displayImage(uri, img, options);
	}
	
	public static void displayRoundedCorner(String uri, ImageView img, int cornerRadiusPixels, int default_pic) {
		DisplayImageOptions options = new DisplayImageOptions.Builder()
				.showImageOnLoading(default_pic)
				.showImageForEmptyUri(default_pic).showImageOnFail(default_pic)
				.cacheInMemory(true).cacheOnDisk(true)
				.bitmapConfig(Bitmap.Config.RGB_565)
				.displayer(new RoundedBitmapDisplayer(cornerRadiusPixels)).build();
		imageLoader.displayImage(uri, img, options);
	}

	
	public static void displayListener(String uri, ImageView imageAware,
			int default_pic, ImageLoadingListener listener) {
		DisplayImageOptions options = new DisplayImageOptions.Builder()
				 .showImageOnLoading(default_pic)
				.showImageForEmptyUri(default_pic).showImageOnFail(default_pic)
				.cacheInMemory(true).cacheOnDisk(true)
				.bitmapConfig(Bitmap.Config.RGB_565)
				.displayer(new SimpleBitmapDisplayer()).build();
		imageLoader.displayImage(uri, imageAware, options, listener);
	}

	public static void clear() {
		imageLoader.clearMemoryCache();
		imageLoader.clearDiskCache();
	}

	public static void resume() {
		imageLoader.resume();
	}

	/**
	 * 暂停加载
	 */
	public static void pause() {
		imageLoader.pause();
	}

	/**
	 * 停止加载
	 */
	public static void stop() {
		imageLoader.stop();
	}

	public static void destroy() {
		imageLoader.destroy();
	}
}
