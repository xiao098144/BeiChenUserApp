package com.ddoctor.utils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import android.database.Cursor;

import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.data.MyDBUtil;
import com.ddoctor.user.wapi.bean.MedicalBean;
import com.ddoctor.user.wapi.bean.MedicalRecordBean;
import com.ddoctor.user.wapi.bean.MedicalRemindBean;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * 用药记录 常用药 增删改查
 * 
 * @author 萧
 * @Date 2015-6-9下午1:38:17
 * @TODO TODO
 */
public class MedicalDBUtil {

	private static MedicalDBUtil instance;

	private MedicalDBUtil() {
		
	}

	private int userId;
	public static MedicalDBUtil getIntance() {
		if (instance == null) {
			instance = new MedicalDBUtil();
		}
		return instance;
	}

	private final static String MEDICALHISTORY = "medicalhistory";
	private final static String MEDICALREMIND = "medicalremind";

	/**
	 * 批量插入 检验记录是否已存在 已存在 更新记录时间 否则直接插入
	 * 
	 * @param userId
	 * @param list
	 */
	public void insertMedicalHistory(List<MedicalRecordBean> list) {
		userId = DataModule.getInstance().getLoginedUserId();
		for (int i = 0; i < list.size(); i++) {
			MedicalRecordBean medicalRecordBean = list.get(i);
			if (IsItemExist(medicalRecordBean.getMedicalId())) {
				updateMedicalHistory(medicalRecordBean.getDate(),
						medicalRecordBean.getMedicalId());
			} else {
				insertMedicalHistory(list.get(i));
			}
		}
	}

	/**
	 * 插入单条记录
	 * 
	 * @param userId
	 * @param medicalRecordBean
	 */
	public void insertMedicalHistory(MedicalRecordBean medicalRecordBean) {
		userId = DataModule.getInstance().getLoginedUserId();
		String insertMedicalHistroy = "insert into " + MEDICALHISTORY
				+ " (userid,medicalid,name,unit,count)" + "VALUES(" + userId
				+ "," + medicalRecordBean.getMedicalId() + ", '"
				+ medicalRecordBean.getName() + "', '"
				+ medicalRecordBean.getUnit() + "' , '"
				+ medicalRecordBean.getCount() + "')";
		MyDBUtil.getInstance().execSQL(insertMedicalHistroy);
	};

	public void deleteMedicalHistory(List<MedicalRecordBean> list) {
		userId = DataModule.getInstance().getLoginedUserId();
		for (int i = 0; i < list.size(); i++) {
			deleteMedicalHistory(list.get(i).getMedicalId());
		}
	}

	public void deleteMedicalHistory(Integer medicalid) {
		userId = DataModule.getInstance().getLoginedUserId();
		String deleteMedicalHistory = "DELETE FROM " + MEDICALHISTORY
				+ " where userid = " + userId + "and medicalid = " + medicalid
				+ ";";
		MyDBUtil.getInstance().execSQL(deleteMedicalHistory);
	}

	public void deleteMedicalHistory(int id) {
		String deleteMedicalHistory = "DELETE FROM " + MEDICALHISTORY
				+ " where id=" + id + ";";
		MyDBUtil.getInstance().execSQL(deleteMedicalHistory);
	}

	public void updateMedicalHistory(String date, int medicalid) {
		userId = DataModule.getInstance().getLoginedUserId();
		String updateMedicalHistory = "UPDATE " + MEDICALHISTORY
				+ " SET date = '" + date + "' where userid=" + userId
				+ " and medicalid =" + medicalid + ";";
		MyDBUtil.getInstance().execSQL(updateMedicalHistory);
	}

	public boolean IsItemExist(int medicalid) {
		userId = DataModule.getInstance().getLoginedUserId();
		String selectMedicalHistory = "select * from " + MEDICALHISTORY
				+ " where userid = " + userId + " and medicalid = " + medicalid;
		Cursor cursor = MyDBUtil.getInstance().querySQL(selectMedicalHistory);
		int count = 0;
		if (cursor != null) {
			count = cursor.getCount();
			cursor.close();
		}
		return count > 0;
	}

	/**
	 * 查询所有记录 返回正常值 删除已超时记录
	 * 
	 * @return
	 */
	public List<MedicalBean> selectMedicalHistory() {
		userId = DataModule.getInstance().getLoginedUserId();
		String selectMedicalHistory = "select * from " + MEDICALHISTORY
				+ " where userid = " + userId;
		Cursor cursor = MyDBUtil.getInstance().querySQL(selectMedicalHistory);
		List<MedicalBean> resultList = new ArrayList<MedicalBean>();
		if (null != cursor) {
			List<Integer> unNormalList = new ArrayList<Integer>();
			while (cursor.moveToNext()) {
				String date = cursor.getString(cursor.getColumnIndex("date"));
				int id = cursor.getInt(cursor.getColumnIndex("id"));
				if (TimeUtil.getInstance().compareTime(date) >= 30) { // 删除超时的记录
					unNormalList.add(id);
					// deleteMedicalHistory(id);
				} else {
					MedicalBean medicalBean = new MedicalBean();
					medicalBean.setId(cursor.getInt(cursor
							.getColumnIndex("medicalid")));
					medicalBean.setName(cursor.getString(cursor
							.getColumnIndex("name")));
					medicalBean.setUnit(cursor.getString(cursor
							.getColumnIndex("unit")));
					resultList.add(medicalBean);
				}
			}
			if (cursor != null) {
				cursor.close();
			}
			if (unNormalList.size() > 0) {
				for (int i = 0; i < unNormalList.size(); i++) {
					deleteMedicalHistory(unNormalList.get(i));
				}
			}
		}
		return resultList;
	}

	/*** 用药提醒数据库操作 **/

	/**
	 * 新增用药提醒
	 * 
	 * @param medicalRemindBean
	 * @return
	 */
	public boolean insertMedicalRemind(MedicalRemindBean medicalRemindBean) {
		userId = DataModule.getInstance().getLoginedUserId();
		Gson gson = new Gson();
		String json = gson.toJson(medicalRemindBean.getDataList());

		String insertMedicalRemind = "INSERT INTO " + MEDICALREMIND
				+ "  (userid , content , medicalsize , "
				+ "time ,routing, parentid ,remark  ) " + "VALUES( " + userId
				+ " , '" + json + "', "
				+ medicalRemindBean.getDataList().size() + " , '"
				+ medicalRemindBean.getTime() + "' , '"
				+ medicalRemindBean.getRouting() + "', "
				+ medicalRemindBean.getParentId() + ", '"
				+ medicalRemindBean.getRemark() + "' );";
		MyUtils.showLog("", "isnermedicalRemind " + insertMedicalRemind);
		return MyDBUtil.getInstance().execSQL(insertMedicalRemind);
	}

	public int selectMedicalCountLatestWeek() {
		userId = DataModule.getInstance().getLoginedUserId();
		String selectSql = " select sum(medicalsize) from "
				+ MEDICALREMIND
				+ " where userid = "
				+ userId
				+ " and createtime >= date(\"now\", \"localtime\", \"weekday 1\", \"-7 day\", \"start of day\");";
		Cursor cursor = MyDBUtil.getInstance().querySQL(selectSql);
		int count = 0;
		if (cursor != null && cursor.getCount() > 0) {
			cursor.moveToFirst();
			count = cursor
					.getInt(cursor.getColumnIndex(cursor.getColumnName(0)));
			cursor.close();
		}
		return count;
	}

	/**
	 * 查询用药提醒
	 * 
	 * @return
	 */
	private ArrayList<MedicalRemindBean> selectMedicalRemind(
			String selectMedicalRemind) {
		userId = DataModule.getInstance().getLoginedUserId();
		MyUtils.showLog("", " 查询 用药提醒  " + selectMedicalRemind);
		Cursor cursor = MyDBUtil.getInstance().querySQL(selectMedicalRemind);
		ArrayList<MedicalRemindBean> dataList = new ArrayList<MedicalRemindBean>();
		if (cursor != null && cursor.getCount() > 0) {
			Gson gson = new Gson();
			Type type = new TypeToken<ArrayList<MedicalRecordBean>>() {
			}.getType();
			while (cursor.moveToNext()) {

				MedicalRemindBean medicalRemindBean = new MedicalRemindBean();
				medicalRemindBean.setId(cursor.getInt(cursor
						.getColumnIndex("id")));
				List<MedicalRecordBean> list = gson.fromJson(
						cursor.getString(cursor.getColumnIndex("content")),
						type);
				medicalRemindBean.setDataList(list);
				medicalRemindBean.setUserid(cursor.getInt(cursor
						.getColumnIndex("userid")));
				medicalRemindBean.setTime(cursor.getString(cursor
						.getColumnIndex("time")));
				medicalRemindBean.setRouting(cursor.getString(cursor
						.getColumnIndex("routing")));
				medicalRemindBean.setRemark(cursor.getString(cursor
						.getColumnIndex("remark")));
				medicalRemindBean.setParentId(cursor.getInt(cursor
						.getColumnIndex("parentid")));
				medicalRemindBean.setState(cursor.getInt(cursor
						.getColumnIndex("state")));
				dataList.add(medicalRemindBean);
			}
			cursor.close();
		}
		MyUtils.showLog("", "查询用药提醒结果 " + dataList.toString());
		return dataList;
	}

	/**
	 * 查询所有用药记录
	 * 
	 * @return
	 */
	public ArrayList<MedicalRemindBean> selectAllMedicalRemind() {
		userId = DataModule.getInstance().getLoginedUserId();
		String selectMedicalRemind = "select * from " + MEDICALREMIND
				+ " where userid = " + userId + " and parentid = " + 0 + " ;";
		return selectMedicalRemind(selectMedicalRemind);
	}

	/**
	 * 按时间查询用药记录
	 * 
	 * @param time
	 *            模糊查询 匹配周期
	 * @return
	 */
	public ArrayList<MedicalRemindBean> selectMedicalRemindByTime(String time,
			String likes) {
		userId = DataModule.getInstance().getLoginedUserId();
		// MyUtils.showLog("",
		// "查询时间  "+time+" "+likes+" "+TimeUtil.getInstance().getStandardDate("MM-dd HH:mm:ss.S"));
		String selectMedicalRemind = "select * from " + MEDICALREMIND
				+ " where userid = " + userId + " and time = '" + time
				+ "' and routing like '" + likes + "' ;";
		return selectMedicalRemind(selectMedicalRemind);
	}

	/**
	 * 关闭闹钟 修改state 为0
	 * 
	 * @param id
	 * @param parentId
	 */
	public void shutMedicalAlarmById(Integer id, Integer parentId) {
		userId = DataModule.getInstance().getLoginedUserId();
		Integer idd = id;
		MyUtils.showLog(" 关闭记录  id = " + id + "  parentId " + parentId);
		if (null != parentId && 0 != parentId) {
			deleteMedicalRemindRecordById(id);
			idd = parentId;
		}
		String shutMedicalAlarm = " UPDATE " + MEDICALREMIND + " SET state = "
				+ 0 + " where id = " + idd + " ;";
		MyUtils.showLog("关闭记录  shutMedicalAlarm  " + shutMedicalAlarm);
		MyDBUtil.getInstance().execSQL(shutMedicalAlarm);

	}

	public void deleteMedicalRemindRecordById(int id) {
		String deleteRecord = " DELETE FROM " + MEDICALREMIND + " WHERE id= "
				+ id + " ; ";
		MyDBUtil.getInstance().execSQL(deleteRecord);
	}

	public void updateMedicalRemindTimeById(String time, int id) {
		userId = DataModule.getInstance().getLoginedUserId();
		String updateMedicalRemindTime = " update " + MEDICALREMIND
				+ " set time = '" + time + "' where id " + id + " ;";
		MyDBUtil.getInstance().execSQL(updateMedicalRemindTime);
	}

}
