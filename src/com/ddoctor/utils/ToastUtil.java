package com.ddoctor.utils;

import android.view.Gravity;
import android.widget.Toast;

import com.ddoctor.application.MyApplication;


public class ToastUtil {
	public static void showToast(String str, int duration) {
		Toast toast = Toast.makeText(MyApplication.getInstance(), str, duration);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();

	}

	public static void showToast(String str) {
		showToast(str, Toast.LENGTH_SHORT);

	}
}
