package com.ddoctor.utils;

import java.util.ArrayList;

import android.database.Cursor;

import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.data.MyDBUtil;
import com.ddoctor.user.wapi.bean.SportRemindBean;

public class SportRemindDBUtil {

	private static SportRemindDBUtil _instance;

	private SportRemindDBUtil() {
	}

	public static SportRemindDBUtil getInstance() {
		if (_instance == null) {
			_instance = new SportRemindDBUtil();
		}
		return _instance;
	}

	private int _userId;
	private final static String SPORTREMIND = "sportremind";

	public boolean managerSportRemind(SportRemindBean sportRemindBean) {
		MyUtils.showLog("SportDBUtil managerSportRemind "
				+ sportRemindBean.toString());
		if (sportRemindBean.getId() == null || sportRemindBean.getId() == 0) {
			return insertSportRemind(sportRemindBean);
		} else {
			return updateSportRemind(sportRemindBean);
		}
	}

	public boolean insertSportRemind(SportRemindBean sportRemindBean) {
		_userId = DataModule.getInstance().getLoginedUserId();
		String insertSportRemind = "INSERT INTO "
				+ SPORTREMIND
				+ " (userid ,type , content , time , routing , parentid,  remark) "
				+ "VALUES(" + _userId + " , " + sportRemindBean.getType()
				+ ", '" + sportRemindBean.getContent() + "', '"
				+ sportRemindBean.getTime() + "' , " + "'"
				+ sportRemindBean.getRouting() + "', "
				+ sportRemindBean.getParentid() + " , '"
				+ sportRemindBean.getRemark() + "' );";
		MyUtils.showLog("", "insertsportremind " + insertSportRemind);
		return MyDBUtil.getInstance().execSQL(insertSportRemind);
	}

	public boolean updateSportRemind(SportRemindBean sportRemindBean) {
		String updateSql = " update  " + SPORTREMIND + " set content = '"
				+ sportRemindBean.getContent() + "', time = '"
				+ sportRemindBean.getTime() + "' , routing= '"
				+ sportRemindBean.getRouting() + "', parentid = "
				+ sportRemindBean.getParentid() + " , remark = '"
				+ sportRemindBean.getRemark() + "'";
		MyUtils.showLog("修改记录  updateSportRemind " + updateSql);
		return MyDBUtil.getInstance().execSQL(updateSql);
	}

	// public boolean

	/**
	 * 获取散步的步数
	 * 
	 * @return
	 */
	public String getWalkContent() {
		_userId = DataModule.getInstance().getLoginedUserId();
		String content = "";
		String selectWalkContent = " select content from " + SPORTREMIND
				+ " where userid = " + _userId + " and type = " + 1
				+ " and parentid = " + 0 + " ;";
		MyUtils.showLog("SportDBUtil getWalkContent  " + selectWalkContent);
		Cursor cursor = MyDBUtil.getInstance().querySQL(selectWalkContent);
		if (cursor != null && cursor.getCount() > 0) {
			cursor.moveToFirst();
			content = cursor.getString(cursor.getColumnIndex(cursor
					.getColumnName(0)));
			cursor.close();
		}
		return content;
	}

	public SportRemindBean selectSportRemindByType(int type) {
		_userId = DataModule.getInstance().getLoginedUserId();
		String selectSportRemind = " select * from " + SPORTREMIND
				+ " where userid = " + _userId + " and type = " + type
				+ " and parentid = " + 0 + " ;";
		MyUtils.showLog("查询 selectSportRemindByType " + selectSportRemind);
		SportRemindBean sportRemindBean = new SportRemindBean();
		Cursor cursor = MyDBUtil.getInstance().querySQL(selectSportRemind);
		if (cursor != null && cursor.getCount() > 0) {
			cursor.moveToFirst();
			sportRemindBean.setId(cursor.getInt(cursor.getColumnIndex("id")));
			sportRemindBean.setTime(cursor.getString(cursor
					.getColumnIndex("time")));
			sportRemindBean
					.setType(cursor.getInt(cursor.getColumnIndex("type")));
			sportRemindBean.setContent(cursor.getString(cursor
					.getColumnIndex("content")));
			sportRemindBean.setRouting(cursor.getString(cursor
					.getColumnIndex("routing")));
			sportRemindBean.setRemark(cursor.getString(cursor
					.getColumnIndex("remark")));
			sportRemindBean.setParentid(cursor.getInt(cursor
					.getColumnIndex("parentid")));
			sportRemindBean.setState(cursor.getInt(cursor
					.getColumnIndex("state")));
			cursor.close();
		}
		return sportRemindBean;

	}

	public ArrayList<SportRemindBean> selectSportRemind(String time,
			String likes) {
		_userId = DataModule.getInstance().getLoginedUserId();
		String selectSportRemind = "select * from " + SPORTREMIND
				+ " where userid = " + _userId + " and time = '" + time
				+ "' and routing like '" + likes + "' ;";
		MyUtils.showLog("", " 查询运动提醒  " + selectSportRemind);
		Cursor cursor = MyDBUtil.getInstance().querySQL(selectSportRemind);
		ArrayList<SportRemindBean> dataList = new ArrayList<SportRemindBean>();
		if (cursor != null && cursor.getCount() > 0) {
			while (cursor.moveToNext()) {
				SportRemindBean sportRemindBean = new SportRemindBean();
				sportRemindBean
						.setId(cursor.getInt(cursor.getColumnIndex("id")));
				sportRemindBean.setTime(cursor.getString(cursor
						.getColumnIndex("time")));
				sportRemindBean.setType(cursor.getInt(cursor
						.getColumnIndex("type")));
				sportRemindBean.setContent(cursor.getString(cursor
						.getColumnIndex("content")));
				sportRemindBean.setRouting(cursor.getString(cursor
						.getColumnIndex("routing")));
				sportRemindBean.setRemark(cursor.getString(cursor
						.getColumnIndex("remark")));
				sportRemindBean.setParentid(cursor.getInt(cursor
						.getColumnIndex("parentid")));
				sportRemindBean.setState(cursor.getInt(cursor
						.getColumnIndex("state")));
				dataList.add(sportRemindBean);
			}
			cursor.close();
		}
		MyUtils.showLog("", "查询运动提醒结果 " + dataList.toString());
		return dataList;
	}

	/**
	 * 关闭闹钟 修改state 为0
	 * 
	 * @param id
	 * @param parentId
	 */
	public void shutSportAlarmById(Integer id, Integer parentId) {
		_userId = DataModule.getInstance().getLoginedUserId();
		Integer idd = id;
		if (null != parentId && 0 != parentId) {
			deleteSportRemindRecordById(id);
			idd = parentId;
		}
		String shutMedicalAlarm = " UPDATE " + SPORTREMIND + " SET state = "
				+ 0 + " where id = " + idd + " ;";
		MyDBUtil.getInstance().execSQL(shutMedicalAlarm);

	}

	public void deleteSportRemindRecordById(int id) {
		String deleteRecord = " DELETE FROM " + SPORTREMIND + " WHERE id= "
				+ id + " ; ";
		MyDBUtil.getInstance().execSQL(deleteRecord);
	}

	public void updateSportRemindTimeById(String time, int id) {
		_userId = DataModule.getInstance().getLoginedUserId();
		String updateMedicalRemindTime = " update " + SPORTREMIND
				+ " set time = '" + time + "' where id " + id + " ;";
		MyDBUtil.getInstance().execSQL(updateMedicalRemindTime);
	}

}
