package com.ddoctor.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import android.util.Log;

/**
 * @filename DownloadUtil.java
 * @TODO
 * @date 2015-1-8下午3:28:34
 * @Administrator 萧
 * 
 */
public class DownloadUtil {

	private DownloadUtil() {
	}

	private static class FileDownLoadInstance {
		private static DownloadUtil voicePlayUtil = new DownloadUtil();
	}

	public static DownloadUtil getInstance() {
		return FileDownLoadInstance.voicePlayUtil;
	}

	public interface OnDownLoadFinishedListener {
		public String getFilePath();

		public void onComplete(String filePath);
	}

	private OnDownLoadFinishedListener onDownLoadFinishedListener = null;

	public void setOnDownLoadFinished(
			OnDownLoadFinishedListener onDownLoadFinishedListener) {
		this.onDownLoadFinishedListener = onDownLoadFinishedListener;
	}

	private static ExecutorService es;

	public void startDown1(String fileUrl, String fileName ,String savePath) {
		if (isExists(fileName, savePath)) {
			onDownLoadFinishedListener.onComplete(updateFile.getAbsolutePath());
		} else {
			createFile();
			if (isCreateFileSucess) {
				es = Executors.newFixedThreadPool(10);
				try {
					onDownLoadFinishedListener.onComplete(downResult(fileUrl));
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					es.shutdown();
				}
			}
		}
	}

	private String downResult(String fileUrl) throws Exception {
		Future<String> submit = es
				.submit(new DownloadUtil().new DownCallable(fileUrl));
		return submit.get();
	}

	private class DownCallable implements Callable<String> {

		private String fileUrl;

		public DownCallable(String fileUrl) {
			this.fileUrl = fileUrl;
			Log.e("", " DownCallable  ");
		}

		@Override
		public String call() throws Exception {
			try {
				URL url = new URL(fileUrl);
				InputStream inputStream;
				OutputStream outputStream;

				HttpURLConnection httpURLConnection = (HttpURLConnection) url
						.openConnection();
				httpURLConnection.setConnectTimeout(15 * 1000);
				httpURLConnection.setReadTimeout(15 * 1000);

				if (httpURLConnection.getResponseCode() == 404) {
					FileOperationUtil.deleteFile(updateFile.getAbsolutePath());
					return "error";
				}

				inputStream = httpURLConnection.getInputStream();
				outputStream = new FileOutputStream(
						updateFile.getAbsoluteFile(), false);// 文件存在则覆盖掉

				byte buffer[] = new byte[1024];
				int readsize = 0;

				while ((readsize = inputStream.read(buffer)) != -1) {
					outputStream.write(buffer, 0, readsize);
					outputStream.flush();
				}
				if (httpURLConnection != null) {
					httpURLConnection.disconnect();
				}
				inputStream.close();
				outputStream.close();

			} catch (IOException e) {

			}
			return updateFile.getAbsolutePath();
		}

	}

	private static File updateDir = null;
	private static File updateFile = null;

	private static boolean isCreateFileSucess;

	private static boolean isExists(String file_name, String savePath) {
		if (android.os.Environment.MEDIA_MOUNTED.equals(android.os.Environment
				.getExternalStorageState())) {
			updateDir = new File(savePath);
			if (!updateDir.exists()) {
				updateDir.mkdirs();
			}
			updateFile = new File(updateDir + "/" + file_name);
			return updateFile.exists();
		}
		return false;
	}

	private static void createFile() {
		if (android.os.Environment.MEDIA_MOUNTED.equals(android.os.Environment
				.getExternalStorageState())) {
			isCreateFileSucess = true;
			try {
				updateFile.createNewFile();
			} catch (IOException e) {
				isCreateFileSucess = false;
				e.printStackTrace();
			}
		} else {
			isCreateFileSucess = false;
		}
	}

}
