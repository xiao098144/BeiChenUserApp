package com.ddoctor.utils;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.util.EntityUtils;

/**
 * 有关http辅助操作类
 * 
 * 
 */
public class HttpHelper {
	public static final int CONNECTION_TIMEOUT = 10 * 1000;
	public static final int SO_TIMEOUT = 20 * 1000;
	
	@SuppressWarnings("deprecation")
	public static String http_get(String url)
	{
		HttpGet request = new HttpGet(url);
		// request.setHeader("User-Agent", MyProfile.http_user_agent);

		HttpClient httpClient = new DefaultHttpClient();
		try {
			httpClient.getParams().setParameter(
					CoreConnectionPNames.CONNECTION_TIMEOUT, CONNECTION_TIMEOUT);
			httpClient.getParams().setParameter(
					CoreConnectionPNames.SO_TIMEOUT, SO_TIMEOUT);
			HttpResponse response = httpClient.execute(request);
			if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				String str = EntityUtils.toString(response.getEntity());
				return str;
			} else {
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}


	@SuppressWarnings("deprecation")
	public static String http_post(String url, List<NameValuePair> pairs) {
		try {
			HttpPost httpPost = new HttpPost(url);
			HttpClient httpclient = new DefaultHttpClient();
			// 请求超时
			httpclient.getParams()
					.setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT,
							CONNECTION_TIMEOUT);
			// 读取超时
			httpclient.getParams().setParameter(
					CoreConnectionPNames.SO_TIMEOUT, SO_TIMEOUT);

			if (pairs != null) {
				// 请求参数设置
				HttpEntity httpentity = new UrlEncodedFormEntity(pairs, "utf8");
				httpPost.setEntity(httpentity);
			}
			HttpResponse httpResponse = httpclient.execute(httpPost);
			// 判断是否成功
			if (httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				return EntityUtils.toString(httpResponse.getEntity());
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	
	@SuppressWarnings("deprecation")
	public static String http_post(String url, String body) {
		return http_post(url, body, -1, -1);
	}
	
	@SuppressWarnings("deprecation")
	public static String http_post(String url, String body, int conn_timeout, int so_timeout) {
		if( conn_timeout == -1 )
			conn_timeout = CONNECTION_TIMEOUT;
		
		if( so_timeout == -1 )
			so_timeout = SO_TIMEOUT;
		
		try {
			HttpPost httpPost = new HttpPost(url);
			HttpClient httpclient = new DefaultHttpClient();
			// 请求超时
			httpclient.getParams()
					.setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT,
							conn_timeout);
			// 读取超时
			httpclient.getParams().setParameter(
					CoreConnectionPNames.SO_TIMEOUT, so_timeout);
			StringEntity s = new StringEntity(body, "UTF-8");
			httpPost.setEntity(s);
			
			HttpResponse httpResponse = httpclient.execute(httpPost);
			// 判断是否成功
			if (httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				
				String respString = EntityUtils.toString(httpResponse.getEntity());
//				String log = String.format("REQ========\n%s\n\nRESP========\n%s\n",
//						body, respString);
//				MyUtils.showLog(log);
				
				
				return respString;
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private static String createUrl(String host, String api) {
		String url = host;
		if (host.charAt(host.length() - 1) != '/') {
			url = host + "/";
		}
		if (api.charAt(0) == '/') {
			url += api.substring(1);
		} else {
			url += api;
		}
		return url;
	}



	public static String uploadArray(String urls, Map<String, Object> map,
			List<File> files, String pkey) {
		String result = "";
		try {
			// 定义数据分隔线
			String BOUNDARY = "------------------------7dc2fd5c0894";
			// 定义最后数据分隔线
			byte[] end_data = ("\r\n--" + BOUNDARY + "--\r\n").getBytes();
			URL url = new URL(urls);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setDoInput(true);
			conn.setUseCaches(false);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("connection", "Keep-Alive");
			conn.setRequestProperty("user-agent",
					"Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Win64; x64; Trident/5.0)");
			conn.setRequestProperty("Charsert", "UTF-8");
			conn.setRequestProperty("Content-Type",
					"multipart/form-data; boundary=" + BOUNDARY);

			OutputStream out = new DataOutputStream(conn.getOutputStream());

			// name参数
			StringBuffer params = new StringBuffer();
			for (Entry<String, Object> entry : map.entrySet()) {
				params.append("--" + BOUNDARY + "\r\n");
				params.append("Content-Disposition: form-data; name=\""
						+ entry.getKey() + "\"" + "\r\n\r\n");
				params.append(entry.getValue());
				params.append("\r\n");
			}
			out.write(params.toString().getBytes());
			for (int i = 0; i < files.size(); i++) {
				File file = files.get(i);
				StringBuilder sb = new StringBuilder();
				sb.append("--");
				sb.append(BOUNDARY);
				sb.append("\r\n");
				sb.append("Content-Disposition: form-data;name=\"image\";filename=\""
						+ file.getName() + "\"\r\n");
				// 这里不能漏掉，根据文件类型来来做处理，由于上传的是图片，所以这里可以写成image/pjpeg
				sb.append("Content-Type:image/pjpeg\r\n\r\n");
				out.write(sb.toString().getBytes());

				DataInputStream in = new DataInputStream(new FileInputStream(
						file));
				int bytes = 0;
				byte[] bufferOut = new byte[1024];
				while ((bytes = in.read(bufferOut)) != -1) {
					out.write(bufferOut, 0, bytes);
				}
				out.write("\r\n".getBytes());
				in.close();
			}
			out.write(end_data);
			out.flush();
			out.close();

			// 定义BufferedReader输入流来读取URL的响应
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					conn.getInputStream()));
			String line = "";
			while ((line = reader.readLine()) != null) {
				result = line;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public String sendPost(String url, String params) {
		String result = "";
		URL realurl = null;
		InputStream in = null;
		HttpURLConnection conn = null;
		try {
			realurl = new URL(url);
			conn = (HttpURLConnection) realurl.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			PrintWriter pw = new PrintWriter(conn.getOutputStream());
			pw.print(params);
			pw.flush();
			pw.close();
			in = conn.getInputStream();
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(in));
			String line = "";
			while ((line = reader.readLine()) != null) {
				result += line;
			}
		} catch (Exception e) {
		}
		return result;
	}

}
