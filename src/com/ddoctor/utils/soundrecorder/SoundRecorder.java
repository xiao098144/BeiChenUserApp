package com.ddoctor.utils.soundrecorder;

import java.io.File;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.AnimationDrawable;
import android.media.AudioManager;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Base64;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ddoctor.enums.RetError;
import com.beichen.user.R;
import com.ddoctor.user.data.DataModule;
import com.ddoctor.user.task.FileUploadTask;
import com.ddoctor.user.task.TaskPostCallBack;
import com.ddoctor.user.wapi.bean.UploadBean;
import com.ddoctor.user.wapi.constant.Upload;
import com.ddoctor.utils.DDResult;
import com.ddoctor.utils.DialogUtil;
import com.ddoctor.utils.DialogUtil.ConfirmDialog;
import com.ddoctor.utils.FileOperationUtil;
import com.ddoctor.utils.MyUtils;
import com.ddoctor.utils.ToastUtil;

public class SoundRecorder extends Activity implements Button.OnClickListener,
		Recorder.OnStateChangedListener {

	private static final String RECORDER_STATE_KEY = "recorder_state";

	private static final String SAMPLE_INTERRUPTED_KEY = "sample_interrupted";

	private static final String MAX_FILE_SIZE_KEY = "max_file_size";

	private static final String FILE_EXTENSION_AMR = ".amr";

	public static final int BITRATE_AMR = 2 * 1024 * 8;

	private static final int SEEK_BAR_MAX = 10000;

	private boolean mCanRequestChanged = false;

	private Recorder mRecorder;

	private RecorderReceiver mReceiver;

	private boolean mSampleInterrupted = false;

	private boolean mShowFinishButton = false;

	private String mErrorUiMessage = null; // Some error messages are displayed
											// 当一个记录因为某种原因中断在UI中显示一些错误消息

	private long mMaxFileSize = -1; // can be specified in the intent

	private RemainingTimeCalculator mRemainingTimeCalculator;

	private String mTimerFormat;
	private String mTimerFormat1;

	private long mLastClickTime;

	private int mLastButtonId;

	private final Handler mHandler = new Handler();

	private Runnable mUpdateTimer = new Runnable() {
		public void run() {
			if (!mStopUiUpdate) {
				updateTimerView();
			}
		}
	};

	private Runnable mUpdateSeekBar = new Runnable() {
		@Override
		public void run() {
			if (!mStopUiUpdate) {
				updateSeekBar();
			}
		}
	};

	private Runnable mUpdatePlayProcess = new Runnable() {

		@Override
		public void run() {
			if (!mStopUiUpdate) {
				updatePlayProcess();
			}
		}
	};

	private String userId;

	private ImageButton mFinishButton;

	private ImageButton mRecordButton;

	private ImageButton mStopButton;

	private ImageButton mPlayButton;
	private ImageButton mPauseButton;

	private ImageButton mDeleteButton;

	private LinearLayout mTimerLayout;

	private LinearLayout mVUMeterLayout;
	private ImageView img_VUMeter;
	private LinearLayout mSeekBarLayout;

	private TextView mStartTime;

	private TextView mTotalTime;

	private SeekBar mPlaySeekBar;

	private BroadcastReceiver mSDCardMountEventReceiver = null;

	private boolean mStopUiUpdate;

	private AnimationDrawable recording_anim;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mShowFinishButton = false;
		setContentView(R.layout.act_sound);
		if (userId == null) {
			userId = DataModule.getInstance().getLoginedUserId() + "";
		}
		mRecorder = new Recorder(this);
		mRecorder.setOnStateChangedListener(this);
		mReceiver = new RecorderReceiver();
		mRemainingTimeCalculator = new RemainingTimeCalculator();

		initResourceRefs();

		setResult(RESULT_CANCELED);
		registerExternalStorageListener();
		if (savedInstanceState != null) {
			Bundle recorderState = savedInstanceState
					.getBundle(RECORDER_STATE_KEY);
			if (recorderState != null) {
				mRecorder.restoreState(recorderState);
				mSampleInterrupted = recorderState.getBoolean(
						SAMPLE_INTERRUPTED_KEY, false);
				mMaxFileSize = recorderState.getLong(MAX_FILE_SIZE_KEY, -1);
			}
		}

		setVolumeControlStream(AudioManager.STREAM_MUSIC);

		if (mShowFinishButton) {
			// 它是一个记录的要求重置状态如果
			mRecorder.reset();
		}
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);

		boolean preShowFinishButton = mShowFinishButton;
		mShowFinishButton = false;
		if (mShowFinishButton || preShowFinishButton != mShowFinishButton) {
			// 重置状态如果是记录请求或状态发生了变化
			mRecorder.reset();
		}
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);

		setContentView(R.layout.act_sound);
		initResourceRefs();
		updateUi();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		if (mRecorder.sampleLength() == 0)
			return;

		Bundle recorderState = new Bundle();

		if (mRecorder.state() != Recorder.RECORDING_STATE) {
			mRecorder.saveState(recorderState);
		}
		recorderState.putBoolean(SAMPLE_INTERRUPTED_KEY, mSampleInterrupted);
		recorderState.putLong(MAX_FILE_SIZE_KEY, mMaxFileSize);

		outState.putBundle(RECORDER_STATE_KEY, recorderState);
	}

	/**
	 * Whenever the UI is re-created (due f.ex. to orientation change) we have
	 * to reinitialize references to the views. 每当UI重现(由于f.ex。方向变化)我们必须初始化引用视图。
	 */
	private void initResourceRefs() {
		mFinishButton = (ImageButton) findViewById(R.id.finishButton);
		mRecordButton = (ImageButton) findViewById(R.id.recordButton);
		mStopButton = (ImageButton) findViewById(R.id.stopButton);
		mPlayButton = (ImageButton) findViewById(R.id.playButton);
		mPauseButton = (ImageButton) findViewById(R.id.pauseButton);
		mDeleteButton = (ImageButton) findViewById(R.id.deleteButton);
		// mNewButton.setOnClickListener(this);
		mFinishButton.setOnClickListener(this);
		mRecordButton.setOnClickListener(this);
		mStopButton.setOnClickListener(this);
		mPlayButton.setOnClickListener(this);
		mPauseButton.setOnClickListener(this);
		mDeleteButton.setOnClickListener(this);

		mTimerLayout = (LinearLayout) findViewById(R.id.time_calculator);
		mVUMeterLayout = (LinearLayout) findViewById(R.id.vumeter_layout);
		img_VUMeter = (ImageView) findViewById(R.id.img0);
		mSeekBarLayout = (LinearLayout) findViewById(R.id.play_seek_bar_layout);
		mStartTime = (TextView) findViewById(R.id.starttime);
		mTotalTime = (TextView) findViewById(R.id.totaltime);
		mPlaySeekBar = (SeekBar) findViewById(R.id.play_seek_bar);
		mPlaySeekBar.setMax(SEEK_BAR_MAX);
		mPlaySeekBar.setOnSeekBarChangeListener(mSeekBarChangeListener);

		mTimerFormat = getResources().getString(R.string.timer_format);
		mTimerFormat1 = getResources().getString(R.string.timer_format1);

		if (mShowFinishButton) {
			mFinishButton.setVisibility(View.VISIBLE);
		}

		mLastClickTime = 0;
		mLastButtonId = 0;
	}

	/**
	 * Make sure we're not recording music playing in the background, ask the
	 * MediaPlaybackService to pause playback.
	 * 确保我们没有记录的背景音乐,问MediaPlaybackService暂停播放。
	 */
	private void stopAudioPlayback() {
		Intent i = new Intent("com.android.music.musicservicecommand");
		i.putExtra("command", "pause");

		sendBroadcast(i);
	}

	/*
	 * Handle the buttons.
	 */
	public void onClick(View button) {
		if (System.currentTimeMillis() - mLastClickTime < 300) {
			return;
		}

		if (!button.isEnabled())
			return;

		if (button.getId() == mLastButtonId
				&& button.getId() != R.id.finishButton) {
			// 随着录音机与UI状态是异步我们需要避免重复动作
			return;
		}

		if (button.getId() == R.id.stopButton
				&& System.currentTimeMillis() - mLastClickTime < 1500) {
			// 似乎媒体记录器不够健壮的时候它崩溃后停止记录开始
			return;
		}

		mLastClickTime = System.currentTimeMillis();
		mLastButtonId = button.getId();
		switch (button.getId()) {

		case R.id.recordButton:
			showOverwriteConfirmDialogIfConflicts();
			break;
		case R.id.stopButton:
			stopAnimation();
			mRecorder.stop();
			break;
		case R.id.playButton:
			mRecorder.startPlayback(mRecorder.playProgress());
			break;
		case R.id.pauseButton:
			mRecorder.pausePlayback();
			break;
		case R.id.finishButton:
			if (mRecorder.state() != Recorder.RECORDING_STATE) {
				if (mRecorder.state() == Recorder.PLAYING_STATE) {
					mRecorder.stop();

				}
				if (mRecorder.sampleFile() != null
						&& mRecorder.sampleLength() > 0) {

					onUploadVioce(mRecorder.sampleFile().getAbsolutePath());

				}
				mRecorder.reset();
			} else {
				mRecorder.stop();
				stopAnimation();
			}

			break;
		case R.id.deleteButton:
			showDeleteConfirmDialog();
			break;
		}
	}

	@TargetApi(Build.VERSION_CODES.GINGERBREAD_MR1)
	private void startRecording() {
		mRemainingTimeCalculator.reset();
		if (!Environment.getExternalStorageState().equals(
				Environment.MEDIA_MOUNTED)) {
			mSampleInterrupted = true;
			mErrorUiMessage = getResources().getString(R.string.insert_sd_card);
			updateUi();
		} else if (!mRemainingTimeCalculator.diskSpaceAvailable()) {
			mSampleInterrupted = true;
			mErrorUiMessage = getResources()
					.getString(R.string.storage_is_full);
			updateUi();
		} else { // 先判断SD卡是否存在 空间是否足够 否则无法录音
			stopAudioPlayback();

			boolean isHighQuality = true;
			mRemainingTimeCalculator.setBitRate(BITRATE_AMR);
			int outputfileformat = MediaRecorder.OutputFormat.AMR_NB;
			mRecorder.startRecording(outputfileformat, "recorder",
					FILE_EXTENSION_AMR, isHighQuality, mMaxFileSize);
			startAnimation();
			if (mMaxFileSize != -1) {
				mRemainingTimeCalculator.setFileSizeLimit(
						mRecorder.sampleFile(), mMaxFileSize);
			}
		}
	}

	/*
	 * Handle the "back" hardware key.
	 */
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			switch (mRecorder.state()) {
			case Recorder.IDLE_STATE:
			case Recorder.PLAYING_PAUSED_STATE:
				if (mRecorder.sampleLength() > 0) { // 存在录音文件
					showNoticeDialog();
				} else {
					finish();
				}
				break;
			case Recorder.PLAYING_STATE:
				mRecorder.stop();
				showNoticeDialog();
				break;
			case Recorder.RECORDING_STATE:
				if (mShowFinishButton) {
					mRecorder.clear();
				} else {
					mRecorder.stop();
					stopAnimation();
					showNoticeDialog();
				}
				break;
			}
			return true;
		} else {
			return super.onKeyDown(keyCode, event);
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		// Map<String, String> map = new HashMap<String, String>();
		// MobclickAgent.onEvent(this, "30012", map);
		if (mCanRequestChanged) {
			mRecorder.reset();
		}
		mCanRequestChanged = false;

		if (!mRecorder.syncStateWithService()) {
			mRecorder.reset();
		}

		if (mRecorder.state() == Recorder.RECORDING_STATE) {
			String preExtension = FILE_EXTENSION_AMR;
			if (!mRecorder.sampleFile().getName().endsWith(preExtension)) {
				mRecorder.reset();
			} else {
				mRemainingTimeCalculator.setBitRate(BITRATE_AMR);
			}
		} else {
			File file = mRecorder.sampleFile();
			if (file != null && !file.exists()) {
				mRecorder.reset();
			}
		}

		IntentFilter filter = new IntentFilter();
		filter.addAction(RecorderService.RECORDER_SERVICE_BROADCAST_NAME);
		registerReceiver(mReceiver, filter);

		mStopUiUpdate = false;
		updateUi();

		if (RecorderService.isRecording()) {
			Intent intent = new Intent(this, RecorderService.class);
			intent.putExtra(RecorderService.ACTION_NAME,
					RecorderService.ACTION_DISABLE_MONITOR_REMAIN_TIME);
			startService(intent);
		}
	}

	@Override
	protected void onPause() {
		if (mRecorder.state() != Recorder.RECORDING_STATE || mShowFinishButton
				|| mMaxFileSize != -1) {
			mRecorder.stop();

		}

		if (mReceiver != null) {
			unregisterReceiver(mReceiver);
		}

		mCanRequestChanged = true;
		mStopUiUpdate = true;

		if (RecorderService.isRecording()) {
			Intent intent = new Intent(this, RecorderService.class);
			intent.putExtra(RecorderService.ACTION_NAME,
					RecorderService.ACTION_ENABLE_MONITOR_REMAIN_TIME);
			startService(intent);
		}

		super.onPause();
	}

	@Override
	protected void onStop() {
		if (mShowFinishButton) {
			finish();
		}
		super.onStop();

	}

	private void showNoticeDialog() {
		DialogUtil.confirmDialog(this, "提示",
				" 您的语音文件尚未发送至服务器，您确定离开页面，不再发送？点击确定返回上一页，点击取消留在本页面继续。 ",
				getString(R.string.basic_confirm),
				getString(R.string.basic_cancel), new ConfirmDialog() {

					@Override
					public void onOKClick(Bundle data) {
						finish();
					}

					@Override
					public void onCancleClick() {

					}
				}).show();
	}

	private void showDeleteConfirmDialog() {
		AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
		dialogBuilder.setIcon(android.R.drawable.ic_dialog_alert);
		dialogBuilder.setTitle(R.string.delete_dialog_title);
		dialogBuilder.setPositiveButton(android.R.string.ok,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						mRecorder.delete();
					}
				});
		dialogBuilder.setNegativeButton(android.R.string.cancel,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						mLastButtonId = 0;
					}
				});
		dialogBuilder.show();
	}

	private void showOverwriteConfirmDialogIfConflicts() {
		String fileName = "recorder" + FILE_EXTENSION_AMR;
		//
		// if (mRecorder.isRecordExisted(fileName) && !mShowFinishButton) {
		// AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
		// dialogBuilder.setIcon(android.R.drawable.ic_dialog_alert);
		// dialogBuilder.setTitle(getString(R.string.overwrite_dialog_title,
		// fileName));
		// dialogBuilder.setPositiveButton(android.R.string.ok,
		// new DialogInterface.OnClickListener() {
		// @Override
		// public void onClick(DialogInterface dialog, int which) {
		// startRecording();
		// }
		// });
		// dialogBuilder.setNegativeButton(android.R.string.cancel,
		// new DialogInterface.OnClickListener() {
		// @Override
		// public void onClick(DialogInterface dialog, int which) {
		// mLastButtonId = 0;
		// }
		// });
		// dialogBuilder.show();
		// } else {
		startRecording();
		// }
	}

	/**
	 * Called on destroy to unregister the SD card mount event receiver.
	 * 呼吁摧毁注销的SD卡挂载事件接收者。
	 */
	@Override
	public void onDestroy() {
		if (mSDCardMountEventReceiver != null) {
			unregisterReceiver(mSDCardMountEventReceiver);
			mSDCardMountEventReceiver = null;
		}
		super.onDestroy();
	}

	/**
	 * Registers an intent to listen for
	 * ACTION_MEDIA_EJECT/ACTION_MEDIA_UNMOUNTED/ACTION_MEDIA_MOUNTED
	 * notifications. 寄存器的意图听ACTION_MEDIA_EJECT / ACTION_MEDIA_UNMOUNTED
	 * ACTION_MEDIA_MOUNTED通知。
	 */
	private void registerExternalStorageListener() {
		if (mSDCardMountEventReceiver == null) {
			mSDCardMountEventReceiver = new BroadcastReceiver() {
				@Override
				public void onReceive(Context context, Intent intent) {
					mSampleInterrupted = false;
					mRecorder.reset();
					updateUi();
				}
			};
			IntentFilter iFilter = new IntentFilter();
			iFilter.addAction(Intent.ACTION_MEDIA_EJECT);
			iFilter.addAction(Intent.ACTION_MEDIA_UNMOUNTED);
			iFilter.addAction(Intent.ACTION_MEDIA_MOUNTED);
			iFilter.addDataScheme("file");
			registerReceiver(mSDCardMountEventReceiver, iFilter);
		}
	}

	private ImageView getTimerImage(char number) {
		ImageView image = new ImageView(this);
		LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT);
		if (number != ':') {
			image.setBackgroundResource(R.drawable.background_number);
		}
		switch (number) {
		case '0':
			image.setImageResource(R.drawable.number_0);
			break;
		case '1':
			image.setImageResource(R.drawable.number_1);
			break;
		case '2':
			image.setImageResource(R.drawable.number_2);
			break;
		case '3':
			image.setImageResource(R.drawable.number_3);
			break;
		case '4':
			image.setImageResource(R.drawable.number_4);
			break;
		case '5':
			image.setImageResource(R.drawable.number_5);
			break;
		case '6':
			image.setImageResource(R.drawable.number_6);
			break;
		case '7':
			image.setImageResource(R.drawable.number_7);
			break;
		case '8':
			image.setImageResource(R.drawable.number_8);
			break;
		case '9':
			image.setImageResource(R.drawable.number_9);
			break;
		case ':':
			image.setImageResource(R.drawable.colon);
			break;
		}
		image.setLayoutParams(lp);
		return image;
	}

	/**
	 * Update the big MM:SS timer. If we are in playback, also update the
	 * progress bar.
	 */
	private void updateTimerView() {
		int state = mRecorder.state();

		boolean ongoing = state == Recorder.RECORDING_STATE
				|| state == Recorder.PLAYING_STATE;

		long time = mRecorder.progress();

		String timeStr = "0";

		if (time % 60 == 59) {
			stopAnimation();
			mRecorder.stop();
		} else {
			if (time % 60 >= 50) {
				timeStr = String.format(mTimerFormat1, 60 - time % 60);
			} else if (time % 60 < 50) {
				timeStr = String.format(mTimerFormat1, time % 60);
			}
			setTimerView(timeStr);
		}
		if (state == Recorder.RECORDING_STATE) {
			updateTimeRemaining();
		}

		if (ongoing) {
			mHandler.postDelayed(mUpdateTimer, 500);
		}

	}

	private void setTimerView(String timeStr) {
		mTimerLayout.removeAllViews();
		if (mSeekBarLayout.getVisibility() == View.VISIBLE) {
			mStartTime.setText(String.format(mTimerFormat, 0,
					Integer.valueOf(timeStr)));
		}
		for (int i = 0; i < timeStr.length(); i++) {
			mTimerLayout.addView(getTimerImage(timeStr.charAt(i)));
		}
	}

	private void updateSeekBar() {
		if (mRecorder.state() == Recorder.PLAYING_STATE) {
			mPlaySeekBar.setProgress((int) (SEEK_BAR_MAX * mRecorder
					.playProgress()));
			mHandler.postDelayed(mUpdateSeekBar, 10);
		}
	}

	private void updatePlayProcess() {
		if (mRecorder.state() == Recorder.PLAYING_STATE) {
			mStartTime.setText(String.format(mTimerFormat, 0,
					mRecorder.playProgress()));
			mHandler.postDelayed(mUpdatePlayProcess, 100);
		}
	}

	private void updateTimeRemaining() {
		long t = mRemainingTimeCalculator.timeRemaining();

		if (t <= 0) {
			mSampleInterrupted = true;

			int limit = mRemainingTimeCalculator.currentLowerLimit();
			switch (limit) {
			case RemainingTimeCalculator.DISK_SPACE_LIMIT:
				mErrorUiMessage = getResources().getString(
						R.string.storage_is_full);
				break;
			case RemainingTimeCalculator.FILE_SIZE_LIMIT:
				mErrorUiMessage = getResources().getString(
						R.string.max_length_reached);
				break;
			default:
				mErrorUiMessage = null;
				break;
			}
			stopAnimation();
			mRecorder.stop();
			return;
		}
	}

	/**
	 * Shows/hides the appropriate child views for the new state.
	 */
	private void updateUi() {
		switch (mRecorder.state()) {
		case Recorder.IDLE_STATE:
			mLastButtonId = 0;
		case Recorder.PLAYING_PAUSED_STATE:
			if (mRecorder.sampleLength() == 0) {
				mRecordButton.setVisibility(View.VISIBLE);
				mStopButton.setVisibility(View.GONE);
				mPlayButton.setVisibility(View.GONE);
				mPauseButton.setVisibility(View.GONE);
				mDeleteButton.setEnabled(false);
				mRecordButton.requestFocus();

				mVUMeterLayout.setVisibility(View.VISIBLE);
				mSeekBarLayout.setVisibility(View.GONE);
			} else {
				mRecordButton.setVisibility(View.GONE);
				mStopButton.setVisibility(View.GONE);
				mPlayButton.setVisibility(View.VISIBLE);
				mPauseButton.setVisibility(View.GONE);
				mDeleteButton.setEnabled(true);
				mPauseButton.requestFocus();

				mVUMeterLayout.setVisibility(View.GONE);
				mSeekBarLayout.setVisibility(View.VISIBLE);
				mStartTime.setText(String.format(mTimerFormat, 0, 0));
				mTotalTime.setText(String.format(mTimerFormat,
						mRecorder.sampleLength() / 60,
						mRecorder.sampleLength() % 60));
			}

			if (mRecorder.sampleLength() > 0) {
				if (mRecorder.state() != Recorder.PLAYING_PAUSED_STATE) {
					mPlaySeekBar.setProgress(0);
				}
			}
			if (mSampleInterrupted && mErrorUiMessage == null) {
				Toast.makeText(this, R.string.recording_stopped,
						Toast.LENGTH_SHORT).show();
			}

			if (mErrorUiMessage != null) {
				Toast.makeText(this, mErrorUiMessage, Toast.LENGTH_SHORT)
						.show();
			}

			break;
		case Recorder.RECORDING_STATE:
			mRecordButton.setVisibility(View.GONE);
			mStopButton.setVisibility(View.VISIBLE);
			mPlayButton.setVisibility(View.GONE);
			mPauseButton.setVisibility(View.GONE);
			mDeleteButton.setEnabled(false);
			mStopButton.requestFocus();

			mVUMeterLayout.setVisibility(View.VISIBLE);
			mSeekBarLayout.setVisibility(View.GONE);
			break;

		case Recorder.PLAYING_STATE:
			mRecordButton.setVisibility(View.GONE);
			mStopButton.setVisibility(View.GONE);
			mPlayButton.setVisibility(View.GONE);
			mPauseButton.setVisibility(View.VISIBLE);
			mDeleteButton.setEnabled(false);
			mPauseButton.requestFocus();

			mSeekBarLayout.setVisibility(View.VISIBLE);

			break;
		}

		updateTimerView();
		updateSeekBar();

	}

	private void startAnimation() {
		mVUMeterLayout.setVisibility(View.VISIBLE);
		img_VUMeter.setVisibility(View.VISIBLE);
		img_VUMeter.setBackgroundResource(R.anim.voice_recording);
		recording_anim = (AnimationDrawable) img_VUMeter.getBackground();
		recording_anim.start();
	}

	private void stopAnimation() {
		if (recording_anim != null && recording_anim.isRunning()) {
			recording_anim.stop();
			recording_anim = null;
		}
		img_VUMeter.setVisibility(View.GONE);
		mVUMeterLayout.setVisibility(View.GONE);
	}

	/*
	 * Called when Recorder changed it's state.
	 */
	public void onStateChanged(int state) {
		if (state == Recorder.PLAYING_STATE
				|| state == Recorder.RECORDING_STATE) {
			mSampleInterrupted = false;
			mErrorUiMessage = null;
		}

		updateUi();
	}

	/*
	 * Called when MediaPlayer encounters an error.
	 */
	public void onError(int error) {
		Resources res = getResources();

		String message = null;
		switch (error) {
		case Recorder.STORAGE_ACCESS_ERROR:
			message = res.getString(R.string.error_sdcard_access);
			break;
		case Recorder.IN_CALL_RECORD_ERROR:
			// TODO: update error message to reflect that the recording
			// could not be
			// performed during a call.
		case Recorder.INTERNAL_ERROR:
			message = res.getString(R.string.error_app_internal);
			break;
		}
		if (message != null) {
			new AlertDialog.Builder(this).setTitle(R.string.app_name)
					.setMessage(message).setPositiveButton("ok", null)
					.setCancelable(false).show();
		}
	}

	private SeekBar.OnSeekBarChangeListener mSeekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {
		private final int DELTA = SEEK_BAR_MAX / 20;

		private int mProgress = 0;

		private boolean mPlayingAnimation = false;

		private boolean mForwardAnimation = true;

		@Override
		public void onStopTrackingTouch(SeekBar seekBar) {
			mRecorder.startPlayback((float) seekBar.getProgress()
					/ SEEK_BAR_MAX);
		}

		@Override
		public void onStartTrackingTouch(SeekBar seekBar) {
			mRecorder.pausePlayback();
			mPlayingAnimation = false;
		}

		@Override
		public void onProgressChanged(SeekBar seekBar, int progress,
				boolean fromUser) {
			if (fromUser) {
				if (!mPlayingAnimation) {
					mForwardAnimation = true;
					mPlayingAnimation = true;
					mProgress = progress;
				}

				if (progress >= mProgress + DELTA) {
					if (!mForwardAnimation) {
						mForwardAnimation = true;
					}
					mProgress = progress;
				} else if (progress < mProgress - DELTA) {
					if (mForwardAnimation) {
						mForwardAnimation = false;
					}
					mProgress = progress;
				}

				long time = (long) ((((float) progress) / SEEK_BAR_MAX) * mRecorder
						.sampleLength());
				String timeStr = String.format(mTimerFormat1, time % 60);
				setTimerView(timeStr);
				mLastButtonId = 0;
			}
		}
	};

	private class RecorderReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent
					.hasExtra(RecorderService.RECORDER_SERVICE_BROADCAST_STATE)) {
				boolean isRecording = intent
						.getBooleanExtra(
								RecorderService.RECORDER_SERVICE_BROADCAST_STATE,
								false);
				mRecorder.setState(isRecording ? Recorder.RECORDING_STATE
						: Recorder.IDLE_STATE);
			} else if (intent
					.hasExtra(RecorderService.RECORDER_SERVICE_BROADCAST_ERROR)) {
				int error = intent.getIntExtra(
						RecorderService.RECORDER_SERVICE_BROADCAST_ERROR, 0);
				mRecorder.setError(error);
			}
		}
	}

	// private void getUploadFileData(final String filePath) {
	// showProgressDialog();
	// DataMiddleWare d = DataMiddlewareFactory
	// .createDataMiddleware(SystemConfig.isTest);// isTest
	// // 真数据为假，假数据为真
	// HashMap<String, String> mHashMap = new HashMap<String, String>();
	// mHashMap.put("actId", "22");
	// mHashMap.put("userId", mSharePrefUtil.getString("UserId"));
	// String fileType = "amr";
	// if (filePath.contains(".")) {
	// fileType = filePath.substring(filePath.lastIndexOf(".") + 1,
	// filePath.length());
	// }
	// mHashMap.put("file", Base64Util.GetImageStr(filePath));
	// mHashMap.put("fileType", fileType);
	// mHashMap.put("type", "5");
	// // getLoginResBean 中封装了HTTP请求代码
	// d.getUpLoadCertificateBean(CTX, mHashMap,// 真数据有返回值，假数据无返回值
	// new RequestCbk<HeadPicResBean>() {
	// // 设置网络请求权限
	// @Override
	// public void onSuccess(HeadPicResBean result) {
	// dismissProgressDialog();
	// if (result.getCode().equals("1")) {
	//
	// FileOperationUtil.deleteFile(filePath);
	// Intent intent = new Intent();
	// intent.putExtra("localPath", filePath);
	// intent.putExtra("serverPath", result.getFileUrl());
	// setResult(4, intent);
	// finish();
	// } else if ("18".equals(result.getCode())) {
	// ToastUtil.showShort(CTX, result.getErrMsg());
	// mSharePrefUtil.setString("UserId", "");
	// skip(LoginAct.class, true);
	// } else {
	// ToastUtil.showShort(CTX, "上传语音失败！");
	// dismissProgressDialog();
	// ToastUtil.showShort(CTX, result.getErrMsg());
	//
	// }
	// dismissProgressDialog();
	// }
	//
	// @Override
	// public void onFailure(HttpException error, String msg) {
	//
	// ToastUtil.showShort(CTX, "发送图片失败！");
	// dismissProgressDialog();
	// }
	//
	// @Override
	// public Class<?> getClassType() {
	// return HeadPicResBean.class;
	// }
	// });
	// }
	//
	// @Override
	// protected int getContentViewID() {
	// // TODO Auto-generated method stub
	// return R.layout.act_sound;
	// }
	Dialog _loadingDialog = null;

	private void onUploadVioce(final String filePath) {
		MyUtils.showLog("onSetPhoto:.....");
		_loadingDialog = DialogUtil.createLoadingDialog(this, "提交中...");
		_loadingDialog.show();

		UploadBean uploadBean = new UploadBean();
		uploadBean.setFileType("amr");
		uploadBean.setType(Upload.QUESTION_VOICE);

		byte[] data = MyUtils.GetData(filePath);
		String s_data = android.util.Base64
				.encodeToString(data, Base64.DEFAULT);
		uploadBean.setFile(s_data);

		FileUploadTask task = new FileUploadTask(uploadBean);
		task.setTaskCallBack(new TaskPostCallBack<DDResult>() {
			@Override
			public void taskFinish(DDResult result) {

				if (_loadingDialog != null) {
					_loadingDialog.dismiss();
					_loadingDialog = null;
				}

				if (result.getError() == RetError.NONE) {
					Bundle b = result.getBundle();
					String fileUrl = b.getString("fileUrl");
					FileOperationUtil.deleteFile(filePath);
					Intent intent = new Intent();
					intent.putExtra("localPath", filePath);
					intent.putExtra("serverPath", fileUrl);
					setResult(2, intent);
					finish();
				} else {
					ToastUtil.showToast(result.getErrorMessage());
				}

			}
		});
		task.executeParallel("");
	}
}
