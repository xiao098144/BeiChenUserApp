package com.ddoctor.component;

import java.util.Locale;

import android.app.Activity;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.user.view.DiscView;
import com.ddoctor.user.view.DiscView.DiscViewOnChangedListener;
import com.ddoctor.utils.StringUtils;

public class BloodSugarPopupWindow extends PopupWindow implements DiscViewOnChangedListener{
	private DiscView _discView;

	private TextView tv_value_mmol;
	private TextView tv_value_mg;
	
	private Activity _activity = null;
	
	private OnClickListener _okListener = null;
	
	public void setOnBtnOkClickListener(OnClickListener listener)
	{
		_okListener = listener;
	}

	public BloodSugarPopupWindow(final Activity context) {
		super(context);
		_activity = context;
		
		buildUI(context);

		_discView = (DiscView) getContentView().findViewById(R.id.discView);
		_discView.setDiscViewOnChangedListener(this);
		tv_value_mmol = (TextView)getContentView().findViewById(R.id.addbs_tv_value_mmol);
		tv_value_mg = (TextView)getContentView().findViewById(R.id.addbs_tv_value_mg);

		updateMGValue(tv_value_mmol.getText().toString());
	}
	
	public float getValue(){
		return Float.valueOf(tv_value_mmol.getText().toString());
	}
	
	public void setValue(float value)
	{
		if( value <= 0 )
			return;
		
		String s = String.format(Locale.CHINESE, "%.01f", value);
		tv_value_mmol.setText(s);
		updateMGValue(s);
		
		float degree = DiscView.value2degree(value);
		
		_discView.setDegree(degree, true);
	}
	
	
	public void show(View parent)
	{
		this.showAtLocation(parent, Gravity.BOTTOM, 0, 0);
		
	}
	
	private void buildUI(final Activity context){
		View view = View.inflate(context, R.layout.layout_bloodsugar_popwin, null);
		this.setContentView(view);
		this.setWidth(LayoutParams.MATCH_PARENT);
		this.setHeight(LayoutParams.WRAP_CONTENT);
		view.startAnimation(AnimationUtils.loadAnimation(context,
				R.anim.slide_in_from_bottom));
		view.setFocusable(true);
		view.setFocusableInTouchMode(true);

		this.setBackgroundDrawable(new ColorDrawable(0x00000000));

		WindowManager.LayoutParams lp = context.getWindow().getAttributes();
		lp.alpha = 0.4f;
		context.getWindow().setAttributes(lp);

		// 设置点击窗口外边窗口消失
		this.setOutsideTouchable(true);

//		popupWindow.setSoftInputMode(PopupWindow.INPUT_METHOD_NEEDED);
//		popupWindow
//				.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

		// 设置此参数获得焦点，否则无法点击
		this.setFocusable(true);
//		this.showAtLocation(parent, Gravity.BOTTOM, 0, 0);
		this.update();
		this.setOnDismissListener(new OnDismissListener() {

			@Override
			public void onDismiss() {
				// TODO Auto-generated method stub
				WindowManager.LayoutParams lp = context.getWindow().getAttributes();
				lp.alpha = 1f;
				context.getWindow().setAttributes(lp);
			}

		});

		TextView cancelTextView = (TextView) view.findViewById(R.id.btn_cancel);
		cancelTextView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dismiss();
			}

		});

		Button btnOk = (Button) view.findViewById(R.id.btn_ok);
		btnOk.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if( _okListener != null )
					_okListener.onClick(v);
				dismiss();
			}
		});

		Button btnCancel = (Button) view.findViewById(R.id.btn_cancel);
		btnCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dismiss();
			}
		});

		view.setOnKeyListener(new OnKeyListener() {

			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				// TODO Auto-generated method stub
				if (keyCode == KeyEvent.KEYCODE_BACK) {
					dismiss();
					return true;
				}
				return false;
			}
		});

		
	}
	
	public void updateMGValue(String value)
	{
		float valueInmg = Float.valueOf(value)
				* Integer.valueOf(_activity.getResources().getString(R.string.mmol2mg));

		StringBuilder sb = new StringBuilder();
		sb.append(StringUtils.formatnum(valueInmg, "#.0"));
		sb.append("mg/dl");
		tv_value_mg.setText(sb.toString());
		
	}

	@Override
	public void onDegreeChanged(float degree) {
		String s = String.format(Locale.CHINESE, "%.01f", DiscView.degree2value(degree));
		tv_value_mmol.setText(s);
		updateMGValue(s);
		
	}
}