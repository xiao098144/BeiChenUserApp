package com.ddoctor.component.PhotoAlbum;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.RelativeLayout;

import com.beichen.user.R;
import com.ddoctor.user.activity.BaseActivity;
import com.ddoctor.utils.MyUtils;
import com.ddoctor.utils.ToastUtil;
import com.umeng.analytics.MobclickAgent;
import com.zhuge.analysis.stat.ZhugeSDK;

public class DDPhotoAlbumActivity extends BaseActivity{
	

	AlbumHelper _helper;
	List<ImageBucket> _bucketList;
	List<ImageItem> _imageList;
	GridView _gridView;
	ImageGridAdapter _adapter;// 自定义的适配器
	

	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("DDPhotoAlbumActivity");
		MobclickAgent.onResume(DDPhotoAlbumActivity.this);
		ZhugeSDK.getInstance().init(getApplicationContext());
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("DDPhotoAlbumActivity");
		MobclickAgent.onPause(DDPhotoAlbumActivity.this);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_photoalbum);
		
		_helper = AlbumHelper.getHelper();
		_helper.init(getApplicationContext());
		
		
		setResult(0);
		
		initData();
		initUI();
		
	}

	private void initUI() {
		this.setTitle("图片");
		
		RelativeLayout rl = (RelativeLayout) findViewById(R.id.titleBar);
		if (rl != null)
			rl.setBackgroundColor(getResources().getColor(R.color.default_titlebar));
		
		getLeftButton().setText("返回");
		getLeftButton().setOnClickListener(this);
		getLeftButton().setVisibility(View.VISIBLE);
		
//		getRightButton().setText("确定");
//		getRightButton().setOnClickListener(this);
//		getRightButton().setVisibility(View.VISIBLE);
		
		
		Button btn = (Button)findViewById(R.id.btn_ok);
		btn.setOnClickListener(this);
		_gridView = (GridView)findViewById(R.id.gridView);
		
		_adapter = new ImageGridAdapter(this, _imageList, mHandler);
		_gridView.setAdapter(_adapter);
	}
	
	

	Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case 0:
				ToastUtil.showToast("最多选择9张图片");
				break;

			default:
				break;
			}
		}
	};
	
	
	private void initData(){
		_bucketList = _helper.getImagesBucketList(false);
		if( _bucketList.size() > 0 )
		{
			int idx = -1;
			for(int i = 0; i < _bucketList.size(); i++)
			{
				ImageBucket ib = _bucketList.get(i);
				MyUtils.showLog(ib.bucketName);
				if( ib.bucketName.equalsIgnoreCase("Camera") )
					idx = i;
			}
			
			if( idx == -1 )
			{
				// 没找到, 把所有图片都放到一起
				_imageList = new ArrayList<ImageItem>();
				_helper.loadImagesList(_imageList);
			}
			else
				_imageList = _bucketList.get(idx).imageList;
		}
	}
	
	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.btn_left:
			this.finishThisActivity();
			break;
		case R.id.btn_ok:
			on_btn_ok();
			break;
		default:
			break;
		}

	}

	private void on_btn_ok()
	{
		if( _adapter.map.size() == 0 )
		{
			ToastUtil.showToast("请选择图片！");
			return;
		}
		
		
		ArrayList<String> list = new ArrayList<String>();
		Collection<String> c = _adapter.map.values();
		Iterator<String> it = c.iterator();
		for (; it.hasNext();) {
			list.add(it.next());
		}
		
		for(int i = 0; i < list.size(); i++ )
			MyUtils.showLog(list.get(i));
		
		Intent intent = new Intent();
		intent.putStringArrayListExtra("list",list);
		
		setResult(1, intent);
		this.finish();
	}
}
