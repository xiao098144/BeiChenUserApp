package com.ddoctor.interfaces;

import android.os.Bundle;

public interface OnDataSetChangedListener {

	/** 饮食首页 点选日期以后 通知更新fragment */
	void onDataSetChaned(Bundle data1 , Bundle data2 , Bundle data3);
	
	void onRefreshUI(Bundle data);
	
}
