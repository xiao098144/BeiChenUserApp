package com.ddoctor.interfaces;

import android.os.Bundle;


public interface OnConnectFragmentActivityListener {

	void notifyFragment(Bundle data);  // 通知fragment   
	
}
