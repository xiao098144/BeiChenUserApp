package com.ddoctor.application;

import io.rong.imkit.RongIM;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Vibrator;

import com.baidu.location.LocationClient;
import com.baidu.mapapi.SDKInitializer;
import com.ddoctor.user.service.PollingBroadcastReceiver;
import com.ddoctor.utils.CrashHandler;
import com.ddoctor.utils.MyUtils;
import com.rongcloud.RCProvider;
import com.rongcloud.RongCloudEvent;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.tauth.Tencent;

public class MyApplication extends Application {
	private static MyApplication _instance;
	private static List<Activity> activityList = new ArrayList<Activity>();

	/**
	 * 百度地图
	 */
	public static LocationClient mLocationClient;
	public Vibrator mVibrator;

	public static IWXAPI mwxAPI;
	public static Tencent tencent;
	
	@Override
	public void onCreate() {
		super.onCreate();
		MyUtils.showLog("MyApplication::onCreate()");
		SDKInitializer.initialize(getApplicationContext());
		if ("com.beichen.user".equals(getCurProcessName(getApplicationContext()))) {
			RongIM.init(this);
			RongCloudEvent.init(this);
			RCProvider.init(this);
		}
		_instance = this;

		 CrashHandler catchHandler = CrashHandler.getInstance();
		 catchHandler.init(this);

		mLocationClient = new LocationClient(this.getApplicationContext());
		//mGeofenceClient = new GeofenceClient(getApplicationContext());

		mVibrator = (Vibrator) getApplicationContext().getSystemService(
				Service.VIBRATOR_SERVICE);

		IntentFilter filter = new IntentFilter(Intent.ACTION_TIME_TICK);
		registerReceiver(new PollingBroadcastReceiver(), filter);
		
	}
	
	@Override
	public void onLowMemory()
	{
		MyUtils.showLog("MyApplication::onLowMemory()");
		super.onLowMemory();
	}
	
	@Override
	public void onTerminate(){
		MyUtils.showLog("MyApplication::onTerminate()");
		super.onTerminate();
		
	}

	/**
     * 获得当前进程号
     *
     * @param context
     * @return
     */
    public static String getCurProcessName(Context context) {
        int pid = android.os.Process.myPid();
        ActivityManager activityManager = (ActivityManager) context
                .getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningAppProcessInfo appProcess : activityManager
                .getRunningAppProcesses()) {
            if (appProcess.pid == pid) {
                return appProcess.processName;
            }
        }
        return null;
    }
	
	public static MyApplication getInstance() {
		return _instance;
	}

	public static void addActivity(Activity activity) {
		activityList.add(activity);
	}

	public static void exit(boolean flag) {
		for (int i = 0; i < activityList.size(); i++) {
			Activity activity = activityList.get(i);
			if (activity != null) {
				activity.finish();
			}
		}
		activityList.clear();
		if (flag) {
			System.exit(0);
		}

	}
}
