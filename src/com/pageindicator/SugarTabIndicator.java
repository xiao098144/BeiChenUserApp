package com.pageindicator;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;
import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.beichen.user.R;
import com.ddoctor.utils.MyUtils;
import com.ddoctor.utils.ToastUtil;

/**
 * 血糖时段标签栏 改写自ViewpagerIndicator-TabPagerIndicator
 * 
 * @author 萧
 * @Date 2015-5-19下午3:00:21
 * @TODO TODO 选中项始终滑动至屏幕水平中央
 */
public class SugarTabIndicator extends HorizontalScrollView implements OnTouchListener {

	/** Title text used when no title is provided by the adapter. */
	private static final CharSequence EMPTY_TITLE = "";

	/**
	 * Interface for a callback when the selected tab has been reselected.
	 */
	public interface OnTabReselectedListener {
		/**
		 * Callback when the selected tab has been reselected.
		 * 
		 * @param position
		 *            Position of the current center item.
		 */
		void onTabReselected(int position);
	}

	private Runnable mTabSelector;

	public interface TabSelectedListener {
		void onSelected(int position);

		int getSelectedPosiiton();
	}

	private TabSelectedListener tabSelectedListener;

	public void setTabSelectedListener(TabSelectedListener tabSelectedListener) {
		this.tabSelectedListener = tabSelectedListener;
	}

	private final OnClickListener mTabClickListener = new OnClickListener() {
		public void onClick(View view) {
			TabView tabView = (TabView) view;
			final int oldSelected = tabSelectedListener.getSelectedPosiiton();
			final int newSelected = tabView.getIndex();
			if (newSelected != oldSelected) {
				tabSelectedListener.onSelected(newSelected);
				setCurrentItem(newSelected);
			}
			if (oldSelected == newSelected && mTabReselectedListener != null) {
				mTabReselectedListener.onTabReselected(newSelected);
			}
		}
	};

	private final IcsLinearLayout mTabLayout;

	private int mMaxTabWidth;
	private int mSelectedTabIndex;
	private static final int mVisibleCount = 3;

	private OnTabReselectedListener mTabReselectedListener;

	public SugarTabIndicator(Context context) {
		this(context, null);
	}

	public SugarTabIndicator(Context context, AttributeSet attrs) {
		super(context, attrs);
		setHorizontalScrollBarEnabled(false);
		
		this.setOnTouchListener(this);

		mTabLayout = new IcsLinearLayout(context,
				R.attr.vpiTabPageIndicatorStyle);
		addView(mTabLayout, new ViewGroup.LayoutParams(WRAP_CONTENT,
				MATCH_PARENT));
		

	}

	public void setOnTabReselectedListener(OnTabReselectedListener listener) {
		mTabReselectedListener = listener;
		
	}
	

	private int lastScrollX; // 上次偏移量
	private int length; // 滑动距离

	private void animateToTab(final int position) {
		// final TabView tabView = (TabView) mTabLayout.getChildAt(position);
		if (mTabSelector != null) {
			removeCallbacks(mTabSelector);
		}
		int scrollX = getScrollX() - lastScrollX; // 当前偏移量与上次偏移量的差值 判断有无滑动
		if (getScrollX() == 0 && scrollX == 0&&position==1) { // 初始状态下无需滑动
			length = 0;
		} else { //
			length = computeTabCenter(position, tabWidth) - screenWidth / 2;
		}
		mTabSelector = new Runnable() {
			public void run() {
				smoothScrollBy(length, 0);
				mTabSelector = null;
			}
		};
		lastScrollX = getScrollX();
		post(mTabSelector);
	}

	/**
	 * 计算position位置tab中心点相对屏幕左侧的距离 tab宽度限定为屏幕三分之一
	 * 
	 * @return
	 */
	private int computeTabCenter(int position, int tabWidth) {
		int ss = getScrollX() / tabWidth; // 当前最左边显示的是第几个tabview
		int leftOutScreen = getScrollX() % tabWidth; // 当前最左侧tab已移除屏幕的距离
		return (position - ss) * tabWidth - leftOutScreen + tabWidth / 2;
	}

	@Override
	public void onAttachedToWindow() {
		super.onAttachedToWindow();
		if (mTabSelector != null) {
			// Re-post the selector we saved
			post(mTabSelector);
		}
	}

	@Override
	public void onDetachedFromWindow() {
		super.onDetachedFromWindow();
		if (mTabSelector != null) {
			removeCallbacks(mTabSelector);
		}
	}

	private void addTab(int index, CharSequence text) {
		final TabView tabView = new TabView(getContext());
		tabView.mIndex = index;
		tabView.setFocusable(true);
		tabView.setTextSize(20);
		tabView.setGravity(Gravity.CENTER);
		tabView.setTextColor(Color.WHITE);
		tabView.setPadding(5, 5, 5, 5);
		if (text != EMPTY_TITLE) {
			tabView.setOnClickListener(mTabClickListener);
			tabView.setText(text);
		}
		tabView.setWidth(tabWidth);
		mTabLayout.addView(tabView, new LinearLayout.LayoutParams(0,
				MATCH_PARENT, 1));
	}

	public void notifyDataSetChanged() {
		mTabLayout.removeAllViews();
		final int count = title.length;
		addTab(0, EMPTY_TITLE);
		for (int i = 0; i < count; i++) {
			
			CharSequence tt  = title[i];
			if (i==0) {
				tt= title[i]+"三时";
			}
			
			if (tt == null) {
				tt = EMPTY_TITLE;
			}
			addTab(i + 1, tt);
		}
		addTab(count + 1, EMPTY_TITLE);
		if (mSelectedTabIndex > count) {
			mSelectedTabIndex = count - 1;
		}
		if (mSelectedTabIndex == 0) {
			mSelectedTabIndex += 1;
		}
		setCurrentItem(mSelectedTabIndex);
		requestLayout();
	}

	String[] title;

	public void setTitle(String[] title) {
		if (tabSelectedListener == null) {
			ToastUtil.showToast("先设置TabSelectedListener");
			throw new IllegalStateException(
					"tabSelectedListener can not be null");
		}
		if (tabWidth == 0) {
			ToastUtil.showToast("Tab 宽度不能为0 应设置为屏幕宽度的三分之一");
			throw new IllegalArgumentException(
					"tabWidth can not be 0 should be SW/3");
		}
		this.title = title;
		notifyDataSetChanged();
	}

	public void setTitle(String[] title, int initIndex) {
		 mSelectedTabIndex = initIndex;
		setTitle(title);
	}

	public void setCurrentItem(int item) {
		mSelectedTabIndex = item;
		if (tabSelectedListener != null) {
			tabSelectedListener.onSelected(item);
		}
		final int tabCount = mTabLayout.getChildCount();
		for (int i = 0; i < tabCount; i++) {
			if (i > 0 && i < tabCount - 1) {
				final TabView child = (TabView) mTabLayout.getChildAt(i);
				final boolean isSelected = (i == item);
				child.setSelected(isSelected);
				if (isSelected) {
					animateToTab(item);
				}
			}
		}
	}

	private class TabView extends TextView {
		private int mIndex;

		public TabView(Context context) {
			super(context, null, R.attr.vpiTabPageIndicatorStyle);
		}

		@Override
		public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
			super.onMeasure(widthMeasureSpec, heightMeasureSpec);
			if (mMaxTabWidth > 0 && getMeasuredWidth() > mMaxTabWidth) {
				super.onMeasure(MeasureSpec.makeMeasureSpec(mMaxTabWidth,
						MeasureSpec.EXACTLY), heightMeasureSpec);
			}
		}

		public int getIndex() {
			return mIndex;
		}

	}

	int tabWidth;
	int screenWidth;

	public void setTabWidth(int width) {
		if (tabSelectedListener == null) {
			throw new IllegalStateException(
					"tabSelectedListener can not be null");
		}
		screenWidth = width;
		tabWidth = width / mVisibleCount;
	}

	@Override
	public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		final int widthMode = MeasureSpec.getMode(widthMeasureSpec);
		final boolean lockedExpanded = widthMode == MeasureSpec.EXACTLY;
		setFillViewport(lockedExpanded);

		final int childCount = mTabLayout.getChildCount();
		if (childCount > 1
				&& (widthMode == MeasureSpec.EXACTLY || widthMode == MeasureSpec.AT_MOST)) {
			if (childCount > 2) {
				mMaxTabWidth = (int) (MeasureSpec.getSize(widthMeasureSpec) * 0.4f);
			} else {
				mMaxTabWidth = MeasureSpec.getSize(widthMeasureSpec) / 2;
			}
		} else {
			mMaxTabWidth = -1;
		}

		final int oldWidth = getMeasuredWidth();
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		final int newWidth = getMeasuredWidth();

		if (lockedExpanded && oldWidth != newWidth) {
			if (mSelectedTabIndex == 0) {
				mSelectedTabIndex += 1;
			}
			if (mSelectedTabIndex == childCount - 1) {
				mSelectedTabIndex = childCount - 2;
			}
			setCurrentItem(mSelectedTabIndex);
		}
	}

	
	private int lastX = 0;
	private int touchEventId = -9983761;
	
	Handler handler = new Handler() {
	    @Override
	    public void handleMessage(Message msg) {
	        super.handleMessage(msg);
	        View scroller = (View)msg.obj;
	        if(msg.what==touchEventId) 
	        {
	            if( lastX == scroller.getScrollX()) 
	            {
	                handleStop(scroller);
	            }
	            else 
	            {
	                handler.sendMessageDelayed(handler.obtainMessage(touchEventId,scroller), 5);
	                lastX = scroller.getScrollX();
	            }
	        }
	    }
	};
	
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		 if(event.getAction() == MotionEvent.ACTION_UP) {
		        handler.sendMessageDelayed(handler.obtainMessage(touchEventId,v), 5);
		    }
		return false;
	}
	
	//这里写真正的事件
	private void handleStop(Object view) {
		// 滚动停止，修正tab位置
		
		MyUtils.showLog("scrollX=" + this.getScrollX());
		
		// 判断最接近屏幕中心点的是哪一个tab
		final int tabCount = mTabLayout.getChildCount();
		int left = -this.getScrollX();
		int centerX = this.screenWidth / 2;
		for (int i = 0; i < tabCount; i++) {
			int right = left + this.tabWidth;
			
			if( centerX >= left && centerX <= right )
			{
				this.setCurrentItem(i);
				break;
			}
			
			left += tabWidth;
		}
		
	}
}
